/* initialisation on load */

jQuery(document).ready(function () {
    var announcement_id = ""
    announcement_id = $("#announcementID").val();

    if(announcement_id !== undefined){
        var annoucementData = getAnnouncement(announcement_id);
    }

    /*function to get a Announcement data by id */
    function getAnnouncement(announcement_id) {
        $.ajax
        ({
            type: 'get',
            url: '/announcement-ajax',
            data: {
                class: "AnnouncementAjax",
                action: "getAnnouncement",
                id: announcement_id

            },
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    var announcementData = response.data;
                    if(announcementData.title == 'System Maintainance'){
                        $("#ism").attr("checked",true);
                        $(".title").attr("readonly", "readonly");
                        $(".end_date").attr("readonly", "readonly");
                        $(".end_time").attr("disabled", "disabled");
                        $('.description').attr("disabled", "disabled");
                        $('.end_date').css("background", "#eee");
                        $('.end_time').css("background", "#eee");
                        $('.descr-label').html("Description");

                    }
                    $("#title").val(announcementData.title);
                    $(".start_date").datepicker({dateFormat: datepicker_format});
                    $(".end_date").datepicker({dateFormat: datepicker_format});
                    $(".start_date").datepicker('setDate', announcementData.start_date);
                    $('.start_time').timepicker('setTime', announcementData.start_time);
                    $('.end_time').timepicker('setTime', announcementData.end_time);
                    $(".end_date").datepicker('setDate', announcementData.end_date);
                    $("#description").val(announcementData.description);
                    if(announcementData.title == 'System Maintainance'){
                        $(".end_date").datepicker("destroy");
                    }
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    var $dp1 = $(".start_date").datepicker({
        dateFormat: datepicker_format,
        changeYear: true,
        minDate: 0,
        yearRange: "-100:+20",
        changeMonth: true,
        onClose: function (selectedDate) {
            $(".end_date").datepicker("option", "minDate", selectedDate);
        }
    });

    $(".end_date").datepicker({
        dateFormat: datepicker_format,
        changeYear: true,
        minDate: 0,
        yearRange: "-100:+20",
        changeMonth: true,
        onClose: function (selectedDate) {
            $(".start_date").datepicker("option", "maxDate", selectedDate);
        }
    });
    $('.start_time').timepicker();

    $('.end_time').timepicker();
    $('.ism').change(function () {
        if ($(this).is(":checked")) {
            console.log("test");
            $(".title").val("System Maintainance");
            $(".title").attr("readonly", "readonly");
            $(".end_date").attr("readonly", "readonly");
            $(".end_time").attr("disabled", "disabled");
            $('.description').attr("disabled", "disabled");
            $('.end_date').css("background", "#eee");
            $('.end_time').css("background", "#eee");
            $('.descr-label').html("Description");
            jQuery(".start_date").val("");
            jQuery(".start_time").val("");
            jQuery(".end_date").val("");
            jQuery(".end_time").val("");
            jQuery(".description").val("");
            $(".end_date").datepicker("destroy");
        } else {
            console.log("test2");

            jQuery(".title").val("");
            jQuery(".title").removeAttr("readonly");
            $('.description').removeAttr("disabled");
            $(".end_date").removeAttr("disabled");
            $(".end_time").removeAttr("disabled");
            $('.end_date').css("background", "none");
            $('.end_time').css("background", "none");
            $('.descr-label').html("Description <em class='red-star'>*</em>");
            $( ".end_date" ).val('');
            $( ".start_date" ).datepicker('setDate','');
            $(".end_date").datepicker({dateFormat: datepicker_format,minDate: 0,});
            $(".end_time").timepicker();
        }
    });

    /* System maintainance start and end date */
    jQuery(".start_date").change(function () {
        if ($('.ism').is(':checked')) {
            $(".end_date").val($(".start_date").val());
            $(".end_date").datepicker("destroy");
        }
    });

    /* System maintainance start and end time */
    $('.start_time').timepicker('option', 'change', function (time) {
        if ($('.ism').is(':checked')) {

            var later = new Date(time.getTime() + (2 * 60 * 60 * 1000));
            $('.end_time').timepicker();
            $('.end_time').timepicker('setTime', later);
            var startT = $.trim($(".start_time").val());
            if ("10:00 PM" == startT || "10:30 PM" == startT ||
                    "11:00 PM" == startT || "11:30 PM" == startT
                    || "12:00 PM" == startT) {
                var startD = $.trim($(".start_date").val());
                var date = new Date(startD);
                date.setDate(date.getDate() + 1);
                date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
                //$(".end_date").val(date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate());
              //  $(".end_date").datepicker('setDate', date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate());
                $(".end_date").datepicker({dateFormat: datepicker_format});
                var date2 = $('.start_date').datepicker('getDate','+1d');
                date2.setDate(date2.getDate()+1);
                $('.end_date').datepicker('setDate', date2);
                $(".end_date").datepicker("destroy");
            } else {
                var starD = $.trim($(".start_date").val());
                $(".end_date").val(starD);
            }


        }
    });

    jQuery(".announcement-save").click(function () {

        var valuestart = $.trim($(".start_time").val());
        var valuestop = $.trim($(".end_time").val());
        var start_date = $.trim(jQuery(".start_date").val());
        var end_date = $.trim(jQuery(".end_date").val());
        var diff = countDiff(valuestart, valuestop);
        if (start_date != "0" && start_date != "" && end_date != "0" && end_date != "") {
            if (start_date == end_date) {
                if (parseInt(diff) < 0) {
                    alert("End time cannot be less than start time!");
                    return false;
                } else if (parseInt(diff) === 0) {
                    alert("Start time and End time cannot be Equal.");
                    return false;
                } else {
                    return true;
                }
            }
        }
    });

    /*function to calculate time */
    function calcTime(time) {
        if (~time.indexOf("PM")) {
            var getTime = time.split(" ");
            var getHM = getTime[0].split(":");
            var getHour = getHM[0];
            var getMint = getHM[1];
            var totalhour = parseInt(getHour) + 12;
            var totalmint = totalhour * 60;
            var overallTime = totalmint + parseInt(getMint);
            return overallTime;
        } else {
            var getTime = time.split(" ");
            var getHM = getTime[0].split(":");
            var getHour = getHM[0];
            var getMint = getHM[1];
            var totalhour = parseInt(getHour);
            var totalmint = totalhour * 60;
            var overallTime = totalmint + parseInt(getMint);
            return overallTime;
        }
    }

    /*if system maintaenec checkbox is checked */
    if ($('.ism').is(":checked")) {
        $(".end_date").attr("disabled", "disabled");
        $(".end_time").attr("disabled", "disabled");
        $(".description").attr("disabled", "disabled");
        $("#title_name").attr("readonly", "readonly");
        $(".end_date").css("background", "#eee");
        $(".end_time").css("background", "#eee");
        $('.descr-label').html("Description");
    } else {
        $('.descr-label').html("Description <em class='red-star'>*</em>");
        $(".end_date").removeAttr("disabled");
        $(".end_time").removeAttr("disabled");
        $(".description").removeAttr("disabled");
    }

    /*function to get a count difference */
    function countDiff(starTime, endTime) {
        //var output = ( new Date("1970-1-1 12:00") - new Date("1970-1-1 22:00") ) / 1000 / 60 / 60 ;
        var startTime1 = convertTime12to24(starTime);
        var endTime1 = convertTime12to24(endTime);
        var end_time = "1970-1-1 " + endTime1;
        var start_time = "1970-1-1 " + startTime1;

        var output = (new Date(end_time) - new Date(start_time)) / 1000 / 60 / 60;
        return output;
    }

    /*function to convert a time format  */
    function convertTime12to24(time12h) {
        console.log(time12h);
        var explodeTime = time12h.split(' ');
        //let [time, modifier] = time12h.split(' ');
        var explodeHour = explodeTime[0].split(':');
        //let [hours, minutes] = explodeTime[0].split(':');

        if (explodeHour[0] === '12') {
            explodeHour[0] = '00';
        }

        if (explodeTime[1] === 'PM') {
            explodeHour[0] = parseInt(explodeHour[0], 10) + 12;
        }

        return explodeHour[0] + ':' + explodeHour[1];
    }


    //add announcement
    $("#add_announcement").validate({
        rules: { title: {
                required: true
            }, start_date: {
                required: true
            }, start_time: {
                required: true
            }, end_date: {
                required: true
            }, end_time: {
                required: true
            }, description: {
                required: function (element) {
                    if ($("#ism").prop('checked') == true) {
                        return false;
                    } else {
                        return true;
                    }

                }
            }
        },
        submitHandler: function (form) {
            event.preventDefault();
            var title = $("#title").val();
            var start_date = $("#start_date").val();
            var start_time = $("#start_time").val();
            var end_date = $("#end_date").val();
            var end_time = $("#end_time").val();
            var description = $("#description").val();
            var time = new Date();
            var start_date_time = new Date(start_date + ' ' + start_time);
            var end_date_time = new Date(end_date + ' ' + end_time);
            var current_date = new Date();
            var current_date_time_diff = ((start_date_time - current_date) / 100).toString();
            var end_date_time_diff = ((end_date_time - start_date_time) / 100).toString();
            var timezone_offset_minutes = new Date().getTimezoneOffset();
            timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;
            // alert(timezone_offset_minutes); ; return fasle;
            // Timezone difference in minutes such as 330 or -360 or 0//
            // console.log(timezone_offset_minutes);
            // var dd = start_date.replace('(Wed.)', '');
           //  // dd = str.split(" ",2);
           //  StartDate = moment("22-01-2014").format('YYYY-MM-DD');
           //  // dd = dd.replace(')', '');
           //
           //  alert(StartDate); return false;
           //  //alert(start_date); alert('fdsdf');
           // // // alert(start_date.replace(/([a-zA-Z-. ])/g, ""));
           // //  var datesss = start_date.replace(/[{()}]/g, '').replace(/[^\d-]/g, '');
           // //  var isoDate = new Date(start_date).toISOString();
           // //  var utc = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
           // //  alert(isoDate); return false;
           // var test =  Date.parse(datesss);
           //  return false;
           //  if (current_date_time_diff <= 0) {
           //      toastr.error("The time you have selected has past, please schedule a time in the future.");
           //      return false;
           //  }
           //  if (end_date_time_diff == 0) {
           //      toastr.error("Start Time and End Time cannot be Equal.");
           //      return false;
           //  }
           //
           //  if (end_date_time_diff < 0) {
           //      toastr.error("End Time cannot be Less than start time.");
           //      return false;
           //  }
            var formData = $('#add_announcement').serializeArray();
            $.ajax
                    ({
                        type: 'post',
                        url: '/announcement-ajax',
                        data: {
                            class: "AnnouncementAjax",
                            action: "addAnnouncement",
                            form: formData,
                            timezone: timezone_offset_minutes

                        },
                        success: function (response) {
                            var response = JSON.parse(response);
                            if(response.status == 'success' && response.code == 200){
                                localStorage.setItem("Message","Announcement Created Successfully")
                                localStorage.setItem("rowcolor",true)
                               // toastr.success("Announcement Created Successfully.");
                                window.location.href = '/Announcement/Announcements';
                            }else if(response.status == 'error' && response.code == 502){
                                toastr.error(response.message);
                            } else if(response.status == 'error' && response.code == 400){
                                $('.error').html('');
                                $.each(response.data, function (key, value) {
                                    $('.'+key).html(value);
                                });
                            }
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                // alert(key+value);
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
        }
    });


    //login validation
    $("#edit_announcement").validate({

        rules: { title: {
                required: true
            }, start_date: {
                required: true
            }, start_time: {
                required: true
            }, end_date: {
                required: true
            }, end_time: {
                required: true
            }, description: {
                required: function (element) {
                    if ($("#ism").prop('checked') == true) {
                        return false;
                    } else {
                        return true;
                    }

                }
            }
        },
        submitHandler: function (form) {

            event.preventDefault();
            var title = $("#title").val();
            var start_date = $("#start_date").val();
            var start_time = $("#start_time").val();
            var end_date = $("#end_date").val();
            var end_time = $("#end_time").val();
            var description = $("#description").val();
            var time = new Date();
            var start_date_time = new Date(start_date + ' ' + start_time);
            var end_date_time = new Date(end_date + ' ' + end_time);
            var current_date = new Date();
            var current_date_time_diff = ((start_date_time - current_date) / 100).toString();
            var end_date_time_diff = ((end_date_time - start_date_time) / 100).toString();
            var timezone_offset_minutes = new Date().getTimezoneOffset();
            timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;


            if (current_date_time_diff <= 0) {
                toastr.error("The time you have selected has past, please schedule a time in the future.");
                return false;
            }
            if (end_date_time_diff == 0) {
                toastr.error("Start Time and End Time cannot be Equal.");
                return false;
            }

            if (end_date_time_diff < 0) {
                toastr.error("End Time cannot be Less than start time.");
                return false;
            }
            var formData = $('#edit_announcement').serializeArray();
            var announcementid = $('#announcementID').val();
            $.ajax
            ({
                type: 'post',
                url: '/announcement-ajax',
                data: {
                    class: "AnnouncementAjax",
                    action: "editAnnouncement",
                    form: formData,
                    annoucement_id:announcementid,
                    timezone: timezone_offset_minutes

                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        localStorage.setItem("Message","Announcement Updated Successfully")
                        localStorage.setItem("rowcolor",true)
                        // toastr.success("Announcement Created Successfully.");
                        window.location.href = '/Announcement/Announcements';
                    }else if(response.status == 'error' && response.code == 502){
                        toastr.error(response.message);s
                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });

});