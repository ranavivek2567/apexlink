<div class="modal fade" id="cancel_modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Change Status</h4>
            </div>
            <div class="modal-body">
                <p><img src="<?php echo SUPERADMIN_SITE_URL.'/images/notice.png';?>"> Do you want to cancel this action now?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default yes-cancel">Yes</button>
                <button type="button" class="btn btn-default no-cancel" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>