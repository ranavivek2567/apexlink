<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/admin_header.php"); ?>
<!-- HTML Start -->

<div id="wrapper">
    <!-- Top navigation start -->
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php"); ?>
    <!-- Top navigation end -->

    <!-- Content Start-->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <!-- Sidebar start-->
                <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/sidebar.php"); ?>
                <!-- Sidebar end -->
                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="welcome-text visible-xs">
                        <div class="welcome-text-inner">
                            <div class="col-xs-9">    Welcome:  Sonny Kesseben (ACL Properties), June 01, 2018
                            </div>
                            <div class="col-xs-3">
                                <a herf="javascript:;"><i class="fa fa-calendar" aria-hidden="true"></i></a>
                                <a href="javascript:;"><i class="fa fa-calculator" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="content-rt">
                        <div class="content-rt-hdr">
                            <h3>Admin >> Manage User</h3>
                        </div>
                        <div class="content-data">
                            <!-- Main tabs -->
                            <div class="main-tabs">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#default-settings" aria-controls="home" role="tab" data-toggle="tab">Manage User</a></li>
                                    <li role="presentation"><a href="#clock-settings" aria-controls="profile" role="tab" data-toggle="tab">Change Password</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="default-settings">
                                        <div class="accordion-form">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <form method="post" id="addUserForm">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">
                                                                        <a>New User</a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body">
                                                                        <div class="row">
                                                                            <div class="form-outer">
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>First Name <em class="red-star">*</em></label>
                                                                                    <input class="form-control" placeholder="Eg: Jason" name="first_name" type="text"/>
                                                                                    <span class="first_nameErr error"></span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>MI</label>
                                                                                    <input class="form-control" name="mi" type="text"/>
                                                                                    <span class="miErr error"></span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Last Name <em class="red-star">*</em></label>
                                                                                    <input class="form-control" name="last_name" type="text"/>
                                                                                    <span class="last_nameErr error"></span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Maiden Name</label>
                                                                                    <input class="form-control" name="maiden_name" type="text"/>
                                                                                    <span class="maiden_nameErr error"></span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Nick Name</label>
                                                                                    <input class="form-control" name="nick_name" type="text"/>
                                                                                    <span class="nick_nameErr error"></span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Email <em class="red-star">*</em></label>
                                                                                    <input cltimeass="form-control" name="email" type="text"/>
                                                                                    <span class="emailErr error"></span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Status <em class="red-star">*</em></label>
                                                                                    <select class="form-control" name="status">
                                                                                        <option value="default">Select</option>
                                                                                        <option value="1">Active</option>
                                                                                        <option value="0">InActive</option>
                                                                                    </select>
                                                                                    <span class="statusErr error"></span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Work Phone</label>
                                                                                    <input maxlength="12" class="form-control number_format" name="work_phone" type="text"/>
                                                                                    <span class="work_phoneErr error"></span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Country</label>
                                                                                    <select class="form-control" name="country" id="countryCode">
                                                                                        <option value="default">Select Country Code</option>
                                                                                    </select>
                                                                                    <span class="countryErr error"></span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Mobile Number <em class="red-star">*</em></label>
                                                                                    <input maxlength="12" class="form-control number_format" name="mobile_number" type="text"/>
                                                                                    <span class="mobile_numberErr error"></span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Fax</label>
                                                                                    <input maxlength="12" class="form-control number_format" name="fax" type="text"/>
                                                                                    <span class="faxErr error"></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="btn-outer">
                                                                <button type="submit" class="blue-btn">Save</button>
                                                                <button class="grey-btn">Cancel</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Accordian Ends -->
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="clock-settings">
                                        <div class="accordion-form">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-menu-down"></span> Default Date & Clock Settings</a> <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="form-outer">
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Default Date Format <em class="red-star">*</em></label>
                                                                                <select class="form-control"></select>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Date Format</label>
                                                                                <select class="form-control"></select>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Default Clock Format <em class="red-star">*</em></label>
                                                                                <select class="form-control"></select>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>&nbsp;</label>
                                                                                <input disabled="" class="form-control" type="text"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="btn-outer">
                                                                    <button class="blue-btn">Update</button>
                                                                    <button class="grey-btn">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Accordian Ends -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Content End-->
</div>
<?php include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php"); ?>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/validation/users/manageUser.js" type="text/javascript"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/super_admin/manageUsers.js" type="text/javascript"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/common.js"></script>
<script>
    $(document).ready(function() {
        //get country code
        $.ajax({
            type: 'post',
            url: '/helper-ajax',
            data: {class: 'HelperAjax', action: "getCountryCode", db: 'conn'},
            success: function (response) {
                var data = JSON.parse(response);
                var html = '';
                $.each(data.data, function (key, value) {
                    html += '<option value=' + value.id + '>' + value.name + '(' + value.code + ')</option>';
                });
                $('#countryCode').append(html);
            },
            error: function (data) {
                console.log(data);
            }
        });
    });
</script>

<script>
    $('#leftnav3').addClass('in');
    $('.apexlink_new ').addClass('active');
</script>
</body>
</html>
