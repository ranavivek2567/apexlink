<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}

?>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/admin_header.php"); ?>
<?php include_once(ROOT_URL . "/superadmin/views/settings/manage_user/modal.php");?>
<!-- HTML Start -->
<link rel="stylesheet" type="text/css" href="<?php echo SUBDOMAIN_URL; ?>/company/css/Chart.css">
<div id="wrapper">
    <canvas id="moveInsCharts"></canvas>
</div>
<script  src="<?php echo SUBDOMAIN_URL; ?>/company/js/spotlight/Chart.js"></script>
<script  src="<?php echo SUBDOMAIN_URL; ?>/company/js/spotlight/Chart.bundle.js"></script>
<script  src="<?php echo SUBDOMAIN_URL; ?>/superadmin/js/spotlight/serverSpace.js"></script>
<script>

</script>


</body>
</html>
