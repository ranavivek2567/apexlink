<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_header.php");
$request = $_SERVER['REQUEST_URI'];
$request1 = explode('/', $_SERVER["REQUEST_URI"]);
$key = end($request1);
//print_r($key);die();
if (is_numeric($key)) {
    $id = $key;
} else {
    $id = '';
}

?>
<script type="text/javascript">
    $(document).ready(function () {

        var id = $('#hidden_plan_id').val();
        $.ajax
        ({
            type: 'post',
            url: '/Plans/PlanAjax',
            data: {
                class: 'PlanAjax',
                action: "view",
                id: id
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success")
                {   var result = data.data;
                    $('#plan_name').html(result.plan_name);
                    $('#number_of_units').html(result.number_of_units);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
            }
        });
    });
</script>
<!-- HTML Start -->

<div id="wrapper">

    <!-- Top navigation start -->
    <?php include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/top_navigation.php");?>
    <!-- Top navigation end -->
    <main class="apxpg-main">
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer apxpg-top-search">
                    <div class="row">
                        <!--<div class="col-md-8 col-sm-8 col-xs-12">
                            <strong class="apxpg-title">Title goes here...</strong>
                        </div>-->
                        <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="content-data apxpg-allcontent">
                    <!--single add-form-box-->
                    <div class="apx-adformbox">
                        <div class="apx-adformbox-title">
                            <strong class="left">Plan Details</strong>
                            <a class="back right" href="/Plans/List"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                        </div>
                        <div class="apx-adformbox-content">
                            <div class="row">
                                <input type="hidden" name="hidden_plan_id" id="hidden_plan_id" value="<?php echo $id;?>">
                                <div class="form-outer apx-planrows">
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6"> <label>Plan Name :</label></div>
                                            <div class="col-md-6 col-sm-6"><span id="plan_name"></span></div>
                                        </div>

                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6"> <label>No. of Units :</label></div>
                                            <div class="col-md-6 col-sm-6"><span id="number_of_units"></span></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row apx-border-row">
                                <div class="col-md-2 text-right pull-right col-xs-12">
                                    <a id="editPlan" class="apx-edt-btn"><i class="fa fa-edit"></i> Edit</a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!--End single add-form-box-->

                </div>
            </div>
        </div>
    </section>
    </main>
</div>


<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/validation/plan/plan.js"></script>
<script>
    $('.plan-top').addClass('active');
    var hidden_plan_id = $('#hidden_plan_id').val();
    $('#editPlan').on('click', function() {
        $.ajax({
            type: 'post',
            url: '/Plans/PlanAjax',
            data: {
                class  : 'PlanAjax',
                action : 'checkPlanInUse',
                id     : hidden_plan_id
            },
            success : function(response){
                response = $.parseJSON(response);
                if(response.status == 'success' && response.code == 200){
                    toastr.error(response.message);
                } else {
                    window.location.href = '/Plans/Edit/'+hidden_plan_id;
                }
            }
        });
    });
</script>
<?php
include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
</body>
</html>
<!-- HTML END -->


