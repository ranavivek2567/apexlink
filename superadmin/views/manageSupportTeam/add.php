<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if(isset($_SESSION[SESSION_DOMAIN]['user_id'])){
    if(isset($_POST['logout'])){
        unset($_SESSION[SESSION_DOMAIN]['user_id']);
        //  $url = BASE_URL."login";
        header('Location: '.BASE_URL);
    }
}else{
//    $url = BASE_URL."login";
    header('Location: '.BASE_URL);
}

$request1 = explode('/', $_SERVER["REQUEST_URI"]);
$key = end($request1);
if (is_numeric($key)) {
    if($key == 0){
        $id = '';
    } else {
        $id = $key;
    }
} else {
    $id = '';
}

?>
<?php
include_once(SUPERADMIN_DIRECTORY_URL .  "/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(SUPERADMIN_DIRECTORY_URL .  "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
<style>
    span.error {
        color: red;
    }
</style>
    <!-- Content Start-->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <!-- Sidebar start-->

                <!-- Sidebar end -->
                <div class="col-sm-12">

                    <div class="content-rt">
                        <div class="col-sm-12 bread-search-outer">
                            <div class="col-sm-4 pull-right">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">
                            <!-- Main tabs -->
                            <div class="main-tabs">
                                <!-- Nav tabs -->

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>
                                                Add/Edit New Member
                                            </h3>
                                        </div>
                                        <form name="addMembers" id="addNewMemberForm" method="post">
                                            <div class="form-data">
                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                    <label>First Name <em class="red-star">*</em></label>
                                                    <input id="first_name" name="first_name" class="form-control" type="text"/>
                                                    <span class="error" id="first_nameErr"></span>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                    <label>Mi</label>
                                                    <input id="middle_name" name="middle_name" class="form-control" maxlength="1" type="text"/>
                                                    <span class="error" id="middle_nameErr"></span>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                    <label>Last Name</label>
                                                    <input id="last_name" name="last_name" class="form-control" type="text"/>
                                                    <span class="error" id="last_nameErr"></span>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                    <label>Maiden Name</label>
                                                    <input id="maiden_name" name="maiden_name" class="form-control" type="text"/>
                                                    <span class="error" id="maiden_nameErr"></span>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                    <label>Nick Name</label>
                                                    <input id="nick_name" name="nick_name" class="form-control" type="text"/>
                                                    <span class="error" id="nick_nameErr"></span>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                    <label>Email <em class="red-star">*</em></label>
                                                    <input id="email" name="email" class="form-control" type="text"/>
                                                    <span class="error" id="emailErr"></span>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                    <label>Phone</label>
                                                    <input id="phone_number" name="phone_number" class="form-control number_only superadmin_phoneformat" maxlength="12" type="text"/>
                                                    <span class="error" id="phone_numberErr"></span>
                                                </div>
                                            </div>
                                            <div class="btn-outer text-right">
                                                <input name="edit_support_id" class="form-control" value="<?php echo $id?>" id="edit_support_id" type="hidden"/>
                                                <button type="submit" id="saveBtnId" class="blue-btn">Save</button>


                                                <?php if(!empty($id)){ ?>
                                                <input type='button'  value="Reset" class="clear-btn ResetForm"  >
                                                <?php } else { ?>
                                                <input type='button'  value="Clear" class="clear-btn clearForm" >
                                                <?php } ?>
                                                <button type="button" id="cancelAddNewMember" class="grey-btn">Cancel</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- Content End-->
</div>
<!-- Footer Starts -->
<?php
include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->

<!-- Jquery Starts -->
<script src="<?php echo SUPERADMIN_SITE_URL ?>/js/validation/manageSupportTeam/manageSupportTeam.js"></script>
<script src="<?php echo SUPERADMIN_SITE_URL ?>/js/super_admin/manageSupportTeam/manageSupportTeam.js"></script>
<script type="text/javascript">
    var id = $('#edit_support_id').val();
    if(id == 0){
        // e.preventDefault();
    } else {
        $.ajax
        ({
            type: 'post',
            url: '/Support/SupportAjax',
            data: {
                class: "ManageSupportTeamAjax",
                action: "view",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                // console.log('sdsd', response);
                if (response.status == "success") {
                    $("#first_name").val(response.data.first_name);
                    $("#middle_name").val(response.data.middle_name);
                    $("#last_name").val(response.data.last_name);
                    $("#maiden_name").val(response.data.maiden_name);
                    $("#nick_name").val(response.data.nick_name);
                    $("#email").val(response.data.email);
                    $("#phone_number").val(response.data.phone_number);
                    $("#number_of_units").val(response.data.number_of_units);
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $('.support-team-top').addClass('active');
</script>
<!-- Jquery Ends -->
</body>
</html>



