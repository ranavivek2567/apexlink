<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/17/2019
 * Time: 3:23 PM
 */

if (isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] != '')) {
    $url = BASE_URL . "Dashboard/Dashboard";
    header('Location: ' . $url);
} else {
    // write your code here
}

include_once(SUPERADMIN_DIRECTORY_URL. "/views/layouts/admin_header.php");
?>
<div class="apxpg-login">
<div id="wrapper">
    <div class="login-bg">
        <div class="login-outer">
            <div class="login-logo"><img src="<?php echo SUPERADMIN_SITE_URL;?>/images/logo-login.png"/></div>
            <div class="login-inner">
                <form name="forgot_password" id="forgot_password">
                    <h2>Enter Your Email</h2>
                    <div class="login-data">
                        <label>
                            <input class="form-control" name="email" placeholder="Email" id="email" type="text"/>
                        </label>
                        <div class="btn-outer">
                            <input type="submit" name="send_mail" value="Send Mail" class="blue-btn" id="send_mail"/>
                            <input  type="button" value="Cancel" class="grey-btn" id="forgot_cancel"/>
                        </div>
                    </div>
                </form>
            </div>
            <div id="click_here_login_link"   style="display: none;">
                <p>The Reset Link associated to the email address you provided was emailed to you. Thank you!</p>
                <a href="/" style="text-decoration: underline;">Click Here to Login</a>
            </div>
        </div>
    </div>
</div>
<!-- Wrapper Ends -->
<?php
include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->
<!-- Jquery Starts -->
<script src="<?php echo SUPERADMIN_SITE_URL;?>/js/validation/users/users.js"></script>
<script type="text/javascript">

</script>
<!-- Jquery Ends -->
</div>
</body>

</html>