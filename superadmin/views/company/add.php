<!DOCTYPE html>
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}
?>

<?php include_once(SUPERADMIN_DIRECTORY_URL."/views/layouts/admin_header.php"); ?>

<div id="wrapper">
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php");
    ?>
    <main class="apxpg-main">
        <!-- MAIN Navigation Ends -->
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer apxpg-top-search">
                        <div class="row">
                            <!--<div class="col-md-8 col-sm-8 col-xs-12">
                                <strong class="apxpg-title">Title goes here...</strong>
                            </div>-->
                            <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data apxpg-allcontent">
                        <div class="overlay">
                            <div id='loadingmessage' style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'; >
                                <img width="200"  height="200" src='<?php echo SUPERADMIN_SITE_URL ?>/images/loading.gif'/>
                            </div>
                        </div>
                        <form action="" method="POST" id="addCompanyForm">
                            <!--single add-form-box-->
                            <div class="apx-adformbox">
                                <div class="apx-adformbox-title">
                                    <strong class="left">Add New Company</strong>
                                    <a onclick="goBack()" class="back right" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                </div>
                                <div class="apx-adformbox-content">
                                    <div class="row">
                                        <div class="form-outer">
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>First Name <em class="red-star">*</em></label>
                                                <input class="form-control capsOn" type="text" id="first_name" name="first_name">
                                                <span class="first_nameErr error red-star"></span>
                                            </div>
                                            <div class="col-sm-3 col-md-3 mi-name">
                                                <div class="mi-name-lt">
                                                    <label>MI</label>
                                                    <input class="form-control" type="text" id="mi" value="" name="middle_name" maxlength="1">
                                                </div>
                                                <div class="mi-name-rt">
                                                    <label>Last Name <em class="red-star">*</em></label>
                                                    <input class="form-control capsOn" type="text" id="last_name" name="last_name">
                                                    <span class="last_nameErr error red-star"></span>
                                                </div>


                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Maiden Name</label>
                                                <input class="form-control capsOn" type="text" id="maiden_name" name="maiden_name">
                                                <span class="maiden_nameErr error red-star"></span>
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Nick Name</label>
                                                <input class="form-control capsOn" type="text" id="nick_name" name="nick_name">
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Email <em class="red-star">*</em></label>
                                                <input class="form-control" type="email" id="email" name="email" >
                                                <span class="emailErr error red-star"></span>
                                            </div>
                                            <div class="col-sm-3 col-md-3" id="password_checker">
                                                <label>Password <em class="red-star">*</em></label>
                                                <input class="email-psw-input form-control"  type="password" id="password" name="password" maxlength="25">
                                                <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password" ></span>
                                                <span id="result"><div class="bar" style="display: none;"></div><div id="strength"></div></span>
                                                <span class="passwordErr error red-star"></span>
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Confirm Password <em class="red-star">*</em></label>
                                                <input class="form-control email-psw-input" type="password"  id="password_confirmation" name="password_confirmation" maxlength="25"  >
                                                <span toggle="#password_confirmation" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                                <span class="password_confirmationErr error red-star"></span>
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Company Name <em class="red-star">*</em></label>
                                                <input class="form-control capsOn" type="text" id="company_name" name="company_name">
                                                <img src="<?php echo SUPERADMIN_SITE_URL; ?>/images/tick-icon.png" alt="" id="imgCompanyAvailable" style="display: none;">
                                                <span class="company_required" style="display: none;float: left;color: red;">
                                                                        Company Already Exist
                                                                    </span>
                                                <span class="company_nameErr error red-star"></span>
                                            </div>
                                            <div class="col-sm-3 col-md-3 phone_num">
                                                <label>Phone <em class="red-star">*</em></label>
                                                <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control" fixedLength="10" id="phone_number" name="phone_number" value="">
                                                <span class="phone_numberErr error red-star"></span>
                                                <input type="hidden" value="" id="phone_number_prefix" name="phone_number_prefix">
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Zipcode <em class="red-star">*</em></label>
                                                <input type="text" class="form-control" id="zipcode" name="zipcode" value="" maxlength="9">
                                                <span class="zipcodeErr error red-star"></span>
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>City <em class="red-star">*</em></label>
                                                <input type="text" class="form-control capsOn" id="city" name="city" value="">
                                                <span class="cityErr error red-star"></span>
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>State <em class="red-star">*</em></label>
                                                <input type="text" class="form-control capsOn" id="state" name="state" value="">
                                                <span class="stateErr error red-star"></span>
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Country <em class="red-star">*</em></label>
                                                <input type="text" class="form-control capsOn" id="country" name="country" value="">
                                                <span class="countryErr error red-star"></span>
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Default Currency <em class="red-star">*</em></label>
                                                <select class="form-control" id="default_currency" name="default_currency">
                                                </select>
                                                <span class="default_currencyErr error red-star"></span>
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Subscription Plan <em class="red-star">*</em></label>
                                                <select class="form-control"  id="subscription_plan" name="subscription_plan">
                                                    <option value="">Select</option>
                                                </select>
                                                <span class="subscription_planErr error red-star"></span>
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Plan Term <em class="red-star">*</em></label>
                                                <select class="form-control" id="term_plan" name="term_plan">
                                                    <option value="">Select</option>
                                                    <option value="1" calc="1">Monthly</option>
<!--                                                    <option value="2" calc="3">Quarterly</option>-->
<!--                                                    <option value="2" calc="6">Semi-Annual</option>-->
                                                    <option value="4" calc="12">Annual</option>
                                                </select>
                                                <span class="term_planErr error red-star"></span>
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Plan Price</label>
                                                <input readonly class="form-control" type="text" id="plan_price" name="plan_price">
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Discount</label>
                                                <input class="form-control" type="number" min="0" id="discount" value="" name="discount">
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Pay Plan Price</label>
                                                <input disabled class="form-control" type="text" id="pay_plan_price" value="" name="pay_plan_price">
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Account Type <em class="red-star">*</em></label>
                                                <div class="check-outer">
                                                    <input type="radio" value="1" name="account_type" checked>
                                                    <label>Cash</label>
                                                </div>
                                                <div class="check-outer">
                                                    <input type="radio" value="0"  name="account_type">
                                                    <label>Accrual</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Domain Name <em class="red-star">*</em></label>
                                                <input class="form-control" type="text" id="domain_name" name="domain_name"  value="">
                                                <img src="<?php echo SUPERADMIN_SITE_URL; ?>/images/tick-icon.png" alt="" id="imgDomainAvailable" style="display: none;">
                                                <span class="domain_nameErr error red-star"></span>
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Fax</label>
                                                <input class="form-control fax" type="text" type="tel" maxlength="12" placeholder="000-000-0000" fixedLength="12"  id="fax" name="fax" value="">
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Address 1</label>
                                                <input type="text" class="form-control capsOn" id="address1" name="address1" value="">                                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Address 2</label>
                                                <input type="text" class="form-control capsOn" id="address2" name="address2" value="">
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Address 3</label>
                                                <input type="text" class="form-control capsOn" id="address3" name="address3" value="">
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Address 4</label>
                                                <input type="text" class="form-control" id="address4" name="address4" value="">
                                            </div>
                                            <div class="col-sm-3 col-md-3 expiration_date_check">
                                                <label>&nbsp;</label>
                                                <div class="check-outer">
                                                    <input type="checkbox" id="free_plan" name="free_plan" >
                                                    <label>Free Plan</label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-sm-3 col-md-3 freeplan" style="display: none;">
                                                <label>Number of days (Remaining)</label>
                                                <input type="text" readonly class="form-control" remaining_days="60" id="remaining_days" name="remaining_days" value="60">
                                            </div>
                                            <div class="col-sm-3 col-md-3 freeplan" style="display: none;">
                                                <label>Extended # of days</label>
                                                <input type="text" class="form-control" min="1" id="extended_days" pattern="\d*" maxlength="6" name="extended_days" value="">
                                            </div>
                                            <div class="col-sm-3 col-md-3 freeplan" style="display: none;">
                                                <label>Expiration</label>
                                                <span id="expiration_date_label"></span>
                                                <input type="hidden" class="form-control" id="expiration_date" name="expiration_date" value="" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End single add-form-box-->

                            <h2 class="left" style="font-size: 20px;text-decoration: underline;margin-bottom: 10px;">
<!--                                <div class="online-payment">-->
<!--                                    <a href="javascript:void(0)" id="online_payments">Online Payments</a>-->
<!--                                </div>-->
                                <!--single add-form-box-->
                                <div class="apx-adformbox">
                                    <div class="apx-adformbox-title">
                                        <strong class="left">EFT</strong>
                                    </div>
                                    <div class="apx-adformbox-content">
                                        <div class="row">
                                            <div class="form-outer">
                                                <div class="col-sm-3 col-md-3 ">
                                                    <label>Select Country</label>
                                                    <select id="country_code" class="form-control" name="eft_country">
                                                        <option value="" selected="selected">Select</option>
                                                        <option value="UnitedStates">UnitedStates</option>
                                                        <option value="Canada">Canada</option>
                                                    </select>
                                                </div>
                                                <div id="Canada" style="display: none">
                                                    <div class="col-sm-3">
                                                        <label for="originator_id_number">Originator ID Number<em class="red-star">*</em></label>
                                                        <input type="text" class="form-control" maxlength="10" id="originator_id_number" name="originator_id_number" value="">
                                                        <span class="originator_id_numberErr error red-star"></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label for="cpa_code">CPACode<em class="red-star">*</em></label>
                                                        <input type="text" class="form-control" id="cpa_code" name="cpa_code" value="" maxlength="3">
                                                        <span class="cpa_codeErr error red-star"></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label for="originator_name">Originator Name<em class="red-star">*</em></label>
                                                        <input type="text" class="form-control" id="originator_name" name="originator_name" value="">
                                                        <span class="originator_nameErr error red-star"></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label for="transit_number">Transit Number<em class="red-star">*</em></label>
                                                        <input type="text" class="form-control" id="transit_number" name="transit_number" value="" maxlength="9">
                                                        <span class="transit_numberErr error red-star"></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label for="account_number">Account Number<em class="red-star">*</em></label>
                                                        <input type="text" class="form-control hide_copy" id="account_number" name="account_number" value="" maxlength="12">
                                                        <span class="account_numberErr error red-star"></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label for="due_date">Due Date<em class="red-star">*</em></label>
                                                        <select id="due_date" class="form-control" name="due_date">
                                                            <option value="" selected="selected">Select</option>
                                                            <?php for ($i = 1; $i <= 28; $i++) { ?>
                                                                <option value="<?php echo $i ; ?>"><?php echo $i ; ?></option>
                                                            <?php  } ?>
                                                        </select>
                                                        <span class="due_dateErr error red-star"></span>
                                                    </div>
                                                </div>
                                                <div id="UnitedStates" style="display:none;">
                                                    <div class="col-sm-3">
                                                        <label for="routing_number">Routing Number<em class="red-star">*</em></label>
                                                        <input maxlength="9" type="text" class="form-control" id="routing_number" name="routing_number" value="">
                                                        <span class="routing_numberErr error red-star"></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label for="origin">Origin<em class="red-star">*</em></label>
                                                        <input maxlength="10" type="text" class="form-control" id="origin" name="origin" value="">
                                                        <span class="originErr error red-star"></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label for="trace_number">Trace Number<em class="red-star">*</em></label>
                                                        <input maxlength="15" type="text" class="form-control" id="trace_number" name="trace_number" value="">
                                                        <span class="trace_numberErr error red-star"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                    </div>
                    <!--End single add-form-box-->
                    <div class="btn-outer apex-btn-block text-right col-sm-12" style="margin-bottom: 20px;">
                        <input type="submit" class="blue-btn" value="Save">
                        <input type='button'  value="Clear" class="clear-btn clearForm" >
                        <input  type="button" value="Cancel" class="grey-btn" id="cancel_company"/>
                    </div>


                    </form>

                </div>
            </div>
        </section>
    </main>
</div>
<!-- Wrapper Ends -->

<div class="container">
    <div class="modal fade" id="financial-info" role="dialog">
        <div class="modal-dialog modal-md" style="width: 60%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>

                    <h4 class="modal-title">Online Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="apx-adformbox-content">
                        <form method="post" id="financialInfo">
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-4 col-md-4">
                                        <label>First Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="ffirst_name" name="ffirst_name">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Middle Name</label>
                                        <input class="form-control capsOn" type="text" id="fmiddle_name" name="fmiddle_name">
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Last Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="flast_name" name="flast_name">
                                        <span class="flast_nameErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-4 col-md-4">
                                        <label>Email <em class="red-star">*</em></label>
                                        <input class="form-control" type="email" id="femail" name="femail" >
                                        <span class="femailErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4 fphone_num">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control" fixedLength="10" id="fphone_number" name="fphone_number" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                        <input type="hidden" value="" id="phone_number_prefix" name="phone_number_prefix">
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Birth Date<em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fbirth_date" name="fbirth_date">
                                        <span class="fbirth_dateErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-4 col-md-4">
                                        <label>Postal Code <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fzipcode" name="fzipcode" value=""  maxlength="9">
                                        <span class="fzipcodeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>City <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fcity" name="fcity" value="" >
                                        <span class="fcityErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>State <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fstate" name="fstate" value="">
                                        <span class="fstateErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-4 col-md-4">
                                        <label>Address 1 <em class="red-star">*</em></label>
<!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control" id="faddress" name="faddress" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Address 2</label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control" id="faddress2" name="faddress2" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px;">
                                <div class="form-outer">
                                    <div class="col-sm-4 col-md-4">
                                        <label>Business Type<em class="red-star">*</em></label>
                                        <select class="form-control" id="fbusiness" name="fbusiness" readonly="">
                                            <option value="individual" selected>Individual</option>
                                        </select>
                                        <span class="fbusinessErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Social Security Number(Last 4 Digits) <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fssn" name="fssn" value="" maxlength="4">
                                        <span class="fssnErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-4 col-md-4">
                                        <label>Account Holder Name<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="faccount_holder" name="faccount_holder" value="" >
                                        <span class="faccount_holderErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Account Holder Type<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="faccount_holdertype" name="faccount_holdertype" value="individual" maxlength="9" readonly>
                                        <span class="faccount_holdertypeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Account Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control hide_copy" id="faccount_number" name="faccount_number" value="">
                                        <span class="faccount_numberErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-4 col-md-4">
                                        <label>ISO Country Code<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fiso" name="fiso" value="US" readonly>
                                        <span class="fisoErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Currency<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fcurrency" name="fcurrency" value="USD" readonly>
                                        <span class="fcurrencyErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Routing Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="frouting_number" name="frouting_number" value="">
                                        <span class="frouting_numberErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <button type="submit"  id="savefinancial" class="blue-btn">Submit</button>
                            <a href="javascript:void(0)" class="grey-btn" id="closePop">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
<!-- Jquery Starts -->
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/passwordscheck.js"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/intlTelInput.js"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/super_admin/company_users/admin-company.js"></script>
<script>
    $('.company-top').addClass('active');
</script>
</body>

</html>