<!DOCTYPE html>
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}
?>

<?php
include_once(SUPERADMIN_DIRECTORY_URL."/views/layouts/admin_header.php");
?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/css/intlTelInput.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <!--Content Start-->
    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer apxpg-top-search">
                        <div class="row">
                            <!--<div class="col-md-8 col-sm-8 col-xs-12">
                                <strong class="apxpg-title">Title goes here...</strong>
                            </div>-->
                            <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                            <div class="content-data apxpg-allcontent">
                                <div class="apx-adformbox">
                                    <div class="form-hdr apx-adformbox-title">
                                        <strong class="left">Account Type</strong>
                                        <a class="back right" href="/Companies/List"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>

                                    </div>
                                    <div class="form-data renew_plan_data-outer apx-adformbox-content">
                                            <form action="" method="POST" id="MakePayment">
                                                <div class="col-sm-12">
                                                    <div class="col-sm-3 col-md-3">
                                                        <div class="check-outer">
                                                            <input type="radio" value="1" class="payment_type" name="payment_type" checked>
                                                            <label>Cash</label>
                                                        </div>
                                                        <div class="check-outer">
                                                            <input type="radio" value="2" class="payment_type" name="payment_type">
                                                            <label>Cheque</label>
                                                        </div>
                                                        <div class="check-outer">
                                                            <input type="radio" value="3" class="payment_type" name="payment_type">
                                                            <label>Money Order</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="btn-outer apex-btn-block left col-sm-12" style="margin-bottom: 20px;">
                                                        <input type="hidden" value="<?php echo $_REQUEST['id'] ?>" class="company_id" name="company_id">
                                                        <input type="submit" class="blue-btn" value="Submit">
                                                        <input type="button" value="Cancel" style="padding: 8px 15px;" class="grey-btn" id="cancel_company">
                                                    </div>
                                                </div>
                                            </form>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </section>
    </main>
</div>
<!-- Wrapper Ends -->
<?php
include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
<!-- Jquery Starts -->
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script>
    $(document).ready(function(){
        //jqGrid status
        $('#MakePayment').on('submit',function(e){
            var data = $(this).serializeArray();
            e.preventDefault();
            $.ajax
            ({
                type: 'post',
                url: '/company-user-ajax',
                data: {
                    class: "CompanyUserAjax",
                    action: "paymentStatus",
                    data: data
                },
                success: function (response) {
                    var response = $.parseJSON(response);
                    if(response.code == 400)
                    {
                        $.each(response.data, function (key) {
                            $('.' + key).text('* This field is required');
                        });
                    }else{
                       toastr.success("Company Launched successfully.");
                      setTimeout(function () {
                          window.location.href = '/Companies/List';
                      },2000)
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('.' + key + 'Err').text(value);
                    });
                }
            });

        });
    });

    $(document).ready(function () {
        $(document).on("click", "#cancel_company", function (e) {
            bootbox.confirm("Do you want to cancel this action now?", function (result) {
                if (result == true) {
                    window.location.href='/Companies/List';
                }
            });
        });
    });


</script>
</body>

</html>