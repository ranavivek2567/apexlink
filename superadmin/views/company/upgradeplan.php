<!DOCTYPE html>
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}
$edit_id = (isset($_REQUEST['id']))?$_REQUEST['id']:'';
?>


<?php include_once(SUPERADMIN_DIRECTORY_URL."/views/layouts/admin_header.php"); ?>

<div id="wrapper">
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php");
    ?>
    <!-- MAIN Navigation Ends -->

    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row flex">

                    <div class="col-sm-8 col-md-10 main-content-rt">
                        <div class="upgrade_plan_data_div" >
                            <form id="form_upgrade_plan_data" method="post">
                                <div class="content-data apxpg-allcontent">
                                    <div class="apx-adformbox">
                                        <div class="form-hdr apx-adformbox-title">
                                            <h3>Subscription Summary</h3>
                                        </div>
                                        <div class="form-data renew_plan_data-outer apx-adformbox-content" style="margin-bottom: 10px;">
                                            <div class="col-sm-12">
                                                <div class="col-sm-1">
                                                    <a href="javascript:void(0)"  id="disable_remove">
                                                        <img src="<?php echo SUPERADMIN_SITE_URL; ?>/images/home.png ?>" alt="">
                                                    </a>
                                                </div>
                                                <div class="col-sm-8 renew_plan_data">
                                                    <h5  id="lblSubscriptionType"><b>MONTH TO MONTH</b></h5>
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <p>Subscription Start Date  : <span class="lblStartDate"></span></p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p>Subscription Expire Date : <span class="lblExpireDate"></span></p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p>Subscription Fee : <span class="spnPlanDollar"></span></p>
                                                            </td>
                                                        </tr>

                                                        </tbody></table>

                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="greyhdr-box">
                                                    <div class="greyhdr-box-top">
                                                        <h2>
                                                            Total Balance Due Today
                                                        </h2>
                                                        <a href="javascript:;"></a>
                                                    </div>
                                                    <div class="greyhdr-box-bot">
                                                        <label>
                                                            Total Unpaid
                                                        </label>
                                                        <span class="planTermAmmout"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="apx-adformbox form-outer">
                                        <div class="form-hdr apx-adformbox-title">
                                            <h3>
                                                <strong class="left">
                                                    Upgrade Plan Information
                                                </strong>
                                            </h3>
                                        </div>

                                        <div class="form-data apx-adformbox-content">
                                            <div class="row">
                                                <div class="form-outer">
                                                    <div class="col-sm-12 col-md-12">
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>New Plan  <em class="red-star">*</em></label>
                                                            <span>
                                                            <select name="subscription_plan" class="form-control UpgradeNewPlanName" id="NewPlanName" style="cursor: pointer;">

                                                            </select>
                                                        </span>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>No.Of Unit</label>
                                                            <span id="unitInput">
                                                            <input type="text" name="no_of_units"  style="cursor: pointer;" spellcheck="true" class="form-control UpgradeNewPlanUnits" readonly>
                                                        </span>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>
                                                                Payment Type  <em class="red-star">*</em>
                                                            </label>
                                                            <span>
                                                            <input type="hidden" value="" id="UsersdiscountPrice">
                                                            <select name="term_plan"  style="cursor: pointer;" class="form-control UpgradeNewTermPlan">
                                                                <option value="1">Monthly</option>
                                                                <option value="4">Yearly</option>
                                                            </select>
                                                        </span>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Plan Price  <em class="red-star">*</em></label>
                                                            <span>
                                                                 <input type="text" name="plan_price" id="NoOfUnit" style="cursor: pointer;" spellcheck="true" class="form-control UpgradeNewPlanPrice" readonly>

                                                        </span>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12 col-md-12">
                                                    <input type="hidden" value="" id="pay_pan_price_id">
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>
                                                                Pay Plan Price
                                                            </label>
                                                            <span>
                                                            <input type="text" name="pay_plan_price" id="lblTotalammount" style="cursor: pointer;" spellcheck="true"  class="form-control UpgradeNewPayPlanPrice" readonly>
                                                        </span>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3" id="spnCommision" style="display:none;margin:15px">
                                                            <span><label style="font-size: 15px;" id="spnStripeCharge"> Stripe Charges( <span id="stripeAccountFee"></span> + <span id="stripeTransactionFee"></span>) : <span>$</span><span id="totalcompanyfeeAdd"></span></span></label><label style="font-size: 15px;" id="spnTotalStripe"> Total : <span>$</span><span id="totalcompanyfee"></span></label></span>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-outer apx-adformbox">
                                        <div class="form-hdr apx-adformbox-title">
                                            <h3>
                                                <strong class="left">
                                                    Payment Method
                                                </strong>
                                            </h3>
                                        </div>
                                        <div class="form-data apx-adformbox-content">
                                            <div class="check-outer amenitieschck-radio">
                                                <input  id="credit_card" type="radio" name="card" value="card">
                                                <label>Credit Card</label>
                                            </div>
                                            <div class="check-outer amenitieschck-radio">
                                                <input  id="ach" type="radio" name="card" value="ach">
                                                <label>ACH</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="btn-outer text-right">
                                        <a href="javascript:;">
                                            <input type="submit" class="blue-btn SavePlansUpgrade" value="Save">
                                        </a>
                                        <button type="button" class="clear-btn ClearUpgradePlan">Clear</button>
                                        <a href="javascript:;">
                                            <input type="button" class="grey-btn cancel_upgradePlan" value="Cancel">
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </main>


</div>
<!-- Wrapper Ends -->
<?php
include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
<!-- Jquery Starts -->
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/passwordscheck.js"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/intlTelInput.js"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/super_admin/company_users/admin-company.js"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/payment/existingPlan.js"></script>
<script>
    $('.company-top').addClass('active');

    $(document).on('click','.cancel_upgradePlan',function(){
        bootbox.confirm({
            message: "Do you want to cancel this action ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    window.location.href ='/Companies/List';
                }
            }
        });
    })
    $(document).on('click','.ClearUpgradePlan',function(){
        bootbox.confirm("Do you want to clear this form?", function (result) {
            if (result == true) {
                    location.reload();
            }
        });
    });

</script>
</body>

</html>