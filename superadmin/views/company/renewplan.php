<!DOCTYPE html>
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}
$edit_id = (isset($_REQUEST['id']))?$_REQUEST['id']:'';
?>


<?php include_once(SUPERADMIN_DIRECTORY_URL."/views/layouts/admin_header.php"); ?>

<div id="wrapper">
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php");
    ?>
    <!-- MAIN Navigation Ends -->

    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer apxpg-top-search">
                        <div class="row">
                            <!--<div class="col-md-8 col-sm-8 col-xs-12">
                                <strong class="apxpg-title">Title goes here...</strong>
                            </div>-->
                            <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                            <div class="content-data apxpg-allcontent">
                                <div class="apx-adformbox">
                                    <div class="form-hdr apx-adformbox-title">
                                        <h3>Subscription Summary</h3>
                                    </div>
                                    <div class="form-data renew_plan_data-outer apx-adformbox-content" style="margin-bottom: 10px;">
                                        <div class="col-sm-12">
                                            <div class="col-sm-1">
                                                <a href="javascript:void(0)"  id="disable_remove">
                                                    <img src="<?php echo SUPERADMIN_SITE_URL; ?>/images/home.png ?>" alt="">
                                                </a>
                                            </div>
                                            <div class="col-sm-8 renew_plan_data">
                                                <h5  id="lblSubscriptionType"><b>MONTH TO MONTH</b></h5>
                                                <table>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <p>Propertyware start date : <span id="lblStartDate">3/30/2018 12:00:00 AM</span></p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p>Propertyware Expire date : <span id="lblExpireDate">3/30/2018 12:00:00 AM</span></p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p>Subscription Fee:<span id="spnPlanDollar">3/30/2018 12:00:00 AM</span></p>
                                                        </td>
                                                    </tr>

                                                    </tbody></table>

                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="greyhdr-box">
                                                <div class="greyhdr-box-top">
                                                    <h2>
                                                        Total Balance Due Today
                                                    </h2>
                                                    <a href="javascript:;"></a>
                                                </div>
                                                <div class="greyhdr-box-bot">
                                                    <label>
                                                        Total Unpaid
                                                    </label>
                                                    <span id="planTermAmmout">100</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="apx-adformbox" style="display: flow-root;">
                                    <div class="apx-adformbox-title">
                                        <strong class="left">
                                            Renew Plan Information
                                        </strong>
                                    </div>
                                    <div class="apx-adformbox-content">
                                        <div class="row">
                                            <div class="form-outer">
                                                <div class="col-sm-12 col-md-12">
                                                    <div class="col-sm-3 col-md-3">
                                                        <label>New Plan</label>
                                                        <span>
                                                            <select name="" id="NewPlanName" style="cursor: pointer;" disabled="disabled" class="form-control">
                                                                <option value="">Select</option>
                                                                <option value="1" units="1-25" noofunitid="1" pricepunit="1" price="75">1-25 Plan</option>
                                                                <option value="2" units="1-50" noofunitid="2" pricepunit="1" price="75">26-50 Plan</option>
                                                                <option value="3" units="1-100" noofunitid="3" pricepunit="1" price="75">51-100 Plan</option>
                                                                <option value="4" units="1-200" noofunitid="4" pricepunit="1" price="75">101-200 Plan</option>
                                                                <option value="5" units="1-300" noofunitid="5" pricepunit="1" price="75">201-300 Plan</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3 ">
                                                        <label>No.Of Unit</label>
                                                        <span>
                                                            <input type="text" name="" id="NoOfUnit" style="cursor: pointer;" spellcheck="true" disabled="disabled" class="form-control">
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3">
                                                        <label>
                                                            Payment Type
                                                        </label>
                                                        <span>
                                                            <select name="" id="Paymenttype" style="cursor: pointer;" disabled="disabled" class="form-control">undefined
                                                                <option value="1">Monthly</option>
                                                                <option value="2">Quarterly</option>
                                                                <option value="3">Half-Yearly</option><option value="4">Yearly</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3">
                                                        <label>Plan Price</label>
                                                        <span>
                                                            <select name="" id="Paymenttype" style="cursor: pointer;" disabled="disabled" class="form-control">undefined
                                                                <option value="1">Monthly</option>
                                                                <option value="2">Quarterly</option>
                                                                <option value="3">Half-Yearly</option><option value="4">Yearly</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-md-12">
                                                    <div class="col-sm-3 col-md-3">
                                                        <label>
                                                            Discount
                                                        </label>
                                                        <span>
                                                            <input type="text" name="" id="txtPlanDiscount" style="cursor: pointer;" spellcheck="true" class="form-control">
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3 ">
                                                        <label>
                                                            Pay Plan Price
                                                        </label>
                                                        <span>
                                                            <input type="text" name="" id="lblTotalammount" style="cursor: pointer;" spellcheck="true" disabled="disabled" class="form-control">
                                                        </span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="apx-adformbox" style="display: flow-root;">
                                    <div class="apx-adformbox-title">
                                        <strong class="left">
                                            Renew Plan Information
                                        </strong>
                                    </div>
                                    <div class="apx-adformbox-content">
                                        <div class="row">
                                            <div class="form-outer">
                                                <div class="col-sm-3 col-md-3 ">
                                                    <label>Payment Method</label>
                                                    <select id="country_code" class="form-control" name="">
                                                        <option value="">Select</option>
                                                        <option value="2">Credit/Debit Card</option>
                                                        <option value="1">ACH</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </section>
    </main>


</div>
<!-- Wrapper Ends -->
<?php
include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
<!-- Jquery Starts -->
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/passwordscheck.js"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/intlTelInput.js"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/super_admin/company_users/admin-company.js"></script>
<script>
    $('.company-top').addClass('active');
</script>
</body>

</html>