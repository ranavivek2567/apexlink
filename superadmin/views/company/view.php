﻿<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/constants.php");
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}

$view_id = (isset($_REQUEST['id']))?$_REQUEST['id']:'';
?>
<!DOCTYPE html>


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/admin_header.php");
?>
<!-- HTML Start -->



<div id="wrapper">
    <main class="apxpg-main">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php");
    ?>
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer apxpg-top-search">
                    <div class="row">
<!--                        <div class="col-md-8 col-sm-8 col-xs-12">-->
<!--                            <strong class="apxpg-title">  Company>> <span>View Company</span></strong>-->
<!--                        </div>-->
                        <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data apxpg-allcontent">
                    <div class="content-section">
                        <!--single add-form-box-->
                        <div class="apx-adformbox">
                            <div class="apx-adformbox-title">
                                <strong class="left">Company Details</strong>
                                <a onclick="goBack()" class="back right" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                            </div>
                            <div class="apx-adformbox-content">
                                <input type="hidden" id="view_id" name="id" value="<?php echo $view_id; ?>">
                                <input type="hidden" id="page" name="page" value="view">
                                <div class="row">
                                    <div class="view-outer">
                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">First Name  :</label>
                                            <span class="first_name"></span>
                                        </div>

                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Maiden Name :</label>
                                            <span class="maiden_name"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Last Name :</label>
                                            <span class="last_name"></span>
                                        </div>

                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Nick Name :</label>
                                            <span class="nick_name"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Middle Name :</label>
                                            <span class="middle_name"></span>
                                        </div>

                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Zip / Postal Code :</label>
                                            <span class="zipcode"></span>
                                        </div>

                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Email :</label>
                                            <span class="email"></span>
                                        </div>

                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">City :</label>
                                            <span class="city"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Company Name :</label>
                                            <span class="company_name"></span>
                                        </div>

                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">State / Province :</label>
                                            <span class="state"></span>
                                        </div>

                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Phone :</label>
                                            <span class="phone_number"></span>
                                        </div>

                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Country :</label>
                                            <span class="country"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Fax :</label>
                                            <span class="fax"></span>
                                        </div>

                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Subscription Plan :</label>
                                            <span class="subscription_plan"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Domain Name :</label>
                                            <span class="domain_name"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Address1 :</label>
                                            <span class="address1"></span>
                                        </div>

                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Address2 :</label>
                                            <span class="address2"></span>
                                        </div>

                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Address3 :</label>
                                            <span class="address3"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Address4 :</label>
                                            <span class="address4"></span>
                                        </div>

                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Plan Type :</label>
                                            <span class="plan_type"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Extended # of days :</label>
                                            <span class="extended_days"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Remaining # of days :</label>
                                            <span class="remaining_days"></span>
                                        </div>

                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Expiry Date :</label>
                                            <span class="expiration_date_label"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End single add-form-box-->

                        <!--single add-form-box-->
                        <div class="apx-adformbox">
                            <div class="apx-adformbox-title">
                                <strong class="left">Payment Method</strong>
                            </div>
                            <div class="apx-adformbox-content">
                                <input type="hidden" id="view_id" name="id" value="<?php echo $view_id; ?>">
                                <input type="hidden" id="page" name="page" value="view">
                                <div class="row">
                                    <div class="view-outer">
                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Payment Method :</label>
                                            <span calss="payment_method"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Expires On :</label>
                                            <span class="expires_on"></span>
                                        </div>
                                       <!-- <div class="col-xs-12 col-sm-6">
                                            <label class="blue-label">Credit Card Number :</label>
                                            <span class="credit_card_number"></span>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End single add-form-box-->
                        </div>
                    </div>
                    <!--tab Ends -->
                </div>
            </div>
        </div>
</div>
</section>
</main>
</div>
<!-- Wrapper Ends -->

<!-- Footer Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->
<!-- Footer Ends -->
<!-- Jquery Starts -->

<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.responsivetabs.js"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/super_admin/company_users/admin-company.js"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/passwordscheck.js"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/intlTelInput.js"></script>

<script>
    $(function () {
        $('.nav-tabs').responsiveTabs();
    });

    $("#show").click(function () {
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function () {
        $("#bs-example-navbar-collapse-2").hide();
    });

    $('.company-top').addClass('active');

    $(document).ready(function () {
        $(".slide-toggle").click(function () {
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function () {
        $(".slide-toggle2").click(function () {
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });

</script>





<!-- Jquery Starts -->

</body>

</html>