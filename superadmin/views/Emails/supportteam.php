<table id="background-table" style="background-color:#ececec" width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr style="border-collapse:collapse">
			<td style="font-family:'Helvetica Neue',Arial,Helvetica,Geneva,sans-serif; border-collapse:collapse" bgcolor="#ececec" align="center">
				<table class="w640" style="margin-top:0; margin-bottom:0; margin-right:10px; margin-left:10px" width="640" cellspacing="0" cellpadding="0" border="0">
					<tbody>
						<tr style="background-color:#00b0f0; height:20px"><td></td></tr>
						<tr style="border-collapse:collapse; background-color:#fff">
							<td class="w580" style="font-family:'Helvetica Neue',Arial,Helvetica,Geneva,sans-serif; border-collapse:collapse" width="580">
								<strong>
									<a href="UrlBlockedError.aspx" style="color:#ffffff; text-decoration:none" target="_blank">
								<center>
									<div style="margin-top:1px; margin-bottom:10px"> </div>
								</center>
								</a>
								</strong>
							</td>
						</tr>
						<tr style="background-color:#00b0f0; height:20px"><td></td></tr>
						<tr id="simple-content-row" style="border-collapse:collapse">
							<td class="w640" style="font-family:'Helvetica Neue',Arial,Helvetica,Geneva,sans-serif; border-collapse:collapse" width="640" bgcolor="#ffffff">
								<table class="w640" width="640" cellspacing="0" cellpadding="0" border="0" align="left">
									<tbody>
									<tr style="border-collapse:collapse">
										<td class="w30" style="font-family:'Helvetica Neue',Arial,Helvetica,Geneva,sans-serif; border-collapse:collapse" width="30">
										</td>
										<td class="w580" style="font-family:'Helvetica Neue',Arial,Helvetica,Geneva,sans-serif; border-collapse:collapse" width="580">
											<table class="w580 testinner" width="580" cellspacing="0" cellpadding="0" border="0">
												<tbody>
													<tr style="border-collapse:collapse">
														<td class="w580" style="font-family:'Helvetica Neue',Arial,Helvetica,Geneva,sans-serif; border-collapse:collapse; padding-right: 20px; padding-left: 20px;" width="580">
															<p class="article-title" style="font-weight:normal; font-size:24px; line-height:24px; color:#00b0f0; margin-top:10px; margin-bottom:18px; font-family:'Helvetica Neue',Arial,Helvetica,Geneva,sans-serif" align="center">
															Welcome to Apexlink</p>
															<div class="article-content" style="font-size:13px; line-height:18px; color:#444444; margin-top:0px; margin-bottom:14px; font-family:'Helvetica Neue',Arial,Helvetica,Geneva,sans-serif" align="left">
															<p style="margin-bottom:15px">Dear :: </p>
															<p style="margin-bottom:15px">Welcome to the ApexLink Support Team. We are pleased that you are joining us in providing high quality property management software, services and support to our customers.
															</p>
															<p style="margin-bottom:15px">We have promised our customers the most responsive standards of service. We count on you, as a Support Team member, to meet or exceed customer expectations.
															</p>
															<p style="margin-bottom:15px">When a customer requests support or information, you will receive a call or an email from either
															<a href="UrlBlockedError.aspx" style="color:#3a00ff; text-decoration:underline" target="_blank">
															support@ApexLink.com</a> or <a href="UrlBlockedError.aspx" style="color:#3a00ff; text-decoration:underline" target="_blank">
															info@ApexLink.com</a> . Your responsibility is to answer those requests immediately as they come in â€“ without delay. We promise 24/7 support and tell our clients that â€œweâ€™ll be there when you call.â€? Your primary responsibility is to â€œbe thereâ€? to meet their
															 needs. </p>
															<p style="margin-bottom:15px">If for some reason, you cannot assist the customer inquiry, immediately forward the request to your supervisor with the reason you cannot respond. That way we can quickly transfer it to another available Support Team member and
															 meet our service promise to the customer. </p>
															<p style="margin-bottom:15px">We looking forward to working with you. Thank you for your commitment to ApexLink.
															</p>
															<p style="margin-bottom:15px">The ApexLink Management Team </p>
															</div>
														</td>
													</tr>
													<tr style="border-collapse:collapse">
														<td class="w580" style="font-family:'Helvetica Neue',Arial,Helvetica,Geneva,sans-serif; border-collapse:collapse" width="580" height="10">
															<table id="footer" class="w640" style="background-color:#00b0f0; color:#ffffff; height:20px" width="640" cellspacing="0" cellpadding="0" border="0" bgcolor="#00b0f0">
																<tbody>
																	<tr style="border-collapse:collapse">
																		<td class="w640" style="font-family:'Helvetica Neue',Arial,Helvetica,Geneva,sans-serif; border-collapse:collapse" width="640">
																			<table id="Table1" style="background-color:#585858; color:#ffffff; margin:auto; width:640px" width="640" cellspacing="0" cellpadding="0" border="0" bgcolor="#0e867b" align="center">
																				<tbody>
																				<tr>
																					<td style="padding:10px; font-size:12px; color:#ffffff" bgcolor="#05a0e4" align="center">
																					ApexLink Property Manager ● <a href="UrlBlockedError.aspx" style="color:#fff; text-decoration:none" target="_blank">
																					support@apexlink.com ● 772-212-1950</a> </td>
																				</tr>
																				</tbody>
																			</table>
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
										<td class="w30" style="font-family:'Helvetica Neue',Arial,Helvetica,Geneva,sans-serif; border-collapse:collapse" width="30">
										</td>
									</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr style="border-collapse:collapse">
							<td class="w640" style="font-family:'Helvetica Neue',Arial,Helvetica,Geneva,sans-serif; border-collapse:collapse" width="640"> &nbsp; </td>
						</tr>
						<tr style="border-collapse:collapse">
							<td class="w640" style="font-family:'Helvetica Neue',Arial,Helvetica,Geneva,sans-serif; border-collapse:collapse" width="640" height="60"></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>