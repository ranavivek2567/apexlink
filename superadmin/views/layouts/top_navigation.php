<header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-2 visible-xs">
 
            </div>
            <div class="col-sm-3 col-xs-8">
                <div class="logo"><img src="<?php echo SUPERADMIN_SITE_URL;?>/images/logo.png"></div>
            </div>
            <div class="col-sm-9 col-xs-2">
                <div class="hdr-rt">
                    <!-- TOP Navigation Starts -->

                    <nav class="navbar navbar-default">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li class="hidden-xs">Welcome:  <?php echo $_SESSION[SESSION_DOMAIN]['name'];?>, <span id="formatted_date"><?php echo $_SESSION[SESSION_DOMAIN]['formated_date'];?></span> <a herf="javascript:;"> <a href="javascript:;" data-toggle="modal" data-target="#myModalCalc" data-backdrop="false" data-keyboard="false"><i class="fa fa-calculator" aria-hidden="true"></i></a></li>
                                <li class="dropdown">
                                    <a href="/Setting/Settings" class="nav-link disabled"><i class="fa fa-cog" aria-hidden="true"></i>Admin</a>
                                </li>
                                <!-- <li class="nav-item">
                                    <a class="nav-link disabled" href="#"><i class="fa fa-question" aria-hidden="true"></i>Help</a>
                                    <ul id="help-submenu" style="display:none;">
                                            <li><a target="_blank" href="https://apexlink.com/kb.php?h=TRUE" class="clsSuggestion help_dp" ><span class="helpHeader"></span>Help Center</a></li>
                                            <li><a href="javascript:void(0)" class="clsSuggestion help_dp" data-toggle="modal" id="apexklink_newT"><span class="Suggestion help_dp"></span>Apexlink New</a></li>
                                            <li><a href="javascript:void(0)" class="clsSuggestion help_dp" data-toggle="modal" data-target="#helpThird"><span class="Suggestion"></span>Apexlink Connect</a></li>
                                        </ul>
                                </li> -->
                                <li class="nav-item help_class dropdown help_class_dropdown">
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" id="help_id" href="#"><i class="fa fa-question" aria-hidden="true"></i>Help<span class="arrow"></span></a>
                                        <ul id="help-submenus" class="dropdown-menu">
                                            
                                            <li><a href="javascript:void(0)" class="clsSuggestion help_dp"  data-toggle="modal" data-target="helpThirds"><span class="Suggestion"></span>Apexlink Connect</a></li>
                                        </ul>
                                    </li>
                                <li class="nav-item">
                                    <form name="logout" method="post" action="">
                                        <a class="nav-link disabled" role="button" type="submit" name="logout" id='logout' value="logout" ><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
                                    </form>
                                </li>
                            </ul>

                        </div><!-- /.navbar-collapse -->
                    </nav>
                    <!-- TOP Navigation Ends -->

                </div>
            </div>
        </div>
    </div>

</header>
<!-- MAIN Navigation Starts -->
<section class="main-nav">
    <nav class="navbar navbar-default apx-topnav">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header visible-xs">
            <a id="show" class="icon-bar" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <a class="nav-close visible-xs" id="close" href="#"><i class="fa fa-times" aria-hidden="true"></i> </a>
            <ul class="nav navbar-nav">
                <li class="spotlight-top"><a href="/Dashboard/Dashboard">Spotlight</a></li>
                <li class="company-top"><a href="/Companies/List">Subscribed Companies/Users</a></li>
                <li class="plan-top"><a href="/Plans/List">Plans</a></li>
                <li class="payment-top"><a href="/Payment/List">Payments</a></li>
                <li class="announcement-top"><a href="/Announcement/Announcements">Announcements</a></li>
                <li class="support-team-top"><a href="/Support/Index">Manage Support Team</a></li>
                <li class="communication-top"><a href="/Communication/SentEmails">Communication</a></li>
                <li class="audit-trial-top"><a href="/Setting/IPAddress">Audit Trails</a></li>
                <li class="reports-top"><a href="/Reporting/Reporting">Reports</a></li>
            </ul>

        </div><!-- /.navbar-collapse -->
    </nav>
    <script src="<?php echo SUPERADMIN_SITE_URL;?>/js/validation/users/users.js"></script>
</section>
<!-- MAIN Navigation Ends -->
