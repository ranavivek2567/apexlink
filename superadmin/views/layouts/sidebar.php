<!-- Sidebar Navigation -->
<div class="col-sm-4 col-md-2 main-content-lt">
    <div class="left-links slide-toggle"><i class="fa fa-caret-right" aria-hidden="true"></i></div>
    <div id="LeftMenu" class="box">
        <div class="list-group panel">
            <!-- One  Starts-->
            <a href="#leftnav2" class="default-sidebar list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu"><i class="fa fa-building-o" aria-hidden="true"></i>Company Setup</a>
            <div class="collapse submenu-outer" id="leftnav2">
                <a href="/Setting/Settings" class="list-group-item sub-item default_settings">Default Settings</a>
                <a href="/Setting/ManageUser" class="list-group-item sub-item manage_user">Manage User</a>
            </div>
            <!-- One Ends-->

            <!-- Two Ends-->
            <a href="#leftnav3" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu"><i class="fa fa-home" aria-hidden="true"></i>Apexlink New</a>
            <div class="collapse submenu-outer" id="leftnav3">
                <a href="/Setting/ApexLinkNew" class="list-group-item sub-item apexlink_new">What's New</a>
            </div>
            <!-- Two Ends-->
        </div>
    </div>
</div>
<!-- MAIN Navigation Ends -->
