    <?php
/**
 * Created by PhpStorm.
 * User: DeviSarita
 * Date: 1/24/2019
 * Time: 5:02 PM
 */

if(isset($_SESSION[SESSION_DOMAIN]['user_id'])){
    if(isset($_POST['logout'])){
        unset($_SESSION[SESSION_DOMAIN]['user_id']);
        //   $url = BASE_URL."login";
        header('Location: '.BASE_URL);
    }
}else{
    header('Location: '.BASE_URL);
}
?>


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->



    <section class="main-content">
        <div class="container-fluid">
            <div class="row bread-search-outer">
                <div class="col-sm-8"></div>
                <div class="col-sm-4">
                    <div class="easy-search">
                        <input placeholder="Easy Search" type="text"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Tab panes -->

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="regular-rent">
                                <div class="property-status">

                                    </div>
                                </div>
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> Super Admin Reports</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark">
                                                                        <thead>

                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>

                                                                           <!-- general ledger starts-->
                                                                            <td><a  data-toggle="modal" data-target="#myModal"  href="javascript:;">General Ledger</a></td>


                                                                            <div class="modal fade" id="myModal" role="dialog">
                                                                                <div class="modal-dialog modal-sm">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                            <h4 class="modal-title">General Ledger Filter</h4>
                                                                                        </div>
                                                                                        <div class="modal-body">
                                                                                            <label>Company*</label>
                                                                                            <select class="form-control" id="jqGridStatus">

                                                                                                <option value="All">All Selected</option>
                                                                                                <option value="1">uat1</option>


                                                                                            </select>
                                                                                            <label>Start Date</label>
                                                                                            <input type="date" name="bday"  class="form-control" id="jqGridStatus">

                                                                                            <label>End Date</label>
                                                                                            <input type="date" name="bday"  class="form-control" id="jqGridStatus">


                                                                                        </div>
                                                                                        <div class="modal-footer">
                                                                                            <button type="button" class="blue-btn" data-dismiss="modal">Apply Fiters</button>
                                                                                            <button type="button" class="grey-btn cancel_action" data-dismiss="modal">Cancel</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                </div>

                                                                <!-- Collection Report starts-->
                                                                            <td><a  data-toggle="modal" data-target="#myModal1" href="javascript :;">Collection Report</a></td>
                                                                <div class="modal fade" id="myModal1" role="dialog">
                                                                    <div class="modal-dialog modal-sm">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                <h4 class="modal-title">Collection Report</h4>
                                                                            </div>
                                                                            <div class="modal-body">

                                                                                <label>Start Date</label>
                                                                                <input type="date" name="bday"  class="form-control" id="jqGridStatus">

                                                                                <label>End Date</label>
                                                                                <input type="date" name="bday"  class="form-control" id="jqGridStatus">

                                                                                <label>Company*</label>
                                                                                <select class="form-control" id="jqGridStatus">

                                                                                    <option value="All">All Selected</option>
                                                                                    <option value="1">uat1</option>


                                                                                </select>


                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="blue-btn" data-dismiss="modal">Apply Fiters</button>
                                                                                <button type="button" class="grey-btn cancel_action" data-dismiss="modal">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!-- AR Aging Summary starts-->
                                                                            <td><a  data-toggle="modal" data-target="#myModal2" href="javascript :;">AR Aging Summary </a></td>
                                                            <div class="modal fade" id="myModal2" role="dialog">
                                                                <div class="modal-dialog modal-sm">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                            <h4 class="modal-title">AR Aging Summary</h4>
                                                                        </div>
                                                                        <div class="modal-body">

                                                                            <label> Date</label>
                                                                            <input type="date" name="bday"  class="form-control" id="jqGridStatus">

                                                                            <label>Company*</label>
                                                                            <select class="form-control" id="jqGridStatus">

                                                                                <option value="All">All Selected</option>
                                                                                <option value="1">uat1</option>


                                                                            </select>


                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="blue-btn" data-dismiss="modal">Apply Fiters</button>
                                                                            <button type="button" class="grey-btn cancel_action" data-dismiss="modal">Cancel</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                                        </tr>
                                                                        <tr>

                                                                            <!-- Subscription Summary starts-->
                                                                            <td><a data-toggle="modal" data-target="#myModal3" href="javascript:;">Subscription Summary Report</a></td>
                                                                            <div class="modal fade" id="myModal3" role="dialog">
                                                                                <div class="modal-dialog modal-sm">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                            <h4 class="modal-title">Subscription Summary Report</h4>
                                                                                        </div>
                                                                                        <div class="modal-body">

                                                                                            <label>Start Date</label>
                                                                                            <input type="date" name="bday"  class="form-control" id="jqGridStatus">

                                                                                            <label>End Date</label>
                                                                                            <input type="date" name="bday"  class="form-control" id="jqGridStatus">

                                                                                            <label>Company*</label>
                                                                                            <select class="form-control" id="jqGridStatus">

                                                                                                <option value="All">All Selected</option>
                                                                                                <option value="1">uat1</option>


                                                                                            </select>


                                                                                        </div>
                                                                                        <div class="modal-footer">
                                                                                            <button type="button" class="blue-btn" data-dismiss="modal">Apply Fiters</button>
                                                                                            <button type="button" class="grey-btn cancel_action" data-dismiss="modal">Cancel</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                    </div>
                                                    <!-- Subscription List By Company Size starts-->
                                                                            <td><a  data-toggle="modal" data-target="#myModal4" href="javascript :;">Subscription List By Company Size</a></td>
                                                    <div class="modal fade" id="myModal4" role="dialog">
                                                        <div class="modal-dialog modal-sm">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    <h4 class="modal-title">Subscription List By Company Size </h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <label>Company*</label>
                                                                    <select class="form-control" id="jqGridStatus">

                                                                        <option value="All">All Selected</option>
                                                                        <option value="1">uat1</option>


                                                                    </select>

                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="blue-btn" data-dismiss="modal">Apply Fiters</button>
                                                                    <button type="button" class="grey-btn cancel_action" data-dismiss="modal">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Income Register starts-->
                                                                            <td><a data-toggle="modal" data-target="#myModal5" href="javascript :;">Income Register </a></td>
                                                <div class="modal fade" id="myModal5" role="dialog">
                                                    <div class="modal-dialog modal-sm">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Income Register</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <label>Company*</label>
                                                                <select class="form-control" id="jqGridStatus">

                                                                    <option value="All">All Selected</option>
                                                                    <option value="1">uat1</option>


                                                                </select>


                                                                <label>Date</label>
                                                                <input type="date" name="bday"  class="form-control" id="jqGridStatus">


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="blue-btn" data-dismiss="modal">Apply Fiters</button>
                                                                <button type="button" class="grey-btn cancel_action" data-dismiss="modal">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                                                        </tr>
                                                                        <tr>

                                                                            <!-- Subscription Cancellation starts-->
                                                                            <td><a data-toggle="modal" data-target="#myModal6" href="javascript:;">Subscription Cancellation Report</a></td>
                                                                            <div class="modal fade" id="myModal6" role="dialog">
                                                                                <div class="modal-dialog modal-sm">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                            <h4 class="modal-title">Subscription Cancellation Report</h4>
                                                                                        </div>
                                                                                        <div class="modal-body">

                                                                                            <label>Start Date</label>
                                                                                            <input type="date" name="bday"  class="form-control" id="jqGridStatus">

                                                                                            <label>End Date</label>
                                                                                            <input type="date" name="bday"  class="form-control" id="jqGridStatus">

                                                                                            <label>Company*</label>
                                                                                            <select class="form-control" id="jqGridStatus">

                                                                                                <option value="All">All Selected</option>
                                                                                                <option value="1">uat1</option>


                                                                                            </select>


                                                                                        </div>
                                                                                        <div class="modal-footer">
                                                                                            <button type="button" class="blue-btn" data-dismiss="modal">Apply Fiters</button>
                                                                                            <button type="button" class="grey-btn cancel_action" data-dismiss="modal">Cancel</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                        </div>

                                        <!-- Customer Statement starts-->
                                                                            <td><a data-toggle="modal" data-target="#myModal7" href="javascript :;">Customer Statement</a></td>
                                        <div class="modal fade" id="myModal7" role="dialog">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Customer Statement</h4>
                                                    </div>
                                                    <div class="modal-body">

                                                        <label>Start Date</label>
                                                        <input type="date" name="bday"  class="form-control" id="jqGridStatus">

                                                        <label>End Date</label>
                                                        <input type="date" name="bday"  class="form-control" id="jqGridStatus">

                                                        <label>Company*</label>
                                                        <!--<select class="form-control" id="jqGridStatus">

                                                            <option value="All">All Selected</option>
                                                            <option value="1">uat1</option>


                                                        </select>-->
                                                        <select name="add_owner_hobby[]" class="form-control add_owner_hobby" multiple="multiple">
                                                            <option>test 1</option>
                                                            <option>test 2</option>
                                                            <option>test 3</option>
                                                            <option>test 4</option>
                                                            <option>test 5</option>
                                                        </select>


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="blue-btn" data-dismiss="modal">Apply Fiters</button>
                                                        <button type="button" class="grey-btn cancel_action" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <!-- Income Statement starts-->
                                    <td><a data-toggle="modal" data-target="#myModal8" href="javascript :;">Income Statement </a></td>
                                    <div class="modal fade" id="myModal8" role="dialog">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Income Statement</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <label>Company*</label>
                                                    <select class="form-control" id="jqGridStatus">

                                                        <option value="All">All Selected</option>
                                                        <option value="1">uat1</option>


                                                    </select>
                                                    <label>Date</label>
                                                    <input type="date" name="bday"  class="form-control" id="jqGridStatus">

                                                    <label>Portfolio Name</label>
                                                    <input type="text" class="form-control" id="jqGridStatus">

                                                    <label>Property Name</label>
                                                    <input type="text" class="form-control" id="jqGridStatus">







                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="blue-btn" ">Apply Fiters</button>
                                                    <button type="button" class="grey-btn cancel_action" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Regular Rent Ends -->
                        </div>
                    </div>
                    <!--Tabs Ends -->

                </div>
            </div>

        </div>
</div>
</section>
</div>
<!-- Wrapper Ends -->

<footer>
    <div class="container-fluid">
        &copy; <?php echo date("Y"); ?> Apexlink, Inc.  All rights reserved.
    </div>
</footer>
<!-- Footer Ends -->


<!-- Jquery Starts -->
<?php include_once(ROOT_URL . "/superadmin/views/layouts/admin_footer.php"); ?>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.responsivetabs.js"></script>
<script src="js/multiselect.js"></script>
<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });





        $('.add_owner_hobby').multiselect({
            includeSelectAllOption: true,
            nonSelectedText: 'Hobbies'
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    <!--- Accordians -->
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
    <!--- Accordians -->

    $('.reports-top').addClass('active');
</script>





<!-- Jquery Starts -->

</body>

</html>
