<!DOCTYPE html>
<?php
/**
 * Created by PhpStorm.
 * User: raghav chugh
 * Date: 1/25/2019
 * Time: 11:19 AM
 */
if (isset($_SESSION[SESSION_DOMAIN]['user_id'])) {
    if (isset($_POST['logout'])) {
        unset($_SESSION[SESSION_DOMAIN]['user_id']);
        //   $url = BASE_URL . "login";
        header('Location: ' . BASE_URL);
    }
} else {
    // $url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
}
?>


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/admin_header.php");
?>




<!-- Wrapper Starts -->
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer apxpg-top-search ">
                        <div class="row">
                            <!--<div class="col-md-8 col-sm-8 col-xs-12">
                                <strong class="apxpg-title">Title goes here...</strong>
                            </div>-->
                            <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="content-data apxpg-allcontent">
                        <!--Tabs Starts -->
                        <!-- Main tabs -->
                        <div class="main-tabs apx-tabs">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="default-settings">
                                    <!--Filters Bar-->
                                    <div class="white-btn-outer">

                                        <div class="col-sm-2">
                                            <label>Status</label>
                                            <select class="fm-txt form-control" id="jqGridStatus">
                                                <option value="All">All</option>
                                                <option selected value="1">Active</option>
                                                <option value="0">InActive</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3 pull-right text-right">
                                            <label></label>
                                            <a class="blue-btn" href="/Announcement/AddAnnouncement">Add New Announcement</a>
                                        </div>

                                    </div>
                                    <!--End Filters Bar-->
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div class="panel panel-default">
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body pad-none">
                                                                <div class="grid-outer">
                                                                    <!--Apex table-->
                                                                    <div class="apx-table">
                                                                        <div class="table-responsive">
                                                                            <table id="announcement-table" class="table table-bordered"></table>
                                                                        </div>
                                                                    </div>
                                                                    <!--End Apex Table-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>
<!-- Wrapper Ends -->

<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/admin_footer.php"); ?>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/validation/announcement/announcement.js"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/super_admin/announcement.js"></script>
<script>
    $('.announcement-top').addClass('active');
</script>

</body>

</html>