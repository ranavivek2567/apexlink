<!DOCTYPE html>
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}
?>

<?php
include_once(SUPERADMIN_DIRECTORY_URL."/views/layouts/admin_header.php");
?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/css/intlTelInput.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <!--Content Start-->
    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer apxpg-top-search">
                        <div class="row">
                            <!--<div class="col-md-8 col-sm-8 col-xs-12">
                                <strong class="apxpg-title">Title goes here...</strong>
                            </div>-->
                            <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data apxpg-allcontent">
                        <!--Tabs Starts -->
                        <!-- Main tabs -->
                        <div class="main-tabs apx-tabs">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="default-settings">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" ><a href="/Communication/SentEmails" >Email</a></li>
                                        <li role="presentation" class="active"><a href="/Communication/TextMessage" >Text Message</a></li>
                                        <li role="presentation"><a href="/communication/GroupMessage" >Group Message/Email</a></li>
                                    </ul>

                                    <!--Filters Bar-->
                                    <div class="white-btn-outer">

                                        <div class="col-sm-12">
                                            <a href="/Communication/AddTextMessage" class="blue-btn pull-right">Compose Message</a>

                                            <a href="/Communication/TextMessageDrafts" class="blue-btn pull-right">Drafts</a>
                                        </div>
                                    </div>

                                    <!--End Filters Bar-->
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div class="panel panel-default">
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body pad-none">
                                                                <div class="grid-outer">
                                                                    <!--Apex table-->
                                                                    <div class="apx-table">
                                                                        <div class="table-responsive">
                                                                            <table id="sentemails-table" class="table table-bordered">
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <!--End Apex Table-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Content End-->
    </main>
</div>
<div class="modal fade" id="sentMailModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width:800px;">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: unset;">
                <a class="close closeAnnouncementmodal closeSystemAnnouncement" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i></a>

                <a  href="javascript:void(0)"><h4>Sent Message </h4></a>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <strong>From: </strong>
                        <span class="modal_from"></span>
                    </div>
                    <div class="col-sm-12">
                        <strong>To: </strong>
                        <span class="modal_to"></span>
                    </div>
<!--                    <div class="col-sm-12">-->
<!--                        <strong>Subject: </strong>-->
<!--                        <span class="modal_subject"></span>-->
<!--                    </div>-->
                    <div class="col-sm-12">
                        <strong>Message: </strong>
                        <span class="modal_message"></span>
                    </div>
                    <div class="col-sm-12"></div>
                    <input type="hidden" id="compose_mail_id">
                    <div class="col-sm-12 text-right">
<!--                        <a class="blue-btn forward-email-btn new_classs">Forward</a>-->
                        <button class="blue-btn forward-email-btn">Forward</button>
                        <a class="grey-btn" id="cancel_communication" href="javascript:void(0)">
                            Cancel
                        </a>
                        <a class="blue-btn" id="message_recall" href="javascript:void(0)">
                            Recall
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Wrapper Ends -->
<?php
include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
<!-- Jquery Starts -->
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script>

    jQuery(document).ready(function () {

        jQuery(document).on('change','.select_options',function () {
            var select_options = $(this).val();
            var data_id = $(this).attr('data_id');

            if(select_options == 'View')
            {
                window.location.href= '/Companies/View?id='+data_id;
            }else if(select_options == 'Edit')
            {
                window.location.href= '/Companies/Edit?id='+data_id;
            }else if(select_options == 'Deactivate')
            {
                bootbox.confirm("Do you want to deactivate the record ?", function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/company-user-ajax',
                            data: {class: 'CompanyUserAjax', action: 'deactivate', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Company activated successfully');
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                });

            }else if(select_options == 'Go To Site')
            {
                alert('Go To Site');
            }else if(select_options == 'Renew Plan')
            {
                alert('Renew Plan');
            }else if(select_options == 'Upgrade Plan')
            {
                alert('Upgrade Plan');
            }else if(select_options == 'Resend Welcome Mail')
            {
                alert('Resend Welcome Mail');
            }else if(select_options == 'Activate')
            {
                bootbox.confirm("Do you want to activate the record ?", function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/company-user-ajax',
                            data: {class: 'CompanyUserAjax', action: 'activate', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Company activated successfully');
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                });
            }else if(select_options == 'Delete')
            {
                bootbox.confirm("Do you want to delete this record ?", function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/company-user-ajax',
                            data: {class: 'CompanyUserAjax', action: 'delete', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Company deleted successfully');
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                });
            }
        });
        alphabeticSearch();

        function alphabeticSearch(){
            $.ajax({
                type: 'post',
                url:'/Companies/List/jqgrid',
                data: {class: 'jqGrid',
                    action: "alphabeticSearch",
                    table: 'users',
                    column: 'company_name'},
                success : function(response){
                    var response = JSON.parse(response);
                    if(response.code == 200){
                        var html = '';

                        $.each(response.data, function(key,val) {
                            var color = '#05A0E4'
                            if(val == 0) color = '#c5c5c5';
                            html += '<span class="getAlphabet" style="color:'+color+'" data_id="'+val+'">'+key+'</span>';
                        });
                        $('.AtoZ').html(html);
                    }
                }
            });
        }

        $(document).on('click','#AZ',function(){
            $('.AZ').hide();
            $('#apex-alphafilter').show();
        });

        $(document).on('click','#allAlphabet',function(){
            var grid = $("#companyUser-table");
            $('.AZ').show();
            $('#apex-alphafilter').hide();
            grid[0].p.search = false;
            $.extend(grid[0].p.postData,{filters:""});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
            //$('#companyUser-table').trigger( 'reloadGrid' );
        });

        $(document).on('click','.getAlphabet',function(){
            var grid = $("#companyUser-table"),f = [];
            var value = $(this).attr('data_id');
            var search = $(this).text();
            if(value != '0'){
                f.push({field:"company_name",op:"bw",data:search});
                grid[0].p.search = true;
                $.extend(grid[0].p.postData,{filters:JSON.stringify(f)});
                grid.trigger("reloadGrid",[{page:1,current:true}]);
            }
        })
    });

    /**
     * Function for view company on clicking row
     */
    $(document).on('click','#sentemails-table tr td:not(:last-child)',function(){
        var compose_mail_id = $(this).closest("tr").attr('id');
        console.log(compose_mail_id);
        $.ajax({
            url:'/Communication/composeTextMessageAjax',
            method: 'post',
            data: {
                class: "composeTextMessageAjax",
                action: "getComposeForView",
                'id': compose_mail_id,
            },
            success: function (data) {
                info =  JSON.parse(data);
                if(info.status=="success"){
                    var res = info.data;
                    // $('.to').tagsinput('add', res.email_to);
                    // $('.to_name').tagsinput('add', res.user_name+'>'+res.email_to);
                    // $('.subject').val(res.email_subject);
                    // $('#mesgbody').text(res.email_message);


                    if(res.email_to)
                    {
                        $('.modal_to').text(res.email_to);
                    }

                    $('.modal_from').text(res.email_from);
                    $('.modal_message').html(res.email_message);
                    $('#compose_mail_id').val(res.id);

                }
            },

        });

        $('#sentMailModal').modal('show');
        // window.location.href = '/Companies/View?id='+id;
    });

    function triggerReload(){
        var grid = $("#companyUser-table");
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:""});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }


    function statusFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Active";
        else if(cellvalue == '0')
            return "Not Activated";
        else if(cellvalue == 2)
            return "Pending Payment";
        else if(cellvalue == 3)
            return "Deleted";
        else
            return '';
    }

    function accountTypeFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Cash";
        else
            return 'Accural';
    }


    function actionFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            console.log(rowObject.Status);
            var select = '';
            if(rowObject.Status == 1)  select = ['View','Edit','Deactivate','Go To Site','Delete','Renew Plan','Upgrade Plan','Resend Welcome Mail'];
            if(rowObject.Status == '0' || rowObject.Status == '')  select = ['View','Edit','Activate','Delete'];
            if(rowObject.Status == 2)  select = ['Edit','Launch','Delete'];
            if(rowObject.Status == 3)  select = '';
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }

    }

    $('.communication-top').addClass('active');

</script>
<link rel="stylesheet" href="<?php echo SUPERADMIN_SITE_URL; ?>/css/bootstrap-tagsinput.css" />
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
<link rel="stylesheet" href="https://rawgit.com/timschlechter/bootstrap-tagsinput/master/src/bootstrap-tagsinput.css" />
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/super_admin/communication/textMessage/compose-message.js"></script>
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/super_admin/communication/textMessage/sent-messages.js"></script>
</body>

</html>
