<!DOCTYPE html>
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}
?>

<?php
include_once(SUPERADMIN_DIRECTORY_URL."/views/layouts/admin_header.php");
?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/css/intlTelInput.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <!--Content Start-->
    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Communication &gt;&gt; <span>Drafts Group Message/Email </span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data">
<!--                        --><?php //include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                        <!--Tabs Starts -->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="/Communication/SentEmails" >Email</a></li>
                                <li role="presentation"><a href="/Communication/TextMessage" >Text Message</a></li>
                                <li role="presentation"><a href="/communication/GroupMessage" >Group Message/Email</a></li>
                            </ul>

                            <!-- Tab panes -->

                            <div class="tab-content">
                                <div class="panel-heading">
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="receivables">
                                            <div class="property-status">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="content-section">
                                                            <div class="main-tabs">
                                                                <div role="tabpanel" class="tab-pane active" id="communication-one">
                                                                    <!-- Sub Tabs Starts-->
                                                                    <div class="property-status">
                                                                        <div class="row">
                                                                            <div class="col-sm-12 text-right" style="padding-bottom: 10px;
    float: left;
    width: 100%;
    margin-bottom: 17px;">
                                                                                <a class="blue-btn"  id="composer_group" href="/Communication/AddGroupMessage">Compose New Group Message</a>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="btn-outer text-right">
                                                                                    <a class="drafted_mail blue-btn" href="/Communication/DraftedGroupMessages">Drafted Group Emails</a>
                                                                                    <a class="drafted_message blue-btn" href="javascript:void(0)">Drafted Group Text Messages</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-outer" >
                                                                        <input type="hidden" value="" id="composer_mail_id" name="edit_id">
                                                                        <div class="form-hdr">
                                                                            <h3>Drafts</h3>
                                                                        </div>
                                                                        <div class="form-data">
                                                                            <div class="col-sm-12">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <div class="apx-table ">
                                                                                            <div class="email_tab table-responsive">
                                                                                                <table id="communication-draft-mail-table" class="table table-bordered"></table>
                                                                                            </div>
                                                                                            <div class="text_tab table-responsive">
                                                                                                <table id="communication-draft-mesg-table" class="table table-bordered"></table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Regular Rent Ends -->

                                    </div>
                                </div>
                                <!-- Sub tabs ends-->
                            </div>



                            <!-- Sub Tabs Starts-->

                            <!-- Sub tabs ends-->
                        </div>

                    </div>
                </div>

                <!--Tabs Ends -->

            </div>
</div>
</div>
</section>
    </main>
</div>
<?php
include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
<script>
    $(document).on("click",".clear-btn.email_clear",function () {
        $("#sendEmail .label-info").text('');
        $("#sendEmail #mesgbody").val('');
        $("#sendEmail .subject").val('');
        //  resetFormClear('#sendEmail',[''],'form',true);
    });
</script>
<link rel="stylesheet" href="<?php echo SUPERADMIN_SITE_URL; ?>/css/bootstrap-tagsinput.css" />
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
<link rel="stylesheet" href="https://rawgit.com/timschlechter/bootstrap-tagsinput/master/src/bootstrap-tagsinput.css" />
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/super_admin/communication/groupMessage/draft-group-message.js"></script>
</body>
</html>
