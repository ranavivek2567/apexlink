<!DOCTYPE html>
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}
?>

<?php
include_once(SUPERADMIN_DIRECTORY_URL."/views/layouts/admin_header.php");
?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/css/intlTelInput.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/superadmin/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <!--Content Start-->
    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Communication &gt;&gt; <span>Email </span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text" />
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-12">
                        <div class="content-section">
                            <div class="main-tabs">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" ><a href="/Communication/SentEmails" >Email</a></li>
                                    <li role="presentation" class="active"><a href="/Communication/TextMessage" >Text Message</a></li>
                                    <li role="presentation"><a href="/communication/GroupMessage" >Group Message/Email</a></li>
                                </ul>
                                <div role="tabpanel" class="tab-pane active" id="communication-one">
                                    <!-- Sub Tabs Starts-->
                                    <div class="property-status" style="margin-top: 50px;">
                                        <div class="row">
                                            <div class="col-sm-2">
                                            </div>
                                            <div class="col-sm-10">
                                                <div class="btn-outer text-right">
                                                    <a class="blue-btn" href="/Communication/TextMessageDrafts">Drafts</a>
                                                    <a class="blue-btn" href="/Communication/AddTextMessage">Compose Message</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="accordion-form">
                                        <div class="accordion-outer">
                                            <form id="sendEmail">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                        Text Message</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="panel-body compose-email2">
                                                                    <div class="row">
                                                                        <div class="col-sm-1">
                                                                            <a class="blue-btn compose-email-btn" style="float: left;">
                                                                                Send
                                                                            </a>
                                                                        </div>
                                                                        <div class="col-sm-8">
                                                                            <div class="row">
                                                                                <input class="form-control mail_type" name="mail_type" type="hidden" value="draft" />
                                                                                <div class="col-sm-2">
                                                                                    <label>To<em class="red-star">*</em></label>
                                                                                </div>
                                                                                <div class="col-sm-10 to_field">
                                                                                <span>
                                                                                    <input class="form-control to" name="to" autofocus type="text"/>
                                                                                     </span>

                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                                    <label style="font-weight:bolder">Please enter the recipient’s country code plus their cell phone number.  </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-1">
                                                                            <input class="form-control to_name" name="to_name" type="text" style="display:none;"/>

                                                                        </div>
<!--                                                                        <div class="col-sm-8">-->
<!--                                                                            <div class="row">-->
<!--                                                                                <div class="col-sm-2">-->
<!--                                                                                    <label>-->
<!--                                                                                        Subject <em class="red-star">*</em>-->
<!--                                                                                    </label>-->
<!--                                                                                    <span style="display: none;">-->
<!--                                                                                <input class="form-control to_usertype" name="to_usertype" type="text" style="display:none;"/>-->
<!--                                                                            </span>-->
<!--                                                                                </div>-->
<!--                                                                            </div>-->
<!--                                                                        </div>-->
<!--                                                                        <div class="col-sm-3">-->
<!--                                                                        </div>-->
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-1">
                                                                        </div>
                                                                        <div class="col-sm-8">
                                                                            <div class="row">
                                                                                <div class="col-sm-2">
                                                                                    <label>Message
                                                                                        <em class="red-star">*</em></label>
                                                                                </div>
                                                                                <input type="hidden" value="" id="compose_mail_id" name="edit_id">
                                                                                <div class="col-sm-10">
                                                                                <span>
                                                                                    <textarea id="mesgbody" style="height:150px;" class="form-control capital" name="mesgbody" maxlength="300"></textarea>
                                                                                    <strong style="color:#585858;float:left;width:100%; margin-top:6px;"><span id="chracter_count">300</span> Characters left</strong>
                                                                                </span>
                                                                                    <br>  <br>  <br>  <br>
                                                                                    <div class="btn-outer text-right">
                                                                                        <a class="blue-btn compose-email-save-btn" id="abc">Save</a>
                                                                                        <a class="grey-btn" id="cancel_email" href="javascript:void(0)">
                                                                                            Cancel
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                          <label class="compose-text-msg">For all US and Canadian recipients, please enter +1 before the area code and then the cell phone number. </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Accordian Ends -->
                                            </form>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
        </section>

        <div class="container">
            <div class="modal fade" id="torecepents" role="dialog">
                <div class="modal-dialog modal-sm" style="width: 600px;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title">Add Recipients </h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-outer" style="float: none;height:auto;min-height: 300px;">
                                <div class="col-sm-6">
                                    <label>Select <em class="red-star">*</em></label>
                                    <select class="form-control selectUsers">
                                        <option value="">Select</option>
                                        <option value="2">Tenant</option>
                                        <option value="4">Owner</option>
                                        <option value="3">Vendor</option>
                                        <option value="5">Other Contacts</option>
                                        <option value="6">Guest Card</option>
                                        <option value="10">Rental Application</option>
                                        <option value="8">Employee</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <label></label>
                                    <br/>
                                    <div class="blue-search">
                                        <input type="text" value="" class="form-control" id="toSearch">
                                        <span class="icon">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </span>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="userDetails"><table class="table" border="1px"><tbody><tr>
                                                <th>id</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                            </tr><tr><td colspan="3" align="center"  bgcolor="#f7f7f7">
                                                    No Record Found
                                                </td></tr></tbody></table></div>
                                </div>
                                <div class="popup_values" style="display: none;">
                                    <input class="form-control popup_to" type="text"/>
                                </div>
                                <div class="col-sm-12">
                                    <button class="blue-btn" id="SendselectToUsers">Done</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
  </div>
 <?php
 include_once(SUPERADMIN_DIRECTORY_URL . "/views/layouts/admin_footer.php");
  ?>
<script>
    $(document).on("click",".clear-btn.email_clear",function () {
        $("#sendEmail .label-info").text('');
        $("#sendEmail #mesgbody").val('');
        $("#sendEmail .subject").val('');
        //  resetFormClear('#sendEmail',[''],'form',true);
    });
</script>
<link rel="stylesheet" href="<?php echo SUPERADMIN_SITE_URL; ?>/css/bootstrap-tagsinput.css" />
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
<link rel="stylesheet" href="https://rawgit.com/timschlechter/bootstrap-tagsinput/master/src/bootstrap-tagsinput.css" />
<script src="<?php echo SUPERADMIN_SITE_URL; ?>/js/super_admin/communication/textMessage/compose-message.js"></script>
</body>
</html>
