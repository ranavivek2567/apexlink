<?php


include_once( ROOT_URL."/superadmin/functions/FlashMessage.php");
$request = $_SERVER['REQUEST_URI'];

$request = preg_replace('{/$}', '', $request);

$request1 = explode('/', $_SERVER["REQUEST_URI"]);
$key = end($request1);
if (is_numeric($key)) {
    array_pop($request1);
    $request = implode('/', $request1);
}

$var1 = strrpos($request, '?');
if (!empty($var1)) {
    $url = explode('?', $request);
    array_pop($url);
    $request = end($url);
}

function Route($uri,$prefix=null,$controller,$function){
    if($_SERVER['REQUEST_URI'] != $uri) return;
    FlashMessage::add($function);
    if(!empty($prefix)) $controller = $prefix.'/'.$controller;
    return require __DIR__ . '/functions/'.$controller.'.php';
}
switch ($request) {
    case '/' :
        require __DIR__ . '/views/auth/login.php';
        break;
    case '' :
        require __DIR__ . '/views/auth/login.php';
        break;
    case '/login' :
        require __DIR__ . '/views/auth/login.php';
        break;
    case '/Dashboard/Dashboard' :
        require __DIR__ . '/views/dashboard/dashboard.php';
        break;

    /*Forgot Password Module Starts*/
    case '/MasterData/ForgotPassword' :
        require __DIR__ . '/views/auth/forgot_password.php';
        break;
    case '/User/forgotPassword' :
        require __DIR__ . '/functions/user.php';
        break;
    case '/User/ResetPassword' :
        require __DIR__ . '/views/auth/reset_password.php';
        break;
    case '/User/resetPassword' :
        require __DIR__ . '/functions/user.php';
        break;
    /*Forgot Password Module Ends*/

    case '/user-ajax' :
        require __DIR__ . '/functions/user.php';
        break;
    /* parvesh routest start */
    case '/Setting/ApexLinkNew' :
        require __DIR__ . '/views/settings/apexlink_new/list.php';
        break;
    /* parvesh routest emd */

    /*Plans Module Starts*/
    case '/Plans/List' :
        require __DIR__ . '/views/plans/list.php';
        break;
    //jqgrid
    case '/Plans/List/jqgrid' :
        require __DIR__ . '/functions/jqgrid.php';
        break;
    case '/Plans/ViewPlan' :
        require __DIR__ . '/views/plans/view.php';
        break;
    case '/Plans/PlanAjax' :
        require __DIR__ . '/functions/plan.php';
        break;
    case '/Plans/Add' :
        require __DIR__ . '/views/plans/add.php';
        break;
    case '/Plans/Edit' :
        require __DIR__ . '/views/plans/edit.php';
        break;
    case '/Plans/updateStatus' :
        require __DIR__ . '/views/plans/edit.php';
        break;
    /*Plans Module Ends*/

    /*Payment Module Start*/
    case '/Payment/List' :
        require __DIR__ . '/views/payment/list.php';
        break;
    //listing
    /*Payment Module End*/

    case '/company-user-ajax' :
        require __DIR__ . '/functions/companyUser.php';
        break;
    case '/Setting/Settings' :
        require __DIR__ . '/views/settings/settings.php';
        break;
    case '/Setting/ManageUser' :
        require __DIR__ . '/views/settings/manage_user/list.php';
        break;
    //with new redirect functionality
    case '/Setting/AddManageUser' :
//        Route('/Setting/AddManageUser','user','DefaultSettings','view');
        require __DIR__ . '/views/settings/manage_user/add.php';
        break;
    //end\
    case '/settings-ajax' :
        require __DIR__ . '/functions/default_settings.php';
        break;
    case '/helper-ajax' :
        require __DIR__ . '/functions/helper_old.php';
        break;

    case ('/Spotlight') :
        require __DIR__ . '/functions/spotlight/spotlight.php';
        break;
    case '/mail' :
        require __DIR__ . '/helper/mail.php';
        break;
    case '/Announcement/Announcements' :
        require __DIR__ . '/views/announcements/list.php';
        break;
    case '/Announcement/AddAnnouncement' :
        require __DIR__ . '/views/announcements/add.php';
        break;
    case '/Announcement/EditAnnouncement' :
        require __DIR__ . '/views/announcements/edit.php';
        break;
    case '/Announcement/CopyAnnouncement' :
        require __DIR__ . '/views/announcements/copy.php';
        break;
    case '/announcement-ajax' :
        require __DIR__ . '/functions/announcement.php';
        break;
    case '/runMigrations' :
        require __DIR__ . '/helper/MigrationCusers.php';
        MigrationCusers::runMigrationsAll();
        break;



    /*Manage Support Team Module Starts*/
    case '/Support/Index' :
        require __DIR__ . '/views/manageSupportTeam/list.php';
        break;
    case '/Support/AddSupportMember' :
        require __DIR__ . '/views/manageSupportTeam/add.php';
        break;
    case '/Support/SupportAjax' :
        require __DIR__ . '/functions/manageSupportTeam.php';
        break;

    /*Manage Support Team Module Ends*/

    /*Company Module Start*/
    case '/Companies/View' :
        require __DIR__ . '/views/company/view.php';
        break;
    //listing
    case '/Companies/List' :
        require __DIR__ . '/views/company/list.php';
        break;
    case '/Companies/makepayment' :
        require __DIR__ . '/views/company/makepayment.php';
        break;
    //jqgrid
    case '/Companies/List/jqgrid' :
        require __DIR__ . '/functions/jqgrid.php';
        break;
    case '/Companies/Add' :
        require __DIR__ . '/views/company/add.php';
        break;
    case ('/Companies/Edit') :
        require __DIR__ . '/views/company/edit.php';
        break;
    case ('/Reporting/Reporting') :
        require __DIR__ . '/views/reports/view.php';
        break;
    case ('/Setting/IPAddress') :
        require __DIR__ . '/views/audit trails/view.php';
        break;
    case ('/Communication/SentEmails') :
        require __DIR__ . '/views/communication/view.php';
        break;
    case ('/Companies/RenewPlan') :
        require __DIR__ . '/views/company/renewplan.php';
        break;
    case ('/Companies/UpgradePlan') :
        require __DIR__ . '/views/company/upgradeplan.php';
        break;

    /*Company Module End*/


    /* Custom field */
    case ('/Companies/Edit') :
        Route('/CustomField/add',null,'CustomFields','add');
        break;

    /* End */
    //payment
    case ('/payment-ajax') :
        require __DIR__ . '/functions/payment/payment.php';
        break;

    case ('/test_cron_sub') :
        require __DIR__ . '/../cron/subsciptionCron.php';
        break;
//
   case ('/tenantPaymentCron') :
       require __DIR__ . '/../cron/tenantCron.php';
       break;
    case ('/journal_entry_cron') :
        require __DIR__ . '/../cron/journalEntryCron.php';
        break;
    case '/payment-stripe-ajax' :
        require __DIR__ . '/functions/payment/payment.php';
        break;
    /*For Report module*/

    /*For Communication Module*/
    case ('/Communication/ComposeEmail') :
        require __DIR__ . '/views/communication/email/compose.php';
        break;
    case ('/Communication/ComposeMailAjaxs') :
        require __DIR__ . '/functions/communication/email/composeMailAjax.php';
        break;
    case ('/Communication/SentEmails') :
        require __DIR__ . '/views/communication/view.php';
        break;
    case ('/Communication/DraftedMails') :
        require __DIR__ . '/views/communication/drafts.php';
        break;
    case ('/Communication/TextMessage') :
        require __DIR__ . '/views/communication/textmsg.php';
        break;
    case ('/communication/GroupMessage') :
        require __DIR__ . '/views/communication/groupmsg.php';
        break;
     case ('/Communication/DraftedGroupMessages') :
         require __DIR__ . '/views/communication/groupMessage/draftGMessage.php';
        break;
    case ('/Communication/AddGroupMessage') :
        require __DIR__ . '/views/communication/groupMessage/composeMessage.php';
        break;
    case ('/Communication/AddTextMessage') :
        require __DIR__ . '/views/communication/textMessage/composeText.php';
        break;
    case ('/Communication/composeTextMessageAjax') :
        require __DIR__ . '/functions/communication/textMessage/composeTextMessageAjax.php';
        break;
    case ('/Communication/TextMessageDrafts') :
        require __DIR__ . '/views/communication/textMessage/draftText.php';
        break;
    case ('/Communication/GroupMessageAjax') :
        require __DIR__ . '/functions/communication/groupMessage/groupMessageAjax.php';
        break;

    /*For Communication Module*/

    /*Cron for Notification*/
    case ('/test_cron_notification') :
        require __DIR__ . '/../cron/notificationUser.php';
        break;
    /*Cron for Notification*/

    case '/drop_database' :
        require __DIR__ . '/helper/MigrationCusers.php';
        MigrationCusers::dropDatabase();
        break;


    case ('/serverSpace') :
        require __DIR__ . '/functions/spotlight/serverSpace.php';
        break;
    case ('/serverSpaceView') :
        require __DIR__ . '/views/spotlight/serverSpace.php';
        break;


    default:
        require __DIR__ . '/views/auth/login.php';
        break;
}