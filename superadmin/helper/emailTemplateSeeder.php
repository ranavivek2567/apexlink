<?php

try{
$data = array(
array('template_title' => 'WELCOME EMAIL TO NEW COMPANY USER', 'template_html' => '<div class="wrapper">
    <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="newCompanyUser_Key">

    <table style="box-shadow: #96999d 0px 0px 2px; border: 1px solid #dddddd; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" data-mce-style="box-shadow: #96999d 0px 0px 2px; border: 1px solid #dddddd; width: 640px;" class="mceItemTable">
        <tbody>
        <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;"><td>&nbsp;</td></tr>
        <tr style="border-collapse: collapse; background-color: #fff;" data-mce-style="border-collapse: collapse; background-color: #fff;">
        </tr></tbody></table>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{Company Logo}<table style="box-shadow: #96999d 0px 0px 2px; border: 1px solid #dddddd; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" data-mce-style="box-shadow: #96999d 0px 0px 2px; border: 1px solid #dddddd; width: 640px;" class="mceItemTable"><tbody><tr style="border-collapse: collapse; background-color: #fff;" data-mce-style="border-collapse: collapse; background-color: #fff;"><td style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" align="center" data-mce-style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;">
            </td>
        </tr>

        <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;">
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="padding: 20px;" data-mce-style="padding: 20px;"><h3 style="color: #00b0f0; font-family: arial; text-align: center;" data-mce-style="color: #00b0f0; font-family: arial; text-align: center;">Welcome to ApexLink&nbsp;{Firstname}</h3>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">We are so pleased that you/your team has selected ApexLink as your property management software solution. So let get started!</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">We show that you will be using the following email address and password for your account:</p>
                <p style="font-family: arial; text-align: center;" data-mce-style="font-family: arial; text-align: center;"><strong>USER NAME</strong>=&nbsp;{Username}</p>
                <p style="font-family: arial; text-align: center;" data-mce-style="font-family: arial; text-align: center;">Please click on the link below to setup your password and login:</p>
                <h3 style="font-family: arial; text-align: center; font-weight: bold;" data-mce-style="font-family: arial; text-align: center; font-weight: bold;"><a href="{Url}" data-mce-href="{Url}">Click here to Login</a></h3>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">We look forward to having you as one of our premier ApexLink users. Remember, if you have any questions, we are here for you 24/7!</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Sincerely,</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Your ApexLink User Support Team</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">By logging in this software you are agreeing to the ApexLink Terms of use agreement, Privacy Statement and Security Statement located on our main website at <a href="javascript:;" data-mce-href="javascript:;"> www.apexLink.com</a></p>

            </td>
        </tr>
        <tr>
            <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;" align="center" bgcolor="#05a0e4" data-mce-style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;">
                ApexLink Property Manager <span>&#x025CF;</span>&nbsp;<a style="color: #fff; text-decoration: none;" href="javascript:;" data-mce-href="javascript:;" data-mce-style="color: #fff; text-decoration: none;">support@apexlink.com &nbsp;<span>&#x025CF;</span> 772-212-1950</a>
            </td>
        </tr>
        </tbody>
    </table>
</div>','template_key' =>'newCompanyUser_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'WELCOME EMAIL TO NEW OWNER', 'template_html' => '<div class="wrapper">
    <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="newOwnerWelcome_Key">
    <table style="box-shadow: #96999d 0px 0px 2px; border: 1px solid #dddddd; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" data-mce-style="box-shadow: #96999d 0px 0px 2px; border: 1px solid #dddddd; width: 640px;" class="mceItemTable">

        <tbody>
        <tr><td style="background-color: #00b0f0;" data-mce-style="background-color: #00b0f0;">&nbsp;</td></tr>
        <tr><td style="padding: 20px 0;" align="center" data-mce-style="padding: 20px 0;">{CompanyLogo}</td></tr>
        <tr><td style="background-color: #00b0f0;" data-mce-style="background-color: #00b0f0;">&nbsp;</td></tr>
        <tr><td style="padding: 20px;" data-mce-style="padding: 20px;">
                <h3 style="color: #00b0f0; font-family: arial; text-align: center;" data-mce-style="color: #00b0f0; font-family: arial; text-align: center;">Welcome to ApexLink&nbsp;{Firstname}</h3>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Your Owner Portal has been created, so let get started! On your portal, you can manage your account, submit work order request, view your statements and transactions, communicate with the office and much more.</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">We show that you will be using the following email address and password for your account:</p>
                <p style="font-family: arial; text-align: center;" data-mce-style="font-family: arial; text-align: center;"><strong>USER NAME</strong>=&nbsp;{Username}</p>
                <p style="font-family: arial; text-align: center;" data-mce-style="font-family: arial; text-align: center;">Please click on the link below to setup your password and login:</p><h3 style="font-family: arial; text-align: center; font-weight: bold;" data-mce-style="font-family: arial; text-align: center; font-weight: bold;">
                    <a href="{Url}" data-mce-href="{Url}">Click here to Login</a></h3>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Sincerely,</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Your ApexLink User Support Team</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">By logging in this software you are agreeing to the ApexLink Terms of use agreement, Privacy Statement and Security Statement located on our main website at <a href="javascript:;" data-mce-href="javascript:;">www.apexLink.com</a></p>
            </td></tr>
        <tr><td style="background-color: #00b0f0; font-family: arial; text-align: center; color: #fff; padding: 20px 0;" data-mce-style="background-color: #00b0f0; font-family: arial; text-align: center; color: #fff; padding: 20px 0;">ApexLink Property Manager <span>&#x025CF;</span> support@apexlink.com <span>&#x025CF;</span> 772-212-1950</td>
        </tr>
        </tbody>
    </table>
</div>','template_key' =>'newOwnerWelcome_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'WELCOME EMAIL TO NEW TENANT', 'template_html' => '<div class="wrapper">
    <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="newTenantWelcome_Key">
    <table style="box-shadow: 0 0 2px #96999d;
            border: 1px solid #ddd;" width="640" border="0" cellspacing="0" cellpadding="0" align="center" data-mce-style="box-shadow: 0 0 2px #96999d; border: 1px solid #ddd;" class="mceItemTable">

        <tbody>
        <tr>
            <td style="background-color: #00b0f0;" data-mce-style="background-color: #00b0f0;">&nbsp;</td>
        </tr>
        <tr>
            <td style="padding: 20px 0;" align="center" data-mce-style="padding: 20px 0;">{CompanyLogo}</td>
        </tr>
        <tr>
            <td style="background-color: #00b0f0;" data-mce-style="background-color: #00b0f0;">&nbsp;</td>
        </tr>
        <tr>
            <td style="padding: 20px;" data-mce-style="padding: 20px;">

                <h3 style="color: #00b0f0; font-family: arial; text-align: center;" data-mce-style="color: #00b0f0; font-family: arial; text-align: center;">Welcome to ApexLink {Firstname}</h3>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Your Tenant Portal has been created, so let’s get started! On your portal, you can manage your account, pay rent online, submit work order request, view your statements and transactions, communicate with the office and much more.</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">We show that you will be using the following email address and password for your account:</p>
                <p style="font-family: arial; text-align: center;" data-mce-style="font-family: arial; text-align: center;"><strong>USER NAME</strong>= <a href="javascript:;" data-mce-href="javascript:;">{Username}</a></p>
                <p style="font-family: arial; text-align: center;" data-mce-style="font-family: arial; text-align: center;">Please click on the link below to setup your password and login:</p>
                <h3 style="font-family: arial; text-align: center; font-weight: bold;" data-mce-style="font-family: arial; text-align: center; font-weight: bold;"><a href="{Url}" data-mce-href="{Url}">Click here to Login</a></h3>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Sincerely,</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Your ApexLink User Support Team</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">By logging in this software you are agreeing to the ApexLink Terms of use agreement, Privacy Statement and Security Statement located on our main website at <a href="javascript:;" data-mce-href="javascript:;"> www.apexLink.com</a></p>
            </td>
        </tr>
        <tr>
            <td style="padding:10px; font-size:12px; color:#ffffff; font-weight: bold;" align="center" bgcolor="#05a0e4" data-mce-style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;">ApexLink Property Manager <span>&#x025CF;</span> <a style="color: #fff; text-decoration: none;" href="javascript:;" data-mce-href="javascript:;" data-mce-style="color: #fff; text-decoration: none;">support@apexlink.com&nbsp; <span>&#x025CF;</span> 772-212-1950</a></td>
        </tr>
        </tbody>
    </table>
</div>','template_key' =>'newTenantWelcome_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'WELCOME EMAIL TO NEW VENDOR', 'template_html' => '<div class="wrapper">
    <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="newVendorWelcome_Key">
    <table style="box-shadow: 0 0 2px #96999d;
        border: 1px solid #ddd;" width="640" border="0" cellspacing="0" cellpadding="0" align="center" data-mce-style="box-shadow: 0 0 2px #96999d; border: 1px solid #ddd;" class="mceItemTable">
        <tbody>

        <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;">
            <td>&nbsp;</td>
        </tr>
        <tr style="border-collapse: collapse; background-color: #fff;" data-mce-style="border-collapse: collapse; background-color: #fff;"><td style="font-family:  Arial, Helvetica, Geneva, sans-serif;
                border-collapse: collapse;" align="center" data-mce-style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;"></td>
        </tr>
        <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;">
            <td>&nbsp;</td>
        </tr><tr><td style="padding: 20px;" data-mce-style="padding: 20px;">
                <h3 style="color: #00b0f0; font-family: arial; text-align: center;" data-mce-style="color: #00b0f0; font-family: arial; text-align: center;">Welcome to ApexLink {Firstname}</h3>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Your Vendor Portal has been created. On your portal you will be able to; manage your account, view your statements and documents, manage work orders assigned to you, communicate with the office and much more.</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">We show that you will be using the following email address and password for your account:</p>
                <p style="font-family: arial; text-align: center;" data-mce-style="font-family: arial; text-align: center;"><strong>USER NAME</strong>= <a href="javascript:;" data-mce-href="javascript:;">{Username}</a></p>
                <p style="font-family: arial; text-align: center;" data-mce-style="font-family: arial; text-align: center;">Please click on the link below to setup your password and login:</p>
                <h3 style="font-family: arial; text-align: center; font-weight: bold;" data-mce-style="font-family: arial; text-align: center; font-weight: bold;"><a href="{Url}" data-mce-href="{Url}">Click here to Login</a></h3>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Sincerely,</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Your ApexLink User Support Team</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">By logging in this software you are agreeing to the ApexLink Terms of use agreement, Privacy Statement and Security Statement located on our main website at <a href="javascript:;" data-mce-href="javascript:;"> www.apexLink.com</a></p></td>
        </tr>
        <tr>
            <td style="padding: 10px; font-size: 12px; color: #ffffff;font-weight: bold;" align="center" bgcolor="#05a0e4" data-mce-style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;">ApexLink Property Manager <span>&#x025CF;</span>
                <a style="color: #fff; text-decoration: none;" href="javascript:;" data-mce-href="javascript:;" data-mce-style="color: #fff; text-decoration: none;">support@apexlink.com&nbsp; <span>&#x025CF;</span> 772-212-1950</a>
            </td>
        </tr>
        </tbody>
    </table>
</div>','template_key' =>'newVendorWelcome_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Forgot Password Email', 'template_html' => '<div class="wrapper">
    <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="forgetPasswordEmail_Key">
    <table class="w640 mceItemTable" style="margin: 0px 10px; width: 640px;" border="0" cellspacing="0" cellpadding="0" data-mce-style="margin: 0px 10px; width: 640px;">
        <tbody>
        <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;">
            <td>;</td>
        </tr>
        <tr style="border-collapse: collapse; background-color: #fff;" data-mce-style="border-collapse: collapse; background-color: #fff;"><td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580" data-mce-style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;"><center><div style="margin-top: 1px; margin-bottom: 10px;" data-mce-style="margin-top: 1px; margin-bottom: 10px;"></div></center></td></tr>
        <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;"><td></td></tr>
        <tr id="simple-content-row" style="border-collapse: collapse;" data-mce-style="border-collapse: collapse;"><td class="w640" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" bgcolor="#ffffff" width="640" data-mce-style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;">
                <table class="w640 mceItemTable" style="width: 640px;" border="0" cellspacing="0" cellpadding="0" align="left" data-mce-style="width: 640px;">
                    <tbody>
                    <tr style="border-collapse: collapse;" data-mce-style="border-collapse: collapse;"><td class="w30" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="30" data-mce-style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;"></td><td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580" data-mce-style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;"><table class="w580 mceItemTable" style="width: 580px;" border="0" cellspacing="0" cellpadding="0" data-mce-style="width: 580px;"><tbody>
                                <tr style="border-collapse: collapse;" data-mce-style="border-collapse: collapse;"><td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580" data-mce-style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;"><center><p class="article-title" style="font-size: 22px; line-height: 24px; color: #ff0000; font-weight: bold; text-align: center; margin-top: 0px; margin-bottom: 18px; font-family: Arial, Helvetica, Geneva, sans-serif;" align="left" data-mce-style="font-size: 22px; line-height: 24px; color: #ff0000; font-weight: bold; text-align: center; margin-top: 0px; margin-bottom: 18px; font-family: Arial, Helvetica, Geneva, sans-serif;">Forgotten your Password?</p></center><div class="article-content" style="font-size: 13px; line-height: 18px; color: #444444; margin-top: 0px; margin-bottom: 18px; font-family:  Arial, Helvetica, Geneva, sans-serif;" align="left" data-mce-style="font-size: 13px; line-height: 18px; color: #444444; margin-top: 0px; margin-bottom: 18px; font-family: Arial, Helvetica, Geneva, sans-serif;"><p style="margin-bottom: 15px;" data-mce-style="margin-bottom: 15px;">Dear  ,</p><p style="margin-bottom: 15px;" data-mce-style="margin-bottom: 15px;">This email was sent automatically to you by Apexlink in response to your request to recover your password. This is done for your protection; only you, the recipient of this email can take the necessary step in your password recover process.</p><p style="margin-bottom: 15px;" data-mce-style="margin-bottom: 15px;">To reset your password and access your account either click on the following link below or copy link and paste the link into the address bar of your browser:</p><p style="margin-bottom: 15px;" data-mce-style="margin-bottom: 15px;"><!--{Url}--> <a href="{Url}" data-mce-href="{Url}">Click here reset password</a></p><p style="margin-bottom: 15px;" data-mce-style="margin-bottom: 15px;">This request was made from:</p><p style="margin-bottom: 15px;" data-mce-style="margin-bottom: 15px;">IP address: {IpAddress}</p><p style="margin-bottom: 15px;" data-mce-style="margin-bottom: 15px;">Thank you</p><p style="margin-bottom: 15px;" data-mce-style="margin-bottom: 15px;">Apexlink Team</p></div></td></tr>
                                <tr style="border-collapse: collapse;" data-mce-style="border-collapse: collapse;"><td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580" height="10" data-mce-style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;"></td></tr></tbody></table></td><td class="w30" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="30" data-mce-style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;"></td></tr></tbody></table></td></tr>
        <tr style="border-collapse: collapse;" data-mce-style="border-collapse: collapse;"><td class="w640" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="640" data-mce-style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;"><table id="footer" class="w640 mceItemTable" style="border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased; background-color: #00b0f0; color: #ffffff; width: 640px;" border="0" cellspacing="0" cellpadding="0" bgcolor="#00b0f0" data-mce-style="border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased; background-color: #00b0f0; color: #ffffff; width: 640px;"><tbody>
                    <tr style="border-collapse: collapse;" data-mce-style="border-collapse: collapse;"><td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" colspan="4" valign="top" width="360" data-mce-style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;"><p class="footer-content-left" style="-webkit-text-size-adjust: none; -ms-text-size-adjust: none; font-size: 13px; line-height: 15px; color: #ffffff; margin-top: 0px; margin-bottom: 15px; width: 630px;" align="center" data-mce-style="-webkit-text-size-adjust: none; -ms-text-size-adjust: none; font-size: 13px; line-height: 15px; color: #ffffff; margin-top: 0px; margin-bottom: 15px; width: 630px;">ApexLink Property Manager <span>&#x025CF;</span> <a style="color: #fff; text-decoration: none;" href="javascript:;" data-mce-href="javascript:;" data-mce-style="color: #fff; text-decoration: none;">support@apexlink.com; <span>&#x025CF;</span> 772-212-1950</a></p></td></tr></tbody></table></td></tr>
        <tr style="border-collapse: collapse;" data-mce-style="border-collapse: collapse;"><td class="w640" style="font-family:Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="640" height="60" data-mce-style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;"></td>
        </tr>
        </tbody>
    </table>
</div>','template_key' =>'forgetPasswordEmail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Rental Application', 'template_html' => '<div class="wrapper">
    <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="rentalApplication_Key">
    <table style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; background-color: #d6f0fd; color: #2d3091;  width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#d6f0fd" data-mce-style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; background-color: #d6f0fd; color: #2d3091; margin: auto; width: 640px;" class="mceItemTable">
        <tbody>
        <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;">
            <td></td>
        </tr>
        <tr style="border-collapse: collapse; background-color: #fff;" data-mce-style="border-collapse: collapse; background-color: #fff;">
            <td class="w580" style="font-family: "Helvetica Neue, Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse; width="580" data-mce-style="font-family: "Helvetica Neue, Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;"><center>
                <div style="margin-top: 1px; margin-bottom: 10px;" data-mce-style="margin-top: 1px; margin-bottom: 10px;"></div></center></td>
        </tr>
        <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;">
            <td></td>
        </tr>
        </tbody>
    </table>


    <table style="width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" data-mce-style="width: 640px;" class="mceItemTable">
        <tbody>
        <tr>
            <td style="border-left: 1px solid #C1DBE8; border-right: 1px solid #C1DBE8; padding: 10px 0;" valign="top" width="100%" data-mce-style="border-left: 1px solid #C1DBE8; border-right: 1px solid #C1DBE8; padding: 10px 0;">
                <h3 style="width: 100%; margin: 0 auto 20px auto; font-size: 16px; color: #303030; text-align: center; padding: 0;" data-mce-style="width: 100%; margin: 0 auto 20px auto; font-size: 16px; color: #303030; text-align: center; padding: 0;">
                    <span style="text-decoration: underline;" data-mce-style="text-decoration: underline;">ApexLink Notification: New Applicant</span>
                </h3>

                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>User Alert:</strong> {Subject}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">Dear {Username}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">A new rental application for {PropertyName} {BuildingName} has been generated, as follows:
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Rental Application ID:</strong> {RentalApplicationId}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Created On :</strong> {CreatedOn}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Applicant Name:</strong> {ApplicantName}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Property ID:</strong> {PropertyId}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Property Name:</strong> {PropertyName}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Building :</strong> {BuildingName}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Unit NO.:</strong> {UnitNumber}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Monthly Rent:</strong> {MonthlyRent}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Security Deposit:</strong> {SecurityDeposit}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">Please <a href="{Url}" data-mce-href="{Url}">
                        <strong><span style="text-decoration: underline;" data-mce-style="text-decoration: underline;">click here </span></strong></a>to view details.
                </p>
                <p>
                </p>
                <div style="width: 97%; margin-top: 30px; padding: 0 10px;" data-mce-style="width: 97%; margin-top: 30px; padding: 0 10px;">
                    <p></p>
                    <p style="margin-bottom: 5px;" data-mce-style="margin-bottom: 5px;">Thank You,
                    </p>
                    <p style="margin-bottom: 5px;" data-mce-style="margin-bottom: 5px;">{Signature}
                    </p>
                    <p>
                    </p>
                </div>
            </td>
        </tr>
        </tbody>
    </table>


    <table id="footer" style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased; background-color: #585858; color: #ffffff; margin: auto; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#0e867b" data-mce-style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased; background-color: #585858; color: #ffffff; margin: auto; width: 640px;" class="mceItemTable">
        <tbody>
        <tr>
            <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;" align="center" bgcolor="#05a0e4" data-mce-style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;">ApexLink Property Manager <span>&#x025CF;</span> <a style="color: #fff; text-decoration: none;" href="javascript:;" data-mce-href="javascript:;" data-mce-style="color: #fff; text-decoration: none;">support@apexlink.com <span>&#x025CF;</span> 772-212-1950 </a>
            </td>
        </tr>
        </tbody>
    </table>
</div>','template_key' =>'rentalApplication_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Lease Renewal', 'template_html' => '<div class="wrapper">
    <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="leaseRenewal_Key">
    <table style="box-shadow: #96999d 0px 0px 2px; border: 1px solid #dddddd; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" data-mce-style="box-shadow: #96999d 0px 0px 2px; border: 1px solid #dddddd; width: 640px;" class="mceItemTable">
        <tbody>
        <tr>
            <td style="background-color: #00b0f0;" data-mce-style="background-color: #00b0f0;">&nbsp;</td>
        </tr>
        <tr>
            <td style="padding: 20px 0;" align="center" data-mce-style="padding: 20px 0;">{CompanyLogo}</td>
        </tr>
        <tr>
            <td style="background-color: #00b0f0;" data-mce-style="background-color: #00b0f0;">&nbsp;</td>
        </tr>
        <tr>
            <td style="padding: 20px;" data-mce-style="padding: 20px;">

                <p>[Company_Name]</p>

                <p>[Company_Address]</p>

                <p>[Company_City], [Company_State], [Company_Zip]</p>

                <p>[Current_Date]</p>
                <hr>

                <p style="text-align: center;" align="right" data-mce-style="text-align: center;">
								<span style="font-size: 16px;" data-mce-style="font-size: 16px;">
									<strong>
										<span style="color: #002060;" data-mce-style="color: #002060;">RENEWAL OF LEASING AGREEMENT</span>
										<br> 
									</strong>
								</span>
                </p>
                <p style="text-align: justify;" data-mce-style="text-align: justify;">
                    <span style="font-size: 12px;" data-mce-style="font-size: 12px;">[Tenant_Name]</span>
                </p>
                <p style="text-align: justify;" data-mce-style="text-align: justify;">
                    <span style="font-size: 12px;" data-mce-style="font-size: 12px;">[Property_Address]</span>
                </p>
                <p style="text-align: justify;" data-mce-style="text-align: justify;">
                    <span style="font-size: 12px;" data-mce-style="font-size: 12px;">[Property_City], [Property_State], [Property_Zip]</span>
                </p>
                <p style="text-align: justify;" data-mce-style="text-align: justify;">
                    <span style="font-size: 12px;" data-mce-style="font-size: 12px;">Dear [Tenant_Name]:</span>
                </p>
                <p style="text-align: justify;" data-mce-style="text-align: justify;">
                    <span style="font-size: 12px;" data-mce-style="font-size: 12px;">Due to the near expiration of your current leasing agreement, we would like to extend you an opportunity to renew your lease.&nbsp; Please be aware that your current lease ends on [LeaseEndDate].&nbsp;</span>
                </p>
                <p style="text-align: justify;" data-mce-style="text-align: justify;">
                    <span style="font-size: 12px;" data-mce-style="font-size: 12px;">The renewal of your lease is for the following duration:</span>
                </p>
                <p style="text-align: justify;" data-mce-style="text-align: justify;">
                    <span style="font-size: 12px;" data-mce-style="font-size: 12px;">?______ Months&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ?_______ Years&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ? a Month to Month basis</span>
                </p>
                <p style="text-align: justify;" data-mce-style="text-align: justify;">
                    <span style="font-size: 12px;" data-mce-style="font-size: 12px;">If you desire to move forward with the renewal, all of the former terms and conditions will remain effective.&nbsp; Please forward the bottom of this notice so that a record of your decision is logged.&nbsp; If this notice is not returned, your lease will automatically be renewed.&nbsp;</span>
                </p>
                <p style="text-align: justify;" data-mce-style="text-align: justify;">
                    <span style="font-size: 12px;" data-mce-style="font-size: 12px;">If you have any concerns or questions, please free to contact me.&nbsp;</span>
                </p>
                <p style="text-align: justify;" data-mce-style="text-align: justify;">
                    <span style="font-size: 12px;" data-mce-style="font-size: 12px;">Sincerely,</span>
                </p>
                <p style="text-align: justify;" data-mce-style="text-align: justify;">
                    <span style="font-size: 12px;" data-mce-style="font-size: 12px;">&nbsp;[LandLord_Name]</span>
                </p>
                <p style="text-align: justify;" data-mce-style="text-align: justify;">
                    <span style="font-size: 12px;" data-mce-style="font-size: 12px;">_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</span>
                </p>
                <p style="text-align: justify;" data-mce-style="text-align: justify;">
                    <span style="font-size: 12px;" data-mce-style="font-size: 12px;">Please return this form with you desired selection.</span>
                </p>
                <p style="text-align: justify;" data-mce-style="text-align: justify;">
                    <span style="font-size: 12px;" data-mce-style="font-size: 12px;">?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; I would like to renew my lease as directed in the renewal letter.</span>
                </p>
                <p style="text-align: justify;" data-mce-style="text-align: justify;">
                    <span style="font-size: 12px;" data-mce-style="font-size: 12px;">?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; I do not want to renew my lease and I will leave the leased property at the end of</span>
                </p>
                <p style="text-align: justify;" data-mce-style="text-align: justify;">
                    <span style="font-size: 12px;" data-mce-style="font-size: 12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; the leasing term, which is [LeaseEndDate].&nbsp;&nbsp;</span>
                </p></td>
        </tr>
        <tr>
            <td style="background-color: #00b0f0; font-family: arial; text-align: center; color: #fff; padding: 20px 0;" data-mce-style="background-color: #00b0f0; font-family: arial; text-align: center; color: #fff; padding: 20px 0;">ApexLink Property Manager <span>&#x25BA;</span>&nbsp; support@apexlink.com <span>&#x025CF;</span> 772-212-1950</td>
        </tr>
        </tbody>
    </table>
</div>','template_key' =>'leaseRenewal_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Vendor Work Order Notification', 'template_html' => '<div class="wrapper">
    <p>{Usernameeee}{WorkOrderType}</p><!--<title>Apexlink</title>-->
    <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="vendorWorkOrderNotification_Key">
    <table style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; background-color: #d6f0fd; color: #2d3091; margin: auto; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#d6f0fd" data-mce-style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; background-color: #d6f0fd; color: #2d3091; margin: auto; width: 640px;" class="mceItemTable">
        <tbody>
        <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;"><td>&nbsp;
            </td>
        </tr>
        <tr style="border-collapse: collapse; background-color: #fff;" data-mce-style="border-collapse: collapse; background-color: #fff;">
            <td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580" data-mce-style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;">
                <center>
                    <div style="margin-top: 1px; margin-bottom: 10px;" data-mce-style="margin-top: 1px; margin-bottom: 10px;">
                        <p class="clsDefaultImage">{CompanyLogo}</p>
                    </div>
                </center>
            </td>
        </tr>
        <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;">
            <td>&nbsp;</td>
        </tr>
        </tbody>
    </table>



    <table style="width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" data-mce-style="width: 640px;" class="mceItemTable">
        <tbody>
        <tr>
            <td style="border-left: 1px solid #C1DBE8; border-right: 1px solid #C1DBE8; padding: 10px 0;" valign="top" width="100%" data-mce-style="border-left: 1px solid #C1DBE8; border-right: 1px solid #C1DBE8; padding: 10px 0;">
                <h3 style="width: 100%; margin: 0 auto 20px auto; font-size: 16px; color: #303030; text-align: center; padding: 0;" data-mce-style="width: 100%; margin: 0 auto 20px auto; font-size: 16px; color: #303030; text-align: center; padding: 0;">
								<span style="text-decoration: underline;" data-mce-style="text-decoration: underline;">Vendor Work Order Notification
								</span>
                </h3>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>User Alert:</strong> {Subject},
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">Dear {Username},
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">A new Work Order has been assigned to you.Please log into your Vendor Portal and view the work details of our Work Order request.
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Work Order ID :</strong> {WorkOrderId}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Work Order No. :</strong> {WorkOrderNumber}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Work Order Type :</strong> {WorkOrderType}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Date Of Work Order:</strong> {StartDate}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Expected Completion Date:</strong> {EndDate}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Actual Completion Date:</strong> { }
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Property Name:</strong> {PropertyName}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Property ID:</strong> {PropertyId}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Building Name:</strong> {Building}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Unit:</strong> {Unit}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Work Order Status:</strong> {Status}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">Please <a href="{Url}" data-mce-href="{Url}">
                        <strong>
								<span style="text-decoration: underline;" data-mce-style="text-decoration: underline;">click here 
								</span></strong></a>to log into your Vendor Portal.
                </p>
                <div style="width: 97%; margin-top: 20px; padding: 0 10px;" data-mce-style="width: 97%; margin-top: 20px; padding: 0 10px;">
                    <p>&nbsp;hkj</p>
                    <p style="margin-bottom: 5px;" data-mce-style="margin-bottom: 5px;">Thank You,
                    </p>
                    <p style="margin-bottom: 5px;" data-mce-style="margin-bottom: 5px;">{Signature}</p>
                    <p>&nbsp;</p>
                </div>
            </td>
        </tr>
        </tbody>
    </table>

    <table id="footer" style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased; background-color: #585858; color: #ffffff; margin: auto; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#0e867b" data-mce-style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased; background-color: #585858; color: #ffffff; margin: auto; width: 640px;" class="mceItemTable">
        <tbody>
        <tr>
            <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;" align="center" bgcolor="#05a0e4" data-mce-style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;">ApexLink Property Manager <span>&#x025CF;</span> <a style="color: #fff; text-decoration: none;" href="javascript:;" data-mce-href="javascript:;" data-mce-style="color: #fff; text-decoration: none;">support@apexlink.com <span>&#x025CF;</span> 772-212-1950 </a>
            </td>
        </tr>
        </tbody>
    </table>
</div>','template_key' =>'vendorWorkOrderNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Work Order Completion Notification', 'template_html' => '<div class="wrapper">
    <p>&nbsp;</p>

    <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="workOrderCompletionNotification_Key">
    <table style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; background-color: #d6f0fd; color: #2d3091; margin: auto; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#d6f0fd" data-mce-style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; background-color: #d6f0fd; color: #2d3091; margin: auto; width: 640px;" class="mceItemTable">
        <tbody>
        <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;">
            <td>&nbsp;</td>
        </tr>
        <tr style="border-collapse: collapse; background-color: #fff;" data-mce-style="border-collapse: collapse; background-color: #fff;">
            <td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580" data-mce-style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;">
                <center>
                    <div style="margin-top: 1px; margin-bottom: 10px;" data-mce-style="margin-top: 1px; margin-bottom: 10px;">{CompanyLogo}
                    </div>
                </center>
            </td>
        </tr>
        <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;">
            <td>&nbsp;</td>
        </tr>
        </tbody>
    </table>


    <table style="width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" data-mce-style="width: 640px;" class="mceItemTable">
        <tbody>
        <tr>
            <td style="border-left: 1px solid #C1DBE8; border-right: 1px solid #C1DBE8; padding: 10px 0;" valign="top" width="100%" data-mce-style="border-left: 1px solid #C1DBE8; border-right: 1px solid #C1DBE8; padding: 10px 0;">
                <h3 style="width: 100%; margin: 0 auto 20px auto; font-size: 16px; color: #303030; text-align: center; padding: 0;" data-mce-style="width: 100%; margin: 0 auto 20px auto; font-size: 16px; color: #303030; text-align: center; padding: 0;">
			    				<span style="text-decoration: underline;" data-mce-style="text-decoration: underline;">ApexLink Notification: Work Order Completed
			    				</span>
                </h3>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>User Alert:</strong> {Subject}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">Dear {Username}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">Work order {WorkOrderId} has been completed.
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Work Order No. :</strong> {WorkOrderNumber}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Work Order Type :</strong> {WorkOrderType}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Created On:</strong> {CreatedOn}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Completed On:</strong> {CompletedOn}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Start Date:</strong> {StartDate}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>End Date:</strong> {EndDate}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">Please <a href="{Url}" data-mce-href="{Url}">
                        <strong>
                            <span style="text-decoration: underline;" data-mce-style="text-decoration: underline;">click here </span></strong>
                    </a>to view details.
                </p>
                <p>&nbsp;</p>
                <div style="width: 97%; margin-top: 30px; padding: 0 10px;" data-mce-style="width: 97%; margin-top: 30px; padding: 0 10px;">
                    <p>&nbsp;</p>
                    <p style="margin-bottom: 5px;" data-mce-style="margin-bottom: 5px;">Thank You,</p>
                    <p style="margin-bottom: 5px;" data-mce-style="margin-bottom: 5px;">{Signature}</p>
                    <p>&nbsp;</p>
                </div>
            </td>
        </tr>
        </tbody>
    </table>


    <table id="footer" style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased; background-color: #585858; color: #ffffff; margin: auto; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#0e867b" data-mce-style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased; background-color: #585858; color: #ffffff; margin: auto; width: 640px;" class="mceItemTable">
        <tbody>
        <tr>
            <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;" align="center" bgcolor="#05a0e4" data-mce-style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;">ApexLink Property Manager<span>&#x025CF;</span>
                <a style="color: #fff; text-decoration: none;" href="javascript:;" data-mce-href="javascript:;" data-mce-style="color: #fff; text-decoration: none;">support@apexlink.com <span>&#x025CF;</span> 772-212-1950 </a>
            </td>
        </tr>
        </tbody>
    </table>
</div>','template_key' =>'workOrderCompletionNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Vendor Insurance Expiration Notification', 'template_html' => '<div class="wrapper">
    <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="VendorInsuranceExpirationNotification_Key">
    <table style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; background-color: #d6f0fd; color: #2d3091; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#d6f0fd" data-mce-style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; background-color: #d6f0fd; color: #2d3091;  width: 640px;" class="mceItemTable">
        <tbody>
        <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;">
            <td>&nbsp;</td>
        </tr>
        <tr style="border-collapse: collapse; background-color: #fff;" data-mce-style="border-collapse: collapse; background-color: #fff;">
            <td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580" data-mce-style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;">
                <center>
                    <div style="margin-top: 1px; margin-bottom: 10px;" data-mce-style="margin-top: 1px; margin-bottom: 10px;">{CompanyLogo}{Vendor}
                    </div>
                </center>
            </td>
        </tr>
        <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;">
            <td>&nbsp;</td>
        </tr>
        </tbody>
    </table>


    <table style="width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" data-mce-style="width: 640px;" class="mceItemTable">
        <tbody>
        <tr>
            <td style="border-left: 1px solid #C1DBE8; border-right: 1px solid #C1DBE8; padding: 10px 0;" valign="top" width="100%" data-mce-style="border-left: 1px solid #C1DBE8; border-right: 1px solid #C1DBE8; padding: 10px 0;">
                <h3 style="width: 100%; margin: 0 auto 20px auto; font-size: 16px; color: #303030; text-align: center; padding: 0;" data-mce-style="width: 100%; margin: 0 auto 20px auto; font-size: 16px; color: #303030; text-align: center; padding: 0;">
			    				<span style="text-decoration: underline;" data-mce-style="text-decoration: underline;">ApexLink Notification: Work Order Completed
			    				</span>
                </h3>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>User Alert:</strong> {Subject}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">Dear {Username}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">Work order {WorkOrderId} has been completed.
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Work Order No. :</strong> {WorkOrderNumber}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Work Order Type :</strong> {WorkOrderType}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Created On:</strong> {CreatedOn}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Completed On:</strong> {CompletedOn}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>Start Date:</strong> {StartDate}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">
                    <strong>End Date:</strong> {EndDate}
                </p>
                <p style="padding: 0 10px; margin-bottom: 5px;" data-mce-style="padding: 0 10px; margin-bottom: 5px;">Please <a href="{Url}" data-mce-href="{Url}">
                        <strong>
                            <span style="text-decoration: underline;" data-mce-style="text-decoration: underline;">click here </span></strong>
                    </a>to view details.
                </p>
                <p>&nbsp;</p>
                <div style="width: 97%; margin-top: 30px; padding: 0 10px;" data-mce-style="width: 97%; margin-top: 30px; padding: 0 10px;">
                    <p>&nbsp;</p>
                    <p style="margin-bottom: 5px;" data-mce-style="margin-bottom: 5px;">Thank You,</p>
                    <p style="margin-bottom: 5px;" data-mce-style="margin-bottom: 5px;">{Signature}</p>
                    <p>&nbsp;</p>
                </div>
            </td>
        </tr>
        </tbody>
    </table>


    <table id="footer" style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased; background-color: #585858; color: #ffffff; margin: auto; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#0e867b" data-mce-style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased; background-color: #585858; color: #ffffff; margin: auto; width: 640px;" class="mceItemTable">
        <tbody>
        <tr>
            <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;" align="center" bgcolor="#05a0e4" data-mce-style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;">ApexLink Property Manager <span>&#x025CF;</span>
                <a style="color: #fff; text-decoration: none;" href="javascript:;" data-mce-href="javascript:;" data-mce-style="color: #fff; text-decoration: none;">support@apexlink.com <span>&#x025CF;</span> 772-212-1950 </a>
            </td>
        </tr>
        </tbody>
    </table>
</div>','template_key' =>'VendorInsuranceExpirationNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Late Rent Payment Due', 'template_html' => '<div class="wrapper">
    <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="lateRentPaymentDue_Key">
    <table style="width: 640px; overflow: hidden; margin: 0 auto; border: 4px solid #00b0f0;" cellspacing="0" cellpadding="0" bgcolor="#00b0f0" data-mce-style="width: 640px; overflow: hidden; margin: 0 auto; border: 4px solid #00b0f0;" class="mceItemTable">
        <tbody>
        <tr>
            <td style="height: 20px; background: #00b0f0;" data-mce-style="height: 20px; background: #00b0f0;">&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold; background: #fff;" data-mce-style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold; background: #fff;">{CompanyName/Logo}
            </td>
        </tr>
        <tr>
            <td style="background-repeat: no-repeat; background-size: 100%;" data-mce-style="background-repeat: no-repeat; background-size: 100%;">
                <h5 style="font-family: arial; margin: 0; padding: 7px; font-size: 16px; text-align: center; color: #fff;" data-mce-style="font-family: arial; margin: 0; padding: 7px; font-size: 16px; text-align: center; color: #fff;">Late Rent Payment Due
                </h5>
            </td>
        </tr>
        <tr>
            <td style="padding: 20px; background: #fff;" data-mce-style="padding: 20px; background: #fff;">
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Date of Notice: {Date}</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Dear {TenantName},</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">{GuarantorName}</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">As of today, your {Month} rent payment is overdue by {OverDueDay} days. Hopefully, this is an oversight on your part.</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Please stop by the office immediately so that you can make your payment and not incur any late fees – or visit your Tenant Portal and make your payment today.</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">As a reminder, payments arriving {OverDueDay} days late incur a late fee of {LateFee}.</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">If you have any questions, contact your Property Manager Team at {ContactNumber} or {email}.</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Thank you.</p>
            </td>
        </tr>
        <tr>
            <td style="height: 30px; background: #00b0f0;" data-mce-style="height: 30px; background: #00b0f0;">&nbsp;</td>
        </tr>
        </tbody>
    </table>
</div>','template_key' =>'lateRentPaymentDue_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Tenant Monthly Rent Receipts', 'template_html' => '<div class="wrapper">
    <div style="float:left; width:100%;" data-mce-style="float: left; width: 100%;">
        <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="tenantMonthlyRentReceipts_Key">
        <table style="padding:10px 25px 25px 25px; width: 100%;" cellspacing="0" cellpadding="0" data-mce-style="padding: 10px 25px 25px 25px; width: 100%;" class="mceItemTable">
            <tbody>
            <tr>

                <td>{CompanyName/Logo}</td>
                <td style="font-family: arial; font-size: 14px;" align="right" data-mce-style="font-family: arial; font-size: 14px;">
                    <strong>{PropertyName}<br>{Address} <br>{CompanyCity}, {CompanyState} {CompanyZip} <br>{Phone} - <a title="" href="mailto:info@acl.com" data-mce-href="mailto:info@acl.com">{Email}</a>
                    </strong>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <h5 style="background: #00359e; font-family: arial; font-size: 16px; color: #fff; text-align: center; padding: 2px 10px;" data-mce-style="background: #00359e; font-family: arial; font-size: 16px; color: #fff; text-align: center; padding: 2px 10px;">TENANT INVOICE
                    </h5>{GuarantorName}
                    <table style="width: 100%;" cellspacing="0" cellpadding="0" data-mce-style="width: 100%;" class="mceItemTable">
                        <tbody>
                        <tr>
                            <td style="font-family: arial; font-weight: bold; font-size: 14px; padding: 2px 5px;" width="20%" data-mce-style="font-family: arial; font-weight: bold; font-size: 14px; padding: 2px 5px;">Tenant:</td>
                            <td style="font-family: arial; font-size: 14px; padding:2px 5px;" width="30%" data-mce-style="font-family: arial; font-size: 14px; padding: 2px 5px;">{TenantName}</td>
                            <td style="font-family: arial; font-weight: bold; font-size: 14px; padding:2px 5px;" width="20%" data-mce-style="font-family: arial; font-weight: bold; font-size: 14px; padding: 2px 5px;">Invoice Date:</td>
                            <td style="font-family: arial; font-size: 14px; padding:2px 5px;" width="30%" data-mce-style="font-family: arial; font-size: 14px; padding: 2px 5px;">{Date}</td></tr><tr>
                            <td style="font-family: arial; font-weight: bold; font-size: 14px; padding:2px 5px;" width="20%" data-mce-style="font-family: arial; font-weight: bold; font-size: 14px; padding: 2px 5px;">Address:</td>
                            <td style="font-family: arial; font-size: 14px; padding:2px 5px;" width="30%" data-mce-style="font-family: arial; font-size: 14px; padding: 2px 5px;">{PropertyAddress}</td>
                            <td style="font-family: arial; font-weight: bold; font-size: 14px; padding:2px 5px;" width="20%" data-mce-style="font-family: arial; font-weight: bold; font-size: 14px; padding: 2px 5px;">Invoice Number:</td>
                            <td style="font-family: arial; font-size: 14px; padding:2px 5px;" width="30%" data-mce-style="font-family: arial; font-size: 14px; padding: 2px 5px;">{RentReceiptNumber}</td></tr><tr>
                            <td style="font-family: arial; font-weight: bold; font-size: 14px; padding:2px 5px;" width="20%" data-mce-style="font-family: arial; font-weight: bold; font-size: 14px; padding: 2px 5px;">Building #</td>
                            <td style="font-family: arial; font-size: 14px; padding:2px 5px;" width="30%" data-mce-style="font-family: arial; font-size: 14px; padding: 2px 5px;">{BuildingName}</td>
                            <td style="font-family: arial; font-weight: bold; font-size: 14px; padding:2px 5px;" width="20%" data-mce-style="font-family: arial; font-weight: bold; font-size: 14px; padding: 2px 5px;">Due Date</td>
                            <td style="font-family: arial; font-size: 14px; padding:2px 5px;" width="30%" data-mce-style="font-family: arial; font-size: 14px; padding: 2px 5px;">{Date}</td></tr><tr>
                            <td style="font-family: arial; font-weight: bold; font-size: 14px; padding:2px 5px;" width="20%" data-mce-style="font-family: arial; font-weight: bold; font-size: 14px; padding: 2px 5px;">Unit #</td>
                            <td style="font-family: arial; font-size: 14px; padding:2px 5px;" width="30%" data-mce-style="font-family: arial; font-size: 14px; padding: 2px 5px;">{UnitName}</td></tr><tr>
                            <td style="font-family: arial; font-weight: bold; font-size: 14px; padding:2px 5px;" width="20%" data-mce-style="font-family: arial; font-weight: bold; font-size: 14px; padding: 2px 5px;">City, State Zip</td>
                            <td style="font-family: arial; font-size: 14px; padding:2px 5px;" width="30%" data-mce-style="font-family: arial; font-size: 14px; padding: 2px 5px;">{PropertyCity}, {PropertyState} {PropertyZip}</td>
                            <td style="font-family: arial; font-weight: bold; font-size: 14px; padding:2px 5px;" width="20%" data-mce-style="font-family: arial; font-weight: bold; font-size: 14px; padding: 2px 5px;">&nbsp;</td>
                            <td style="font-family: arial; font-size: 14px; padding:2px 5px;" width="30%" data-mce-style="font-family: arial; font-size: 14px; padding: 2px 5px;">&nbsp;</td>
                        </tr>
                        <tr></tr>
                        </tbody>
                    </table>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <h5 style="background: #00359e; font-family: arial; font-size: 16px; color: #fff; text-align: center; padding: 2px 10px; margin: 20px 0 0;" data-mce-style="background: #00359e; font-family: arial; font-size: 16px; color: #fff; text-align: center; padding: 2px 10px; margin: 20px     0 0;">CHARGES
                    </h5>
                    <table style="width: 100%;" cellspacing="0" cellpadding="0" data-mce-style="width: 100%;" class="mceItemTable">
                        <tbody>
                        <tr>
                            <th style="background: #e6f6fe; font-family: arial; font-size: 13px; padding:2px 7px; border: 1px solid #444;" width="20%" data-mce-style="background: #e6f6fe; font-family: arial; font-size: 13px; padding: 2px 7px; border: 1px solid #444;">Property
                            </th>
                            <th style="background: #e6f6fe; font-family: arial; font-size: 13px; padding:2px 7px; border: 1px solid #444; border-left: 0;" width="30%" data-mce-style="background: #e6f6fe; font-family: arial; font-size: 13px; padding: 2px 7px; border: 1px solid #444; border-left: 0;">Description
                            </th>
                            <th style="background: #e6f6fe; font-family: arial; font-size: 13px; padding:2px 7px; border: 1px solid #444; border-left: 0;" width="25%" data-mce-style="background: #e6f6fe; font-family: arial; font-size: 13px; padding: 2px 7px; border: 1px solid #444; border-left: 0;">Service Period
                            </th>
                            <th style="background: #e6f6fe; font-family: arial; font-size: 13px; padding:2px 7px; border: 1px solid #444; border-left: 0;" width="25%" data-mce-style="background: #e6f6fe; font-family: arial; font-size: 13px; padding: 2px 7px; border: 1px solid #444; border-left: 0;">Amount
                            </th>
                        </tr>
                        <tr>
                            <td>{ListRentReceipt}</td>
                        </tr>
                        <tr>
                            <td style="background: #fff; font-family: arial; font-size: 13px; padding:2px 7px; border: 0;" data-mce-style="background: #fff; font-family: arial; font-size: 13px; padding: 2px 7px; border: 0;">&nbsp;</td>
                            <td style="background: #fff; font-family: arial; font-size: 13px; padding:2px 7px; border: none;" data-mce-style="background: #fff; font-family: arial; font-size: 13px; padding: 2px 7px; border: none;">&nbsp;</td>
                            <td style="background: #fff; font-family: arial; font-size: 13px; padding:2px 7px; border: 0; border-right: 1px solid #444;" data-mce-style="background: #fff; font-family: arial; font-size: 13px; padding: 2px 7px; border: 0; border-right: 1px solid #444;">&nbsp;</td>
                            <td style="background: #e6f6fe; font-family: arial; font-size: 13px; padding:2px 7px; border: 1px solid #444; border-top: 0; border-left: 0; text-align: right;" data-mce-style="background: #e6f6fe; font-family: arial; font-size: 13px; padding: 2px 7px; border: 1px solid #444; border-top: 0; border-left: 0; text-align: right;">Total: &nbsp; &nbsp; {TotalAmount}</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="background: #fff; font-family: arial; font-size: 13px; padding:2px 7px; border: 0;" data-mce-style="background: #fff; font-family: arial; font-size: 13px; padding: 2px 7px; border: 0;">&nbsp;</td>
                            <td style="background: #fff; font-family: arial; font-size: 13px; padding:2px 7px; border: none;" data-mce-style="background: #fff; font-family: arial; font-size: 13px; padding: 2px 7px; border: none;">&nbsp;</td>
                            <td style="background: #fff; font-family: arial; font-size: 13px; padding:2px 7px; border: 1px solid #444; text-align:right;" data-mce-style="background: #fff; font-family: arial; font-size: 13px; padding: 2px 7px; border: 1px solid #444; text-align: right;">Tax:</td>
                            <td style="background: #e6f6fe; font-family: arial; font-size: 13px; padding:2px 7px; border: 1px solid #444; border-left: 0; text-align: right;" data-mce-style="background: #e6f6fe; font-family: arial; font-size: 13px; padding: 2px 7px; border: 1px solid #444; border-left: 0; text-align: right;">({Tax})</td>
                        </tr>
                        <tr>
                            <td style="background: #fff; font-family: arial; font-size: 13px; padding:2px 7px; border: 0;" data-mce-style="background: #fff; font-family: arial; font-size: 13px; padding: 2px 7px; border: 0;">&nbsp;</td>
                            <td style="background: #fff; font-family: arial; font-size: 13px; padding:2px 7px; border: none;" data-mce-style="background: #fff; font-family: arial; font-size: 13px; padding: 2px 7px; border: none;">&nbsp;</td>
                            <td style="background: #fff; font-family: arial; font-size: 13px; padding:2px 7px; border: 1px solid #444;  border-top: 0px;text-align:right;" data-mce-style="background: #fff; font-family: arial; font-size: 13px; padding: 2px 7px; border: 1px solid #444; border-top: 0px; text-align: right;">Total Amount:</td>
                            <td style="background: #e6f6fe; font-family: arial; font-size: 13px; padding:2px 7px; border: 1px solid #444; border-top: 0; border-left: 0; text-align: right;" data-mce-style="background: #e6f6fe; font-family: arial; font-size: 13px; padding: 2px 7px; border: 1px solid #444; border-top: 0; border-left: 0; text-align: right;">({TotalTaxAmount})</td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <h5 style="background: #00359e; font-family: arial; font-size: 16px; color: #fff; text-align: center; padding: 2px 10px; margin: 20px 0 0;" data-mce-style="background: #00359e; font-family: arial; font-size: 16px; color: #fff; text-align: center; padding: 2px 10px; margin: 20px     0 0;">CREDITS
                    </h5>
                    <table style="width: 100%;" cellspacing="0" cellpadding="0" data-mce-style="width: 100%;" class="mceItemTable"><tbody>
                        <tr>
                            <th style="background: #e6f6fe; font-family: arial; font-size: 13px; padding:2px  7px; border: 1px solid #444;" width="20%" data-mce-style="background: #e6f6fe; font-family: arial; font-size: 13px; padding: 2px  7px; border: 1px solid #444;">Property</th>
                            <th style="background: #e6f6fe; font-family: arial; font-size: 13px; padding:2px 7px; border: 1px solid #444; border-left: 0;" width="30%" data-mce-style="background: #e6f6fe; font-family: arial; font-size: 13px; padding: 2px 7px; border: 1px solid #444; border-left: 0;">Description</th>
                            <th style="background: #e6f6fe; font-family: arial; font-size: 13px; padding:2px 7px; border: 1px solid #444; border-left: 0;" width="25%" data-mce-style="background: #e6f6fe; font-family: arial; font-size: 13px; padding: 2px 7px; border: 1px solid #444; border-left: 0;">Service Period</th>
                            <th style="background: #e6f6fe; font-family: arial; font-size: 13px; padding:2px 7px; border: 1px solid #444; border-left: 0;" width="25%" data-mce-style="background: #e6f6fe; font-family: arial; font-size: 13px; padding: 2px 7px; border: 1px solid #444; border-left: 0;">Amount</th>
                        </tr>
                        <tr>
                            <td>{ListRentReceiptT}</td></tr><tr>
                            <td style="background: #fff; font-family: arial; font-size: 13px; padding:2px 7px; border: 0;" data-mce-style="background: #fff; font-family: arial; font-size: 13px; padding: 2px 7px; border: 0;">&nbsp;</td>
                            <td style="background: #fff; font-family: arial; font-size: 13px; padding:2px 7px; border: none;" data-mce-style="background: #fff; font-family: arial; font-size: 13px; padding: 2px 7px; border: none;">&nbsp;</td>
                            <td style="background: #fff; font-family: arial; font-size: 13px; padding:2px 7px; border: 0; border-right: 1px solid #444;" data-mce-style="background: #fff; font-family: arial; font-size: 13px; padding: 2px 7px; border: 0; border-right: 1px solid #444;">&nbsp;</td>
                            <td style="background: #e6f6fe; font-family: arial; font-size: 13px; padding:2px 7px; border: 1px solid #444; border-top: 0; border-left: 0; text-align: right;" data-mce-style="background: #e6f6fe; font-family: arial; font-size: 13px; padding: 2px 7px; border: 1px solid #444; border-top: 0; border-left: 0; text-align: right;">Total: &nbsp; &nbsp; {TotalAmount}</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="background: #fff; font-family: arial; font-size: 13px; padding:2px 7px; border: 0px solid #444;" data-mce-style="background: #fff; font-family: arial; font-size: 13px; padding: 2px 7px; border: 0px solid #444;">&nbsp;</td>
                            <td style="background: #fff; font-family: arial; font-size: 13px; padding:2px 7px; border: 0px solid #444;" data-mce-style="background: #fff; font-family: arial; font-size: 13px; padding: 2px 7px; border: 0px solid #444;">&nbsp;</td>
                            <td style="background: #fff; font-family: arial; font-size: 13px; padding:2px 7px; border: 1px solid #444;text-align:right;" data-mce-style="background: #fff; font-family: arial; font-size: 13px; padding: 2px 7px; border: 1px solid #444; text-align: right;">Total Paid Amount:</td>
                            <td style="background: #e6f6fe; font-family: arial; font-size: 13px; padding:2px 7px; border: 1px solid #444; border-left: 0; text-align: right;" data-mce-style="background: #e6f6fe; font-family: arial; font-size: 13px; padding: 2px 7px; border: 1px solid #444; border-left: 0; text-align: right;">({TotalPaidAmount})</td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <h5 style="background: #00359e; font-family: arial; font-size: 16px; color: #fff; text-align: center; padding: 2px 10px; margin: 20px 0 0;" data-mce-style="background: #00359e; font-family: arial; font-size: 16px; color: #fff; text-align: center; padding: 2px 10px; margin: 20px     0 0;">TOTAL DUE
                    </h5>
                    <table style="width: 100%;" cellspacing="0" cellpadding="0" data-mce-style="width: 100%;" class="mceItemTable">
                        <tbody>
                        <tr>
                            <td style="background: #e6f6fe; font-family: arial; font-size: 15px; font-weight:bold; padding:3px 7px; border: 1px solid #444;     text-align: right; font-weight: 600;" colspan="4" data-mce-style="background: #e6f6fe; font-family: arial; font-size: 15px; font-weight: 600; padding: 3px 7px; border: 1px solid #444; text-align: right;">{BalanceAmount}</td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2"><hr style="border: 1px dashed #444; height: 1px; margin-top: 20px;" data-mce-style="border: 1px dashed #444; height: 1px; margin-top: 20px;"></td></tr><tr>
                <td style="font-family: arial; font-size: 14px; text-align: left; font-style: italic;" data-mce-style="font-family: arial; font-size: 14px; text-align: left; font-style: italic;">Please detach and send coupon with payment. Thank you! <br></td>

                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="font-family: arial; font-size: 15px;" data-mce-style="font-family: arial; font-size: 15px;">Invoice Number : {RentReceiptNumber}<br>{TenantName}<br>{PropertyAddress}<br> {PropertyCity}, {PropertyState} {PropertyZip}</td>

                <td>
                    <table style="border: 1px solid #444444; float: right; max-width: 400px; padding: 10px; width: 100%;" data-mce-style="border: 1px solid #444444; float: right; max-width: 400px; padding: 10px; width: 100%;" class="mceItemTable">
                        <tbody>
                        <tr>
                            <td style="font-family: arial; font-size: 14px;" data-mce-style="font-family: arial; font-size: 14px;">Amount Due:</td>
                            <td style="font-family: arial; font-size: 14px;" align="right" data-mce-style="font-family: arial; font-size: 14px;">{BalanceAmount}</td></tr><tr>
                            <td style="font-family: arial; font-size: 14px;" data-mce-style="font-family: arial; font-size: 14px;">Due Date:</td>
                            <td style="font-family: arial; font-size: 14px;" align="right" data-mce-style="font-family: arial; font-size: 14px;">{Date}</td></tr><tr>
                            <td style="font-family: arial; font-size: 14px;" data-mce-style="font-family: arial; font-size: 14px;">Enter Amount of Payment Enclosed:</td>
                            <td style="border:1px solid;" data-mce-style="border: 1px solid;">{TotalEnclosedAmount}</td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>{PaidImage}</td>
                <td style="max-width: 215px; font-family: arial; font-size: 16px; float: right;" data-mce-style="max-width: 215px; font-family: arial; font-size: 16px; float: right;"><br><br>{PropertyName}<br>{Address}<br> {CompanyCity}, {CompanyState} {CompanyZip}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>','template_key' =>'tenantMonthlyRentReceipts_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Tenant Give Notice', 'template_html' => '<div class="wrapper">
    <p>&nbsp;</p>
    <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="tenantGiveNotice_Key">
    <table style="width: 640px; overflow: hidden; margin: 0 auto; border: 4px solid #00b0f0;" cellspacing="0" cellpadding="0" bgcolor="#00b0f0" data-mce-style="width: 640px; overflow: hidden; margin: 0 auto; border: 4px solid #00b0f0;" class="mceItemTable">
        <tbody>
        <tr>
            <td style="height: 20px; background: #00b0f0;" data-mce-style="height: 20px; background: #00b0f0;">&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold; background: #fff;" data-mce-style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold; background: #fff;"><!--{CompanyLogo}-->
                <h5>30-Day Notice Notification</h5>
            </td>
        </tr>
        <tr>
            <td style="background-repeat: no-repeat; background-size: 100%;" data-mce-style="background-repeat: no-repeat; background-size: 100%;">&nbsp;</td>
        </tr>
        <tr>
            <td style="padding: 20px; background: #fff;" data-mce-style="padding: 20px; background: #fff;"><!--<p style="font-family: arial;">Date of Notice: {Monthdateyear}</p>-->
                <p style="font-family: arial;" data-mce-style="font-family: arial;">To {PropertyManager},</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">The following resident has provided their 30-day notice to end their lease:</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Resident: {ResidentName}</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Address: {PropertyName},{Address},{UnitName}</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Notice Date: {NoticeDate}
                </p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Scheduled Move-Out Date: {MoveOutDate}
                </p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Reason for Notice: {ReasonNotice}
                </p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Resident Contact Information: {Phone},{Email}
                </p>
            </td>
        </tr>
        <tr>
            <td style="height: 30px; background: #00b0f0;" data-mce-style="height: 30px; background: #00b0f0;">&nbsp;
            </td>
        </tr>
        </tbody>
    </table>
</div>','template_key' =>'tenantGiveNotice_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Complaint By Tenant', 'template_html' => '<div class="wrapper">
    <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="complaintByTenant_Key">
    <table style="margin: auto; width: 640px; background:#fff;margin-bottom :30px ;border:2px solid #00b0f0" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#fff" data-mce-style="margin: auto; width: 640px; background: #fff; margin-bottom: 30px; border: 2px solid #00b0f0;" class="mceItemTable">
        <tbody>
        <tr>
            <td>
                <table style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; color: #2d3091; margin: auto; width: 100%;" border="0" cellspacing="0" cellpadding="0" align="center" data-mce-style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; color: #2d3091; margin: auto; width: 100%;" class="mceItemTable">
                    <tbody>
                    <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;">
                        <td>&nbsp;</td>
                    </tr>
                    <tr style="border-collapse: collapse; background-color: #fff;" data-mce-style="border-collapse: collapse; background-color: #fff;">
                        <td class="w580" style="font-family: " width="580" data-mce-style="">
                            <center>
                                <div style="margin-top: 1px; margin-bottom: 10px;" data-mce-style="margin-top: 1px; margin-bottom: 10px;">
                                    <p class="clsDefaultImage">{CompanyLogo}</p>
                                </div>
                            </center>
                        </td>
                    </tr>
                    <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;">
                        <td>&nbsp;</td>
                    </tr>
                    </tbody>
                </table>
                <table style="width: 640px; padding:10px 20px; background:#fff;" border="0" cellspacing="0" cellpadding="0" align="center" data-mce-style="width: 640px; padding: 10px 20px; background: #fff;" class="mceItemTable">
                    <tbody>
                    <tr>
                        <td style="padding: 10px 0;" valign="top" width="100%" data-mce-style="padding: 10px 0;">
                            <h3 style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0;
	                                text-align: center; padding: 0; font-family: " data-mce-style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0; text-align: center; padding: 0;">Complaint By Tenant</h3>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-family: " data-mce-style="">The following are the complaint(s) our office received from this tenant <br> <br></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="padding:0px 20px 50px 20px;" width="100%" cellspacing="0" cellpadding="0" data-mce-style="padding: 0px 20px 50px 20px;" class="mceItemTable">
                                <tbody>
                                <tr>
                                    <td style="border: 1px solid #444;text-align:center; font-family: " width="30%" data-mce-style="border: 1px solid #444; text-align: center;">Tenant Name:</td>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">{TenantName}</td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid #444; text-align:center;font-family: " data-mce-style="border: 1px solid #444; text-align: center;">Address:</td>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">{Address1}<br> {Address2}<br> {Address3}<br> {CityName},{StateName} {Zip}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid #444; text-align:center;font-family: " data-mce-style="border: 1px solid #444; text-align: center;">Complaint Type:
                                    </td>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">{ComplaintType}</td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid #444; text-align:center;font-family: " data-mce-style="border: 1px solid #444; text-align: center;">Description:</td>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">{Description} <br> <br> <br> <br> <br> <br>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table id="footer" style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px;
	                -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased;
	                background-color: #585858; color: #ffffff; margin: auto; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#0e867b" data-mce-style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased; background-color: #585858; color: #ffffff; margin: auto; width: 640px;" class="mceItemTable">
                    <tbody>
                    <tr>
                        <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: " align="center" bgcolor="#05a0e4" data-mce-style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;">ApexLink. <a style="color: #fff; text-decoration: none;" href="javascript:;" data-mce-href="javascript:;" data-mce-style="color: #fff; text-decoration: none;"> {Email}.{Phone} </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>','template_key' =>'complaintByTenant_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Complaint About Tenant', 'template_html' => '		<div class="wrapper">
    <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="complaintAboutTenant_Key">
    <table style="margin: auto; width: 640px; background:#fff;margin-bottom :30px ;border:2px solid #00b0f0" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#fff" data-mce-style="margin: auto; width: 640px; background: #fff; margin-bottom: 30px; border: 2px solid #00b0f0;" class="mceItemTable">
        <tbody>
        <tr>
            <td>
                <table style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; color: #2d3091; margin: auto; width: 100%;" border="0" cellspacing="0" cellpadding="0" align="center" data-mce-style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; color: #2d3091; margin: auto; width: 100%;" class="mceItemTable">
                    <tbody>
                    <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;">
                        <td>&nbsp;</td>
                    </tr>
                    <tr style="border-collapse: collapse; background-color: #fff;" data-mce-style="border-collapse: collapse; background-color: #fff;">
                        <td class="w580" style="font-family: " width="580" data-mce-style="">
                            <center>
                                <div style="margin-top: 1px; margin-bottom: 10px;" data-mce-style="margin-top: 1px; margin-bottom: 10px;">
                                    <p class="clsDefaultImage">{CompanyLogo}</p>
                                </div>
                            </center>
                        </td>
                    </tr>
                    <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;">
                        <td>&nbsp;</td>
                    </tr>
                    </tbody>
                </table>
                <table style="width: 640px; padding:10px 20px; background:#fff;" border="0" cellspacing="0" cellpadding="0" align="center" data-mce-style="width: 640px; padding: 10px 20px; background: #fff;" class="mceItemTable">
                    <tbody>
                    <tr>
                        <td style="padding: 10px 0;" valign="top" width="100%" data-mce-style="padding: 10px 0;">
                            <h3 style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0;
	                                text-align: center; padding: 0; font-family: " data-mce-style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0; text-align: center; padding: 0;">Complaint about Tenant</h3>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-family: " data-mce-style="">The following are the complaint(s) our office received from this tenant <br> <br></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="padding:0px 20px 50px 20px;" width="100%" cellspacing="0" cellpadding="0" data-mce-style="padding: 0px 20px 50px 20px;" class="mceItemTable">
                                <tbody>
                                <tr>
                                    <td style="border: 1px solid #444;text-align:center; font-family: " width="30%" data-mce-style="border: 1px solid #444; text-align: center;">Tenant Name:</td>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">{TenantName}</td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid #444; text-align:center;font-family: " data-mce-style="border: 1px solid #444; text-align: center;">Address:</td>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">{Address1}<br> {Address2}<br> {Address3}<br> {CityName},{StateName} {Zip}</td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid #444; text-align:center;font-family: " data-mce-style="border: 1px solid #444; text-align: center;">Complaint Type:</td>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">{ComplaintType}</td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid #444; text-align:center;font-family: " data-mce-style="border: 1px solid #444; text-align: center;">Description:</td>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">{Description} <br> <br> <br> <br> <br> <br>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table id="footer" style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px;
	                -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased;
	                background-color: #585858; color: #ffffff; margin: auto; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#0e867b" data-mce-style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased; background-color: #585858; color: #ffffff; margin: auto; width: 640px;" class="mceItemTable">
                    <tbody>
                    <tr>
                        <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: " align="center" bgcolor="#05a0e4" data-mce-style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;">ApexLink. <a style="color: #fff; text-decoration: none;" href="javascript:;" data-mce-href="javascript:;" data-mce-style="color: #fff; text-decoration: none;"> {Email}.{Phone} </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>','template_key' =>'complaintAboutTenant_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Lease Expiry Soon', 'template_html' => '<div class="wrapper">
    <p>&nbsp;</p>
    <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="leaseExpirySoon_Key">
    <table style="width: 640px; overflow: hidden; margin: 0 auto; border: 4px solid #00b0f0;" cellspacing="0" cellpadding="0" bgcolor="#00b0f0" data-mce-style="width: 640px; overflow: hidden; margin: 0 auto; border: 4px solid #00b0f0;" class="mceItemTable">
        <tbody>
        <tr>
            <td style="height: 20px; background: #00b0f0;" data-mce-style="height: 20px; background: #00b0f0;">&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold; background: #fff;" data-mce-style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold; background: #fff;">{CompanyLogo}</td>
        </tr>
        <tr>
            <td style="background-repeat: no-repeat; background-size: 100%;" data-mce-style="background-repeat: no-repeat; background-size: 100%;">&nbsp;</td>
        </tr>
        <tr>
            <td style="padding: 20px; background: #fff;" data-mce-style="padding: 20px; background: #fff;">
                <h3 style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0;text-align: center; adding: 0; font-family: " data-mce-style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0; text-align: center; padding: 0;">Your Lease is Expiring Soon
                </h3>

                <p style="font-family: arial;" data-mce-style="font-family: arial;">Dear {TenantName},</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">This note is to let you know that {Days} days from today, on {Date} your lease will be up. We appreciate your business and hope that you are planning to stay with us and renew your lease for:</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">{Address}</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">{City},{State},{ZipCode}</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">If you plan to renew, please stop by the {CompanyName} office or contact us at {CompanyPhoneNumber} or {CompanyEmail} so that we can prepare your new lease for signing.</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;"><a href="javascript:;" data-mce-href="javascript:;">If, unfortunately, you have decided not to renew your lease for another term, please notify us by {Date} which is the required timeline in accordance with your lease agreement.</a></p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;"><a href="javascript:;" data-mce-href="javascript:;">We look forward to hearing from you,</a></p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">
                    <strong>Your Property Management Team</strong>
                </p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">{CompanyName}</p>
                <hr>
                <p style="font-family: arial;font-size:10px;" data-mce-style="font-family: arial; font-size: 10px;">
                    <strong>You will received this email from {CompanyName}, because you will registered a lost item, either through your portal, or by contacting staff directly.</strong>
                </p>
            </td>
        </tr>
        <tr>
            <td style="height: 30px; background: #00b0f0;" data-mce-style="height: 30px; background: #00b0f0;">&nbsp;</td>
        </tr>
        </tbody>
    </table>
</div>','template_key' =>'leaseExpirySoon_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Happy Birthday', 'template_html' => '<div class="wrapper">
    <p>&nbsp;</p>
    <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="happyBirthday_Key">
    <table style="width: 640px; overflow: hidden; margin: 0 auto; border: 4px solid #00b0f0;" cellspacing="0" cellpadding="0" bgcolor="#00b0f0" data-mce-style="width: 640px; overflow: hidden; margin: 0 auto; border: 4px solid #00b0f0;" class="mceItemTable">
        <tbody>
        <tr>
            <td style="height: 20px; background: #00b0f0;" data-mce-style="height: 20px; background: #00b0f0;">&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold; background: #fff;" data-mce-style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold; background: #fff;">{CompanyLogo}</td></tr>
        <tr>
            <td style="background-repeat: no-repeat; background-size: 100%;" data-mce-style="background-repeat: no-repeat; background-size: 100%;">&nbsp;</td></tr>
        <tr>
            <td style="padding: 20px; background: #fff;" data-mce-style="padding: 20px; background: #fff;">{BirthdayMailImg}
                <h3 style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0;
	                                text-align: center; padding: 0; font-family: Calibri;" data-mce-style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0; text-align: center; padding: 0; font-family: Calibri;">Happy Birthday, {TenantName}!
                </h3>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">The staff at {CompanyName} is pleased to wish you the happiest of birthdays!</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">Hope it’s a great day,</p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">
                    <strong>Your Property Management Team</strong>
                </p>
                <p style="font-family: arial;" data-mce-style="font-family: arial;">({CompanyName})</p>
                <hr>
                <p style="font-family: arial;font-size:10px;" data-mce-style="font-family: arial; font-size: 10px;">
                    <strong>You will received this email from {CompanyName}, because you will registered a lost item, either through your portal, or by contacting staff directly.</strong>
                </p>
            </td>
        </tr>
        <tr>
            <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;" align="center" bgcolor="#05a0e4" data-mce-style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;">{CompanyName} . <a style="color: #fff; text-decoration: none;" href="javascript:;" data-mce-href="javascript:;" data-mce-style="color: #fff; text-decoration: none;"> www.apexlink.com . {CompanyPhone} </a>
            </td>
        </tr>
        </tbody>
    </table>
</div>','template_key' =>'happyBirthday_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Vendor Complaint', 'template_html' => '<div class="wrapper">
    <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="vendorComplaint_Key">
    <table style="margin: auto; width: 640px; margin-bottom:30px ;border:2px solid #00b0f0" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#fff" data-mce-style="margin: auto; width: 640px; margin-bottom: 30px; border: 2px solid #00b0f0;" class="mceItemTable">
        <tbody>
        <tr>
            <td>
                <table style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased;color: #2d3091; margin: auto; width: 100%;" border="0" cellspacing="0" cellpadding="0" align="center" data-mce-style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; color: #2d3091; margin: auto; width: 100%;" class="mceItemTable">
                    <tbody>
                    <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;">
                        <td>&nbsp;</td>
                    </tr>
                    <tr style="border-collapse: collapse; background-color: #fff;" data-mce-style="border-collapse: collapse; background-color: #fff;">
                        <td class="w580" style="background-color: #fff; font-family: " width="580" data-mce-style="background-color: #fff;">
                            <center>
                                <div style="margin-top: 1px; margin-bottom: 10px;" data-mce-style="margin-top: 1px; margin-bottom: 10px;">
                                    <p class="clsDefaultImage">{CompanyLogo}</p>
                                </div>
                            </center>
                        </td>
                    </tr>
                    <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;">
                        <td>&nbsp;</td>
                    </tr>
                    </tbody>
                </table>
                <table style="width: 640px; padding:10px 20px;background:#fff;" border="0" cellspacing="0" cellpadding="0" align="center" data-mce-style="width: 640px; padding: 10px 20px; background: #fff;" class="mceItemTable">
                    <tbody>
                    <tr>
                        <td style="padding: 10px 0;" valign="top" width="100%" data-mce-style="padding: 10px 0;">
                            <h3 style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0;    text-align: center; padding: 0; font-family: " data-mce-style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0; text-align: center; padding: 0;">{Complaint}
                            </h3>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-family: " data-mce-style=""><br> <br></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="padding:0px 20px 50px 20px;" width="100%" cellspacing="0" cellpadding="0" data-mce-style="padding: 0px 20px 50px 20px;" class="mceItemTable">
                                <tbody>
                                <tr>
                                    <td style="border: 1px solid #444; font-family: " width="30%" data-mce-style="border: 1px solid #444;">Vendor Name:</td>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">{VendorName}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">Address:</td>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">{Address1}<br> {Address2}<br> {Address3}<br> {CityName},{StateName} {Zip}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">Complaint Type:</td>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">{ComplaintType}</td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">Description:</td>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">{Description} <br> <br> <br> <br> <br> <br>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table id="footer" style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px;
	                -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased;
	                background-color: #585858; color: #ffffff; margin: auto; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#0e867b" data-mce-style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased; background-color: #585858; color: #ffffff; margin: auto; width: 640px;" class="mceItemTable">
                    <tbody>
                    <tr>
                        <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: " align="center" bgcolor="#05a0e4" data-mce-style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;">
                            <a style="color: #fff; text-decoration: none;" href="javascript:;" data-mce-href="javascript:;" data-mce-style="color: #fff; text-decoration: none;"> {PmCompanyName}.{PmCompanyEmail}.{PhoneNumber} </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>','template_key' =>'vendorComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Owner Complaint', 'template_html' => '<div class="wrapper">
    <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="ownerComplaint_Key">
    {Complaint}<table style="margin: auto; width: 640px; margin-bottom:30px ;border:2px solid #00b0f0" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#fff" data-mce-style="margin: auto; width: 640px; margin-bottom: 30px; border: 2px solid #00b0f0;" class="mceItemTable">
        <tbody>
        <tr>
            <td>
                <table style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; background-color: #d6f0fd; color: #2d3091; margin: auto; width: 100%;" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#d6f0fd" data-mce-style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; background-color: #d6f0fd; color: #2d3091; margin: auto; width: 100%;" class="mceItemTable">
                    <tbody>
                    <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;">
                        <td>&nbsp;</td>
                    </tr>
                    <tr style="border-collapse: collapse; background-color: #fff;" data-mce-style="border-collapse: collapse; background-color: #fff;">
                        <td class="w580" style="font-family: " width="580" data-mce-style="">
                            <center>
                                <div style="margin-top: 1px; margin-bottom: 10px;" data-mce-style="margin-top: 1px; margin-bottom: 10px;"><p class="clsDefaultImage">{CompanyLogo}</p>
                                </div>
                            </center>
                        </td>
                    </tr>
                    <tr style="background-color: #00b0f0; height: 20px;" data-mce-style="background-color: #00b0f0; height: 20px;">
                        <td>&nbsp;</td>
                    </tr>
                    </tbody>
                </table>
                <table style="width: 640px; padding:10px 20px;background:#fff;" border="0" cellspacing="0" cellpadding="0" align="center" data-mce-style="width: 640px; padding: 10px 20px; background: #fff;" class="mceItemTable">
                    <tbody>
                    <tr>
                        <td style="padding: 10px 0;" valign="top" width="100%" data-mce-style="padding: 10px 0;">
                            <h3 style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0;text-align: center; padding: 0; font-family: " data-mce-style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0; text-align: center; padding: 0;">{Complaint}</h3>
                        </td>
                    </tr>
                    <tr>
                        <td><p style="font-family: " data-mce-style=""><br> <br></p></td>
                    </tr>
                    <tr>
                        <td>
                            <table style="padding:0px 20px 50px 20px;" width="100%" cellspacing="0" cellpadding="0" data-mce-style="padding: 0px 20px 50px 20px;" class="mceItemTable">
                                <tbody>
                                <tr>
                                    <td style="border: 1px solid #444; font-family: " width="30%" data-mce-style="border: 1px solid #444;">Owner Name:</td>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">{OwnerName}</td></tr>
                                <tr>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">Address:</td>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">{Address1}<br> {Address2}<br> {Address3}<br> {CityName},{StateName} {Zip}</td>
                                </tr>
                                <tr>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">Complaint Type:</td>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">{ComplaintType}</td></tr>
                                <tr>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">Description:</td>
                                    <td style="border: 1px solid #444; font-family: " data-mce-style="border: 1px solid #444;">{Description} <br> <br> <br> <br> <br> <br>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table id="footer" style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px;
			                -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased;
			                background-color: #585858; color: #ffffff; margin: auto; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#0e867b" data-mce-style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased; background-color: #585858; color: #ffffff; margin: auto; width: 640px;" class="mceItemTable">
                    <tbody>
                    <tr>
                        <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: " align="center" bgcolor="#05a0e4" data-mce-style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;">
                            <a style="color: #fff; text-decoration: none;" href="javascript:;" data-mce-href="javascript:;" data-mce-style="color: #fff; text-decoration: none;"> {PmCompanyName}.{PmCompanyEmail}.{PhoneNumber}
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>','template_key' =>'ownerComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'HOA Violation', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="HoaViolation_Key">
<table style="margin: auto; width: 640px; margin-bottom:30px;background:#fff ;border:2px solid #00b0f0" border="0" cellspacing="0" cellpadding="0" align="center"  bgcolor="#fff">
    <tr>
        <td>
            <table style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased;color: #2d3091; margin: auto; width: 100%;" border="0"
                cellspacing="0" cellpadding="0" align="center">
                <tbody>
                    <tr style="background-color: #00b0f0; height: 20px;">
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="border-collapse: collapse; background-color: #fff;">
                        <td class="w580" style="font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580">
                            <!-- <div align="center" id="headline">
                                                            <p style="color: #ffffff; font-family: "Lucida Grande", "Lucida Sans Unicode", Verdana, sans-serif;
                                                                font-size: 36px; text-align: center; margin-top: -40px; margin-bottom: 30px;">-->
                            <!-- http://Apexlink.seasiaconsulting.com-->
                            <center>
                                <div style="margin-top: 1px; margin-bottom: 10px;">
                                    <p class="clsDefaultImage">
                                        {CompanyLogo}</p>
                                </div>
                            </center>
                            <!--  </p>
                                                        </div>-->
                        </td>
                    </tr>
                    <tr style="background-color: #00b0f0; height: 20px;">
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </tbody>
            </table>
            <table style="width: 640px; padding:10px 20px;background:#fff;" border="0" cellspacing="0" cellpadding="0" align="center">
                <tbody>
                    <tr>
                        <td style="padding: 10px 0;"
                            valign="top" width="100%">
                            <h3 style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0;
                                text-align: center; padding: 0; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;">Tenant HOA Violation(s)</h3>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size: 15px;">
                              The following are the HOA violation(s) we have on file for this tenant.
                                <br />
                                <br />
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0" style="padding:0px 20px 50px 20px;">
                                <tr>
                                    <td width="30%" style="border: 1px solid #444; text-align: center;font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;height:30px;">
                                        Tenant Name:
                                    </td>
                                    <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                        {TenantName}
                                    </td>
                                </tr>
                                  <tr>
                                    <td style="border: 1px solid #444; text-align: center;font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;text-align: center;">			
                                       Address:										
                                    </td>											
                                     <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">			
                                        {Address1}<br/>
										{Address2}<br/>
										{Address3}<br/>
										{CityName},{StateName} {Zip}									
                                    </td>							
                                										
                                </tr>												
                                <tr>												
                                    <td style="border: 1px solid #444; text-align: center;font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;text-align: center;">		
                                        Violation Type(s)						
                                    </td>										
                                    <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                        {ViolationType}
                                    </td>
                                </tr>
                              
                                <tr>
                                    <td style="border: 1px solid #444; text-align: center;font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;text-align: center;">
                                        Description:
                                    </td>
                                    <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                        {Description}
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                       
                            </table>
                        </td>
                    </tr>
                
                </tbody>
            </table>
                <br />
                    <br />
            <table id="footer" style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px;
                -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased;
                background-color: #585858; color: #ffffff; margin: auto; width: 640px;" border="0"
                cellspacing="0" cellpadding="0" align="center" bgcolor="#0e867b">
                <tbody>
                    <tr>
                        <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px;" align="center"
                            bgcolor="#05a0e4">
                           <a style="color: #fff; text-decoration: none;" href="javascript:;">
                                {PmCompanyName}.{PmCompanyEmail}.{PhoneNumber} </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>

</div>','template_key' =>'HoaViolation_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

array('template_title' => 'Lease Agreement', 'template_html' => '<div class="wrapper">
    <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="LeaseAgreement_Key">

    <table style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; background-color: #d6f0fd; color: #2d3091; margin: auto; font-family: arial; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#d6f0fd">
    <tbody>
    <tr>
    <td style="background-color: #00b0f0;">&nbsp;</td>
    </tr>
    <tr>
    <td style="padding: 20px 0;" align="center">{CompanyLogo}</td>
    </tr>
    <tr>
    <td style="background-color: #00b0f0;">&nbsp;</td>
    </tr>
    </tbody>
    </table>
    <table style="width: 640px; margin: auto;" border="0" cellspacing="0" cellpadding="0" align="center">
    <tbody>
    <tr>
    <td style="border-left: 1px solid #C1DBE8; border-right: 1px solid #C1DBE8; padding: 20PX;" valign="top">
    <h3 style="color: #00b0f0; font-family: arial; text-align: center;">Residential Lease agreementRentDueDate</h3>
    <!-- <h4 style="padding:5px 10px; margin:5px 0; background-color:#e7e5e5; color:#303030; text-align:center; font-size:14px;">
                DRAFT	
          </h4>-->
    <p style="padding: 0 10px; margin-bottom: 5px; font-family: arial;">The following residential agreement is between {CompanyName} and {PrimaryTenantName}, as of {LeaseStartDate}. Each numbered section defines the term, conditions, and responsibility of each party.</p>
    <p style="padding: 0 10px; margin-bottom: 5px; font-family: arial;">{Currency}{UnitCity}</p>
    {RentDueDay}<br />
    <div style="width: 97%; margin-top: 30px; padding: 0 10px;">
    <div style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;">LANDLORD</div>
    <p style="margin: 5px 0; font-family: arial;">In this agreement the Landlord(s) and/or related agent(s) is/are referred to as &ldquo;Landlord&rdquo;. {CompanyName} is the Landlord.</p>
    </div>
    <br />
    <div style="width: 97%; margin-top: 30px; padding: 0 10px;">
    <div style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;">TENANT</div>
    <p style="margin: 5px 0; font-family: arial;">Every instance of the term &ldquo;Tenant&rdquo; in this Lease will refer to {PrimaryTenantName}.</p>
    </div>
    <br />
    <div style="width: 97%; margin-top: 30px; padding: 0 10px;">
    <div style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;">RENTAL PROPERTY</div>
    <p style="margin: 5px 0; font-family: arial;">The property located at {UnitAddress}, {UnitCity},{UnitZipCode}, which is owned/managed by the Landlord will be rented to the Tenant. In this Lease the property will be referred to as &ldquo;lease Premises&rdquo;.</p>
    </div>
    <br />
    <div style="width: 97%; margin-top: 30px; padding: 0 10px;">
    <div style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; font-family: arial; border-bottom: 1px solid #2d3091; padding-bottom: 2px;">TERM OF LEASE AGREEMENT</div>
    <p style="margin: 5px 0; font-family: arial;">This Lease Agreement will start on {LeaseStartDate} and ends on {LeaseEndDate}.</p>
    </div>
    <br />
    <div style="width: 97%; margin-top: 30px; padding: 0 10px;">
    <div style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;">USE &amp; OCCUPANCY OF PROPERTY</div>
    <p style="margin: 5px 0; font-family: arial;">The only authorized person(s) that may live in the Leased Premises is/are: {PrimaryTenantName}.</p>
    </div>
    <br />
    <div style="width: 97%; margin-top: 30px; padding: 0 10px;">
    <div style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;">AMOUNT OF RENT</div>
    <p style="margin: 5px 0; font-family: arial;">The exact amount of Rent due monthly for the right to live in the Leased Premises is {Currency}{RentAmount}.</p>
    </div>
    <br />
    <div style="width: 97%; margin-top: 30px; padding: 0 10px;">
    <div style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;">DUE DATE FOR RENT</div>
    <p style="margin: 5px 0; font-family: arial;">Rent is to be paid before or on the {RentDueDate}. A {graceperiod} day grace period will ensue; nevertheless, rent cannot be paid after this grace period without incurring a late fee.</p>
    </div>
    <br />
    <div style="width: 97%; margin-top: 30px; padding: 0 10px;">
    <div style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;">LATE FEE</div>
    <p style="margin: 5px 0; font-family: arial;">In the event that rent or associated fees are not paid in full by the rental due date or before the end of the {graceperiod} day grace period, a late fee of <span>{LateFee}</span>&nbsp; ({OneTimeOrdaily}) will be incurred.</p>
    </div>
    <br />
    <div style="width: 97%; margin-top: 30px; padding: 0 10px;">
    <div style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;">RETURNED PAYMENTS</div>
    <p style="margin: 5px 0; font-family: arial;">Any returned payments from financial institutions that the Tenant utilizes will incur an addition fee of {AdditionalFee} that will be added to the rental amount.</p>
    </div>
    <br />
    <div style="width: 97%; margin-top: 30px; padding: 0 10px;">
    <div style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;">SECURITY DEPOSIT</div>
    <p style="margin: 5px 0; font-family: arial;">A. Security Deposit of {Currency}{SecurityDeposit} is to be paid by the Tenant before move-in.</p>
    <p style="margin: 5px 0; font-family: arial;">B. This Security Deposit is reserved for the purpose of paying any cost toward damages, cleaning, excessive wear to the property, and in the event of unreturned keys or abandonment of the Leased Premises before or upon the end of the Lease.</p>
    </div>
    <br />
    <div style="width: 97%; margin-top: 30px; padding: 0 10px;">
    <div style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;">UTILITIES &amp; SERVICES</div>
    <p style="margin: 5px 0; font-family: arial;">A. The responsibility of the registering and paying for the following utility services is upon the Tenant:</p>
    <p><input style="border: 1px solid #ddd; padding: 5px; margin-top: 5px; margin-right: 20PX; font-family: arial;" type="text" /> &nbsp; &nbsp; &nbsp; <input style="border: 1px solid #ddd; padding: 5px; margin-top: 5px;" type="text" /></p>
    <p style="font-family: arial;">These services should be maintained at all time during the leasing period.</p>
    <p style="margin: 5px 0; font-family: arial;">B. The Landlord has included the Water utility as a part of rent; however, if the property water usage exceeds Enter amount here the Landlord reserves the right to request that any overages in usage be compensated as a fee in addition to the normal rental rate. These overages must be paid within 20 days.</p>
    </div>
    <br />
    <div style="width: 97%; margin-top: 30px; padding: 0 10px;">
    <div style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;">MAINTENANCE &amp; REPAIRS</div>
    <p style="font-family: arial;">General maintenance and repairs are the responsibility of the Landlord, except in cases where the Tenant has been negligent or has accidentally caused damages to the property.</p>
    <p style="margin: 5px 0; font-family: arial;">A. It will remain the Tenant&rsquo;s responsibility to promptly notify the Landlord of any necessary repairs to the property.</p>
    <p style="margin: 5px 0; font-family: arial;">B. Any damages that have resulted from the Tenant or the Tenant&rsquo;s guests will be the Tenants full responsibility to pay for the costs of repair that is required.</p>
    </div>
    <br />
    <div style="width: 97%; margin-top: 30px; padding: 0 10px;">
    <div style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;">CONDITION OF PROPERTY</div>
    <p style="font-family: arial;">A. The Tenant has inspected the Leased Premises acknowledges and accepts that the Leased Premises is in an acceptable living condition and that all parts of the Leased Premises are in good working order.&nbsp;</p>
    <p style="margin: 5px 0; font-family: arial;">B. It is agreed by the Landlord and Tenant that no promises are made concerning the condition of the Leased Premises.</p>
    <p style="font-family: arial;">C. The Leased Premises will be returned to the Landlord by the Tenant in the same or better condition that it was in at the commencement of the Lease.</p>
    </div>
    <br />
    <div style="width: 97%; margin-top: 30px; padding: 0 10px;">
    <div style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;">ENDING OR RENEWING THE LEASE AGREEMENT</div>
    <p style="font-family: arial;">A. When this Lease ends, if the Tenant or Landlord do not furnish a written agreement to end the Lease it will continue on a month to month basis indefinitely. In order to terminate or renew the Lease agreement either party, Landlord or Tenant, must furnish a written notice at least 30 days before the end of this Lease agreement.</p>
    </div>
    <br />
    <div style="width: 97%; margin-top: 30px; padding: 0 10px;">
    <div style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;">GOVERNING LAW</div>
    <p style="font-family: arial;">The terms, conditions, and any addenda to this Lease are to be governed and held in accord to the statutes and Laws of Enter Governing entity here.</p>
    </div>
    <div style="width: 97%; margin-top: 30px; padding: 0 10px;">
    <div style="width: 100%; margin-top: 20px; color: #2d3091; font-weight: bold; font-size: 16px; border-bottom: 1px solid #2d3091; padding-bottom: 2px; font-family: arial;">ENTIRE AGREEMENT</div>
    <p style="font-family: arial;">NOTICE: This is a LEGALLY binding document.</p>
    <p style="font-family: arial;">A. You are relinquishing specific important rights.</p>
    <p style="margin: 5px 0; font-family: arial;">B. You may reserve the right to have an attorney review the Lease Agreement before signing it.</p>
    <p style="font-family: arial;">By signing this Lease Agreement, the Tenant certifies in good faith to have read, understood, and agrees to abide by all of the terms and conditions stated within this Lease.</p>
    </div>
    <div style="width: 97%; margin-top: 20px; padding: 0 10px;">
    <table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
    <td style="font-family: arial;" width="28%" height="50">Tenant Signature:</td>
    <td style="font-family: arial;" width="28%" height="50">&nbsp;</td>
    <td style="padding-right: 10px; font-family: arial;" align="right" width="22%" height="50">Date:</td>
    <td width="22%" height="50">&nbsp;</td>
    </tr>
    <tr>
    <td style="font-family: arial;" height="40">Landlord / Agent Signature:</td>
    <td height="40">&nbsp;</td>
    <td style="padding-right: 10px; font-family: arial;" align="right" height="40">Date:</td>
    <td height="40">&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    </tr>
    </tbody>
    </table>
    </div>
    </td>
    </tr>
    </tbody>
    </table>
    <table id="footer" style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased; background-color: #585858; color: #ffffff; margin: auto; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#0e867b">
    <tbody>
    <tr>
    <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;" align="center" bgcolor="#05a0e4">ApexLink Property Manager <span>&#x025CF;</span> <a style="color: #fff; text-decoration: none;" href="javascript:;">support@apexlink.com&nbsp; <span>&#x025CF;</span> 772-212-1950</a></td>
    </tr>
    </tbody>
    </table></div>','template_key' =>'LeaseAgreement_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'InTouch', 'template_html' => '<div class="wrapper">

   <input type="hidden" name="templateEdit_key" id="templateEdit_key" value="">

<table style="margin: auto; width: 640px; margin-bottom:30px;background:#fff ;border:2px solid #00b0f0" border="0" cellspacing="0" cellpadding="0" align="center"  bgcolor="#fff">      <tr>          <td>              <table style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased;color: #2d3091; margin: auto; width: 100%;" border="0"                  cellspacing="0" cellpadding="0" align="center">                  <tbody>                      <tr style="background-color: #00b0f0; height: 20px;">                          <td>                              &nbsp;                          </td>                      </tr>                      <tr style="border-collapse: collapse; background-color: #fff;">                          <td class="w580" style="font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580">                              <!-- <div align="center" id="headline">                                                              <p style="color: #ffffff; font-family: "Lucida Grande", "Lucida Sans Unicode", Verdana, sans-serif;                                                                  font-size: 36px; text-align: center; margin-top: -40px; margin-bottom: 30px;">-->                              <!-- http://Apexlink.seasiaconsulting.com-->                              <center>                                  <div style="margin-top: 1px; margin-bottom: 10px;">                                      <p class="clsDefaultImage">                                          {CompanyLogo}</p>                                  </div>                              </center>                              <!--  </p>                                                          </div>-->                          </td>                      </tr>                      <tr style="background-color: #00b0f0; height: 20px;">                          <td>                              &nbsp;                          </td>                      </tr>                  </tbody>              </table>              <table style="width: 640px; padding:10px 20px;background:#fff;" border="0" cellspacing="0" cellpadding="0" align="center">                  <tbody>                      <tr>                          <td style="padding: 10px 0;"                              valign="top" width="100%">                              <h3 style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0;                                  text-align: center; padding: 0; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;">In-Touch Reminder(s)</h3>                          </td>                      </tr>                      <tr>                          <td>                           <p style="font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size: 15px;">                                Dear User,                                  <br />                                  <br />                              </p>                              <p style="font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size: 15px;">                               Greetings!                                  <br />                                  <br />                              </p>                              <p style="font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size: 15px;">                             This is an email reminder, the information below does not required action at this time.                                  <br />                                  <br />                              </p>                          </td>                      </tr>                      <tr>                          <td>                              <table width="100%" cellpadding="0" cellspacing="0" style="padding:0px 20px 50px 20px;">                                  <tr>                                      <td style="border: 1px solid #444; text-align: center;font-family: "Helvetica                                           Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;height:30px;" class="style2">                                          In-Touch ID:                                      </td>                                      <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial,                                           Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;" class="style3">                                          {InTouchID}                                      </td>                                  </tr>                                    <tr>                                      <td style="border: 1px solid #444; text-align: center;font-family: "Helvetica                                             Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;text-align: center;" class="style1">                                            Status:                                                </td>                                                  <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;">                                           {Status}                                              </td>                                                                                     </tr>                                              <tr>                                                  <td style="border: 1px solid #444; text-align: center;font-family: "Helvetica                                           Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;text-align: center;" class="style1">                                            In-Touch Type:                                            </td>                                                <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;">                                          {Type}                                      </td>                                  </tr>                                          <tr>                                                  <td style="border: 1px solid #444; text-align: center;font-family: "Helvetica                                           Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;text-align: center;" class="style1">                                            Category:                                            </td>                                                <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;">                                          {For}                                      </td>                                  </tr>                                                                 <tr>                                                  <td style="border: 1px solid #444; text-align: center;font-family: "Helvetica                                           Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;text-align: center;" class="style1">                                            Subject:                                            </td>                                                <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;">                                          {Subject}                                      </td>                                  </tr>                                        <tr>                                                  <td style="border: 1px solid #444; text-align: center;font-family: "Helvetica                                           Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;text-align: center;" class="style1">                                            Notes:                                            </td>                                                <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;">                                          {Notes}                                      </td>                                  </tr>                                     <tr>                                                  <td style="border: 1px solid #444; text-align: center;font-family: "Helvetica                                           Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;text-align: center;" class="style1">                                            Created By:                                            </td>                                                <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;">                                          {CreatedBy}                                      </td>                                  </tr>                                       <tr>                                                  <td style="border: 1px solid #444; text-align: center;font-family: "Helvetica                                           Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;text-align: center;" class="style1">                                            Created Date:                                            </td>                                                <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;">                                          {CreatedDate}                                      </td>                                  </tr>                                          <tr>                                                  <td style="border: 1px solid #444; text-align: center;font-family: "Helvetica                                           Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;text-align: center;" class="style1">                                            Due Date:                                            </td>                                                <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;">                                          {DueDate}                                      </td>                                  </tr>                                    <tr>                                      <td style="border: 1px solid #444; text-align: center;font-family: "Helvetica                                           Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;text-align: center;" class="style1">                                          Subscription Detail:                                      </td>                                      <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px; padding: 5px;">                                          {SubscriptionDetail}                                          <br />                                          <br />                                          <br />                                          <br />                                          <br />                                          <br />                                      </td>                                  </tr>                                                       </table>                          </td>                      </tr>                                    </tbody>              </table>                  <br />                      <br />              <table id="footer" style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px;                  -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased;                  background-color: #585858; color: #ffffff; margin: auto; width: 640px;" border="0"                  cellspacing="0" cellpadding="0" align="center" bgcolor="#0e867b">                  <tbody>                      <tr>                          <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px;" align="center"                              bgcolor="#05a0e4">                             <a style="color: #fff; text-decoration: none;" href="javascript:;">                                  {PmCompanyName} . {PmCompanyEmail} . {PhoneNumber} </a>                          </td>                      </tr>                  </tbody>              </table>          </td>      </tr>  </table>


</div>','template_key' =>'InTouch_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'PhoneCall', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="PhoneCall_Key">
<table cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">
    <tr>
        <td style="height: 11px; background: #00b0f0;"> </td>
    </tr>
    <tr>
        <td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold;background: #fff; ">{CompanyLogo}</td>
    </tr>
    <tr>
        <td style="background: #00b0f0; height: 15px;"> </td>
        
    </tr>
    <tr>
        <td style="padding: 20px; background: #fff; padding: 30px;" >
            <table style="width: 100%; padding: 0 0 20px 0">
                <tr>
                    <td width="33.33%" ></td>
                    <td width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: center; " >Phone Call Log</td>
                    <td width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: right; ">Date:{Date}</td>
                </tr>
                <tr>
                    <td colspan="2" height="50px"></td>
                </tr>
            </table>
            <table width="100%" style=" border:2px solid #e9e9e9;  border-collapse: collapse; " cellspacing="0" cellpadding="0" >
                <tr>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Name</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Phone</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;" >Date</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Call type</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Length of call</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Call Category</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Purpose of Call</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Action Taken</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Follow Up Needed</th>
                </tr>
                {tblbody}
            </table>

        </td>
    </tr>
    <tr>
        <td style="height: 11px; background: #00b0f0;"> </td>
    </tr>
</table>
</div>','template_key' =>'PhoneCall_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'PhoneCall Mail', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="PhoneCallMail_Key">

<table style="box-shadow: #96999d 0px 0px 2px; border: 1px solid #dddddd; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr style="background-color: #00b0f0; height: 20px;">
<td>&nbsp;</td>
</tr>
<tr style="border-collapse: collapse; background-color: #fff;">
<td style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" align="center"><div align="center" id="headline">
</td>
</tr>
<tr style="background-color: #00b0f0; height: 20px;">
<td>&nbsp;</td>
</tr>
<tr>
<td style="padding: 20px;">
<h3 style="color: #00b0f0; font-family: arial; text-align: center;">Phone Call Log</h3>
<p style="font-family: arial;">Dear {UserName},</p>
<p style="font-family: arial;">Greetings!</p>
<p style="font-family: arial;">Please find the attached the phone call log for today i.e. {Date}</p>

<p style="font-family: arial;">Thank you for using Apexlink Property Management System to manage your property</p>
<p style="font-family: arial;">Best Regards,</p>
<p style="font-family: arial;">ApexLink Inc.</p>
</td>
</tr>
 <tr>
<td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size: 14px;" align="center" bgcolor="#05a0e4">
<a style="color: #fff; text-decoration: none;" href="javascript:;">
{PmCompanyName}. {PmCompanyEmail} . {PhoneNumber} </a>
</td>
</tr>
</tbody>
</table>


</div>','template_key' =>'PhoneCallMail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'TimeSheetPrint', 'template_html' => '<div class="wrapper">

<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="TimeSheetPrint_Key">

<table cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">
    <tr>
        <td style="height: 11px; background: #00b0f0;"> </td>
    </tr>
    <tr>
        <td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold;background: #fff; ">{CompanyLogo}</td>
    </tr>
    <tr>
        <td style="background: #00b0f0; height: 15px;"> </td>
    </tr>
    <tr>
        <td style="padding: 20px; background: #fff; padding: 30px;" >
            <table style="width: 100%; padding: 0 0 20px 0">
                <tr>
                    <td width="33.33%" ></td>
                    <td width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: center; " >Employee Timesheet</td>
                    <td width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: right; ">Date: {ReportDate}</td>
                </tr>
                <tr>
                    <td colspan="2" height="50px"></td>
                </tr>
            </table>
            <table width="100%" style=" border:2px solid #e9e9e9;  border-collapse: collapse; " cellspacing="0" cellpadding="0" >
                <tr>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Employee Name</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Role</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;" >Email</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Phone</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Date</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Last Login</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Time</th>						
                </tr>
                {tblTimeSheetBody}
            </table>
        </td>
    </tr>
    <tr>
        <td style="height: 11px; background: #00b0f0;"> </td>
    </tr>
</table>

</div>','template_key' =>'TimeSheetPrint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'DailyVisitorPdf', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="DailyVisitorPdf_Key">

<table id="table" cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">
		<tr>
			<td colspan="3" style="height: 11px; background: #00b0f0;"> </td>
		</tr>
		<tr>
			<td colspan="3" style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold;background: #fff; ">{CompanyLogo}</td>
		</tr>
		<tr>
			<td style="background: #00b0f0; height: 15px;" colspan="3"> </td>
		</tr>
		<tr>
			<td style="background: #fff; padding:20px 10px 0 30px; width: 33.33%; text-align: center; "  width="33.33%">&nbsp; </td>
			<td style="background: #fff; padding:20px 10px 0 10px; width: 33.33%;font-family: arial; color: #00b0f0; font-size: 25px; text-align: center; "  width="33.33%">DailyVisitor </td>
			<td style="background: #fff; padding:20px 30px 0 10px; width: 33.33%; font-family: arial; color: #00b0f0; font-size: 25px; text-align: right; "  width="33.33%">Date:{Date} </td>
		</tr>
		<tr>
			<td colspan="3"  style="background: #fff; padding:20px 30px 30px 30px; width: 100%; "  width="100%">	
				<table width="100%" style=" border:2px solid #e9e9e9;  border-collapse: collapse; " cellspacing="0" cellpadding="0" >
					<tr>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">VisitorName</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">PhoneNumber</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;" >Date</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Reason for Visit</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Length of Visit</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Time In</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Time Out</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Action Taken</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Follow Up Needed</th>
					</tr>
					{tblbody}
				</table>

			</td>
		</tr>
		<tr>
			<td colspan="3" style="height: 11px; background: #00b0f0;"> </td>
		</tr>
	</table>
</div>','template_key' =>'DailyVisitorPdf_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'TimeSheetMail', 'template_html' => '<div class="wrapper">

<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="TimeSheetPrint_Key">

<table cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">
    <tr>
        <td style="height: 11px; background: #00b0f0;"> </td>
    </tr>
    <tr>
        <td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold;background: #fff; ">{CompanyLogo}</td>
    </tr>
    <tr>
        <td style="background: #00b0f0; height: 15px;"> </td>
    </tr>
    <tr>
        <td style="padding: 20px; background: #fff; padding: 30px;" >
            <table style="width: 100%; padding: 0 0 20px 0">
                <tr>
                    <td width="33.33%" ></td>
                    <td width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: center; " >Employee Timesheet</td>
                    <td width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: right; ">Date: {ReportDate}</td>
                </tr>
                <tr>
                    <td colspan="2" height="50px"></td>
                </tr>
            </table>
            <table width="100%" style=" border:2px solid #e9e9e9;  border-collapse: collapse; " cellspacing="0" cellpadding="0" >
                <tr>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Employee Name</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Role</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;" >Email</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Phone</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Date</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Last Login</th>
                    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Time</th>						
                </tr>
                {tblTimeSheetBody}
            </table>
        </td>
    </tr>
    <tr>
        <td style="height: 11px; background: #00b0f0;"> </td>
    </tr>
</table>

</div>','template_key' =>'TimeSheetMail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'DailyVisitorEmail', 'template_html' => '<div class="wrapper">

<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="DailyVisitorEmail_Key">

<table style="box-shadow: #96999d 0px 0px 2px; border: 1px solid #dddddd; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr style="background-color: #00b0f0; height: 20px;">
<td>&nbsp;</td>
</tr>
<tr style="border-collapse: collapse; background-color: #fff;">
<td style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" align="center"></td>
</tr>
<tr style="background-color: #00b0f0; height: 20px;">
<td>&nbsp;</td>
</tr>
<tr>
<td style="padding: 20px;">
<h3 style="color: #00b0f0; font-family: arial; text-align: center;">Daily Visitor Log</h3>
<p style="font-family: arial;">Dear {UserName},</p>
<p style="font-family: arial;">Greetings!</p>
<p style="font-family: arial;">Please find the attached the Daily Visitor Log for today i.e. {Date}</p>
    <p style="font-family: arial;">&nbsp;</p>
    <p style="font-family: arial;">&nbsp;</p>

<p style="font-family: arial;">Thank you for using Apexlink Property Management System to manage your property</p>
<p style="font-family: arial;">Best Regards,</p>
<p style="font-family: arial;">ApexLink Inc.</p>
</td>
</tr>
     <tr>
<td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size: 14px;" align="center" bgcolor="#05a0e4">
<a style="color: #fff; text-decoration: none;" href="javascript:;">
{PmCompanyName}.{PmCompanyEmail}.{PhoneNumber} </a>
 </td>
 </tr>
</tbody>
</table>
</div>','template_key' =>'DailyVisitorEmail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'TwoWayEmail', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="TwoWayEmail_Key">

<table style="border: 1px solid #dcdcdc; width: 600px;" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td>
<table style="width: 100%;" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="height: 15px; width: 100%;" bgcolor="#00b0f0">&nbsp;</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; text-align: center;">{CompanyLogo}</td>
</tr>
<tr>
<td style="height: 15px; width: 100%;" bgcolor="#00b0f0">&nbsp;</td>
</tr>
</tbody>
</table>
<table style="font-family: Arial; width: 850px;" cellspacing="0" cellpadding="10" align="center">
<tbody>
<tr>
<td style="padding: 30px 10px 0; margin: 0;">
<p style="margin: 0px 0 5px; font-family: arial; font-size: 14px;"><span>Date: </span><span>{Date}</span></p>
<p style="margin: 0px 0 5px; font-family: arial; font-size: 14px;"><span>From: </span><span style="text-transform: uppercase;">{From}</span></p>
<p style="margin: 0px 0 5px; font-family: arial; font-size: 14px;"><span>To: </span><span>{To}</span></p>
<p style="margin: 0px 0 5px; font-family: arial; font-size: 14px;"><span>Subject: </span><span>{Subject}</span></p>
</td>
</tr>
<tr>
<td style="padding: 20px 10px 10px; margin: 0;">
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">Dear Client</p>
</td>
</tr>
<tr>
<td style="padding: 0 10px 10px; margin: 0;">
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">{Body}</p>
</td>
</tr>
<tr>
<td style="padding: 0 10px 10px; margin: 0;">
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">Please <a style="text-decoration: none; color: #00aced;" href="{Url}"> Click here</a>&nbsp;to check the text/Message</p>
</td>
</tr>
<tr>
<td style="padding: 0 10px 10px; margin: 0;">
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">For ant further assistance you can reach us via our Support Team <a style="text-decoration: none; color: #00aced;" href="#">{SupportTeamMail}</a></p>
</td>
</tr>
<tr>
<td style="padding: 0 10px 10px; margin: 0;">
<p style="margin: 0px; font-family: arial; font-size: 14px;">Regards</p>
<p style="margin: 0px; font-family: arial; font-size: 14px;">Apexlink Inc.</p>
</td>
</tr>
<tr>
<td style="padding: 0 10px 30px; margin: 0;">
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">Disclaimer: This is a system generated email. Please do not reply to this message</p>
</td>
</tr>
</tbody>
</table>
<table style="text-align: center; width: 100%;" cellspacing="0" cellpadding="0" bgcolor="#00b0f0">
<tbody>
<tr>
<td style="padding: 15px 0px 15px 34px; display: block; vertical-align: top;">
<p style="font-family: arial; font-size: 15px; color: #fff; margin: 0; padding: 0;">{PmCompanyName}.{PmCompanyEmail}.{PhoneNumber}</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>','template_key' =>'TwoWayEmail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'CredentialExpiry', 'template_html' => '<div class="wrapper">

<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="CredentialExpiry_Key">

<table style="width: 640px; overflow: hidden; margin: 0 auto; border: 4px solid #00b0f0;" cellspacing="0" cellpadding="0" bgcolor="#00b0f0">
<tbody>
<tr>
<td style="height: 20px; background: #00b0f0;">&nbsp;</td>
</tr>
<tr>
<td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold; background: #fff;">{CompanyLogo}</td>
</tr>
<tr>
<td style="background-repeat: no-repeat; background-size: 100%;">
<h5 style="font-family: arial; margin: 0; padding: 7px; font-size: 16px; text-align: center; color: #fff;"></h5>
</td>
</tr>
<tr>
<td style="padding: 20px; background: #fff;">
&nbsp;<h3 style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0;
                                text-align: center; padding: 0; font-family: Calibri;">Dear&nbsp; {Name}! </h3>
                        

<p style="font-family: arial;">Your Credentials are Expiring in {Days} days.Please 
    Renew these.Your Credential Details are as follows:</p>
    <p style="font-family: arial;">Credential Name: {CredentialName}</p>
    <p style="font-family: arial;">Credential Type: {CredentialType}</p>
    <p style="font-family: arial;">Expiry Date:{ExpirationDate}</p>
    <p style="font-family: arial;">&nbsp;</p>
<p style="font-family: arial;">({CompanyName})</p>
<hr/>
<p style="font-family: arial;font-size:10px;"><b>You will received this email from {CompanyName}, because you will registered a lost item, either through your portal, or by contacting staff directly.</b> </p>
</td>
</tr>
<tr>
 <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;" align="center"
                bgcolor="#05a0e4">
               {CompanyName} . <a style="color: #fff; text-decoration: none;" href="javascript:;">
                    www.apexlink.com . {CompanyPhone} </a>
           
</td>
</tr>
</tbody>
</table>
</div>','template_key' =>'CredentialExpiry_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Inventory Mail', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="InventoryMail_Key">


<table align="center" border="0" width="640" style="box-shadow: 0 0 2px #96999d; border:1px solid #ddd;"	  cellpadding="0" cellspacing="0">
    <tr>
        <td style="background-color:#00b0f0;">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" style="padding:20px 0;">{CompanyLogo}</td>
    </tr>
    <tr>
        <td style="background-color:#00b0f0;">&nbsp;</td>
    </tr>
    
    <tr>
        <td style="padding:20px;">
            <h3 style="color:#00b0f0; font-family:arial; text-align:center;">Inventory Alert</h3>
            <p style="font-family:arial;">Dear {administrator first name},</p>
            <p style="font-family:arial;">This notice is to inform you that the warranty/guarantee for the following equipment expires in {Days} days on {Warranty Date}</p>
            <table style="font-family:arial;">
            <tr>
                <td height="30">Equipment</td>
                <td>{Equipment type}</td>
            </tr>
            <tr>
                <td height="30"></td>
                <td>{Equipment brand name}</td>
            </tr>
            <tr>
                <td height="30"></td>
                <td>{Equipment model}</td>
            </tr>
            <tr>
                <td height="30"></td>
                <td>{Serial number}</td>
            </tr>
            <tr>
                <td height="30">Equipment Location</td>
                <td>{Property name}</td>
            </tr>
              <tr>
                <td height="30"></td>
                <td>{Building name}</td>
            </tr> 
                <tr>
                <td height="30"></td>
                <td>{Building Location}</td>
                </tr>
                    <tr>
                    <td height="30"></td>
                    <td>{Unit name}</td>
                </tr>
                </table>
                <p style="font-family:arial;">Thank you for using Apexlink!</p>
                <p style="font-family:arial;">The Apexlink Management Team</p>
			</td>
		</tr>
		<tr>
			<td style="background-color:#00b0f0; font-family:arial; font-weight: bold; text-align:center; color:#fff; padding:20px 0;">{PmCompanyName} . {PmCompanyEmail} . {PhoneNumber}</td>
		</tr>
	  </table></div>','template_key' =>'InventoryMail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Inventory Preview', 'template_html' => '<div class="wrapper">

<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="InventoryPreview_Key">

<table cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">
		<tr>
			<td style="height: 11px; background: #00b0f0;"> </td>
		</tr>
		<tr>
			<td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold;background: #fff; ">{CompanyLogo}</td>
		</tr>
		<tr>
			<td style="background: #00b0f0; height: 15px;"> </td>
		</tr>
		<tr>
			<td style="padding: 20px; background: #fff; padding: 30px;" >
				<table style="width: 100%; padding: 0 0 20px 0">
					<tr>
						<td width="33.33%" style=" text-align:center; font-size:large; font-family: arial; " >Inventory Details</td>
					</tr>
					<tr>
						<td colspan="2" height="50px"></td>
					</tr>
				</table>
				<table width="100%" style=" border:2px solid #e9e9e9;  border-collapse: collapse; " cellspacing="0" cellpadding="0" >
					<tr>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Building</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Unit</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;" >Name</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Warranty Date</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Guarantee</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Replacement Date</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Serial#</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Item#</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Make</th>
					</tr>
					{tblbody}
				</table>

			</td>
		</tr>
		<tr>
			<td style="height: 11px; background: #00b0f0;"> </td>
		</tr>
	</table>
</div>','template_key' =>'InventoryPreview_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Lost Item', 'template_html' => '<div class="wrapper">

<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="LostItem_Key">

<table style="border: 1px solid #dcdcdc; width: 600px;" cellspacing="0" cellpadding="0" align="center"><!--main_table_start-->
<tbody>
<tr>
<td>
<table style="width: 100%;" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="height: 15px; width: 100%;" bgcolor="#00b0f0">&nbsp;</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; text-align: center;">{CompanyLogo}</td>
</tr>
<tr>
<td style="height: 15px; width: 100%;" bgcolor="#00b0f0">&nbsp;</td>
</tr>
</tbody>
</table>
<table style="font-family: Arial; width: 850px;" cellspacing="0" cellpadding="10" align="center">
<tbody>
<tr>
<td style="padding: 20px 10px 10px; margin: 0;">
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">Hi {Name}</p>
</td>
</tr>
<tr>
<td style="padding: 0 10px 10px; margin: 0;">
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">Thank you for reporting your lost item to the {PmCompanyName} Team.</p>
</td>
</tr>
<tr>
<td style="padding: 0 10px 10px; margin: 0;">
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">If we already have your lost item in our Lost and Found department, we will contact you as soon as possible so you can pick up your item.</p>
</td>
</tr>
<tr>
<td style="padding: 0 10px 10px; margin: 0;">
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">Please be aware that if we have not found your item, we will continue to store that information in our Lost and Found tracking system. As soon as your lost property is found and brought to our office, we will notify you.</p>
</td>
</tr>
<tr>
<td style="padding: 0 10px 10px; margin: 0;">
<p style="margin: 0px; font-family: arial; font-size: 14px;">Lost Item Type: {Category}</p>
<p style="margin: 0px; font-family: arial; font-size: 14px;">Lost Item Details: {BriefDescription}</p>
<p style="margin: 0px; font-family: arial; font-size: 14px;">Lost Date: {LostDate}</p>
<p style="margin: 0px; font-family: arial; font-size: 14px;">Lost Location: {LocationLost}</p>
</td>
</tr>
<tr>
<td style="padding: 0 10px 30px; margin: 0;">
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">Thank you,</p>
</td>
</tr>
<tr>
<td style="padding: 0 10px 30px; margin: 0;">
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">{UserName}</p>
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">{PmCompanyName}</p>
</td>
</tr>
<tr>
<td style="padding: 0 10px 30px; margin: 0;">
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">You have received this email from {PmCompanyName}, because you have registered a lost item, either through your portal, or by contacting staff directly.</p>
</td>
</tr>
</tbody>
</table>
<table style="text-align: center; width: 100%;" cellspacing="0" cellpadding="0" bgcolor="#00b0f0">
<tbody>
<tr>
<td style="padding: 15px 0px 15px 34px; display: block; vertical-align: top;">
<p style="font-family: arial; font-size: 15px; color: #fff; margin: 0; padding: 0;">{PmCompanyName}.{PmCompanyEmail}.{PhoneNumber}</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>','template_key' =>'LostItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Found Item', 'template_html' => '<div class="wrapper">

<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="FoundItem_Key">

<table style="border: 1px solid #dcdcdc; width: 600px;" cellspacing="0" cellpadding="0" align="center"><!--main_table_start-->
<tbody>
<tr>
<td>
<table style="width: 100%;" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="height: 15px; width: 100%;" bgcolor="#00b0f0">&nbsp;</td>
</tr>
<tr>
<td style="padding: 0; margin: 0; text-align: center;">{CompanyLogo}</td>
</tr>
<tr>
<td style="height: 15px; width: 100%;" bgcolor="#00b0f0">&nbsp;</td>
</tr>
</tbody>
</table>
<table style="font-family: Arial; width: 850px;" cellspacing="0" cellpadding="10" align="center">
<tbody>
<tr>
<td style="padding: 20px 10px 10px; margin: 0;">
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">Hi {Name}</p>
</td>
</tr>
<tr>
<td style="padding: 0 10px 10px; margin: 0;">
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">We have good news! Your lost property has been found. Please contact {PmCompanyName} by calling {PhoneNumber} or emailing {PmCompanyEmail} as soon as possible to collect this item.</p>
</td>
</tr>
<tr>
<td style="padding: 0 10px 10px; margin: 0;">
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">Please note that our policy is to keep found items for a maximum of 90 days. If you do not come to retrieve your found item in the next 90 days, the item will be disposed of, donated or recycled.</p>
</td>
</tr>
<tr>
<td style="padding: 0 10px 10px; margin: 0;">
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">We look forward to seeing you soon,</p>
</td>
</tr>
<tr>
<td style="padding: 0 10px 30px; margin: 0;">
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">Thank you,</p>
</td>
</tr>
<tr>
<td style="padding: 0 10px 30px; margin: 0;">
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">{UserName}</p>
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">{PmCompanyName}</p>
</td>
</tr>
<tr>
<td style="padding: 0 10px 10px; margin: 0;">
<p style="margin: 0px; font-family: arial; font-size: 14px;">Lost Item Type: {Category}</p>
<p style="margin: 0px; font-family: arial; font-size: 14px;">Lost Item Details: {BriefDescription}</p>
<p style="margin: 0px; font-family: arial; font-size: 14px;">Lost Date: {LostDate}</p>
<p style="margin: 0px; font-family: arial; font-size: 14px;">Lost Location: {LocationLost}</p>
<p style="margin: 0px; font-family: arial; font-size: 14px;">Found Date: {FoundDate}</p>
<p style="margin: 0px; font-family: arial; font-size: 14px;">Found On: {FoundOn}</p>
<p style="margin: 0px; font-family: arial; font-size: 14px;">Location Found: {FoundLocation}</p>
</td>
</tr>
<tr>
<td style="padding: 0 10px 30px; margin: 0;">
<p style="padding: 0; margin: 0; font-family: arial; font-size: 14px;">You have received this email from {PmCompanyName}, because you have registered a lost item, either through your portal, or by contacting staff directly.harry</p>
</td>
</tr>
</tbody>
</table>
<table style="text-align: center; width: 100%;" cellspacing="0" cellpadding="0" bgcolor="#00b0f0">
<tbody>
<tr>
<td style="padding: 15px 0px 15px 34px; display: block; vertical-align: top;">
<p style="font-family: arial; font-size: 15px; color: #fff; margin: 0; padding: 0;">{PmCompanyName}.{PmCompanyEmail}.{PhoneNumber}</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>','template_key' =>'FoundItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Reporting', 'template_html' => '<div class="wrapper">

<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="Reporting_Key">

    <table cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto;">
                    <tr>					
                        <td style="padding: 20px;" colspan="2">						  
                            <p style="font-family: arial;">Dear {UserName},</p>
                            <p style="font-family: arial;">Please find attached the {ReportName} generated on {Date}. </p>
                            <p style="font-family: arial;">Have a great day!</p>
                            <p style="font-family: arial;">&nbsp;</p>
                            <p style="font-family: arial;font-weight:bold;">Your Property Management Team</p>
                            <p style="font-family: arial;">{CompanyName}</p>
                        </td>
                    </tr>
                </table>
</div>','template_key' =>'Reporting_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'PropertyComplaint', 'template_html' => '<div class="wrapper">

<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="PropertyComplaint_Key">

<table style="margin: auto; width: 640px; margin-bottom:30px ;border:2px solid #00b0f0" border="0" cellspacing="0" cellpadding="0" align="center"  bgcolor="#fff">
    <tr>
        <td>
            <table style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased;color: #2d3091; margin: auto; width: 100%;" border="0"
                cellspacing="0" cellpadding="0" align="center">
                <tbody>
                    <tr style="background-color: #00b0f0; height: 20px;">
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="border-collapse: collapse; background-color: #fff;">
                        <td class="w580" style="background-color: #fff; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
                            border-collapse: collapse;" width="580">
                            <!-- <div align="center" id="headline">
                                                            <p style="color: #ffffff; font-family: "Lucida Grande", "Lucida Sans Unicode", Verdana, sans-serif;
                                                                font-size: 36px; text-align: center; margin-top: -40px; margin-bottom: 30px;">-->
                            <!-- http://Apexlink.seasiaconsulting.com-->
                            <center>
                                <div style="margin-top: 1px; margin-bottom: 10px;">
                                    <p class="clsDefaultImage">
                                        {CompanyLogo}</p>
                                </div>
                            </center>
                            <!--  </p>
                                                        </div>-->
                        </td>
                    </tr>
                    <tr style="background-color: #00b0f0; height: 20px;">
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </tbody>
            </table>
            <table style="width: 640px; padding:10px 20px;background:#fff;" border="0" cellspacing="0" cellpadding="0" align="center">
                <tbody>
                    <tr>
                        <td style="padding: 10px 0;"
                            valign="top" width="100%">
                            <h3 style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0;
                                text-align: center; padding: 0; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;">{Complaint}</h3>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size: 15px;">
                            
                                <br />
                                <br />
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0" style="padding:0px 20px 50px 20px;">
                                <tr>
                                    <td width="30%" style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                        Name:
                                    </td>
                                    <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                        {Name}
                                    </td>
                                </tr>
                                  <tr>
                                    <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">			
                                       Address:										
                                    </td>											
                                     <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">			
                                        {Address}<br/>
										{CityName},{StateName} {Zip}									
                                    </td>							
                                										
                                </tr>												
                                <tr>												
                                    <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">		
                                        Complaint Type:							
                                    </td>										
                                    <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                        {ComplaintType}
                                    </td>
                                </tr>
                              
                                <tr>
                                    <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                        Description:
                                    </td>
                                    <td style="border: 1px solid #444; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                        {Description}
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                       
                            </table>
                        </td>
                    </tr>
                
                </tbody>
            </table>
             
            <table id="footer" style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px;
                -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased;
                background-color: #585858; color: #ffffff; margin: auto; width: 640px;" border="0"
                cellspacing="0" cellpadding="0" align="center" bgcolor="#0e867b">
                <tbody>
                    <tr>
                        <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px;" align="center"
                            bgcolor="#05a0e4">
                           <a style="color: #fff; text-decoration: none;" href="javascript:;">
                                {PmCompanyName}.{PmCompanyEmail}.{PhoneNumber} </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>

</div>','template_key' =>'PropertyComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'InventoryPdf', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="InventoryPdf_Key">

<table id="table" cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">  
	   <tr>     <td colspan="3" style="height: 11px; background: #00b0f0;"> </td>    </tr>   
	    <tr>     <td colspan="3" style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold;background: #fff; ">{CompanyLogo}</td>    </tr>   
		 <tr>     <td style="background: #00b0f0; height: 15px;" colspan="3"> </td>    </tr>     

 <tr>     <td colspan="3"  style="background: #fff; padding:20px 30px 30px 30px; width: 100%; "  width="100%">      

<table width="100%" style=" border:2px solid #e9e9e9;  border-collapse: collapse; " cellspacing="0" cellpadding="0" >   

<tr> 
 <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Category</th>      
   <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">SubCategory</th>  
 
  <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;" >Description</th> 

  <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Brand</th>

 <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Supplier</th> 

 <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">No. Of Items</th>
  <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Cost/Unit</th>   
       <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Stock Reorder Level</th>
  <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Property</th> 
         <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Building</th> 
		        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">SubLocation</th>  
				     </tr>       {tblbody}   
					    </table>       </td>    </tr>    <tr>     <td colspan="3" style="height: 11px; background: #00b0f0;"> </td>    </tr>  
					  </table>


</div>','template_key' =>'InventoryPdf_Key ', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'InventoryPrint', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="InventoryPrint_Key">
<table style="margin: auto; width: 640px; background:#fff;margin-bottom :30px ;border:2px solid #00b0f0" border="0"cellspacing="0" cellpadding="0" align="center"  bgcolor="#fff">
   <tr>
      <td>
         <table style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased;color: #2d3091; margin: auto; width: 100%;" border="0" cellspacing="0"cellpadding="0" align="center" >
            <tbody>
               <tr style="background-color:#00b0f0; height: 20px;">
                  <td>&nbsp;  </td>
               </tr>
               <tr style="border-collapse: collapse; background-color: #fff;">
                  <td class="w580" style="font-family: "Helvetica Neue",Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580">  
                  <center>
                     <div style="margin-top: 1px; margin-bottom: 10px;">
                        <p class="clsDefaultImage">{CompanyLogo}</p>
                     </div>
                  </center>
                  </td>                      
               </tr>
              
            </tbody>
         </table>
         <table style="width: 640px; padding:10px 20px; background:#fff;" border="0" cellspacing="0" cellpadding="0" align="center">
            <tbody>
               <tr>
                  <td style="padding: 10px 0;" valign="top"  width="100%"> 
                     <h3 style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0;text-align: center; padding: 0; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;">Inventory Details
                     </h3>
                  </td>
               </tr>
               <tr>
                  <td>
                     <table width="100%" cellpadding="0" cellspacing="0" style="padding:0px 20px 50px 20px;">
                        <tr>
                           <td height="26px" width="30%" style="border: 1px solid #444;text-align:center; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;font-size: 14px; padding: 5px;height:30px;">Category:                    
                           </td>        
                           <td style="border: 1px solid #444; text-align:center; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;font-size: 14px; padding: 5px;">{Category}
                           </td>
                        </tr>
                        <tr>                                      
                           <td height="26px"  style="border: 1px solid #444; text-align:center;font-family: "Helvetica Neue", Arial, Helvetica,Geneva, sans-serif; font-size: 14px; padding: 5px;">SubCategory:                
                           </td>                                  
                           <td style="border: 1px solid #444; text-align:center; font-family: "Helvetica Neue", Arial, Helvetica,Geneva, sans-serif; font-size: 14px; padding: 5px;">{SubCategory}
                           </td>   
                        </tr>
                        <tr>                                                  
                           <td height="26px" style="border: 1px solid #444; text-align:center;font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;font-size:14px; padding: 5px;">Description:
                           </td>                                                
                           <td style="border: 1px solid #444; text-align:center; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;font-size:14px; padding: 5px;">{Description}
                           </td>
                        </tr>
                        
                        <tr>
                           <td height="26px" style="border: 1px solid #444; text-align:center;font-family: "Helvetica Neue", Arial, Helvetica, Geneva,sans-serif;font-size: 14px; padding: 5px;">Brand:</td>
                           <td style="border: 1px solid #444; text-align:center; font-family: "Helvetica Neue", Arial, Helvetica, Geneva,sans-serif;font-size: 14px; padding: 5px;">{Brand}
                           </td>
                        </tr>
                        <tr> 
                           <td height="26px" style="border: 1px solid #444;text-align:center;font-family: "Helvetica Neue",Arial, Helvetica, Geneva, sans-serif;font-size: 14px; padding: 5px;"> Supplier:  
                           </td>                                                 
                           <td style="border: 1px solid #444;text-align:center; font-family: "Helvetica Neue", Arial,Helvetica, Geneva, sans-serif; font-size: 14px; padding: 5px;">{Supplier}
                           </td>
                        </tr>
                        <tr>
                           <td height="26px" style="border: 1px solid #444; text-align:center;font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size: 14px; padding: 5px;">No. of Items:
                           </td>                                                 
                           <td style="border:1px solid #444;  text-align:center; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size: 14px; padding: 5px;">{NoOfItems}
                           </td>
                        </tr>
                        <tr>
                           <td height="26px" style="border: 1px solid #444;text-align:center;font-family: "Helvetica Neue",Arial,Helvetica, Geneva, sans-serif; font-size: 14px; padding: 5px;">Cost/Unit:
                           </td>
                           <td style="border: 1px solid #444;text-align:center; font-family: "Helvetica Neue", Arial,Helvetica, Geneva, sans-serif;font-size: 14px; padding: 5px;">{ItemCost}
                           </td>
                        </tr>
                        <tr>
                           <td height="26px" style="border:1px solid #444; text-align:center;font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;font-size: 14px; padding: 5px;">Stock Reorder Level:</td> 
                           <td style="border: 1px solid #444; text-align:center; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;  
                           font-
                           size: 14px; padding: 5px;">                                             {StockReOrder}                                  
                           </td>                                             
                        </tr>
                        <tr>                                                  
                        <td height="26px" style="border: 1px solid #444;text-align:center;font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;font-size:
                           14px; padding: 5px;">                        Property:                                             
                           </td>                                                
                            <td style="border: 1px solid #444;  text-align:center;font-family:"Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;font-size: 14px;
                           "padding: 5px;">                                            
                           {Property}                                              </td>                                             
                        </tr>
                                       
                        <tr>                                                  
                        <td height="26px" style="border: 1px solid #444; text-align:center;font-family: "Helvetica Neue", Arial, Helvetica, Geneva,
                           sans-serif;font-size: 14px; padding: 5px;">                                           Building:                         
                           </td>                                                 
                           <td style="border: 1px solid #444;  text-align:center;font-family: "Helvetica Neue", Arial, Helvetica, Geneva,
                           sans-serif;font-size: 14px; padding: 5px;">                                             {Building}                                 
                           </td>                                             
                        </tr>
                        <tr>                                                 
                           <td style="border: 1px solid #444; text-align:center;font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px;
                           "padding: 5px;">SubLocation:                                             
                           </td>                                            
                           <td style="border: 1px solid #444;text-align:center; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;                                          font-size: 14px;
                           "padding: 5px;">                                                 
                           {SubLocation}  <br /> <br /> <br />  <br /> <br />                  
                           </td>                                  
                        </tr>
                     </table>
                  </td>
               </tr>
            </tbody>
         </table>
         <table id="footer" style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius: 0px 0px 6px 6px; -
            webkit-font-smoothing: antialiased; background-color: #585858; color: #ffffff; margin: auto; width:640px;" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#0e867b">
            <tbody>
               <tr>    
                  <td style="padding: 
                  10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: "Helvetica Neue", Arial, Helvetica, Geneva,
                  sans-serif; font-size: 14px;" align="center" bgcolor="#05a0e4">  ApexLink.
                    <a style="color: #fff; text-decoration: none;" href="javascript:;">{Email}.{Phone} </a>
                  </td>               
               </tr>
            </tbody>
         </table>
      </td>
   </tr>
</table>
</div>','template_key' =>'InventoryPrint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'MileageLog', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="MileageLog_Key">

<table cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">
        <tr>
            <td style="height: 11px; background: #00b0f0;"> </td>
        </tr>
        <tr>
            <td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold;background: #fff; ">{CompanyLogo}</td>
        </tr>
        <tr>
            <td style="background: #00b0f0; height: 15px;"> </td>
        </tr>
        <tr>
            <td style="padding: 20px; background: #fff; padding: 30px;">
                <table style="width: 100%; padding: 0 0 20px 0">
                    <tr>
                        <td colspan="5" width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: right; ">Date: {ReportDate}</td>

                    </tr>
                    <tr>
                        <td colspan="6" width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: center; ">{heading}</td>
                    </tr>
                </table>
                <table width="100%" style=" border:2px solid #e9e9e9;  border-collapse: collapse; " cellspacing="0" cellpadding="0">
                    {thead} {tbody}
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 11px; background: #00b0f0;"> </td>
        </tr>
    </table>
</div>','template_key' =>'MileageLog_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'MileageLogEmail', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="MileageLogEmail_Key">

<table style="box-shadow: #96999d 0px 0px 2px; border: 1px solid #dddddd; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr style="background-color: #00b0f0; height: 20px;">
<td>&nbsp;</td>
</tr>
<tr style="border-collapse: collapse; background-color: #fff;">
<td style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" align="center">
                                                    
                                                
                                                    </td>
</tr>
<tr style="background-color: #00b0f0; height: 20px;">
<td>&nbsp;</td>
</tr>
<tr>
<td style="padding: 20px;">
<h3 style="color: #00b0f0; font-family: arial; text-align: center;">{heading}</h3>
<p style="font-family: arial;">Dear {UserName},</p>
<p style="font-family: arial;">Greetings!</p>
<p style="font-family: arial;">Please find the attached the Mileage log for {Date}</p>
    <p style="font-family: arial;">&nbsp;</p>
    <p style="font-family: arial;">&nbsp;</p>

<p style="font-family: arial;">Thank you for using Apexlink Property Management System to manage your property</p>
<p style="font-family: arial;">Best Regards,</p>
<p style="font-family: arial;">ApexLink Inc.</p>
</td>
</tr>
     <tr>
<td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size: 14px;" align="center" bgcolor="#05a0e4">
<a style="color: #fff; text-decoration: none;" href="javascript:;">
{PmCompanyName}.{PmCompanyEmail}.{PhoneNumber} </a>
 </td>
 </tr>
</tbody>
</table>
</div>','template_key' =>'MileageLogEmail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'StockReOrder', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="StockReOrder_Key">

<table style="width: 640px; overflow: hidden; margin: 0 auto; border: 4px solid #00b0f0;" cellspacing="0" cellpadding="0" bgcolor="#00b0f0">
<tbody>
<tr>
<td style="height: 20px; background: #00b0f0;">&nbsp;</td>
</tr>
<tr>
<td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold; background: #fff;">{CompanyLogo}</td>
</tr>
<tr>
<td style="background-repeat: no-repeat; background-size: 100%;" bgcolor="#00b0f0">
<h5 style="font-family: arial; margin: 0; padding: 10px; font-size: 16px; text-align: center; color: #fff;"></h5>
</td>
</tr>
<tr>
<td style="padding: 20px; background: #fff;">
&nbsp;<h3 style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0;
                                text-align: center; padding: 0; font-family: Calibri;">Dear&nbsp; {UserName}! </h3>
             

    <p style="font-family: arial;">{SubCategory} of {Category} stored at {SubLocation} of {Building} under {Property} is at the restock level.</p>
    <p style="font-family: arial;"> It’s time to reorder this item to ensure you have the appropriate level of inventory for you and your team.</p>
    <p style="font-family: arial;">Your Property Management Team</p>
    <p style="font-family: arial;">&nbsp;</p>
<p style="font-family: arial;">({CompanyName})</p>
<hr/>

</td>
</tr>
<tr>
 <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;" align="center"
                bgcolor="#05a0e4">
               {CompanyName} . <a style="color: #fff; text-decoration: none;" href="javascript:;">
                    www.apexlink.com . {CompanyPhone} </a>
           
</td>
</tr>
</tbody>
</table>

</div>','template_key' =>'StockReOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Maintenance Log', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="MaintenanceLog_Key">

<table cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">    
	<tr>     <td style="height: 11px; background: #00b0f0;"> </td>    </tr>    
	<tr>     <td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold;background: #fff; ">{CompanyLogo}</td>    </tr>    
	<tr>     <td style="background: #00b0f0; height: 15px;"> </td>    </tr>    <tr>     <td style="padding: 20px; background: #fff; padding: 30px;" >     
	 <table style="width: 100%; padding: 0 0 20px 0">       
	 <tr>        <td width="33.33%" style=" text-align:center; font-size:large; font-family: arial; " >Maintenance Log</td>         
	 <td width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: right; ">Date: {ReportDate}</td> </tr> 
	     <tr>        <td colspan="2" height="50px"></td>       </tr> 
	      </table>      
		  <table width="100%" style=" border:2px solid #e9e9e9;  border-collapse: collapse; " cellspacing="0" cellpadding="0" >       <tr>        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Work Order No</th>        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Work Order Category</th>        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;" >Building</th>        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Unit</th>        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Status</th>        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Requested by</th>        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Name</th>        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Completed On</th>        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Maintenance Amount</th>       </tr>       {tblbody}      </table>       </td>    </tr>    <tr>     <td style="height: 11px; background: #00b0f0;"> </td>    </tr>   </table>

</div>','template_key' =>'MaintenanceLog_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Print Envelope', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="PrintEnvelope_Key">

<table cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">
		<tr>
			<td style="height: 11px; background: #00b0f0;"> </td>
		</tr>
		<tr>
			<td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold;background: #fff; ">{CompanyLogo}</td>
		</tr>
		<tr>
			<td style="background: #00b0f0; height: 15px;"> </td>
		</tr>
		<tr>
			<td style="padding: 20px; background: #fff; padding: 30px;" >
				<table style="width: 100%; padding: 0 0 20px 0">
					<tr>
						<td width="33.33%" style=" text-align:center; font-size:large; font-family: arial; " >Print Envelope</td>
					</tr>
					<tr>
						<td colspan="2" height="50px"></td>
					</tr>
				</table>
				<table width="100%" style=" border:2px solid #e9e9e9;  border-collapse: collapse; " cellspacing="0" cellpadding="0" >
				{printenvelope}
				</table>

			</td>
		</tr>
		<tr>
			<td style="height: 11px; background: #00b0f0;"> </td>
		</tr>
	</table>

</div>','template_key' =>'PrintEnvelope_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Maintenance Log Mail', 'template_html' => '<div class="wrapper">

<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="MaintenanceLogMail_Key">

<table style="box-shadow: #96999d 0px 0px 2px; border: 1px solid #dddddd; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center"> 
<tbody> 
<tr style="background-color: #00b0f0; height: 20px;"> 
<td>&nbsp;</td> 
</tr> 
<tr style="border-collapse: collapse; background-color: #fff;">
 <td style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" align="center"><div align="center" id="headline">                                                      
 <div style="margin-top: 1px; margin-bottom: 10px;">{CompanyLogo}</div>  
 </p>                                                     </div>                                                                                                                                                      
 </td> 
 </tr> 
 <tr style="background-color: #00b0f0; height: 20px;"> <td>&nbsp;</td> </tr> <tr> <td style="padding: 20px;"> 
 <h3 style="color: #00b0f0; font-family: arial; text-align: center;">Maintenance Log</h3> <p style="font-family: arial;">Dear {UserName},</p> <p style="font-family: arial;">Greetings!</p> <p style="font-family: arial;">Please find attached the Maintenance log for {Date}</p>     <p style="font-family: arial;">&nbsp;</p>     <p style="font-family: arial;">&nbsp;</p>  <p style="font-family: arial;">Thank you for using Apexlink Property Management System to manage your property</p> <p style="font-family: arial;">Best Regards,</p> <p style="font-family: arial;">ApexLink Inc.</p> </td> </tr>      <tr>  <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size: 14px;" align="center" bgcolor="#05a0e4">  <a style="color: #fff; text-decoration: none;" href="javascript:;">  {PmCompanyName}.{PmCompanyEmail}.{PhoneNumber} </a>   </td>   </tr> </tbody> </table>                                                                                                                                                       </td> </tr> <tr style="background-color: #00b0f0; height: 20px;"> <td>&nbsp;</td> </tr> <tr> <td style="padding: 20px;"> <h3 style="color: #00b0f0; font-family: arial; text-align: center;">Maintenance Log</h3> <p style="font-family: arial;">Dear {UserName},</p> <p style="font-family: arial;">Greetings!</p> <p style="font-family: arial;">Please find attached the Maintenance log for {Date}</p>     <p style="font-family: arial;">&nbsp;</p>     <p style="font-family: arial;">&nbsp;</p>  <p style="font-family: arial;">Thank you for using Apexlink Property Management System to manage your property</p> <p style="font-family: arial;">Best Regards,</p> <p style="font-family: arial;">ApexLink Inc.</p> </td> </tr>      <tr>  <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size: 14px;" align="center" bgcolor="#05a0e4">  <a style="color: #fff; text-decoration: none;" href="javascript:;">  {PmCompanyName}.{PmCompanyEmail}.{PhoneNumber} </a>   </td>   </tr> </tbody> </table>
</div>','template_key' =>'MaintenanceLogMail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Short Term Rental', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="ShortTermRental_Key">

<table style="border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; background-color: #d6f0fd; color: #2d3091; margin: auto; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#d6f0fd">
    <tbody>
        <tr style="background-color: #00b0f0; height: 20px;">
            <td>&nbsp;</td>
        </tr>
        <tr style="border-collapse: collapse; background-color: #fff;">
            <td class="w580" style="font-family: Helvetica Neue, Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580">
                <center>
                    <div style="margin-top: 1px; margin-bottom: 10px;">{CompanyLogo}</div>
                </center>
            </td>
        </tr>
        <tr style="background-color: #00b0f0; height: 20px;"> <td>&nbsp;</td> </tr>
    </tbody>
</table>
<table style="width: 640px; margin: auto;" border="0" cellspacing="0" cellpadding="0" align="center">
    <tbody>
        <tr>
            <td style="border-left: 1px solid #C1DBE8; border-right: 1px solid #C1DBE8; padding: 10px 0;" valign="top" width="100%">
                <h3 style="width: 100%; margin: 0 auto 20px auto; font-size: 16px; color: #303030; text-align: center; padding: 0;">
                    <span style="text-decoration: underline;">New Short Term Renter</span>
                </h3>
                <p style="padding: 0 10px; margin-bottom: 5px;">Dear {Username}</p>
                <p style="padding: 0 10px; margin-bottom: 5px;">You have booked a unit for short term rental, as follows:</p>                
                <p style="padding: 0 10px; margin-bottom: 5px;"><strong>Renter Name:</strong> {Username}</p>                
                <p style="padding: 0 10px; margin-bottom: 5px;"><strong>Property Name:</strong> {PropertyName}</p>
                <p style="padding: 0 10px; margin-bottom: 5px;"><strong>Building :</strong> {BuildingName}</p>
                <p style="padding: 0 10px; margin-bottom: 5px;"><strong>Unit :</strong> {UnitNumber}</p>
                <p style="padding: 0 10px; margin-bottom: 5px;"><strong>Booking Amount:</strong> {Amount}</p>             
                <div style="width: 97%; margin-top: 30px; padding: 0 10px;">
                    <p>&nbsp;</p>
                    <p style="margin-bottom: 5px;">Thank You,</p>
                    <p style="margin-bottom: 5px;">{Signature}</p>
                    <p>&nbsp;</p>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<table id="footer" style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased; background-color: #585858; color: #ffffff; margin: auto; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#0e867b">
    <tbody>
        <tr>
            <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;" align="center" bgcolor="#05a0e4">                
                <a style="color: #fff; text-decoration: none;" href="javascript:;">{PmCompanyName}.{PmCompanyEmail}.{PhoneNumber} </a>
            </td>
        </tr>
    </tbody>
</table>

</div>','template_key' =>'ShortTermRental_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Flag', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="Flag_Key">

<table cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">
		<tr>
			<td style="height: 11px; background: #00b0f0;"> </td>
		</tr>
		<tr>
			<td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold;background: #fff; ">{CompanyLogo}</td>
		</tr>
		<tr>
			<td style="background: #00b0f0; height: 15px;"> </td>
		</tr>
		<tr>
			<td style="padding: 20px; background: #fff; padding: 30px;" >
				<table style="width: 100%; padding: 0 0 20px 0">
					<tr>
						<td width="33.33%" ></td>
						<td width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: center; " >Flag Bank List</td>
						<td width="33.33%" style="font-family: arial; color: #00b0f0; font-size: 25px; text-align: right; ">Date: {Date}</td>
					</tr>
					<tr>
						<td colspan="2" height="50px"></td>
					</tr>
				</table>
                                <h2>List Of Flag Bank</h2>
				<table width="100%" style=" border:2px solid #e9e9e9;  border-collapse: collapse; " cellspacing="0" cellpadding="0" >
					<tr>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Flag For</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Flag Name</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;" >Date</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Phone Number</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Flag Reason</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Completed</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Flagged By</th>	
					        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Flag Type</th>
                                                <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Note</th>
					</tr>
					<tr>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">{FlagFor}</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">{FlagName}</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;" >{Date}</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">{PhoneNumber}</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">{FlagReason}</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">{Completed}</th>
						<th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">{FlaggedBy}</th>	
					        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">{FlagType}</th>
                                                <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">{Note}</th>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="height: 11px; background: #00b0f0;"> </td>
		</tr>
	</table>



</div>','template_key' =>'Flag_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'WorkOrderPdf', 'template_html' => '<div class="wrapper">

<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="WorkOrderPdf_Key">

<table id="table" cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">  
	   <tr>     <td colspan="3" style="height: 11px; background: #00b0f0;"> </td>    </tr>   
	    <tr>     <td colspan="3" style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold;background: #fff; ">{CompanyLogo}</td>    </tr>   
		 <tr>     <td style="background: #00b0f0; height: 15px;" colspan="3"> </td>    </tr>     

 <tr>     <td colspan="3"  style="background: #fff; padding:20px 30px 30px 30px; width: 100%; "  width="100%">      

<table width="100%" style=" border:2px solid #e9e9e9;  border-collapse: collapse; " cellspacing="0" cellpadding="0" >   

<tr> 
 <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Work Order Number</th>      
   <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Work Order Category</th>  
 
  <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;" >Vendor</th> 

  <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Property</th>

 <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Created On</th> 

 <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Caller/RequestedBy</th>
  <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Amount</th>   
       <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Priority</th>
  <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Status</th> 
       
				     </tr>       {tblbody}   
					    </table>       </td>    </tr>    <tr>     <td colspan="3" style="height: 11px; background: #00b0f0;"> </td>    </tr>  
					  </table>
</div>','template_key' =>'WorkOrderPdf_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'TicketPdf', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="TicketPdf_Key">

<table id="table" cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">  
	   <tr>     <td colspan="3" style="height: 11px; background: #00b0f0;"> </td>    </tr>   
	    <tr>     <td colspan="3" style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold;background: #fff; ">{CompanyLogo}</td>    </tr>   
		 <tr>     <td style="background: #00b0f0; height: 15px;" colspan="3"> </td>    </tr>     

 <tr>     <td colspan="3"  style="background: #fff; padding:20px 30px 30px 30px; width: 100%; "  width="100%">      

<table width="100%" style=" border:2px solid #e9e9e9;  border-collapse: collapse; " cellspacing="0" cellpadding="0" >   

<tr> 
 <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Tenant Name</th>      
   <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Unit</th>  
 
  <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;" >Property</th> 

  <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Ticket Number</th>

 <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Ticket Type</th> 

 <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Category</th>
  <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Created Date</th>   
  <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Status</th> 
       
				     </tr>       {tblbody}   
					    </table>       </td>    </tr>    <tr>     <td colspan="3" style="height: 11px; background: #00b0f0;"> </td>    </tr>  
					  </table>

</div>','template_key' =>'TicketPdf_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Insurance', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="Insurance_Key">

<table align="center" border="0" width="640" style="box-shadow: 0 0 2px #96999d; border:1px solid #ddd;"	  cellpadding="0" cellspacing="0">
		<tr>
			<td style="background-color:#00b0f0;">&nbsp;</td>
		</tr>
		<tr>
			<td align="center" style="padding:20px 0;"> {CompanyLogo} </td>
		</tr>
		<tr>
			<td style="background-color:#00b0f0;">&nbsp;</td>
		</tr>
		
		<tr>
			<td style="padding:20px;">
				
				<p style="font-family:arial;">Dear {username},</p>
				<p style="font-family:arial;">Here your Insurance Start Date is {StartDate} and End Date is {EndDate} </p>
                                <p style="font-family:arial;">Your Insurance Agent Name is {InsuranceAgentName} and Insurance Type are {InsuranceType}</p>
                                <p style="font-family:arial;">Your Submitted Email is {EmailAddress} and Phone Number is {PhoneNumber}</p>
                                <p style="font-family:arial;">Thanks from {InsuranceCompanyName}</p> 
		
	  </table>

</div>','template_key' =>'Insurance_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'PaymentPlanDue', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="PaymentPlanDue_Key">
<p>&nbsp;</p>  
<table style="width: 640px; overflow: hidden; margin: 0 auto; border: 4px solid #00b0f0;" cellspacing="0" cellpadding="0" bgcolor="#00b0f0">  
<tbody>  <tr>  <td style="height: 20px; background: #00b0f0;">&nbsp;</td>  </tr>  
<tr>  
<td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold; background: #fff;">{CompanyLogo}</td>
  </tr> 
   <tr> 
    <td style="background-repeat: no-repeat; background-size: 100%; background: #00b0f0;"> 
     <h5 style="font-family: arial; margin: 0; padding: 7px; font-size: 16px; text-align: center; color: #fff;"></h5>  </td>  </tr>  <tr>  <td style="padding: 20px; background: #fff;">  &nbsp;<h3 style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0; padding: 0; font-family: Calibri;">Dear&nbsp; {PayerName} </h3><p style="font-family: arial;">Your payment of {Amount} is due on {PaymentDate} </p> <p style="font-family: arial;">&nbsp;</p>  <p style="font-family: arial;">({CompanyName})</p>  <hr />   </td>  </tr>  <tr>   <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;" align="center" bgcolor="#05a0e4">                 {CompanyName} . <a style="color: #fff; text-decoration: none;" href="javascript:;">                      www.apexlink.com . {CompanyPhone} </a>               </td>  </tr>  </tbody>  </table>
</div>','template_key' =>'PaymentPlanDue_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'PaymentPlanMissed', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="PaymentPlanMissed_Key">

<p>&nbsp;</p>  <table style="width: 640px; overflow: hidden; margin: 0 auto; border: 4px solid #00b0f0;" cellspacing="0" cellpadding="0" bgcolor="#00b0f0">  <tbody>  <tr>  <td style="height: 20px; background: #00b0f0;">&nbsp;</td>  </tr>  <tr>  <td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold; background: #fff;">{CompanyLogo}</td>  </tr>  <tr>  <td style="background-repeat: no-repeat; background-size: 100%;">  <h5 style="font-family: arial; margin: 0; padding: 7px; font-size: 16px; text-align: center; color: #fff; background:#00b0f0;"></h5>  </td>  </tr>  <tr>  <td style="padding: 20px; background: #fff;">  &nbsp;<h3 style="width: 100%; margin: 0 auto 20px auto; font-size: 18px; color: #00b0f0; padding: 0; font-family: Calibri;">Dear&nbsp; {PayerName} </h3><p style="font-family: arial;">You missed your payment of {Amount} scheduled on {PaymentDate}</p> <p style="font-family: arial;">&nbsp;</p>  <p style="font-family: arial;">({CompanyName})</p>  <hr />   </td>  </tr>  <tr>   <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;" align="center" bgcolor="#05a0e4">                 {CompanyName} . <a style="color: #fff; text-decoration: none;" href="javascript:;">                      www.apexlink.com . {CompanyPhone} </a>               </td>  </tr>  </tbody>  </table>

</div>','template_key' =>'PaymentPlanMissed_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Pakage Delivered here for you', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="PakageDeliveredHere_Key">

<table align="center" border="0" width="640" style="box-shadow: 0 0 2px #96999d; border:1px solid #ddd;"	  cellpadding="0" cellspacing="0">
		<tr>
			<td style="background-color:#00b0f0;">&nbsp;</td>
		</tr>
		<tr>
			<td align="center" style="padding:20px 0;"> {CompanyLogo} </td>
		</tr>
		<tr>
			<td style="background-color:#00b0f0;">&nbsp;</td>
		</tr>
		
		<tr>
			<td style="padding:20px;">
				<h3 style="color:#00b0f0; font-family:arial; text-align:center;">Pakage Delivered here for you,{username}! </h3>
				<p style="font-family:arial;">Dear {username},</p>
				<p style="font-family:arial;">we have a package delivered for you here at our office,please stop by the {Company Name} office and pick it up as soon as you can during our bussiness hours.</p>
                                <p style="font-family:arial;">See you soon,</p>
                                <p style="font-family:arial;">Your {Company Name} Team</p>
		<tr>
			<td style="background-color:#00b0f0; font-family:arial; font-weight: bold; text-align:center; color:#fff; padding:20px 0;"> {Company Name} . {Companywebsite.com} . {Phone number} </td>
		</tr>
	  </table>
</div>','template_key' =>'PakageDeliveredHere_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Verification Code', 'template_html' => '<div class="wrapper">

<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="VerificationCode_Key">

<table style="box-shadow: #96999d 0px 0px 2px; border: 1px solid #dddddd; width: 640px;" border="0" cellspacing="0" cellpadding="0" align="center"> 
	<tbody> 
		<tr style="background-color: #00b0f0; height: 20px;"> 			
			<td>&nbsp;</td> 
		</tr> 
		<tr style="border-collapse: collapse; background-color: #fff;"> 
			<td style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" align="center">
		</tr> 

		<tr style="background-color: #00b0f0; height: 20px;"> 
			<td>&nbsp;</td> 
		</tr> 
		<tr> 
			<td style="padding: 20px;"> 
				<h3 style="color: #00b0f0; font-family: arial; text-align: center;">Verification Code</h3> 
				<p style="font-family: arial;">Dear {UserName},	</p> 
				<p style="font-family: arial;">This is your system Verification Code: 
					<span style="background-color: #00FF00">{Code}</span>
				</p>     
				<p style="font-family: arial;">Please note that each verification code lasts for only 10 minutes, after 10 minutes, this code will become invalid and unusable, you must request a new verification code to access the system.
				</p>  
				<p style="font-family: arial;">Please contact your Property Manager or system administrator if you have any questions.
				</p>    
				<p style="font-family: arial;">&nbsp;
				</p>  
				<p style="font-family: arial;">Sincerely,
				</p> 
				<p style="font-family: arial;">Your ApexLink User Support Team
				</p> 
				<p style="font-family: arial;">By logging in this software you are agreeing to the ApexLink Terms of use agreement, Privacy Statement and Security Statement located on our main website at www.apexLink.com
				</p> 
			</td> 
		</tr>      
		<tr>  
			<td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif; font-size: 14px;" align="center" bgcolor="#05a0e4">  
				<a style="color: #fff; text-decoration: none;" href="javascript:;">  {PmCompanyName}.{PmCompanyEmail}.{PhoneNumber} </a>   
			</td>   
		</tr> 
	</tbody> 
</table>
</div>','template_key' =>'VerificationCode_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Payment Receipt', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="PaymentReceipt_Key">


<table id="table" 

cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">     

          <tr>           <td colspan="3" style="height: 11px; background: #00b0f0;"> </td>    </tr>                         <tr>     <td colspan="3" style="text-

align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold;background: #fff; ">{CompanyLogo}</td>             </tr>          <tr>   

  <td style="background: #00b0f0; height: 15px;" colspan="3"> </td>    </tr>                        <tr>     <td style="background: #fff; padding:20px 30px 

0 10px; width: 33.33%; font-family: arial; color: #00b0f0; font-size: 25px; text-align: right; "  width="33.33%">Name:  {PersonName} </td>    <td 

style="background: #fff; padding:20px 10px 0 10px; width: 33.33%;font-family: arial; color: #00b0f0; font-size: 25px; text-align: center; "  

width="33.33%">Payment Receipt </td>         <td style="background: #fff; padding:20px 30px 0 10px; width: 33.33%; font-family: arial; color: 

#00b0f0; font-size: 25px; text-align: right; "  width="33.33%">Date:  {Date} </td>   </tr>             <tr>  <td colspan="3"  style="background: #fff; 

padding:20px 30px 30px 30px; width: 100%; "  width="100%">                 <table width="100%" style=" border:2px solid #e9e9e9;  border-collapse: 

collapse; " cellspacing="0" cellpadding="0" >       <tr>                      <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 

16px;">Installment Number</th>             <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Installment 

Date</th>                 <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;" >Installment Amount{CurrencySymbol}

</th>           <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Installment Frequency</th>                      <th 

style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Extra Amount{CurrencySymbol2}</th>                  <th 

style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Interest Amount{CurrencySymbol1}</th>                                     

  </tr>       {tblbody}      </table>                           </td>    </tr>    <tr>     <td colspan="3" style="height: 11px; background: #00b0f0;"> </td>    

</tr>   </table>

</div>','template_key' =>'PaymentReceipt_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'PurchaseOrder', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="PurchaseOrder_Key">

<p>&nbsp;</p>  <table style="width: 640px; overflow: hidden; margin: 0 auto; border: 4px solid #00b0f0;" cellspacing="0" cellpadding="0" bgcolor="#00b0f0">      <tbody>          <tr>              <td style="height: 20px; background: #00b0f0;">&nbsp;</td>          </tr>          <tr>              <td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold; background: #fff;">{CompanyLogo}</td>          </tr>          <tr>              <td style="background-repeat: no-repeat; background-size: 100%; background:#00b0f0;">                  <h5 style="font-family: arial; margin: 0; padding: 7px; font-size: 16px; text-align: center; color: #fff;">Approval Requested for Purchase Order PO: {PONumber}</h5> </td>          </tr>          <tr>              <td style="padding: 20px; background: #fff;">                  <p style="font-family: arial;">Dear {Approver},</p>                  <p style="font-family: arial;">A new purchase has been generated, below are details for Purchase Order:-</p>                  <p style="font-family: arial;">Created On: - {CurrentDate} Required By Date: - {RequiredDate}.</p>                  <p style="font-family: arial;">Property: - {PropertyName} Building: - {BuildingName} Unit: - {UnitName} </p>                  <p style="font-family: arial;">Work Order(s):- {WorkOrder} Approver(s):- {Approvers}</p>                  <p style="font-family: arial;">Invoice No: - {InvoiceNo}</p>       {PurchaseOrderArea}                  {PurchaseOrderEmail}                  <p style="font-family: arial;">If you have any questions, contact your Property Manager Team at {PMPhone} or {PMEmail}.</p>                  <p style="font-family: arial;">Thank you. </p>                  <p style="font-family: arial;">Apexlink Team</p>              </td>          </tr>          <tr>              <td style="height: 30px; background: #00b0f0;">&nbsp;</td>          </tr>      </tbody>  </table>


</div>','template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'PurchaseOrderPdf', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="PurchaseOrderPdf_Key">

<table id="table" cellpadding="0"    cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">        <tr>     <td    colspan="3" style="height: 11px; background: #00b0f0;"> </td>    </tr>          <tr>     <td colspan="3" style="text-align: center; font-family: arial;    padding: 20px 0; font-size: 20px; font-weight: bold;background: #fff; ">{CompanyLogo}</td>    </tr>        <tr>     <td style="background: #00b0f0;    height: 15px;" colspan="3"> </td>    </tr>          <tr>     <td colspan="3"  style="background: #fff; padding:20px 30px 30px 30px; width: 100%; "     width="100%">          <table width="100%" style=" border:2px solid #e9e9e9;  border-collapse: collapse; " cellspacing="0" cellpadding="0" >          <tr>    <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">PONumber</th>           <th style="border:2px solid    #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">PropertyName</th>         <th style="border:2px solid #e9e9e9; padding: 5px; font-family:    arial;  font-size: 16px;" >BuildingName</th>       <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size:    16px;">UnitName</th> <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">RequiredByDate</th><th    style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Vendor</th>    <th style="border:2px solid #e9e9e9; padding:    5px; font-family: arial;  font-size: 16px;">WorkOrders</th>       <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size:    16px;">Approvers</th>        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">InvoiceNumber</th>   <th    style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">POStatus</th>            </tr>       {tblbody}              </table>          </td>    </tr>    <tr>     <td colspan="3" style="height: 11px; background: #00b0f0;"> </td>    </tr>           </table>
</div>','template_key' =>'PurchaseOrderPdf_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'DelinquentTemplate', 'template_html' => '<div class="wrapper">

<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="DelinquentTemplate_Key">

<p>&nbsp;</p>
<table style="width: 640px; overflow: hidden; margin: 0 auto; border: 4px solid #00b0f0;" cellspacing="0" cellpadding="0" bgcolor="#00b0f0">
    <tbody>
        <tr>
            <td style="height: 20px; background: #00b0f0;">&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold; background: #fff;">{CompanyName}</td>
        </tr>
        <tr>
            <td style="background-repeat: no-repeat; background-size: 100%; background:#00b0f0;">
                <h5 style="font-family: arial; margin: 0; padding: 7px; font-size: 16px; text-align: center; color: #fff;">Late Rent Payment Due </h5> </td>
        </tr>
        <tr>
            <td style="padding: 20px; background: #fff;">
                <p style="font-family: arial;">Date of Notice: {Month date, year},</p>
                <p style="font-family: arial;">Dear {Tenant Name},</p>
				 <p style="font-family: arial;">{GuarantorName}</p>
                <p style="font-family: arial;">As of today, your {Month} rent payment is overdue by {X} days. Hopefully, this is an oversight on your part.</p>
                <p style="font-family: arial;">Please stop by the office immediately so that you can make your payment and not incur any late fees – or visit your Tenant Portal and make your payment today. Your total payment due as of today is ${Outstanding}.</p>
                <p style="font-family: arial;">As a reminder, payments arriving {x} days late incur a late fee of ${LateFee}.</p>
                <p style="font-family: arial;">If you have any questions, contact your Property Manager Team at {ContactNumber} or {email}.</p>
                <p style="font-family: arial;">Thank you. </p>
                <p style="font-family: arial;">Apexlink Team</p>
            </td>
        </tr>
        <tr>
            <td style="height: 30px; background: #00b0f0;">&nbsp;</td>
        </tr>
    </tbody>
</table>

</div>','template_key' =>'DelinquentTemplate_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'PurchaseOrderAttachment', 'template_html' => '<div class="wrapper">

<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="PurchaseOrderAttachment_Key">


<table width="640" border="0" align="center" cellpadding="0" cellspacing="0">    <tr>      <td colspan="2">{logo}</td>    </tr>    <tr>      <td>      <h2 style="font-family: arial; font-size: 30px;">Purchase Order</h2>       <p style="color: #000; font-size: 17px; text-align: left; font-family: arial; margin:0 0 5px 0; font-weight: bold;">{PropertyName}</p>      <p style="font-size: 14px; text-align: left; font-family: arial; margin:0;">{PropertyAddress}</p>          <p style="font-size: 14px; text-align: left; font-family: arial; margin:0;">Phone - {PropertyPhone}</p>        <p style="font-size: 14px; text-align: left; font-family: arial; margin:0;">Fax - {PropertyFax}</p></td>        <td>        <p style="color: #000; font-size: 17px; text-align: left; font-family: arial; margin:30px 0 0 0;">PO#: {PONumber}</p>        <p style="color: #000; font-size: 17px; text-align: left; font-family: arial; margin:0 0 0 0;">Created On: {PODate}</p>          <p style="color: #000; font-size: 17px; text-align: left; font-family: arial; margin:0 0 0 0;">Approvers: {POApprovers}</p>      </td>    </tr>    <tr>      <td>      <p style="color: #000; font-size: 17px; text-align: left; font-family: arial; margin:30px 0 5px 0; font-weight: bold;">VENDOR:</p>      <p style="color: #000; font-size: 17px; text-align: left; font-family: arial;  margin:0 0 0 0;">{VendorName}</p>        <p style="font-size: 14px; text-align: left; font-family: arial; margin:0;">{VendorAddress}</p>          <!--<p style="font-size: 14px; text-align: left; font-family: arial; margin:0;">CHARLOTTE, NC 28201</p>-->        <!--<td>        <p style="color: #000; font-size: 17px; text-align: left; font-family: arial; margin:0 0 0 0; font-weight: bold;">PROPERTY</p>        <p style="color: #000; font-size: 17px; text-align: left; font-family: arial; margin:0 0 0 0;">Created On: 01/14/2019</p>          <p style="color: #000; font-size: 17px; text-align: left; font-family: arial; margin:0 0 0 0;">Approvers: Sonny Kessebeh</p>      </td>-->    </tr>    <tr>      <td>            <p style="color: #000; font-size: 17px; text-align: left; font-family: arial; margin:30px 0 5px 0; font-weight: bold;">Vendor Instruction:</p>      <p style="color: #000; font-size: 17px; text-align: left; font-family: arial;  margin:0 0 0 0;">{VendorInstruction}</p>      </td>     </tr>  </table>      <table style="border-collapse:collapse; margin-top:20px; font-size:12px;" width="640" cellspacing="0" cellpadding="5" border="1" align="center">          <tbody><tr>              <th style="padding-left:5px; line-height:16px; background-color:#dfdfdf; font-family: arial;" width="127" valign="top" align="left">Quantity</th>              <th style="padding-left:5px; line-height:16px; background-color:#dfdfdf; font-family: arial;" width="91" valign="top" align="left">GL Account</th>             <th style="padding-left:5px; line-height:16px; background-color:#dfdfdf; font-family: arial;" width="135" valign="top" align="left">Description</th>              <th style="padding-left:5px; line-height:16px; background-color:#dfdfdf; font-family: arial;" width="95" valign="top" align="left">Item Amount</th>              <th style="padding-left:5px; line-height:16px; background-color:#dfdfdf; font-family: arial;" width="95" valign="top" align="left">Total</th>            </tr>            {VendorTrDetails}                         <tr>              <td colspan="3" style="padding-left:5px; font-weight: bold; font-family: arial;" align="right">Total</td>              <td style="padding-left:5px; font-family: arial;" align="left"></td>              <td style="padding-left:5px; font-weight: bold; font-family: arial;" align="left">{GrandTotal}</td>            </tr>          </tbody>        </table>

</div>','template_key' =>'PurchaseOrderAttachment_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'DelinquentReports', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="DelinquentReports_Key">


<table id="table" cellpadding="0" cellspacing="0" width="100%" style="width:100%; overflow:hidden; margin:0 auto; border:4px solid #00b0f0;" bgcolor="#00b0f0">
        <tr>
            <td colspan="3" style="height: 11px; background: #00b0f0;"> </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: center; font-family: arial; padding: 20px 0; font-size: 20px; font-weight: bold;background: #fff; ">{CompanyLogo}</td>
        </tr>
        <tr>
            <td style="background: #00b0f0; height: 15px;" colspan="3"> </td>
        </tr>
        <tr>
            <td colspan="3" style="background: #fff; padding:20px 30px 30px 30px; width: 100%; " width="100%">
                <table width="100%" style=" border:2px solid #e9e9e9;  border-collapse: collapse; " cellspacing="0" cellpadding="0">
                    <tr>
                        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Unit</th>
                        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Name</th>
                        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Date Of Last Payment</th>
                        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Total({Currency})</th>
                        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">0-30 Days({Currency})</th>
                        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">31-60 Days({Currency})</th>
                        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">61-90 Days({Currency})</th>
                        <th style="border:2px solid #e9e9e9; padding: 5px; font-family: arial;  font-size: 16px;">Over 90 Days({Currency})</th>
                    </tr> {tblbody} </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 11px; background: #00b0f0;"> </td>
        </tr>
    </table>

</div>','template_key' =>'DelinquentReports_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Credit Report', 'template_html' => '<div class="wrapper"><input type="hidden" name="templateEdit_key" id="templateEdit_key" value="CreditReport_Key"><table width="700" align="center" cellspacing="0" cellpadding="0" style="font-family:arial; font-size: 14px; background: #fff;"><tr><td><table width="100%" cellspacing="0" cellpadding="0" bgcolor="#2797f6" style="background: #2797f6;"><tr><td style="padding: 0 10px;" width="30px"> <img width="30px" src="images/credit-icon.png" alt="" style="display: inline-block;" /></td><td width="100%" valign="middle" style="padding: 15px 0px;"><h2 style="font-family:arial; font-size: 22px; font-weight: 400; margin:0; padding: 0; color: #fff;"> Credit Report</h2></td><td style="font-family:arial; font-size: 16px; font-weight: 400; margin:0; padding: 0 10px 0 0; color: #fff;">01/15/2015</td></tr></table></td></tr><tr><td style="padding: 20px 0;"><table width="100%" cellspacing="0" cellpadding="0"><tr><td style="padding: 0 5px 8px 5px;" width="50%"><h3 style="font-family:arial; font-size: 24px; font-weight: 600; margin:0; padding: 0; color: #000;">{FirstName} {LastName}</h3></td><td style="font-family:arial; font-size: 13px; font-weight: 400; margin:0; padding: 0 5px 0px 5px; color: #000; text-align: right;">	<strong>SSN Message: </strong> {BureauErrorMessage} (confirmed by bureau)</td></tr><tr><td width="100%" valign="top" colspan="2"><table width="100%" cellspacing="0" cellpadding="0"><tr><td colspan="3" style="font-family:arial; font-size: 18px; font-weight: 400; margin:0; padding: 0 5px 0px 5px; color: #2797f6;">Address</td></tr><tr><td width="33.33%" style="padding: 5px" valign="top"><table cellpadding="0" cellspacing="0" width="100%"><tr><td valign="top" width="20%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #464646;">Current Address:</p></td></tr>{AddressRequired}</table></td><td width="33.33%" style="padding: 5px" valign="top"><table cellpadding="0" cellspacing="0" width="100%"><tr><td valign="top" width="20%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #464646;">Address Discrepancy:</p></td></tr><tr><td valign="top" width="20%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 30px 0 0; color: #000; line-height: 18px;">{AddressDiscrepancyIndicator}</p></td></tr></table></td><td width="33.33%" style="padding: 5px" valign="top"><table cellpadding="0" cellspacing="0" width="100%"><tr><td valign="top" width="20%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #464646;">Phone:</p></td></tr><tr><td valign="top" width="20%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0; color: #000; line-height: 18px;">{PhoneNumber}</p></td></tr></table></td></tr><tr><td colspan="3" style="padding: 20px 5px 0 5px"><table cellpadding="0" cellspacing="0" width="100%"><tr><td style="font-family:arial; font-size: 18px; font-weight: 400; margin:0; padding: 0 0px 8px 0px; color: #2797f6;">Employment</td></tr>{EmploymentAddress}</table></td></tr><tr><td colspan="3"><hr/></td></tr><tr><td colspan="3" style="padding: 20px 5px 0 5px"><table cellpadding="0" cellspacing="0" width="100%"><tr><td style="font-family:arial; font-size: 18px; font-weight: 400; margin:0; padding: 0 0px 8px 0px; color: #2797f6;">Fraud Indicators</td></tr><tr><td><table cellpadding="0" cellspacing="0" width="100%"><tr><td style="padding: 5px 10px;color: #464646; font-family:arial; font-size: 12px; font-weight: 400;">Description</td></tr>{FraudIndicatorDescription}</table></td></tr></table></td></tr><tr><td colspan="3"><hr/></td></tr><tr><td colspan="3" style="padding: 20px 5px 0 5px"><table cellpadding="0" cellspacing="0" width="100%"><tr><td style="font-family:arial; font-size: 18px; font-weight: 400; margin:0; padding: 0 0px 8px 0px; color: #2797f6;">Profile Summary</td></tr><tr><td><table cellpadding="0" cellspacing="0" width="100%"><tr><td style="padding: 5px 10px;color: #464646; font-family:arial; font-size: 12px; font-weight: 400;">Record Counts</td></tr><tr><td><table cellspacing="0" cellpadding="0" width="100%"><tr><td align="center" width="25%"><h4 style="font-family:arial; font-size: 16px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; line-height: 22px;">Tradelines:</h4><p style="font-family:arial; font-size: 24px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; line-height: 22px;">{Tradeline}</p></td><td align="center" width="25%"><h4 style="font-family:arial; font-size: 16px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; line-height: 22px;">Collections:</h4><p style="font-family:arial; font-size: 24px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; ">{CollectionCount}</p></td><td align="center" width="25%"><h4 style="font-family:arial; font-size: 16px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; line-height: 22px;">Public Records:</h4><p style="font-family:arial; font-size: 24px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; ">{PublicRecordCount}</p></td><td align="center" width="25%"><h4 style="font-family:arial; font-size: 16px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; line-height: 22px;">Inquiries:</h4><p style="font-family:arial; font-size: 24px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; ">{NumberOfInquiries}</p></td></tr></table></td></tr></table></td></tr><tr><td style="padding: 15px 0 0 0"><table cellpadding="0" cellspacing="0" width="100%"><tr><td style="padding: 5px 10px;color: #464646; font-family:arial; font-size: 12px; font-weight: 400;">Derogatory Items</td></tr><tr><td><table cellspacing="0" cellpadding="0" width="100%"><tr><td align="center" width="33.33%"><h4 style="font-family:arial; font-size: 16px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; line-height: 22px;">Negative Tradelines:</h4><p style="font-family:arial; font-size: 24px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; line-height: 22px;">{NegTradelineCount}</p></td><td align="center" width="33.33%"><h4 style="font-family:arial; font-size: 16px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; line-height: 22px;">Tradelines with any historical negatives:</h4><p style="font-family:arial; font-size: 24px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; ">{HistNegTradelineCount}</p></td><td align="center" width="33.33%"><h4 style="font-family:arial; font-size: 16px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; line-height: 22px;">Occurrence of any historical negatives:</h4><p style="font-family:arial; font-size: 24px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; ">{OccuranceHistCount}</p></td></tr></table></td></tr></table></td></tr><tr><td style="padding: 15px 0 0 0;"><table cellspacing="0" cellpadding="0" width="100%"><thead><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Tradeline Summary</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Count</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">High Credit</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Credit Limit</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Balance</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Past Due</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Payment</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Available</th></tr></thead><tbody><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; text-align:left;">Revolving</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{TradelineRevolvingCount}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{TradelineRevolvingHighCredit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{TradelineRevolvingCreditLimit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{TradelineRevolvingBalance}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{TradelineRevolvingInstallmentPastDue}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{TradelineRevolvingMonthlyPayment}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{TradelineRevolvingPercentCreditAvail}</td></tr><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">Installment</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineInstallmentCount}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineInstallmentHighCredit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineInstallmentCreditLimit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineInstallmentBalance}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineInstallmentInstallmentPastDue}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineInstallmentMonthlyPayment}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineInstallmentPercentCreditAvail}</td></tr><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; text-align:left;">Mortgage</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{TradelineMortgageCount}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{TradelineMortgageHighCredit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{TradelineMortgageCreditLimit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{TradelineMortgageBalance}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{TradelineMortgageInstallmentPastDue}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{TradelineMortgageMonthlyPayment}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{TradelineMortgagePercentCreditAvail}</td></tr><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">Open</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineOpenCount}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineOpenHighCredit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineOpenCreditLimit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineOpenBalance}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineOpenInstallmentPastDue}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineOpenMonthlyPayment}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineOpenPercentCreditAvail}</td></tr><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; text-align:left;">Closed w Bal</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineClosed w BalCount}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineClosed w BalHighCredit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineClosed w BalCreditLimit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineClosed w BalBalance}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineClosed w BalInstallmentPastDue}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineClosed w BalMonthlyPayment}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineClosed w BalPercentCreditAvail}</td></tr><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; text-align:left;">Total</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineTotalCount}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineTotalHighCredit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineTotalCreditLimit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineTotalBalance}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineTotalInstallmentPastDue}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineTotalMonthlyPayment}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{TradelineTotalPercentCreditAvail}</td></tr></tbody></table></td></tr></table></td></tr><tr><td colspan="3"><hr/></td></tr><tr><td colspan="3" style="padding: 20px 5px 0 5px"><table cellpadding="0" cellspacing="0" width="100%"><tr><td style="font-family:arial; font-size: 18px; font-weight: 400; margin:0; padding: 0 0px 8px 0px; color: #2797f6;">Resident Score</td></tr><tr><td><table cellpadding="0" cellspacing="0" width="100%"><tr><td valign="top" style="padding: 5px 10px;color: #464646; font-family:arial; font-size: 32px; font-weight: 400;">{Score}</td><td valign="top"><h4 style="padding: 5px 10px;color: #464646; font-family:arial; font-size: 12px; font-weight: 400; margin:0;">Score Factors</h4><table cellspacing="0" cellpadding="0" width="100%"><tr><td><p style="padding: 5px 10px;color: #464646; font-family:arial; font-size: 12px; font-weight: 400; margin:0;">{ScoreFactor1}</p></td></tr><tr><td><p style="padding: 5px 10px;color: #464646; font-family:arial; font-size: 12px; font-weight: 400; margin:0;">{ScoreFactor2}</p></td></tr><tr><td><p style="padding: 5px 10px;color: #464646; font-family:arial; font-size: 12px; font-weight: 400; margin:0;">{ScoreFactor3}</p></td></tr><tr><td><p style="padding: 5px 10px;color: #464646; font-family:arial; font-size: 12px; font-weight: 400; margin:0;">{ScoreFactor4}</p></td></tr><tr><td><p style="padding: 5px 10px;color: #464646; font-family:arial; font-size: 12px; font-weight: 400; margin:0;">{RejectMessageCode}</p></td></tr></table></td></tr></table></td></tr></table></td></tr><tr><td colspan="3"><hr/></td></tr><tr><td colspan="3" style="padding: 20px 5px 0 5px"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td style="font-family:arial; font-size: 18px; font-weight: 400; margin:0; padding: 0 0px 8px 0px; color: #2797f6;">Tradelines</td></tr><tr><td><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td style="padding: 5px 10px;color: #464646; font-family:arial; font-size: 12px; font-weight: 400;">Tradeline Total</td></tr><tr><td><table cellspacing="0" cellpadding="0" width="100%"><tbody><tr><td align="center" width="25%"><h4 style="font-family:arial; font-size: 16px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; line-height: 22px;">Count:</h4><p style="font-family:arial; font-size: 24px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; line-height: 22px;">{TradelinesCount}</p></td><td align="center" width="25%"><h4 style="font-family:arial; font-size: 16px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; line-height: 22px;">Balance Total:</h4><p style="font-family:arial; font-size: 24px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; ">{TradelinesBalanceTotal}</p></td><td align="center" width="25%"><h4 style="font-family:arial; font-size: 16px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; line-height: 22px;"> Total Credit Limit:</h4><p style="font-family:arial; font-size: 24px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; ">{TradelinesTotalCreditLimit}</p></td><td align="center" width="25%"><h4 style="font-family:arial; font-size: 16px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; line-height: 22px;">Total Past Due:</h4><p style="font-family:arial; font-size: 24px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; ">{TradelinesTotalAmountPastDue}</p></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td style="padding: 15px 0 0 0"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td style="padding: 5px 10px;color: #464646; font-family:arial; font-size: 12px; font-weight: 400;">Trades</td></tr><tr><td colspan="3" style="padding: 5px 10px;color: #464646; font-family:arial; font-size: 30px; font-weight: 400;">PEOPLES ENGY</td></tr><tr><td><table cellspacing="0" cellpadding="0" width="100%"><tbody><tr><td width="33.33%" style="padding: 5px" valign="top"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Industry:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TradesIndustryCode}</p></td></tr><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Loan Type:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TradesLoanType}</p></td></tr><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Loan Terms:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TradesTermsFrequencyOfPayment}</p></td></tr></tbody></table></td><td width="33.33%" style="padding: 5px" valign="top"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Open:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TradesDateOpened}</p></td></tr><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Verified:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TradesDateVerified}</p></td></tr><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Closed:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TradesDateClosed}</p></td></tr><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Paid:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TradesDatePaidOut}</p></td></tr></tbody></table></td><td width="33.33%" style="padding: 5px" valign="top"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" width="60%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Balance:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TradesBalanceAmount}</p></td></tr><tr><td valign="top" width="60%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Past Due:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TradesAmountPastDue}</p></td></tr><tr><td valign="top" width="60%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Credit Limit:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TradesCreditLimit}</p></td></tr><tr><td valign="top" width="60%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Payment Amount:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TradesPaymentAmount}</p></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td style="padding: 15px 0 0 0"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table cellspacing="0" cellpadding="0" width="100%"><tbody><tr><td width="100%" style="padding: 5px" valign="top"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" width="20%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Account Type:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TradesAccountType}</p></td></tr><tr><td valign="top" width="15%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Status:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TradesStatus}</p></td></tr><tr><td valign="top" width="15%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Notes:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TradesNotes}</p></td></tr><tr><td valign="top" width="15%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">&nbsp;</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TradesNotes}</p></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td style="padding: 15px 0 0 0"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><p style="font-family:arial; font-size: 16px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Late Payment Summary	<span style="font-weight: 400;">(48 Months)</span></p></td></tr><tr><td><table cellpadding="0" cellspacing="0" width="45%"><tbody><tr><td width="10%" style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; text-align:center; padding: 0 0 5px 0; color: #000;">18</td><td width="10%" style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; text-align:center; padding: 0 0 5px 0; color: #000;">2</td><td width="10%" style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; text-align:center; padding: 0 0 5px 0; color: #000;">2</td><td width="10%" style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; text-align:center; padding: 0 0 5px 0; color: #000;">2</td></tr><tr><td width="10%" style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; text-align:center; padding: 0 0 5px 0; color: #f85353;"> <img width="35px" src="images/120.png" alt="" /></td><td width="10%" style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; text-align:center; padding: 0 0 5px 0; color: #f85353;"> <img width="35px" src="images/90.png" alt="" /></td><td width="10%" style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; text-align:center; padding: 0 0 5px 0; color: #f8b453;"> <img width="35px" src="images/60.png" alt="" /></td><td width="10%" style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; text-align:center; padding: 0 0 5px 0; color: #f8e353;"> <img width="35px" src="images/30.png" alt="" /></td></tr></tbody></table></td></tr></tbody></table></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td style="padding: 15px 5px;" colspan="2"><table cellspacing="0" cellpadding="0" width="100%"><thead><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Payment History</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Dec</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Nov</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Oct</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Sep</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Aug</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Jul</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Jun</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">May</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Apr</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Mar</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Feb</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Jan</th></tr></thead><tbody><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; text-align:left;">2009</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">&nbsp;</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">&nbsp;</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">&nbsp;</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">&nbsp;</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">&nbsp;</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">&nbsp;</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt="" /></td></tr><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; text-align:left;">2008</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt="" /></td></tr><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; text-align:left;">2007</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/tick.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/tick.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/tick.png" alt="" /></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/tick.png" alt="" /></td></tr></tbody></table></td></tr><tr><td colspan="3"><hr/></td></tr><tr><td style="padding: 15px 0 0 0" colspan="2"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td colspan="3" style="padding: 5px 10px;color: #464646; font-family:arial; font-size: 30px; font-weight: 400;">TURNER ACCEP</td></tr><tr><td><table cellspacing="0" cellpadding="0" width="100%"><tbody><tr><td width="33.33%" style="padding: 5px" valign="top"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Industry:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TurneraaccepIndustryCode}</p></td></tr><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Loan Type:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TurneraaccepLoanType}<p></td></tr><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Loan Terms:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TurneraaccepTermsFrequencyOfPayment}</p></td></tr></tbody></table></td><td width="33.33%" style="padding: 5px" valign="top"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Open:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TurneraaccepDateOpened}</p></td></tr><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Verified:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TurneraaccepDateVerified}</p></td></tr><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Closed:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TurneraaccepDateClosed}</p></td></tr><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Paid:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TurneraaccepDatePaidOut}</p></td></tr></tbody></table></td><td width="33.33%" style="padding: 5px" valign="top"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" width="60%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Balance:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TurneraaccepBalanceAmount}</p></td></tr><tr><td valign="top" width="60%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Past Due:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TurneraaccepAmountPastDue}</p></td></tr><tr><td valign="top" width="60%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Credit Limit:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TurneraaccepCreditLimit}</p></td></tr><tr><td valign="top" width="60%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Payment Amount:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TurneraaccepPaymentAmount}</p></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td style="padding: 15px 0 0 0" colspan="2"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table cellspacing="0" cellpadding="0" width="100%"><tbody><tr><td width="100%" style="padding: 5px" valign="top"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" width="20%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Account Type:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TurneraaccepAccountType}</p></td></tr><tr><td valign="top" width="15%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Status:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TurneraaccepStatus}</p></td></tr><tr><td valign="top" width="15%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Notes:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TurneraaccepNotes}</p></td></tr><tr><td valign="top" width="15%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">&nbsp;</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">Account closed because of charge-off or repossession 11/27/2008</p></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td style="padding: 15px 5px;" colspan="2"><table cellspacing="0" cellpadding="0" width="100%"><thead><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Payment History</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Dec</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Nov</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Oct</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Sep</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Aug</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Jul</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Jun</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">May</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Apr</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Mar</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Feb</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Jan</th></tr></thead><tbody><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; text-align:left;">2009</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">&nbsp;</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">&nbsp;</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">&nbsp;</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">&nbsp;</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">&nbsp;</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">&nbsp;</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td></tr><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; text-align:left;">2008</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td></tr><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; text-align:left;">2007</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/tick.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/tick.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/tick.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/tick.png" alt=""></td></tr></tbody></table></td></tr><tr><td colspan="3"><hr></td></tr><tr><td style="padding: 15px 0 0 0" colspan="2"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td colspan="3" style="padding: 5px 10px;color: #464646; font-family:arial; font-size: 30px; font-weight: 400;">TURNER ACCEP</td></tr><tr><td><table cellspacing="0" cellpadding="0" width="100%"><tbody><tr><td width="33.33%" style="padding: 5px" valign="top"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Industry:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TURNERACCEPIndustryCode}</p></td></tr><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Loan Type:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TURNERACCEPLoanType}</p></td></tr><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Loan Terms:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TURNERACCEPTermsFrequencyOfPayment}</p></td></tr></tbody></table></td><td width="33.33%" style="padding: 5px" valign="top"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Open:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TURNERACCEPDateOpened}</p></td></tr><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Verified:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TURNERACCEPDateVerified}</p></td></tr><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Closed:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TURNERACCEPDateClosed}</p></td></tr><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Paid:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TURNERACCEPDatePaidOut}</p></td></tr></tbody></table></td><td width="33.33%" style="padding: 5px" valign="top"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" width="60%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Balance:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TURNERACCEPBalanceAmount}</p></td></tr><tr><td valign="top" width="60%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Past Due:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TURNERACCEPAmountPastDue}</p></td></tr><tr><td valign="top" width="60%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Credit Limit:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TURNERACCEPCreditLimit}</p></td></tr><tr><td valign="top" width="60%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Payment Amount:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TURNERACCEPPaymentAmount}</p></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td style="padding: 15px 0 0 0" colspan="2"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><table cellspacing="0" cellpadding="0" width="100%"><tbody><tr><td width="100%" style="padding: 5px" valign="top"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" width="20%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Account Type:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TURNERACCEPAccountType}</p></td></tr><tr><td valign="top" width="15%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Status:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TURNERACCEPStatus}</p></td></tr><tr><td valign="top" width="15%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Notes:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TURNERACCEPNotes}</p></td></tr><tr><td valign="top" width="15%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">&nbsp;</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{TURNERACCEPNotes}</p></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td style="padding: 15px 5px" colspan="2"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><p style="font-family:arial; font-size: 16px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Late Payment Summary	<span style="font-weight: 400;">(48 Months)</span></p></td></tr><tr><td><table cellpadding="0" cellspacing="0" width="45%"><tbody><tr><td width="10%" style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; text-align:center; padding: 0 0 5px 0; color: #000;">18</td><td width="10%" style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; text-align:center; padding: 0 0 5px 0; color: #000;">2</td><td width="10%" style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; text-align:center; padding: 0 0 5px 0; color: #000;">2</td><td width="10%" style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; text-align:center; padding: 0 0 5px 0; color: #000;">2</td></tr><tr><td width="10%" style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; text-align:center; padding: 0 0 5px 0; color: #f85353;"> <img width="35px" src="images/120.png" alt=""></td><td width="10%" style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; text-align:center; padding: 0 0 5px 0; color: #f85353;"> <img width="35px" src="images/90.png" alt=""></td><td width="10%" style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; text-align:center; padding: 0 0 5px 0; color: #f8b453;"> <img width="35px" src="images/60.png" alt=""></td><td width="10%" style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; text-align:center; padding: 0 0 5px 0; color: #f8e353;"> <img width="35px" src="images/30.png" alt=""></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td style="padding: 15px 5px;" colspan="2"><table cellspacing="0" cellpadding="0" width="100%"><thead><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Payment History</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Dec</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Nov</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Oct</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Sep</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Aug</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Jul</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Jun</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">May</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Apr</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Mar</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Feb</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Jan</th></tr></thead><tbody><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; text-align:left;">2009</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">&nbsp;</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">&nbsp;</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">&nbsp;</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">&nbsp;</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">&nbsp;</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">&nbsp;</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td></tr><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; text-align:left;">2008</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/120.png" alt=""></td></tr><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; text-align:left;">2007</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/na.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/tick.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/tick.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/tick.png" alt=""></td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;"> <img width="25px" src="images/tick.png" alt=""></td></tr></tbody></table></td></tr><tr><td colspan="3"><hr></td></tr><tr><td colspan="3" style="padding: 20px 5px 0 5px"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td style="font-family:arial; font-size: 18px; font-weight: 400; margin:0; padding: 0 0px 8px 0px; color: #2797f6;">Collections</td></tr><tr><td><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td style="padding: 5px 10px;color: #464646; font-family:arial; font-size: 12px; font-weight: 400;">Collection Totals</td></tr><tr><td><table cellspacing="0" cellpadding="0" width="100%"><tbody><tr><td align="center" width="20%"><h4 style="font-family:arial; font-size: 16px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; line-height: 22px;">Count:</h4><p style="font-family:arial; font-size: 24px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; line-height: 22px;">{CollectionsCount}</p></td><td align="left" width="25%"><h4 style="font-family:arial; font-size: 16px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; line-height: 22px;">Balance Total:</h4><p style="font-family:arial; font-size: 24px; font-weight: 400; margin:0; padding: 0 0px 0px 0px; color: #000; ">{CollectionsBalanceTotal}</p></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td style="padding: 15px 0 0 0"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td style="padding: 5px 10px;color: #464646; font-family:arial; font-size: 12px; font-weight: 400;">Collections</td></tr><tr><td colspan="3" style="padding: 5px 10px;color: #464646; font-family:arial; font-size: 30px; font-weight: 400;">COLLECTION</td></tr><tr><td><table cellspacing="0" cellpadding="0" width="100%"><tbody><tr><td width="40.33%" style="padding: 5px" valign="top"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" width="40%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Industry:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{CollectionsIndustryCode}</p></td></tr><tr><td valign="top" width="40%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Loan Type/Terms:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{CollectionsLoanType}</p></td></tr><tr><td valign="top" width="40%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Account Type:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{CollectionsAccountType}</p></td></tr><tr><td valign="top" width="40%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Status:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{CollectionsStatus}</p></td></tr><tr><td valign="top" width="40%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Remarks:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{CollectionsRemarks}</p></td></tr><tr><td valign="top" width="40%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Comment:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{Collectionscomment}</p></td></tr><tr><td valign="top" width="40%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Credit Grantor:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{CollectionsCreditGrantor}</p></td></tr></tbody></table></td><td width="25%" style="padding: 5px" valign="top"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Open:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{CollectionsDateOpened}</p></td></tr><tr><td valign="top" width="35%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Verified:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{CollectionsDateVerified}</p></td></tr></tbody></table></td><td width="33.33%" style="padding: 5px" valign="top"><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td valign="top" width="60%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Current Balance:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{CollectionsCurrentBalance}</p></td></tr><tr><td valign="top" width="60%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Orignal Balance:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{CollectionsOrignalBalance}</p></td></tr><tr><td valign="top" width="60%"><p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0 0 5px 0; color: #000;">Past Due:</p></td><td valign="top"><p style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 0; color: #000; line-height: 18px;">{CollectionsAmountPastDue}</p></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td colspan="3"><hr></td></tr><tr><td style="padding: 15px 5px;" colspan="2"><table cellspacing="0" cellpadding="0" width="100%"><thead><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Tradeline Summary</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Count</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">High Credit</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Credit Limit</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Balance</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Past Due</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Payment</th><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background: #eaf6fa; text-align:left;">Available</th></tr></thead><tbody><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; text-align:left;">Revolving</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsRevolvingCount}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsRevolvingHighCredit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsRevolvingCreditLimit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsRevolvingBalance}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsRevolvingInstallmentPastDue}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsRevolvingMonthlyPayment}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsRevolvingPercentCreditAvail}</td></tr><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">Installment</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsInstallmentCount}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsInstallmentHighCredit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsInstallmentCreditLimit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsInstallmentBalance}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsInstallmentPastDue}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsInstallmentMonthlyPayment}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsInstallmentPercentCreditAvail}</td></tr><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; text-align:left;">Mortgage</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsMortgageCount}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsMortgageHighCredit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsMortgageCreditLimit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsMortgageBalance}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsMortgageInstallmentPastDue}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsMortgageMonthlyPayment}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsMortgagePercentCreditAvail}</td></tr><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">Open</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsOpenCount}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsOpenHighCredit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsOpenCreditLimit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsOpenBalance}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsOpenInstallmentPastDue}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsOpenMonthlyPayment}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsOpenPercentCreditAvail}</td></tr><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; text-align:left;">Closed w Bal</th><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsClosed w BalCount}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsClosed w BalHighCredit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsClosed w BalCreditLimit}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsClosed w BalBalance}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsClosed w BalInstallmentPastDue}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsClosed w BalMonthlyPayment}</td><td style="font-family:arial; font-size: 12px; font-weight: 400; margin:0; padding: 10px 3px; color: #000; text-align:left;">{CollectionsClosed w BalPercentCreditAvail}</td></tr><tr><th style="font-family:arial; font-size: 14px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">Total</th><td style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsTotalCount}</td><td style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsTotalHighCredit}</td><td style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsTotalCreditLimit}</td><td style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsTotalBalance}</td><td style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsTotalInstallmentPastDue}</td><td style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsTotalMonthlyPayment}</td><td style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 10px 3px; color: #000; background:#eaf6fa; text-align:left;">{CollectionsTotalPercentCreditAvail}</td></tr></tbody></table></td></tr></table></td></tr><tr><td style="padding: 15px 5px; background: #000;"> <img src="images/logo.png" alt="" width="100px" /></td></tr></table></td></tr></table></div>','template_key' =>'CreditReport_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Eviction Report', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="EvictionReport_Key">

<table width="100%" cellspacing="0" cellpadding="0" bgcolor="#229015" style="background: #229015;"> <tr> <td style="padding: 0 10px;"> <img src="{imgUrl}" alt="" style="display: inline-block;" /> </td> <td width="100%" valign="middle" style="padding: 15px 0px;"> <h2 style="font-family:arial; font-size: 22px; font-weight: 400; margin:0; padding: 0; color: #fff;"> Evictions Report</h2> </td> </tr> </table> </td> </tr> <tr> <td style="padding: 30px 5px 5px 5px;"> <table width="100%" cellspacing="0" cellpadding="0"> <tr> <td colspan="2" style="padding: 0 0 8px 0;"> <h3 style="font-family:arial; font-size: 16px; font-weight: 600; margin:0; padding: 0; color: #229015;">Applicant Information Submitted</h3> </td> </tr> <tr> <td width="100%" valign="top"> <table width="100%" cellspacing="0" cellpadding="0"> <tr> <td width="50%" style="padding: 5px" valign="top"> <table cellpadding="0" cellspacing="0" width="100%"> <tr> <td valign="top" width="20%"> <p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0; color: #404040;"> Name:</p> </td> <td style="padding: 0 0 0 5px" valign="top"> <p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0; color: #404040;"> {FirstName} {LastName}</p> </td> </tr> </table> </td> <td style="padding: 5px" valign="top"> <table cellpadding="0" cellspacing="0" width="100%"> <tr> <td valign="top" width="40%"> <p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0; color: #404040;"> Requested By:</p> </td> <td style="padding: 0 0 0 5px" valign="top"> <p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0; color: #404040;"> {RequestedBy}</p> </td> </tr> </table> </td> </tr> <tr> <td width="50%" style="padding: 5px" valign="top"> <table cellpadding="0" cellspacing="0" width="100%"> <tr> <td valign="top" width="20%"> <p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0; color: #404040;"> Address:</p> </td> <td style="padding: 0 0 0 5px" valign="top"> <p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0; color: #404040;">{NewStreetAddress}, <br/>{NewCity},{NewState} {NewZipCode} </p> </td> </tr> </table> </td> <td style="padding: 5px" valign="top"> <table cellpadding="0" cellspacing="0" width="100%"> <tr> <td width="40%"> <p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0; color: #404040;"> Date/Time Requested: </p> </td> <td style="padding: 0 0 0 5px"> <p style="font-family:arial; font-size: 12px; font-weight: 600; margin:0; padding: 0; color: #404040;"> {RequestedDateTime}</p> </td> </tr> </table> </td> </tr> <tr> <td colspan="3" style="padding: 20px 0 0 0"> <table cellpadding="0" cellspacing="0" width="100%" align="left" bgcolor="#f8fefe" style="background: #f8fefe;"> <thead> <tr> <th style="padding: 10px 10px; font-family:arial; font-size: 13px; font-weight: 600; margin:0;color: #000; text-align: left; background: #e6f3f9;">Results</th> </tr> </thead> <tbody> <tr> <td style="padding: 10px;"> <table cellpadding="0" cellspacing="0" width="100%"> {Results} </table> </td> </tr> </tbody> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table>

</div>','template_key' =>'EvictionReport_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'TransUnion Report', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="TransUnionReport_Key">

<table width="700" align="center" cellspacing="0" cellpadding="0" style="font-family:arial; font-size: 14px; background: #fff;">
         <tr>
            <td>
               <table width="100%" cellspacing="20" cellpadding="0" bgcolor="#2797f6" style="background: #000000;">
                  <tr>
                     <td  style="padding: 0 10px;float:left;width:700px;">
                        <img width="" src="images/logo.png" alt="" style="display: inline-block;" />
                       <a href="javascript:;"> <img width="" src="images/print.jpg" alt="" style="display: inline-block;float:right;padding: 12px 0 0 0;" /></a>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td style="padding:0px 0;">
               <table width="100%" cellspacing="10" cellpadding="0" style="background:#fcd800;">
                  <tr>
                     <td style="padding: 0;" width="50%">
                        <img src="images/link.png" alt="link" title="link">
                        <span style="display: inline-block; padding: 11px 0 0 13px; vertical-align: top; font-size: 40px;">Criminal Report</span>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td style="padding:0px 0;position:relative;">
               <table width="100%" cellspacing="10" cellpadding="0" style="background:#fff;position:relative;">
                
                  <tr>
                     <td style="padding: 0;" width="50%">
                        <span style="color:#e0c000;font-size:17px;font-family:arial;font-size:18px;text-align:left;">Applicant Data as Entered</span>
                        <span style="display:block;padding:5px 0 0 0;color:#000;"><b>Name:</b> {FullName}</span>
                     </td>
                     <td style="padding: 0;" width="50%">
                        <span style="display:block;padding:17px 0 0 0;color:#000;"><b>Requested By:</b> {Requested By}</span>
                        <span style="display:block;padding:5px 0 0 0;color:#000;"><b>Property:</b> {Property}</span>
                        <span style="display:block;padding:5px 0 0 0;color:#000;"><b>Date/Time: </b> 8/27/2014 4:27 AM (Mountain)</span>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td style="padding:0px 0;">
               <table width="100%" cellspacing="10" cellpadding="0" style="background:#fff;">
                  <tr>
                     <td style="padding: 0;" width="100%">
                        <span style="color:#000;font-family:arial;font-size:25px;text-align:left;">MARK HANSEN</span>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td style="padding:0px 0;">
               <table width="100%" cellspacing="10" cellpadding="0" style="background:#fff;position:relative;">
                  <tr>
                     <td style="padding: 0;" width="25%">
                        <img src="images/photo-unvailbale..jpg" alt="img" title="img" width="288xpx">
                     </td>
                     <td style="padding: 0 0 0 10px;display:inline-block;" width="50%">
                        <span style="display:block;padding:0px 0 0 0;color:#000;"><b>State:</b> {LocationStateName}</span>
                        <span style="display:block;padding:10px 0 0 0;color:#000;"><b>Age:</b>{PersonAgeDescriptionText} </span>
                        <span style="display:block;color:#e0c000;font-size:17px;font-family:arial;font-size:18px;text-align:left;padding:33px 0 0 0;"> Aliases</span>
                        <span style="display:block;padding:5px 0 0 0;color:#000;">{Aliases}</span>
                        <span style="display:block;color:#e0c000;font-size:17px;font-family:arial;font-size:18px;text-align:left;padding:33px 0 0 0;">  Physical Features</span>
                        <span style="display:block;padding:5px 0 0 0;color:#000;">Physical Details</span>
                        <span style="display:block;padding:10px 0 0 0;color:#000;"><b>Sex:</b>{Value}</span>
                        <span style="display:block;padding:10px 0 0 0;color:#000;"><b>Heights:</b>{PersonHeightMeasure}</span>
                        <span style="display:block;padding:10px 0 0 0;color:#000;"><b>Weight:</b>{PersonWeightMeasure}</span> 
                        <span style="display:block;padding:10px 0 0 0;color:#000;"><b>Race:</b>{PersonRaceText}</span>
                        <span style="display:block;padding:22px 0 0 0;color:#000;">Person Physical Features</span>
                        <span style="display:block;padding:5px 0 0 0;color:#000;"> No physical features listed</span>
                     </td>
                  </tr>
                
               </table>
            </td>
         </tr>
         <tr>
            <td style="padding:0px 0;">
               <hr>
               <table width="100%" cellspacing="10" cellpadding="0" style="background:#fff;">
                  <tr>
                     <td colspan="3" style="padding: 0;" width="100%">
                        <span style="display:block;color:#e0c000;font-size:17px;font-family:arial;font-size:22px;text-align:left;padding:0px 0 0 0;">Summary</span>
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 0;" width="33%">
                        <span style="display:block;color:#000;font-size:15px;font-family:arial;font-size:15px;text-align:center;padding:20px 0 0 0;"><b>Incident(s): <br>		
                        <span style="color:#464646;font-weight:500;font-family:arial;font-size:25px;">{Incident}</span></b>  
                        </span>
                     </td>
                     <td style="padding: 0;" width="33%">
                        <span style="display:block;color:#000;font-family:arial;font-size:15px;text-align:center;padding:20px 0 0 0;"><b>Booking(s): <br>		
                        <span style="color:#464646;font-weight:500;font-family:arial;font-size:25px;">{Booking}</span></b>  
                        </span>
                     </td>
                     <td style="padding: 0;" width="33%">
                        <span style="display:block;color:#000;font-size:15px;font-family:arial;text-align:center;padding:20px 0 0 0;"><b>Arrest(s):</b>  <br>		
                        <span style="color:#464646;font-weight:500;font-family:arial;font-size:25px;">{Arrest}</span></b>  
                        </span>
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 0;" width="33%">
                        <span style="display:block;color:#000;font-size:15px;font-family:arial;font-size:15px;text-align:center;padding:20px 0 0 0;"><b>Court Action(s):	</b><br>
                        <span style="color:#464646;font-weight:500;font-family:arial;font-size:25px;">{CourtAction}</span></b>  						
                        </span>
                     </td>
                     <td style="padding: 0;" width="33%">
                        <span style="display:block;color:#000;font-family:arial;font-size:15px;text-align:center;padding:20px 0 0 0;"><b>Sentencing(s):</b>  <br>
                        <span style="color:#464646;font-weight:500;font-family:arial;font-size:25px;">{Sentencing}</span></b>
                        </span>
                     </td>
                     <td style="padding: 0;" width="33%">
                        <span style="display:block;color:#000;font-size:15px;font-family:arial;text-align:center;padding:20px 0 0 0;"><b>Supervision(s):  </b>  <br>
                        <span style="color:#464646;font-weight:500;font-family:arial;font-size:25px;">{Supervision}</span></b>
                        </span>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td style="padding:0px 0;">
               <hr>
               <table width="100%" cellspacing="10" cellpadding="0" style="background:#fff;position:relative;">
                 
                  <tr>
                     <td colspan="3" style="padding: 0;" width="100%">
                        <span style="display:block;color:#e0c000;font-size:17px;font-family:arial;font-size:22px;text-align:left;padding:0px 0 0 0;">Comments</span>
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 0;" width="33%">
                        <span style="display:block;padding:10px 0 0 0;color:#000;"><b>  Status:</b> {Status} </span>
                     </td>
                     <td style="padding: 0;" width="33%">
                        <span style="display:block;padding:10px 0 0 0;color:#000;"><b>  Charge Number: </b>{ChargeNumber}</span>
                     </td>
                     <td style="padding: 0;" width="33%">
                        <span style="display:block;padding:10px 0 0 0;color:#000;text-align:right;"><b>  Citation Number: </b>{Citation}</span>
                     </td>
                  </tr>
               </table>
               <br>
               <hr>
            </td>
         </tr>
         <tr>
            <td style="padding:0px 0;">
               <table width="100%" cellspacing="10" cellpadding="0" style="background:#fff;position:relative;">
                  <tr>
                     <td colspan="3" style="padding: 0;" width="100%">
                        <span style="display:block;color:#e0c000;font-size:17px;font-family:arial;font-size:22px;text-align:left;padding:0px 0 0 0;">Court Action</span>
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 0;" width="33%">
                        <span style="display:block;padding:10px 0 0 0;color:#000;"><b>  Activity Type:</b> {ActivityTypeText}	 </span>
                     </td>
                     <td style="padding: 0;" width="33%">
                        <span style="display:block;padding:10px 0 0 0;color:#000;"><b>   Court Record Id: </b>{CourtRecordId}</span>
                     </td>
                  </tr>
               </table>
               <br>
            </td>
         </tr>
         <tr>
            <td style="padding:0px 0;">
               <table width="100%" cellspacing="10" cellpadding="0" style="background:#fff;position:relative;">
                  <tr>
                     <td colspan="2" style="padding: 0;" width="100%">
                        <span style="display:block;color:#e0c000;font-size:17px;font-family:arial;font-size:22px;text-align:left;padding:0px 0 0 0;">Court</span>
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 0;" width="100%">
                        <span style="display:block;padding:10px 0 0 0;color:#000;text-transform:uppercase">Organization Jurisdiction</span>
                        <span style="display:block;padding:10px 0 0 0;color:#000;text-align:left;"><b>  Jurisdiction Description:</b>{JurisdictionDescriptionText}</span>
                     </td>
                  </tr>
               </table>
               <br>
            </td>
         </tr>
         <tr>
            <td style="padding:0px 0;">
               <table width="100%" cellspacing="10" cellpadding="0" style="background:#fff;position:relative;">
		
                  <tr>
                     <td style="padding: 0;" width="100%">
                        <span style="display:block;color:#e0c000;font-size:17px;font-family:arial;font-size:22px;text-align:left;padding:0px 0 0 0;">Court Change</span>
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 0;" width="100%">
                        <span style="display:block;padding:10px 0 0 0;color:#000;text-align:left;"><b>  Effective Date:</b>{EffectiveDate}</span>
                        <span style="display:block;padding:10px 0 0 0;color:#000;text-align:left;"><b>  Charge Sequence Id:</b>Charge Number: {ChargeSequenceId}</span>
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 0;" width="100%">
                        <span style="display:block;padding:0px 0 0 0;color:#000;text-align:left;"><b>  Charge Description:</b>{ChargeDescriptionText}   </span>
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 0;" width="100%">
                        <span style="display:block;padding:20px 0 0 0;color:#000;text-align:left;">CHARGE CLASSIFICATION</span> 
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 0;" width="100%">
                        <span style="display:block;padding:0px 0 0 0;color:#000;text-align:left;"><b>  Charge Degree: </b>{ChargeDegreeText}</span>
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 0;" width="100%">
                        <span style="display:block;padding:20px 0 0 0;color:#000;text-align:left;">CHARGE DIPOSITION</span> 
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 0;" width="100%">
                        <span style="display:block;padding:0px 0 0 0;color:#000;text-align:left;"><b> ChargeDispositionAdditionalInformation: </b>Status: (Citation Number: {Citation})</span>
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 0;" width="100%">
                        <span style="display:block;padding:0px 0 0 0;color:#000;text-align:left;"><b> Charge Disposition Date:  </b>{ChargeDispositionDate}</span>
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 0;" width="100%">
                        <span style="display:block;padding:0px 0 0 0;color:#000;text-align:left;"><b> Disposition:  </b>{ChargeDispositionDescriptionText}</span>
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 0;" width="100%">
                        <span style="display:block;padding:20px 0 0 0;color:#000;text-align:left;">CHARGE STATUS</span> 
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 0;" width="100%">
                        <span style="display:block;padding:0px 0 0 0;color:#000;text-align:left;"><b>  Expiration Date: </b>{Expiration Date}</span>
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 0;" width="100%">
                        <span style="display:block;padding:20px 0 0 0;color:#000;text-align:left;">CHARGE STATUE</span> 
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 0;" width="100%">
                        <span style="display:block;padding:0px 0 0 0;color:#000;text-align:left;"><b>  Statute Code Id: </b>{StatuteCodeID}</span>
                     </td>
                  </tr>
				  
               </table>
               <br>
            </td>
         </tr>
         <tr>
            <td style="padding:0px 0;">
               <table style="background:#fff;position:relative;" cellspacing="10" cellpadding="0" width="100%">
                  <tbody>
                     <tr>
                        <td colspan="2" style="padding: 0;" width="100%">
                           <span style="display:block;color:#e0c000;font-size:17px;font-family:arial;font-size:22px;text-align:left;padding:0px 0 0 0;"> Disposition</span>
                        </td>
                     </tr>
                     <tr>
                        <td style="padding: 0;" width="100%">
                           <span style="display:block;padding:10px 0 0 0;color:#000;text-align:left;"><b> Disposition: </b>{Disposition}        </span>
                        </td>
                     </tr>
                     <tr>
                        <td style="padding: 0;" width="100%">
                           <span style="display:block;padding:0px 0 0 0;color:#000;text-align:left;"><b> Disposition Date:  </b>{ChargeDispositionDate}     </span>
                        </td>
                     </tr>
                  </tbody>
               </table>
               <br>
               <hr>
            </td>
         </tr>
		 
		 <tr>
            <td style="padding:0px 0;">
               <table style="background:#fff;position:relative;padding: 30px 0 0 0;" cellspacing="10" cellpadding="0" width="100%">
                  <tbody>
                     <tr>
                        <td style="padding: 0;" width="10%">
                        </td>
						  <td style="padding: 0;" width="80%">
                           <span style="display:block;color:#000000;font-size:15px;font-weight:600;font-family:arial;text-align:left;padding:0px 0 0 0;"> ATTENTION</span>
					   <p style="font-family: arial; text-align: justify; line-height: 25px; color: #000; font-size: 13px;">All data provided is intended solely for the customer who initially receives such data from the provider. The provider cannot guarantee or warrant
						the accuracy, correctness, or completeness of the data. The provider delivers all data to customers on an \'\'as is\'\' \'\'as available\'\' basis without any
						express or implied warranty, guaranty, or representation of any kind concerning the data itself, its merchantability, or its fitness for a particular
						purpose or function. Neither the provider nor any of their affiliates shall be liable for any damages of whatever kind may result from the
						customer\'\'s reliance on (or use of) the data provided, even if the provider, or any of their affiliates has been alerted to the possibility of such
						damages. The data information may include records that have been expunged, sealed, or otherwise have become inaccessible to the public since
						that date. By accessing any such data, the customer acknowledges and agrees that the customer has not relied on anything that may be
						inconsistent with this Legal Statement.
							</p>
                        </td>
						
						  <td style="padding: 0;" width="10%">
                        </td>
                     </tr>
            
                  </tbody>
               </table>
               <br>
            </td>
         </tr>
		        <tr>
            <td>
               <table width="100%" cellspacing="15" cellpadding="0" bgcolor="#2797f6" style="background: #000000;">
                  <tr>
                     <td  style="padding: 0 10px;float:left;width:700px;">
                        <img width="150" src="images/logo.png" alt="" style="display: inline-block;" />
                      
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>

</div>','template_key' =>'TransUnionReport_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Billing and Subscribed', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="BillingSubscribed_Key">


<div style="float:left; width:100%;"> <table style="padding:10px 25px 25px 25px; width: 100%;" cellspacing="0" cellpadding="0"> <tbody> <tr> 
<td> {CompanyLogo}</td>

</tr> 
<tr> 
<td colspan="2"> 
<h5 style="background: #00359e; font-family: arial; font-size: 16px; color: #fff; text-align: center; padding: 2px 10px;">Billing & Subscription</h5> 
<table style="width: 100%;" cellspacing="0" cellpadding="0">
 <tr> 
 <td style="font-family: arial; font-weight: bold; font-size: 14px; padding: 2px 5px;" width="20%">Billed To:</td> </tr>
 <tr> <td style="font-family: arial;font-size: 14px; padding:2px 5px;" width="20%">{CompanyName}</td> </tr> 
 <tr> <td style="font-family: arial; font-size: 14px; padding:2px 5px;" width="20%">{ZipCode}</td>  </tr> 
 <tr> <td style="font-family: arial; font-size: 14px; padding:2px 5px;" width="20%">{Address}</td> </tr> 
 </table> 
 </td></tr>
 <tr> 
 <td colspan="2"> <h5 style="background: #00359e; font-family: arial; font-size: 16px; color: #fff; text-align: left; padding: 2px 10px; margin: 20px 0 0;">
 Subscription Details</h5>
 <table style="width: 100%;" cellspacing="0" cellpadding="0">
 <tbody> 
 <tr><th style="background: #e6f6fe; font-family: arial; font-size: 13px; padding:2px 7px; border: 1px solid #444;" width="33%">Date</th>
 <th style="background: #e6f6fe; font-family: arial; font-size: 13px; padding:2px 7px; border: 1px solid #444; border-left: 0;" width="33%">Item</th>
 <th style="background: #e6f6fe; font-family: arial; font-size: 13px; padding:2px 7px; border: 1px solid #444; border-left: 0;" width="34%">Amount</th>
 </tr>
 <tr>
 {ListSubscriptionInfo}
 </tr>
</tbody> </table> 
</td> </tr>
 <tr> 
 <td colspan="2"> 
 <table style="width: 100%;" cellspacing="0" cellpadding="0"> 
 <tbody> 

 <tr> 
 <td colspan="2">
 <hr style="border: 1px dashed #444; height: 1px; margin-top: 20px;" /></td>
 </tr> <tr> 
 <td style="font-family: arial; font-size: 14px; text-align: left; font-style: italic;">Thank you! for subscribing to Apexlink <br /></td> 
 <td>&nbsp;</td> </tr>
 </tbody> </table> </td> </tr> 
  </tbody> </table> </div>

</div>','template_key' =>'BillingSubscribed_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
array('template_title' => 'Stripe Account', 'template_html' => '<div class="wrapper">
<input type="hidden" name="templateEdit_key" id="templateEdit_key" value="StripeAccount_Key">

<table>
<tbody>
Hello {User},
<br/><br/>
Your stripe account has been created, So please go to your portal and complete stripe account verification and then
get advantage of online payments.
<br/><br/>
Account Number : <b>{AccountNumber}</b><br/>
Phone : <b>{AccountPhone}</b>
<br/><br/>
Thanks,
<br/>Team Apexlink

</tbody>
</table>

</div>','template_key' =>'StripeAccount_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

);

$stm = $connection->prepare("TRUNCATE TABLE emailtemplatesadmin");

$stm->execute();

foreach($data as $rows)
{
$query = "INSERT INTO `emailtemplatesadmin`( `template_title`,`template_html`,`template_key`,`created_at`,`updated_at`) VALUES
('{$rows['template_title']}','{$rows['template_html']}','{$rows['template_key']}','{$rows['created_at']}','{$rows['updated_at']}')";
$stm = $connection->prepare($query);
$stm->execute();
}
return array('status' => 'success', 'code'=> 200);
print_r("sfa");

}catch (Exception $exception)
{
print_r($exception);
}
?>