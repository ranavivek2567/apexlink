<?php
/**
 * DDL class used for DDL operations
 * Created by PhpStorm.
 * User: Parvesh
 * Date: 17-April-19
 */

include ("$_SERVER[DOCUMENT_ROOT]/config.php");
class MigrationCusers extends DBConnection {

    //public $conn;
    function __construct() {
        parent::__construct();
    }

    /*Drop database function starts*/
    public static function dropDatabase() {
        $db = new DBConnection();
        $dbDetails = $db->conn->query("SELECT * FROM users where user_type!='0'")->fetchAll();
        if (isset($dbDetails)) {
            foreach ($dbDetails as $key => $udata) {
                $connection = DBConnection::dynamicDbConnection($udata['host'], $udata['database_name'], $udata['db_username'], $udata['db_password']);
                $database_name = $udata['database_name'];
                //get all of the tables

                $target_tables  = $connection->query('SHOW TABLES')->fetchAll();
                $backup_name        = "";
                foreach($target_tables as $table){
                    $database_name_result = 'Tables_in_'.$database_name;
                    $table = $table[$database_name_result];

                    $result         =   $connection->query('SELECT COUNT(*) as total_rows FROM '.$table);
                    $fields_amount  =   $result->fetch();
                    $fields_amount  =   $fields_amount['total_rows'];

                    $res            =   $connection->query('SHOW CREATE TABLE '.$table);
                    $TableMLine     =   $res->fetch();
                    $TableMLine     =   $TableMLine['Create Table'];
                    $content        = (!isset($content) ?  '' : $content) . "\n\n".$TableMLine.";\n\n";



                    $column_names   =   $connection->query("SELECT DISTINCT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME =  '".$table."'");
                    $column_names_arr   =   $column_names->fetchAll();

                    foreach ($column_names_arr as $k => $v){
                        $column_names_arr1[$k] = $v['COLUMN_NAME'];
                    }
//                    echo "<pre>"; print_r($column_names_arr1);die();
                    array_shift($column_names_arr1);
                    $column_names   =   implode( $column_names_arr1, ',');


                    $column_data   =   $connection->query("SELECT * FROM  $table");
                    $column_data   =   $column_data->fetchAll();

                    if ($fields_amount >= 1) {
                        $content .= "\nINSERT INTO " . $table;
                        $content .= "\n(" . $column_names;
                        $content .= "\n) VALUES";
                    }

//
                    for ($i = 0; $i < $fields_amount;   $i++) {
                        $content .= "\n(";
                        foreach ($column_names_arr1 as $k1 => $v1) {
                            if (isset($column_data[$i][$v1])) {
                                if (gettype($column_data[$i][$v1]) == 'integer'){
                                    $content .= $column_data[$i][$v1];
                                } else {
                                    $content .= "'" . $column_data[$i][$v1] . "'";
                                }
                            } else {
                                $content .= "NULL";
                            }
                            $content.= ',';
                        }

                        $content = rtrim($content, ',');
                        $content .="),";
                    }
                    $content = rtrim($content, ',');

                    $content .="\n\n\n";
                }
                $backup_name = $backup_name ? $backup_name : $database_name."_  _(".date('H-i-s')."_".date('d-m-Y').")__rand".rand(1,11111111).".sql";
                $backup_name = $backup_name ? $backup_name : $database_name.".sql";
                header('Content-Type: application/octet-stream');
                header("Content-Transfer-Encoding: Binary");
                header("Content-disposition: attachment; filename=\"".$backup_name."\"");

                $stm = $db->conn->prepare("DROP Database IF EXISTS $database_name");
                $stm->execute();

                $stm = $db->conn->prepare("DELETE FROM users WHERE  database_name = $database_name");
                $stm->execute();

                echo $content; exit;
            }
        }
    }
    /*Drop database function ends*/
    /**
     *  Insert Data to Company Users
     */
    public static function runMigrations($connection, $runQuery = null) {
        $return = array();
        try {

            self::createMigrationTable($connection);
            self::createUserTable($connection);
            self::company_vendor_type($connection);
            self::createActiveInactiveStatus($connection);
            self::createCompanyUnitType($connection);
            self::createCompanyPropertyType($connection);
            self::createCompanyPropertyStyle($connection);
            self::createCompanyPropertySubType($connection);
            self::createCompanyPropertyGroups($connection);
            self::createCompanyPropertyAmenities($connection);
            self::createPropertyPortfolio($connection);
            self::createPropertyCustomFields($connection);
            self::createPropertyPlansHistory($connection);
            self::createPropertyPlans($connection);
            self::createPropertyEmailSignatures($connection);
            self::createPropertyCompanypreference($connection);
            self::createPropertyTimezone($connection);
            self::createPropertyAuditTrial($connection);
            self::createPropertyDefaultCurrency($connection);
            self::createPropertyDateClockSettings($connection);
            self::createPropertyCompanyTimeSettings($connection);
            self::createPropertyDefaultSettings($connection);
            self::createPropertyType($connection);
            self::createPropertyZipCodeMaster($connection);
            self::company_inventory_preferences($connection);
            self::company_severity_type($connection);
            self::company_category_type($connection);
            self::company_priority_type($connection);
            self::company_event_type($connection);
            self::company_manage_vehicle($connection);
            self::createLeaseGuestCard($connection);
            self::createESignHistory($connection);
            self::company_maintenance_subcategory($connection);
            self::company_accounting_preferences($connection);
            self::company_accounting_charge_code_preferences($connection);
            self::company_accounting_bank_account($connection);
            self::company_account_type($connection);
            self::company_chart_of_accounts($connection);
            self::company_accounting_tax_setup($connection);
            self::company_accounting_charge_code($connection);
            self::list_of_volumes($connection);
            self::list_of_reason($connection);
            self::company_workorder_type($connection);
            self::createCompanyUserRoles($connection);
            self::createCompanySubAccountType($connection);
            self::createTenantDetails($connection);
            self::createTenantAdditionalDetails($connection);
            self::createTenantPhone($connection);
            self::createTenantSSNId($connection);
            self::createTenantParking($connection);
            self::createTenantMedicalAllergies($connection);
            self::createTenantCollection($connection);
            self::createTenantCollectionReason($connection);
            self::createTenantCredentils($connection);
            self::createTenantCredentilsType($connection);
            self::createTenantHobby($connection);
            self::createTenantHobbies($connection);
            self::createReferralSource($connection);
            self::createTenantServiceAnimal($connection);
            self::createTenantServiceAnimalGender($connection);
            self::createTenantPet($connection);
            self::createTenantPetGender($connection);
            self::createEthnicity($connection);
            self::createTenantFileLibrary($connection);
            self::createTenantChargeCode($connection);
            self::createTenantGuarantor($connection);
            self::createTenantmaritalStatus($connection);
            self::createTenantLeaseDetails($connection);
            self::createTenantReferralSource($connection);
            self::createTenantCharges($connection);
            self::createTenantCharge($connection);
            self::createTenantTax($connection);
            self::createTenantComplaint($connection);
            self::createTenantHOAType($connection);
            self::createTenantHOA($connection);
            self::createTenantMaintenance($connection);
            self::createTenantNotes($connection);
            self::createTenantProperty($connection);
            self::createTenantMoveInListRecord($connection);
            self::createTenantrenterInsurance($connection);
            self::createTenantTransfer($connection);
            self::alterTenantTransfer($connection);
            self::createTenantTaxPass($connection);
            self::createVeteranStatus($connection);
            self::createTenantVehicles($connection);
            self::createCarrier($connection);
            self::createPhoneType($connection);
            self::createCountry($connection);
            self::createTenantEmergencyDetails($connection);
            self::createTenantFinalMoveIn($connection);
            self::createTenantChargeFiles($connection);
            self::createTenantChargeNotes($connection);
            self::createTenantRentalLeaseSignaures($connection);
            self::company_property_unit($connection);
            self::company_maintenance_supplier($connection);
            self::company_maintenance_brand($connection);
            self::createCompanyRoleAuthorization($connection);
            self::createCompanyCreditAccounts($connection);
            self::createCompanyDebitAccounts($connection);
            self::createCompanyAccountingPreferences($connection);
            //property tables
            self::createGeneralProperty($connection);
            self::createPropertyFileUploads($connection);
            self::createKeyTracker($connection);
            self::createLateFeeManagement($connection);
            //            self::createLinkOwners($connection);

            self::createMaintenanceInformation($connection);
            self::createManagementFees($connection);
            self::createManagementInfo($connection);
            self::createCustomFields($connection);
            self::createCompanyGarageAvail($connection);
            self::createCompanyKeyAccess($connection);
            self::createCompanyPetFriendly($connection);
            self::createOwnerBlacklistVendors($connection);
            self::createMortgageInformation($connection);
            self::createPropertyInsurance($connection);
            self::createWarrantyInformation($connection);
            self::createPropertyNotes($connection);
            self::createPropertyLoan($connection);
            self::createRenovation_details($connection);
            self::createCompanyPropertyInsurance($connection);
            self::createFixture_types($connection);
            self::createCompanyPropertyPolicy($connection);
            self::createCompanyManagementReason($connection);
            self::createCompanyKeyTracker($connection);
            self::createPropertyBankAccount($connection);
            self::createUnitDetails($connection);
            self::createUnitGroupFields($connection);
            self::createUnitImages($connection);
            self::createUnitKeys($connection);
            self::createFlags($connection);
            self::createBuildingDetails($connection);
            self::createBuildingFileUploads($connection);
            self::createBuildingKeys($connection);
            self::createBuildingTrackKeys($connection);
            self::createComplaints($connection);
            self::createComplaintTypes($connection);
            self::createPropertyManageCharges($connection);
            self::createPropertyWatingList($connection);
            self::createPropertyAccountName($connection);
            self::createPropertyAccountType($connection);
            self::unit_track_key($connection);
            self::unit_key_checkout($connection);
            self::unit_keys($connection);
            self::unit_group_fields($connection);
            self::unit_file_uploads($connection);
            //Incpection table
            self::createInspectionNameList($connection);
            self::createPropertyInspection($connection);
            self::createInspectionPhotos($connection);
            self::createInspectionArea($connection);
            self::createInspectionListProperty($connection);
            self::createKeyCheckout($connection);
            self::createAdminCheckTemplate($connection);
            //useralerts
            self::createUserAlerts($connection);
            self::createBuildingKeyCheckout($connection);
            self::createEmployeeDetails($connection);
            //vendor Migrations
            self::createVendorAdditionalDetails($connection);
            self::createVendorComplaints($connection);
            self::createPropertyPercentOwned($connection);
            self::createEmailTemplatesAdmin($connection);
            self::createTagsEmailTemplate($connection);
            self::createOwnerAccountName($connection);
            self::createOwnerDetails($connection);
            self::createOwnerPropertyOwned($connection);
            self::createOwnerBankingInformation($connection);
            self::createReasonMoveOutTenant($connection);
            self::createcompany_annual_income($connection);
            self::createcompany_employment_history($connection);
            self::createcompany_rental_applications($connection);
            self::createcompany_rental_history($connection);
            self::alterUserTable($connection);
            self::alterUserTableOwnerPortal($connection);
            self::alterUserTableCompanyName($connection);
            self::createContactNotes($connection);
            self::alterOwnerTableOtherRelation($connection);
            self::alterVendorAdditionalTable($connection);
            self::alterEmergencyDetailsTable($connection);
            self::altertenantEmergencyDetails($connection);
            self::altertenantMaintenance($connection);
            //self::alterFlagTable($connection);
            self::alterPropertyTable($connection);
            //self::alterFlagTableForOwner($connection);
            self::alterUserTableForGender($connection);
            self::alterComplaintsTableForOwner($connection);
            self::alterTenantPhone($connection);
            self::alterOwnerDetailTableForPortal($connection);
            self::createOwnerBillingInfo($connection);
            self::altertenantEmailTemplate($connection);
            self::createForwardAddress($connection);
            self::createMoveoutTenant($connection);
            self::alterTenant_lease_details($connection);
            self::alterUsersTableForPortal($connection);
            self::alterUsersTableForPortalStatus($connection);
            self::alterUsersTableForUserType($connection);
            self::alterTenantImages($connection);
            self::altertenant_additional_details($connection);
            self::altertenant_guarantor($connection);
            self::createAnnouncements($connection);
            self::createTagsLettersNotices($connection);
            self::createLettersNoticesTemplate($connection);
            self::alterTemplateTable($connection);
            self::createAnnouncementFileLibrary($connection);
            self::altertenant_additional_details($connection);
            self::altertenant_guarantor($connection);
            self::createCommunicationEmail($connection);
            self::maintenanceItem($connection);
            self::maintenanceCategory($connection);
            self::maintenanceColor($connection);
            self::maintenanceLostFound($connection);
            self::createAnnouncementUserStatus($connection);
            self::alterPropertyIdToLostFound($connection);
            self::CreateWorkorderCategory($connection);
            self::CreateWorkorderRequest($connection);
            self::CreateWorkorderSource($connection);
            self::CreateWorkorderStatus($connection);
            self::CreateWorkorderType($connection);
            self::CreateWorkorderPriorityType($connection);
            self::CreateWorkorder($connection);
            self::CreateWorkorderFileLibrary($connection);
            self::CreateIntouchType($connection);
            self::maintenancePurchaseOrder($connection);
            self::alterAnnouncementsTable($connection);
            self::maintenancePurchaseOrderDetails($connection);
            self::createMaintenanceFiles($connection);
            self::taskReminders($connection);
            self::Createmielagelog($connection);
            self::CreateCallType($connection);
            self::CreateCallPhoneLogs($connection);
            self::CreateMaintenanceLostNote($connection);
            // self::alterTaskReminders($connection);
            self::CreateCommunicationEmailAttachments($connection);
            self::createConversationTable($connection);
            self::alterAnnouncementTable($connection);
            self::createProblemCategoryTable($connection);
            // self::alterTaskReminders($connection);

            self::CreateChatHistory($connection);
            self::alterUserTableForCreatedByColumn($connection);
            self::alterTableCommunicationEmail($connection);
            self::alterTableCommunicationEmailGtype($connection);
            self::maintenanceInventory($connection);
            self::altermaintenanceInventory($connection);
            self::maintenanceInventoryCategory($connection);
            self::maintenanceInventorySubCategory($connection);
            self::maintenanceInventoryBrand($connection);
            self::maintenanceInventorySupplier($connection);
            self::maintenanceInventoryQuantityChangeReason($connection);
            self::maintenanceInventoryVolume($connection);
            self::maintenanceInventorySubLocation($connection);
            self::alterPurchaseOrderCustomField($connection);
            self::createPackageTrackerCarrier($connection);
            self::createLocationStored($connection);
            self::createpackageTracker($connection);
            self::alterConversationTable($connection);
            self::alterConversationTablePortal($connection);
            self::createInTouch($connection);
            self::createInTouchLogs($connection);
            self::createInTouchNotes($connection);
            self::alterInventoryTableCustomField($connection);
            self::createDailyVisitorLogTable($connection);
            self::createTimeSheetTable($connection);
            self::workordernotes($connection);
            self::waitinglist($connection);
            self::createOwnerPropertyRelation($connection);
            self::createMcc($connection);
            //            self::alterPurchaseOrderCustomField($connection);
            self::CreateReasonsTable($connection);
            self::alterPackageTracker_Table($connection);
            self::alterTenantPhoneTableForExtension($connection);
            self::alterGeneralPropertyTable($connection);
            self::alterUsersTableForExtension($connection);
            self::alterAddPackageTracker_Table($connection);
            self::alterUserTableDiscountPrice($connection);
            self::alterOwnerPreffTable($connection);
            self::createUserAccountDetails($connection);
            self::propertyInventory($connection);
            self::alterOwnerRelationTable($connection);
            self::createTransactionTable($connection);
            self::alterPlanHistoryTable($connection);
            self::alterAutopayForUserTable($connection);
            self::alterflagsTable($connection);
            // self::alterTaskReminders($connection);
            self::alterpropertyinspectionTable($connection);
            self::createCampaign($connection);
            self::createMarketingContactDetail($connection);
            self::createmaintenanceitemcode($connection);
            self::createMarketingPostsTable($connection);
            self::alterGenPropertyTable($connection);
            self::createFlyerTable($connection);
            self::createMapAddress($connection);
            self::createAccountingInvoice($connection);
            self::createAccountingManageCharges($connection);
//            self::alterTenantChargesTable($connection);
//            self::alterTransactionsTable($connection);
            //self::alterTenantChargesTable($connection);
//            self::alterTransactionsTable($connection);
            self::create_short_term_rental($connection);
            self::create_short_term_rental_booking_dates($connection);
            self::create_company_bills($connection);
            self::create_company_bill_files($connection);
            self::create_company_bill_items($connection);
            self::create_company_bill_notes($connection);

            self::createAccountingJournalEnteries($connection);
            self::createJournalEntryDetail($connection);
            self::createAccountingFileUploads($connection);
            self::createAccountingLedger($connection);

            self::alterAccountingInvoicesTable($connection);
            self::alterflyerPostsTable($connection);
            self::altermarketingContactDetailTable($connection);

            self::smLandlord($connection);
            self::smProperty($connection);
            self::smApplication($connection);
            self::alterTenantRentalLeaseSignaures($connection);
            self::alterFlyerTable($connection);
            self::createCompany_budgetTable($connection);
            self::createCompany_budget_detailsTable($connection);



            self::alterCompanyBillTable($connection);
//            self::createMarketingPostsTable($connection);
            self::createUtilityBillingTable($connection);


            self::createBankReconciliationTable($connection);
            self::alterCompanyAccountingBankAccount($connection);

            // self::alterAccountingInvoices($connection);
            self::create_accounting_banking($connection);
            self::alterAccountingManageCharges($connection);

            //  self::alterDropTables($connection);

            self::alterTransactionsTable($connection);
            self::alterTransactionsTableSM($connection);
            self::alterPropertyBankDetails($connection);
            self::alterPropertyBankDetailsSource($connection);
            self::alterCompanyAccountingBankAccountSource($connection);
            self::alterPropertyBankDetailsBankId($connection);
            self::createpaymentplanTable($connection);
            self::createpaymentplandatesTable($connection);
            self::alterTransactionsColumnPaymentTypeTable($connection);
            self::createdashbordspotsetting($connection);
            self::createEsignSignatureTable($connection);
            self::alterActiveInactiveStatusTable($connection);
            self::alterActiveInactiveStatusTable1($connection);
            self::alterActiveInactiveStatusTable2($connection);
            self::alterActiveInactiveStatusTable3($connection);
            self::alterusertableTerm_plan($connection);
            self::createOwnerContributionTable($connection);
            self::alteraccounting_bankingtable1($connection);
            self::alteraccounting_bankingtable2($connection);
            self::alterInspectionphotos($connection);
            self::alteraIntouchNotes($connection);
            self::alteraIntouchNotesDetail($connection);
            self::createSubLocationTable($connection);
            self::alterManagementFeesTable($connection);
            self::createProcessManagementFeeTable($connection);
            self::alterXmlField($connection);
            self::createPropertyRentalTable($connection);
            self::alterWorkOrderUserid($connection);
            self::alterTransactionsTableForPropertyId($connection);
            self::createOwnerDrawTable($connection);
            self::alterTenantDetailColumns($connection);
            self::alterCarrerTable($connection);



            self::createCalendarTable($connection);



            return array('code' => 200, 'status' => 'success', 'message' => 'company user table created successfully');
        }
        catch(PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
        }
        return $return;
    }
    public static function createMigrationTable($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `migration_table` (
                      `id` int(20) NOT NULL AUTO_INCREMENT,
                      `name` varchar(250) DEFAULT NULL,
                      `is_processed` enum('0','1') DEFAULT NULL,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createUserTable($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS users (
                 `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                 `name`  VARCHAR(255) DEFAULT NULL,
                 `password`  VARCHAR(255) DEFAULT NULL,
                 `actual_password` text ,
                 `admin_user_id` VARCHAR(255) DEFAULT NULL,
                 `token_2fa`  VARCHAR(255) DEFAULT NULL,
                 `enabled_2fa` int(11) DEFAULT 0,
                 `admindb_id` int(11),
                 `last_user_url`  VARCHAR(255) DEFAULT NULL,
                 `last_login` timestamp NULL DEFAULT NULL,
                 `salutation`  VARCHAR(255) DEFAULT NULL,
                 `first_name`  VARCHAR(255) DEFAULT NULL,
                 `last_name`  VARCHAR(255) DEFAULT NULL,
                 `middle_name`  VARCHAR(255) DEFAULT NULL,
                 `nick_name`  VARCHAR(255) DEFAULT NULL,
                 `maiden_name`  VARCHAR(255) DEFAULT NULL,
                 `company_name`  VARCHAR(255) DEFAULT NULL,
                 `zipcode`  VARCHAR(255) DEFAULT NULL,
                 `city`  VARCHAR(255) DEFAULT NULL,
                 `state`  VARCHAR(255) DEFAULT NULL,
                 `country`  VARCHAR(255) DEFAULT NULL,
                 `default_currency`  VARCHAR(255) DEFAULT NULL,
                 `subscription_plan`  VARCHAR(255) DEFAULT NULL,
                 `term_plan` enum('0','1','2','3','4') NOT NULL,
                 `plan_price` text ,
                 `discount`  VARCHAR(255) DEFAULT NULL,
                 `pay_plan_price` text ,
                 `account_type` enum('0','1') NOT NULL,
                 `fax`  VARCHAR(255) DEFAULT NULL,
                 `address1` text ,
                 `address2` text ,
                 `address3` text ,
                 `free_plan` enum('0','1') NOT NULL,
                 `remaining_days`  VARCHAR(255) DEFAULT NULL,
                 `extended_days`  VARCHAR(255) DEFAULT NULL,
                 `eft_country`  VARCHAR(255) DEFAULT NULL,
                 `eft_cuntry` enum('0','1','2') NOT NULL,
                 `routing_number`  VARCHAR(255) DEFAULT NULL,
                 `trace_number`  VARCHAR(255) DEFAULT NULL,
                 `website` text ,
                 `company_logo` text ,
                 `signature` text ,
                 `number_of_units` varchar(255) DEFAULT NULL,
                 `plan_size`  VARCHAR(255) DEFAULT NULL,
                 `tax_id`  VARCHAR(255) DEFAULT NULL,
                 `transmission_control_code` text ,
                 `contact_name` text ,
                 `application_fees`  VARCHAR(255) DEFAULT NULL,
                 `currency`  VARCHAR(255) DEFAULT NULL,
                 `rent_amount`  VARCHAR(255) DEFAULT NULL,
                 `default_payment_method`  VARCHAR(255) DEFAULT NULL,
                 `is_admin` enum('0','1') NOT NULL,
                 `user_type` enum('0','1','2','3','4','5','6','7','8') NOT NULL,
                 `phone_number`  VARCHAR(255) DEFAULT NULL,
                 `domain_name` text ,
                 `parent_id` int(11) DEFAULT 0,
                 `updated_by_user` int(11) DEFAULT NULL,
                 `role`  VARCHAR(255) DEFAULT NULL,
                 `country_code`  VARCHAR(255) DEFAULT NULL,
                 `deleted_at` timestamp NULL DEFAULT NULL,
                 `remember_token`  VARCHAR(100) DEFAULT NULL,
                 `created_at` timestamp NULL DEFAULT NULL,
                 `updated_at` timestamp NULL DEFAULT NULL,
                 `use_email_signature` enum('0','1') NOT NULL,
                 `email_signature`  VARCHAR(255) DEFAULT NULL,
                 `dob` date DEFAULT NULL,
                 `ownner_company` text ,
                 `ethnicity` text ,
                 `maritial_status` text ,
                 `hobbies` text ,
                 `veteran_status` text ,
                 `ssn_sin_id`  VARCHAR(255) DEFAULT NULL,
                 `phone_type`  VARCHAR(255) DEFAULT NULL,
                 `carrier`  VARCHAR(255) DEFAULT NULL,
                 `phone_number_note` text ,
                 `referral_source`  VARCHAR(255) DEFAULT NULL,
                 `tax_payer_name`  VARCHAR(255) DEFAULT NULL,
                 `tax_payer_id`  VARCHAR(255) DEFAULT NULL,
                 `eligible_1099` enum('0','1') NOT NULL,
                 `send_1099` enum('0','1') NOT NULL,
                 `owners_portal` enum('0','1') NOT NULL,
                 `send_owners_package` enum('0','1') NOT NULL,
                 `hold_owners_payments` enum('0','1') NOT NULL,
                 `email_financial_info` enum('0','1') NOT NULL,
                 `include_reports` enum('0','1') NOT NULL,
                 `draw_payment_method`  VARCHAR(255) DEFAULT NULL,
                 `notes`  VARCHAR(255) DEFAULT NULL,
                 `mi`  VARCHAR(255) DEFAULT NULL,
                 `address4` text ,
                 `originator_id_number`  VARCHAR(255) DEFAULT NULL,
                 `cpa_code` text ,
                 `originator_name` text ,
                 `transit_number`  VARCHAR(255) DEFAULT NULL,
                 `account_number` text ,
                 `due_date`  VARCHAR(255) DEFAULT NULL,
                 `origin` text ,
                 `status` enum('0','1','2','3','4') NOT NULL,
                 `expiration_date` datetime DEFAULT NULL,
                 `payment_status` enum('0','1') DEFAULT NULL,
                 `account_admin_phone_number`  VARCHAR(255) DEFAULT NULL,
                 `account_admin_name`  VARCHAR(255) DEFAULT NULL,
                 `account_admin_email_address`  VARCHAR(255) DEFAULT NULL,
                 `contact_name_1099_efile`  VARCHAR(255) DEFAULT NULL,
                 `work_phone_extension`  VARCHAR(255) DEFAULT NULL,
                 `work_phone`  VARCHAR(255) DEFAULT NULL,
                 `home_phone`  VARCHAR(255) DEFAULT NULL,
                 `mobile_number`  VARCHAR(255) DEFAULT NULL,       
                 `prefilled_token` text ,                 
                 `token_2fa_expiry` datetime DEFAULT NULL,
                 `token_2fa_login` datetime DEFAULT NULL,
                 `token_2fa_time` int(11) DEFAULT NULL,
                 `gender` enum('0','1','2','3','4') NOT NULL,
                 `email`  VARCHAR(255) DEFAULT NULL,
                 `sms_carrier`  VARCHAR(255) DEFAULT NULL,
                 `phone_number_prefix`  VARCHAR(255) DEFAULT NULL,
                 `custom_fields` text ,
                 `tenant_account_status` enum('0','1') NOT NULL,
                 `record_status` enum('1','0') NOT NULL,
                 `reset_password_token`  text DEFAULT NULL,  
                 `customer_id`  VARCHAR(255) DEFAULT NULL,              
                  PRIMARY KEY (`id`)
                )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createActiveInactiveStatus($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `active_inactive_status` (
                      `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                      `user_id` int(11) DEFAULT NULL,
                      `module` varchar(250) DEFAULT NULL,
                      `status` varchar(250) DEFAULT NULL,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    )";
            $date = date("Y-m-d H:i:s");
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createCompanyUnitType($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS company_unit_type (

                `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                `user_id` int(11) DEFAULT NULL,               

                `unit_type`  VARCHAR(255) DEFAULT NULL,

                `description`  text,

                `is_default` enum('0','1') NOT NULL,

                `status` enum('0','1') NOT NULL,

                `deleted_at` timestamp NULL DEFAULT NULL,

                `created_at` timestamp NULL DEFAULT NULL,

                `updated_at` timestamp NULL DEFAULT NULL,

                `is_editable`  TINYINT DEFAULT NULL,

                 PRIMARY KEY (`id`)

               )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createCompanyPropertyType($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS company_property_type (

                 `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                 `property_type`  VARCHAR(255) DEFAULT NULL,

                 `description`  text,

                 `user_id` int(11) DEFAULT NULL,

                 `is_default` enum('0','1') NOT NULL,

                 `status` enum('0','1') NOT NULL,

                 `deleted_at` timestamp NULL DEFAULT NULL,

                 `created_at` timestamp NULL DEFAULT NULL,

                 `updated_at` timestamp NULL DEFAULT NULL,

                 `is_editable` TINYINT DEFAULT NULL,

                  PRIMARY KEY (`id`)

                )";
            $connection->prepare($table)->execute();
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createCompanyPropertyStyle($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS company_property_style (

                 `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                 `property_style`  VARCHAR(255) DEFAULT NULL,

                 `description`  text,

                 `user_id` int(11) DEFAULT NULL,

                 `is_default` enum('0','1') NOT NULL,

                 `status` enum('0','1') NOT NULL,

                 `deleted_at` timestamp NULL DEFAULT NULL,

                 `created_at` timestamp NULL DEFAULT NULL,

                 `updated_at` timestamp NULL DEFAULT NULL,

                 `is_editable` TINYINT DEFAULT NULL,

                  PRIMARY KEY (`id`)

                )";
            $connection->prepare($table)->execute();
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createCompanyPropertySubType($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS company_property_subtype (

                 `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                 `property_subtype`  VARCHAR(255) DEFAULT NULL,

                 `description`  text,

                 `user_id` int(11) DEFAULT NULL,

                 `is_default` enum('0','1') NOT NULL,

                 `status` enum('0','1') NOT NULL,

                 `deleted_at` timestamp NULL DEFAULT NULL,

                 `created_at` timestamp NULL DEFAULT NULL,

                 `updated_at` timestamp NULL DEFAULT NULL,

                 `is_editable` TINYINT DEFAULT NULL,

                  PRIMARY KEY (`id`)

                )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createCompanyPropertyGroups($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS company_property_groups (

                 `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                 `group_name`  VARCHAR(255) DEFAULT NULL,

                 `description`  text,

                 `user_id` int(11) DEFAULT NULL,

                 `is_default` enum('0','1') NOT NULL,

                 `status` enum('0','1') NOT NULL,

                 `deleted_at` timestamp NULL DEFAULT NULL,

                 `created_at` timestamp NULL DEFAULT NULL,

                 `updated_at` timestamp NULL DEFAULT NULL,

                 `is_editable` TINYINT DEFAULT NULL,

                  PRIMARY KEY (`id`)

                )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createCompanyPropertyAmenities($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_property_amenities` (

              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

              `user_id` int(11) NOT NULL,             

              `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

              `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,

              `created_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NULL DEFAULT NULL,

              `deleted_at` timestamp NULL DEFAULT NULL,

              `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

              `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `is_editable` TINYINT DEFAULT NULL,

              PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createPropertyType($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS company_property_type (

                 `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                 `property_type`  VARCHAR(255) DEFAULT NULL,

                 `description`  text,

                 `user_id` int(11) DEFAULT NULL,

                 `is_default` enum('0','1') NOT NULL,

                 `status` enum('0','1') NOT NULL,

                 `deleted_at` timestamp NULL DEFAULT NULL,

                 `created_at` timestamp NULL DEFAULT NULL,

                 `updated_at` timestamp NULL DEFAULT NULL,

                 `is_editable` TINYINT DEFAULT NULL,

                  PRIMARY KEY (`id`),

                )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createPropertyPortfolio($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_property_portfolio` (

              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

             `user_id` bigint(20) DEFAULT NULL,

             `portfolio_id` varchar(100) NOT NULL,

             `portfolio_name` text NOT NULL,

             `is_default` enum('0','1') DEFAULT NULL,

             `is_editable` enum('0','1') DEFAULT NULL,

             `custom_field` text,

             `status` enum('0','1') DEFAULT NULL,

             `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

             `updated_at` timestamp NULL DEFAULT NULL,

             `deleted_at` timestamp NULL DEFAULT NULL,

               PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createPropertyCustomFields($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `custom_fields` (

              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

             `company_id` int(20) DEFAULT NULL,

             `field_name` varchar(250) DEFAULT NULL,

             `data_type` varchar(250) DEFAULT NULL,

             `default_value` text,

             `is_required` enum('0','1') DEFAULT NULL,

             `is_deletable` enum('0','1') DEFAULT NULL,

             `is_editable` enum('0','1') DEFAULT NULL,

             `module` varchar(250) DEFAULT NULL,

             `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

             `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,

             `deleted_at` timestamp NULL DEFAULT NULL,

             PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createPropertyPlansHistory($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `plans_history` (

             `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

             `subscription_plan` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,

             `term_plan` enum('0','1','2','3') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,

             `plan_price` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,

             `discount` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,

             `pay_plan_price` text NOT NULL,

             `user_id` int(11) NOT NULL,

             `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

             `updated_at` timestamp NULL DEFAULT NULL,

             `deleted_at` timestamp NULL DEFAULT NULL,

             `trial_acc` enum('0','1') NOT NULL,

             `start_date` timestamp NULL DEFAULT NULL,

             `end_date` timestamp NULL DEFAULT NULL,

             `no_of_units` varchar(255) NOT NULL,

             `status` enum('0','1') DEFAULT NULL,

             PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createPropertyPlans($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `plans` (

             `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

             `plan_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

             `number_of_units` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

             `min_units` int(11) NOT NULL,

             `max_units` int(11) NOT NULL,

             `month_rate` double(11,2) DEFAULT NULL,

             `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',

             `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

             `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

             `deleted_at` timestamp NULL DEFAULT NULL,

              PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createPropertyTimezone($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `timezone` (

             `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

              `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

              `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

              `timezone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `hours_calc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `hours_gap` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `created_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NULL DEFAULT NULL,

               PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //echo '<pre>'; print_r($exception->getMessage()); die();
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createPropertyEmailSignatures($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `email_signature` (

                      `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                      `email_signature` text DEFAULT NULL,

                      `user_id` int(10) DEFAULT NULL,

                      `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            // echo '<pre>'; print_r($exception);echo '</pre>';die;
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createPropertyCompanypreference($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_preference` (

             `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

              `user_id` int(10) DEFAULT NULL,

              `notifications_to_me` varchar(255) DEFAULT NULL,

              `show_last_name` enum('0','1') DEFAULT NULL,

              `birthday_notifications` enum('0','1') DEFAULT NULL,

              `inventory_alert` enum('0','1') DEFAULT NULL,

              `insurance_alert` varchar(255) DEFAULT NULL,

              `activate_2fa` enum('0','1') DEFAULT NULL,

              `in_touch` enum('0','1') DEFAULT NULL,

              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

              `deleted_at` timestamp NULL DEFAULT NULL,

              PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createPropertyAuditTrial($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `audit_trial` (

            `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

            `user_id` int(11) NOT NULL,

            `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

            `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

            `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

            `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

            `description` text COLLATE utf8mb4_unicode_ci NOT NULL,

            `created_at` timestamp NULL DEFAULT NULL,

            `updated_at` timestamp NULL DEFAULT NULL,

             PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createPropertyDefaultCurrency($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `default_currency` (

              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

              `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

              `symbol` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

              `created_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NULL DEFAULT NULL,

              `deleted_at` timestamp NULL DEFAULT NULL,

               PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function company_category_type($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS  `company_category_type` (

               `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

              `user_id` int(11) NOT NULL,

              `category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

              `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,

              `is_default` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

              `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

              `created_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NULL DEFAULT NULL,

              `deleted_at` timestamp NULL DEFAULT NULL,

              `is_editable` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',

              PRIMARY KEY (`id`)

            ) 

            ";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function list_of_volumes($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `list_of_volumes` (

   `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

  `user_id` int(11) NOT NULL,

  `volume` varchar(255) NOT NULL,

  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

  `deleted_at` timestamp NULL DEFAULT NULL,

  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

  PRIMARY KEY (`id`)

)

            ";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createPropertyDateClockSettings($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `default_date_clock_settings` (

             `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

              `user_id` int(11) NOT NULL,

              `default_date_format` enum('1','2','3','4') NOT NULL,

              `date_format` int(11) NOT NULL,

              `default_clock_format` enum('12','24') NOT NULL,

              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

              `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

              `deleted_at` timestamp NULL DEFAULT NULL,

               PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createPropertyCompanyTimeSettings($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_time_setting` (

               `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

              `user_id` int(11) DEFAULT NULL,

              `timezone` varchar(255) DEFAULT NULL,

              `time_to_send_notices` varchar(255) DEFAULT NULL,

              `time_to_send_email_reminders` varchar(255) DEFAULT NULL,

              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

              `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

              `deleted_at` timestamp NULL DEFAULT NULL,

               PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createPropertyDefaultSettings($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `default_settings` (

                      `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                      `user_id` int(11) DEFAULT NULL,

                      `payment_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '1',

                      `property_size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'Sq ft',

                      `page_size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '10',

                      `notice_period` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '4',

                      `timeout` enum('15','30','45','60','90') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '15',

                      `default_rent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `application_fees` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `zip_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `currency` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '174',

                      `refund_time_out` varchar(100) DEFAULT NULL,

                      `tenant_time_bond` varchar(100) DEFAULT NULL,

                      `created_at` timestamp NULL DEFAULT NULL,

                      `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createPropertyZipCodeMaster($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `zip_code_master` (

                  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                  `user_id` int(11) DEFAULT NULL,

                  `zip_code` varchar(100) DEFAULT NULL,

                  `latitude` varchar(100) DEFAULT NULL,

                  `longitude` varchar(100) DEFAULT NULL,

                  `city` varchar(100) DEFAULT NULL,

                  `state` varchar(100) DEFAULT NULL,

                  `county` varchar(100) DEFAULT NULL,

                  `country` varchar(100) DEFAULT NULL,

                  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

                  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

                  `deleted_at` timestamp NULL DEFAULT NULL,

                   PRIMARY KEY (`id`)

                )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function company_inventory_preferences($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS  `company_inventory_preferences` (

              `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

              `user_id` int(11) NOT NULL,

              `inventory_item` enum('0','1') NOT NULL,

              `add_suppliers` enum('0','1') NOT NULL,

              `upload_photos` enum('0','1') NOT NULL,

              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

              `deleted_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

              PRIMARY KEY (`id`)) ";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function company_severity_type($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS  `company_severity_type` (

              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

              `user_id` int(11) DEFAULT NULL,

              `severity` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

              `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,

              `is_default` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

              `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

              `created_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NULL DEFAULT NULL,

              `deleted_at` timestamp NULL DEFAULT NULL,

              `is_editable` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',

              PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function company_priority_type($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `company_priority_type` (

              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

              `user_id` int(11) DEFAULT NULL,

              `priority` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

              `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,

              `is_default` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

              `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

              `created_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NULL DEFAULT NULL,

              `deleted_at` timestamp NULL DEFAULT NULL,

              `is_editable` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',

               PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function company_event_type($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS  `company_event_type` (

              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

              `user_id` int(11) DEFAULT NULL,

              `event_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

              `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,

              `is_default` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

              `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

              `created_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NULL DEFAULT NULL,

              `deleted_at` timestamp NULL DEFAULT NULL,

              `is_editable` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',

               PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function company_manage_vehicle($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_manage_vehicle`(

                  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                  `user_id` int(11) DEFAULT NULL,

                  `vehicle_name` varchar(255) DEFAULT NULL,

                  `vehicle` varchar(255) DEFAULT NULL,

                  `vehicle_type` varchar(11) DEFAULT NULL,

                  `make` varchar(255) DEFAULT NULL,

                  `model` varchar(255) DEFAULT NULL,

                  `vin` varchar(255) DEFAULT NULL,

                  `registration` varchar(255) DEFAULT NULL,

                  `plate_number` varchar(255) DEFAULT NULL,

                  `color` varchar(255) DEFAULT NULL,

                  `year_of_vehicle` int(11) NOT NULL,

                  `date_purchased` datetime DEFAULT NULL,

                  `starting_mileage` varchar(255) DEFAULT NULL,

                  `amount` varchar(255) DEFAULT NULL,

                  `created_at` timestamp NULL DEFAULT NULL,

                  `updated_at` timestamp NULL DEFAULT NULL,

                  `deleted_at` timestamp NULL DEFAULT NULL,

                  `is_editable` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',

                  PRIMARY KEY (`id`)

                )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function company_vendor_type($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `company_vendor_type` (

              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

              `user_id` int(11) DEFAULT NULL,

              `vendor_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

              `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,

              `is_default` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

              `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

              `created_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NULL DEFAULT NULL,

              `deleted_at` timestamp NULL DEFAULT NULL,

              `is_editable` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',

              PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function company_maintenance_subcategory($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_maintenance_subcategory` (

              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

              `user_id` int(11) DEFAULT NULL,

              `category` varchar(255) DEFAULT NULL,

              `sub_category` varchar(255) DEFAULT NULL,

              `supplier` varchar(255) DEFAULT NULL,

              `brand` varchar(255) DEFAULT NULL,

              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

              `deleted_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

              `is_editable` enum('1','0') NOT NULL,

              `parent_id` int(11) DEFAULT NULL,

              PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function company_maintenance_supplier($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_maintenance_supplier` (

              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

              `user_id` int(11) DEFAULT NULL,
              
              `brand_id` int(11) DEFAULT NULL,

              `category_id` varchar(255) NOT NULL,

              `sub_category_id` varchar(255) DEFAULT NULL,

              `supplier` varchar(255) DEFAULT NULL,

              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

              `deleted_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

              `is_editable` enum('1','0') NOT NULL,

              `parent_id` int(11) DEFAULT NULL,

              PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function company_maintenance_brand($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_maintenance_brand` (

              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

              `user_id` int(11) DEFAULT NULL,

              `category_id` varchar(255) NOT NULL,

              `sub_category_id` varchar(255) DEFAULT NULL,

              `brand` varchar(255) DEFAULT NULL,

              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

              `deleted_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

              `is_editable` enum('1','0') NOT NULL,

              `parent_id` int(11) DEFAULT NULL,

              PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantDetails($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_details` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `is_additional_tenant` ENUM('0', '1') DEFAULT NULL,

          `tenant_image` longblob DEFAULT NULL,

          `tenant_contact` varchar(255) DEFAULT NULL,

          `phone_number_note` text DEFAULT NULL,

          `email1` varchar(255) DEFAULT NULL,

          `email2` varchar(255) DEFAULT NULL,

          `email3` varchar(255) DEFAULT NULL,

          `referral_source` int(11) DEFAULT NULL,

          `move_in` date DEFAULT NULL,

          `tenant_license_state` varchar(255) DEFAULT NULL,

          `tenant_license_number` varchar(255) DEFAULT NULL,

          `movein_key_signed` int(11) DEFAULT NULL,

          `vehicle` ENUM('0', '1'),

          `pet` ENUM('0', '1'),

          `animal` ENUM('0', '1'),

          `parking_space` ENUM('0', '1'),

          `medical_allergy` ENUM('0', '1'),

          `guarantor` ENUM('0', '1'),

          `collection` ENUM('0', '1'),

          `smoker` ENUM('0', '1'),

          `status` ENUM('1','0','2','3','4','5','6'),/* 0=>'deative', 1=> active, 2=>'evicting',3=>'in-collection',4=>'bankruptcy',5=>'evicted',6=>'' */

          `record_status` enum('0','1') NOT NULL,

          `custom_field` text ,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantAdditionalDetails($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_additional_details` (

          `id` int(11) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED DEFAULT NULL,

          `extra_record` int(11) DEFAULT NULL,

          `salutation` varchar(255) DEFAULT NULL,

          `first_name` varchar(255) DEFAULT NULL,

          `mi` varchar(255) DEFAULT NULL,

          `last_name` varchar(255) DEFAULT NULL,

          `maiden_name` varchar(255) DEFAULT NULL,

          `nick_name` varchar(255) DEFAULT NULL,

          `gender` varchar(255) DEFAULT NULL,

          `relationship` varchar(255) DEFAULT NULL,

          `ethnicity` varchar(255) DEFAULT NULL,

          `marital_status` varchar(255) DEFAULT NULL,

          `veteran_status` varchar(255),

          `email1` varchar(255) DEFAULT NULL,

          `email2` varchar(255) DEFAULT NULL,

          `email3` varchar(255) DEFAULT NULL,

          `additional_key` varchar(255) DEFAULT NULL,

          `referral_source` varchar(255) DEFAULT NULL,

          `tenant_status` varchar(255) DEFAULT '1',

          `financial_responsible` int(11) DEFAULT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantPhone($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_phone` (

          `id` int(11) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED DEFAULT NULL,

          `parent_id` int(11) DEFAULT NULL,

          `user_type` enum('0','1','2','3'),

          `phone_type` int(11) DEFAULT NULL,

          `carrier` int(11) DEFAULT NULL,

          `country_code` int(11) DEFAULT NULL,

          `phone_number` varchar(255) DEFAULT NULL,

          `additional_key` varchar(255) DEFAULT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantEmergencyDetails($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `emergency_details` (

          `id` int(11) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED DEFAULT NULL,

          `parent_id` int(11) DEFAULT NULL,

          `emergency_contact_name` varchar(255) DEFAULT NULL,

          `emergency_relation` varchar(255) DEFAULT NULL,

          `emergency_country_code` varchar(255) DEFAULT NULL,

          `emergency_phone_number` varchar(255) DEFAULT NULL,

          `emergency_email` varchar(255) DEFAULT NULL,

          `additional_key` varchar(255) DEFAULT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT NULL,

          `updated_at` timestamp NULL DEFAULT NULL   

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function altertenantEmergencyDetails($connection) {
        try {
            $table = "ALTER TABLE `emergency_details` 

                      MODIFY `parent_id` int(11) DEFAULT NULL";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function altertenantMaintenance($connection) {
        try {
            $table = "ALTER TABLE IF NOT EXISTS `tenant_maintenance` 

                      MODIFY `ticket_type` varchar(255) DEFAULT NULL";
            $connection->prepare($table)->execute();
            $table = "ALTER TABLE `tenant_maintenance` MODIFY `duration_type` varchar(255) DEFAULT NULL";
            $connection->prepare($table)->execute();
            $table = "ALTER TABLE `tenant_maintenance` MODIFY `priority` varchar(255) DEFAULT NULL";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantFinalMoveIn($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_move_in` (

          `id` int(11) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `scheduled_move_in` varchar(255),

          `actual_move_in` varchar(255),

          `paid_amount` varchar(255),

          `check_no` varchar(255),

          `prorated_amount` varchar(255),

          `memo` varchar(255),

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantChargeFiles($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_chargefiles` (
                  `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                  `user_id` int(10) UNSIGNED NOT NULL,
                  `type` enum('N','T') DEFAULT 'N',
                  `filename` varchar(255) DEFAULT NULL, 
                  `file_type` enum('1','2','3','4') DEFAULT NULL,
                  `file_location` text DEFAULT NULL ,
                  `file_extension` varchar(255) DEFAULT NULL,
                  `deleted_at` timestamp NULL DEFAULT NULL,
                  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
                );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantChargeNotes($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_chargenote` (

              `id` int(11) AUTO_INCREMENT PRIMARY KEY,

              `user_id` int(10) UNSIGNED NOT NULL,

              `note` text DEFAULT NULL,

              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

            );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantRentalLeaseSignaures($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_rental_lease_signaures` (
              `id` int(11) AUTO_INCREMENT PRIMARY KEY,
              `user_id` int(10) UNSIGNED NOT NULL,
              `status` enum('0','1','2'),
              `signature_image` BLOB DEFAULT NULL,
              `signature_text` VARCHAR(255) DEFAULT NULL,
              `parent_id` int(11) DEFAULT '0',
              `usertype` enum('T','AT','G','O','M') DEFAULT 'T',
              `record_status` enum('0','1') DEFAULT '0',
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
            );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantSSNId($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_ssn_id` (

          `id` int(11) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `parent_id` int(11) NOT NULL,

          `ssn` varchar(255) DEFAULT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantParking($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_parking` (

          `id` int(11) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `permit_number` varchar(255) DEFAULT NULL,

          `space_number` varchar(255) DEFAULT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantMedicalAllergies($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_medical_allergies` (

          `id` int(11) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `allergy` varchar(255) DEFAULT NULL,

          `note` text DEFAULT NULL,

          `date` timestamp NULL DEFAULT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantCollection($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_collection` (

          `id` int(11) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `collection_id` varchar(255) DEFAULT NULL,

          `reason` int(11) DEFAULT NULL,

          `description` text DEFAULT NULL,

          `status` int(11) DEFAULT NULL,

          `amount_due` varchar(255) DEFAULT NULL,

          `notes` text DEFAULT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantCollectionReason($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_collection_reason` (

          `id` int(11) AUTO_INCREMENT PRIMARY KEY,

          `reason` varchar(255) NOT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantCredentils($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `tenant_credential` (

          `id` int(11) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED  DEFAULT NULL,

          `credential_name` varchar(255) DEFAULT NULL,

          `credential_type` int(11) DEFAULT NULL,

          `acquire_date` timestamp NULL DEFAULT NULL,

          `expire_date` timestamp NULL DEFAULT NULL,

          `notice_period` varchar(255) DEFAULT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantCredentilsType($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_credential_type` (

          `id` int(11) AUTO_INCREMENT PRIMARY KEY,

          `user_id` varchar(255)  DEFAULT '1',

          `credential_type` varchar(255) DEFAULT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantHobby($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `hobbies` (

          `id` int(11) AUTO_INCREMENT PRIMARY KEY,

          `hobby` varchar(255) NOT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantHobbies($connection) {
        /*user_type

            0 => tenant

            1 => additional_tenant

            2=> gurrantor

        */
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_hobbies` (

          `id` int(11) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `user_type` enum('0','1','2','3'),

          `hobby` varchar(255) NOT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createReferralSource($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `referral_source` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `referral` varchar(255) DEFAULT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantServiceAnimal($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_service_animal` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `name` varchar(255) DEFAULT NULL,

          `animal_id` varchar(255) DEFAULT NULL,

          `type` varchar(255) DEFAULT NULL,

          `dob` timestamp NULL DEFAULT NULL,

          `age` int(11) DEFAULT NULL,

          `gender` int(11) DEFAULT NULL,

          `weight` int(11) DEFAULT NULL,

          `weight_unit` enum('0','1','2','3'),

          `note` text,

          `color` varchar(255) DEFAULT NULL,

          `chip_id` varchar(255) DEFAULT NULL,

          `hospital_name` varchar(255) DEFAULT NULL,

          `country_code` int(11) DEFAULT NULL,

          `phone_number` varchar(255) DEFAULT NULL,

          `last_visit` date DEFAULT NULL,

          `next_visit` date DEFAULT NULL,

          `medical_condition` enum('0','1'),

          `medical_condition_note` text,

          `shots` enum('0','1'),

          `shots_name` varchar(255) DEFAULT NULL,

          `shots_given_date` date DEFAULT NULL,

          `shots_expire_date` date DEFAULT NULL,

          `shots_followup_date` date DEFAULT NULL,

          `shots_note` text,

          `rabies` enum('0','1'),

          `rabies_name` varchar(255) DEFAULT NULL,

          `rabies_given_date` date DEFAULT NULL,

          `rabies_expire_date` date DEFAULT NULL,

          `rabies_note` text,

          `image1` BLOB DEFAULT NULL,

          `image2` BLOB DEFAULT NULL,

          `image3` BLOB DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

          `deleted_at` timestamp NULL DEFAULT NULL

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantServiceAnimalGender($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_service_animal_gender` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `gender` varchar(255) DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

          `deleted_at` timestamp NULL DEFAULT NULL

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantPet($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_pet` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `name` varchar(255) DEFAULT NULL,

          `pet_id` varchar(255) DEFAULT NULL,

          `type` varchar(255) DEFAULT NULL,

          `dob` DATE NULL DEFAULT NULL,

          `age` int(11) DEFAULT NULL,

          `gender` int(11) DEFAULT NULL,

          `weight` int(11) DEFAULT NULL,

          `weight_unit` enum('0','1','2','3'),

          `note` text,

          `color` varchar(255) DEFAULT NULL,

          `chip_id` varchar(255) DEFAULT NULL,

          `hospital_name` varchar(255) DEFAULT NULL,

          `country_code` int(11) DEFAULT NULL,

          `phone_number` varchar(255) DEFAULT NULL,

          `last_visit` date DEFAULT NULL,

          `next_visit` date DEFAULT NULL,

          `medical_condition` enum('0','1'),

          `medical_condition_note` text,

          `shots` enum('0','1'),

          `shots_name` varchar(255) DEFAULT NULL,

          `shots_given_date` date DEFAULT NULL,

          `shots_expire_date` date DEFAULT NULL,

          `shots_followup_date` date DEFAULT NULL,

          `shots_note` text,

          `rabies` enum('0','1'),

          `rabies_name` varchar(255) DEFAULT NULL,

          `rabies_given_date` date DEFAULT NULL,

          `rabies_expire_date` date DEFAULT NULL,

          `rabies_note` text,

          `image1` BLOB DEFAULT NULL,

          `image2` BLOB DEFAULT NULL,

          `image3` BLOB DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

          `deleted_at` timestamp NULL DEFAULT NULL

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantPetGender($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_pet_gender` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `gender` varchar(255) DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

          `deleted_at` timestamp NULL DEFAULT NULL

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createEthnicity($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_ethnicity` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `title` varchar(255) DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

          `deleted_at` timestamp NULL DEFAULT NULL

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantFileLibrary($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_file_library` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `name` varchar(255) DEFAULT NULL,

          `extension` varchar(255) DEFAULT NULL,

          `path` varchar(255) DEFAULT NULL,

          `status` varchar(255) DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

          `deleted_at` timestamp NULL DEFAULT NULL

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantChargeCode($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_charge_code` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `charge_code` varchar(255) DEFAULT NULL,

          `credit_amount` varchar(255) DEFAULT NULL,

          `debit_amount` varchar(255) DEFAULT NULL,

          `status` enum('0','1'),

          `description` text COLLATE utf8mb4_unicode_ci,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantGuarantor($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_guarantor` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `company_name` varchar(255) DEFAULT NULL,

          `is_guarantor_company` enum('0','1'),

          `salutation` varchar(255) DEFAULT NULL,

          `description` text,

          `first_name` varchar(255) DEFAULT NULL,

          `middle_name` varchar(255) DEFAULT NULL,

          `last_name` varchar(255) DEFAULT NULL,

          `relationship` varchar(255) DEFAULT NULL,

          `zip_code` varchar(255) DEFAULT NULL,

          `country` varchar(255) DEFAULT NULL,

          `state` varchar(255) DEFAULT NULL,

          `city` varchar(255) DEFAULT NULL,

          `address1` varchar(255) DEFAULT NULL,

          `address2` varchar(255) DEFAULT NULL,

          `address3` varchar(255) DEFAULT NULL,

          `address4` varchar(255) DEFAULT NULL,

          `email1` varchar(255) DEFAULT NULL,

          `email2` varchar(255) DEFAULT NULL,

          `email3` varchar(255) DEFAULT NULL,

          `guarantee_years` int(11) DEFAULT NULL,

          `note` text,

          `entity_fid_number` varchar(255) DEFAULT NULL,

          `mc_first_name` varchar(255) DEFAULT NULL,

          `mc_mi` varchar(255) DEFAULT NULL,

          `mc_last_name` varchar(255) DEFAULT NULL,

          `mc_email1` varchar(255) DEFAULT NULL,

          `mc_email2` varchar(255) DEFAULT NULL,

          `mc_email3` varchar(255) DEFAULT NULL,

          `file_name` varchar(255) DEFAULT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantmaritalStatus($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_marital_status` (

          `id` int(11) AUTO_INCREMENT PRIMARY KEY,

          `marital` varchar(255) DEFAULT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantLeaseDetails($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_lease_details` (

          `id` int(11) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `days_remaining` date DEFAULT NULL,

          `balance` varchar(255) DEFAULT NULL,

          `move_in` date DEFAULT NULL,

          `move_out` date DEFAULT NULL,

          `start_date` date DEFAULT NULL,

          `term` int(11) DEFAULT NULL,

          `tenure` varchar(255) DEFAULT NULL,

          `end_date` date DEFAULT NULL,

          `notice_period` varchar(255) DEFAULT NULL,

          `notice_date` date DEFAULT NULL,

          `rent_due_day` int(11) DEFAULT NULL,

          `rent_amount` varchar(255) DEFAULT NULL,

          `cam_amount` varchar(255) DEFAULT NULL,

          `security_deposite` varchar(255) DEFAULT NULL,

          `next_rent_incr` varchar(255) DEFAULT NULL,

          `flat_perc` varchar(255) DEFAULT NULL,

          `amount_incr` varchar(255) DEFAULT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

          `record_status` enum('0','1') NOT NULL

      );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantReferralSource($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_referral_source` (

          `id` int(11) AUTO_INCREMENT PRIMARY KEY,

          `referral` varchar(255) DEFAULT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantCharges($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_charges` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,
          
          `user_type` int(11) DEFAULT NULL,
          
          `invoice_id` INT(11) DEFAULT NULL,
          
          `waive_of_amount` DECIMAL (11,2) DEFAULT NULL,
          
          `waive_of_comment` TEXT DEFAULT NULL,
          
          `invoice_from` INT(11) DEFAULT NULL,
          
          `amount_due` DECIMAL(10,2) DEFAULT NULL,
          
           `amount_paid` DECIMAL(10,2) DEFAULT NULL,
           
           `amount_refunded` DECIMAL(10,2) DEFAULT NULL,
           
          `charge_code` varchar(255) DEFAULT NULL,

          `frequency` varchar(255) DEFAULT NULL,

          `amount` varchar(255) DEFAULT NULL,

          `start_date` date DEFAULT NULL,

          `end_date` date DEFAULT NULL,

          `status` varchar(255) DEFAULT NULL,

          `extra_field` varchar(255) DEFAULT NULL,

          `record_status` enum('0','1') NOT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function alterTenant_lease_details($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `tenant_lease_details` LIKE 'actual_move_out'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $table = "ALTER TABLE tenant_lease_details
                     ADD actual_move_out date DEFAULT NULL AFTER move_out;";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantTax($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_taxdetails` (

            `id` int(11) AUTO_INCREMENT PRIMARY KEY,

            `user_id` int(10) unsigned NOT NULL,

            `tax_name` varchar(255) NOT NULL,

            `tax_type` enum('P','F') NOT NULL,

            `tax_value` varchar(255) NOT NULL,

            `tax_chargeCode` varchar(255) NOT NULL,

            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

            );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantCharge($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_charge` (

          `id` int(11) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `charge_type` varchar(255) DEFAULT NULL,

          `apply_type` varchar(255)  DEFAULT NULL,

          `fee` int(10) NULL DEFAULT NULL,

          `grace_period` varchar(255) DEFAULT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantComplaint($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_complaints` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `complaint` varchar(255) DEFAULT NULL,

          `complaint_id` varchar(255) DEFAULT NULL,

          `type` varchar(255) DEFAULT NULL,

          `notes` text DEFAULT NULL,

          `date` date DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

          `deleted_at` timestamp NULL DEFAULT NULL

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantHOAType($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `hoa_violation_type` (

            `id` int(10) AUTO_INCREMENT PRIMARY KEY,

            `type` varchar(255) DEFAULT NULL,

            `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

            `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

            `deleted_at` timestamp NULL DEFAULT NULL

            );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantHOA($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `hoa_violation` (

            `id` int(10) AUTO_INCREMENT PRIMARY KEY,

            `user_id` int(10) UNSIGNED NOT NULL,

            `hoa_violation_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

            `date` date DEFAULT NULL,

            `time` time DEFAULT NULL,

            `hoa_type` int(11) DEFAULT NULL,

            `hoa_description` text DEFAULT NULL,

            `upload_pic_1` BLOB DEFAULT NULL,

            `upload_pic_2` BLOB DEFAULT NULL,

            `upload_pic_3` BLOB DEFAULT NULL,

            `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

            `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

            `deleted_at` timestamp NULL DEFAULT NULL

            );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantMaintenance($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_maintenance` (
          `id` int(10) AUTO_INCREMENT PRIMARY KEY,
          `user_id` int(10) UNSIGNED NOT NULL,
          `ticket_type` varchar(255) DEFAULT NULL,
          `duration_type` varchar(255) DEFAULT NULL,
          `ticket_number` varchar(255) DEFAULT NULL,
          `created_on` date DEFAULT NULL,
          `category` varchar(255) DEFAULT NULL,
          `start_date` date DEFAULT NULL,
          `end_date` date DEFAULT NULL,
          `priority` varchar(255) DEFAULT NULL,
          `estimate_cost` varchar(255) DEFAULT NULL,
          `required_matirial` text DEFAULT NULL,
          `vendor_instruction` text DEFAULT NULL,
          `description` text DEFAULT NULL,
          `property_id` int(11) DEFAULT NULL,
          `unit_id` int(11) DEFAULT NULL,
          `name` varchar(255) DEFAULT NULL,
          `phone` varchar(255) DEFAULT NULL,
          `assign_to` varchar(255) DEFAULT NULL,
          `image1` LONGBLOB DEFAULT NULL,
          `image2` LONGBLOB DEFAULT NULL,
          `image3` LONGBLOB DEFAULT NULL,
          `note` text DEFAULT NULL,
          `files` text DEFAULT NULL,
          `status` int(11) DEFAULT NULL,
          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          `deleted_at` timestamp NULL DEFAULT NULL
        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantNotes($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_notes` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `notes` text,

          `type` int(11) DEFAULT NULL,

          `record_status` enum('0','1') NOT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantMoveInListRecord($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `movein_list_record` (
              `id` int(11) AUTO_INCREMENT PRIMARY KEY,
              `user_id` int(10) UNSIGNED NOT NULL,
              `status` enum('0','1','2','3')  DEFAULT '0',
              `record_status` enum('0','1') DEFAULT '1',
              `deleted_at` timestamp NULL DEFAULT NULL,
              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
            );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
        }
    }
    public static function createTenantProperty($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_property` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `property_id` int(11) DEFAULT NULL,

          `building_id` int(11) DEFAULT NULL,

          `unit_id` int(11) DEFAULT NULL,

          `contact` varchar(255) DEFAULT NULL,

          `record_status` enum('0','1') NOT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantrenterInsurance($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_renter_insurance` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `policy_holder` varchar(255) DEFAULT NULL,

          `policy_no` varchar(255) DEFAULT NULL,

          `provider` varchar(255) DEFAULT NULL,

          `status` int(11) DEFAULT NULL,

          `expire_date` date DEFAULT NULL,

          `effective_date` date DEFAULT NULL,

          `renewel_date` date DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

          `deleted_at` timestamp NULL DEFAULT NULL

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantTransfer($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_transfer` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `current_city` varchar(255) DEFAULT NULL,

          `current_state` varchar(255) DEFAULT NULL,

          `current_property` int(11) DEFAULT NULL,

          `current_unit` int(11) DEFAULT NULL,

          `new_city` varchar(255) DEFAULT NULL,

          `new_state` varchar(255) DEFAULT NULL,

          `new_property` int(11) DEFAULT NULL,

          `new_property_type` varchar(255) DEFAULT NULL,

          `new_building` int(11) DEFAULT NULL,

          `new_unit` int(11) DEFAULT NULL,

          `new_occupant` int(11) DEFAULT NULL,

          `rental_approved` int(11) DEFAULT NULL,

          `status` int(11) DEFAULT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function alterTenantTransfer($connection) {
        try {
            $table = "ALTER TABLE `tenant_transfer` 

                      ADD `current_building` INT(11) NULL DEFAULT NULL AFTER `status`, 

                      ADD `secuirty_deposite` VARCHAR(255) NULL DEFAULT NULL AFTER `current_building`;";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantTaxPass($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `tenant_taxpass` (

                `id` int(11) NOT NULL AUTO_INCREMENT,

                `user_id` int(255) NOT NULL,

                `tax_type` enum('F','P') DEFAULT NULL,

                `amount` int(255) DEFAULT NULL,

                `frequency` varchar(255) DEFAULT NULL,

                `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

                PRIMARY KEY (`id`)

                ) ";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createVeteranStatus($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_veteran_status` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `veteran` varchar(255) DEFAULT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createTenantVehicles($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tenant_vehicles` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `user_id` int(10) UNSIGNED NOT NULL,

          `amount` varchar(255) DEFAULT NULL,

          `starting_mileage` int(11) DEFAULT NULL,

          `date_purchased` date DEFAULT NULL,

          `model` varchar(255) DEFAULT NULL,

          `plate` varchar(255) DEFAULT NULL,

          `vehicle_number` varchar(255) DEFAULT NULL,

          `vehicle_name` varchar(255) DEFAULT NULL,

          `type` varchar(255) DEFAULT NULL,

          `make` varchar(255) DEFAULT NULL,

          `license` varchar(255) DEFAULT NULL,

          `color` varchar(255) DEFAULT NULL,

          `year` varchar(255) DEFAULT NULL,

          `vin` varchar(255) DEFAULT NULL,

          `registration` varchar(255) DEFAULT NULL,

          `photo1` BLOB DEFAULT NULL,

          `photo2` BLOB DEFAULT NULL,

          `photo3` BLOB DEFAULT NULL,

          `record_status` enum('0','1') NOT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createCarrier($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `carrier` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `carrier` varchar(255) DEFAULT NULL,
  
          `sms_gateway` varchar(255) DEFAULT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createCountry($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `countries` (

              `id` int(10) AUTO_INCREMENT PRIMARY KEY,

              `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

              `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

              `deleted_at` timestamp NULL DEFAULT NULL,

              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

              `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

            );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createPhoneType($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `phone_type` (

          `id` int(10) AUTO_INCREMENT PRIMARY KEY,

          `type` varchar(255) DEFAULT NULL,

          `deleted_at` timestamp NULL DEFAULT NULL,

          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

        );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function company_accounting_preferences($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `company_accounting_preferences` (

              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

              `user_id` int(11) NOT NULL,

              `eft_generate_date` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

              `created_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NULL DEFAULT NULL,

              `fiscal_year_first_month` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `cash_account_credit_bill_posting` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `rent_charges` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `check_receive_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `debit_type_receive_payment_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `vacancy_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `waiveoff_on_payment_received_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `bad_debit_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `write_off_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `money_order_payment_received_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `auto_debit_payment_receive_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `cash_payment_received_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `nsf_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `check_bounce_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `check_issue_vendor_tenant_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `refund_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `interest_received_from_bank_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `bank_adjustment_fee_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `concession_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `transfer_fee_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `late_fee_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `discount_account_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `eft_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `cam_charges` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `sd_refund_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `warn_transaction_days_future` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `warn_transaction_days_past` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `color_negative_balances_in_red` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `lease_expire_soon_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `use_parenthesis_instead_of_a_minus_sign` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `auto_invoice` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `auto_invoice_days_before_end_of_month` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `warning_future_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `warning_past_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function company_accounting_charge_code_preferences($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_accounting_charge_code_preferences` (

             `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

             `user_id` int(11) NOT NULL,

             `charge_code_type` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

             `charge_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

             `frequency` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

             `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

             `unit_type_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

             `status` enum('0','1','2') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

             `is_default` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

             `is_editable` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

             `created_at` timestamp NULL DEFAULT NULL,

             `updated_at` timestamp NULL DEFAULT NULL,

             `deleted_at` timestamp NULL DEFAULT NULL,

             PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function company_accounting_bank_account($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `company_accounting_bank_account` (

             `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

             `user_id` int(11) NOT NULL,

            

             `portfolio` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

             `bank_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

             `bank_account_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

             `fdi_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

             `branch_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

             `initial_amount` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

             `last_used_check_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

             `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

             `is_default` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

             `created_at` timestamp NULL DEFAULT NULL,

             `updated_at` timestamp NULL DEFAULT NULL,

             `deleted_at` timestamp NULL DEFAULT NULL,

             PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function company_account_type($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `company_account_type` (

             `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

             `user_id` int(11) NOT NULL,

             `range_from` int(11) NOT NULL,

             `range_to` int(11) NOT NULL,

             `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

             `is_default` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

             `created_at` timestamp NULL DEFAULT NULL,

             `updated_at` timestamp NULL DEFAULT NULL,

             `account_type_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

             `deleted_at` timestamp NULL DEFAULT NULL,

             PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function company_chart_of_accounts($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `company_chart_of_accounts` (

            id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

            user_id int(11) NOT NULL,

            account_type_id int(11) NOT NULL,

            account_code varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

            account_name varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

            reporting_code varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

            sub_account varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

            posting_status varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

            status enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

            is_default enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

            is_editable enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

            created_at timestamp NULL DEFAULT NULL,

            updated_at timestamp NULL DEFAULT NULL,

            deleted_at timestamp NULL DEFAULT NULL,

             PRIMARY KEY (id)

           )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function company_accounting_tax_setup($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_accounting_tax_setup` (

              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

              `user_id` int(11) NOT NULL,

              `tax_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

              `tax_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

              `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

              `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,

              `created_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NULL DEFAULT NULL,

              `deleted_at` timestamp NULL DEFAULT NULL,

              PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function list_of_reason($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `list_of_reason` (

              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

              `user_id` int(11) NOT NULL,

              `reason` varchar(255) NOT NULL,

              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

              `deleted_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

              `is_editable` enum('0','1') NOT NULL,

              PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function company_accounting_charge_code($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_accounting_charge_code` (

             `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

             `user_id` int(11) NOT NULL,

             `debit_account` int(11) DEFAULT NULL,

             `credit_account` int(11) DEFAULT NULL,

             `charge_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

             `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

             `created_at` timestamp NULL DEFAULT NULL,

             `updated_at` timestamp NULL DEFAULT NULL,

             `deleted_at` timestamp NULL DEFAULT NULL,

             `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,

             `priority` int(11) DEFAULT NULL,

             `is_editable` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

             `is_default` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

             PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function company_workorder_type($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_workorder_type` (

               `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

              `user_id` int(11) DEFAULT NULL,

              `workorder` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

              `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,

              `is_default` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

              `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

              `created_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NULL DEFAULT NULL,

              `deleted_at` timestamp NULL DEFAULT NULL,

              `is_editable` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',

              PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function company_property_unit($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `company_property_unit` (

                `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                 `floor` int(11) DEFAULT NULL,

                 `unit` int(11) DEFAULT NULL,

                 `buildings_name` varchar(255) DEFAULT NULL,

                 `status` enum('0','1') NOT NULL,

                 `bedrooms` int(11) DEFAULT NULL,

                 `bathroom` int(11) DEFAULT NULL,

                 `unit_status` enum('0','1') NOT NULL,

                 `deleted_at` timestamp NULL DEFAULT NULL,

                 `created_at` timestamp NULL DEFAULT NULL,

                 `updated_at` timestamp NULL DEFAULT NULL,

                   PRIMARY KEY (`id`)

                )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createCompanyUserRoles($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_user_roles` (

                `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                `user_id` int(11) NOT NULL,

                `role_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

                `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,

                `is_editable` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',

                `created_at` timestamp NULL DEFAULT NULL,

                `updated_at` timestamp NULL DEFAULT NULL,

                `deleted_at` timestamp NULL DEFAULT NULL,

                `role` longtext COLLATE latin1_swedish_ci DEFAULT NULL,

                PRIMARY KEY (`id`)

        )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //            return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    //property tables
    public static function createGeneralProperty($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `general_property` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `property_id` varchar(250) DEFAULT NULL,

                      `property_name` varchar(250) DEFAULT NULL,

                      `legal_name` varchar(250) DEFAULT NULL,

                      `portfolio_id` varchar(255) DEFAULT NULL,

                      `status` enum('1','2','3','4','5','6','7','8') DEFAULT '1' COMMENT '1=Active,2=All Advertised,3=All Archived,4=All For Sale,5=All Leased,6=All Resigned,7=All Vacant,8=All Short-Term Rentals',

                      `property_for_sale` varchar(50) DEFAULT NULL,

                      `property_parcel_number` varchar(255) DEFAULT NULL,

                      `property_tax_ids` text,

                      `is_short_term_rental` int(11) DEFAULT NULL,

                      `zipcode` varchar(255) DEFAULT NULL,

                      `country` varchar(255) DEFAULT NULL,

                      `state` varchar(255) DEFAULT NULL,

                      `city` varchar(255) DEFAULT NULL,

                      `address1` varchar(255) DEFAULT NULL,

                      `address2` varchar(255) DEFAULT NULL,

                      `address3` varchar(255) DEFAULT NULL,

                      `address4` varchar(255) DEFAULT NULL,

                      `address_list` varchar(250) DEFAULT NULL,

                      `manager_id` text,

                      `manager_list` varchar(250) DEFAULT NULL,

                      `owner_id` text,

                      `owner_list` varchar(250) DEFAULT NULL,

                      `owner_percentowned` text,

                      `payment_type` varchar(250) DEFAULT NULL,

                      `vendor_1099_payer` varchar(250) DEFAULT NULL,

                      `fiscal_year_end` varchar(250) DEFAULT NULL,

                      `country_code` varchar(255) DEFAULT NULL,

                      `phone_number` text,

                      `note` varchar(255) DEFAULT NULL,

                      `fax` varchar(255) DEFAULT NULL,

                      `property_price` varchar(255) DEFAULT NULL,

                      `attach_groups` text,

                      `property_type` varchar(11) DEFAULT NULL,

                      `property_style` int(11) DEFAULT NULL,

                      `property_subtype` int(11) DEFAULT NULL,

                      `property_year` int(11) DEFAULT NULL,

                      `property_squareFootage` varchar(255) DEFAULT NULL,

                      `last_renovation_date` varchar(255) DEFAULT NULL,

                      `last_renovation_time` varchar(255) DEFAULT NULL,

                      `last_renovation_description` varchar(255) DEFAULT NULL,

                      `no_of_buildings` int(11) DEFAULT NULL,

                      `no_of_units` int(11) DEFAULT NULL,

                      `building_exist` int(10) DEFAULT '1',

                      `unit_exist` int(10) DEFAULT '1',

                      `smoking_allowed` varchar(255) DEFAULT NULL,

                      `pet_friendly` varchar(255) DEFAULT NULL,

                      `school_district_municipality` varchar(255) DEFAULT NULL,

                      `key_access_codes_info` varchar(255) DEFAULT NULL,

                      `key_access_code_desc` varchar(255) DEFAULT NULL,

                      `garage_available` varchar(255) DEFAULT NULL,

                      `no_of_vehicles` text,

                      `garage_note` text,

                      `online_listing` varchar(255) DEFAULT NULL,

                      `online_application` varchar(255) DEFAULT NULL,

                      `disable_sync` varchar(255) DEFAULT NULL,

                      `amenities` text,

                      `description` text,

                      `is_property_with_no_building` varchar(255) DEFAULT NULL,

                      `unit_type` varchar(255) DEFAULT NULL,

                      `base_rent` varchar(255) DEFAULT NULL,

                      `market_rent` varchar(255) DEFAULT NULL,

                      `security_deposit` varchar(255) DEFAULT NULL,

                      `school_district_code` varchar(255) DEFAULT NULL,

                      `school_district_notes` varchar(255) DEFAULT NULL,

                      `created_at` timestamp NULL DEFAULT NULL,

                      `update_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                        )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createPropertyFileUploads($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `property_file_uploads` (

                      `id` int(20) NOT NULL AUTO_INCREMENT,

                      `property_id` int(11) DEFAULT NULL,

                      `user_id` int(20) DEFAULT NULL,

                      `file_name` text,

                      `file_size` varchar(20) DEFAULT NULL,

                      `file_location` text,

                      `file_extension` varchar(20) DEFAULT NULL,

                      `codec` varchar(100) DEFAULT NULL,

                      `marketing_site` enum('0','1') DEFAULT NULL,

                      `file_type` enum('1','2','3') DEFAULT NULL COMMENT '1=images,2=documents,3=videos',

                      `created_at` timestamp NULL DEFAULT NULL,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                      )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createKeyTracker($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `key_tracker` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `property_id` int(11) DEFAULT NULL,

                      `key_tag` varchar(255) DEFAULT NULL,

                      `description` varchar(255) DEFAULT NULL,

                      `total_keys` varchar(255) DEFAULT NULL,

                      `available_keys` int(11) DEFAULT NULL,

                      `status` enum('0','1','2') DEFAULT '0' COMMENT '0 for key available,1 for not available',

                      `created_at` timestamp NULL DEFAULT NULL,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createLateFeeManagement($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `late_fee_management` (

                     `id` int(11) NOT NULL AUTO_INCREMENT,

                     `property_id` int(20) UNSIGNED DEFAULT NULL,

                     `checkbox` enum('1','2') DEFAULT NULL COMMENT '1=Apply One Time,2=Apply Daily',

                     `apply_one_time_flat_fee` varchar(255) DEFAULT NULL,

                     `apply_one_time_percentage` varchar(100) DEFAULT NULL,

                     `apply_daily_flat_fee` varchar(255) DEFAULT NULL,

                     `apply_daily_percentage` varchar(100) DEFAULT NULL,

                     `grace_period` varchar(255) DEFAULT NULL,

                     `created_at` timestamp NULL DEFAULT NULL,

                     `update_at` timestamp NULL DEFAULT NULL,

                     `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)                  

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createLinkOwners($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `link_owners` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `property_id` int(11) DEFAULT NULL,

                      `owner_percentowned` text,

                      `payment_type` varchar(255) DEFAULT NULL,

                      `vendor_1099_payer` varchar(255) DEFAULT NULL,

                      `fiscal_year_end` varchar(255) DEFAULT NULL,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `update_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createMaintenanceInformation($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `maintenance_information` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `property_id` int(11) DEFAULT NULL,

                      `maintenance_speed_time` varchar(255) DEFAULT NULL,

                      `amount` varchar(255) DEFAULT NULL,

                      `Insurance_expiration` varchar(255) DEFAULT NULL,

                      `warranty_expiration` varchar(255) DEFAULT NULL,

                      `special_instructions` varchar(255) DEFAULT NULL,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createManagementFees($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `management_fees` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `property_id` int(11) DEFAULT NULL,

                      `management_type` varchar(255) DEFAULT NULL,

                      `management_value` varchar(255) DEFAULT NULL,

                      `minimum_management_fee` varchar(100) DEFAULT NULL,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createManagementInfo($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `management_info` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `property_id` int(11) DEFAULT NULL,

                      `management_company_name` varchar(255) DEFAULT NULL,

                      `management_start_date` varchar(255) DEFAULT NULL,

                      `management_end_date` varchar(255) DEFAULT NULL,

                      `reason_management_end` varchar(255) DEFAULT NULL,

                      `description` varchar(255) DEFAULT NULL,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createCustomFields($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `property_custom_fields` (

                      `id` int(20) NOT NULL AUTO_INCREMENT,

                      `user_id` int(20) DEFAULT NULL,

                      `property_id` int(11) DEFAULT NULL,

                      `custom_field` text,

                      `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createCompanyGarageAvail($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_garage_avail` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `user_id` int(11) DEFAULT NULL,

                      `garage_avail` varchar(255) DEFAULT NULL,

                      `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

                      `update_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createCompanyKeyAccess($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_key_access` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `user_id` int(11) NOT NULL,

                      `key_code` varchar(255) NOT NULL,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `update_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createCompanyPetFriendly($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_pet_friendly` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `pet_friendly` varchar(255) DEFAULT NULL,

                      `user_id` int(11) DEFAULT NULL,

                      `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createOwnerBlacklistVendors($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `owner_blacklist_vendors` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `property_id` int(11) DEFAULT NULL,

                      `vendor_name` varchar(255) DEFAULT NULL,

                      `vendor_address` varchar(255) DEFAULT NULL,

                      `blacklist_date` varchar(255) DEFAULT NULL,

                      `blacklist_reason` varchar(255) DEFAULT NULL,

                      `status` int(11) DEFAULT NULL,

                      `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                       PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createMortgageInformation($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `mortgage_information` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `property_id` int(11) DEFAULT NULL,

                      `mortgage_amount` varchar(100) DEFAULT NULL,

                      `property_assessed_amount` varchar(100) DEFAULT NULL,

                      `mortgage_start_date` varchar(100) DEFAULT NULL,

                      `term` int(11) NOT NULL,

                      `interest_rate` int(11) NOT NULL,

                      `financial_institution` varchar(250) NOT NULL,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createPropertyInsurance($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `property_insurance` (

                     `id` int(11) NOT NULL AUTO_INCREMENT,

                     `property_id` int(11) DEFAULT NULL,

                     `company_insurance_type` varchar(255) DEFAULT NULL,

                     `insurance_company_name` varchar(255) DEFAULT NULL,

                     `policy` varchar(255) DEFAULT NULL,

                     `policy_name` int(10) DEFAULT NULL,

                     `agent_name` varchar(255) DEFAULT NULL,

                     `insurance_country_code` varchar(255) DEFAULT NULL,

                     `phone_number` varchar(255) DEFAULT NULL,

                     `property_insurance_email` varchar(255) DEFAULT NULL,

                     `coverage_amount` varchar(255) DEFAULT NULL,

                     `annual_premium` varchar(255) DEFAULT NULL,

                     `start_date` varchar(255) NOT NULL,

                     `end_date` varchar(255) NOT NULL,

                     `renewal_date` varchar(255) NOT NULL,

                     `notes` varchar(255) DEFAULT NULL,

                     `status` enum('0','1') DEFAULT '1',

                     `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                     `updated_at` timestamp NULL DEFAULT NULL,

                     `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`),

                      KEY `property_id` (`property_id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createWarrantyInformation($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `warranty_information` (

                     `id` int(11) NOT NULL AUTO_INCREMENT,

                     `property_id` int(11) DEFAULT NULL,

                     `key_fixture_name` varchar(255) DEFAULT NULL,

                     `fixture_type` varchar(255) DEFAULT NULL,

                     `model` varchar(255) DEFAULT NULL,

                     `assessed_age` varchar(255) DEFAULT NULL,

                     `maintenance_reminder` varchar(255) DEFAULT NULL,

                     `warranty_expiration_date` varchar(255) DEFAULT NULL,

                     `insurance_expiration_date` varchar(255) DEFAULT NULL,

                     `manufacturer_name` varchar(255) DEFAULT NULL,

                     `manufacturer_phonenumber` varchar(255) DEFAULT NULL,

                     `add_notes` varchar(255) DEFAULT NULL,

                     `status` enum('0','1') DEFAULT '1',

                     `condition_fixture` varchar(255) DEFAULT NULL,

                     `created_at` timestamp NULL DEFAULT NULL,

                     `updated_at` timestamp NULL DEFAULT NULL,

                     `deleted_at` timestamp NULL DEFAULT NULL,

                     PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createPropertyNotes($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `property_notes` (

                     `id` int(11) NOT NULL AUTO_INCREMENT,

                     `property_id` int(11) DEFAULT NULL,

                     `notes` varchar(255) DEFAULT NULL,

                     `status` enum('0','1') DEFAULT '1',

                     `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                     `updated_at` timestamp NULL DEFAULT NULL,

                     `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createPropertyLoan($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `property_loan` (

                     `id` int(11) NOT NULL AUTO_INCREMENT,

                     `property_id` int(11) DEFAULT NULL,

                     `loan_status` int(11) NOT NULL,

                     `vendor_name` varchar(255) DEFAULT NULL,

                     `loan_amount` varchar(255) DEFAULT NULL,

                     `loan_start_date` varchar(255) DEFAULT NULL,

                     `loan_maturity_date` varchar(255) DEFAULT NULL,

                     `monthly_due_date` varchar(255) DEFAULT NULL,

                     `payment_amount` varchar(255) DEFAULT NULL,

                     `interest_only` varchar(255) NOT NULL,

                     `loan_terms` varchar(255) DEFAULT NULL,

                     `loan_type` varchar(255) DEFAULT NULL,

                     `loan_rate` varchar(255) DEFAULT NULL,

                     `loan_number` varchar(255) NOT NULL,

                     `created_at` timestamp NULL DEFAULT NULL,

                     `updated_at` timestamp NULL DEFAULT NULL,

                     `deleted_at` timestamp NULL DEFAULT NULL,

                     PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    //new
    public static function createRenovation_details($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `renovation_details` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `user_id` int(11) NOT NULL,

                      `object_id` int(11) DEFAULT NULL,

                      `object_type` varchar(255) NOT NULL COMMENT 'property,building or unit',

                      `last_renovation_date` date DEFAULT NULL,

                      `last_renovation_time` time DEFAULT NULL,

                      `last_renovation_description` text,

                      `status` tinyint(4) NOT NULL,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createCompanyPropertyInsurance($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_property_insurance` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `property_id` int(11) NOT NULL,

                      `insurance_type` varchar(255) NOT NULL, 

                      `user_id` int(11) NOT NULL,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createFixture_types($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `fixture_types` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `user_id` int(11) UNSIGNED NOT NULL,

                      `fixture_type` varchar(255) NOT NULL,

                      `fixture_desc` varchar(255) NOT NULL,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createCompanyPropertyPolicy($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_property_policy` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `property_id` int(11) DEFAULT NULL,

                      `user_id` int(11) DEFAULT NULL,

                      `policy_type` varchar(255) NOT NULL,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createCompanyManagementReason($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `company_management_reason` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `user_id` int(11) DEFAULT NULL,

                      `reason` varchar(255) DEFAULT NULL,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createCompanyCreditAccounts($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `company_credit_accounts` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `user_id` int(11) NOT NULL,

                      `credit_accounts` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      `created_at` timestamp NULL DEFAULT NULL,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createCompanyDebitAccounts($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `company_debit_accounts` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `user_id` int(11) NOT NULL,

                      `debit_accounts` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      `created_at` timestamp NULL DEFAULT NULL,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createCompanyKeyTracker($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `track_key` (

                     `id` int(11) NOT NULL AUTO_INCREMENT,

                     `property_id` int(11) DEFAULT NULL,

                     `key_name` varchar(255) DEFAULT NULL,

                     `trackemail` varchar(255) DEFAULT NULL,

                     `company_name` varchar(255) DEFAULT NULL,

                     `phone` varchar(255) DEFAULT NULL,

                     `Address1` varchar(255) DEFAULT NULL,

                     `Address2` varchar(255) DEFAULT NULL,

                     `Address3` varchar(255) DEFAULT NULL,

                     `Address4` varchar(255) DEFAULT NULL,

                     `key_number` varchar(255) DEFAULT NULL,

                     `key_quality` varchar(255) DEFAULT NULL,

                     `pick_up_date` varchar(255) DEFAULT NULL,

                     `pick_up_time` varchar(255) DEFAULT NULL,

                     `return_date` varchar(255) DEFAULT NULL,

                     `return_time` varchar(255) DEFAULT NULL,

                     `key_designator` varchar(255) DEFAULT NULL,

                     `created_at` timestamp NULL DEFAULT NULL,

                     `updated_at` timestamp NULL DEFAULT NULL,

                     `deleted_at` timestamp NULL DEFAULT NULL,

                     PRIMARY KEY (`id`) 

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createPropertyBankAccount($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `property_bank_details` (

                      `id` int(20) NOT NULL AUTO_INCREMENT,

                      `property_id` int(11) DEFAULT NULL,

                      `account_name` varchar(255) DEFAULT NULL,

                      `chart_of_account` varchar(255) DEFAULT NULL,

                      `account_type` varchar(255) DEFAULT NULL,

                      `bank_name` varchar(255) DEFAULT NULL,

                      `bank_routing` varchar(255) DEFAULT NULL,

                      `bank_account` varchar(255) DEFAULT NULL,

                      `default_security_deposit` varchar(255) DEFAULT NULL,

                      `bank_account_min_reserve_amount` varchar(255) DEFAULT NULL,

                      `description` text,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`) 

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());

        }
    }
    public static function createUnitDetails($connection) {
        $return = array();
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `unit_details` (

                  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,                  

                  `user_id` int(11) NOT NULL,

                  `property_id` int(11) DEFAULT NULL,

                  `building_id` int(11) DEFAULT NULL,

                  `floor_no` int(11) NOT NULL,

                  `unit_prefix` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

                  `unit_type_id` int(11) DEFAULT NULL,

                  `unit_no` int(11) NOT NULL,

                  `square_ft` varchar(255) DEFAULT NULL,

                  `bedrooms_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `bathrooms_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `base_rent` double DEFAULT NULL,

                  `market_rent` double DEFAULT NULL,

                  `security_deposit` double DEFAULT NULL,

                  `smoking_allowed` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',

                  `pet_friendly_id` int(11) DEFAULT NULL,

                  `pet_friendly_condition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `phone_number1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `phone_number2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `phone_number3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `fax_number1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `fax_number2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `fax_number3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `school_district_municipality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `school_district_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `school_district_notes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `building_unit_status` int(11) DEFAULT NULL,

                  `last_renovation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `last_renovation_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `last_renovation_description` text COLLATE utf8mb4_unicode_ci,

                  `key_access_codes` text COLLATE utf8mb4_unicode_ci,

                  `key_access_codes_desc` text COLLATE utf8mb4_unicode_ci,

                  `amenities` text COLLATE utf8mb4_unicode_ci,

                  `building_description` text COLLATE utf8mb4_unicode_ci,

                  `created_by` int(11) DEFAULT NULL,

                  `modified_by` int(11) DEFAULT NULL,

                  `status` int(11) DEFAULT NULL,

                  `deleted_at` timestamp NULL DEFAULT NULL,

                  `created_at` timestamp NULL DEFAULT NULL,

                  `updated_at` timestamp NULL DEFAULT NULL,

                  `unit_as` enum('1','2') COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `building_unit_notes` text COLLATE utf8mb4_unicode_ci,

                  `key_tag` text COLLATE utf8mb4_unicode_ci,

                  `key_desc` text COLLATE utf8mb4_unicode_ci,

                  `total_keys` text COLLATE utf8mb4_unicode_ci,

                  `building_unit_note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `custom_field` text COLLATE utf8mb4_unicode_ci,

                  `group_fields` text COLLATE utf8mb4_unicode_ci,

                  `building_id_check` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `unit_check` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `key_unit` enum('1','2') COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `building_name_and_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                   PRIMARY KEY (`id`)

                )";
            $connection->prepare($table)->execute();
            return array('code' => 200, 'status' => 'success', 'message' => 'company propertySetup style table created successfully');
        }
        catch(PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
        }
        return $return;
    }
    public static function createBuildingDetails($connection) {
        $return = array();
        try {
            $table = "CREATE TABLE IF NOT EXISTS `building_detail` (

                  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

                  `property_id` int(11) NOT NULL,

                  `building_id` varchar(20) DEFAULT NULL,

                  `building_name` varchar(255) DEFAULT NULL,

                  `legal_name` varchar(30) DEFAULT NULL,

                  `address` text DEFAULT NULL,

                  `phone_number` text,

                  `note` varchar(255) DEFAULT NULL,

                  `fax_number` varchar(255) DEFAULT NULL,

                  `last_renovation_date` varchar(255) DEFAULT NULL,

                  `last_renovation_time` varchar(255) DEFAULT NULL,

                  `last_renovation_description` varchar(255) DEFAULT NULL,

                  `no_of_units` int(11) DEFAULT NULL,

                  `linked_units` int(11) NOT NULL,

                  `smoking_allowed` varchar(255) DEFAULT NULL,

                  `pet_friendly` varchar(255) DEFAULT NULL,

                  `school_district_municipality` varchar(255) DEFAULT NULL,

                  `key_access_codes_info` varchar(255) DEFAULT NULL,

                  `key_access_code_desc` text DEFAULT NULL,

                  `amenities` longtext,

                  `description` text,

                  `is_building_with_no_unit` varchar(255) DEFAULT NULL,

                  `unit_type` varchar(255) DEFAULT NULL,

                  `base_rent` varchar(255) DEFAULT NULL,

                  `market_rent` varchar(255) DEFAULT NULL,

                  `security_deposit` varchar(255) DEFAULT NULL,

                  `custom_field` text,

                  `status` tinyint(4) DEFAULT '1',

                  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

                  `deleted_at` timestamp NULL DEFAULT NULL,

                   PRIMARY KEY (`id`)

                )";
            $connection->prepare($table)->execute();
            return array('code' => 200, 'status' => 'success', 'message' => 'building detail table created successfully');
        }
        catch(PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
        }
        return $return;
    }
    public static function createBuildingFileUploads($connection) {
        $return = array();
        try {
            $table = "CREATE TABLE IF NOT EXISTS `building_file_uploads` (

                  `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT,

                  `building_id` int(20) UNSIGNED DEFAULT NULL,

                  `user_id` int(20) DEFAULT NULL,

                  `file_name` text,

                  `file_size` varchar(20) DEFAULT NULL,

                  `file_location` text,

                  `file_extension` varchar(20) DEFAULT NULL,

                  `codec` varchar(100) DEFAULT NULL,

                  `file_type` enum('1','2','3') DEFAULT NULL COMMENT '1=images,2=documents,3=videos',

                  `created_at` timestamp NULL DEFAULT NULL,

                  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,

                  `deleted_at` timestamp NULL DEFAULT NULL,

                  PRIMARY KEY (`id`)

                  )";
            $connection->prepare($table)->execute();
            return array('code' => 200, 'status' => 'success', 'message' => 'building file uploads table created successfully');
        }
        catch(PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
        }
        return $return;
    }
    public static function createBuildingKeys($connection) {
        $return = array();
        try {
            $table = "CREATE TABLE IF NOT EXISTS `building_keys` (

                  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

                  `building_id` int(11) DEFAULT NULL,

                  `key_id` varchar(255) DEFAULT NULL,

                  `key_description` varchar(255) DEFAULT NULL,

                  `total_keys` varchar(255) DEFAULT NULL,

                  `available_keys` int(11) NOT NULL DEFAULT '0',

                  `status` int(11) NOT NULL COMMENT '0=>keys not available,1=>keys available',

                  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

                  `deleted_at` timestamp NULL DEFAULT NULL,

                  PRIMARY KEY (`id`)

                  )";
            $connection->prepare($table)->execute();
            return array('code' => 200, 'status' => 'success', 'message' => 'building keys table created successfully');
        }
        catch(PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
        }
        return $return;
    }
    public static function createBuildingTrackKeys($connection) {
        $return = array();
        try {
            $table = "CREATE TABLE IF NOT EXISTS `building_track_key` (

                  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

                  `building_id` int(11) DEFAULT NULL,

                  `key_holder` varchar(255) DEFAULT NULL,

                  `email` varchar(255) DEFAULT NULL,

                  `company_name` varchar(255) DEFAULT NULL,

                  `phone` varchar(255) DEFAULT NULL,

                  `Address1` varchar(255) DEFAULT NULL,

                  `Address2` varchar(255) DEFAULT NULL,

                  `Address3` varchar(255) DEFAULT NULL,

                  `Address4` varchar(255) DEFAULT NULL,

                  `key_number` varchar(255) DEFAULT NULL,

                  `key_quality` varchar(255) DEFAULT NULL,

                  `pick_up_date` varchar(255) DEFAULT NULL,

                  `pick_up_time` varchar(255) DEFAULT NULL,

                  `return_date` varchar(255) DEFAULT NULL,

                  `return_time` varchar(255) DEFAULT NULL,

                  `key_designator` varchar(255) DEFAULT NULL,

                  `created_at` datetime DEFAULT NULL,

                  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,

                  `deleted_at` timestamp NULL DEFAULT NULL,

                   PRIMARY KEY (`id`)

                  )";
            $connection->prepare($table)->execute();
            return array('code' => 200, 'status' => 'success', 'message' => 'building keys table created successfully');
        }
        catch(PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
        }
        return $return;
    }
    public static function createComplaints($connection) {
        $return = array();
        try {
            $table = "CREATE TABLE IF NOT EXISTS `complaints` (

                  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                  `user_id` int(11) NOT NULL,

                  `object_id` int(11) DEFAULT NULL,

                  `module_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                  `complaint_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,

                  `complaint_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,

                  `complaint_date` date DEFAULT NULL,

                  `complaint_type_id` int(11) DEFAULT NULL,

                  `other_notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,

                  `complaint_note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,

                  `status` int(11) DEFAULT NULL,

                  `deleted_at` timestamp NULL DEFAULT NULL,

                  `created_at` timestamp NULL DEFAULT NULL,

                  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,

                   PRIMARY KEY (`id`)

                  )";
            $connection->prepare($table)->execute();
            return array('code' => 200, 'status' => 'success', 'message' => 'building keys table created successfully');
        }
        catch(PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
        }
        return $return;
    }
    public static function createComplaintTypes($connection) {
        $return = array();
        try {
            $table = "CREATE TABLE IF NOT EXISTS `complaint_types` (

                      `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                      `user_id` int(11) DEFAULT '1',

                      `complaint_type` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `status` int(11) DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      `created_at` timestamp NULL DEFAULT NULL,

                      `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,

                       PRIMARY KEY (`id`)

                  )";
            $connection->prepare($table)->execute();
            return array('code' => 200, 'status' => 'success', 'message' => 'building keys table created successfully');
        }
        catch(PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
        }
        return $return;
    }
    public static function createBuildingKeyCheckout($connection) {
        $return = array();
        try {
            $table = "CREATE TABLE IF NOT EXISTS `building_key_checkout` (

                    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                    `key_id` int(11) DEFAULT NULL,

                    `key_holder` varchar(255) DEFAULT NULL,

                    `checkout_desc` varchar(255) DEFAULT NULL,

                    `checkout_keys` int(11) DEFAULT NULL,

                    `available_keys` int(11) DEFAULT NULL,

                    `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                    `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,

                     PRIMARY KEY (`id`)

                  )";
            $connection->prepare($table)->execute();
            return array('code' => 200, 'status' => 'success', 'message' => 'building keys checkout table created successfully');
        }
        catch(PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
        }
        return $return;
    }
    public static function createPropertyPercentOwned($connection) {
        $return = array();
        try {
            $table = "CREATE TABLE IF NOT EXISTS `property_percent_owned` (

                    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                    `property_id` int(11) DEFAULT NULL,

                    `owner_id` int(11) DEFAULT NULL,

                    `percet_owned` varchar(255) DEFAULT NULL,

                    `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                    `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,

                     PRIMARY KEY (`id`)

                  )";
            $connection->prepare($table)->execute();
            return array('code' => 200, 'status' => 'success', 'message' => 'Property percent owned table created successfully');
        }
        catch(PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
        }
        return $return;
    }
    public static function createUnitGroupFields($connection) {
        $return = array();
        try {
            $table = "

                    CREATE TABLE IF NOT EXISTS  `unit_group_fields` (

                      `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                      `unit_id` int(11) NOT NULL,

                      `type` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL,

                      `building_id` int(11) DEFAULT NULL,

                      `floor_no` int(11) DEFAULT NULL,

                      `unit_prefix` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `unit_from` double DEFAULT NULL,

                      `unit_to` double DEFAULT NULL,

                      `unit_no` double DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      `created_at` timestamp NULL DEFAULT NULL,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
            return array('code' => 200, 'status' => 'success', 'message' => 'company propertySetup style table created successfully');
        }
        catch(PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
        }
        return $return;
    }
    public static function createUnitImages($connection) {
        $return = array();
        try {
            $table = "

                    CREATE TABLE IF NOT EXISTS  `unit_images` (

                     `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,                      

                     `user_id` int(11) NOT NULL,

                      `image_url` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `unit_id` int(11) DEFAULT NULL,

                      `image_type` int(11) DEFAULT NULL,

                      `created_by` int(11) DEFAULT NULL,

                      `modified_by` int(11) DEFAULT NULL,

                      `status` int(11) DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      `created_at` timestamp NULL DEFAULT NULL,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `unit_unique_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `file_size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
            return array('code' => 200, 'status' => 'success', 'message' => 'company propertySetup style table created successfully');
        }
        catch(PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
        }
        return $return;
    }
    public static function createUnitKeys($connection) {
        $return = array();
        try {
            $table = "

                   CREATE TABLE IF NOT EXISTS  `unit_keys` (

                      `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                      `unit_id` int(11) DEFAULT NULL,

                      `key_id` varchar(255) DEFAULT NULL,

                      `key_description` varchar(255) DEFAULT NULL,

                      `total_keys` varchar(255) DEFAULT NULL,

                      `available_keys` int(11) NOT NULL DEFAULT '0',

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      `status` int(11) DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
            return array('code' => 200, 'status' => 'success', 'message' => 'company propertySetup style table created successfully');
        }
        catch(PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
        }
        return $return;
    }
    //property table end
    public static function createCompanySubAccountType($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_account_sub_type` (

                `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                `user_id` int(11) NOT NULL,

                `account_type_id` int(11) NOT NULL,

                `account_sub_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

                `created_at` timestamp NULL DEFAULT NULL,

                `updated_at` timestamp NULL DEFAULT NULL,

                `deleted_at` timestamp NULL DEFAULT NULL,

                PRIMARY KEY (`id`)

        )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);

        }
    }
    public static function createCompanyRoleAuthorization($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `user_role_authorizations` (

                `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                `property_id` int(11) DEFAULT NULL,

                `user_id` int(11) DEFAULT NULL,

                `role_id` int(11) NOT NULL,

                `created_at` timestamp NULL DEFAULT NULL,

                `updated_at` timestamp NULL DEFAULT NULL,

                `deleted_at` timestamp NULL DEFAULT NULL,

                PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //  print_r($exception);

        }
    }
    public static function createCompanyAccountingPreferences($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_accounting_preferences` (

                      `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                      `user_id` int(11) DEFAULT NULL,

                      `eft_generate_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

                      `created_at` timestamp NULL DEFAULT NULL,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `fiscal_year_first_month` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `cash_account_credit_bill_posting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `rent_charges` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `check_receive_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `debit_type_receive_payment_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `vacancy_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `waiveoff_on_payment_received_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `bad_debit_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `write_off_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `money_order_payment_received_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `auto_debit_payment_receive_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `cash_payment_received_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `nsf_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `check_bounce_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `check_issue_vendor_tenant_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `refund_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `interest_received_from_bank_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `bank_adjustment_fee_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `concession_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `transfer_fee_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `late_fee_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `discount_account_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `eft_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `cam_charges` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `sd_refund_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `warn_transaction_days_future` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `warn_transaction_days_past` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `color_negative_balances_in_red` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `lease_expire_soon_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `use_parenthesis_instead_of_a_minus_sign` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `auto_invoice` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `auto_invoice_days_before_end_of_month` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `warning_future_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `warning_past_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                       PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);

        }
    }
    public static function createFlags($connection) {
        try {
            //   $sql=$connection->prepare("DROP TABLE  flags ");
            //$sql->execute();
            $table = "CREATE TABLE IF NOT EXISTS `flags` (

                      `id` int(20) NOT NULL AUTO_INCREMENT,

                      `object_id` int(20) DEFAULT NULL,

                      `user_id` int(20) DEFAULT NULL,

                      `object_type` enum('property','unit','building','tenant','employee','contact','vendor','owner') DEFAULT NULL,

                      `flag_name` varchar(200) DEFAULT NULL,

                      `flag_by` varchar(250) DEFAULT NULL,

                      `date` varchar(50) DEFAULT NULL,

                      `country_code`  varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `flag_phone_number` varchar(20) DEFAULT NULL,

                      `flag_reason` varchar(250) DEFAULT NULL,

                      `completed` enum('0','1') DEFAULT NULL,

                      `flag_note` text,

                      `created_at` timestamp NULL DEFAULT NULL,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //      print_r($exception);

        }
    }
    public static function createPropertyManageCharges($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `property_manage_charge` (

                      `id` int(20) NOT NULL AUTO_INCREMENT,

                      `property_id` int(11) DEFAULT NULL,

                      `prefrence_id` int(20) DEFAULT NULL,

                      `user_id` int(20) DEFAULT NULL,

                      `charge_code_type` enum('0','1') DEFAULT NULL,

                      `charge_code` int(10) DEFAULT NULL,

                      `frequency` enum('1','2') DEFAULT NULL,

                      `type` enum('1','2','3') DEFAULT NULL,

                      `unit_type_id` int(10) DEFAULT NULL,

                      `amount` varchar(50) DEFAULT NULL,

                      `is_editable` enum('0','1') DEFAULT '1',

                      `checked` enum('0','1') DEFAULT NULL,

                      `created_at` timestamp NULL DEFAULT NULL,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);

        }
    }
    public static function createPropertyWatingList($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `property_wating_list` (

                      `id` int(20) NOT NULL AUTO_INCREMENT,

                      `property_id` int(11) DEFAULT NULL,

                      `use_entity_name` enum('0','1') DEFAULT NULL,

                      `entity_name` varchar(200) DEFAULT NULL,

                      `preferred` int(10) DEFAULT NULL,

                      `pets` enum('0','1','2') DEFAULT NULL,

                      `property_name` varchar(200) DEFAULT NULL,

                      `building_id` int(10) DEFAULT NULL,

                      `unit` int(10) DEFAULT NULL,

                      `first_name` varchar(200) DEFAULT NULL,

                      `middle_name` varchar(200) DEFAULT NULL,

                      `last_name` varchar(200) DEFAULT NULL,

                      `maiden_name` varchar(200) DEFAULT NULL,

                      `nickname` varchar(200) DEFAULT NULL,

                      `phone` varchar(50) DEFAULT NULL,

                      `email` varchar(150) DEFAULT NULL,

                      `zipcode` varchar(50) DEFAULT NULL,

                      `country` varchar(50) DEFAULT NULL,

                      `state` varchar(50) DEFAULT NULL,

                      `city` varchar(50) DEFAULT NULL,

                      `address1` varchar(200) DEFAULT NULL,

                      `address2` varchar(200) DEFAULT NULL,

                      `address3` varchar(200) DEFAULT NULL,

                      `address4` varchar(200) DEFAULT NULL,

                      `note` text,

                      `created_at` timestamp NULL DEFAULT NULL,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //  print_r($exception);

        }
    }
    public static function createPropertyAccountName($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `account_name_details` (

                     `id` int(11) NOT NULL AUTO_INCREMENT,

                     `user_id` int(11) DEFAULT NULL,

                     `account_name` varchar(255) DEFAULT NULL,

                     `created_at` timestamp NULL DEFAULT NULL,

                     `updated_at` timestamp NULL DEFAULT NULL,

                     `deleted_at` timestamp NULL DEFAULT NULL,

                     PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //  print_r($exception);

        }
    }
    public static function createPropertyAccountType($connection) {
        try {
            $sql = $connection->prepare("DROP TABLE IF EXISTS account_type_details ");
            $sql->execute();
            $table = "CREATE TABLE IF NOT EXISTS `account_type_details` (

                     `id` int(11) NOT NULL AUTO_INCREMENT,

                     `user_id` int(11) DEFAULT NULL,

                     `account_type` varchar(255) DEFAULT NULL,

                     `account_type_id` int(30) DEFAULT NULL,

                     `created_at` timestamp NULL DEFAULT NULL,

                     `updated_at` timestamp NULL DEFAULT NULL,

                     `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);

        }
    }
    public static function unit_file_uploads($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `unit_file_uploads` (

              `id` int(11) NOT NULL AUTO_INCREMENT,

              `unit_id` int(20) UNSIGNED DEFAULT NULL,

              `user_id` int(20) DEFAULT NULL,

              `file_name` text,

              `file_size` varchar(20) DEFAULT NULL,

              `file_location` text,

              `file_extension` varchar(20) DEFAULT NULL,

              `codec` varchar(100) DEFAULT NULL,

              `file_type` enum('1','2','3') DEFAULT NULL COMMENT '1=images,2=documents,3=videos',

              `created_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NULL DEFAULT NULL,

              `deleted_at` timestamp NULL DEFAULT NULL,

              PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);

        }
    }
    public static function unit_group_fields($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `unit_group_fields` (

               `id` int(11) NOT NULL AUTO_INCREMENT,

              `unit_id` int(11) NOT NULL,

              `type` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL,

              `building_id` int(11) DEFAULT NULL,

              `floor_no` int(11) DEFAULT NULL,

              `unit_prefix` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

              `unit_from` double DEFAULT NULL,

              `unit_to` double DEFAULT NULL,

              `unit_no` double DEFAULT NULL,

              `deleted_at` timestamp NULL DEFAULT NULL,

              `created_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NULL DEFAULT NULL,

              PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);

        }
    }
    public static function unit_keys($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `unit_keys` (

               `id` int(11) NOT NULL AUTO_INCREMENT,

              `unit_id` int(11) DEFAULT NULL,

              `key_id` varchar(255) DEFAULT NULL,

              `key_description` varchar(255) DEFAULT NULL,

              `total_keys` varchar(255) DEFAULT NULL,

              `available_keys` int(11) NOT NULL DEFAULT '0',

              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

              `deleted_at` timestamp NULL DEFAULT NULL,

              `status` int(11) DEFAULT NULL,

              PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //print_r($exception);

        }
    }
    public static function unit_key_checkout($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `unit_key_checkout` (

              `id` int(11) NOT NULL AUTO_INCREMENT,

             `unit_id` int(11) NOT NULL,

              `key_id` int(11) DEFAULT NULL,

              `key_holder` varchar(255) DEFAULT NULL,

              `checkout_desc` varchar(255) DEFAULT NULL,

              `checkout_keys` int(11) DEFAULT NULL,

              `available_keys` int(11) DEFAULT NULL,

              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

              `updated_at` timestamp NULL DEFAULT NULL,

              PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //  print_r($exception);

        }
    }
    public static function unit_track_key($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `unit_track_key` (

              `id` int(11) NOT NULL AUTO_INCREMENT,

             `unit_id` int(11) DEFAULT NULL,

              `key_name` varchar(255) DEFAULT NULL,

              `email` varchar(255) DEFAULT NULL,

              `company_name` varchar(255) DEFAULT NULL,

              `phone` varchar(255) DEFAULT NULL,

              `Address1` varchar(255) DEFAULT NULL,

              `Address2` varchar(255) DEFAULT NULL,

              `Address3` varchar(255) DEFAULT NULL,

              `Address4` varchar(255) DEFAULT NULL,

              `key_number` varchar(255) DEFAULT NULL,

              `key_quality` varchar(255) DEFAULT NULL,

              `pick_up_date` varchar(255) DEFAULT NULL,

              `pick_up_time` varchar(255) DEFAULT NULL,

              `return_date` varchar(255) DEFAULT NULL,

              `return_time` varchar(255) DEFAULT NULL,

              `key_designator` varchar(255) DEFAULT NULL,

              `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,

              `updated_at` timestamp NULL DEFAULT NULL,

              `deleted_at` timestamp NULL DEFAULT NULL,

              PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);

        }
    }
    public static function createInspectionNameList($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS inspection_list_property (

                    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

                    `inspection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

                    `status` enum('all','0','1') NOT NULL,

                    `created_at` timestamp NULL DEFAULT NULL,

                    `updated_at` timestamp NULL DEFAULT NULL,

                     PRIMARY KEY (`id`)

                   )";
            $date = date("Y-m-d H:i:s");
            $connection->prepare($table)->execute();
            $insert = "INSERT INTO inspection_list_property (status,created_at,updated_at) VALUES ('all','$date','$date')";
            $connection->prepare($insert)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createPropertyInspection($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `property_inspection` (

                    `id` int(10) NOT NULL AUTO_INCREMENT,

                    `user_id` int(11) NOT NULL,

                    `inspection_date` date NOT NULL,

                    `inspection_type` varchar(255) NOT NULL,

                    `inspection_name` varchar(255) NOT NULL,

                    `object_id` varchar(255) NOT NULL,

                    `object_type` varchar(255) NOT NULL,

                    `notes` varchar(255) DEFAULT NULL,

                    `inspection_area` varchar(255) DEFAULT NULL,

                    `custom_field` varchar(255) DEFAULT NULL,

                    `property_name` varchar(255) DEFAULT NULL,

                    `property_address` varchar(255) DEFAULT NULL,

                    `building_name` varchar(255) DEFAULT NULL,

                    `building_address` varchar(255) DEFAULT NULL,

                    `unit_name` varchar(255) DEFAULT NULL,

                    `created_at` timestamp NULL DEFAULT NULL,

                    `updated_at` timestamp NULL DEFAULT NULL,

                    `deleted_at` timestamp NULL DEFAULT NULL,

                    PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);

        }
    }
    public static function createInspectionPhotos($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `inspection_photos` (

                      `id` int(10) UNSIGNED NOT NULL,

                      `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

                      `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

                      `file_size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `inspection_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

                      `module_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                      `created_at` timestamp NULL DEFAULT NULL,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      `file_location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL

                    ) ";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //   print_r($exception);

        }
    }
    public static function alterInspectionphotos($connection) {
        try {
            $table = "ALTER TABLE `inspection_photos` MODIFY `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }

    public static function createInspectionArea($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `inspection_area` (

                    `id` int(10) NOT NULL AUTO_INCREMENT,

                    `user_id` int(11) NOT NULL,

                    `inspection_area` varchar(255) NOT NULL,

                    `parent_id` varchar(255) NOT NULL,

                    `object_id` varchar(255) NOT NULL,

                    `object_type` varchar(255) NOT NULL,

                    `created_at` timestamp NULL DEFAULT NULL,

                    `updated_at` timestamp NULL DEFAULT NULL,

                    `deleted_at` timestamp NULL DEFAULT NULL,

                    PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //  print_r($exception);

        }
    }
    public static function createInspectionListProperty($connection) {
        try {
            $date = date("Y-m-d H:i:s");
            $table = "CREATE TABLE IF NOT EXISTS `inspection_list_property` (

                    `id` int(10) NOT NULL AUTO_INCREMENT,

                    `inspection_name` varchar(255) NOT NULL,

                    `status` enum('all','0','1') NOT NULL,

                    `created_at` timestamp NULL DEFAULT NULL,

                    `updated_at` timestamp NULL DEFAULT NULL,

                    PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
            $insert = "INSERT INTO `inspection_list_property` (`id`, `inspection_name`, `status`, `created_at`, `updated_at`) VALUES

                        (1, 'Move In Inspection', '1', '$date', '$date'),

                        (2, 'Move Out Inspection', '1', '$date', '$date'),

                        (3, 'Weekly Inspection', '1', '$date', '$date'),

                        (4, 'Monthly Inspection', '1', '$date', '$date'),

                        (5, 'Six-Month Inspection', '1', '$date', '$date'),

                        (6, 'Annual Inspection', '1', '$date', '$date'),

                        (7, 'General Inspection', '1', '$date', '$date'),

                        (8, 'Emergency Inspection', '1', '$date', '$date'),

                        (9, 'Fd', '1', '$date', '$date'),

                        (10, 'Other', '1', '$date', '$date')";
            $connection->prepare($insert)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createKeyCheckout($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `key_checkout` (

                     `id` int(11) NOT NULL AUTO_INCREMENT,

                     `key_id` int(11) DEFAULT NULL,

                     `key_holder` varchar(255) DEFAULT NULL,

                     `checkout_desc` varchar(255) DEFAULT NULL,

                     `checkout_keys` int(11) DEFAULT NULL,

                     `available_keys` int(11) DEFAULT NULL,

                     `created_at` timestamp NULL DEFAULT NULL,

                     `updated_at` timestamp NULL DEFAULT NULL,

                     PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);

        }
    }
    public static function createAdminCheckTemplate($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `cheque_template` (

                     `id` int(11) NOT NULL AUTO_INCREMENT,

                     `company_id` int(11) NOT NULL,

                     `type` enum('1','2','3','4','5') NOT NULL,

                     `elements` varchar(255) NOT NULL,

                     `created_at` timestamp NULL DEFAULT NULL,

                     `updated_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);

        }
    }
    public static function createOwnerAccountName($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `owner_account_name` (

                     `id` int(11) NOT NULL AUTO_INCREMENT,

                      `user_id` int(11) DEFAULT NULL,

                      `account_name` varchar(255) DEFAULT NULL,

                      `created_at` timestamp NULL DEFAULT NULL,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //  print_r($exception);

        }
    }
    public static function createTagsEmailTemplate($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tags_emailtemplate` (

                       `id` int(11) NOT NULL AUTO_INCREMENT,

                      `tag_name` varchar(255) DEFAULT NULL,

                      `template_key` varchar(255) NOT NULL,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                       PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //   print_r($exception);

        }
    }
    public static function createEmailTemplatesAdmin($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `emailtemplatesadmin` (

                       `id` int(11) NOT NULL AUTO_INCREMENT,

                      `template_title` varchar(255) DEFAULT NULL,

                      `template_html` longtext,

                      `template_key` varchar(255) NOT NULL,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                       PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);

        }
    }
    public static function altertenantEmailTemplate($connection) {
        try {
            $table = "ALTER TABLE `emailtemplatesadmin` 

                      MODIFY `template_html` longtext CHARACTER SET utf8";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createOwnerDetails($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `owner_details` (

              `id` int(11) NOT NULL AUTO_INCREMENT,

              `user_id` int(11) DEFAULT NULL,

              `if_entity_name_display` enum('0','1') DEFAULT '0',

              `owner_image` longblob,

              `email` text,

              `entity_company` varchar(255) DEFAULT NULL,

              `emergency_contact_name` text DEFAULT NULL,

              `emergency_relation` text DEFAULT NULL,

              `emergency_country_code` text DEFAULT NULL,

              `emergency_phone_number` text DEFAULT NULL,

              `emergency_email` text DEFAULT NULL,

              `property_id` text,

              `property_percent_owned` text DEFAULT NULL,

              `owner_credential_name` text DEFAULT NULL,

              `owner_credential_type` text DEFAULT NULL,

              `owner_credential_acquire_date` text DEFAULT NULL,

              `owner_credential_expiration_date` text DEFAULT NULL,

              `owner_credential_notice_period` text DEFAULT NULL,

              `status` enum('0','1','2','3','4') DEFAULT '1',

              `reason_for_leaving` text DEFAULT NULL,

              `reason_for_leaving_note` text DEFAULT NULL,

              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

              `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

              `deleted_at` timestamp NULL DEFAULT NULL,

               PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            // echo '<pre>'; print_r($exception);echo '</pre>';

        }
    }
    public static function createOwnerPropertyOwned($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `owner_property_owned` (

            `id` int(11) NOT NULL AUTO_INCREMENT,

              `user_id` int(11) NOT NULL,

              `property_id` int(11) DEFAULT NULL,

              `property_percent_owned` text DEFAULT NULL,

              `created_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NULL DEFAULT NULL,

              `deleted_at` timestamp NULL DEFAULT NULL,

              PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //  print_r($exception);

        }
    }
    public static function createReasonMoveOutTenant($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `reasonmoveout_tenant` (

                        `id` int(11) NOT NULL AUTO_INCREMENT,

                        `reasonName` varchar(255) NOT NULL,

                        `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                        `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

                        `deleted_at` timestamp NULL DEFAULT NULL,

                        PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //  print_r($exception);

        }
    }
    public static function alterUserTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `users` LIKE 'if_entity_name_display'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE users ADD if_entity_name_display enum('0','1') DEFAULT '0'";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);

        }
    }
    public static function alterFlagTable($connection) {
        try {
            //            $query = "ALTER TABLE flags CHANGE object_type
            //
            //enum('property','unit','building','tenant','employee','contact','owner','vendor') DEFAULT NULL";
            //
            //            $connection->prepare($query)->execute();
            //  die($query);

        }
        catch(Exception $exception) {
            // print_r($exception); //die;

        }
    }
    public static function alterPropertyTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `general_property` LIKE 'clone_status'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `general_property` ADD `clone_status` INT NULL DEFAULT '0' AFTER `deleted_at`, ADD `clone_id` INT NULL AFTER `deleted_at`";
            $connection->prepare($query)->execute();
            //  die($query);

        }
        catch(Exception $exception) {
            // print_r($exception); //die;

        }
    }
    public static function alterUserTableOwnerPortal($connection) {
        try {
            $query2 = "ALTER TABLE `users` CHANGE `owners_portal` `owners_portal`

                ENUM('0','1','2') CHARACTER SET utf8

                COLLATE utf8_general_ci NOT NULL";
            $connection->prepare($query2)->execute();
        }
        catch(Exception $exception) {
            //   print_r($exception);

        }
    }
    public static function createEmployeeDetails($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS  `employee_details` (

                `id` int(10) NOT NULL AUTO_INCREMENT,

                `user_id` int(10) UNSIGNED NOT NULL,

                `employee_image` longblob,

                `phone_number_note` text,

                `email1` varchar(255) DEFAULT NULL,

                `email2` varchar(255) DEFAULT NULL,

                `email3` varchar(255) DEFAULT NULL,

                `referral_source` int(11) DEFAULT NULL,

                `employee_license_state` varchar(255) DEFAULT NULL,

                `employee_license_number` varchar(255) DEFAULT NULL,

                `vehicle` enum('0','1') DEFAULT NULL,

                `current_company` varchar(255) DEFAULT NULL,

                `previous_company` varchar(255) DEFAULT NULL,

                `status` enum('1','0','2','3','4','5','6') DEFAULT NULL,

                `record_status` enum('0','1') NOT NULL,

                `custom_field` text,

                `deleted_at` timestamp NULL DEFAULT NULL,

                `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

                `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

                PRIMARY KEY (`id`)

                )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //   print_r($exception);

        }
    }
    public static function createOwnerBankingInformation($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `owner_banking_information` (

              `id` int(11) NOT NULL AUTO_INCREMENT,

              `user_id` int(11) NOT NULL,

              `account_name` text DEFAULT NULL,

              `account_for_transaction` text DEFAULT NULL,

              `bank_account_type` text DEFAULT NULL,

              `bank_account_number` text DEFAULT NULL,

              `routing_transit_number` text DEFAULT NULL,

              `bank_institution_name` text DEFAULT NULL,

              `bank_fraction_number` text DEFAULT NULL,

              `created_at` timestamp NULL DEFAULT NULL,

              `updated_at` timestamp NULL DEFAULT NULL,

              `deleted_at` timestamp NULL DEFAULT NULL,

               PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //   print_r($exception);

        }
    }
    public static function createVendorAdditionalDetails($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `vendor_additional_detail` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `vendor_id` int(11) NOT NULL,

                      `status` enum('0','1','2','3','4') DEFAULT '1',

                      `vendor_rate` varchar(255) DEFAULT NULL,

                      `vendor_random_id` varchar(255) DEFAULT NULL,

                      `vendor_type_id` int(11) DEFAULT NULL,

                      `image` longblob,

                      `account_name` varchar(255) DEFAULT NULL,

                      `bank_account_number` varchar(255) DEFAULT NULL,

                      `routing_number` varchar(255) DEFAULT NULL,

                      `eligible_for_1099` varchar(255) DEFAULT NULL,

                      `comment` varchar(255) DEFAULT NULL,

                      `consolidate_checks` varchar(255) DEFAULT NULL,

                      `order_limit` varchar(255) DEFAULT NULL,

                      `default_GL` varchar(255) DEFAULT NULL,

                      `is_pmc` enum('0','1') DEFAULT NULL,

                      `name_on_check` varchar(250) DEFAULT NULL,

                      `reason_for_leaving` varchar(100) DEFAULT NULL,

                      `reason_note` text,

                      `rating` varchar(11) DEFAULT NULL,

                      `email` text,

                      `created_at` timestamp NULL DEFAULT NULL,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //  print_r($exception);

        }
    }
    public static function createUserAlerts($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `user_alerts` (

                        `id` int(11) NOT NULL AUTO_INCREMENT,

                         `user_id` int(11) NOT NULL,

                         `alert_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

                         `description` text COLLATE utf8mb4_unicode_ci NOT NULL,

                         `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

                         `send_to_users` text COLLATE utf8mb4_unicode_ci,

                         `module_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

                         `alert_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,

                         `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,

                         `created_at` timestamp NULL DEFAULT NULL,

                         `updated_at` timestamp NULL DEFAULT NULL,

                         `deleted_at` timestamp NULL DEFAULT NULL,

                         `no_of_days_before` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

                         `alert_message` text COLLATE utf8mb4_unicode_ci,

                           PRIMARY KEY (`id`)

    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //  print_r($exception);

        }
    }
    public static function createContactNotes($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `contact_notes` (

              `id` int(11) AUTO_INCREMENT PRIMARY KEY,

              `user_id` int(10) UNSIGNED NOT NULL,

              `contact_notes` varchar(225) DEFAULT NULL,

              `type` int(10) DEFAULT NULL,

              `record_status` enum('0','1') DEFAULT '0',

              `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP

            );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //    print_r($exception);

        }
    }
    public static function createcompany_rental_history($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_rental_history` (
                     `id` int(11) NOT NULL AUTO_INCREMENT,
                     `user_id` int(255) DEFAULT NULL,
                     `current_address` varchar(255) DEFAULT NULL,
                     `current_zip_code` varchar(255) DEFAULT NULL,
                     `current_city` varchar(255) DEFAULT NULL,
                     `current_state` varchar(255) DEFAULT NULL,
                     `current_landlord` varchar(255) DEFAULT NULL,
                     `current_landlord_no` varchar(255) DEFAULT NULL,
                     `current_salary` varchar(255) DEFAULT NULL,
                     `current_resided_from` varchar(255) DEFAULT NULL,
                     `current_resided_to` varchar(255) DEFAULT NULL,
                     `current_reason` varchar(255) DEFAULT NULL,
                     `previous_address` varchar(255) DEFAULT NULL,
                     `previous_zip_code` varchar(255) DEFAULT NULL,
                     `previous_city` varchar(255) DEFAULT NULL,
                     `previous_state` varchar(255) DEFAULT NULL,
                     `previous_landlord` varchar(255) DEFAULT NULL,
                     `previous_landlord_no` varchar(255) DEFAULT NULL,
                     `previous_salary` varchar(255) DEFAULT NULL,
                     `previous_resided_from` varchar(255) DEFAULT NULL,
                     `previous_resided_to` varchar(255) DEFAULT NULL,
                     `previous_reason` varchar(255) DEFAULT NULL,
                     `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                     `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                     `deleted_at` timestamp NULL DEFAULT NULL,
                    PRIMARY KEY (`id`)
                    );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //   _r($exception);

        }
    }
    public static function createcompany_rental_applications($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `company_rental_applications` (
                     `id` int(11) NOT NULL AUTO_INCREMENT,
                     `user_id` int(255) DEFAULT NULL,
                     `email2` varchar(255) DEFAULT NULL,
                     `email3` varchar(255) DEFAULT NULL,
                     `exp_move_in` date DEFAULT NULL,
                     `exp_move_out` date DEFAULT NULL,
                     `lease_term` int(255) DEFAULT NULL,
                     `lease_tenure` varchar(255) DEFAULT NULL,
                     `prop_id` int(255) DEFAULT NULL,
                     `build_id` int(255) DEFAULT NULL,
                     `unit_id` int(255) DEFAULT NULL,
                     `address1` varchar(255) DEFAULT NULL,
                     `address2` varchar(255) DEFAULT NULL,
                     `address3` varchar(255) DEFAULT NULL,
                     `address4` varchar(255) DEFAULT NULL,
                     `zip_code` int(255) DEFAULT NULL,
                     `city` varchar(255) DEFAULT NULL,
                     `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                     `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
                     `deleted_at` timestamp NULL DEFAULT NULL,
                     `state` varchar(255) DEFAULT NULL,
                     `market_rent` int(255) DEFAULT NULL,
                     `base_rent` int(255) DEFAULT NULL,
                     `sec_deposite` int(255) DEFAULT NULL,
                     `status` int(10) DEFAULT NULL,
                     PRIMARY KEY (`id`)
                    );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            /*print_r($exception);*/
        }
    }
    public static function createcompany_employment_history($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `company_employment_history` (
                     `id` int(11) NOT NULL AUTO_INCREMENT,
                     `user_id` int(255) NOT NULL,
                     `current_employer` varchar(255) DEFAULT NULL,
                     `current_employee_address` varchar(255) DEFAULT NULL,
                     `current_employer_zip` varchar(255) DEFAULT NULL,
                     `current_employee_city` varchar(255) DEFAULT NULL,
                     `current_employee_state` varchar(255) DEFAULT NULL,
                     `current_employee_position` varchar(255) DEFAULT NULL,
                     `current_employee_salary` varchar(255) DEFAULT NULL,
                     `current_employee_from_date` date DEFAULT NULL,
                     `current_employee_to_date` date DEFAULT NULL,
                     `current_employee_no` varchar(255) DEFAULT NULL,
                     `previous_employer` varchar(255) DEFAULT NULL,
                     `previous_employee_address` varchar(255) DEFAULT NULL,
                     `previous_employer_zip` varchar(255) DEFAULT NULL,
                     `previous_employee_city` varchar(255) DEFAULT NULL,
                     `previous_employee_state` varchar(255) DEFAULT NULL,
                     `previous_employee_position` varchar(255) DEFAULT NULL,
                     `previous_employee_salary` varchar(255) DEFAULT NULL,
                     `previous_employee_from_date` date DEFAULT NULL,
                     `previous_employee_to_date` date DEFAULT NULL,
                     `previous_employee_no` varchar(255) DEFAULT NULL,
                     `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                     `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                     `deleted_at` datetime DEFAULT NULL,
                     PRIMARY KEY (`id`)
                    ) ;";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            /*print_r($exception);*/
        }
    }
    public static function createcompany_annual_income($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `company_annual_income` (
                     `id` int(11) NOT NULL AUTO_INCREMENT,
                     `user_id` int(255) NOT NULL,
                     `amount` int(255) DEFAULT NULL,
                     `source_of_income` varchar(255) DEFAULT NULL,
                     `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                     `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                     `deleted_at` timestamp NULL DEFAULT NULL,
                     PRIMARY KEY (`id`)
                    );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            /*print_r($exception);*/
        }
    }
    public static function createVendorComplaints($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `vendor_complaints` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `vendor_id` int(11) DEFAULT NULL,

                      `complaint_id` varchar(50) DEFAULT NULL,

                      `complaint` enum('1','2') DEFAULT NULL COMMENT '1=by vendor,2=about vendor',

                      `complaint_date` datetime DEFAULT NULL,

                      `complaint_type_id` int(11) DEFAULT NULL,

                      `other_note` text,

                      `complaint_note` text,

                      `created_at` timestamp NULL DEFAULT NULL,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            /*print_r($exception);*/
        }
    }
    public static function alterUserTableCompanyName($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `users` LIKE 'company_name_as_tax_payer'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE users ADD company_name_as_tax_payer enum('0','1') DEFAULT '0' AFTER tax_payer_name";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            /*print_r($exception);*/
        }
    }
    public static function alterEmergencyDetailsTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `emergency_details` LIKE 'other_relation'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE emergency_details ADD other_relation VARCHAR(255) DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            //echo '<pre>';print_r($exception); die;

        }
    }
    public static function alterVendorAdditionalTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `vendor_additional_detail` LIKE 'use_company_name'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE vendor_additional_detail ADD use_company_name enum('0','1') DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            /*print_r($exception);*/
        }
    }
    public static function alterOwnerTableOtherRelation($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM owner_details LIKE 'emergency_other_relation'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE owner_details ADD emergency_other_relation  varchar(255) DEFAULT NULL AFTER emergency_relation";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function alterFlagTableForOwner($connection) {
        try {
            //            $query = "ALTER TABLE `flags` CHANGE `object_type` `object_type`
            //
            //                                    enum('property','unit','building','tenant','employee','contact','owner') DEFAULT NULL";
            //
            //
            //
            //            $connection->prepare($query)->execute();

        }
        catch(Exception $exception) {
            //  print_r($exception); //die;

        }
    }
    public static function alterUserTableForGender($connection) {
        try {
            $query = "ALTER TABLE `users` CHANGE `gender` `gender` ENUM('0','1','2','3','4') DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception); //die;

        }
    }
    public static function alterComplaintsTableForOwner($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `complaints` LIKE 'complaint_by_about'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE complaints ADD complaint_by_about varchar(50) DEFAULT NULL AFTER module_type";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            //   print_r($exception);

        }
    }
    public static function alterTenantPhone($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `tenant_phone` LIKE 'work_phone_extension'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE tenant_phone ADD work_phone_extension varchar(50) DEFAULT NULL AFTER phone_type";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function alterOwnerDetailTableForPortal($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `owner_details` LIKE 'billing_info_as_contact_info'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE owner_details ADD billing_info_as_contact_info varchar(50) DEFAULT NULL AFTER if_entity_name_display";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function createForwardAddress($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `forwardadress_moveout` (
                       `id` int(11) NOT NULL AUTO_INCREMENT,
                        `frwdAddress_FName` varchar(255) DEFAULT NULL,
                        `frwdAddress_LName` varchar(255) DEFAULT NULL,
                        `frwdAddress_Address` varchar(255) DEFAULT NULL,
                        `frwdAddress_Zip` varchar(255) DEFAULT NULL,
                        `frwdAddress_Country` varchar(255) DEFAULT NULL,
                        `frwdAddress_State` varchar(255) DEFAULT NULL,
                        `frwdAddress_City` varchar(255) DEFAULT NULL,
                        `frwdAddress_Phone` varchar(255) DEFAULT NULL,
                        `email` varchar(255) DEFAULT NULL,
                        `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        `user_id` int(11) DEFAULT NULL,
                        `deleted_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
                        PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function createMoveoutTenant($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `moveouttenant` (
                       `id` int(11) NOT NULL AUTO_INCREMENT,
                        `noticeDate` datetime DEFAULT NULL,
                        `scheduledMoveOutDate` datetime DEFAULT NULL,
                        `reason_id` int(11) DEFAULT NULL,
                        `user_id` int(11) DEFAULT NULL,
                        `actualMoveOutDate` datetime DEFAULT NULL,
                        `unitAvailableDate` datetime DEFAULT NULL,
                        `noOfKeysSigned_movein` int(11) DEFAULT NULL,
                        `noOfKeysSigned_moveout` int(11) DEFAULT NULL,
                        `status` enum('0','1','2') DEFAULT NULL,
                        `record_status` enum('0','1') DEFAULT NULL,
                        `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        `deleted_at` timestamp NULL DEFAULT NULL,
                        PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function createOwnerBillingInfo($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `owner_billing_info` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `user_id` int(11) NOT NULL,
                      `billing_info_as_contact_info` int(11) DEFAULT NULL,
                      `billing_address1` varchar(250) DEFAULT NULL,
                      `billing_address2` varchar(250) DEFAULT NULL,
                      `billing_address3` varchar(250) DEFAULT NULL,
                      `billing_address4` varchar(250) DEFAULT NULL,
                      `billing_zipcode` varchar(250) DEFAULT NULL,
                      `billing_city` varchar(250) DEFAULT NULL,
                      `billing_state` varchar(250) DEFAULT NULL,
                      `billing_email` varchar(250) DEFAULT NULL,

                      `created_at` timestamp NULL DEFAULT NULL,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,

                      PRIMARY KEY (`id`)

                    );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function createLeaseGuestCard($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `lease_guest_card` (
                       id int(11) NOT NULL AUTO_INCREMENT,
                       user_id int(10) unsigned NOT NULL,
                       unit_id int(11) DEFAULT NULL,
                       guest_card_image blob,
                       guestcard_contact varchar(255) DEFAULT NULL,
                       phone_number_note text,
                       email1 varchar(255) DEFAULT NULL,
                       email2 varchar(255) DEFAULT NULL,
                       email3 varchar(255) DEFAULT NULL,
                       referral_source int(11) DEFAULT NULL,
                       expected_move_in date DEFAULT NULL,
                       tenant_license_state varchar(255) DEFAULT NULL,
                       tenant_license_number varchar(255) DEFAULT NULL,
                       pet_friendly enum('0','1') DEFAULT NULL,
                       parking enum('0','1') DEFAULT NULL,
                       status enum('0','1','2','3','4','5','6') DEFAULT NULL,
                       record_status enum('0','1') NOT NULL,
                       custom_field text,
                       deleted_at timestamp NULL DEFAULT NULL,
                       created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                       updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                       PRIMARY KEY (id)
                       );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function createESignHistory($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `e_sign_history` (
                       id int(11) NOT NULL AUTO_INCREMENT,
                       user_id int(10) unsigned NOT NULL,
                       username varchar(255) DEFAULT NULL,
                       email varchar(255) DEFAULT NULL,
                       done_date date DEFAULT NULL,
                       document_name  text DEFAULT NULL,
                       status enum('0','1','2','3','4') DEFAULT '0',
                       record_status enum('0','1') DEFAULT '1',
                       created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                       updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                       PRIMARY KEY (id)
                       );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function alterUsersTableForPortal($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `users` LIKE 'portal_password_key'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE users ADD portal_password_key varchar(50) DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function alterUsersTableForPortalStatus($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `users` LIKE 'portal_status'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE users ADD portal_status enum('0','1','2') DEFAULT '0'";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function alterUsersTableForUserType($connection) {
        try {
            /*
                        $ifExists = $connection->query("SHOW COLUMNS FROM `users` LIKE 'user_type'")->fetchAll();

                        if(!empty($ifExists)){

                            return ;

                        }*/
            $query = "ALTER TABLE `users` CHANGE `user_type` `user_type` ENUM('0','1','2','3','4','5','6','7','8','9','10','11') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function altertenant_additional_details($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `tenant_additional_details` LIKE 'ssn'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `tenant_additional_details` ADD `ssn` INT(255) NULL AFTER `relationship`";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function altertenant_guarantor($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `tenant_guarantor` LIKE 'gender'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `tenant_guarantor` ADD `gender` VARCHAR(255) NULL AFTER `last_name`";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function maintenanceItem($connection) {
        try {
            $stm = $connection->prepare("DROP TABLE IF EXISTS maintenance_item");
            $stm->execute();
            $table = "CREATE TABLE IF NOT EXISTS `maintenance_item` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `item_name` varchar(255) NOT NULL,
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`)
            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function maintenanceCategory($connection) {
        try {
            $stm = $connection->prepare("DROP TABLE IF EXISTS maintenance_category");
            $stm->execute();
            $table = "CREATE TABLE IF NOT EXISTS `maintenance_category` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `category_name` varchar(255) NOT NULL,
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`)
            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function maintenanceColor($connection) {
        try {
            $stm = $connection->prepare("DROP TABLE IF EXISTS maintenance_color");
            $stm->execute();
            $table = "CREATE TABLE IF NOT EXISTS `maintenance_color` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `color_name` varchar(255) NOT NULL,
                  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (`id`)
                )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function maintenanceLostFound($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `maintenance_lost_found` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `item_name` varchar(255) NOT NULL,
              `item_number` varchar(255) NOT NULL,
              `category_id` int(11) NOT NULL,
              `description` text NULL DEFAULT NULL,
              `brand_name` varchar(255) NULL DEFAULT NULL,
              `user_id` int(11) NOT NULL,
              `color_id` int(11) NOT NULL,
              `age_item_id` int(11) NOT NULL,
              `serial_number` varchar(255) NOT NULL,
              `lost_location` varchar(255) NOT NULL,
              `lost_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              `other_details` text NOT NULL,
               `property_id` int DEFAULT NULL,
               `type`  enum('L','F') DEFAULT 'L',
               `status`  enum('1','0') DEFAULT '1',
               `expiration_date` timestamp NULL DEFAULT NULL,
               `match_date` timestamp NULL DEFAULT NULL,
               `matched_status` enum('True','False') DEFAULT 'False',
               `matched_returned` enum('True','False') DEFAULT 'False',
               `return_date` timestamp NULL DEFAULT NULL,
               `release_date` timestamp NULL DEFAULT NULL,
               `release_method` varchar(255) NULL DEFAULT NULL,
               `deleted_at` timestamp NULL DEFAULT NULL,
                `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`)
            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function maintenancePurchaseOrder($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS purchaseOrder (
            `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `po_number`  VARCHAR(255) DEFAULT NULL,
            `created_by` INT(11) DEFAULT NULL  COMMENT 'Property Owner',
            `currentdate` timestamp NULL DEFAULT NULL,
            `required_by` timestamp NULL DEFAULT NULL,
            `property`  INT(11) DEFAULT NULL,
            `building`  INT(11) DEFAULT NULL,
            `unit`  INT(11) DEFAULT NULL,
            `work_order` INT(11) DEFAULT NULL,
            `owner_approve` enum('0','1') DEFAULT '0' COMMENT '0=>none,1=>active',
            `tenant_approve` enum('0','1') DEFAULT '0' COMMENT '0=>none,1=>active',
            `tenants`  INT(11) DEFAULT NULL,
            `owners` INT(11) DEFAULT NULL,
            `invoice_number` VARCHAR(255) DEFAULT NULL,
            `vendor`  VARCHAR(255) DEFAULT NULL,
            `vendorid` INT(11) DEFAULT NULL,
            `address` text DEFAULT NULL,
            `vendor_instruction` text DEFAULT NULL,
            `status`  enum('1','0','2','3','4') DEFAULT '1' COMMENT 'reocrd status 0=>Rejected,1=>Created,2=>Approved,3=>Completed',
            `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
            `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `deleted_at` timestamp NULL DEFAULT NULL,
            PRIMARY KEY (`id`)
        )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function maintenancePurchaseOrderDetails($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `purchaseOrderDetails` (
                `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `purchase_id` INT(11) DEFAULT NULL,
                `qty_number` INT(11) DEFAULT NULL,
                `gl_account` INT(11) DEFAULT NULL,
                `description` text DEFAULT NULL,
                `item_amount` INT(11) DEFAULT NULL,
                `item_total` INT(11) DEFAULT NULL,
                `total` INT(11) DEFAULT NULL,
                `status`  enum('1','0') DEFAULT '1' COMMENT 'reocrd status 0=>inactive,1=>active',
                `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `deleted_at` timestamp NULL DEFAULT NULL,
                PRIMARY KEY (`id`)
            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function createTagsLettersNotices($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `tags_letter_notices` (
                       `id` int(11) NOT NULL AUTO_INCREMENT,
                      `tag_name` varchar(255) DEFAULT NULL,
                      `letter_type` varchar(255) NOT NULL COMMENT '1=lease_doc,2=tenant_doc,3=landlord/owner_doc,4=e-sign_doc',
                      `template_key` varchar(255) NOT NULL,
                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                      `deleted_at` timestamp NULL DEFAULT NULL,
                       PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function createLettersNoticesTemplate($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `letters_notices_template` (
                       `id` int(11) NOT NULL AUTO_INCREMENT,
                      `letter_name` varchar(255) DEFAULT NULL,
                      `letter_type` varchar(255) NOT NULL COMMENT '1=lease_doc,2=tenant_doc,3=landlord/owner_doc,4=e-sign_doc',
                      `original_template_html` longtext,
                      `template_html` longtext,
                      `description` text,
                      `is_editable` enum('0','1') DEFAULT '0',
                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                      `deleted_at` timestamp NULL DEFAULT NULL,
                       PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function alterTemplateTable($connection) {
        try {
            $table = "ALTER TABLE `letters_notices_template` CHANGE `is_editable` `is_editable` ENUM('0','1','2') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0' ";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createAnnouncements($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `announcements` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `announcement_for` varchar(255) NOT NULL,
                          `property` varchar(255) DEFAULT NULL,
                          `building` varchar(255) DEFAULT NULL,
                          `unit` varchar(255) DEFAULT NULL,
                          `user_name` varchar(255) NOT NULL,
                          `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                          `start_date` date NOT NULL,
                          `start_time` time NOT NULL,
                          `end_date` date NOT NULL,
                          `end_time` time NOT NULL,
                          `description` text COLLATE utf8mb4_unicode_ci,
                          `status` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
                          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                          `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                          `deleted_at` timestamp NULL DEFAULT NULL,
                           PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function createAnnouncementFileLibrary($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `announcement_file_library` (
                  `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                  `announcement_id` int(10) UNSIGNED NOT NULL,
                  `type` enum('N','T') DEFAULT 'N',
                  `filename` varchar(255) DEFAULT NULL, 
                  `file_type` enum('1','2','3','4') DEFAULT NULL,
                  `file_location` text DEFAULT NULL ,
                  `file_extension` varchar(255) DEFAULT NULL,
                  `deleted_at` timestamp NULL DEFAULT NULL,
                  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
                );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createCommunicationEmail($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `communication_email` 
              (
                  `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                  `email_to` text DEFAULT NULL,
                  `email_cc` text DEFAULT NULL,
                  `email_bcc` text DEFAULT NULL,
                  `email_from` varchar(255) DEFAULT NULL,
                  `email_subject` varchar(255) NOT NULL,
                  `email_message` longblob DEFAULT NULL,
                  `user_type` enum('0','1','2','3','4','5','6','8','9','10') NOT NULL,
                  `user_id` int(11) NOT NULL, `email_parent_id` int(11) DEFAULT NULL, 
                  `status` enum('0','1','2','3','4') NOT NULL DEFAULT '2', 
                  `deleted_at` timestamp NULL DEFAULT NULL,
                  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
                );";
            $connection->prepare($table)->execute();
            // print_r($connection);

        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
            die;
        }
    }
    public static function createMaintenanceFiles($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `maintenance_files` (
                  `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                  `type` enum('P','LF','I','IT') DEFAULT 'P',
                  `record_id` int(11) DEFAULT NULL,
                  `filename` varchar(255) DEFAULT NULL, 
                  `file_type` enum('1','2','3','4') DEFAULT NULL,
                  `file_location` text DEFAULT NULL ,
                  `file_extension` varchar(255) DEFAULT NULL,
                  `deleted_at` timestamp NULL DEFAULT NULL,
                  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
                );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }
    public static function createAnnouncementUserStatus($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `announcements_user_status` (
                  `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                  `user_id` int(11) NOT NULL,
                  `announcement_id` int(11) NOT NULL,
                  `status` enum('0','1') NOT NULL, 
                  `deleted_at` timestamp NULL DEFAULT NULL,
                  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
                )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function alterTenantImages($connection) {
        try {
            $tablee = "ALTER TABLE `tenant_details` CHANGE `tenant_image` `tenant_image` LONGBLOB NULL DEFAULT NULL";
            $connection->prepare($tablee)->execute();
            $table1 = "ALTER TABLE `tenant_pet` CHANGE `image1` `image1` LONGBLOB NULL DEFAULT NULL, CHANGE `image2` `image2` LONGBLOB NULL DEFAULT NULL, CHANGE `image3` `image3` LONGBLOB NULL DEFAULT NULL;";
            $connection->prepare($table1)->execute();
            $table2 = "ALTER TABLE `tenant_vehicles` CHANGE `photo1` `photo1` LONGBLOB NULL DEFAULT NULL, CHANGE `photo2` `photo2` LONGBLOB NULL DEFAULT NULL, CHANGE `photo3` `photo3` LONGBLOB NULL DEFAULT NULL;";
            $connection->prepare($table2)->execute();
            $table3 = "ALTER TABLE `tenant_service_animal` CHANGE `image1` `image1` LONGBLOB NULL DEFAULT NULL, CHANGE `image2` `image2` LONGBLOB NULL DEFAULT NULL, CHANGE `image3` `image3` LONGBLOB NULL DEFAULT NULL;";
            $connection->prepare($table3)->execute();
        }
        catch(Exception $exception) {
        }
    }
    public static function taskReminders($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `task_reminders` 
              (
                  `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                  `title` varchar(255) NOT NULL,
                  `property` varchar(255) DEFAULT NULL,
                  `building` varchar(255) DEFAULT NULL,
                  `unit` varchar(255) DEFAULT NULL,
                  `unit_ids` text DEFAULT NULL,
                  `details` text  DEFAULT NULL,
                  `assigned_to` int(11) DEFAULT NULL,
                  `due_date` date NOT NULL,
                  `user_id` int(11) NOT NULL,
                  `recurring_task` int(11) DEFAULT NULL,
                  `frequency` int(11) DEFAULT NULL,
                  `next_date` date DEFAULT NULL,
                  `status` enum('0','1','2','3','4','5') NOT NULL, 
                  `deleted_at` timestamp NULL DEFAULT NULL,
                  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
                );";
            $connection->prepare($table)->execute();
            // print_r($connection);

        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function alterTaskReminders($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `task_reminders` LIKE 'unit_ids'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE task_reminders ADD unit_ids text NULL  DEFAULT NULL ";
            $connection->prepare($query)->execute();
            $table1 = "ALTER TABLE `task_reminders` CHANGE `details` `details` text NULL DEFAULT NULL, CHANGE `assigned_to` `assigned_to` int(11) NULL DEFAULT NULL";
            $connection->prepare($table1)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);

        }
    }
    public static function alterAnnouncementsTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `announcements` LIKE 'is_file_library'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE announcements ADD is_file_library enum('0','1') DEFAULT '0'";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function runMigrationsAll() {
        $db = new DBConnection();
        $dbDetails = $db->conn->query("SELECT * FROM users where user_type!='0'")->fetchAll();
        if (isset($dbDetails)) {
            foreach ($dbDetails as $key => $udata) {
                $connection = DBConnection::dynamicDbConnection($udata['host'], $udata['database_name'], $udata['db_username'], $udata['db_password']);
                MigrationCusers::runMigrations($connection);
                include_once (SUPERADMIN_DIRECTORY_URL . "/helper/SeederCusers.php");
                SeederCusers::runSeeders($connection);
            }
        }
    }
    public static function alterPropertyIdToLostFound($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `maintenance_lost_found` LIKE 'property_id'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE maintenance_lost_found ADD property_id int(11) DEFAULT NULL AFTER item_number";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `maintenance_lost_found` LIKE 'expiration_date'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE maintenance_lost_found ADD expiration_date timestamp() DEFAULT NULL AFTER lost_date";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    //    work order tables
    public static function CreateWorkorderCategory($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_workorder_category` (
                  `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                  `user_id` int(11) NOT NULL,
                  `category` VARCHAR(255) DEFAULT NULL,
                  `description` VARCHAR(255) DEFAULT NULL,
                  `status` enum('0','1') NOT NULL, 
                  `deleted_at` timestamp NULL DEFAULT NULL,
                  `created_at` timestamp NULL DEFAULT NULL,
                  `updated_at` timestamp NULL DEFAULT NULL,
                  `is_editable` enum('0','1') NULL DEFAULT NULL
                )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function CreateWorkorderRequest($connection) {
        try {
            $sql = $connection->prepare("DROP TABLE IF EXISTS company_workorder_requestedby ");
            $sql->execute();
            $table = "CREATE TABLE IF NOT EXISTS `company_workorder_requestedby` (
                      `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                      `user_id` bigint(20) DEFAULT NULL,
                      `work_order_request` varchar(255) DEFAULT NULL,
                      `is_editable` enum('0','1') DEFAULT NULL,
                      `status` enum('0','1') DEFAULT NULL,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function CreateWorkorderSource($connection) {
        try {
            $sql = $connection->prepare("DROP TABLE IF EXISTS company_workorder_source ");
            $sql->execute();
            $table = "CREATE TABLE IF NOT EXISTS `company_workorder_source` (
                      `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                      `work_order_source` varchar(255) DEFAULT NULL,
                      `is_editable` enum('0','1') DEFAULT NULL,
                      `status` enum('0','1') DEFAULT NULL,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL,
                      `user_id` bigint(20) DEFAULT NULL
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function CreateWorkorderStatus($connection) {
        try {
            $sql = $connection->prepare("DROP TABLE IF EXISTS company_workorder_status ");
            $sql->execute();
            $table = "CREATE TABLE IF NOT EXISTS `company_workorder_status` (
                      `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                      `user_id` bigint(20) DEFAULT NULL,
                      `work_order_status` varchar(255) DEFAULT NULL,
                      `is_editable` enum('0','1') DEFAULT NULL,
                      `status` enum('0','1') DEFAULT NULL,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function CreateWorkorderType($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_workorder_type` (
                      `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                      `user_id` int(11) DEFAULT NULL,
                      `workorder` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                      `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                      `is_default` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                      `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL,
                      `is_editable` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0'
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function CreateWorkorderPriorityType($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `company_priority_type` (
                      `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                      `user_id` int(11) DEFAULT NULL,
                      `priority` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                      `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                      `is_default` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                      `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL,
                      `is_editable` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0'
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function CreateWorkorder($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `work_order` (
                      `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                      `work_order_type` enum('0','1') DEFAULT NULL,
                      `work_order_number` varchar(255) DEFAULT NULL,
                      `created_on` datetime DEFAULT NULL,
                      `work_order_cat` int(11) DEFAULT NULL,
                      `priority_id` int(11) DEFAULT NULL,
                      `portfolio_id` int(11) DEFAULT NULL,
                      `property_id` int(11) DEFAULT NULL,
                      `building_id` int(11) DEFAULT NULL,
                      `unit_id` int(11) DEFAULT NULL,
                      `pet_id` int(11) DEFAULT NULL,
                      `status_id` int(11) DEFAULT NULL,
                      `source_id` int(11) DEFAULT NULL,
                      `request_id` int(11) DEFAULT NULL,
                      `estimated_cost` varchar(255) DEFAULT NULL,
                      `scheduled_on` date DEFAULT NULL,
                      `completed_on` date DEFAULT NULL,
                      `authority` varchar(255) DEFAULT NULL,
                      `recipient` varchar(255) DEFAULT NULL,
                      `work_order_description` varchar(255) DEFAULT NULL,
                      `required_materials` varchar(255) DEFAULT NULL,
                      `approved_by_owner` varchar(255) DEFAULT NULL,
                      `publish_to_tenant` varchar(255) DEFAULT NULL,
                      `send_alert` varchar(255) DEFAULT NULL,
                      `publish_to_owner` varchar(255) DEFAULT NULL,
                      `publish_to_vendor` varchar(255) DEFAULT NULL,
                      `tenant_id` int(11) DEFAULT NULL,
                      `vendor_id` int(11) DEFAULT NULL,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL,
                      `rec_month` varchar(255) DEFAULT NULL,
                      `custom_fields` text DEFAULT NULL
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function CreateWorkorderFileLibrary($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `work_file_library` (
                      `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                      `work_order_id` int(11) DEFAULT NULL,
                      `name` varchar(255) DEFAULT NULL,
                      `extension` varchar(255) DEFAULT NULL,
                      `path` varchar(255) DEFAULT NULL,
                      `status` varchar(255) DEFAULT NULL,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function CreateIntouchType($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `in_touch_type` (
                      `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                      `user_id` int(11) DEFAULT NULL,
                      `in_touch_type` varchar(255) DEFAULT NULL,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL,
                      `status` int(11) NOT NULL
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function createConversationTable($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `conversation` (
              `id` int(11) AUTO_INCREMENT PRIMARY KEY,
              `user_id` int(11)  NOT NULL,
              `user_type` enum('1','2','3') NOT NULL COMMENT '1=Tenant,2=Owner,3=Vendor',
              `problem_category` varchar(255)  NOT NULL,
              `description` varchar(255)  NOT NULL,
              `selected_user_id` int(11)  NOT NULL,
              `parent_id` int(11) DEFAULT NULL,   
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              `deleted_at` timestamp NULL DEFAULT NULL
            );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function alterAnnouncementTable($connection) {
        try {
            $table = "ALTER TABLE `announcements` MODIFY `user_name` longtext DEFAULT NULL";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function CreateCallType($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `call_type` (
                      `id` int(11) AUTO_INCREMENT,
                      `user_id` int(11) DEFAULT NULL,
                      `call_type` varchar(255) DEFAULT NULL,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL,
                       PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function CreateCallPhoneLogs($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `phone_call_logs` (
                      `id` int(11) AUTO_INCREMENT,
                      `user_id` int(11) DEFAULT NULL,
                      `caller_name` varchar(255) DEFAULT NULL,
                      `call_for` varchar(255) DEFAULT NULL,
                      `phone_number` varchar(255) DEFAULT NULL,
                      `incoming_outgoing` enum('1','2') DEFAULT NULL,
                      `due_date` date NOT NULL,
                      `time` time NOT NULL,
                      `call_type_id` int(11) DEFAULT NULL,
                      `call_purpose` text ,
                      `action_taken` text ,
                      `call_length` text ,
                      `follow_up_needed` enum('1','2') DEFAULT NULL,
                      `notes` text ,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL,
                      `status` int(11) NOT NULL,
                       PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function CreateMaintenanceLostNote($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `lost_found_notes` (
                      `id` int(11) AUTO_INCREMENT,
                      `item_id` int(11) DEFAULT NULL,
                      `notes` text,
                      `status` enum('0','1') DEFAULT NULL,
                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                       PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function createProblemCategoryTable($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `conversation_problem_category` (
              `id` int(11) AUTO_INCREMENT PRIMARY KEY,
              `problem_category_name` varchar(255)  NOT NULL,
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              `deleted_at` timestamp NULL DEFAULT NULL
            );";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function Createmielagelog($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `mielage_log` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                    `user_id` int(11) DEFAULT NULL,
                   `driver` varchar(255) DEFAULT NULL,
                   `vehicle_type` varchar(255) DEFAULT NULL,
                   `company_name` varchar(255) DEFAULT NULL,
                   `branch` varchar(255) DEFAULT NULL,
                  `date` date DEFAULT NULL,
                  `emp_name` varchar(255) DEFAULT NULL,
                   `start_location` varchar(255) DEFAULT NULL,
                  `end_location` varchar(255) DEFAULT NULL,
                 `initial_reading` int(11) DEFAULT NULL,
                  `final_reading` int(11) DEFAULT NULL,
                  `mileage` int(11) DEFAULT NULL,
                   `vehicle_name` varchar(255) DEFAULT NULL,
                   `vehicle_id` varchar(255) DEFAULT NULL,
                   `vehicle_make` varchar(255) DEFAULT NULL,
                    `model` varchar(255) DEFAULT NULL,
                    `vin_no` varchar(255) DEFAULT NULL,
                   `registration` varchar(255) DEFAULT NULL,
                 `licence_plate` varchar(255) DEFAULT NULL,
                  `color` varchar(255) DEFAULT NULL,
                `year_of_vehicle` varchar(255) DEFAULT NULL,
                  `date_purchased` date DEFAULT NULL,
                   `amount` int(11) DEFAULT NULL,
                  `notes` varchar(255) DEFAULT NULL,
                   `type` enum('C','E') DEFAULT 'E' COMMENT 'E=employee,C=company',
                `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `deleted_at` timestamp NULL DEFAULT NULL,
               `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
            die;
        }
    }
    /**
     * function to create chat history table
     * @param $connection
     */
    public static function CreateChatHistory($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `chat_history` (
                      `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                      `user_id` int(20) NOT NULL,
                      `sender_id` int(20) NOT NULL,
                      `recevier_id` int(20) NOT NULL,
                      `conversation_id` varchar(50) DEFAULT NULL,
                      `message` text NOT NULL,
                      `type` enum('1','2') DEFAULT NULL COMMENT '1=private,2=room',
                      `date` varchar(50) DEFAULT NULL,
                      `time` varchar(50) DEFAULT NULL,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
            die;
        }
    }
    /**
     * function to create reasons table
     * @param $connection
     */
    public static function CreateReasonsTable($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `reasons` (
                          `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                          `reason` varchar(200) NOT NULL,
                          `is_editable` enum('0','1') NOT NULL DEFAULT '0',
                          `created_at` timestamp NULL DEFAULT NULL,
                          `updated_at` timestamp NULL DEFAULT NULL
                        )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function CreateCommunicationEmailAttachments($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `communication_email_attachments` (
                  `id` int(11) AUTO_INCREMENT,
                  `email_id` int(10) UNSIGNED NOT NULL,
                  `filename` varchar(255) DEFAULT NULL,
                  `file_type` enum('1','2','3','4') DEFAULT NULL,
                  `file_location` text,
                  `file_extension` varchar(255) DEFAULT NULL,
                  `type` enum('N','T') DEFAULT 'N',
                  `deleted_at` timestamp NULL DEFAULT NULL,
                  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                   PRIMARY KEY (`id`)
                )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function alterUserTableForCreatedByColumn($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `users` LIKE 'created_by'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE users ADD created_by int(11) DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);

        }
    }
    public static function alterTableCommunicationEmail($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `communication_email` LIKE 'type'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE communication_email ADD `type` enum('E','T','G') DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            echo '<pre>';
            print_r($exception);
            echo '</pre>';
            die('d');
        }
    }
    public static function alterTableCommunicationEmailGtype($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `communication_email` LIKE 'gtype'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            //
            $query1 = "ALTER TABLE communication_email ADD `gtype` enum('E','T') DEFAULT NULL";
            $connection->prepare($query1)->execute();
            $query2 = "ALTER TABLE communication_email ADD `selected_user_type` enum('All','0','1','2','3','4','5','6','7','8') DEFAULT NULL";
            $connection->prepare($query2)->execute();
            $query3 = "ALTER TABLE communication_email ADD `user_name` varchar(500)DEFAULT NULL";
            $connection->prepare($query3)->execute();
        }
        catch(Exception $exception) {
            echo '<pre>';
            print_r($exception);
            echo '</pre>';
            die('d');
        }
    }
    public static function maintenanceInventoryCategory($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `maintenance_inventory_category` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `category` varchar(255) DEFAULT NULL,
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`)
        )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function maintenanceInventorySubCategory($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `maintenance_inventory_subcategory` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `category_id` int(11) DEFAULT NULL,
          `subcategory` varchar(255) DEFAULT NULL,
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`)
        )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function maintenanceInventoryBrand($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `maintenance_inventory_brand` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `category_id` int(11) DEFAULT NULL,
          `subcategory_id` int(11) DEFAULT NULL,
          `brand` varchar(255) DEFAULT NULL,
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`)
        )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function maintenanceInventorySupplier($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `maintenance_inventory_supplier` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `category_id` int(11) DEFAULT NULL,
          `subcategory_id` int(11) DEFAULT NULL,
          `brand_id` int(11) DEFAULT NULL,
          `supplier` varchar(255) DEFAULT NULL,
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`)
        )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function maintenanceInventoryQuantityChangeReason($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `maintenance_inventory_quantity_change_reason` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `reason` varchar(255) DEFAULT NULL,
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`)
        )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function maintenanceInventoryVolume($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `maintenance_inventory_volume` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `volume` varchar(255) DEFAULT NULL,
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`)
        )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function maintenanceInventorySubLocation($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `maintenance_inventory_sublocation` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `sublocation` varchar(255) DEFAULT NULL,
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`)
        )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function maintenanceInventory($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `maintenance_inventory` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
           `image` longblob,
           `category_id` int(11) DEFAULT NULL,
           `sub_category_id` int(11) DEFAULT NULL,
           `brand_id` int(11) DEFAULT NULL,
           `supplier_id` int(11) DEFAULT NULL,
           `description` text,
           `item_purchased` varchar(255) DEFAULT NULL,
           `size` varchar(255) DEFAULT NULL,
           `volume_id` int(11) DEFAULT NULL,
           `cost_per_item` varchar(255) DEFAULT NULL,
           `purchase_cost` varchar(255) DEFAULT NULL,
           `purchase_date` varchar(255) DEFAULT NULL,
           `expected_deliver_date` varchar(255) DEFAULT NULL,
           `stock_reorder_level` varchar(255) DEFAULT NULL,
           `property_id` int(11) DEFAULT NULL,
           `building_id` int(11) DEFAULT NULL,
           `sub_location_id` int(11) DEFAULT NULL,
           `custom_field` text,
           `deleted_at` timestamp NULL DEFAULT NULL,
           `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
           `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
         PRIMARY KEY (`id`)
       )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function altermaintenanceInventory($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `maintenance_inventory` LIKE 'user_id'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `maintenance_inventory` ADD `user_id` INT(255) NULL AFTER `id`";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            /*print_r($exception);*/
        }
    }
    public static function alterPurchaseOrderCustomField($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `purchaseOrder` LIKE 'custom_field'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `purchaseOrder` ADD `custom_field` TEXT NULL DEFAULT NULL AFTER `status`";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            /*print_r($exception);*/
        }
    }
    public static function createPackageTrackerCarrier($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `packageTrackerCarrier_table` (

                        `id` int(11) NOT NULL AUTO_INCREMENT,

                        `carrierName` varchar(255) NOT NULL,

                        `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                        `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

                        `deleted_at` timestamp NULL DEFAULT NULL,

                        PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //  print_r($exception);

        }
    }
    public static function createLocationStored($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `locationStored_table` (

                        `id` int(11) NOT NULL AUTO_INCREMENT,

                        `locationName` varchar(255) NOT NULL,

                        `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                        `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,

                        `deleted_at` timestamp NULL DEFAULT NULL,

                        PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //  print_r($exception);

        }
    }
    public static function createpackageTracker($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `packageTracker_table` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `packageOwnerName` varchar(255) DEFAULT NULL,
                          `pcktrackList` varchar(255) DEFAULT NULL,
                          `parcelNumber` varchar(255) DEFAULT NULL,
                          `quantityOfPackages` int(11) DEFAULT NULL,
                          `trackerDatePackage` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                          `ownerTypeEmail` varchar(255) DEFAULT NULL,
                          `carrierName_id` int(11) DEFAULT NULL,
                          `packageSize` varchar(255) DEFAULT NULL,
                          `country_code` varchar(255) DEFAULT NULL,
                          `locationName_id` int(11) DEFAULT NULL,
                          `pickUpDate` timestamp NULL DEFAULT NULL,
                          `pickUpTime` timestamp NULL DEFAULT NULL,
                          `ownerType_phone_number` text,
                          `PickedUpBy` varchar(255) DEFAULT NULL,
                          `receivedByPackage` varchar(255) DEFAULT NULL,
                          `notesPackages` text,
                          `IsPackageDelivered` varchar(255) DEFAULT NULL,
                          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                          `deleted_at` timestamp NULL DEFAULT NULL,
                        PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function alterPackageTracker_Table($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `packageTracker_table` LIKE 'notesPickedUp'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `packageTracker_table` ADD `notesPickedUp` text DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function alterAddPackageTracker_Table($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `packageTracker_table` LIKE 'othersData'")->fetchAll();
            if (!empty($ifExists)) {
                $query = "ALTER TABLE `packageTracker_table` MODIFY `packageOwnerName` int(11) DEFAULT NULL";
                $connection->prepare($query)->execute();
                $query = "ALTER TABLE `packageTracker_table` MODIFY `pcktrackList` int(11) DEFAULT NULL";
                $connection->prepare($query)->execute();
            } else {
                $query = "ALTER TABLE `packageTracker_table` ADD `othersData` varchar(255) DEFAULT NULL";
                $connection->prepare($query)->execute();
                $query = "ALTER TABLE `packageTracker_table` MODIFY `packageOwnerName` int(11) DEFAULT NULL";
                $connection->prepare($query)->execute();
                $query = "ALTER TABLE `packageTracker_table` MODIFY `pcktrackList` int(11) DEFAULT NULL";
                $connection->prepare($query)->execute();
            }
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function alterInventoryTableCustomField($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `maintenance_inventory` LIKE 'custom_field'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `maintenance_inventory` ADD `custom_field` TEXT NULL DEFAULT NULL AFTER `sub_location_id`";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            /*print_r($exception);*/
        }
    }
    public static function alterConversationTable($connection) {
        try {
            $table = "ALTER TABLE `conversation` MODIFY `problem_category` varchar(255) DEFAULT NULL";
            $connection->prepare($table)->execute();
            $table = "ALTER TABLE `conversation` MODIFY `description` varchar(255) DEFAULT NULL";
            $connection->prepare($table)->execute();
            $table = "ALTER TABLE `conversation` MODIFY `selected_user_id` int(11) DEFAULT NULL";
            $connection->prepare($table)->execute();
            $ifExists = $connection->query("SHOW COLUMNS FROM `conversation` LIKE 'comment_by'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `conversation` ADD `comment_by`  varchar(255) DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function createDailyVisitorLogTable($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `daily_visitor_log` (
                      `id` int(20) NOT NULL AUTO_INCREMENT,
                      `visitor` varchar(200) DEFAULT NULL,
                      `company` varchar(200) DEFAULT NULL,
                      `phone` varchar(50) DEFAULT NULL,
                      `date` date DEFAULT NULL,
                      `time_in` time DEFAULT NULL,
                      `time_out` time DEFAULT NULL,
                      `reason_for_visit` varchar(200) DEFAULT NULL,
                      `length_of_visit` float DEFAULT NULL,
                      `action_taken` varchar(200) DEFAULT NULL,
                      `follow_up` enum('0','1') DEFAULT NULL COMMENT '0=no,1=yes',
                      `note` text,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function createTimeSheetTable($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `time_sheet` (
                      `id` int(20) NOT NULL AUTO_INCREMENT,
                      `employee_id` int(20) DEFAULT NULL,
                      `position` int(20) DEFAULT NULL,
                      `email` varchar(250) DEFAULT NULL,
                      `mobile_number` varchar(50) DEFAULT NULL,
                      `last_login` datetime DEFAULT NULL,
                      `date` date DEFAULT NULL,
                      `time_in` time DEFAULT NULL,
                      `time_out_for_meal` time DEFAULT NULL,
                      `time_in_after_meal` time DEFAULT NULL,
                      `time_out` time DEFAULT NULL,
                      `total_hours` float DEFAULT NULL,
                      `note` text,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function createInTouch($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `in_touch_detail` (
                      `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                      `type` varchar(255) DEFAULT NULL,
                      `category` varchar(255) DEFAULT NULL,
                      `assigned_user` varchar(255) DEFAULT NULL,
                      `optional_due_date` datetime DEFAULT NULL,
                      `subscribed_user` varchar(255) DEFAULT NULL,
                      `assigned_for` varchar(255) DEFAULT NULL,
                      `subject` varchar(255) DEFAULT NULL,
                      `send_reminder` enum('0','1') DEFAULT NULL,
                      `remind_days` bigint(20) DEFAULT NULL,
                      `remind_due_hours` bigint(20) DEFAULT NULL,
                      `remind_overdue_hours` bigint(20) DEFAULT NULL,
                      `status` enum('0','1','2','3') DEFAULT NULL COMMENT '0 for pending,1 for completed,2 for cancel,3 for overdue',
                      `created_at` datetime DEFAULT NULL,
                      `updated_at` datetime DEFAULT NULL,
                      `deleted_at` datetime DEFAULT NULL,
                      `notes` varchar(255) DEFAULT NULL
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            // echo '<pre>'; print_r($exception);echo '</pre>';

        }
    }
    public static function createInTouchLogs($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `in_touch_logs` (
                      `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                      `username` varchar(255) DEFAULT NULL,
                      `action` varchar(255) DEFAULT NULL,
                      `action_time` datetime DEFAULT NULL,
                      `description` varchar(255) DEFAULT NULL,
                      `in_touch_id` int(11) DEFAULT NULL
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            // echo '<pre>'; print_r($exception);echo '</pre>';

        }
    }
    public static function createInTouchNotes($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `in_touch_notes` (
                      `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                      `in_touch_id` int(11) DEFAULT NULL,
                      `notes` varchar(255) DEFAULT NULL,
                      `created_at` datetime DEFAULT NULL
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            // echo '<pre>'; print_r($exception);echo '</pre>';

        }
    }
    public static function workordernotes($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `work_order_notes` (
                     `id` int(11) NOT NULL AUTO_INCREMENT,
                     `work_order_id` int(11) DEFAULT NULL,
                     `notes` varchar(255) DEFAULT NULL,
                     `created_at` timestamp NULL DEFAULT NULL,
                     `updated_at` timestamp NULL DEFAULT NULL,
                     PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function waitinglist($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS waiting_list (
                    id int(11) NOT NULL AUTO_INCREMENT,
                    user_id int(11) DEFAULT NULL,
                    first_name varchar(255) DEFAULT NULL,
                    middle_name varchar(255) DEFAULT NULL,
                    last_name varchar(255) DEFAULT NULL,
                    preferred_id varchar(250) DEFAULT NULL,
                    property_id int(11) DEFAULT NULL,
                    building_id int(11) DEFAULT NULL,
                    unit_id int(11) DEFAULT NULL,
                    pet_id varchar(150) DEFAULT NULL,
                    maiden_name varchar(255) DEFAULT NULL,
                    nickname varchar(255) DEFAULT NULL,
                    phone varchar(255) DEFAULT NULL,
                    email varchar(255) DEFAULT NULL,
                    address_1 varchar(255) DEFAULT NULL,
                    address_2 varchar(255) DEFAULT NULL,
                    address_3 varchar(255) DEFAULT NULL,
                    address_4 varchar(255) DEFAULT NULL,
                    postal_code varchar(255) DEFAULT NULL,
                    country varchar(255) DEFAULT NULL,
                    state varchar(255) DEFAULT NULL,
                    city varchar(255) DEFAULT NULL,
                    note varchar(255) DEFAULT NULL,
                    created_at timestamp NULL DEFAULT NULL,
                    updated_at timestamp NULL DEFAULT NULL,
                    deleted_at timestamp NULL DEFAULT NULL,
                    PRIMARY KEY (id)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }

    public static function propertyInventory($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `property_inventory` (
                      `id` int(20) NOT NULL AUTO_INCREMENT,
                      `select_type` enum('0','1','2') DEFAULT NULL,
                      `property_id` int(20) DEFAULT NULL,
                      `building_id` int(20) DEFAULT NULL,
                      `unit_id` int(20) DEFAULT NULL,
                      `inventory_img1` longblob,
                      `inventory_img2` longblob,
                      `inventory_img3` longblob,
                      `sublocation` int(20) DEFAULT NULL,
                      `category` int(20) DEFAULT NULL,
                      `sub_category` int(20) DEFAULT NULL,
                      `item_code` varchar(100) DEFAULT NULL,
                      `warranty_from` date DEFAULT NULL,
                      `warranty_to` date DEFAULT NULL,
                      `installation_date` date DEFAULT NULL,
                      `cost` varchar(200) DEFAULT NULL,
                      `replacement_date` date DEFAULT NULL,
                      `guarantee` int(20) DEFAULT NULL,
                      `description` text,
                      `company_name` varchar(200) DEFAULT NULL,
                      `company_person` varchar(200) DEFAULT NULL,
                      `phone_number` varchar(50) DEFAULT NULL,
                      `notes` text,
                      `warranty_alert` enum('0','1') DEFAULT '0',
                      `replacement_alert` enum('0','1') DEFAULT '0',
                      `secondary_note` text,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }


    public static function createMcc($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `mcc_types` (

              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,

             `user_id` bigint(20) DEFAULT NULL,

             `name` text  DEFAULT NULL,

             `code` int(11) DEFAULT NULL,

             `status` enum('0','1') DEFAULT NULL,

             `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

             `updated_at` timestamp NULL DEFAULT NULL,

              PRIMARY KEY (`id`)

            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            //return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }

    public static function alterConversationTablePortal($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `conversation` LIKE 'is_portal'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `conversation` ADD `is_portal`  varchar(255) DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function createOwnerPropertyRelation($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `owner_property_relation` (
                      `id` int(20) NOT NULL AUTO_INCREMENT,
                      `owner_id` int(20) DEFAULT NULL,
                      `property_id` int(20) DEFAULT NULL,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function alterTenantPhoneTableForExtension($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `tenant_phone` LIKE 'other_work_phone_extension'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `tenant_phone` ADD `other_work_phone_extension`  varchar(255) DEFAULT NULL AFTER `work_phone_extension`";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function alterGeneralPropertyTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `general_property` LIKE 'vacant'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `general_property` ADD `vacant` INT(20) NULL DEFAULT '0' AFTER `clone_id`";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function alterUsersTableForExtension($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `users` LIKE 'other_work_phone_extension'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `users` ADD `other_work_phone_extension` varchar(255) DEFAULT NULL AFTER `work_phone_extension`";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public static function alterOwnerPreffTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `owner_blacklist_vendors` LIKE 'vendor_id'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `owner_blacklist_vendors` ADD `vendor_id` INT NULL AFTER `deleted_at`;";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);

        }
    }
    public static function alterUserTableDiscountPrice($connection) {
        try {

            $ifExists = $connection->query("SHOW COLUMNS FROM `users` LIKE 'discount_price'")->fetchAll();

            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE users ADD discount_price VARCHAR(255) DEFAULT NULL";
            $connection->prepare($query)->execute();
            $query = "ALTER TABLE users ADD stripe_account_id VARCHAR(255) DEFAULT NULL";
            $connection->prepare($query)->execute();
            $query = "ALTER TABLE users ADD stripe_customer_id VARCHAR(255) DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }

    public static function alterOwnerRelationTable($connection) {
        try {

            $ifExists = $connection->query("SHOW COLUMNS FROM `owner_property_relation` LIKE 'percentage'")->fetchAll();

            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE owner_property_relation ADD percentage VARCHAR(255) DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }

    public static function createUserAccountDetails($connection) {
        try {
            $table = "CREATE TABLE  IF NOT EXISTS `user_account_detail` (

              `id` int(11) NOT NULL AUTO_INCREMENT,
   
              `user_id` int(11) DEFAULT NULL,

              `country` varchar(255) DEFAULT NULL,  
  
              `mcc` varchar(255) DEFAULT NULL,
  
              `url` varchar(255) DEFAULT NULL,
  
              `business_type` varchar(255) DEFAULT NULL,
  
              `company_city` varchar(255) DEFAULT NULL,
  
              `company_address1` varchar(255) DEFAULT NULL,
  
              `company_address2` varchar(255) DEFAULT NULL,
  
              `company_postal_code` varchar(255) DEFAULT NULL,
  
              `company_state` varchar(255) DEFAULT NULL,
  
              `company_name` varchar(255) DEFAULT NULL,
  
              `tax_id` varchar(255) DEFAULT NULL,
  
              `company_phone_number` varchar(255) DEFAULT NULL,
  
              `company_document_id` varchar(255) DEFAULT NULL,
  
              `address_1` varchar(255) DEFAULT NULL,
  
              `address_2` varchar(255) DEFAULT NULL,

              `account_type`  varchar(255) DEFAULT NULL,

              `account_number`  varchar(255) DEFAULT NULL,

              `routing_number`  varchar(255) DEFAULT NULL,

              `city`  varchar(255) DEFAULT NULL,

              `line1`  varchar(255) DEFAULT NULL,

              `address_line2` varchar(255) DEFAULT NULL,

              `postal_code`  varchar(255) DEFAULT NULL,

              `state`  varchar(255) DEFAULT NULL,

              `email`  varchar(255) DEFAULT NULL,

              `first_name` varchar(255) DEFAULT NULL,

              `last_name`  varchar(255) DEFAULT NULL,

              `phone`  varchar(255) DEFAULT NULL,

              `ssn_last`  varchar(255) DEFAULT NULL,

              `dob`  varchar(255) DEFAULT NULL,

              `year` varchar(255) DEFAULT NULL,
               
              `month`  varchar(255) DEFAULT NULL,
              
              `day`  varchar(255) DEFAULT NULL,

              `created_at` datetime DEFAULT NULL,

              `updated_at` datetime DEFAULT NULL,

              `deleted_at` datetime DEFAULT NULL,

               PRIMARY KEY (`id`)
            )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            // echo '<pre>'; print_r($exception);echo '</pre>';

        }
    }

    public static function createTransactionTable($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `transactions` (
                      `id` bigint(20) NOT NULL AUTO_INCREMENT,
                      `transaction_id` varchar(255) DEFAULT NULL,
                      `charge_id` varchar(255) DEFAULT NULL,
                      `plan_id` int(10) DEFAULT NULL,
                      `term_plan` enum('MONTHLY','YEARLY') DEFAULT NULL,
                      `user_id` bigint(20) DEFAULT NULL,
                      `user_type` enum('OWNER','VENDOR','TENANT','PM','STR','OTHER') DEFAULT NULL,
                      `amount` float DEFAULT NULL,
                      `prorated_payment_amount` float DEFAULT NULL,
                      `prorated_payment_days` int(10) DEFAULT NULL,
                      `security_amount` float DEFAULT NULL,
                      `extra_charge_amount` float DEFAULT NULL,
                      `discount` float DEFAULT NULL,
                      `total_charge_amount` float DEFAULT NULL,
                      `stripe_status` text,
                      `payment_status` enum('SUCCESS','ERROR','PENDING') DEFAULT NULL,
                      `payment_response` text,
                      `apex_link_service_fee` float DEFAULT NULL,
                      `pay_plan_price` float DEFAULT NULL,
                      `apex_link_admin_fee` float DEFAULT NULL,
                      `stripe_account_fee` float DEFAULT NULL,
                      `stripe_transaction_fee` float DEFAULT NULL,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `payment_module` varchar(255) DEFAULT NULL,
                      `payment_mode` enum('one_time','reccuring') DEFAULT 'one_time',
                      `property_id` int(11) DEFAULT NULL,
                      `type` enum('STR','RENT','RENT_FIRSTTIME','TAX','CHARGES','SUBSCRIPTION') DEFAULT NULL,
                      `bank_id` int(11) DEFAULT NULL,
                      `reference_no` varchar(255) DEFAULT NULL,
                      `check_number` varchar(255) DEFAULT NULL,
                      `payment_type` enum('card','ACH','check','cash','money-order') NOT NULL DEFAULT 'card',
                      PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }

    public static function alterPlanHistoryTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `plans_history` LIKE 'routing_number'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `plans_history` CHANGE `term_plan` `term_plan` ENUM('0','1','2','3','4') DEFAULT NULL;";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function alterAutopayForUserTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `users` LIKE 'auto_pay'")->fetchAll();

            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `users` ADD `auto_pay` ENUM('ON','OFF')  DEFAULT 'ON' AFTER `stripe_customer_id`";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }

    public static function alterflagsTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `flags` LIKE 'object_name'")->fetchAll();

            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `flags` ADD `object_name` VARCHAR(200) NULL DEFAULT NULL AFTER `user_id`";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function alterpropertyinspectionTable($connection) {
        try {
            $query = "ALTER TABLE `property_inspection` CHANGE `custom_field` `custom_field` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function createCampaign($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `campaign` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `source_name` varchar(255) DEFAULT NULL,

                      `compaign_name` varchar(255) DEFAULT NULL,
                      
                      `call_track_number` varchar(255) DEFAULT NULL,

                      `start_date` varchar(255) DEFAULT NULL,
                      
                      `end_date` varchar(255) DEFAULT NULL,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,
                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());

        }
    }
    public static function createMarketingContactDetail($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `marketing_contact_detail` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,
  
                      `user_id` int(11) DEFAULT NULL,    

                      `company_name` varchar(255) DEFAULT NULL,

                      `url` varchar(255) DEFAULT NULL,

                      `publish` varchar(255) DEFAULT NULL,
                      
                      `phone_number` varchar(255) DEFAULT NULL,
  
                      `address` varchar(255) DEFAULT NULL,
                      
                      `email` varchar(255) DEFAULT NULL,
                      
                      `zip_code` varchar(255) DEFAULT NULL,
                      
                      `country` varchar(255) DEFAULT NULL,
  
                       `state` varchar(255) DEFAULT NULL,
  
                       `city` varchar(255) DEFAULT NULL,
  
                       `website` varchar(255) DEFAULT NULL,
  
                       `hours_of_operation` varchar(255) DEFAULT NULL,
  
                       `logo` varchar(255) DEFAULT NULL,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT NULL,
                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());

        }
    }

    public static function createmaintenanceitemcode($connection) {
        try {
            $table = "	
                CREATE TABLE IF NOT EXISTS  `maintenance_inventory_item_code` (
                 `id` int(11) NOT NULL AUTO_INCREMENT,
                 `inventory_id` int(11) NOT NULL,
                 `code` varchar(255) NOT NULL,
                 `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                 `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                 PRIMARY KEY (`id`)
                )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }

    public static function createMarketingPostsTable($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `marketing_posts` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `property_id` varchar(255) DEFAULT NULL,

                      `is_published` varchar(255) DEFAULT NULL,
                      
                      `is_featured` varchar(255) DEFAULT NULL,

                      `show_on_map` varchar(255) DEFAULT NULL,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,
                      
                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function createFlyerTable($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `flyer_posts` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `logo` varchar(255) DEFAULT NULL,

                      `template` varchar(255) DEFAULT NULL,
                      
                      `status` enum('0','1') DEFAULT NULL,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,
                      
                      PRIMARY KEY (`id`)

                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }

    public static function createAccountingInvoice($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `accounting_invoices` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `invoice_number` int(20) NOT NULL,
                      `invoice_to` int(11) DEFAULT NULL,
                      `invoice_from` int(11) DEFAULT NULL,
                      `user_id` int(11) DEFAULT NULL,
                      `user_type` int(11) DEFAULT NULL,
                      `property_id` int(11) DEFAULT NULL,
                      `total_amount` decimal(10,2) DEFAULT NULL,
                      `amount_paid` decimal(10,2) DEFAULT NULL,
                      `invoice_date` date DEFAULT NULL,
                      `late_date` date DEFAULT NULL,
                      `amount_due` text,
                      `other_address` longtext,
                      `other_name` varchar(255) DEFAULT NULL,
                      `email_invoice` tinyint(4) DEFAULT NULL,
                      `no_late_fee` tinyint(1) DEFAULT NULL,
                      `frequency` int(11)  DEFAULT NULL,
                      `duration` VARCHAR(255) DEFAULT NULL,
                      `module_type` VARCHAR(255) DEFAULT NULL,
                      `status` int(11) NOT NULL COMMENT '0=>unpaid, 1=>paid, 2=>late',
                      `created_at` datetime NOT NULL,
                      `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                      `deleted_at` datetime DEFAULT NULL,
                      PRIMARY KEY (`id`)
)";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }


    public static function createAccountingManageCharges($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `accounting_manage_charges` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `user_id` int(11) DEFAULT NULL,
                      `invoice_id` int(11) DEFAULT NULL,
                      `total_amount` decimal(10,0) DEFAULT NULL,
                      `total_due_amount` decimal(10,0) DEFAULT NULL,
                      `total_amount_paid` decimal(10,0) DEFAULT '0',
                      `total_refunded_amount` decimal(10,0) DEFAULT NULL,
                      `overpay_underpay` decimal(10,0) DEFAULT NULL,
                      `status` tinyint(4) NOT NULL DEFAULT '0',
                      `created_at` datetime NOT NULL,
                      `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                      PRIMARY KEY (`id`)
                      )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }



    public static function alterGenPropertyTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `general_property` LIKE 'posting_type'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `general_property` ADD `posting_type` VARCHAR(255) NULL AFTER `clone_status`;	";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);
        }
    }
    public static function createMapAddress($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `map_address` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `property_id` int(11) DEFAULT NULL,

                      `address` varchar(255) DEFAULT NULL,
  
                      `latitude` varchar(255) DEFAULT NULL,
  
                      `longitude` varchar(255) DEFAULT NULL,                      

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,

                      `updated_at` timestamp NULL DEFAULT NULL,

                      `deleted_at` timestamp NULL DEFAULT NULL,
                      
                      PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }

    public static function createUtilityBillingTable($connection) {
        try {
//            $table = "DROP TABLE  `utility_billing`";
//            $connection->prepare($table)->execute();
            $table = "CREATE TABLE IF NOT EXISTS `utility_billing` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `user_id` varchar(255) DEFAULT NULL,
                      
                      `start_date` timestamp NULL DEFAULT NULL,
                      
                      `end_date` timestamp NULL DEFAULT NULL,
                      
                      `utility_type` varchar(255) DEFAULT NULL,
                      
                      `utility_amount` varchar(255) DEFAULT NULL,
                      
                      `status` varchar(255) DEFAULT NULL,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      
                      `updated_at` timestamp NULL DEFAULT NULL,
                      
                      `deleted_at` timestamp NULL DEFAULT NULL,
                      
                      PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }

//    public static function alterTenantChargesTable($connection) {
//        try {
//            $query = "ALTER TABLE tenant_charges ADD user_type INT(11) DEFAULT NULL";
//            $connection->prepare($query)->execute();
//            $query = "ALTER TABLE tenant_charges ADD invoice_id INT(11) DEFAULT NULL";
//            $connection->prepare($query)->execute();
//            $query = "ALTER TABLE tenant_charges ADD waive_of_amount DECIMAL (11,2) DEFAULT NULL";
//            $connection->prepare($query)->execute();
//            $query = "ALTER TABLE tenant_charges ADD waive_of_comment TEXT DEFAULT NULL";
//            $connection->prepare($query)->execute();
//            $query = "ALTER TABLE tenant_charges ADD invoice_from INT(11) DEFAULT NULL";
//            $connection->prepare($query)->execute();
//            $query = "ALTER TABLE tenant_charges ADD amount_due DECIMAL(10,2) DEFAULT NULL";
//            $connection->prepare($query)->execute();
//            $query = "ALTER TABLE tenant_charges ADD amount_paid DECIMAL(10,2) DEFAULT NULL";
//            $connection->prepare($query)->execute();
//            $query = "ALTER TABLE tenant_charges ADD amount_refunded DECIMAL(10,2) DEFAULT NULL";
//            $connection->prepare($query)->execute();
//        }
//        catch(Exception $exception) {
//            // print_r($exception);
//        }
//    }

//    public static function alterTransactionsTable($connection) {
//        try {
//            $query = "ALTER TABLE transactions ADD payment_type enum('card','ACH','check','cash','money-order') NOT NULL DEFAULT 'card',";
//            $connection->prepare($query)->execute();
//            $query = "ALTER TABLE transactions ADD ref_id VARCHAR(255) DEFAULT NULL";
//            $connection->prepare($query)->execute();
//            $query = "ALTER TABLE transactions ADD check_number VARCHAR(255) DEFAULT NULL";
//            $connection->prepare($query)->execute();
//
//        }
//        catch(Exception $exception) {
//            // print_r($exception);
//        }
//    }
//    public static function alterTransactionsTable($connection) {
//        try {
//            $ifExists = $connection->query("SHOW COLUMNS FROM `communication_email` LIKE 'gtype'")->fetchAll();
//            if (!empty($ifExists)) {
//                return;
//            }
//            $query = "ALTER TABLE transactions ADD payment_type enum('card','ACH','check','cash','money-order') NOT NULL DEFAULT 'card',";
//            $connection->prepare($query)->execute();
//            $query = "ALTER TABLE transactions ADD ref_id VARCHAR(255) DEFAULT NULL";
//            $connection->prepare($query)->execute();
//            $query = "ALTER TABLE transactions ADD check_number VARCHAR(255) DEFAULT NULL";
//            $connection->prepare($query)->execute();
//
//        }
//        catch(Exception $exception) {
//            // print_r($exception);
//        }
//    }
    public static function create_short_term_rental($connection) {
        try {
            $query = "CREATE TABLE IF NOT EXISTS  `short_term_rental` (
                     `id` int(11) NOT NULL AUTO_INCREMENT,
                     `user_id` int(255) DEFAULT NULL,
                     `email2` varchar(255) DEFAULT NULL,
                     `email3` varchar(255) DEFAULT NULL,
                     `frequency` int(255) DEFAULT NULL,
                     `start_date` date DEFAULT NULL,
                     `end_date` date DEFAULT NULL,
                     `date_id` int(255) DEFAULT NULL,
                     `property_id` int(255) DEFAULT NULL,
                     `building_id` int(255) DEFAULT NULL,
                     `unit_id` int(255) DEFAULT NULL,
                     `image` longblob,
                     `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                     `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
                     `deleted_at` timestamp NULL DEFAULT NULL,
                     PRIMARY KEY (`id`)
                    ) ";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);
        }
    }
    public static function create_short_term_rental_booking_dates($connection) {
        try {
            $query = "CREATE TABLE  IF NOT EXISTS `short_term_rental_booking_dates` (
                     `id` int(11) NOT NULL AUTO_INCREMENT,
                     `user_id` int(255) NOT NULL,
                     `date` date NOT NULL,
                     `unit_booked_id` int(220) NOT NULL,
                     `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                     PRIMARY KEY (`id`)
                    )";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);
        }
    }

    public static function create_company_bills($connection) {
        try {
            $query = "CREATE TABLE IF NOT EXISTS `company_bills` (
                      `id` bigint(10) NOT NULL AUTO_INCREMENT,
                      `user_id` int(20) DEFAULT NULL,
                      `user_type` enum('Vendor','Tenant','Owner') DEFAULT NULL,
                      `vendor_id` bigint(10) DEFAULT NULL,
                      `portfolio_id` int(10) DEFAULT NULL,
                      `property_id` int(20) DEFAULT NULL,
                      `address` text,
                      `bill_date` datetime DEFAULT NULL,
                      `amount` double DEFAULT NULL,
                      `discount_amount` double DEFAULT NULL,
                      `refrence_number` varchar(100) DEFAULT NULL,
                      `change_to` enum('1','2') DEFAULT NULL COMMENT '1=tenant,2=other',
                      `change_to_other` varchar(100) DEFAULT NULL,
                      `due_date` datetime DEFAULT NULL,
                      `invoice_number` varchar(100) DEFAULT NULL,
                      `work_order` text,
                      `term` varchar(100) DEFAULT NULL,
                      `memo` text,
                      `status` enum('0','1','2','3') DEFAULT NULL COMMENT '0=Due,1=Paid,2=Overdue,3=OnHold',
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    )";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);
        }
    }

    public static function create_company_bill_files($connection) {
        try {
            $query = "CREATE TABLE IF NOT EXISTS `company_bill_files` (
                      `id` int(10) NOT NULL AUTO_INCREMENT,
                      `bill_id` bigint(10) DEFAULT NULL,
                      `name` varchar(100) DEFAULT NULL,
                      `extension` varchar(100) DEFAULT NULL,
                      `path` varchar(250) DEFAULT NULL,
                      `status` enum('0','1') DEFAULT NULL,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    )";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);
        }
    }

    public static function create_company_bill_items($connection) {
        try {
            $query = "CREATE TABLE IF NOT EXISTS `company_bill_items` (
                      `id` bigint(10) NOT NULL AUTO_INCREMENT,
                      `bill_id` int(10) DEFAULT NULL,
                      `user_id` bigint(10) DEFAULT NULL,
                      `building_id` bigint(10) DEFAULT NULL,
                      `unit_id` bigint(10) DEFAULT NULL,
                      `account` int(10) DEFAULT NULL,
                      `amount` float DEFAULT NULL,
                      `description` text,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    )";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);
        }
    }

    public static function create_company_bill_notes($connection) {
        try {
            $query = "CREATE TABLE IF NOT EXISTS `company_bill_notes` (
                      `id` int(10) NOT NULL AUTO_INCREMENT,
                      `user_id` bigint(10) DEFAULT NULL,
                      `bill_id` bigint(10) DEFAULT NULL,
                      `note` text,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    )";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);
        }
    }

//    public static function alterUserTableDiscountPrice($connection) {
//        try {
//
//            $ifExists = $connection->query("SHOW COLUMNS FROM `users` LIKE 'discount_price'")->fetchAll();
//
//            if (!empty($ifExists)) {
//                return;
//            }
//            $query = "ALTER TABLE users ADD discount_price VARCHAR(255) DEFAULT NULL";
//            $connection->prepare($query)->execute();
//            $query = "ALTER TABLE users ADD stripe_account_id VARCHAR(255) DEFAULT NULL";
//            $connection->prepare($query)->execute();
//            $query = "ALTER TABLE users ADD stripe_customer_id VARCHAR(255) DEFAULT NULL";
//            $connection->prepare($query)->execute();
//        }
//        catch(Exception $exception) {
//            print_r($exception->getMessage());
//        }
//    }

    public static function alterAccountingInvoicesTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `accounting_invoices` LIKE 'property_id'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE accounting_invoices ADD property_id int(11) DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);

        }
    }
    public static function alterflyerPostsTable($connection) {
        try {
            $query = "ALTER TABLE `flyer_posts` CHANGE `template` `template` LONGTEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL	";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);
        }
    }
    public static function altermarketingContactDetailTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `marketing_contact_detail` LIKE 'latitude'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `marketing_contact_detail` ADD `latitude` VARCHAR(255) NULL AFTER `updated_at`, ADD `longitude` VARCHAR(255) NULL AFTER `latitude`";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);
        }
    }

    public static function alterCompanyBillTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `company_bills` LIKE 'tenant_id'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `company_bills` ADD `tenant_id` INT(10) NULL DEFAULT NULL AFTER `vendor_id`";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);
        }
    }

    public static function smApplication($connection) {
        try {
            $query = "CREATE TABLE IF NOT EXISTS `sm_application` (
                         `id` int(11) NOT NULL AUTO_INCREMENT,
                         `user_id` int(11) DEFAULT NULL,
                         `tenant_id` int(11) DEFAULT NULL,
                         `product_bundle` varchar(255) DEFAULT NULL,
                         `applicants` text,
                         `application_id` varchar(255) DEFAULT NULL,
                         `landlord_pays` varchar(255) DEFAULT NULL,
                         `property_id` varchar(255) DEFAULT NULL,
                         `rent` varchar(255) DEFAULT NULL,
                         `deposit` varchar(255) DEFAULT NULL,
                         `lease_term` varchar(255) DEFAULT NULL,
                         `unit_number` varchar(255) DEFAULT NULL,
                         `currenttime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                         `cancel_status` int(11) NOT NULL DEFAULT '0',
                         `status` int(11) NOT NULL DEFAULT '0',
                         PRIMARY KEY (`id`)
                    )";
            $connection->prepare($query)->execute();
        }catch(Exception $exception) {
        }
    }

    public static function smProperty($connection) {
        try {
            $query = "CREATE TABLE IF NOT EXISTS `sm_property` (
                   `id` int(11) NOT NULL AUTO_INCREMENT,
                   `tenant_id` int(11) DEFAULT NULL,
                   `user_id` int(11) DEFAULT NULL,
                   `property_id` int(11) DEFAULT NULL,
                   `unit_id` int(11) DEFAULT NULL,
                   `property_unique_id` varchar(255) DEFAULT NULL,
                   `address1` text,
                   `address2` text,
                   `city` varchar(255) DEFAULT NULL,
                   `state` varchar(255) DEFAULT NULL,
                   `zipcode` varchar(255) DEFAULT NULL,
                   `rent_amount` varchar(255) DEFAULT NULL,
                   `deposite_amount` varchar(255) DEFAULT NULL,
                   `bankrupters_text` text,
                   `record_status` enum('0','1') DEFAULT '0',
                   `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                   `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                   `status` enum('0','1') DEFAULT '0' COMMENT '0 for report result waiting and 1 for report result success',
                   `sm_property_id` varchar(255) DEFAULT NULL,
                   `property_identifier` varchar(255) DEFAULT NULL,
                   `organization_id` varchar(255) DEFAULT NULL,
                   `sm_active` int(11) DEFAULT NULL,
                   `sm_name` varchar(255) DEFAULT NULL,
                   `sm_street` text,
                   `sm_phone` varchar(255) DEFAULT NULL,
                   `phone_extension` varchar(255) DEFAULT NULL,
                   `unit_number` varchar(255) DEFAULT NULL,
                   `sm_ir` varchar(255) DEFAULT NULL,
                   `include_medical_collections` varchar(255) DEFAULT NULL,
                   `include_foreclosures` varchar(255) DEFAULT NULL,
                   `open_bankruptcy_window` varchar(255) DEFAULT NULL,
                   `is_fcra_agreement_accepted` varchar(255) DEFAULT NULL,
                   `decline_for_open_bankruptcies` varchar(255) DEFAULT NULL,
                   `landlord_firstName` varchar(255) DEFAULT NULL,
                   `landlord_lastName` varchar(255) DEFAULT NULL,
                   `landlord_streetAddressLineOne` varchar(255) DEFAULT NULL,
                   `landlord_streetAddressLineTwo` varchar(255) DEFAULT NULL,
                   `landlord_city` varchar(255) DEFAULT NULL,
                   `landlord_state` varchar(255) DEFAULT NULL,
                   `landlord_zip` varchar(255) DEFAULT NULL,
                   `landlord_phoneNumber` varchar(255) DEFAULT NULL,
                   `landlord_email` varchar(255) DEFAULT NULL,
                   `landlord_isCompany` varchar(255) DEFAULT NULL,
                   PRIMARY KEY (`id`)
                   )";
            $connection->prepare($query)->execute();
        }catch(Exception $exception) {
        }
    }

    public static function smLandlord($connection) {
        try {
            $query = "CREATE TABLE IF NOT EXISTS `sm_landlord` (
                   `id` int(11) NOT NULL AUTO_INCREMENT,
                   `user_id` int(11) NOT NULL COMMENT 'current loggedin user',
                   `tenant_id` int(11) NOT NULL,
                   `user_type` enum('1','2') DEFAULT NULL COMMENT '1 for landlord and 2 for renter',
                   `currenttimestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                   `status` enum('0','1') DEFAULT NULL COMMENT '0 for inactive and 1 for active',
                   `email_id` varchar(255) DEFAULT NULL,
                   `is_agrmnt_accepted` enum('0','1') DEFAULT NULL,
                   `sm_password` varchar(255) DEFAULT NULL,
                   `security_question_1` text,
                   `secQstnAns1` text,
                   `security_question_2` text,
                   `secQstnAns2` text,
                   `security_question_3` text,
                   `secQstnAns3` text,
                   `income` varchar(255) DEFAULT NULL,
                   `income_term` varchar(255) DEFAULT NULL,
                   `otherincome` varchar(255) DEFAULT NULL,
                   `otherincome_term` varchar(255) DEFAULT NULL,
                   `assets` varchar(255) DEFAULT NULL,
                   `empmntstatus` varchar(255) DEFAULT NULL,
                   `renter_created` enum('0','1') DEFAULT '0',
                   PRIMARY KEY (`id`)
                    )";
            $connection->prepare($query)->execute();
        }catch(Exception $exception) {
        }
    }


    public static function alterTenantRentalLeaseSignaures($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `tenant_rental_lease_signaures` LIKE 'style'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `tenant_rental_lease_signaures` ADD `style` TEXT NULL DEFAULT NULL AFTER `usertype`";
            $connection->prepare($query)->execute();
        }catch(Exception $exception) {


            $connection->prepare($query)->execute();
        }
    }


    public static function alterAccountingInvoices($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `accounting_invoices` LIKE 'frequency'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `accounting_invoices` ADD `frequency` int(11) NULL DEFAULT NULL";
            $connection->prepare($query)->execute();
            $query = "ALTER TABLE accounting_invoices ADD `duration` VARCHAR(255) DEFAULT NULL";
            $connection->prepare($query)->execute();
            $query = "ALTER TABLE accounting_invoices ADD `module_type` VARCHAR(255) DEFAULT NULL";
            $connection->prepare($query)->execute();
        }catch(Exception $exception) {


            $connection->prepare($query)->execute();
        }
    }






    public static function alterFlyerTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `flyer_posts` LIKE 'template_name'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `flyer_posts` ADD `template_name` VARCHAR(255) NULL AFTER `deleted_at`";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);
        }
    }

    public static function createAccountingJournalEnteries($connection) {
        try {
            $query = "CREATE TABLE IF NOT EXISTS `accounting_journal_entries` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                    `user_id` int(11) NOT NULL,
                    `journal_entry_type` varchar(250) NOT NULL,
                    `journal_entry_type_select` int(11) DEFAULT NULL,
                    `portfolio_id` int(11) DEFAULT NULL,
                    `property_id` int(11) DEFAULT NULL,
                    `building_id` int(11) DEFAULT NULL,
                    `unit_id` int(11) DEFAULT NULL,
                    `accounting_period` varchar(50) DEFAULT NULL,
                    `bank_account_id` int(11) DEFAULT NULL,
                    `refrence_number` varchar(250) DEFAULT NULL,
                    `notes` longtext,
                    `status` tinyint(4) DEFAULT NULL,
                    `created_at` datetime DEFAULT NULL,
                    `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    `deleted_at` datetime DEFAULT NULL,
                    PRIMARY KEY (`id`)
)";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);
        }
    }



    public static function createJournalEntryDetail($connection) {
        try {
            $query = "CREATE TABLE IF NOT EXISTS `journal_entry_details` (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                        `journal_id` int(11) NOT NULL,
                        `chart_account_id` int(11) NOT NULL,
                        `description` text  DEFAULT NULL,
                        `debit` decimal(10,2) DEFAULT NULL,
                        `credit` decimal(10,2) DEFAULT NULL,
                        `status` int(11) DEFAULT NULL,
                        `created_at` datetime DEFAULT NULL,
                        `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         PRIMARY KEY (`id`)
)";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);
        }
    }



    public static function createAccountingFileUploads($connection) {
        try {
            $query = "CREATE TABLE IF NOT EXISTS `accounting_file_uploads` (
                      `id` int(20) NOT NULL AUTO_INCREMENT,
                      `module_name` varchar(250) DEFAULT NULL,
                      `module_id` int(11) DEFAULT NULL,
                      `user_id` int(20) DEFAULT NULL,
                      `file_name` text,
                      `file_size` varchar(20) DEFAULT NULL,
                      `file_location` text,
                      `file_extension` varchar(20) DEFAULT NULL,
                      `codec` varchar(100) DEFAULT NULL,
                      `marketing_site` enum('0','1') DEFAULT NULL,
                      `file_type` enum('1','2','3') DEFAULT NULL COMMENT '1=images,2=documents,3=videos',
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL,
                      PRIMARY KEY (`id`)
)";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);
        }
    }



    public static function createAccountingLedger($connection) {
        try {
            $query = "CREATE TABLE IF NOT EXISTS `accounting_ledger` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `transaction_id` varchar(250) DEFAULT NULL,
                      `transaction_type` varchar(250) DEFAULT NULL,
                      `module_type` varchar(250) DEFAULT NULL,
                      `module_id` int(11) DEFAULT NULL,
                      `from_user` int(11) DEFAULT NULL,
                      `to_user` int(11) DEFAULT NULL,
                      `debit` decimal(20,2) DEFAULT NULL,
                      `credit` decimal(20,2) DEFAULT NULL,
                      `current_month_year` varchar(50) DEFAULT NULL,
                      `created_at` datetime DEFAULT NULL,
                      `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      PRIMARY KEY (`id`)
)";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);
        }
    }








    public static function createBankReconciliationTable($connection) {
        try {
//            $table = "DROP TABLE  `utility_billing`";
//            $connection->prepare($table)->execute();
            $table = "CREATE TABLE IF NOT EXISTS `bank_reconciliation` (

                      `id` int(11) NOT NULL AUTO_INCREMENT,

                      `user_id` varchar(255) DEFAULT NULL,
                      
                      `bank_account` timestamp NULL DEFAULT NULL,
                      
                      `statement_end_date` timestamp NULL DEFAULT NULL,
                      
                      `beginning_balance` varchar(255) DEFAULT NULL,
                      
                      `ending_balance` varchar(255) DEFAULT NULL,
                      
                      `status` varchar(255) DEFAULT NULL,

                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      
                      `updated_at` timestamp NULL DEFAULT NULL,
                      
                      `deleted_at` timestamp NULL DEFAULT NULL,
                      
                      PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }

    public static function createCompany_budgetTable($connection) {
        try {
//            $table = "DROP TABLE  `utility_billing`";
//            $connection->prepare($table)->execute();
            $table = "CREATE TABLE IF NOT EXISTS `company_budget` (
                      `id` bigint(11) NOT NULL AUTO_INCREMENT,
                      `user_id` bigint(11) DEFAULT NULL,
                      `budget_name` varchar(200) DEFAULT NULL,
                      `starting_month` varchar(200) DEFAULT NULL,
                      `starting_year` varchar(200) DEFAULT NULL,
                      `portfolio_id` bigint(11) DEFAULT NULL,
                      `property_id` bigint(11) DEFAULT NULL,
                      `income_amount` double DEFAULT NULL,
                      `expense_amount` double DEFAULT NULL,
                      `net_income` double DEFAULT NULL,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      `deleted_at` timestamp NULL DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }

    public static function createCompany_budget_detailsTable($connection) {
        try {
//            $table = "DROP TABLE  `utility_billing`";
//            $connection->prepare($table)->execute();
            $table = "CREATE TABLE IF NOT EXISTS `company_budget_details` (
                      `id` bigint(11) NOT NULL AUTO_INCREMENT,
                      `user_id` bigint(11) DEFAULT NULL,
                      `budget_id` bigint(11) DEFAULT NULL,
                      `charts_of_account_id` int(11) DEFAULT NULL,
                      `type` enum('income','expense') DEFAULT NULL,
                      `january` double DEFAULT NULL,
                      `february` double DEFAULT NULL,
                      `march` double DEFAULT NULL,
                      `april` double DEFAULT NULL,
                      `may` double DEFAULT NULL,
                      `june` double DEFAULT NULL,
                      `july` double DEFAULT NULL,
                      `august` double DEFAULT NULL,
                      `september` double DEFAULT NULL,
                      `october` double DEFAULT NULL,
                      `november` double DEFAULT NULL,
                      `december` double DEFAULT NULL,
                      `total_fy` double DEFAULT NULL,
                      `created_at` timestamp NULL DEFAULT NULL,
                      `updated_at` timestamp NULL DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }


    public static function alterCompanyAccountingBankAccount($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `company_accounting_bank_account` LIKE 'routing_number'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `company_accounting_bank_account` ADD `routing_number` VARCHAR(255) NULL AFTER `branch_code`";
            $connection->prepare($query)->execute();
        }catch(Exception $exception) {


            $connection->prepare($query)->execute();
        }
    }
    public static function alterPropertyBankDetails($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `property_bank_details` LIKE 'is_default'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `property_bank_details` ADD `is_default` ENUM('0','1') NULL AFTER `deleted_at`";
            $connection->prepare($query)->execute();
        }catch(Exception $exception) {


            $connection->prepare($query)->execute();
        }
    }
    public static function alterPropertyBankDetailsSource($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `property_bank_details` LIKE 'source_id'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `property_bank_details` ADD `source_id` VARCHAR(255) NULL AFTER `is_default`";
            $connection->prepare($query)->execute();
        }catch(Exception $exception) {


            $connection->prepare($query)->execute();
        }
    }
    public static function alterCompanyAccountingBankAccountSource($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `company_accounting_bank_account` LIKE 'source_id'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `company_accounting_bank_account` ADD `source_id` VARCHAR(255) NULL AFTER `initial_amount`";
            $connection->prepare($query)->execute();
        }catch(Exception $exception) {


            $connection->prepare($query)->execute();
        }
    }
    public static function alterPropertyBankDetailsBankId($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `property_bank_details` LIKE 'bank_id'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `property_bank_details` ADD `bank_id` INT NULL AFTER `source_id`";
            $connection->prepare($query)->execute();
        }catch(Exception $exception) {
            $connection->prepare($query)->execute();
        }
    }


    public static function alterDropTables($connection) {
        try {
            $query = "DROP TABLE IF EXISTS transactions";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);
        }
    }
    public static function createEsignSignatureTable($connection) {
        try {
//            $table = "DROP TABLE  `utility_billing`";
//            $connection->prepare($table)->execute();
            $table = "CREATE TABLE IF NOT EXISTS `esign_signatures`  (
                     `id` int(11) NOT NULL AUTO_INCREMENT,
                     `user_id` int(10) unsigned NOT NULL,
                     `status` enum('0','1','2') DEFAULT NULL,
                     `signature_image` blob,
                     `signature_text` varchar(255) DEFAULT NULL,
                     `temp_id` int(11) DEFAULT '0',
                     `style` varchar(255) NOT NULL,
                     `usertype` enum('T','O','R','G') DEFAULT 'T',
                     `currentLoggedin_id` int(11) DEFAULT NULL,
                     `esign_template` longtext,
                     `record_status` enum('0','1') DEFAULT '0',
                     `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                     `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                     PRIMARY KEY (`id`)
                    )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }


    public static function create_accounting_banking($connection) {
        try {
            $query = "CREATE TABLE IF NOT EXISTS `accounting_banking` (
                            `id` int(11) NOT NULL AUTO_INCREMENT,
                            `user_id` int(11) DEFAULT '0',
                            `check_number` varchar(255) DEFAULT NULL,
                            `other_name` varchar(255) DEFAULT NULL,
                            `check_status` enum('Entered','Printed','Void','Cleared') DEFAULT 'Entered',
                            `other_address` varchar(255) DEFAULT NULL,
                            `memo` text,
                            `amount` decimal(10,0) NOT NULL,
                            `bank_id` int(11) NOT NULL,
                            `date` date DEFAULT NULL,
                            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                            `deleted_at` varchar(255) DEFAULT NULL,
                            `property_id` int(11) DEFAULT NULL,
                            `building_id` int(11) DEFAULT NULL,
                            `unit_id` int(11) DEFAULT NULL,
                            `frequency` varchar(255) DEFAULT 'One Time ',
                            `duration` varchar(255) DEFAULT NULL,
                            `late_fee` int(11) DEFAULT '0',
                            PRIMARY KEY (`id`)
                    )";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);
        }
    }


    public static function alterAccountingManageCharges($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `accounting_manage_charges` LIKE 'waive_on_total_amount'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `accounting_manage_charges` ADD `waive_on_total_amount` DECIMAL(10,2) NULL DEFAULT NULL";
            $connection->prepare($query)->execute();
        }catch(Exception $exception) {


            $connection->prepare($query)->execute();
        }
    }

    public static function createpaymentplanTable($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `payment_plan` (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                         `user_id` int(11) NOT NULL,
                         `payor_type` enum('Tenant','Owner','Vendor','Employee','Other') NOT NULL,
                         `other_name` varchar(250) NOT NULL,
                         `principal_amount` decimal(11,2) NOT NULL,
                         `interest` decimal(11,2) NOT NULL,
                         `installments` int(11) NOT NULL,
                         `amount_due` decimal(11,2) NOT NULL,
                         `frequency` enum('Weekly','Bi-Weekly','Monthly','Annually') NOT NULL,
                         `start_date` date NOT NULL,
                         `next_payment_date` date DEFAULT NULL,
                         `next_payment_amount` decimal(11,2) NOT NULL,
                         `status` varchar(250) NOT NULL,
                         `created_at` datetime NOT NULL,
                         `deleted_at` datetime DEFAULT NULL,
                         `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
                         PRIMARY KEY (`id`)
                        )";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function createpaymentplandatesTable($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `payment_plan_dates_is_paid` (
                 `id` int(11) NOT NULL AUTO_INCREMENT,
                 `payment_id` int(11) NOT NULL,
                 `dates` date NOT NULL,
                 `is_paid` enum('Yes','No') DEFAULT NULL,
                 `amount_paid` decimal(10,2) NOT NULL,
                 `extra_amount` decimal(10,2) DEFAULT NULL,
                 `balance_amount` decimal(10,2) DEFAULT NULL,
                 `created_at` datetime DEFAULT NULL,
                 `updated_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
                 `deleted_at` datetime DEFAULT NULL,
                 PRIMARY KEY (`id`)
                ) ";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }

    public static function alterTransactionsColumnPaymentTypeTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `transactions` LIKE 'payment_type'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE transactions ADD payment_type enum('card','ACH','check','cash','money-order') NOT NULL DEFAULT 'card'";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }

    public static function alterActiveInactiveStatusTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `active_inactive_status` LIKE 'status'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE active_inactive_status ADD status varchar(250) DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }

    public static function alterActiveInactiveStatusTable1($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `active_inactive_status` LIKE 'user_id'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE active_inactive_status ADD user_id int(11) DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }

    public static function alterActiveInactiveStatusTable2($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `active_inactive_status` LIKE 'module'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE active_inactive_status ADD module varchar(250) DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }


    public static function alterActiveInactiveStatusTable3($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `active_inactive_status` LIKE 'deleted_at'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE active_inactive_status ADD deleted_at datetime DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }


    public static function createdashbordspotsetting($connection) {
        try {
            $query = "CREATE TABLE IF NOT EXISTS `dashbordspotsetting` (
             `id` int(11) NOT NULL AUTO_INCREMENT,
             `eighteenSpotChart` enum('0','1') DEFAULT '0',
             `nineSpotChart` enum('0','1') DEFAULT '0',
             `elevenSpotChart` enum('0','1') DEFAULT '0',
             `tenSpotChart` enum('0','1') DEFAULT '0',
             `firstSpotChart` enum('0','1') DEFAULT '0',
             `secSpotChart` enum('0','1') DEFAULT '0',
             `thirdSpotChart` enum('0','1') DEFAULT '0',
             `sixteenSpotChart` enum('0','1') DEFAULT '0',
             `forthSpotChart` enum('0','1') DEFAULT '0',
             `twelveSpotChart` enum('0','1') DEFAULT '0',
             `fifthSpotChart` enum('0','1') DEFAULT '0',
             `thirteenSpotChart` enum('0','1') DEFAULT '0',
             `sixSpotChart` enum('0','1') DEFAULT '0',
             `fourteenSpotChart` enum('0','1') DEFAULT '0',
             `sevenSpotChart` enum('0','1') DEFAULT '0',
             `fifteenSpotChart` enum('0','1') DEFAULT '0',
             `eightSpotChart` enum('0','1') DEFAULT '0',
             `seventeenSpotChart` enum('0','1') NOT NULL DEFAULT '0',
             `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
             `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
             PRIMARY KEY (`id`)
            )";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }

    public static function alterusertableTerm_plan($connection){
        try {
            $query = "ALTER TABLE users CHANGE term_plan term_plan ENUM('0','1','2','3','4')";
        }  catch(Exception $exception) {
            print_r($exception);
        }
    }

    public static function createOwnerContributionTable($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `owner_contribution` (
                 `id` int(11) NOT NULL AUTO_INCREMENT,
                 `date` timestamp NULL DEFAULT NULL,
                 `owner_id` varchar(255) NOT NULL,
                 `portfolio_id` varchar(255) NOT NULL,
                 `property_id` varchar(255) NOT NULL,
                 `bank_account_id` varchar(255) DEFAULT NULL,
                 `chart_of_account_id` varchar(255) NOT NULL,
                 `amount` varchar(255) NOT NULL,
                 `transation_type` varchar(255) NOT NULL,
                 `check_ref` varchar(255) DEFAULT NULL,
                 `remarks` varchar(255) DEFAULT NULL,
                 `created_at` datetime DEFAULT NULL,
                 `updated_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
                 `deleted_at` datetime DEFAULT NULL,
                 PRIMARY KEY (`id`)
                ) ";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }

    public  static  function alteraccounting_bankingtable1($connection){
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `accounting_banking` LIKE 'userType'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE accounting_banking ADD userType int(11) DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }

    public  static  function alteraccounting_bankingtable2($connection){
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `accounting_banking` LIKE 'endingbalance'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE accounting_banking ADD endingbalance double DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public  static  function alteraIntouchNotesDetail($connection){
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `in_touch_detail` LIKE 'notes'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `in_touch_detail` CHANGE `notes` `notes` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }
    public  static  function alteraIntouchNotes($connection){
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `in_touch_notes` LIKE 'notes'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `in_touch_notes` CHANGE `notes` `notes` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            print_r($exception);
        }
    }

    public static function alterTransactionsTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `transactions` LIKE 'auto_transaction_id'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE transactions ADD auto_transaction_id int(11) NOT NULL AFTER transaction_id";
            $connection->prepare($query)->execute();
            $query = "ALTER TABLE transactions ADD stripe_response longtext DEFAULT NULL AFTER stripe_status";
            $connection->prepare($query)->execute();

        }  catch(Exception $exception) {
            // print_r($exception);
        }
    }


    public static function createSubLocationTable($connection) {
        try {
            $query = "CREATE TABLE `maintainence_sub_loc` (
                         `id` int(11) NOT NULL AUTO_INCREMENT,
                         `property_id` int(11) DEFAULT NULL,
                         `building_id` int(11) DEFAULT NULL,
                         `sub_location` varchar(255) DEFAULT NULL,
                         `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                         `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                         PRIMARY KEY (`id`)
                        ) ";
            $connection->prepare($query)->execute();

        }  catch(Exception $exception) {
            // print_r($exception);
        }
    }

    public static function alterManagementFeesTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `management_fees` LIKE 'process_management_fee'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE management_fees ADD income varchar(255) DEFAULT NULL AFTER minimum_management_fee";
            $connection->prepare($query)->execute();

            $query = "ALTER TABLE management_fees ADD process_management_fee varchar(255) DEFAULT NULL AFTER minimum_management_fee";
            $connection->prepare($query)->execute();

        }  catch(Exception $exception) {
            // print_r($exception);
        }
    }

    public static function alterFlags_Table($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `flags` LIKE 'object_type'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `flags` CHANGE `object_type` `object_type` ENUM('property','unit','building','tenant','employee','contact','owner','waiting_list') NULL DEFAULT NULL;";
            $connection->prepare($query)->execute();

        }  catch(Exception $exception) {
            // print_r($exception);
        }
    }
    public static function alterXmlField($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `general_property` LIKE 'is_xml'")->fetchAll();
//            print_r($ifExists);die();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `general_property` ADD `is_xml` ENUM('0','1') DEFAULT '0' AFTER `manager_email` ";
            $connection->prepare($query)->execute();

        }  catch(Exception $exception) {
            // print_r($exception);
        }
    }
    public static function createPropertyRentalTable($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `property_rental` (
                 `id` int(11) NOT NULL AUTO_INCREMENT,
                 `property_id` varchar(255) NOT NULL,
                 `first_name` varchar(255) DEFAULT NULL,
                 `last_name` varchar(255) NOT NULL,
                 `email` varchar(255) NOT NULL,
                 `phone_number` varchar(255) NOT NULL,
                 `number_of_units` int(11) DEFAULT NULL,
                 `number_of_vacancies` int(11) DEFAULT NULL,
                 `property_location` varchar(255) DEFAULT NULL,
                 `xml_link` varchar(255) DEFAULT NULL,
                 `created_at` datetime DEFAULT NULL,
                 `updated_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
                 PRIMARY KEY (`id`)
                ) ";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }


    public static function createProcessManagementFeeTable($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `process_management_fee` (
                 `id` int(11) NOT NULL AUTO_INCREMENT,
                 /*`date` timestamp NULL DEFAULT NULL,*/
                 `property_id` varchar(255) NOT NULL,
                 `income` varchar(255) DEFAULT NULL,
                 `process_management_fee` varchar(255) NOT NULL,
                 `processed_date` varchar(255) NOT NULL,
                 `paid_status` varchar(255) NOT NULL,
                 `paid_date` varchar(255) DEFAULT NULL,
                 `created_at` datetime DEFAULT NULL,
                 `updated_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
                 `deleted_at` datetime DEFAULT NULL,
                 PRIMARY KEY (`id`)
                ) ";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }

    public static function alterTransactionsTableSM($connection) {
        try {
            $query = "ALTER TABLE `transactions` CHANGE `type` `type` ENUM('STR','RENT','RENT_FIRSTTIME','TAX','CHARGES','SUBSCRIPTION','SM') NULL DEFAULT NULL;";
            $connection->prepare($query)->execute();

            $ifExists = $connection->query("SHOW COLUMNS FROM `transactions` LIKE 'receiver_id'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `transactions` ADD `receiver_id` int(11) DEFAULT NULL AFTER `payment_type`";
            $connection->prepare($query)->execute();

        }  catch(Exception $exception) {
            // print_r($exception);
        }
    }
    public static function alterWorkOrderUserid($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `work_order` LIKE 'user_id'")->fetchAll();
//            print_r($ifExists);die();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `work_order` ADD `user_id` int(11) DEFAULT NULL";
            $connection->prepare($query)->execute();

        }  catch(Exception $exception) {
            // print_r($exception);
        }
    }

    public static function alterTransactionsTableForPropertyId($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `transactions` LIKE 'property_id'")->fetchAll();
//            print_r($ifExists);die();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `transactions` CHANGE `type` `type` ENUM('STR','RENT','RENT_FIRSTTIME','TAX','CHARGES','SUBSCRIPTION','SM','OWNER_CONTRIBUTION','OWNER_DRAW','PMC_FEE') NULL DEFAULT NULL;";
            $connection->prepare($query)->execute();

            $query = "ALTER TABLE `transactions` ADD `receiver_id` int(11) DEFAULT NULL";
            $connection->prepare($query)->execute();

            $query = "ALTER TABLE `transactions` ADD `property_id` int(11) DEFAULT NULL";
            $connection->prepare($query)->execute();

        }  catch(Exception $exception) {
            // print_r($exception);
        }
    }

    public static function createOwnerDrawTable($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `owner_draw` (
                 `id` int(11) NOT NULL AUTO_INCREMENT,
                 `owner_id` varchar(255) NOT NULL,
                 `property_id` varchar(255) NOT NULL,
                 `amount` varchar(255) DEFAULT NULL,
                 `account_number_id` varchar(255) DEFAULT NULL,
                 `transaction_type` varchar(255) DEFAULT NULL,
                 `chart_of_account_id` varchar(255) DEFAULT NULL,
                 `created_at` datetime DEFAULT NULL,
                 `updated_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
                 `deleted_at` datetime DEFAULT NULL,
                 PRIMARY KEY (`id`)
                ) ";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }
    public static function alterTenantDetailColumns($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `tenant_lease_details` LIKE 'tenant_status'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE `tenant_lease_details` ADD `tenant_status` INT NOT NULL DEFAULT '0' AFTER `record_status`, ADD `tenant_portal` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `tenant_status`";
            $connection->prepare($query)->execute();
        }
        catch(Exception $exception) {
            // print_r($exception);

        }
    }


    public static function alterCarrerTable($connection) {
        try {
            $ifExists = $connection->query("SHOW COLUMNS FROM `carrier` LIKE 'sms_gateway'")->fetchAll();
            if (!empty($ifExists)) {
                return;
            }
            $query = "ALTER TABLE carrier ADD sms_gateway varchar(255) DEFAULT NULL AFTER carrier";
            $connection->prepare($query)->execute();


        }  catch(Exception $exception) {
            // print_r($exception);
        }
    }
    public static function createCalendarTable($connection) {
        try {
            $table = "CREATE TABLE IF NOT EXISTS `calendar_schedule` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
             `subject` varchar(255) DEFAULT NULL,
             `location` varchar(255) DEFAULT NULL,
             `start_date` date DEFAULT NULL,
             `end_date` date DEFAULT NULL,
             `start_time` time DEFAULT NULL,
             `end_time` time DEFAULT NULL,
             `created_at` datetime DEFAULT NULL,
             `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
             `deleted_at` datetime DEFAULT NULL,
             PRIMARY KEY (`id`)
                ) ";
            $connection->prepare($table)->execute();
        }
        catch(Exception $exception) {
            print_r($exception->getMessage());
        }
    }

}

$ddl = new MigrationCusers();
