<?php

/**

 * DDL class used for DDL operations

 * Created by PhpStorm.

 * User: Parvesh

 * Date: 17-April-19

 */



include("$_SERVER[DOCUMENT_ROOT]/config.php");



if (basename($_SERVER['PHP_SELF']) == basename(__FILE__))

{

    $url = BASE_URL."login";

    header('Location: '.$url);

}



class SeederCusers extends DBConnection{

    //public $conn;

    function __construct()

    {

        parent::__construct();



    }



    /**

     *  Insert Data to Company Users

     */

    public static function runSeeders($connection, $runQuery=null)

    {

        $return = array();



        try {

            ini_set('max_execution_time', -1);



            self::insertCompanyPropertyAmenties($connection);
            self::insertCompanyPropertyType($connection);
            self::insertCompanyPropertyStyle($connection);
            self::insertCompanyUnitType($connection);
            self::insertcompany_vendor_type($connection);
            self::insertCompanyTimezone($connection);
            self::insertCompanyPropertySubType($connection);
            self::insertDefaultCurrency($connection);
            self::insertCompany_inventory_preferences($connection);
            self::insertCompanyAccountType($connection);
            self::insertCompanyReferralSource($connection);
            self::insertCompanyTenantEthnicity($connection);
            self::insertCompanyTenantMaritalStatus($connection);
            self::insertCompanyTenantHobby($connection);
            self::insertCompanyTenantVeteranStatus($connection);
            self::insertCompanyTenantCollectionReason($connection);
            self::insertCompanyCarrier($connection);
            self::insertCompanyPhoneType($connection);
            self::insertCompanyCredentialType($connection);
            self::insertCompanyCountry($connection);
            self::insertCompany_inventory_preferences($connection);

            self::insertCompanyUserRoles($connection);
            self::insertlist_of_reason($connection);
            self::insertCompany_maintenance_subcategory($connection);
            self::insertcompany_category_type($connection);
            self::insertcompany_priority_type($connection);
            self::insertcompany_severity_type($connection);
            self::insertcompany_workorder_type($connection);
            self::insertcompany_event_type($connection);
            self::insertCompanySubAccount($connection);
            self::insertCompanyCreditAccount($connection);
            self::insertCompanyDebitAccount($connection);
            self::insertCompanyChargeCode($connection);
            //self::insertChargeCodePreferences($connection);
            self::insertlist_of_inseptionName($connection);
            self::insertComplaintType($connection);
            self::insertHOAType($connection);
            self::insertCompanyPetFriendly($connection);
            self::insertCompanyGarageAvail($connection);
            self::insertCompanyKeyaccess($connection);
            self::insertCompanyChartOfAccounts($connection);
            self::insertCompanyAccountTypeDetails($connection);
            self::insertTagsEmailTemplateDetails($connection);

            self::insertEmailTemplatesAdmin($connection);

            self::insertReasonMove($connection);
            //useralerts
            self::insertUserAlerts($connection);
            self::insertTagsLettersNoticesDetails($connection);
            // self::insertVendorsAndOwners($connection);

            /*communication Template Docs*/
             self::insertletterNoticesTemplates($connection);
             self::insertPackageCarrier($connection);
             self::insertLocationStored($connection);

            self::insertCallerTypeDetails($connection);
             self::insertProblemCategoryTable($connection);
            self::insertReasonsTable($connection);

               self::insertmaintenancecolor($connection);
               self::insertmaintenancecategory($connection);
               self::insertmaintenanceage($connection);
               self::workorderstatus($connection);
               self::workordersource($connection);
              self::workorderrequestedby($connection);
            //  self::workordersource($connection);
            self::companyworkordercategory($connection);
            self::insertInTouchType($connection);
            self::insertMcc($connection);
            self::insertMarketingTemplatesAdmin($connection);
            self::calendargroups($connection);







//echo 'company user table created successfully';

            return array('code' => 200, 'status' => 'success','message' => 'company user table created successfully');

        }

        catch (PDOException $e) {

            die('nn');

            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }



        return $return;

    }



    public static function insertCompanyPropertyAmenties($connection)

    {

        try {

            $stm = $connection->prepare("TRUNCATE TABLE company_property_amenities");
            $stm->execute();

            $data = array(

                array('name' => 'Air conditioning', 'code' => '1', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Alarm System', 'code' => '2', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Attic fan', 'code' => '3', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Bachelor griller', 'code' => '4', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Back boiler', 'code' => '5', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Balcony', 'code' => '6', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Business Center', 'code' => '7', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Cable Ready', 'code' => '8', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Ceiling fan', 'code' => '9', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Clothes dryer', 'code' => '10', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Clubhouse', 'code' => '11', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Combo washer dryer', 'code' => '12', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Compactor', 'code' => '13', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Computer', 'code' => '14', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Convection heater', 'code' => '15', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Covered Parking', 'code' => '16', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Dehumidifier', 'code' => '17', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Dishwasher', 'code' => '18', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Door Attendant', 'code' => '19', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Electric water boiler', 'code' => '20', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Elevator', 'code' => '21', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Extra Storage', 'code' => '22', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Fire Place', 'code' => '23', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Fitness Center', 'code' => '24', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Fitness Equipment', 'code' => '25', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Freezer', 'code' => '26', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Furnished ', 'code' => '27', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Garbage disposal unit', 'code' => '28', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Garage', 'code' => '29', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Gating & Fences (Entrance Control)', 'code' => '30', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Hair dryer', 'code' => '31', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Hair iron', 'code' => '32', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Hardwood Floors', 'code' => '33', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Heated Parking', 'code' => '34', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Humidifier', 'code' => '35', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'HVAC', 'code' => '36', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Ice machine', 'code' => '37', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Laundry Facility', 'code' => '38', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Microwave oven', 'code' => '39', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Modem', 'code' => '40', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Oil heater', 'code' => '41', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Oven (Stove)', 'code' => '42', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Parking ', 'code' => '43', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Parking (Reserved)', 'code' => '44', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Patio heater', 'code' => '45', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Pet Friendly', 'code' => '46', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Play Ground', 'code' => '47', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Pool', 'code' => '48', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Printer', 'code' => '49', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Radiator (Heating)', 'code' => '50', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Rec Room', 'code' => '51', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Refrigerator', 'code' => '52', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Router', 'code' => '53', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Satellite', 'code' => '54', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Short Term Lease', 'code' => '55', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Smoke Free', 'code' => '56', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Street Parking', 'code' => '57', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Sump Pump', 'code' => '58', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Stove', 'code' => '59', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Tennis Court', 'code' => '60', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Toaster oven', 'code' => '61', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Utilities Included', 'code' => '62', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Walk-in Closets', 'code' => '63', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Washing machine', 'code' => '64', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Water heater', 'code' => '65', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Window fan', 'code' => '66', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Wireless Internet', 'code' => '67', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Other', 'code' => '68', 'is_editable' => '0', 'user_id' => 1, 'status' => '1', 'type' => '1,2,3', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );



            foreach ($data as $rows) {

                $query = "INSERT INTO  `company_property_amenities`( `name`, `code`,`is_editable`,`user_id`,`status`,`type`,`created_at`,`updated_at`) VALUES ('{$rows['name']}','{$rows['code']}','{$rows['is_editable']}','{$rows['user_id']}','{$rows['status']}','{$rows['type']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            };

            return array('status' => 'success', 'code'=> 200);

        } catch (Exception $exception) {

            echo '<pre>';

            print_r($exception->getMessage());

            echo '</pre>';

            die;

        }

    }

    public static function insertCompanyPropertyType($connection)

    {

        try{





            $data = array(

                array('property_type' => 'Affordable Housing', 'description' => '', 'is_editable' => '0', 'is_default' => '1', 'user_id' => 1,  'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_type' => 'Commercial', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1,  'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_type' => 'Industrial', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1,  'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_type' => 'Land', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1,  'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_type' => 'Other', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1,  'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_type' => 'Recreational', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_type' => 'Section 8 Property', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_type' => 'Senior Housing', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_type' => 'Short-Term Rental', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_type' => 'Storage Unit', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_type' => 'Student Housing', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_type' => 'Vacation Property', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_property_type");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `company_property_type`( `property_type`, `description`,`is_editable`,`is_default`,`user_id`,`status`,`created_at`,`updated_at`) VALUES ('{$rows['property_type']}','{$rows['description']}','{$rows['is_editable']}','{$rows['is_default']}','{$rows['user_id']}','{$rows['status']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);



        }catch (Exception $exception)

        {



        }

    }

    public static function insertlist_of_reason($connection)

    {

        try{





            $data = array(

                array('reason' => 'Installed', 'is_editable' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reason' => 'Damaged Inventory', 'is_editable' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reason' => 'Transferred to another property', 'is_editable' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );



            $stm = $connection->prepare("TRUNCATE TABLE list_of_reason");



            $stm->execute();



            foreach($data as $rows)

            {

                $query = "INSERT INTO `list_of_reason`( `reason`,`is_editable`,`created_at`,`updated_at`) VALUES ('{$rows['reason']}','{$rows['is_editable']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

            print_r("sfa");



        }catch (Exception $exception)

        {



        }

    }



    public static function insertCompany_maintenance_subcategory($connection)

    {

        try{





            $data = [];

            $data['Paint'] = array(

                array('category' => 'Paint', 'sub_category' => '', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Brushes', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Buckets', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Putty', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'knife', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Rollers', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Scraper', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $data['Plumbing Equipment'] = array(

                array('category' => 'Plumbing Equipment', 'sub_category' => '', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Bath faucet', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Kitchen faucet', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Pipe', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Plunger', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Sink parts', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Toilet parts', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $data['Tools: Power'] = array(

                array('category' => 'Tools: Power', 'sub_category' => '', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Circular saw', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Grinder', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Jig saw', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Nail gun', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Power drill', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Sander', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $data['Tools: Hand'] = array(

                array('category' => 'Tools: Hand', 'sub_category' => '', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Wire cutter', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Flashlight', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Measuring tape', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Chisel', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Hammer', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Hand saw', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Level', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Pliers', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Screw driver', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Square', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Utility knife', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Wrench', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );



            $data['saftey'] = array(

                array('category' => 'Safety', 'sub_category' => '', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Dust Mask', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Gloves', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Googles', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );



            $data['Heating / Cooling'] = array(

                array('category' => 'Heating / Cooling', 'sub_category' => '', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Filters', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $data['Hardware'] = array(

                array('category' => 'Hardware', 'sub_category' => '', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Cabinet hardware', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Door hardware', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Miscellaneous', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Nails / screws', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $data['Grounds'] = array(

                array('category' => 'Grounds', 'sub_category' => '', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Edger/trimmer', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Gardening tools', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Golf cart', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Lawn mower', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Leaf blower', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Lawn mower', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Rake', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Snow blower', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Snow shovel', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Sprayers', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Tree saw', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Wheelbarrow', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $data['General Supplies'] = array(

                array('category' => 'General Supplies', 'sub_category' => '', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Cleaning supplies', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Fire extinguisher', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Glue', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Light bulbs', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Paper products', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Paper towels', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Rags', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Tarps', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $data['General Equipment'] = array(

                array('category' => 'General Equipment', 'sub_category' => '', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Ladder: Extension', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Ladder: Step', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Hand Truck/Dolly', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $data['Electrical'] = array(

                array('category' => 'Electrical', 'sub_category' => '', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Light fixture', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Light switch', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Light switch cover', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Outlet', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Outlet cover', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Smoke Alarm', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Voltage tester', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Wire', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'co2 detector', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $data['Custodial'] = array(

                array('category' => 'Custodial', 'sub_category' => '', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Broom', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Bucket', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Carpet cleaner', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Floor buffer', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Hose', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Mop', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Vacuum cleaner', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Wet/dry vacuum', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );



            $data['Chemicals'] = array(

                array('category' => 'Chemicals', 'sub_category' => '', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Cleaning', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Lubricants', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Pesticides', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );



            $data['Appliances'] = array(

                array('category' => 'Appliances', 'sub_category' => '', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Bathroom fan/vent parts', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Dishwasher parts', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Dryer parts', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Kitchen fan parts', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Refrigerator parts', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Stove/Oven parts', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Washing machine parts', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('category' => '', 'sub_category' => 'Water heater parts', 'is_editable' => '0', 'parent_id' => '', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );





            $stm = $connection->prepare("TRUNCATE TABLE company_maintenance_subcategory");

            $stm->execute();

            foreach($data as $rows)

            {

                $parent_id = NULL;



                foreach ($rows as $key=>$value) {

                    $query = "INSERT INTO  `company_maintenance_subcategory`( `category`, `sub_category`, `is_editable`,`parent_id`,`created_at`,`updated_at`) VALUES ('{$value['category']}','{$value['sub_category']}','{$value['is_editable']}',NULLIF ('$parent_id',''),'{$value['created_at']}','{$value['updated_at']}')";

                    $stm = $connection->prepare($query);

                    $stm->execute();

                    if(!empty($value['category'])) $parent_id = $connection->lastInsertId();

                }

            }

            return array('status' => 'success', 'code'=> 200);



        }catch (Exception $exception)

        {

            print_r($exception);die;

        }

    }





    public static function insertCompanyPropertyStyle($connection)

    {

        try{

            $data = array(

                array('property_style' => '½', 'description' => '', 'is_editable' => '0', 'is_default' => '1', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_style' => '1.5 story', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_style' => '2 Story', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_style' => '2.5 story', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_style' => '3 level split', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_style' => '3 Story', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_style' => '4  level split', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1,  'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_style' => '5 level split', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1,  'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_style' => 'Back Split', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1,  'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_style' => 'Bi-Level', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_style' => 'Bungalow', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1,  'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_style' => 'Double wide mobile home', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_style' => 'Loft', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_style' => 'Multi-level apartment', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_style' => 'Other', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_style' => 'Penthouse', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_style' => 'Single level apartment', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_style' => 'Single wide mobile home', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_style' => 'Two story split', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_style' => 'Villa', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_property_style");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `company_property_style`( `property_style`, `description`,`is_editable`,`is_default`,`user_id`,`status`,`created_at`,`updated_at`) VALUES ('{$rows['property_style']}','{$rows['description']}','{$rows['is_editable']}','{$rows['is_default']}','{$rows['user_id']}','{$rows['status']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);



        }catch (Exception $exception)

        {

            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }

    public static function insertcompany_category_type($connection)

    {

        try{

            $data = array(

                array('user_id'=>1,'category' => 'Incident', 'description' => '', 'is_editable' => '1', 'is_default' => '1', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>1,'category' => 'Meter Reading', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>1,'category' => 'Monthly Maintenance', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>1,'category' => 'Preventative Maintenance', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>1,'category' => 'Other', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>1,'category' => 'Remove & Replace', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_category_type");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `company_category_type`( `category`, `description`,`is_editable`,`is_default`,`status`,`created_at`,`updated_at`,`user_id`) VALUES ('{$rows['category']}','{$rows['description']}','{$rows['is_editable']}','{$rows['is_default']}','{$rows['status']}','{$rows['created_at']}','{$rows['updated_at']}','{$rows['user_id']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);



        }catch (Exception $exception)

        {

            print_r($exception);

        }



    }

    public static function insertcompany_priority_type($connection)

    {

        try{

            $data = array(

                array('priority' => 'High', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('priority' => 'Low', 'description' => 'Low', 'is_editable' => '1', 'is_default' => '1', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('priority' => 'Normal', 'description' => 'Normal', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('priority' => 'Highest', 'description' => 'Highest', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_priority_type");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `company_priority_type`( `priority`, `description`,`is_editable`,`is_default`,`status`,`created_at`,`updated_at`) VALUES ('{$rows['priority']}','{$rows['description']}','{$rows['is_editable']}','{$rows['is_default']}','{$rows['status']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);



        }catch (Exception $exception)

        {



        }



    }

    public static function companyworkordercategory($connection){
        try{
            $data = array(
                array('user_id' => 1,'category'=>'Delete','description'=>'Delete','status' => '1','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1'),
                array('user_id' => 1,'category'=>'General Inquiry','description'=>'General Inquiry','status' => '1','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1'),
                array('user_id' => 1,'category'=>'Incident','description'=>'Incident','status' => '1','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1'),
                array('user_id' => 1,'category'=>'Inspection','description'=>'Inspection','status' => '1','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1'),
                array('user_id' => 1,'category'=>'Meter Reading','description'=>'Meter Reading','status' => '1','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1'),
                array('user_id' => 1,'category'=>'Monthly Maintenance','description'=>'Monthly Maintenance','status' => '1','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1'),
                array('user_id' => 1,'category'=>'Other','description'=>'Other','status' => '1','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1'),
                array('user_id' => 1,'category'=>'Preventative Maintenance','description'=>'Preventative Maintenance','status' => '1','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1'),
                array('user_id' => 1,'category'=>'Remove & Replace','description'=>'Remove & Replace','status' => '1','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1'),
                array('user_id' => 1,'category'=>'Service Request','description'=>'Service Request','status' => '1','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1'),
                array('user_id' => 1,'category'=>'Urgent','description'=>'Urgent','status' => '1','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1'),
                array('user_id' => 1,'category'=>'Violation','description'=>'Violation','status' => '1','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1'),


            );

            $stm = $connection->prepare("TRUNCATE TABLE company_workorder_category");
            $stm->execute();
            foreach($data as $rows) {
                $query = "INSERT INTO `company_workorder_category`( `user_id`,`category`,`status`,`created_at`,`updated_at`,`is_editable`) VALUES ('{$rows['user_id']}','{$rows['category']}','{$rows['status']}','{$rows['created_at']}','{$rows['updated_at']}','{$rows['is_editable']}')";
//                print_r($query);die;
                $stm = $connection->prepare($query);
                $stm->execute();
            }
            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception) {
            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;
        }
    }

    public static function insertcompany_severity_type($connection)

    {

        try{

            $data = array(

                array('severity' => 'Low', 'description' => 'Low', 'is_editable' => '1', 'is_default' => '1', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('severity' => 'Cosmetic', 'description' => 'Cosmetic', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('severity' => 'Other', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('severity' => 'Medium', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('severity' => 'High', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('severity' => 'Critical', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_severity_type");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `company_severity_type`( `severity`, `description`,`is_editable`,`is_default`,`status`,`created_at`,`updated_at`) VALUES ('{$rows['severity']}','{$rows['description']}','{$rows['is_editable']}','{$rows['is_default']}','{$rows['status']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);



        }catch (Exception $exception)

        {



        }



    }

    public static function insertcompany_workorder_type($connection)

    {

        try{

            $data = array(

                array('workorder' => 'Inspection', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('workorder' => 'Cosmetic', 'description' => '', 'is_editable' => '1', 'is_default' => '1', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('workorder' => 'Incident', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('workorder' => 'Other', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('workorder' => 'Violation', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('workorder' => 'Urgent', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('workorder' => 'Monthly Maintenance', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('workorder' => 'Remove & Replace', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('workorder' => 'Meter Reading', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('workorder' => 'Service Request', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_workorder_type");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `company_workorder_type`( `workorder`, `description`,`is_editable`,`is_default`,`status`,`created_at`,`updated_at`) VALUES ('{$rows['workorder']}','{$rows['description']}','{$rows['is_editable']}','{$rows['is_default']}','{$rows['status']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);



        }catch (Exception $exception)

        {



        }



    }

    public static function insertcompany_vendor_type($connection)

    {

        try{

            $data = array(
                //new
                array('vendor_type' => 'Contractor-Architect', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-Carpenter', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-Dry Wall', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor –Electrical', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-Flooring', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-Food Services', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-Gardener', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-General', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-HVAC', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-Interior Design', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-Inspections', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-Landscaping', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-Masonry', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-Panting', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-Paving', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor- Pest Control', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-Plumbing', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-Printing', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-Restoration Services', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-Roofing', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-Safety & Security', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-Trash Collection', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Contractor-Windows', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Professional-Accounting', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Professional-Legal', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Professional-Realtor', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Supplies-Electrical', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Supplies-General', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Supplies-office', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Supplies-Plumbing', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Supplies-Software & Services', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Tax-Federal', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Tax- State', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Tax-Local', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Utilities-Gas & Electric', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Utilities-Insurance', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Utilities-internet & Phone', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Utilities-Water & Sewer', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('vendor_type' => 'Other', 'description' => 'sddsamnm', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),


            );

            $stm = $connection->prepare("TRUNCATE TABLE company_vendor_type");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `company_vendor_type`( `vendor_type`, `description`,`is_editable`,`is_default`,`status`,`created_at`,`updated_at`) VALUES ('{$rows['vendor_type']}','{$rows['description']}','{$rows['is_editable']}','{$rows['is_default']}','{$rows['status']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);



        }catch (Exception $exception)

        {



        }



    }


    public static function insertcompany_event_type($connection)

    {

        try{

            $data = array(

                array('user_id'=>1,'event_type' => 'End of Year Party', 'description' => '', 'is_editable' => '1', 'is_default' => '1', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>1,'event_type' => 'Site Visit', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>1,'event_type' => 'Showing', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>1,'event_type' => 'Summer Party', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>1,'event_type' => 'Office Team Building', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>1,'event_type' => 'Community Socials', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>1,'event_type' => 'Charitable Event', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>1,'event_type' => 'Family Event', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>1,'event_type' => 'Open House', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>1,'event_type' => 'Neighborhood Event', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>1,'event_type' => 'Wedding', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>1,'event_type' => 'Executive Retreat', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>1,'event_type' => 'Shareholder Meetings', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>1,'event_type' => 'Trade Fair', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>1,'event_type' => 'VIP Event', 'description' => '', 'is_editable' => '1', 'is_default' => '0', 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_event_type");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `company_event_type`( `event_type`, `description`,`is_editable`,`is_default`,`status`,`created_at`,`updated_at`,`user_id`) VALUES ('{$rows['event_type']}','{$rows['description']}','{$rows['is_editable']}','{$rows['is_default']}','{$rows['status']}','{$rows['created_at']}','{$rows['updated_at']}','{$rows['user_id']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);



        }catch (Exception $exception)

        {

            print_r($exception);

        }



    }

    public static function insertCompanyUnitType($connection)

    {

        try{



            $data = array(

                array('unit_type' => 'Assigned Parking(Indoor)', 'description' => '', 'is_editable' => '0', 'is_default' => '1', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('unit_type' => 'Assigned Parking(outdoor)', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('unit_type' => 'Bed Room', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('unit_type' => 'Duplex', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('unit_type' => 'Efficiency', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('unit_type' => 'Loft', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('unit_type' => 'Mini Storage', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('unit_type' => 'Multiplex', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('unit_type' => 'Other', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('unit_type' => 'Restaurant', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('unit_type' => 'Retail', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('unit_type' => 'Studio', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('unit_type' => 'Suite', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('unit_type' => 'Warehouse', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_unit_type");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `company_unit_type`( `unit_type`, `description`,`is_editable`,`is_default`,`user_id`,`status`,`created_at`,`updated_at`) VALUES ('{$rows['unit_type']}','{$rows['description']}','{$rows['is_editable']}','{$rows['is_default']}','{$rows['user_id']}','{$rows['status']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            //echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyTimezone($connection)

    {

        try{

            $data = array(

                array("name" => "International Date Line West" , "hours_calc"=>'sub',"hours_gap"=>'12',"code" => "(UTC-12:00)", "timezone" => "", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Coordinated Universal Time -11" ,  "hours_calc"=>'sub',"hours_gap"=>'11', "code" => "(UTC-11:00)", "timezone" => "", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Hawaii" ,   "hours_calc"=>'sub',"hours_gap"=>'10',"code" => "(UTC-10:00)", "timezone" => "US/Hawaii", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Alaska" ,  "hours_calc"=>'sub',"hours_gap"=>'09', "code" => "(UTC-09:00)", "timezone" => "America/Anchorage","created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Pacific Time (US and Canada)" ,  "hours_calc"=>'sub',"hours_gap"=>'08', "code" => "(UTC-08:00)", "timezone" => "America/Los_Angeles", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Baja California" ,  "hours_calc"=>'sub',"hours_gap"=>'08', "code" => "(UTC-08:00)", "timezone" => " America/Tijuana", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Mountain Time (US and Canada)" , "hours_calc"=>'sub',"hours_gap"=>'07', "code" => "(UTC-07:00)", "timezone" => "US/Mountain", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Chihuahua, La Paz, Mazatlan" ,  "hours_calc"=>'sub',"hours_gap"=>'07', "code" => "(UTC-07:00)", "timezone" => "America/Chihuahua", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Arizona" ,  "hours_calc"=>'sub',"hours_gap"=>'07',"code" => "(UTC-07:00)", "timezone" => "US/Arizona", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Saskatchewan" ,  "hours_calc"=>'sub',"hours_gap"=>'06',"code" => "(UTC-06:00)", "timezone" => "Canada/Saskatchewan", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Central America" ,  "hours_calc"=>'sub',"hours_gap"=>'06',"code" => "(UTC-06:00)", "timezone" => "America/Chicago", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Central Time (US and Canada)" , "hours_calc"=>'sub',"hours_gap"=>'06', "code" => "(UTC-06:00)", "timezone" => "US/Central", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Guadalajara, Mexico City, Monterrey" ,  "hours_calc"=>'sub',"hours_gap"=>'06',"code" => "(UTC-06:00)", "timezone" => "America/Mexico_City", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Eastern Time (US and Canada)" ,  "hours_calc"=>'sub',"hours_gap"=>'05',"code" => "(UTC-05:00)", "timezone" => "US/Eastern", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Bogota, Lima, Quito" ,  "hours_calc"=>'sub',"hours_gap"=>'05',"code" => "(UTC-05:00)", "timezone" => "America/Bogota", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Indiana (East)" ,  "hours_calc"=>'sub',"hours_gap"=>'05',"code" => "(UTC-05:00)", "timezone" => "US/East-Indiana", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Caracas" ,  "hours_calc"=>'sub',"hours_gap"=>'4.50',"code" => "(UTC-04:30)", "timezone" => "America/Caracas", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Atlantic Time (Canada)" ,  "hours_calc"=>'sub',"hours_gap"=>'04',"code" => "(UTC-04:00)", "timezone" => "Canada/Atlantic", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Cuiaba" ,  "hours_calc"=>'sub',"hours_gap"=>'04:00',"code" => "(UTC-04:00)", "timezone" => "America/Cuiaba", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Santiago" ,  "hours_calc"=>'sub',"hours_gap"=>'04:00',"code" => "(UTC-04:00)", "timezone" => "America/Santiago", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Georgetown, La Paz, Manaus, San Juan" ,  "hours_calc"=>'sub',"hours_gap"=>'04:00',"code" => "(UTC-04:00)", "timezone" => "America/Guyana", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Asuncion" ,  "hours_calc"=>'sub',"hours_gap"=>'04:00',"code" => "(UTC-04:00)", "timezone" => "America/Asuncion", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Newfoundland" ,  "hours_calc"=>'sub',"hours_gap"=>'03.50',"code" => "(UTC-03:30)", "timezone" => "Canada/Newfoundland", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Brasilia" ,  "hours_calc"=>'sub',"hours_gap"=>'03',"code" => "(UTC-03:00)", "timezone" => "America/Sao_Paulo", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Greenland" ,  "hours_calc"=>'sub',"hours_gap"=>'03',"code" => "(UTC-03:00)", "timezone" => "America/Godthab", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Montevideo" ,  "hours_calc"=>'sub',"hours_gap"=>'03',"code" => "(UTC-03:00)", "timezone" => "America/Montevideo", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Cayenne, Fortaleza" ,  "hours_calc"=>'sub',"hours_gap"=>'03',"code" => "(UTC-03:00)", "timezone" => "America/Cayenne", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Buenos Aires" ,  "hours_calc"=>'sub',"hours_gap"=>'03',"code" => "(UTC-03:00)", "timezone" => "America/Buenos_Aires", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Mid-Atlantic" ,  "hours_calc"=>'sub',"hours_gap"=>'02',"code" => "(UTC-02:00)", "timezone" => "America/Noronha", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Coordinated Universal Time -02" , "hours_calc"=>'sub',"hours_gap"=>'02', "code"=> "(UTC-02:00)", "timezone" => "", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Azores" ,  "hours_calc"=>'sub',"hours_gap"=>'01', "code" => "(UTC-01:00)", "timezone" => "Atlantic/Azores", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Cabo Verde Is." ,  "hours_calc"=>'sub',"hours_gap"=>'01',"code" => "(UTC-01:00)", "timezone" => "America/Adak", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Dublin, Edinburgh, Lisbon, London" ,  "hours_calc"=>'equal',"hours_gap"=>'00',"code" => "(UTC)", "timezone" => "Europe/Dublin", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Monrovia, Reykjavik" ,  "hours_calc"=>'equal',"hours_gap"=>'00',"code" => "(UTC)","timezone" => "Africa/Monrovia","created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Casablanca" ,  "hours_calc"=>'equal',"hours_gap"=>'00',"code" => "(UTC)", "timezone" => "Africa/Casablanca", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Coordinated Universal Time" ,  "hours_calc"=>'equal',"hours_gap"=>'00',"code" => "(UTC)", "timezone" => "America/Adak", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Belgrade, Bratislava, Budapest, Ljubljana, Prague" ,  "hours_calc"=>'add',"hours_gap"=>'01', "code" => "(UTC+01:00)", "timezone" => "Europe/Belgrade", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Sarajevo, Skopje, Warsaw, Zagreb" ,  "hours_calc"=>'add',"hours_gap"=>'01',"code" => "(UTC+01:00)", "timezone" => "Europe/Sarajevo", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Brussels, Copenhagen, Madrid, Paris" ,  "hours_calc"=>'add',"hours_gap"=>'01',"code" => "(UTC+01:00)","timezone" => "Europe/Paris", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "West Central Africa" ,  "hours_calc"=>'sub',"hours_gap"=>'01',"code" => "(UTC+01:00)", "timezone" => "Africa/Lagos", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna" ,  "hours_calc"=>'add',"hours_gap"=>'01',"code" => "(UTC+01:00)", "timezone" => "Europe/Amsterdam", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Windhoek" ,  "hours_calc"=>'add',"hours_gap"=>'01', "code" => "(UTC+01:00)", "timezone" => "Africa/Windhoek", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Minsk" ,  "hours_calc"=>'add',"hours_gap"=>'02',"code" => "(UTC+02:00)", "timezone" => "Europe/Minsk", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Cairo" ,  "hours_calc"=>'add',"hours_gap"=>'02',"code" => "(UTC+02:00)", "timezone" => "Africa/Cairo", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius" ,  "hours_calc"=>'add',"hours_gap"=>'02', "code" => "(UTC+02:00)", "timezone" => "Europe/Helsinki", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Athens, Bucharest" ,  "hours_calc"=>'add',"hours_gap"=>'02',"code" => "(UTC+02:00)", "timezone" => "Europe/Athens", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Jerusalem" ,  "hours_calc"=>'add',"hours_gap"=>'02',"code" => "(UTC+02:00)", "timezone" => "Asia/Jerusalem", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Amman" ,  "hours_calc"=>'add',"hours_gap"=>'02',"code" => "(UTC+02:00)", "timezone" => "Asia/Amman", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Beirut" ,  "hours_calc"=>'add',"hours_gap"=>'02',"code" => "(UTC+02:00)", "timezone" => "Asia/Beirut", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Harare, Pretoria" ,  "hours_calc"=>'add',"hours_gap"=>'02',"code" => "(UTC+02:00)", "timezone" => "Africa/Harare", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Damascus" ,  "hours_calc"=>'add',"hours_gap"=>'02',"code" => "(UTC+02:00)", "timezone" => "Asia/Damascus", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Istanbul" ,  "hours_calc"=>'add',"hours_gap"=>'02',"code" => "(UTC+02:00)", "timezone" => "Europe/Istanbul", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Kuwait, Riyadh" ,  "hours_calc"=>'add',"hours_gap"=>'03',"code" => "(UTC+03:00)", "timezone" => "Asia/Kuwait", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Baghdad" ,  "hours_calc"=>'add',"hours_gap"=>'03',"code" => "(UTC+03:00)", "timezone" => "Asia/Baghdad", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Nairobi" ,  "hours_calc"=>'add',"hours_gap"=>'03',"code" => "(UTC+03:00)", "timezone" => "Africa/Nairobi", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Kaliningrad" ,  "hours_calc"=>'add',"hours_gap"=>'03',"code" => "(UTC+03:00)", "timezone" => "America/Adak", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Tehran" ,  "hours_calc"=>'add',"hours_gap"=>'03.50',"code" => "(UTC+03:30)", "timezone" => "Asia/Tehran", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Moscow, St. Petersburg, Volgograd" ,  "hours_calc"=>'add',"hours_gap"=>'04', "code" => "(UTC+04:00)", "timezone" => "Europe/Moscow", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Abu Dhabi, Muscat" , "hours_calc"=>'add',"hours_gap"=>'04', "code" => "(UTC+04:00)", "timezone" => "Asia/Muscat", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Baku" ,  "hours_calc"=>'add',"hours_gap"=>'04',"code" => "(UTC+04:00)", "created_at" => date('Y-m-d H:i:s'), "timezone" => "Asia/Tbilisi", "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Yerevan" ,  "hours_calc"=>'add',"hours_gap"=>'04',"code" => "(UTC+04:00)", "timezone" => "Asia/Tbilisi", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Tbilisi" ,  "hours_calc"=>'add',"hours_gap"=>'04',"code" => "(UTC+04:00)", "timezone" => "Asia/Tbilisi", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Port Louis" ,  "hours_calc"=>'add',"hours_gap"=>'04',"code" => "(UTC+04:00)", "timezone" => "Indian/Mauritius", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Kabul" ,  "hours_calc"=>'add',"hours_gap"=>'04.50',"code" => "(UTC+04:30)", "timezone" => "Asia/Kabul", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Tashkent" ,  "hours_calc"=>'add',"hours_gap"=>'05',"code" => "(UTC+05:00)", "timezone" => "Asia/Tashkent", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Islamabad, Karachi" ,  "hours_calc"=>'add',"hours_gap"=>'05', "code" => "(UTC+05:00)", "timezone" => "Asia/Karachi", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Chennai, Kolkata, Mumbai, New Delhi" ,  "hours_calc"=>'add',"hours_gap"=>'05.50', "code" => "(UTC+05:30)", "timezone" => "Asia/Calcutta", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Sri Jayawardenepura" ,  "hours_calc"=>'add',"hours_gap"=>'12',"code" => "(UTC+05:30)", "timezone" => "Asia/Colombo", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Kathmandu" ,  "hours_calc"=>'add',"hours_gap"=>'05.45',"code" => "(UTC+05:45)", "timezone" => "Asia/Kathmandu", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Ekaterinburg" ,  "hours_calc"=>'add',"hours_gap"=>'06',"code" => "(UTC+06:00)", "timezone" => "Asia/Yekaterinburg", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Astana" ,  "hours_calc"=>'add',"hours_gap"=>'06',"code" => "(UTC+06:00)", "timezone" => "Asia/Dhaka", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Dhaka" ,  "hours_calc"=>'add',"hours_gap"=>'06',"code" => "(UTC+06:00)", "timezone" => "Asia/Dhaka", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Yangon (Rangoon)" ,  "hours_calc"=>'add',"hours_gap"=>'06.50',"code" => "(UTC+06:30)", "timezone" => "Asia/Rangoon", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Novosibirsk" ,  "hours_calc"=>'add',"hours_gap"=>'07',"code" => "(UTC+07:00)", "timezone" => "Asia/Novosibirsk", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Bangkok, Hanoi, Jakarta" ,  "hours_calc"=>'add',"hours_gap"=>'07',"code" => "(UTC+07:00)", "timezone" => "Asia/Bangkok", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Krasnoyarsk" ,  "hours_calc"=>'add',"hours_gap"=>'08',"code" => "(UTC+08:00)", "timezone" => "Asia/Krasnoyarsk", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Beijing, Chongqing, Hong Kong, Urumqi" ,  "hours_calc"=>'add',"hours_gap"=>'08',"code" => "(UTC+08:00)", "timezone" => "Asia/Hong_Kong", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Kuala Lumpur, Singapore" ,  "hours_calc"=>'add',"hours_gap"=>'08', "code" => "(UTC+08:00)", "timezone" => "Asia/Singapore", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Taipei" ,   "hours_calc"=>'add',"hours_gap"=>'08',"code" => "(UTC+08:00)", "timezone" => "Asia/Taipei", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Perth" ,  "hours_calc"=>'add',"hours_gap"=>'08',"code" => "(UTC+08:00)", "timezone" => "Australia/Perth", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Ulaanbaatar" ,  "hours_calc"=>'add',"hours_gap"=>'08',"code" => "(UTC+08:00)", "timezone" => "Asia/Ulaanbaatar", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Irkutsk" ,  "hours_calc"=>'add',"hours_gap"=>'09',"code" => "(UTC+09:00)", "timezone" => "Asia/Irkutsk", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Seoul" ,  "hours_calc"=>'add',"hours_gap"=>'09', "code" => "(UTC+09:00)", "timezone" => "Asia/Seoul", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Osaka, Sapporo, Tokyo" ,  "hours_calc"=>'add',"hours_gap"=>'09',"code" => "(UTC+09:00)", "timezone" => "Asia/Tokyo", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Darwin" ,  "hours_calc"=>'add',"hours_gap"=>'09.50',"code" => "(UTC+09:30)", "timezone" => "Australia/Darwin", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Adelaide" ,  "hours_calc"=>'add',"hours_gap"=>'09.50',"code" => "(UTC+09:30)", "timezone" => "Australia/Adelaide", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Yakutsk" ,  "hours_calc"=>'add',"hours_gap"=>'10',"code" => "(UTC+10:00)", "timezone" => "Asia/Yakutsk", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Canberra, Melbourne, Sydney" ,  "hours_calc"=>'add',"hours_gap"=>'10',"code" => "(UTC+10:00)", "timezone" => "Australia/Canberra", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Brisbane" ,  "hours_calc"=>'add',"hours_gap"=>'10',"code" => "(UTC+10:00)", "timezone" => "Australia/Brisbane", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Hobart" ,  "hours_calc"=>'add',"hours_gap"=>'10',"code" => "(UTC+10 :00)", "timezone" => "Australia/Hobart", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Guam, Port Moresby" ,  "hours_calc"=>'add',"hours_gap"=>'10',"code" => "(UTC+10:00)", "timezone" => "Pacific/Guam", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Vladivostok" ,  "hours_calc"=>'add',"hours_gap"=>'11', "code" => "(UTC+11:00)", "timezone" => "Asia/Vladivostok", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Solomon Is., New Caledonia" ,  "hours_calc"=>'add',"hours_gap"=>'11',"code" => "(UTC+11:00)", "timezone" => "Asia/Magadan", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Magadan" ,  "hours_calc"=>'add',"hours_gap"=>'12',"code" => "(UTC+12:00)", "timezone" => "Asia/Magadan", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Fiji" ,  "hours_calc"=>'add',"hours_gap"=>'12',"code" => "(UTC+12:00)", "timezone" => "Pacific/Fiji", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Auckland, Wellington" ,  "hours_calc"=>'add',"hours_gap"=>'12',"code" => "(UTC+12:00)", "timezone" => "Pacific/Auckland", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Coordinated Universal Time +12" ,  "hours_calc"=>'add',"hours_gap"=>'12', "code" => "(UTC+12:00)", "timezone" => "", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Nuku alofa" ,  "hours_calc"=>'add',"hours_gap"=>'13',"code" => "(UTC+13:00)", "timezone" => "Pacific/Tongatapu", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

                array("name" => "Samoa" , "hours_calc"=>'add',"hours_gap"=>'11', "code" => "(UTC-11:00)", "timezone" => "US/Samoa", "created_at" => date('Y-m-d H:i:s'), "updated_at" => date('Y-m-d H:i:s')),

            );



            $stm = $connection->prepare("TRUNCATE TABLE timezone");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `timezone`( `name`, `hours_calc`,`hours_gap`,`code`,`timezone`,`created_at`,`updated_at`) VALUES ('{$rows['name']}','{$rows['hours_calc']}','{$rows['hours_gap']}','{$rows['code']}','{$rows['timezone']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }



            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }

    public static function insertCompanyPropertySubType($connection)

    {

        try{

            $data = array(

                array('property_subtype' => 'Apartment Building', 'description' => '', 'is_editable' => '0', 'is_default' => '1', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Commercial Building ', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Condominium', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Duplex', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Farm/Ranch', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Flat', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Fourplex', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Industrial Building', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Land-Improved', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Land-Unimproved', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Manufacturing', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Marina', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Medical Office', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Mini Storage', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Military-Government', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Mobile Home', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Mobile Home Park', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Multi Family Home', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Office', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Office Building-Class A', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Office Building-Class B', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Parking-Assigned', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Parking-Unassigned', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Parking Management', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Retail Shopping Center', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Single Family Home', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Timeshare', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Triplex', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Student Housing', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Town House', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('property_subtype' => 'Other', 'description' => '', 'is_editable' => '0', 'is_default' => '0', 'user_id' => 1, 'status' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_property_subtype");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `company_property_subtype`( `property_subtype`, `description`,`is_editable`,`is_default`,`user_id`,`status`,`created_at`,`updated_at`) VALUES ('{$rows['property_subtype']}','{$rows['description']}','{$rows['is_editable']}','{$rows['is_default']}','{$rows['user_id']}','{$rows['status']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            // echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertDefaultCurrency($connection)

    {

        try{



            $data = "INSERT INTO `default_currency` (`id`, `currency`, `symbol`, `created_at`, `updated_at`, `deleted_at`) VALUES

                (1, 'Afghan afghani', 'AFN', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (2, 'Algerian Dinar', 'دج', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (3, 'Angolan Kwanza', 'Kz', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (4, 'Argentine Peso', '$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (5, 'Argentinean Peso', 'AR$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (6, 'Armenian Dram', 'դր', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (7, 'Aruban Florin', 'ƒ', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (8, 'Australian Dollar', '$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (9, 'Azerbaijani Manat', 'դր', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (10, 'Bahamian Dollar', 'BS$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (11, 'Bahrain Dinar', 'BD', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (12, 'Bahraini Dinar', 'ب.د', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (13, 'Baht', '฿', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (14, 'Barbadian Dollar', 'Bds$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (15, 'Barbadian Dollar', '$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (16, 'Belarusian Ruble', 'Br', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (17, 'Belize Dollar', 'BZ$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (18, 'Boliviano', 'Bs', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (19, 'Bosnia-H Convertible Mark', 'KM', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (20, 'Brazil Real', 'R$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (21, 'Brazilian Real', 'R$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (22, 'British Pound', '£', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (23, 'Brunei Dollar', 'B$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (24, 'Bulgarian Lev', 'лв', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (25, 'Burma', 'K', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (26, 'Burundi Franc', 'FBu', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (27, 'Cabo Verde Escudo', 'CV$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (28, 'Canadian Dollar', '$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (29, 'Cayman Island Dollar', 'CI$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (30, 'CFA France BCEO', 'franc', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (31, 'CFA France BEAC', 'franc', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (32, 'CFP Franc', 'CFP', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (33, 'Chilean Peso', '$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (34, 'Chinese Yuan', '¥', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (35, 'Colombian Peso', 'Col$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (36, 'Comoro Franc', 'KMF', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (37, 'Cordoba Oro', 'C$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (38, 'Costa Rican Colon', '₡', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (39, 'Croatian Kuna', 'HRK', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (40, 'Cuban Peso', 'CU$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (41, 'Cyprus Pound', 'CY£', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (42, 'Czech Koruna', 'Kč', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (43, 'Dalasi', 'D', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (44, 'Danish Krone', 'ø', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (45, 'Danish Krone', 'kr', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (46, 'Dinar', 'MKD', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (47, 'Djibouti Franc', 'Fdj', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (48, 'Dobra', 'Dd', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (49, 'Dominican Peso', 'RD$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (50, 'Dong', '₫', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (51, 'East Caribbean Dollar', 'EC$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (52, 'East Caribbean Dollar', '$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (53, 'Egypt Pound', 'EGP', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (54, 'Egyptian Pound', 'ج.م', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (55, 'Estonian Kroon', 'kr', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (56, 'Ethiopian Birr', 'Br', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (57, 'Euro', '€', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (58, 'European Euro', '€', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (59, 'Falkland Island Pound', 'FK£', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (60, 'Fiji Dollar', 'FJ$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (61, 'Franc Congolais', 'CDFr', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (62, 'Ghana Cedi', '₵', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (63, 'Gibraltar Pound', 'GI£', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (64, 'Gourde', 'G', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (65, 'Guarani', '₲', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (66, 'Guinea Franc', 'FG', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (67, 'Guinea-Bissau peso', 'franc', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (68, 'Guyana Dollar', 'GY$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (69, 'Hong Kong Dollar', '元', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (70, 'Hryvnia', '₴', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (71, 'Hungarian Forint', 'Ft', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (72, 'Icelandic Krona', 'kr', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (73, 'Indian Rupee', 'rs', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (74, 'Indonesian Rupiah', 'Rp', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (75, 'Iranian Rial', 'يال', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (76, 'Israel shekel', '₪', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (77, 'Jamaican Dollar', 'J$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (78, 'Japanese Yen', '¥', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (79, 'Jordanian Dinar', 'JD', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (80, 'Kenyan Shelling', 'KSh', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (81, 'Kina', 'K', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (82, 'Kip', 'K', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (83, 'Kuwaiti Dinar', 'د.ك', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (84, 'Kwacha', 'MK', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (85, 'Lari', 'GEL', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (86, 'Latvian Lats', 'Ls', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (87, 'Lebanese Pound', '£', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),              

                (89, 'Lempira', 'L', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (90, 'Liberian Dollar', 'L$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (91, 'Libyan Dinar', 'ل.د', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (92, 'Lilangeni', 'SZL', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (93, 'Lithuanian Litas', 'Lt', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (94, 'Loti', 'L', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (95, 'Malagasy Ariary', 'MGAa', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (96, 'Malaysia Ringgit', 'RM', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (97, 'Malaysian Ringgit', 'RM', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (98, 'Mauritius Rupee', 'MURs', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (99, 'Mexican Peso', '$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (100, 'Mexico Peso', '$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (101, 'Moldovan Leu', 'MDL', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (102, 'Moroccan Dirham', 'د.م', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (103, 'Mozambique Metical', 'MTn', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (104, 'Naira', '₦', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (105, 'Nakfa', 'Nfk', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (106, 'Namibian Dollar', '$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (107, 'Namibian Dollar', 'N$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (108, 'Napalese Rupee', 'Rs', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (109, 'Netherlands', 'AG', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (110, 'Netherlands Antillean Guilder', 'NAƒ', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (111, 'New Israeli Sheqel', '₪', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (112, 'New Romanian Leu', 'le', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (113, 'New Zealand Dollar', '$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (114, 'New Zealand Dollar', 'NZ$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (115, 'Ngultrum', 'Nu', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (116, 'North Korean Won', '₩', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (117, 'Norwegian Krone', 'øre', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (118, 'Norwegian Krone', 'kr', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (119, 'Norwegian Krone', 'רre', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (120, 'Nuevo Sol', 'S/.', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (121, 'Omani Rial', 'ر.ع', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (122, 'Ouguiya', 'UM', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (123, 'Pakistan Rupee', 'PKRs', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (124, 'Panamanian Balboa', 'B/.', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (125, 'Pataca', 'MO$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (126, 'Peso Uruguayo', 'UR$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (127, 'Philippine Peso', 'P', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (128, 'Philippine Peso', 'Ph', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (129, 'Polish Zloty', 'z³', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (130, 'Pound Starling', 'UK£', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (131, 'Pula', 'P', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (132, 'Qatari Rial', 'ر.ق', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (133, 'Quetzal', 'Q', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (134, 'Riel', '៛', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (135, 'Romanian Rouble', 'le', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (136, 'Rufiyaa', 'MRf', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (137, 'Russian Ruble', 'p.', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (138, 'Russian Ruble', 'RUруб', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (139, 'Rwanda Franc', 'RF', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (140, 'Saudi Riyal', '﷼', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (141, 'Serbian Dinar', 'הטם', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (142, 'Seychelles Rupee', 'SRe', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (143, 'Sierra Leone', 'Le', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (144, 'Singapore Dollar', '$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (145, 'Sir Lankan Rupee', 'SKRs', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (146, 'Solomon Island Dollar', 'SI$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (147, 'Som', 'KGS', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (148, 'Somali Shelling', 'Sh', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (149, 'Somoni', 'TJS', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (150, 'South African Rand', 'SAR', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (151, 'South Korean Won', '₩', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (152, 'St. Helena Pound', 'SH£', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (153, 'Sudanese Pound', '£Sd', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (154, 'Surinam Dollar', 'SR$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (155, 'Sweden Krona', 'kr', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),
              
                (157, 'Swiss Franc', 'CHF', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (158, 'Switzerland Franc', 'CHF', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (159, 'Syrian Pound', 'S£', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (160, 'Taiwanese  Dollar', '$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (161, 'Taiwanese Dollar', 'NY$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (162, 'Taka', '৳', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (163, 'Tala', 'WS$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (164, 'Tanzanian Shelling', 'TSh', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (165, 'Tenge', 'KZT', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (166, 'Thai Baht', '฿', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (167, 'Trinidad & Tobago Dollar', 'TT$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (168, 'Tugrik', '₮', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (169, 'Tunisian Dinar', 'د.ت', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (170, 'Turkish Lira', 'TRS', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (171, 'Turkmenistan New Manat', 'm', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (172, 'UAE Dirham', 'د.إ', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (173, 'Uganda Shelling', 'USh', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (174, 'US Dollar', '$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (175, 'USA/Canadian dollar', '$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (176, 'Uzbekistan Sum', 'UZS', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (177, 'Vatu', 'VT', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (178, 'Venezuelan Bolivar', 'Bs', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (180, 'Yemeni Rial', 'YER', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (181, 'Yuan Renminbi', 'CN¥', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (182, 'Zambian Kwacha', 'ZK', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (183, 'Zimbabwe Dollar', 'Z$', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL),

                (184, 'Zloty', 'zł', '2019-01-17 11:04:52', '2019-01-17 11:04:52', NULL)";



            $stm = $connection->prepare("TRUNCATE TABLE default_currency");

            $stm->execute();



            $stm = $connection->prepare($data);

            $stm->execute();

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            // echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompany_inventory_preferences($connection)

    {

        try{

            $data = array(

                array('user_id' => '1', 'inventory_item' => '0', 'add_suppliers' => '0', 'upload_photos' => '0', 'updated_at' => date('Y-m-d H:i:s')),



            );

            $stm = $connection->prepare("TRUNCATE TABLE company_inventory_preferences");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO `company_inventory_preferences` (`user_id`, `inventory_item`, `add_suppliers`, `upload_photos`, `updated_at`) VALUES ('{$rows['user_id']}','{$rows['inventory_item']}','{$rows['add_suppliers']}','{$rows['upload_photos']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            //echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyReferralSource($connection)

    {

        try{



            $data = array(

                array('referral' => 'Advertisement', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Banner AD/Sponsor', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Billboard/Outdoor', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Co-Worker', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Employee Referral', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Family Member', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Friend', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Internet', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Job Fair/Exhibit', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Journal/Magazine Ad', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Local Newspaper', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Mailing/Letter', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Neighbour', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Post Card', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Purchased Prospect List', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Radio', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Referral/Word of Mouth', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Television', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Tenant Referral', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Transfer from another Branch', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Walk-ins', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Yellow Pages', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('referral' => 'Other', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE tenant_referral_source");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `tenant_referral_source`( `referral`,`created_at`,`updated_at`) VALUES ('{$rows['referral']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            //echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyTenantEthnicity($connection)

    {

        try{



            $data = array(

                array('title' => 'American Indian/Alaskan Native', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('title' => 'Asian', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('title' => 'Black (not Hispanic or Latino)', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('title' => 'Hispanic or Latino', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('title' => 'Native Hawaiian / Pacific Islander', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('title' => 'Two or More Races (not Hispanic or Latino)', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('title' => 'White (not Hispanic or Latino)', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('title' => 'Not Sure', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('title' => 'Other', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE tenant_ethnicity");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `tenant_ethnicity`( `title`,`created_at`,`updated_at`) VALUES ('{$rows['title']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            //echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyTenantMaritalStatus($connection)

    {

        try{



            $data = array(
                array('marital' => 'Single', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('marital' => 'Married', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('marital' => 'Divorce', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('marital' => 'Widow', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('marital' => 'Widower', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('marital' => 'Not Disclosed', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE tenant_marital_status");

            $stm->execute();



            foreach($data as $rows)

            {

                $query = "INSERT INTO  `tenant_marital_status`( `marital`,`created_at`,`updated_at`) VALUES ('{$rows['marital']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            //echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyTenantHobby($connection)

    {

        try{



            $data = array(

                array('hobby' => 'Animal Care', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Beach', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Bicycling', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Billiards', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Boating', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Bowling', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Camping', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Church Activities', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Computer', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Cooking', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Crafts', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Dancing', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Dating Online', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Eating Out', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Entertaining', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Exercise', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Family Time', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Fishing', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Gardening', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Going to Movies', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Golf', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Hiking', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Horseback Riding', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Housework', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Hunting', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Listening to Music', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Motorcycling', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Painting', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Playing Cards', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Playing Music', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Reading', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Relaxing', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Renting Movies', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Running', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Sewing', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Shopping', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Skiing', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Sleeping', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Socializing', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Swimming', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Team Sports', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Tennis', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Theater', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Traveling', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Volunteer Work', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Walking', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Watching Sports', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Watching TV', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Working on Cars', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('hobby' => 'Writing', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE hobbies");

            $stm->execute();



            foreach($data as $rows)

            {

                $query = "INSERT INTO `hobbies`( `hobby`,`created_at`,`updated_at`) VALUES ('{$rows['hobby']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            //echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyTenantVeteranStatus($connection)

    {

        try{



            $data = array(

                array('veteran' => 'Newly Separated Veteran', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('veteran' => 'Other Protected Veterans', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('veteran' => 'Special Disabled Veteran', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('veteran' => 'Veteran', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('veteran' => 'Veteran of Vietnam Era', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $stm = $connection->prepare("TRUNCATE TABLE tenant_veteran_status");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `tenant_veteran_status`( `veteran`,`created_at`,`updated_at`) VALUES ('{$rows['veteran']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            //echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyTenantCollectionReason($connection)

    {

        try{



            $data = array(

                array('reason' => 'Abandoned the property', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reason' => 'Deceased', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reason' => 'Destroyed the Property', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reason' => 'Did not pay Rent/Mortgage  (This should be the default)', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reason' => 'Hospitalized', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reason' => 'In Jail', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reason' => 'In Prison', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reason' => 'Lost Job', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reason' => 'Not Retuning Calls', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reason' => 'Other (we need a popup here for the user to write notes)', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $stm = $connection->prepare("TRUNCATE TABLE tenant_collection_reason");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `tenant_collection_reason`( `reason`,`created_at`,`updated_at`) VALUES ('{$rows['reason']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            //echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyCarrier($connection)

    {

        try{



            $data = array(

                array('carrier' => 'Alaska Communications Systems', 'sms_gateway' => 'msg.acsalaska.com' ,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Aliant', 'sms_gateway' => NULL ,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Alltel ', 'sms_gateway' => 'message.alltel.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Ameritech', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Assurance Wireless', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'AT&T Wireless', 'sms_gateway' => 'txt.att.net' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Bell Canada', 'sms_gateway' => 'txt.bell.ca' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Bell Mobility', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Bell South', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Bluegrass Cellular', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Boost', 'sms_gateway' => 'myboostmobile.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Cellularone', 'sms_gateway' => 'mobile.celloneusa.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Cellular South', 'sms_gateway' => 'csouth1.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Cellular One MMS', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Centennial Wireless', 'sms_gateway' => 'cwemail.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Cincinnati Bell', 'sms_gateway' => 'gocbw.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Cingular Postpaid', 'sms_gateway' => 'cingular.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Cingular Prepaid', 'sms_gateway' => 'cingulartext.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Clearnet', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Cricket', 'sms_gateway' => 'sms.mycricket.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Edge Wireless', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Fido', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'I Wireless', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Koodo Mobile and Telus Mobnility', 'sms_gateway' => 'msg.telus.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Metro PCS', 'sms_gateway' => 'mymetropcs.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Mobi', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'MT`s Mobility', 'sms_gateway' => 'text.mtsmobility.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Nextel', 'sms_gateway' => 'messaging.nextel.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Ntelos', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'PC Telecom', 'sms_gateway' => 'mobiletxt.ca' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Pioneer Cellular', 'sms_gateway' => 'zsend.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Pocket Wireless', 'sms_gateway' => 'sms.pocket.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'President', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Qwest', 'sms_gateway' => 'qwestmp.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Revol Wireless', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Rogers Wireless', 'sms_gateway' => 'pcs.rogers.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'SaskTell Mobility', 'sms_gateway' => 'sms.sasktel.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Simple mobile', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Sprint PCS', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'South Central Communications', 'sms_gateway' => 'rinasms.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Sprint(PCS)', 'sms_gateway' => 'messaging.sprintpcs.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Sprint(Nextel)', 'sms_gateway' => 'page.nextel.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Straight Talk', 'sms_gateway' => 'vtext.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Syringa Wireless', 'sms_gateway' => 'rinasms.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'T. Mobile', 'sms_gateway' => 'tmomail.net' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Teleflip', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Telus', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'TELUS Mobility', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Trafone', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Unicel', 'sms_gateway' => 'utext.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'US Cellular', 'sms_gateway' => 'email.uscc.net' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Verizon', 'sms_gateway' => 'viaerosms.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Viaero', 'sms_gateway' => 'viaerosms.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Virgin Mobile (CA)', 'sms_gateway' => 'vmobile.ca' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Virgin Mobile (USA)', 'sms_gateway' => 'vmobl.com' , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrier' => 'Other', 'sms_gateway' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE carrier");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `carrier`( `carrier`,`sms_gateway`,`created_at`,`updated_at`) VALUES ('{$rows['carrier']}','{$rows['sms_gateway']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            //echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyPhoneType($connection)

    {

        try{



            $data = array(

                array('type' => 'Mobile', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('type' => 'Work', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('type' => 'Fax', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('type' => 'Home ', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('type' => 'Other', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $stm = $connection->prepare("TRUNCATE TABLE phone_type");

            $stm->execute();

            foreach($data as $rows)

            {



                $query = "INSERT INTO  `phone_type`( `type`,`created_at`,`updated_at`) VALUES ('{$rows['type']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            //echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyCredentialType($connection){

        try{



            $data = array(

                array('user_id'=>'1','credential_type' => 'Bond', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>'1','credential_type' => 'Certification', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>'1','credential_type' => 'Insurance', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>'1','credential_type' => 'License', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>'1','credential_type' => 'Permit ', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>'1','credential_type' => 'Other', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $stm = $connection->prepare("TRUNCATE TABLE tenant_credential_type");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `tenant_credential_type`( `user_id`,`credential_type`,`created_at`,`updated_at`) VALUES ('{$rows['user_id']}','{$rows['credential_type']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            //echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyCountry($connection){

        try{



            $data = array(

                array('name' => 'Abkhazia', 'code' => '+7 840', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Afghanistan', 'code' => '+93', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Albania', 'code' => '+355', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Algeria', 'code' => '+213', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'American Samoa', 'code' => '+1 684', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Andorra', 'code' => '+376', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Angola', 'code' => '+244', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Anguilla', 'code' => '+1 264', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Antigua and Barbuda', 'code' => '+1 268', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Argentina', 'code' => '+54', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Armenia', 'code' => '+374', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Aruba', 'code' => '+297', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Ascension', 'code' => '+247', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Australia', 'code' => '+61', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Austria', 'code' => '+43', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Azerbaijan', 'code' => '+994', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Bahamas', 'code' => '+1 242', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Bahrain', 'code' => '+973', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Bangladesh', 'code' => '+880', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Barbados', 'code' => '+1 246', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Belarus', 'code' => '+375', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Belgium', 'code' => '+32', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Belize', 'code' => '+501', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Benin', 'code' => '+229', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Bermuda', 'code' => '+1 441', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Bhutan', 'code' => '+975', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Bolivia', 'code' => '+591', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Bosnia and Herzegovina', 'code' => '+387', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Botswana', 'code' => '+267', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Brazil', 'code' => '+55', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'British Indian Ocean Territory', 'code' => '+246', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'British Virgin Islands', 'code' => '+1 284', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Brunei', 'code' => '+673', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Bulgaria', 'code' => '+359', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Burkina Faso', 'code' => '+226', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Burundi', 'code' => '+257', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Cambodia', 'code' => '+855', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Cameroon', 'code' => '+237', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Canada', 'code' => '+ 1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Cape Verde', 'code' => '+238', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Cayman Islands', 'code' => '+ 345', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Central African Republic', 'code' => '+236', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Chad', 'code' => '+235', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Chile', 'code' => '+56', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'China', 'code' => '+86', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Cocos-Keeling Islands', 'code' => '+891', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Colombia', 'code' => '+57', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Comoros', 'code' => '+269', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Congo', 'code' => '+242', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Congo, Dem. Rep. of ', 'code' => '+243', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Cook Islands', 'code' => '+682', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Costa Rica', 'code' => '+506', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Croatia', 'code' => '+385', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Cuba', 'code' => '+53', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Curacao', 'code' => '+599', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Cyprus', 'code' => '+537', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Czech Republic', 'code' => '+420', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Denmark', 'code' => '+45', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Djibouti', 'code' => '+253', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Dominica', 'code' => '+1 767', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Dominican Republic', 'code' => '+1 809', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Easter Island', 'code' => '+56', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Ecuador', 'code' => '+593', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Egypt', 'code' => '+20', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'El Salvador', 'code' => '+503', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Equatorial Guinea', 'code' => '+240', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Eritrea', 'code' => '+291', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Estonia', 'code' => '+372', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Ethiopia', 'code' => '+251', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Falkland Islands', 'code' => '+500', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Faroe Islands', 'code' => '+298', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Fiji', 'code' => '+679', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Finland', 'code' => '+358', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'France', 'code' => '+33', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'French Antilles', 'code' => '+596', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'French Guiana', 'code' => '+594', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'French Polynesia', 'code' => '+689', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Gabon', 'code' => '+241', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Gambia', 'code' => '+220', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Georgia', 'code' => '+995', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Germany', 'code' => '+49', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Ghana', 'code' => '+233', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Gibraltar', 'code' => '+350', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Greece', 'code' => '+30', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Greenland', 'code' => '+299', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Grenada', 'code' => '+1 473', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Guadeloupe', 'code' => '+590', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Guam', 'code' => '+1 671', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Guatemala', 'code' => '+502', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Guinea', 'code' => '+224', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Guinea-Bissau', 'code' => '+245', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Guyana', 'code' => '+595', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Haiti', 'code' => '+509', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Honduras', 'code' => '+504', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Hong Kong SAR China', 'code' => '+852', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Hungary', 'code' => '+36', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Iceland', 'code' => '+354', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'India', 'code' => '+91', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Indonesia', 'code' => '+62', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Iran', 'code' => '+98', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Iraq', 'code' => '+964', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Ireland', 'code' => '+353', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Israel', 'code' => '+972', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Italy', 'code' => '+39', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Ivory Coast', 'code' => '+225', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Jamaica', 'code' => '+1 876', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Japan', 'code' => '+81', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Jordan', 'code' => '+962', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Kazakhstan', 'code' => '+7 7', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Kenya', 'code' => '+254', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Kiribati', 'code' => '+686', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Kuwait', 'code' => '+965', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Kyrgyzstan', 'code' => '+996', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Laos', 'code' => '+856', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Latvia', 'code' => '+371', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Lebanon', 'code' => '+961', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Lesotho', 'code' => '+266', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Liberia', 'code' => '+231', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Libya', 'code' => '+218', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Liechtenstein', 'code' => '+423', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Lithuania', 'code' => '+370', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Luxembourg', 'code' => '+352', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Macau SAR China', 'code' => '+853', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Macedonia', 'code' => '+389', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Madagascar', 'code' => '+261', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Malawi', 'code' => '+265', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Malaysia', 'code' => '+60', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Maldives', 'code' => '+960', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Mali', 'code' => '+223', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Malta', 'code' => '+356', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Marshall Islands', 'code' => '+692', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Martinique', 'code' => '+596', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Mauritania', 'code' => '+222', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Mauritius', 'code' => '+230', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Mayotte', 'code' => '+262', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Mexico', 'code' => '+52', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Micronesia', 'code' => '+691', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Midway Island', 'code' => '+1 808', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Moldova', 'code' => '+373', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Monaco', 'code' => '+377', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Mongolia', 'code' => '+976', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Montenegro', 'code' => '+382', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Montserrat', 'code' => '+1664', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Morocco', 'code' => '+212', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Myanmar', 'code' => '+95', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Namibia', 'code' => '+264', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Nauru', 'code' => '+674', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Nepal', 'code' => '+977', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Netherlands', 'code' => '+31', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Netherlands Antilles', 'code' => '+599', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Nevis', 'code' => '+1 869', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'New Caledonia', 'code' => '+687', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'New Zealand', 'code' => '+64', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Nicaragua', 'code' => '+505', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Niger', 'code' => '+227', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Nigeria', 'code' => '+234', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Niue', 'code' => '+683', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Norfolk Island', 'code' => '+672', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'North Korea', 'code' => '+850', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Northern Mariana Islands', 'code' => '+1 670', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Norway', 'code' => '+47', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Oman', 'code' => '+968', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Pakistan', 'code' => '+92', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Palau', 'code' => '+680', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Palestinian Territory', 'code' => '+970', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Panama', 'code' => '+507', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Papua New Guinea', 'code' => '+675', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Paraguay', 'code' => '+595', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Peru', 'code' => '+51', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Philippines', 'code' => '+63', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Poland', 'code' => '+48', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Portugal', 'code' => '+351', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Puerto Rico', 'code' => '+1 787', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Qatar', 'code' => '+974', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Reunion', 'code' => '+ 262', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Romania', 'code' => '+40', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Russia', 'code' => '+7', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Rwanda', 'code' => '+250', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Samoa', 'code' => '+685', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'San Marino', 'code' => '+378', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Saudi Arabia', 'code' => '+966', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Senegal', 'code' => '+221', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Serbia', 'code' => '+381', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Seychelles', 'code' => '+248', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Sierra Leone', 'code' => '+232', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Singapore', 'code' => '+65', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Slovakia', 'code' => '+421', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Slovenia', 'code' => '+386', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Solomon Islands', 'code' => '+677', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'South Africa', 'code' => '+27', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'South Georgia and the South Sandwich Islands', 'code' => '+ 500', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'South Korea', 'code' => '+82', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Spain', 'code' => '+34', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Sri Lanka', 'code' => '+94', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Sudan', 'code' => '+249', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Suriname', 'code' => '+597', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Swaziland', 'code' => '+268', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Sweden', 'code' => '+46', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Switzerland', 'code' => '+41', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Syria', 'code' => '+963', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Taiwan', 'code' => '+886', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Tajikistan', 'code' => '+992', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Tanzania', 'code' => '+255', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Thailand', 'code' => '+66', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Timor Leste', 'code' => '+670', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Togo', 'code' => '+228', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Tokelau', 'code' => '+690', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Tonga', 'code' => '+676', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Trinidad and Tobago', 'code' => '+1 868', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Tunisia', 'code' => '+216', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Turkey', 'code' => '+90', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Turkmenistan', 'code' => '+993', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Turks and Caicos Islands', 'code' => '+1 649', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Tuvalu', 'code' => '+688', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'U.S. Virgin Islands', 'code' => '+1 340', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Uganda', 'code' => '+256', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Ukraine', 'code' => '+380', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'United Arab Emirates', 'code' => '+971', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'United Kingdom', 'code' => '+44', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'United States', 'code' => '+1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Uruguay', 'code' => '+598', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Uzbekistan', 'code' => '+998', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Vanuatu', 'code' => '+678', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Venezuela', 'code' => '+58', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Vietnam', 'code' => '+84', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Wake Island', 'code' => '+1 808', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Wallis and Futuna', 'code' => '+681', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Yemen', 'code' => '+967', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Zambia', 'code' => '+260', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Zimbabwe', 'code' => '+263', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $stm = $connection->prepare("TRUNCATE TABLE countries");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO `countries`( `name`,`code`,`created_at`,`updated_at`) VALUES ('{$rows['name']}','{$rows['code']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            //echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyUserRoles($connection)

    {

        try{

            $roles = json_decode('[{"id":"j1_1","text":"Setup\n","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#","id":"j1_1_anchor"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"#"},{"id":"j1_2","text":"Company Setup\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_1"},{"id":"j1_3","text":"Account Setup","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_2"},{"id":"j1_4","text":"Property Setup\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_1"},{"id":"j1_5","text":"Property Type\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_4"},{"id":"j1_6","text":"Add property Type","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_5"},{"id":"j1_7","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_5"},{"id":"j1_8","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_5"},{"id":"j1_9","text":"Import Property Type","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_5"},{"id":"j1_10","text":"Download Sample","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_5"},{"id":"j1_11","text":"Unit Type\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_4"},{"id":"j1_12","text":"Add Unit Type","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_11"},{"id":"j1_13","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_11"},{"id":"j1_14","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_11"},{"id":"j1_15","text":"Import Unit Type","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_11"},{"id":"j1_16","text":"Download Sample","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_11"},{"id":"j1_17","text":"Property Sub-Type\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_4"},{"id":"j1_18","text":"Add Property Sub Type","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_17"},{"id":"j1_19","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_17"},{"id":"j1_20","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_17"},{"id":"j1_21","text":"Import property Sub Type","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_17"},{"id":"j1_22","text":"Download Sample","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_17"},{"id":"j1_23","text":"Activate/Deactivate","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_17"},{"id":"j1_24","text":"Property Style\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_4"},{"id":"j1_25","text":"Add Property Style","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_24"},{"id":"j1_26","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_24"},{"id":"j1_27","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_24"},{"id":"j1_28","text":"Import Property Style","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_24"},{"id":"j1_29","text":"Download Sample","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_24"},{"id":"j1_30","text":"Activate/Deactivate","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_24"},{"id":"j1_31","text":"Property Groups\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_4"},{"id":"j1_32","text":"Add Group","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_31"},{"id":"j1_33","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_31"},{"id":"j1_34","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_31"},{"id":"j1_35","text":"Import Property Group","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_31"},{"id":"j1_36","text":"Download Sample","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_31"},{"id":"j1_37","text":"Activate/Deactivate","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_31"},{"id":"j1_38","text":"Amenities\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_4"},{"id":"j1_39","text":"Add Amenities","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_38"},{"id":"j1_40","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_38"},{"id":"j1_41","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_38"},{"id":"j1_42","text":"Activate/Deactivate","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_38"},{"id":"j1_43","text":"Accounting\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_1"},{"id":"j1_44","text":"Account Type\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_43"},{"id":"j1_45","text":"Add Account Type","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_44"},{"id":"j1_46","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_44"},{"id":"j1_47","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_44"},{"id":"j1_48","text":"Activate/Deactivate","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_44"},{"id":"j1_49","text":"Import Account Type","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_44"},{"id":"j1_50","text":"Download Sample","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_44"},{"id":"j1_51","text":"Chart of Accounts\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_43"},{"id":"j1_52","text":"Add Chart of account","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_51"},{"id":"j1_53","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_51"},{"id":"j1_54","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_51"},{"id":"j1_55","text":"Activate/Deactivate","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_51"},{"id":"j1_56","text":"Import Chart of Account","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_51"},{"id":"j1_57","text":"Download Sample","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_51"},{"id":"j1_58","text":"Accounting Preferences","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_43"},{"id":"j1_59","text":"Charge Code\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_43"},{"id":"j1_60","text":"Add charge code","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_59"},{"id":"j1_61","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_59"},{"id":"j1_62","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_59"},{"id":"j1_63","text":"Activate/Deactivate","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_59"},{"id":"j1_64","text":"Import Charge Code","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_59"},{"id":"j1_65","text":"Download Sample","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_59"},{"id":"j1_66","text":"Charge Code Preferences\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_43"},{"id":"j1_67","text":"Add charge code Prefernece","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_66"},{"id":"j1_68","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_66"},{"id":"j1_69","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_66"},{"id":"j1_70","text":"Activate/Deactivate","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_66"},{"id":"j1_71","text":"Allocation Order","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_43"},{"id":"j1_72","text":"Check Setup","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_43"},{"id":"j1_73","text":"Users\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_1"},{"id":"j1_74","text":"Manage Users\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_73"},{"id":"j1_75","text":"Add New User","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_74"},{"id":"j1_76","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_74"},{"id":"j1_77","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_74"},{"id":"j1_78","text":"Activate/Deactivate","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_74"},{"id":"j1_79","text":"Manage User Roles\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_73"},{"id":"j1_80","text":"Add User Role","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_79"},{"id":"j1_81","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_79"},{"id":"j1_82","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_79"},{"id":"j1_83","text":"Password Setup","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_73"},{"id":"j1_84","text":"Login History","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_73"},{"id":"j1_85","text":"Vendor\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_1"},{"id":"j1_86","text":"Vendor Type\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_85"},{"id":"j1_87","text":"Add vendor Type","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_86"},{"id":"j1_88","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_86"},{"id":"j1_89","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_86"},{"id":"j1_90","text":"Maintenance\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_1"},{"id":"j1_91","text":"Category\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_90"},{"id":"j1_92","text":"Add category","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_91"},{"id":"j1_93","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_91"},{"id":"j1_94","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_91"},{"id":"j1_95","text":"Activate/Deactivate","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_91"},{"id":"j1_96","text":"Priority\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_90"},{"id":"j1_97","text":"Add Priority","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_96"},{"id":"j1_98","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_96"},{"id":"j1_99","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_96"},{"id":"j1_100","text":"Activate/Deactivate","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_96"},{"id":"j1_101","text":"Severity\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_90"},{"id":"j1_102","text":"Add Severity","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_101"},{"id":"j1_103","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_101"},{"id":"j1_104","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_101"},{"id":"j1_105","text":"Activate/Deactivate","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_101"},{"id":"j1_106","text":"Work Order Type\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_90"},{"id":"j1_107","text":"Add work Order Type","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_106"},{"id":"j1_108","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_106"},{"id":"j1_109","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_106"},{"id":"j1_110","text":"Alerts\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_1"},{"id":"j1_111","text":"User Alerts\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_110"},{"id":"j1_112","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_111"},{"id":"j1_113","text":"Preview","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_111"},{"id":"j1_114","text":"Test Mail","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_111"},{"id":"j1_115","text":"Events\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_1"},{"id":"j1_116","text":"Event Types\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_115"},{"id":"j1_117","text":"Add Event Type","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_116"},{"id":"j1_118","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_116"},{"id":"j1_119","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_116"},{"id":"j1_120","text":"Activate/Deactivate","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_116"},{"id":"j1_121","text":"Trash Bin\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_1"},{"id":"j1_122","text":"Property Type Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_123","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_122"},{"id":"j1_124","text":"Unit Type Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_125","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_124"},{"id":"j1_126","text":"Property Sub-Type Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_127","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_126"},{"id":"j1_128","text":"Property Style Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_129","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_128"},{"id":"j1_130","text":"Property Groups Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_131","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_130"},{"id":"j1_132","text":"Amenities Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_133","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_132"},{"id":"j1_134","text":"Account Type Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_135","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_134"},{"id":"j1_136","text":"Chart of Accounts Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_137","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_136"},{"id":"j1_138","text":"Accounting Prefernece Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_139","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_138"},{"id":"j1_140","text":"Charge Code Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_141","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_140"},{"id":"j1_142","text":"Charge Code Preferences Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_143","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_142"},{"id":"j1_144","text":"Manage Users Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_145","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_144"},{"id":"j1_146","text":"Manage User Roles Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_147","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_146"},{"id":"j1_148","text":"Vendor Type Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_149","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_148"},{"id":"j1_150","text":"Category Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_151","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_150"},{"id":"j1_152","text":"Priority Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_153","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_152"},{"id":"j1_154","text":"Severity Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_155","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_154"},{"id":"j1_156","text":"Event Types Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_157","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_156"},{"id":"j1_158","text":"Contacts Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_159","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_158"},{"id":"j1_160","text":"Work Order Type Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_161","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_160"},{"id":"j1_162","text":"Building Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_163","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_162"},{"id":"j1_164","text":"Unit Trash\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_121"},{"id":"j1_165","text":"Restore","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_164"},{"id":"j1_166","text":"Dashboard","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#","id":"j1_166_anchor"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"#"},{"id":"j1_167","text":"Properties\n                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#","id":"j1_167_anchor"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"#"},{"id":"j1_168","text":"Add Property","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_167"},{"id":"j1_169","text":"Import Property","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_167"},{"id":"j1_170","text":"Edit Property","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_167"},{"id":"j1_171","text":"View Property","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_167"},{"id":"j1_172","text":"Deactivate/Activate Property","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_167"},{"id":"j1_173","text":"Add Building","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_167"},{"id":"j1_174","text":"Edit Building","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_167"},{"id":"j1_175","text":"View Building","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_167"},{"id":"j1_176","text":"Deactivate/Activate Building","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_167"},{"id":"j1_177","text":"Add Unit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_167"},{"id":"j1_178","text":"Edit Unit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_167"},{"id":"j1_179","text":"View Unit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_167"},{"id":"j1_180","text":"Deactivate/Activate Unit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_167"},{"id":"j1_181","text":"Delete Building","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_167"},{"id":"j1_182","text":"Delete Unit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_167"},{"id":"j1_183","text":"Property Amortization","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_167"},{"id":"j1_184","text":"People\n                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#","id":"j1_184_anchor"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"#"},{"id":"j1_185","text":"Tenants\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_184"},{"id":"j1_186","text":"New Tenant","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_185"},{"id":"j1_187","text":"View Tenant Details\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_185"},{"id":"j1_188","text":"View General Details","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_187"},{"id":"j1_189","text":"View Charge Details","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_187"},{"id":"j1_190","text":"View Payment Details","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_187"},{"id":"j1_191","text":"View Ledger Details","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_187"},{"id":"j1_192","text":"View Occupants","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_187"},{"id":"j1_193","text":"View History/Notes","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_187"},{"id":"j1_194","text":"View File","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_187"},{"id":"j1_195","text":"View Rent Insurance","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_187"},{"id":"j1_196","text":"View Tenant portal","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_187"},{"id":"j1_197","text":"Edit Tenant Details\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_185"},{"id":"j1_198","text":"Edit General","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_197"},{"id":"j1_199","text":"Edit Charge","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_197"},{"id":"j1_200","text":"Edit Occupants","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_197"},{"id":"j1_201","text":"Edit History","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_197"},{"id":"j1_202","text":"Edit File","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_197"},{"id":"j1_203","text":"Edit Rent Insurance","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_197"},{"id":"j1_204","text":"Import Tenant","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_185"},{"id":"j1_205","text":"Owners\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_184"},{"id":"j1_206","text":"New Owner","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_205"},{"id":"j1_207","text":"Edit Owner","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_205"},{"id":"j1_208","text":"View Owner","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_205"},{"id":"j1_209","text":"Import Owner","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_205"},{"id":"j1_210","text":"Owner Draw","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_205"},{"id":"j1_211","text":"Owner Receipt","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_205"},{"id":"j1_212","text":"Vendors\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_184"},{"id":"j1_213","text":"New Vendor","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_212"},{"id":"j1_214","text":"View Vendor Details\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_212"},{"id":"j1_215","text":"View General Details","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_214"},{"id":"j1_216","text":"View Payable","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_214"},{"id":"j1_217","text":"View Ledger Details","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_214"},{"id":"j1_218","text":"View Payment Settings","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_214"},{"id":"j1_219","text":"View History/Notes","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_214"},{"id":"j1_220","text":"View File","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_214"},{"id":"j1_221","text":"View WorkOrder","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_214"},{"id":"j1_222","text":"Edit Vendor Details\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_212"},{"id":"j1_223","text":"Edit General Details","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_222"},{"id":"j1_224","text":"Edit History/Notes","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_222"},{"id":"j1_225","text":"Edit File","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_222"},{"id":"j1_226","text":"Edit Payment Settings","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_222"},{"id":"j1_227","text":"Deactivate /Activate","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_212"},{"id":"j1_228","text":"Import Vendor","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_212"},{"id":"j1_229","text":"Contacts\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_184"},{"id":"j1_230","text":"New Contact","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_229"},{"id":"j1_231","text":"Import Contact","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_229"},{"id":"j1_232","text":"Edit Contact","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_229"},{"id":"j1_233","text":"Delete Contact","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_229"},{"id":"j1_234","text":"View Contact","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_229"},{"id":"j1_235","text":"Send Tenant Statement","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_184"},{"id":"j1_236","text":"New Deposit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_184"},{"id":"j1_237","text":"Deposit Register\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_184"},{"id":"j1_238","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_237"},{"id":"j1_239","text":"Mark Deposited","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_237"},{"id":"j1_240","text":"Mark Cleared","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_237"},{"id":"j1_241","text":"Print","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_237"},{"id":"j1_242","text":"Instrument Register","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_184"},{"id":"j1_243","text":"Tenant Instrument Register","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_184"},{"id":"j1_244","text":"Vendor Instrument Register","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_184"},{"id":"j1_245","text":"Leases\n                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#","id":"j1_245_anchor"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"#"},{"id":"j1_246","text":"Guest Card\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_245"},{"id":"j1_247","text":"New Guest card","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_246"},{"id":"j1_248","text":"View Guest card\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_246"},{"id":"j1_249","text":"Book a site Visit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_248"},{"id":"j1_250","text":"Edit Guest card","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_246"},{"id":"j1_251","text":"Generate Rental Application","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_246"},{"id":"j1_252","text":"Generate Lease","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_246"},{"id":"j1_253","text":"Rental Application\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_245"},{"id":"j1_254","text":"New Rental Application","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_253"},{"id":"j1_255","text":"View Rental Application\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_253"},{"id":"j1_256","text":"Run Credit and Background Check","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_255"},{"id":"j1_257","text":"Approve","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_255"},{"id":"j1_258","text":"Checklist","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_255"},{"id":"j1_259","text":"Download","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_255"},{"id":"j1_260","text":"Print","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_255"},{"id":"j1_261","text":"Generate Lease","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_255"},{"id":"j1_262","text":"Edit Rental Application","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_253"},{"id":"j1_263","text":"Run Background Check","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_253"},{"id":"j1_264","text":"Print Blank","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_253"},{"id":"j1_265","text":"Leases\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_245"},{"id":"j1_266","text":"New Lease","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_265"},{"id":"j1_267","text":"View Lease\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_265"},{"id":"j1_268","text":"View Lease Details","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_267"},{"id":"j1_269","text":"View Charges","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_267"},{"id":"j1_270","text":"View File","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_267"},{"id":"j1_271","text":"Edit Lease\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_265"},{"id":"j1_272","text":"Lease Details","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_271"},{"id":"j1_273","text":"Charges","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_271"},{"id":"j1_274","text":"File Library","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_271"},{"id":"j1_275","text":"Lease Renewal\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_265"},{"id":"j1_276","text":"Renewal Lease","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_275"},{"id":"j1_277","text":"Send Renewal Letter","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_275"},{"id":"j1_278","text":"Print Letter","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_275"},{"id":"j1_279","text":"Move-In\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_245"},{"id":"j1_280","text":"Move-In","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_279"},{"id":"j1_281","text":"Unit Vacancy Details","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_245"},{"id":"j1_282","text":"Lease Affordability","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_245"},{"id":"j1_283","text":"Reports","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#","id":"j1_283_anchor"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"#"},{"id":"j1_284","text":"Maintenance\n                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#","id":"j1_284_anchor"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"#"},{"id":"j1_285","text":"Ticket\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_284"},{"id":"j1_286","text":"View Ticket","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_285"},{"id":"j1_287","text":"Delete Ticket","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_285"},{"id":"j1_288","text":"Work Order\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_284"},{"id":"j1_289","text":"View Work Order","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_288"},{"id":"j1_290","text":"Edit Work Order","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_288"},{"id":"j1_291","text":"Delete Work Order","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_288"},{"id":"j1_292","text":"Create WorkOrder","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_288"},{"id":"j1_293","text":"Inspection\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_284"},{"id":"j1_294","text":"View Inspection","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_293"},{"id":"j1_295","text":"Edit Inspection","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_293"},{"id":"j1_296","text":"Delete Inspection","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_293"},{"id":"j1_297","text":"New Inspection","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_293"},{"id":"j1_298","text":"Accounting\n                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#","id":"j1_298_anchor"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"#"},{"id":"j1_299","text":"Receivables\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_298"},{"id":"j1_300","text":"Receive Payment","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_299"},{"id":"j1_301","text":"Payables\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_298"},{"id":"j1_302","text":"Pay selected Bills","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_301"},{"id":"j1_303","text":"Banking\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_298"},{"id":"j1_304","text":"Write Check","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_303"},{"id":"j1_305","text":"Bank Register","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_303"},{"id":"j1_306","text":"Journal Enteries\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_298"},{"id":"j1_307","text":"View","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_306"},{"id":"j1_308","text":"Reverse JE","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_306"},{"id":"j1_309","text":"New Journal Entry","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_306"},{"id":"j1_310","text":"Budgeting\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_298"},{"id":"j1_311","text":"New Budget","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_310"},{"id":"j1_312","text":"Copy Budget","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_310"},{"id":"j1_313","text":"Edit Budget","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_310"},{"id":"j1_314","text":"View Budget","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_310"},{"id":"j1_315","text":"Accounting Closing","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_310"},{"id":"j1_316","text":"Owner Draw","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_310"},{"id":"j1_317","text":"Owner Reciept","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_310"},{"id":"j1_318","text":"Transactions","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_310"},{"id":"j1_319","text":"Process Management fees","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_310"},{"id":"j1_320","text":"Pending Transations","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_310"},{"id":"j1_321","text":"Bank Reconcillation\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_298"},{"id":"j1_322","text":"ReConcile Now","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_321"},{"id":"j1_323","text":"View","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_321"},{"id":"j1_324","text":"Print","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_321"},{"id":"j1_325","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_321"},{"id":"j1_326","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_321"},{"id":"j1_327","text":"EFT\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_298"},{"id":"j1_328","text":"EFT Tenants\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_327"},{"id":"j1_329","text":"Add Tenant","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_328"},{"id":"j1_330","text":"View Tenant","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_328"},{"id":"j1_331","text":"Edit Tenant","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_328"},{"id":"j1_332","text":"Delete Tenant","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_328"},{"id":"j1_333","text":"EFT Transmission\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_327"},{"id":"j1_334","text":"Save","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_333"},{"id":"j1_335","text":"Process EFT","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_333"},{"id":"j1_336","text":"EFT Register\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_327"},{"id":"j1_337","text":"View","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_336"},{"id":"j1_338","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_336"},{"id":"j1_339","text":"Upload To Bank","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_336"},{"id":"j1_340","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_336"},{"id":"j1_341","text":"Process EFT File","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_336"},{"id":"j1_342","text":"Download","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_336"},{"id":"j1_343","text":"Bill\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_298"},{"id":"j1_344","text":"New Bill","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_343"},{"id":"j1_345","text":"View Details","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_343"},{"id":"j1_346","text":"Print","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_343"},{"id":"j1_347","text":"Edit Bill","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_343"},{"id":"j1_348","text":"Delete Bill","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_343"},{"id":"j1_349","text":"On Hold","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_343"},{"id":"j1_350","text":"Due Bill","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_343"},{"id":"j1_351","text":"Apply Late Fee","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_298"},{"id":"j1_352","text":"Account Closing","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_298"},{"id":"j1_353","text":"Transactions","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_298"},{"id":"j1_354","text":"Process Management Fees","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_298"},{"id":"j1_355","text":"Pending Transactions\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_298"},{"id":"j1_356","text":"Pending Transations","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_355"},{"id":"j1_357","text":"Tenant Transaction & NSF","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_355"},{"id":"j1_358","text":"Tenant Payment","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_355"},{"id":"j1_359","text":"Security Deposit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_355"},{"id":"j1_360","text":"Owner Draw","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_355"},{"id":"j1_361","text":"Security Deposit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_355"},{"id":"j1_362","text":"Owner Receipt","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_355"},{"id":"j1_363","text":"Unit Vacancy","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_355"},{"id":"j1_364","text":"Vendor Invoice","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_355"},{"id":"j1_365","text":"Vendor Payment","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_355"},{"id":"j1_366","text":"Recurring transaction\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_298"},{"id":"j1_367","text":"Recurring transaction","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_366"},{"id":"j1_368","text":"Recurring Bills","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_366"},{"id":"j1_369","text":"Recurring Invoices","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_366"},{"id":"j1_370","text":"Utility Billing\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_298"},{"id":"j1_371","text":"Pay Utility Bill","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_370"},{"id":"j1_372","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_370"},{"id":"j1_373","text":"Communication\n                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#","id":"j1_373_anchor"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"#"},{"id":"j1_374","text":"Email\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_373"},{"id":"j1_375","text":"Drafts\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_374"},{"id":"j1_376","text":"Delete Mail","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_375"},{"id":"j1_377","text":"Open Mail","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_375"},{"id":"j1_378","text":"Compose email","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_374"},{"id":"j1_379","text":"Sent Mails","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_374"},{"id":"j1_380","text":"Forward Mail","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_374"},{"id":"j1_381","text":"Message\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_373"},{"id":"j1_382","text":"Drafts\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_381"},{"id":"j1_383","text":"Delete Message","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_382"},{"id":"j1_384","text":"Open Message","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_382"},{"id":"j1_385","text":"Compose message","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_381"},{"id":"j1_386","text":"Sent Message","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_381"},{"id":"j1_387","text":"Forward Message","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_381"},{"id":"j1_388","text":"Letter & Notices\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_373"},{"id":"j1_389","text":"Create new letter","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_388"},{"id":"j1_390","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_388"},{"id":"j1_391","text":"Send","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_388"},{"id":"j1_392","text":"Preview","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_388"},{"id":"j1_393","text":"Download","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_388"},{"id":"j1_394","text":"New Conversation","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_373"},{"id":"j1_395","text":"Group/Message","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_373"},{"id":"j1_396","text":"Task And Reminder\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_373"},{"id":"j1_397","text":"New Task And Reminder","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_396"},{"id":"j1_398","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_396"},{"id":"j1_399","text":"Marketing\n                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#","id":"j1_399_anchor"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"#"},{"id":"j1_400","text":"Listing\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_399"},{"id":"j1_401","text":"View Details\n                                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_400"},{"id":"j1_402","text":"Units","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_401"},{"id":"j1_403","text":"Analytics","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_401"},{"id":"j1_404","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_401"},{"id":"j1_405","text":"Vacant Units","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_400"},{"id":"j1_406","text":"Flyers\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_399"},{"id":"j1_407","text":"Upload","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_406"},{"id":"j1_408","text":"Preview","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_406"},{"id":"j1_409","text":"Set as Default","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_406"},{"id":"j1_410","text":"Campaign\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_399"},{"id":"j1_411","text":"Add new Campaign","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_410"},{"id":"j1_412","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_410"},{"id":"j1_413","text":"Delete","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_410"},{"id":"j1_414","text":"Settings\n                                                                                                                ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_399"},{"id":"j1_415","text":"Edit","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_414"},{"id":"j1_416","text":"Map","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_399"},{"id":"j1_417","text":"Settings\n                                                                                                        ","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#","id":"j1_417_anchor"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"#"},{"id":"j1_418","text":"Settings","icon":true,"li_attr":{"id":false},"a_attr":{"href":"#"},"state":{"loaded":true,"opened":false,"selected":true,"disabled":false},"data":{},"parent":"j1_417"}]');

            $roles = serialize($roles);

            $data = array(

                array('user_id'=>'1','role_name' => 'Admin', 'is_editable' => '0', 'status'=> '1', 'role'=>$roles, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id'=>'1','role_name' => 'Property Manager', 'is_editable' => '1', 'status'=> '1', 'role' => $roles, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_user_roles");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO `company_user_roles`( `user_id`,`role_name`,`status`,`is_editable`,`created_at`,`updated_at`,`role`) VALUES ('{$rows['user_id']}','{$rows['role_name']}','{$rows['status']}','{$rows['is_editable']}','{$rows['created_at']}','{$rows['updated_at']}','{$rows['role']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            //    echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyAccountType($connection)

    {

        try{

            $data = array(

                array("account_type_name" => "Asset", "user_id" =>1, "range_from" => "1000", "is_default" => "0", "status" => "1", "range_to" => "2000", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_name" => "Expense", "user_id" => 1, "range_from" => "6100", "is_default" => "0", "status" => "1", "range_to" => "9000",'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_name" => "Equity", "user_id" =>1, "range_from" => "3107", "is_default" => "1", "status" => "1", "range_to" => "4000", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_name" => "Liability", "user_id" =>1, "range_from" => "2100", "is_default" => "0", "status" => "1", "range_to" => "3000",'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_name" => "Income", "user_id" =>1, "range_from" => "4100", "is_default" => "0", "status" => "1", "range_to" => "6000",'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_account_type");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `company_account_type`( `account_type_name`, `range_from`, `range_to`,`is_default`,`user_id`,`status`,`created_at`,`updated_at`) VALUES ('{$rows['account_type_name']}','{$rows['range_from']}','{$rows['range_to']}','{$rows['is_default']}','{$rows['user_id']}','{$rows['status']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            //echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanySubAccount($connection)

    {

        try{

            $data = array(

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Cash in Bank-Operating", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Cash in Bank-Saving", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Cash in Bank-Money Market", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Security Deposit Bank Account", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Payroll Bank Account", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Petty Cash", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Tenant Receivables", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "CAM Receivable", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Accounts Receivable", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Prepaid 4s", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Other Current Assets", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Furniture & Fixtures", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Equipment", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Vehicles", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Leasehold Improvements", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Building", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Land", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Accum. Depr. Equipment", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Accum. Depr. Vehicles", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Accum. Depr. Building", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Accum. Amort. Leasehold", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Security Deposit", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Intercompany Accounts Receivable", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "1", "user_id" => '1', "account_sub_type" => "Tenant Receivable-Utilities", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Administrative Salaries", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Management Fees", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Manager Salaries", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Clerical Salaries", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Maintenance Salaries", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Payroll Taxes", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Employee Benefits", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Advertising", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Office Supplies", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Dues & Subscriptions", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Postage & Delivery", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Telephone", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Other Administrative 4s", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Maintenance", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Painting & Decorating", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Landscaping", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Cleaning", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Plumbing", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Security", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Building", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Supplies", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Other Operating 4s", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Electricity", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Water & Sanitation", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Trash Collection", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Natural Gas", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Other Utilities/Cable", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Travel & Entertainment", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Bank Charges", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Interest", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Other Interest", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Legal & Accounting", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Data Processing", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Real Estate Taxes", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Fees & Permits", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Insurance", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Depreciation", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Repairs & Maintenance", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Parking Lot Sweeping", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Supplies", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Other Operating 4s", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Non-Operating 4s-Taxes", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "2", "user_id" => '1', "account_sub_type" => "Bad Debt Expense", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "3", "user_id" => '1', "account_sub_type" => "Retained Earnings", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "3", "user_id" => '1', "account_sub_type" => "Owner Draw", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "3", "user_id" => '1', "account_sub_type" => "Owner Contribution of Capital", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "3", "user_id" => '1', "account_sub_type" => "Current Year Earnings/Loss", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "4", "user_id" => '1', "account_sub_type" => "Accounts Payable", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "4", "user_id" => '1', "account_sub_type" => "Security Deposit 2", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "4", "user_id" => '1', "account_sub_type" => "Prepaid Rent 2", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "4", "user_id" => '1', "account_sub_type" => "Notes Payable", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "4", "user_id" => '1', "account_sub_type" => "Fed. 5 Tax Withheld", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "4", "user_id" => '1', "account_sub_type" => "FICA Tax Withheld", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "4", "user_id" => '1', "account_sub_type" => "Medicare Tax Withheld", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "4", "user_id" => '1', "account_sub_type" => "State 5 Tax Withheld", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "4", "user_id" => '1', "account_sub_type" => "SUTA Tax Withheld", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "4", "user_id" => '1', "account_sub_type" => "Sales Tax Payable", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "4", "user_id" => '1', "account_sub_type" => "Intercompany Accounts Payable", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "4", "user_id" => '1', "account_sub_type" => "Contracts Payable", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "4", "user_id" => '1', "account_sub_type" => "Mortgage Payable", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "4", "user_id" => '1', "account_sub_type" => "Long Term Notes Payable", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "4", "user_id" => '1', "account_sub_type" => "Other Long Term Liabilities", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "Rental Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "Parking Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "Application Fee Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "Cleaning Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "Laundry Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "Late Charge Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "Prepaid Rent Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "NSF Fee Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "Maint & Repairs Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "Interest Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "Electricity Utility Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "HOA Dues", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "Water Utility Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "Gas Utility Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "CAM Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "Intercompany Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "Other Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "Non-Operating Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "Less: Concessions", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "Sewer Utility Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "A/C Utility Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "Waste Disposal Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "GST Tax Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "HST Tax Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "PST Tax Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "VAT Tax Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "QST Tax Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("account_type_id" => "5", "user_id" => '1', "account_sub_type" => "Pet Fees Income", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),



            );

            $stm = $connection->prepare("TRUNCATE TABLE company_account_sub_type");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO `company_account_sub_type`( `account_type_id`,`user_id`,`account_sub_type`,`created_at`,`updated_at`) VALUES ('{$rows['account_type_id']}','{$rows['user_id']}','{$rows['account_sub_type']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            //    echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyCreditAccount($connection)

    {

        try{

            $data = array(

                array("credit_accounts" => "1000-Cash in Bank-Operating", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1001-Fdsfds", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1010-Cash in Bank-Saving", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1020-Cash in Bank-Money Market", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1030-Security Deposit Bank Account", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1035-Payroll Bank Account", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1040-Petty Cash", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1100-Tenant Receivables", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1125-Tenant Receivable-Utilities", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1128-Tenant Receivable-Taxes", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1150-CAM Receivable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1175-Accounts Receivable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1200-Prepaid 4s", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1290-Other Current Assets", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1300-Furniture &amp; Fixtures", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1320-Equipment", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1325-Vehicles", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1350-Leasehold Improvements", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1400-Building", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1450-Land", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1520-Accum. Depr. Equipment", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1525-Accum. Depr. Vehicles", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1550-Accum. Depr. Building", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1575-Accum. Amort. Leasehold", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1600-Security Deposit", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "1700-Intercompany Accounts Receivable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "2000-Accounts Payable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "2010-Security Deposit 2", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "2020-Prepaid Rent 2", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "2030-Notes Payable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "2100-Fed. 5 Tax Withheld", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "2110-FICA Tax Withheld", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "2115-Medicare Tax Withheld", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "2120-State 5 Tax Withheld", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "2125-SUTA Tax Withheld", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "2130-Sales Tax Payable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "2140-5 Taxes Payable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "2295-Intercompany Accounts Payable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "2300-Contracts Payable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "2400-Mortgage Payable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "2500-Long Term Notes Payable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "2990-Other Long Term Liabilities", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "3000-Retained Earnings", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "3010-Owner Draw", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "3020-Owner Contribution of Capital", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "3999-Current Year Earnings/Loss", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4020-Parking Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4030-Application Fee Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4040-Cleaning Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4060-Laundry Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4080-Late Charge Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4110-Prepaid Rent Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4120-NSF Fee Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4125-7787892", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4140-Maint &amp; Repairs Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4160-Interest Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4170-Electricity Utility Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4171-HOA Dues", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4180-Water Utility Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4185-Sewer Utility Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4187-A/C Utility Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4188-Waste Disposal Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4190-Gas Utility Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4200-CAM Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4250-GST Tax Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4251-HST Tax Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4253-PST Tax Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4300-Intercompany Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4450-Less: Concessions", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "4490-Other Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5000-Administrative Salaries", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5010-Management Fees", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5020-Manager Salaries", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5030-Clerical Salaries", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5040-Maintenance Salaries", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5050-Payroll Taxes", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5060-Employee Benefits", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5100-Advertising", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5110-Office Supplies", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5120-Dues &amp; Subscriptions", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5130-Postage &amp; Delivery", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5140-Telephone", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5190-Other Administrative 4s", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5200-Maintenance", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5210-Painting &amp; Decorating", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5220-Landscaping", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5230-Cleaning", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5250-Plumbing", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5260-Security", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5270-Building Supplies", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5290-Other Operating 4s", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5300-Electricity", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5310-Water &amp; Sanitation", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5320-Trash Collection", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5330-Natural Gas", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5337-Heating Oil Utility Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5390-Other Utilities/Cable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5400-Travel &amp; Entertainment", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5410-Bank Charges", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5420-Interest", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5430-Other Interest", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5440-Legal &amp; Accounting", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5490-Data Processing", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5500-Real Estate Taxes", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5510-Fees &amp; Permits", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5520-Insurance", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "5530-Depreciation 4", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "6200-Repairs &amp; Maintenance", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "6250-Parking Lot Sweeping", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "6300-Supplies", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "7000-Bad Debt Expense", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "7990-Other Operating 4s", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "8000-Non-Operating Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("credit_accounts" => "9000-Non-Operating 4s-Taxes", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_credit_accounts");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `company_credit_accounts`( `credit_accounts`,`user_id`,`created_at`,`updated_at`) VALUES ('{$rows['credit_accounts']}','{$rows['user_id']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

//            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyDebitAccount($connection)

    {

        try{

            $data = array(

                array("debit_accounts" => "1000-Cash in Bank-Operating", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1001-Fdsfds", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1010-Cash in Bank-Saving", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1020-Cash in Bank-Money Market", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1030-Security Deposit Bank Account", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1035-Payroll Bank Account", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1040-Petty Cash", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1100-Tenant Receivables", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1125-Tenant Receivable-Utilities", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1128-Tenant Receivable-Taxes", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1150-CAM Receivable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1175-Accounts Receivable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1200-Prepaid 4s", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1290-Other Current Assets", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1300-Furniture &amp; Fixtures", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1320-Equipment", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1325-Vehicles", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1350-Leasehold Improvements", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1400-Building", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1450-Land", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1520-Accum. Depr. Equipment", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1525-Accum. Depr. Vehicles", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1550-Accum. Depr. Building<", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1575-Accum. Amort. Leasehold", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1600-Security Deposit", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "1700-Intercompany Accounts Receivable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "2000-Accounts Payable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "2010-Security Deposit 2", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "2020-Prepaid Rent 2", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "2030-Notes Payable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "2030-Notes Payable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "2100-Fed. 5 Tax Withheld", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "2110-FICA Tax Withheld", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "2115-Medicare Tax Withheld", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "2120-State 5 Tax Withheld", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "2125-SUTA Tax Withheld", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "2130-Sales Tax Payable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "2140-5 Taxes Payable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "2295-Intercompany Accounts Payable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "2300-Contracts Payable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "2400-Mortgage Payable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "2500-Long Term Notes Payable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "2990-Other Long Term Liabilities", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "3000-Retained Earnings", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "3010-Owner Draw", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "3020-Owner Contribution of Capital", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "3999-Current Year Earnings/Loss", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4020-Parking Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4030-Application Fee Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4040-Cleaning Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4060-Laundry Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4080-Late Charge Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4110-Prepaid Rent Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4120-NSF Fee Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4125-7787892", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4140-Maint &amp; Repairs Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4160-Interest Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4170-Electricity Utility Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4171-HOA Dues", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4180-Water Utility Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4185-Sewer Utility Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4187-A/C Utility Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4188-Waste Disposal Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4190-Gas Utility Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4200-CAM Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4250-GST Tax Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4251-HST Tax Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4253-PST Tax Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4300-Intercompany Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4450-Less: Concessions", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "4490-Other Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5000-Administrative Salaries", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5010-Management Fees", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5020-Manager Salaries", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5030-Clerical Salaries", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5040-Maintenance Salaries", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5050-Payroll Taxes", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5060-Employee Benefits", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5100-Advertising", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5110-Office Supplies", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5120-Dues &amp; Subscriptions", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5130-Postage &amp; Delivery", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5140-Telephone", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5190-Other Administrative 4s", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5200-Maintenance", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5210-Painting &amp; Decorating", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5220-Landscaping", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5230-Cleaning", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5250-Plumbing", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5260-Security", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5270-Building Supplies", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5290-Other Operating 4s", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5300-Electricity", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5310-Water &amp; Sanitation", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5320-Trash Collection", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5330-Natural Gas", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5337-Heating Oil Utility Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5390-Other Utilities/Cable", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5400-Travel &amp; Entertainment", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5410-Bank Charges", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5420-Interest", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5430-Other Interest", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5440-Legal &amp; Accounting", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5490-Data Processing", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5500-Real Estate Taxes", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5510-Fees &amp; Permits", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5520-Insurance", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "5530-Depreciation 4", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "6200-Repairs &amp; Maintenance", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "6250-Parking Lot Sweeping", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "6300-Supplies", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "7000-Bad Debt Expense", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "7990-Other Operating 4s", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "8000-Non-Operating Income", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("debit_accounts" => "9000-Non-Operating 4s-Taxes", "user_id" => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_debit_accounts");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `company_debit_accounts`( `debit_accounts`,`user_id`,`created_at`,`updated_at`) VALUES ('{$rows['debit_accounts']}','{$rows['user_id']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

//            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyChargeCode($connection)

    {

        try{

            $data = array(

                array('charge_code'=>"RNTCHG",'description' => 'Rent Charge', 'debit_account'=>'1','credit_account'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"CHKINW", 'description' => 'Check Received', 'debit_account'=>'1','credit_account'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"DEBINW", 'description' => 'Debit Card Payment', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"VCCH", 'description' => 'Vacancy Charge', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"WVOFF", 'description' => 'Waive Off', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"LATEFEE", 'description' => 'Late Fee', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"BDDBT", 'description' => 'Bad Debt', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"WRTOFF", 'description' => 'Write Off', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"MOINW", 'description' => 'Money Order', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"ADEB", 'description' => 'Auto Debit', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"CASH", 'description' => 'Cash Received', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"NSF", 'description' => 'NSF Charge', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"CHKBNC", 'description' => 'Check Bounce', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"CHKOW", 'description' => 'Outward Check', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"RFND", 'description' => 'Refund Payment', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"CONC", 'description' => 'Concession Amount', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"BNKADJINT", 'description' => 'Bank Adjustment Interest', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"BNKADJFEE", 'description' => 'Bank Adjustment Fee', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"REFBNS", 'description' => 'Referral Bonus', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"TRANS", 'description' => 'Tenant Transfer Fee', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"SDINTRST", 'description' => 'Security Deposit Interest', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"LBRKFee", 'description' => 'Lease Breakage Fee', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"EFTCRG", 'description' => 'EFT Charge Code', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"CAMCHG", 'description' => 'CAM Charge', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"Gas", 'description' => 'Gas', 'debit_account'=>'9','credit_account'=> '64','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"Water", 'description' => 'Water', 'debit_account'=>'9','credit_account'=> '60','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"Sewer", 'description' => 'Sewer', 'debit_account'=>'9','credit_account'=> '61','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"A/C", 'description' => 'A/C', 'debit_account'=>'9','credit_account'=> '62','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"Electricity", 'description' => 'Electricity', 'debit_account'=>'9','credit_account'=> '58','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"Waste Disposal",'description' => 'Waste Disposal', 'debit_account'=>'9','credit_account'=> '63','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"Cable TV",'description' => 'Cable TV', 'debit_account'=>'9','credit_account'=> '98','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),
                array('charge_code'=>"Heating Oil",'description' => 'Heating Oil', 'debit_account'=>'9','credit_account'=> '97','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),
                array('charge_code'=>"Internet/WI-FI",'description' => 'Internet/WI-FI', 'debit_account'=>'9','credit_account'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),
                array('charge_code'=>"Telephone",'description' => 'Telephone', 'debit_account'=>'9','credit_account'=> '83','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"Sec", 'description' => 'Security Received', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('charge_code'=>"PETF", 'description' => 'Pet Fees', 'debit_account'=>'1','credit_account'=> '1','is_editable' =>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'))

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_accounting_charge_code");

            $stm->execute();

            $i = 0;

            foreach($data as $rows)

            {

                $i = $i+1;

                $query = "INSERT INTO  `company_accounting_charge_code`( `charge_code`,`description`,`debit_account`,`credit_account`,`user_id`,`created_at`,`updated_at`,`status`,`is_editable`,`is_default`,`priority`) VALUES ('{$rows['charge_code']}','{$rows['description']}','{$rows['debit_account']}','{$rows['credit_account']}','{$rows['user_id']}','{$rows['created_at']}','{$rows['updated_at']}','{$rows['status']}','{$rows['is_editable']}','{$rows['is_default']}', $i)";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }





    public static function insertChargeCodePreferences($connection)

    {

        try{

            $data = array(

                array('charge_code_type'=>"1", 'charge_code' => '24', 'frequency'=>'1','type'=> '1','status' =>'1','is_editable' =>'0','unit_type_id' => '1','user_id' => '1','is_default' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'))

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_accounting_charge_code_preferences");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `company_accounting_charge_code_preferences`( `charge_code_type`,`charge_code`,`frequency`,`type`,`status`,`is_editable`,`unit_type_id`,`is_default`,`user_id`,`created_at`,`updated_at`) VALUES ('{$rows['charge_code_type']}','{$rows['charge_code']}','{$rows['frequency']}','{$rows['type']}','{$rows['status']}','{$rows['is_editable']}','{$rows['unit_type_id']}','{$rows['is_default']}','{$rows['user_id']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyPetFriendly($connection)

    {

        try{



            $data = array(

                array('pet_friendly' => 'No','user_id'=>'1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('pet_friendly' => 'Yes','user_id'=>'1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_pet_friendly");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `company_pet_friendly`( `pet_friendly`,`user_id`,`created_at`,`updated_at`) VALUES ('{$rows['pet_friendly']}','{$rows['user_id']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            // echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyGarageAvail($connection)

    {

        try{



            $data = array(

                array('garage_avail' => 'No','user_id'=>'1', 'created_at' => date('Y-m-d H:i:s'), 'update_at' => date('Y-m-d H:i:s')),

                array('garage_avail' => 'Yes','user_id'=>'1', 'created_at' => date('Y-m-d H:i:s'), 'update_at' => date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_garage_avail");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO `company_garage_avail`( `garage_avail`,`user_id`,`created_at`,`update_at`) VALUES ('{$rows['garage_avail']}','{$rows['user_id']}','{$rows['created_at']}','{$rows['update_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

//            print_r($exception);

            // echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyKeyaccess($connection) {

        try {



            $data = array(

                array('key_code' => 'Access Code', 'user_id' => '1', 'created_at' => date('Y-m-d H:i:s'), 'update_at' => date('Y-m-d H:i:s')),

                array('key_code' => 'Alarm Code', 'user_id' => '1', 'created_at' => date('Y-m-d H:i:s'), 'update_at' => date('Y-m-d H:i:s')),

                array('key_code' => 'Code', 'user_id' => '1', 'created_at' => date('Y-m-d H:i:s'), 'update_at' => date('Y-m-d H:i:s')),

                array('key_code' => 'Garage Door Code', 'user_id' => '1', 'created_at' => date('Y-m-d H:i:s'), 'update_at' => date('Y-m-d H:i:s')),



                array('key_code' => 'Number', 'user_id' => '1', 'created_at' => date('Y-m-d H:i:s'), 'update_at' => date('Y-m-d H:i:s')),

                array('key_code' => 'Storage Unit Code', 'user_id' => '1', 'created_at' => date('Y-m-d H:i:s'), 'update_at' => date('Y-m-d H:i:s')),



                array('key_code' => 'Other', 'user_id' => '1', 'created_at' => date('Y-m-d H:i:s'), 'update_at' => date('Y-m-d H:i:s')),



            );

            $stm = $connection->prepare("TRUNCATE TABLE company_key_access");

            $stm->execute();

            foreach ($data as $rows) {

                $query = "INSERT INTO `company_key_access`( `key_code`,`user_id`,`created_at`,`update_at`) VALUES ('{$rows['key_code']}','{$rows['user_id']}','{$rows['created_at']}','{$rows['update_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code' => 200);

        } catch (Exception $exception) {

//            print_r($exception);

            // echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }

    }



    public static function insertComplaintType($connection)

    {

        try{



            $data = array(

                array("user_id" => '1', "status"=>'1' ,"complaint_type" => "Damaged Roof", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" => '1', "status"=>'1' ,"complaint_type" => "Drug Sale", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" => '1', "status"=>'1' ,"complaint_type" => "Excess Debris or grease stains", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" => '1', "status"=>'1' ,"complaint_type" => "Fire Hazard", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" => '1', "status"=>'1' ,"complaint_type" => "Garage Issue", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" => '1', "status"=>'1' ,"complaint_type" => "Illegally Parking", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" => '1', "status"=>'1' ,"complaint_type" => "Landscaping Concerns", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" => '1', "status"=>'1' ,"complaint_type" => "Lawn Issue", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" => '1', "status"=>'1' ,"complaint_type" => "Loud Music", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" => '1', "status"=>'1' ,"complaint_type" => "Paint Issue", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" => '1', "status"=>'1' ,"complaint_type" => "Pet Issue", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" => '1', "status"=>'1' ,"complaint_type" => "Pest Control Issue", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" => '1', "status"=>'1' ,"complaint_type" => "Repair Issue", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" => '1', "status"=>'1' ,"complaint_type" => "Snow removal issue", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" => '1', "status"=>'1' ,"complaint_type" => "Storage Issue", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" => '1', "status"=>'1' ,"complaint_type" => "Trash Collection issues", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" => '1', "status"=>'1' ,"complaint_type" => "Ultimate Holiday Decorations", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" => '1', "status"=>'1' ,"complaint_type" => "Other", 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $stm = $connection->prepare("TRUNCATE TABLE complaint_types");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `complaint_types`(`user_id`,`status`,`complaint_type`,`created_at`,`updated_at`) VALUES ('{$rows['user_id']}','{$rows['status']}','{$rows['complaint_type']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            //echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertHOAType($connection)

    {

        try{



            $data = array(

                array( "type" => 'Select', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Common area violation', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Damaged Roof', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Domestic Violence', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Drug Sale', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Excess Debris or grease stains', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Expired Parking Pass', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Fencing Issue', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Fire Hazard', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Fixtures', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Flags &amp; Other decorations', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Garage Issue', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Grill near building structure', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Grill on Covered Balcony', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'High Grass', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Illegally Parked Vehicle', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Landscaping Concerns', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Lawn Issue', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Loud Music', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Maximum Speed Exceeded', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Noise Complaint', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Off the-lease- pet', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Paint Issue', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Pest Control Issue', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Pet Issue', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Pet Size Exceeded', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Pet Type Not Permitted', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Reckless Residential Driving', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Repair Issue', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Satellite Dishes and external fixtures', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Snow removal issue', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Storage Issue', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Trash Collection issues', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Ultimate Holiday Decorations', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Unapproved Structural Changes', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Unregistered Vehicle', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Vandalism/Intentional Destruction', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Vehicle Type Not Permitted', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Weeds', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array( "type" => 'Other', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))

            );

            $stm = $connection->prepare("TRUNCATE TABLE hoa_violation_type");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `hoa_violation_type`(`type`,`created_at`,`updated_at`) VALUES ('{$rows['type']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            //echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    /*seeder for inseption proprty module*/

    public static function insertlist_of_inseptionName($connection)

    {

        try{

            $data = array(

                array('inspection_name' => 'Move In Inspection', 'status' =>'1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('inspection_name' => 'Move Out Inspection', 'status' =>'1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('inspection_name' => 'Weekly Inspection', 'status' =>'1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('inspection_name' => 'Monthly Inspection', 'status' =>'1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('inspection_name' => 'Six-Month Inspection', 'status' =>'1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('inspection_name' => 'Annual Inspection', 'status' =>'1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('inspection_name' => 'General Inspection', 'status' =>'1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('inspection_name' => 'Emergency Inspection', 'status' =>'1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('inspection_name' => 'Fd', 'status' =>'1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('inspection_name' => 'Other', 'status' =>'1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );



            $stm = $connection->prepare("TRUNCATE TABLE inspection_list_property");



            $stm->execute();



            foreach($data as $rows)

            {

                $query = "INSERT INTO `inspection_list_property`( `inspection_name`,`status`,`created_at`,`updated_at`) VALUES ('{$rows['inspection_name']}','{$rows['status']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

            print_r("sfa");



        }catch (Exception $exception)

        {



        }

    }





    public static function insertTagsEmailTemplateDetails($connection)

    {

        try{

            $data = array(

                array('tag_name' => 'FirstName', 'template_key' =>'newCompanyUser_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Username', 'template_key' =>'newCompanyUser_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyLogo', 'template_key' =>'newCompanyUser_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'FirstName', 'template_key' =>'newOwnerWelcome_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Username', 'template_key' =>'newOwnerWelcome_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyLogo', 'template_key' =>'newOwnerWelcome_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'FirstName', 'template_key' =>'newTenantWelcome_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Username', 'template_key' =>'newTenantWelcome_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyLogo', 'template_key' =>'newTenantWelcome_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'FirstName', 'template_key' =>'newVendorWelcome_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Username', 'template_key' =>'newVendorWelcome_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyLogo', 'template_key' =>'newVendorWelcome_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'EmailID', 'template_key' =>'forgetPasswordEmail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'IpAddress', 'template_key' =>'forgetPasswordEmail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyLogo', 'template_key' =>'forgetPasswordEmail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),


                array('tag_name' => 'Username', 'template_key' =>'rentalApplication_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'BuildingName', 'template_key' =>'rentalApplication_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'RentalApplicationId', 'template_key' =>'rentalApplication_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CreatedOn', 'template_key' =>'rentalApplication_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'ApplicantName', 'template_key' =>'rentalApplication_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PropertyId', 'template_key' =>'rentalApplication_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PropertyName', 'template_key' =>'rentalApplication_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'BuildingName', 'template_key' =>'rentalApplication_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'UnitNumber', 'template_key' =>'rentalApplication_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'MonthlyRent', 'template_key' =>'rentalApplication_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'SecurityDeposit', 'template_key' =>'rentalApplication_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Signature', 'template_key' =>'rentalApplication_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyLogo', 'template_key' =>'rentalApplication_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),


                array('tag_name' => 'Company_Name', 'template_key' =>'leaseRenewal_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Company_Address', 'template_key' =>'leaseRenewal_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Company_City', 'template_key' =>'leaseRenewal_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Company_State', 'template_key' =>'leaseRenewal_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Company_Zip', 'template_key' =>'leaseRenewal_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Current_Date', 'template_key' =>'leaseRenewal_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Tenant_Name', 'template_key' =>'leaseRenewal_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Property_Address', 'template_key' =>'leaseRenewal_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Property_City', 'template_key' =>'leaseRenewal_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Property_State', 'template_key' =>'newCompanyUser_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Property_Zip', 'template_key' =>'leaseRenewal_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'LeaseEndDate', 'template_key' =>'leaseRenewal_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'LandLord_Name', 'template_key' =>'leaseRenewal_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyName/Logo', 'template_key' =>'vendorWorkOrderNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'VendorInsurancePolicy', 'template_key' =>'vendorWorkOrderNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'day', 'template_key' =>'vendorWorkOrderNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Date', 'template_key' =>'vendorWorkOrderNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Vendor', 'template_key' =>'vendorWorkOrderNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'NameofCredential', 'template_key' =>'vendorWorkOrderNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'email', 'template_key' =>'vendorWorkOrderNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'fax#', 'template_key' =>'vendorWorkOrderNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'address', 'template_key' =>'vendorWorkOrderNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'ContactNumber', 'template_key' =>'vendorWorkOrderNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Subject', 'template_key' =>'workOrderCompletionNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Username', 'template_key' =>'workOrderCompletionNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'WorkOrderId', 'template_key' =>'workOrderCompletionNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'WorkOrderNumber', 'template_key' =>'workOrderCompletionNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'WorkOrderType', 'template_key' =>'workOrderCompletionNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CreatedOn', 'template_key' =>'workOrderCompletionNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompletedOn', 'template_key' =>'workOrderCompletionNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'StartDate', 'template_key' =>'workOrderCompletionNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'EndDate', 'template_key' =>'workOrderCompletionNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Signature', 'template_key' =>'workOrderCompletionNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyLogo', 'template_key' =>'workOrderCompletionNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyName/Logo', 'template_key' =>'VendorInsuranceExpirationNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'VendorInsurancePolicy', 'template_key' =>'VendorInsuranceExpirationNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'day', 'template_key' =>'VendorInsuranceExpirationNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Date', 'template_key' =>'VendorInsuranceExpirationNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Vendor', 'template_key' =>'VendorInsuranceExpirationNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'NameofCredential', 'template_key' =>'VendorInsuranceExpirationNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'email', 'template_key' =>'VendorInsuranceExpirationNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'fax#', 'template_key' =>'VendorInsuranceExpirationNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'address', 'template_key' =>'VendorInsuranceExpirationNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'ContactNumber', 'template_key' =>'VendorInsuranceExpirationNotification_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyName/Logo', 'template_key' =>'lateRentPaymentDue_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Date', 'template_key' =>'lateRentPaymentDue_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'TenantName', 'template_key' =>'lateRentPaymentDue_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'OverDueDay', 'template_key' =>'lateRentPaymentDue_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'LateFee', 'template_key' =>'lateRentPaymentDue_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'ContactNumber', 'template_key' =>'lateRentPaymentDue_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'email', 'template_key' =>'lateRentPaymentDue_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PropertyName', 'template_key' =>'tenantMonthlyRentReceipts_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Address', 'template_key' =>'tenantMonthlyRentReceipts_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyCityn', 'template_key' =>'tenantMonthlyRentReceipts_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyState', 'template_key' =>'tenantMonthlyRentReceipts_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyZip', 'template_key' =>'tenantMonthlyRentReceipts_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Phone', 'template_key' =>'tenantMonthlyRentReceipts_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Email', 'template_key' =>'tenantMonthlyRentReceipts_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'TenantName', 'template_key' =>'tenantMonthlyRentReceipts_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Date', 'template_key' =>'tenantMonthlyRentReceipts_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'BuildingName', 'template_key' =>'tenantMonthlyRentReceipts_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'UnitName', 'template_key' =>'tenantMonthlyRentReceipts_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'ListRentReceipt', 'template_key' =>'tenantMonthlyRentReceipts_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'ListRentReceiptT', 'template_key' =>'tenantMonthlyRentReceipts_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Amount', 'template_key' =>'tenantMonthlyRentReceipts_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyName/Logo', 'template_key' =>'tenantMonthlyRentReceipts_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Tax', 'template_key' =>'tenantMonthlyRentReceipts_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'TotalTaxAmount', 'template_key' =>'tenantMonthlyRentReceipts_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'TotalPaidAmount', 'template_key' =>'tenantMonthlyRentReceipts_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PropertyManager', 'template_key' =>'tenantGiveNotice_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'ResidentName', 'template_key' =>'tenantGiveNotice_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PropertyName', 'template_key' =>'tenantGiveNotice_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Address', 'template_key' =>'tenantGiveNotice_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'UnitName', 'template_key' =>'tenantGiveNotice_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'NoticeDate', 'template_key' =>'tenantGiveNotice_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'MoveOutDate', 'template_key' =>'tenantGiveNotice_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'ReasonNotice', 'template_key' =>'tenantGiveNotice_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Phone', 'template_key' =>'tenantGiveNotice_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Email', 'template_key' =>'tenantGiveNotice_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyLogo', 'template_key' =>'complaintByTenant_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'VendorName', 'template_key' =>'complaintByTenant_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Address', 'template_key' =>'complaintByTenant_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'ComplaintType', 'template_key' =>'complaintByTenant_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Description', 'template_key' =>'complaintByTenant_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Email', 'template_key' =>'complaintByTenant_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Phone', 'template_key' =>'complaintByTenant_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'TenantName', 'template_key' =>'complaintAboutTenant_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Address', 'template_key' =>'complaintAboutTenant_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'ComplaintType', 'template_key' =>'complaintAboutTenant_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Description', 'template_key' =>'complaintAboutTenant_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Email', 'template_key' =>'complaintAboutTenant_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Phone', 'template_key' =>'complaintAboutTenant_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'TenantName', 'template_key' =>'leaseExpirySoon_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Days', 'template_key' =>'leaseExpirySoon_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Date', 'template_key' =>'leaseExpirySoon_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Address', 'template_key' =>'leaseExpirySoon_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'City', 'template_key' =>'leaseExpirySoon_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'State', 'template_key' =>'leaseExpirySoon_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'ZipCode', 'template_key' =>'leaseExpirySoon_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyName', 'template_key' =>'leaseExpirySoon_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyPhoneNumber', 'template_key' =>'leaseExpirySoon_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyEmail', 'template_key' =>'leaseExpirySoon_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'TenantName', 'template_key' =>'happyBirthday_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'ComapanyName', 'template_key' =>'happyBirthday_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyWebSite', 'template_key' =>'happyBirthday_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyPhone', 'template_key' =>'happyBirthday_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'BirthdayMailImg', 'template_key' =>'happyBirthday_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyLogo', 'template_key' =>'vendorComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Complaint', 'template_key' =>'vendorComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'VendorName', 'template_key' =>'vendorComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Address1', 'template_key' =>'vendorComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Address2', 'template_key' =>'vendorComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Address3', 'template_key' =>'vendorComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CityName', 'template_key' =>'vendorComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'StateName', 'template_key' =>'vendorComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Zip', 'template_key' =>'vendorComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'ComplaintType', 'template_key' =>'vendorComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Description', 'template_key' =>'vendorComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PmCompanyName', 'template_key' =>'vendorComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PmCompanyEmail', 'template_key' =>'vendorComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PhoneNumber', 'template_key' =>'vendorComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyLogo', 'template_key' =>'ownerComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Complaint', 'template_key' =>'ownerComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'OwnerName', 'template_key' =>'ownerComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Address1', 'template_key' =>'ownerComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Address2', 'template_key' =>'ownerComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Address3', 'template_key' =>'ownerComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CityName', 'template_key' =>'ownerComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'StateName', 'template_key' =>'ownerComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Zip', 'template_key' =>'ownerComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'ComplaintType', 'template_key' =>'ownerComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Description', 'template_key' =>'ownerComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PmCompanyName', 'template_key' =>'ownerComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PmCompanyEmail', 'template_key' =>'ownerComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PhoneNumber', 'template_key' =>'ownerComplaint_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),



                array('tag_name' => 'Complaint', 'template_key' =>'HoaViolation_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'TenantName', 'template_key' =>'HoaViolation_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Address1', 'template_key' =>'HoaViolation_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Address2', 'template_key' =>'HoaViolation_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Address3', 'template_key' =>'HoaViolation_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CityName', 'template_key' =>'HoaViolation_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'StateName', 'template_key' =>'HoaViolation_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Zip', 'template_key' =>'HoaViolation_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'ViolationType', 'template_key' =>'HoaViolation_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Description', 'template_key' =>'HoaViolation_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PmCompanyName', 'template_key' =>'HoaViolation_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PmCompanyEmail', 'template_key' =>'HoaViolation_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PhoneNumber', 'template_key' =>'HoaViolation_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),



                array('tag_name' => 'CompanyName', 'template_key' =>'LeaseAgreement_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PrimaryTenantName', 'template_key' =>'LeaseAgreement_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'LeaseStartDate', 'template_key' =>'LeaseAgreement_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'UnitAddress', 'template_key' =>'LeaseAgreement_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'UnitCity', 'template_key' =>'LeaseAgreement_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'UnitZipCode', 'template_key' =>'LeaseAgreement_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Currency', 'template_key' =>'LeaseAgreement_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'RentAmount', 'template_key' =>'LeaseAgreement_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'RentDueDate', 'template_key' =>'LeaseAgreement_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'graceperiod', 'template_key' =>'LeaseAgreement_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'LateFee', 'template_key' =>'LeaseAgreement_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'OneTimeOrdaily', 'template_key' =>'LeaseAgreement_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'AdditionalFee', 'template_key' =>'LeaseAgreement_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'SecurityDeposit', 'template_key' =>'LeaseAgreement_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyLogo', 'template_key' =>'LeaseAgreement_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'LeaseEndDate', 'template_key' =>'LeaseAgreement_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),



                array('tag_name' => 'Date', 'template_key' =>'TwoWayEmail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'From', 'template_key' =>'TwoWayEmail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'To', 'template_key' =>'TwoWayEmail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Subject', 'template_key' =>'TwoWayEmail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Body', 'template_key' =>'TwoWayEmail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PmCompanyName', 'template_key' =>'TwoWayEmail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PmCompanyEmail', 'template_key' =>'TwoWayEmail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PhoneNumber', 'template_key' =>'TwoWayEmail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyLogo', 'template_key' =>'TwoWayEmail_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),



                array('tag_name' => 'CompanyLogo', 'template_key' =>'LostItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Name', 'template_key' =>'LostItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PmCompanyName', 'template_key' =>'LostItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PhoneNumber', 'template_key' =>'LostItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PmCompanyEmail', 'template_key' =>'LostItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'UserName', 'template_key' =>'LostItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Category', 'template_key' =>'LostItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'BriefDescription', 'template_key' =>'LostItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'LostDate', 'template_key' =>'LostItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'LocationLost', 'template_key' =>'LostItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),



                array('tag_name' => 'CompanyLogo', 'template_key' =>'FoundItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Name', 'template_key' =>'FoundItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PmCompanyName', 'template_key' =>'FoundItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PhoneNumber', 'template_key' =>'FoundItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PmCompanyEmail', 'template_key' =>'FoundItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'UserName', 'template_key' =>'FoundItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Category', 'template_key' =>'FoundItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'BriefDescription', 'template_key' =>'FoundItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'LostDate', 'template_key' =>'FoundItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'LocationLost', 'template_key' =>'FoundItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'FoundDate', 'template_key' =>'FoundItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'FoundOn', 'template_key' =>'FoundItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'FoundLocation', 'template_key' =>'FoundItem_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),



                array('tag_name' => 'UserName', 'template_key' =>'Reporting_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyLogo', 'template_key' =>'Reporting_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Date', 'template_key' =>'Reporting_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CompanyName', 'template_key' =>'Reporting_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'ReportName', 'template_key' =>'Reporting_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PmCompanyEmail', 'template_key' =>'Reporting_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PhoneNumber', 'template_key' =>'Reporting_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PmCompanyName', 'template_key' =>'Reporting_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),



                array('tag_name' => 'CompanyLogo', 'template_key' =>'ShortTermRental_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Username', 'template_key' =>'ShortTermRental_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PropertyName', 'template_key' =>'ShortTermRental_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'BuildingName', 'template_key' =>'ShortTermRental_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'UnitNumber', 'template_key' =>'ShortTermRental_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Amount', 'template_key' =>'ShortTermRental_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Signature', 'template_key' =>'ShortTermRental_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PmCompanyName', 'template_key' =>'ShortTermRental_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PmCompanyEmail', 'template_key' =>'ShortTermRental_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PhoneNumber', 'template_key' =>'ShortTermRental_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),



                array('tag_name' => 'CompanyName', 'template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PONumber', 'template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Approver', 'template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'CreatedDate', 'template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'RequiredDate', 'template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PropertyName', 'template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'BuildingName', 'template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'UnitName', 'template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'WorkOrder', 'template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Approvers', 'template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'InvoiceNo', 'template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Quantity', 'template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'GLAccount', 'template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Description', 'template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'ItemAmount', 'template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'GrandTotal', 'template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PMPhone', 'template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PMEmail', 'template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'PurchaseOrderArea', 'template_key' =>'PurchaseOrder_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),



                array('tag_name' => 'Signer Signature', 'template_key' =>'DelinquentReports_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Signer Entered Text', 'template_key' =>'DelinquentReports_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Signer Entered Date', 'template_key' =>'DelinquentReports_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Signer Checkbox', 'template_key' =>'DelinquentReports_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Originator Signature', 'template_key' =>'DelinquentReports_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Originator Entered Text', 'template_key' =>'DelinquentReports_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );



            $stm = $connection->prepare("TRUNCATE TABLE tags_emailtemplate");



            $stm->execute();



            foreach($data as $rows)

            {

                $query = "INSERT INTO `tags_emailtemplate`( `tag_name`,`template_key`,`created_at`,`updated_at`) VALUES 

                    ('{$rows['tag_name']}','{$rows['template_key']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

            print_r("sfa");



        }catch (Exception $exception)

        {



        }

    }



    /*seeder for inseption proprty module*/

    public static function insertEmailTemplatesAdmin($connection)

    {

        include_once( SUPERADMIN_DIRECTORY_URL."/helper/emailTemplateSeeder.php");

    }
    public static function insertMarketingTemplatesAdmin($connection)

    {

        include_once( SUPERADMIN_DIRECTORY_URL."/helper/marketingTemplateSeeder.php");

    }



    /*seeder for Vendors and owners*/

    public static function insertReasonMove($connection)

    {

        try{



            $data = array(

                array('reasonName' => 'Bad Health','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Bought a New Residence','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Bought a New Place','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Death in the Family','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Family Issues','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Moved Home','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Divorce','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Marriage','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Moving to Less Expensive Residence','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Moving Out of Area','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Moving to another Country','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Moving close to Family','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Moving close to Job','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Moving close to School','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Moving to a nicer Neighborhood','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Moving close to Public Transportation','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Need a Larger Residence','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'New Business','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Small House','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Job Transfer/Loss','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Retired','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('reasonName' => 'Other','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),



            );

            $stm = $connection->prepare("TRUNCATE TABLE reasonmoveout_tenant");
            $stm->execute();

            foreach ($data as $rows) {

                $query = "INSERT INTO `reasonmoveout_tenant`( `reasonName`,`created_at`,`updated_at`) VALUES ('{$rows['reasonName']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);



        }catch (Exception $exception) {

            print_r($exception);

        }

    }

    public static function insertTagsLettersNoticesDetails($connection){
        try{
            $data = array(
                array('tag_name' => 'PropertyName', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Property_Address', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Property_City', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Property_State', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Property_Zip', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Property_Country', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Unit_Name', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'TenantLeaseId', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'LeaseStartDate', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'LeaseEndDate', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'LandLord_NameTemp', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Occupant_NameTemp', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Tenant_Name', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_Name', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_Address', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_City', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_State', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_Country', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_Zip', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_Number', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'LandLord_Number', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'RentersName', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'LandLord_Name', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'GuarantorName', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Occupant_Name', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Pet_Type', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Pet_Weight', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Pet_Name', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Pet_Breed', 'letter_type' => '1', 'template_key' =>'lease_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'Company_Name', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_Address', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_City', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_State', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_Country', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_Zip', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_Number', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Occupant_Name', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Tenant_Name', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Salutation', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'PropertyName', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Property_Country', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Property_Address', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Property_Zip', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Property_State', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Property_City', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Lease_StartDate', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Unit_Name', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'CurrentDate', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Owner_Name', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Owner_Email', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Owner_Number', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Owner_Address', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Owner_City', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Owner_ST', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Guarantor_Name', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Tenant_Amount', 'letter_type' => '2', 'template_key' =>'tenant_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('tag_name' => 'TotalBalance', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Pending_Payment', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_Name', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_Address', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_State', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_Country', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_Zip', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_Number', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_Email', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Company_FaxNumber', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'LeaseStartDate', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'LeaseTerm', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'MoveOutDate', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'LeaseEndDate', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'PropertySize', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'LateFeeOfAmount', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'GracePeriod', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'NextRentIncreaseMonth', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'NextRentIncreaseType', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'NextRentIncreaseAmount', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'NoticePeriod', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'MoveOutNoticeDate', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'CamAmount', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Security_Deposit', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'RentAmount', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'AmountDueDate', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'DueDate', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Moving_Date', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'MoveOut_Date', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'InsufficientAmount', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'LandLord_Number', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'GuarantorName', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Unit_Name', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Building_Name', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Property_Name', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Property_Address', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Property_City', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Property_State', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Property_Zip', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Property_Country', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Is_Smoke', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Is_Pet', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Tenant_DOB', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Charges_Amount', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Tenant_Name', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Occupant_Name', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Occupants_Count', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Tenant_SSN', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Tenant_Email', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Tenant_Number', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Salutation', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'OwnerID', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'OwnerName', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'LandLord_Name', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Owner_Company', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'CompanyName', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'BusinessName', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'StreetAddress', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Zip', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Owner_Zip', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Owner_Country', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Country', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'City', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Owner_City', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Owner_State', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'State', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Owner_Email', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'EmailId', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'LandLord_FaxNumber', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'TotalLateFeeAndRentAmount', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Vehicle_Name', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Vehicle_Color', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Vehicle_Plate', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Vehicle_Make', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Vehicle_Year', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Pet_Name', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Pet_Type', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Pet_Age', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Pet_Weight', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Pet_Sex', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Pet_Note', 'letter_type' => '3', 'template_key' =>'landlord_owner_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),


                array('tag_name' => 'Signer_Signature', 'letter_type' => '4', 'template_key' =>'e_sign_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Signer_Entered_Text', 'letter_type' => '4', 'template_key' =>'e_sign_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Signer_Entered_Date', 'letter_type' => '4', 'template_key' =>'e_sign_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Signer_Checkbox', 'letter_type' => '4', 'template_key' =>'e_sign_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Originator_Signature', 'letter_type' => '4', 'template_key' =>'e_sign_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Originator_Entered_Text', 'letter_type' => '4', 'template_key' =>'e_sign_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('tag_name' => 'Originator_Entered_Date', 'letter_type' => '4', 'template_key' =>'e_sign_doc_Key', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
            );
            $stm = $connection->prepare("TRUNCATE TABLE tags_letter_notices");
            $stm->execute();
            foreach($data as $rows){
                $query = "INSERT INTO `tags_letter_notices`( `tag_name`,`letter_type`,`template_key`,`created_at`,`updated_at`) VALUES ('{$rows['tag_name']}','{$rows['letter_type']}','{$rows['template_key']}','{$rows['created_at']}','{$rows['updated_at']}')";
                $stm = $connection->prepare($query);
                $stm->execute();
            }
            return array('status' => 'success', 'code'=> 200);

        } catch (Exception $exception) {
            print_r($exception);
        }

    }

    /*seeder for inseption proprty module*/

    /*Communication Letter/Notices Templates */

    public static function insertletterNoticesTemplates($connection)
    {
        include_once( SUPERADMIN_DIRECTORY_URL."/helper/letterNoticesTemplateSeeder.php");
    }

    /*seeder for Vendors and owners*/



    public static function insertVendorsAndOwners($connection)

    {

        try{

//            $userquery = 'SELECT domain_name FROM users WHERE id=1';

//            $userData = $connection->query($userquery)->fetch();

//            if(!empty($userData)) {

            $domain = '';

            $password = md5('Mind@123');



            $data = array(

                //Owner

                array('name' => 'Francis Baker','email' => 'francis@yopmail.com','phone_number'=>'121-415-7452','address1' => 'Street 1','address2'=>'street 2','first_name' => 'Francis','last_name' => 'Baker', 'status' => '1', 'role' => '1', 'parent_id' => '1', 'password' => $password, 'actual_password' => 'Mind@123', 'domain_name' => $domain, 'user_type' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Annie Thompson','email' => 'annie@yopmail.com','phone_number'=>'126-566-6762','address1' => 'Street 1','address2'=>'street 2','first_name' => 'Annie','last_name' => 'Thompson', 'status' => '1', 'role' => '1', 'parent_id' => '1', 'password' => $password, 'actual_password' => 'Mind@123', 'domain_name' => $domain, 'user_type' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Lisa Walker','email' => 'lisa@yopmail.com','phone_number'=>'454-565-8732','address1' => 'Street 1','address2'=>'street 2','first_name' => 'Lisa','last_name' => 'Walker', 'status' => '1', 'role' => '1', 'parent_id' => '1', 'password' => $password, 'actual_password' => 'Mind@123', 'domain_name' => $domain, 'user_type' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Jerry Campbell','email' => 'jerry@yopmail.com','phone_number'=>'526-457-2346','address1' => 'Street 1','address2'=>'street 2','first_name' => 'Jerry','last_name' => 'Campbell', 'status' => '1', 'role' => '1', 'parent_id' => '1', 'password' => $password, 'actual_password' => 'Mind@123', 'domain_name' => $domain, 'user_type' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Janet Jackson','email' => 'janet@yopmail.com','phone_number'=>'457-679-2357','address1' => 'Street 1','address2'=>'street 2','first_name' => 'Janet','last_name' => 'Jackson', 'status' => '1', 'role' => '1', 'parent_id' => '1', 'password' => $password, 'actual_password' => 'Mind@123', 'domain_name' => $domain, 'user_type' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                //Vendor

                array('name' => 'Eddie Robinson','email' => 'eddie@yopmail.com','phone_number'=>'346-454-3467','address1' => 'Street 1','address2'=>'street 2','first_name' => 'Eddie','last_name' => 'Robinson', 'status' => '1', 'role' => '3', 'parent_id' => '1', 'password' => $password, 'actual_password' => 'Mind@123', 'domain_name' => $domain, 'user_type' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Philip Evans','email' => 'philip@yopmail.com','phone_number'=>'126-347-1257','address1' => 'Street 1','address2'=>'street 2','first_name' => 'Philip','last_name' => 'Evans', 'status' => '1', 'role' => '3', 'parent_id' => '1', 'password' => $password, 'actual_password' => 'Mind@123', 'domain_name' => $domain, 'user_type' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Randy Peterson','email' => 'randy@yopmail.com','phone_number'=>'232-3464-4567','address1' => 'Street 1','address2'=>'street 2','first_name' => 'Randy','last_name' => 'Peterson', 'status' => '1', 'role' => '3', 'parent_id' => '1', 'password' => $password, 'actual_password' => 'Mind@123', 'domain_name' => $domain, 'user_type' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Andrea Mitchell','email' => 'andrea@yopmail.com','phone_number'=>'348-347-3468','address1' => 'Street 1','address2'=>'street 2','first_name' => 'Andrea','last_name' => 'Mitchell', 'status' => '1', 'role' => '3', 'parent_id' => '1', 'password' => $password, 'actual_password' => 'Mind@123', 'domain_name' => $domain, 'user_type' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('name' => 'Terry Phillips','email' => 'terry@yopmail.com','phone_number'=>'895-235-3435','address1' => 'Street 1','address2'=>'street 2','first_name' => 'Terry','last_name' => 'Phillips', 'status' => '1', 'role' => '3', 'parent_id' => '1', 'password' => $password, 'actual_password' => 'Mind@123', 'domain_name' => $domain, 'user_type' => '1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );



            foreach ($data as $rows) {

                $query = "INSERT INTO `users`( `name`,`first_name`,`last_name`,`email`,`phone_number`,`address1`,`address2`,`status`,`role`,`parent_id`,`password`,`actual_password`,`domain_name`,`user_type`,`created_at`,`updated_at`) VALUES ('{$rows['name']}','{$rows['first_name']}','{$rows['last_name']}','{$rows['email']}','{$rows['phone_number']}','{$rows['address1']}','{$rows['address2']}','{$rows['status']}','{$rows['role']}','{$rows['parent_id']}','{$rows['password']}','{$rows['actual_password']}','{$rows['domain_name']}','{$rows['user_type']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

//            }

            // return array('status' => 'error', 'code'=> 500);



        }catch (Exception $exception) {

            print_r($exception);

        }

    }



    public static function insertCompanyChartOfAccounts($connection)

    {

        try{

            $data = array(

                array('account_code'=>"1000", 'posting_status' => '0','account_name' => 'Cash in Bank-Operating', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '1','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1010", 'posting_status' => '0','account_name' => 'Cash in Bank-Saving', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1020", 'posting_status' => '0','account_name' => 'Cash in Bank-Money Market', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1030", 'posting_status' => '1','account_name' => 'Security Deposit Bank Account', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1035", 'posting_status' => '0','account_name' => 'Payroll Bank Account', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1040", 'posting_status' => '0','account_name' => 'Petty Cash', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1100", 'posting_status' => '0','account_name' => 'Tenant Receivables', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1150", 'posting_status' => '0','account_name' => 'CAM Receivable', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1175", 'posting_status' => '0','account_name' => 'Accounts Receivable', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1200", 'posting_status' => '0','account_name' => 'Prepaid 4s', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1290", 'posting_status' => '0','account_name' => 'Other Current Assets', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1300", 'posting_status' => '0','account_name' => 'Furniture & Fixtures', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1320", 'posting_status' => '0','account_name' => 'Equipment ', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1325", 'posting_status' => '0','account_name' => 'Vehicles', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1350", 'posting_status' => '0','account_name' => 'Leasehold Improvements', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1400", 'posting_status' => '0','account_name' => 'Building', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1450", 'posting_status' => '0','account_name' => 'Land', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1500", 'posting_status' => '0','account_name' => 'Accum. Depr. Furn. & Fix', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1520", 'posting_status' => '0','account_name' => 'Accum. Depr. Equipment', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1525", 'posting_status' => '0','account_name' => 'Accum. Depr. Vehicles', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1550", 'posting_status' => '0','account_name' => 'Accum. Depr. Building', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1575", 'posting_status' => '0','account_name' => 'Accum. Amort. Leasehold', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1600", 'posting_status' => '0','account_name' => 'Security Deposit', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"1700", 'posting_status' => '0','account_name' => 'Intercompany Accounts Receivable', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),



                array('account_code'=>"2000", 'posting_status' => '0','account_name' => 'Accounts Payable', 'account_type_id'=>'2','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"2010", 'posting_status' => '0','account_name' => 'Security Deposit 2', 'account_type_id'=>'2','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"2020", 'posting_status' => '0','account_name' => 'Prepaid Rent 2', 'account_type_id'=>'2','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"2030", 'posting_status' => '0','account_name' => 'Notes Payable', 'account_type_id'=>'2','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"2100", 'posting_status' => '0','account_name' => 'Fed. 5 Tax Withheld', 'account_type_id'=>'2','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"2110", 'posting_status' => '0','account_name' => 'FICA Tax Withheld', 'account_type_id'=>'2','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"2115", 'posting_status' => '0','account_name' => 'Medicare Tax Withheld', 'account_type_id'=>'2','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"2120", 'posting_status' => '0','account_name' => 'State 5 Tax Withheld', 'account_type_id'=>'2','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"2125", 'posting_status' => '0','account_name' => 'SUTA Tax Withheld', 'account_type_id'=>'2','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"2130", 'posting_status' => '0','account_name' => 'Sales Tax Payable', 'account_type_id'=>'2','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"2140", 'posting_status' => '0','account_name' => '5 Taxes Payable', 'account_type_id'=>'2','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"2290", 'posting_status' => '0','account_name' => 'Other Current Liabilities', 'account_type_id'=>'2','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"2295", 'posting_status' => '0','account_name' => 'Intercompany Accounts Payable', 'account_type_id'=>'2','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"2300", 'posting_status' => '0','account_name' => 'Contracts Payable', 'account_type_id'=>'2','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"2400", 'posting_status' => '0','account_name' => 'Mortgage Payable', 'account_type_id'=>'2','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"2500", 'posting_status' => '0','account_name' => 'Long Term Notes Payable', 'account_type_id'=>'2','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"2990", 'posting_status' => '0','account_name' => 'Other Long Term Liabilities', 'account_type_id'=>'2','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),



                array('account_code'=>"3000", 'posting_status' => '0','account_name' => 'Retained Earnings', 'account_type_id'=>'3','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"3010", 'posting_status' => '0','account_name' => 'Owner Draw', 'account_type_id'=>'3','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"3020", 'posting_status' => '0','account_name' => 'Owner Contribution of Capital', 'account_type_id'=>'3','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"3999", 'posting_status' => '0','account_name' => 'Current Year Earnings/Loss', 'account_type_id'=>'3','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),



                array('account_code'=>"4000", 'posting_status' => '0','account_name' => 'Rental Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4020", 'posting_status' => '0','account_name' => 'Parking Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4030", 'posting_status' => '0','account_name' => 'Application Fee Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4040", 'posting_status' => '0','account_name' => 'Cleaning Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4060", 'posting_status' => '0','account_name' => 'Laundry Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4080", 'posting_status' => '0','account_name' => 'Late Charge Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4110", 'posting_status' => '0','account_name' => 'Prepaid Rent Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4120", 'posting_status' => '0','account_name' => 'NSF Fee Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4140", 'posting_status' => '0','account_name' => 'Maint & Repairs Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4160", 'posting_status' => '0','account_name' => 'Interest Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4170", 'posting_status' => '0','account_name' => 'Electricity Utility Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4171", 'posting_status' => '0','account_name' => 'HOA Dues', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4180", 'posting_status' => '0','account_name' => 'Water Utility Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4190", 'posting_status' => '0','account_name' => 'Gas Utility Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4200", 'posting_status' => '0','account_name' => 'CAM Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4300", 'posting_status' => '0','account_name' => 'Intercompany Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4490", 'posting_status' => '0','account_name' => 'Other Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),



                array('account_code'=>"5000", 'posting_status' => '0','account_name' => 'Administrative Salaries', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5010", 'posting_status' => '0','account_name' => 'Management Fees', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5020", 'posting_status' => '0','account_name' => 'Manager Salaries', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5030", 'posting_status' => '0','account_name' => 'Clerical Salaries', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5040", 'posting_status' => '0','account_name' => 'Maintenance Salaries', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5050", 'posting_status' => '0','account_name' => 'Payroll Taxes', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5060", 'posting_status' => '0','account_name' => 'Employee Benefits', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5100", 'posting_status' => '0','account_name' => 'Advertising', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5110", 'posting_status' => '0','account_name' => 'Office Supplies', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5120", 'posting_status' => '0','account_name' => 'Dues & Subscriptions', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5130", 'posting_status' => '0','account_name' => 'Postage & Delivery', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5140", 'posting_status' => '0','account_name' => 'Telephone', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5190", 'posting_status' => '0','account_name' => 'Other Administrative 4s', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5200", 'posting_status' => '0','account_name' => 'Maintenance', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5210", 'posting_status' => '0','account_name' => 'Painting & Decorating', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5220", 'posting_status' => '0','account_name' => 'Landscaping', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5230", 'posting_status' => '0','account_name' => 'Cleaning', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5250", 'posting_status' => '0','account_name' => 'Plumbing', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5260", 'posting_status' => '0','account_name' => 'Security', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5270", 'posting_status' => '0','account_name' => 'Building Supplies', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5290", 'posting_status' => '0','account_name' => 'Other Operating 4s', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5300", 'posting_status' => '0','account_name' => 'Electricity', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5310", 'posting_status' => '0','account_name' => 'Water & Sanitation', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5320", 'posting_status' => '0','account_name' => 'Trash Collection', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5330", 'posting_status' => '0','account_name' => 'Natural Gas', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5390", 'posting_status' => '0','account_name' => 'Other Utilities/Cable', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5400", 'posting_status' => '0','account_name' => 'Travel & Entertainment', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5410", 'posting_status' => '0','account_name' => 'Bank Charges', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5420", 'posting_status' => '0','account_name' => 'Interest', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5430", 'posting_status' => '0','account_name' => 'Other Interest', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5440", 'posting_status' => '0','account_name' => 'Legal & Accounting', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5490", 'posting_status' => '0','account_name' => 'Data Processing', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5500", 'posting_status' => '0','account_name' => 'Real Estate Taxes', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5510", 'posting_status' => '0','account_name' => 'Fees & Permits', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5520", 'posting_status' => '0','account_name' => 'Insurance', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5530", 'posting_status' => '0','account_name' => 'Depreciation 4', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"6150", 'posting_status' => '0','account_name' => 'Commissions', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"6200", 'posting_status' => '0','account_name' => 'Repairs & Maintenance', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"6250", 'posting_status' => '0','account_name' => 'Parking Lot Sweeping', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"6300", 'posting_status' => '0','account_name' => 'Supplies', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"7990", 'posting_status' => '0','account_name' => 'Other Operating 4s', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),



                array('account_code'=>"8000", 'posting_status' => '0','account_name' => 'Non-Operating Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),



                array('account_code'=>"9000", 'posting_status' => '0','account_name' => 'Non-Operating 4s-Taxes', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"7000", 'posting_status' => '0','account_name' => 'Bad Debt Expense', 'account_type_id'=>'4','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),



                array('account_code'=>"4450", 'posting_status' => '0','account_name' => 'Less: Concessions', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4185", 'posting_status' => '0','account_name' => 'Sewer Utility Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4187", 'posting_status' => '0','account_name' => 'A/C Utility Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4188", 'posting_status' => '0','account_name' => 'Waste Disposal Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),



                array('account_code'=>"1125", 'posting_status' => '0','account_name' => 'Tenant Receivable-Utilities', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4250", 'posting_status' => '0','account_name' => 'GST Tax Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4251", 'posting_status' => '0','account_name' => 'HST Tax Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4252", 'posting_status' => '0','account_name' => 'QST Tax Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4253", 'posting_status' => '0','account_name' => 'PST Tax Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"4254", 'posting_status' => '0','account_name' => 'VAT Tax Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),



                array('account_code'=>"1128", 'posting_status' => '0','account_name' => 'Tenant Receivable-Taxes', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),



                array('account_code'=>"5331", 'posting_status' => '0','account_name' => 'Cable TV Utility Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5337", 'posting_status' => '0','account_name' => 'Heating Oil Utility Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5332", 'posting_status' => '0','account_name' => 'Internet Utility Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

                array('account_code'=>"5335", 'posting_status' => '0','account_name' => 'Telephone Utility Income', 'account_type_id'=>'5','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),



                array('account_code'=>"1231", 'posting_status' => '0','account_name' => 'Pet Fees Income', 'account_type_id'=>'1','reporting_code'=> '1','is_editable'=>'0','is_default' => '0','user_id' => '1', 'status' => '1','created_at'=> date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_chart_of_accounts");

            $stm->execute();



            foreach($data as $rows)

            {

                $query = "INSERT INTO  `company_chart_of_accounts`( `account_code`,`posting_status`,`account_name`,`account_type_id`,`reporting_code`,`user_id`,`created_at`,`updated_at`,`status`,`is_editable`,`is_default`) VALUES

                                               ('{$rows['account_code']}','{$rows['posting_status']}','{$rows['account_name']}','{$rows['account_type_id']}','{$rows['reporting_code']}','{$rows['user_id']}','{$rows['created_at']}','{$rows['updated_at']}','{$rows['status']}','{$rows['is_editable']}','{$rows['is_default']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }



    public static function insertCompanyAccountTypeDetails($connection)

    {

        try{

            $data = array(

                array('account_type' => 'Asset','account_type_id' => '1', "user_id" =>1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('account_type' => 'Expense','account_type_id' => '2', "user_id" =>1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('account_type' => 'Equity','account_type_id' => '3', "user_id" =>1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('account_type' => 'Liability','account_type_id' => '4', "user_id" =>1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('account_type' => 'Income','account_type_id' => '5', "user_id" =>1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE account_type_details");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `account_type_details`( `account_type`,`account_type_id`,`user_id`,`created_at`,`updated_at`) VALUES ('{$rows['account_type']}','{$rows['account_type_id']}','{$rows['user_id']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }

    }



    public static function insertUserAlerts($connection)

    {

        try{



            $data = array(

                array('alert_name' => 'T/Portal Login Request Alert', 'description' => 'This real time alert is sent to specified users and respective tenant with account creation email along with credentials', 'user_id' => 1,'no_of_days_before'=>'','subject'=>'T/Portal Login Request Alert','send_to_users'=>'','module_type'=>'tenant','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'New Account has been created. Kindly login to tenant portal with the details shared with you in email'),

                array('alert_name' => 'Tenant Information Update', 'description' => "This real time alert is sent to specified users whenever any tenants information get updated", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Tenant Information Update','send_to_users'=>'','module_type'=>'tenant','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'The information of Tenant ID-XXXX ( Name -XXXX) is been updated.'),

                array('alert_name' => 'Tenant Make Payment', 'description' => "This real time alert is sent to specified users that a tenant has made payment", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Tenant Make Payment','send_to_users'=>'','module_type'=>'tenant','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Tenant with an ID-XXXX (Name -XXXX , Property ID-XXXX) has made payment of $XXX & transaction ID - XXXX.'),

                array('alert_name' => 'Tenant Gave Notice', 'description' => "This real time alert is sent to specified users that a tenant has given notice to vacate.", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Tenant Gave Notice','send_to_users'=>'','module_type'=>'tenant','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Tenant ID-XXXX (XXXX, Property ID - XXXX, Unit # -XXX) has given a notice to vacate the unit on DD MM,YYYY'),

                array('alert_name' => 'Send Tenant Monthly Rent Invoices', 'description' => "This alert is sent to specified users on a set number of days till last day of the current month to automatically send the invoices .", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Send Tenant Monthly Rent Invoices','send_to_users'=>'','module_type'=>'tenant','alert_type'=>'Days Specific', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Tenant with an ID-XXXX(Name -XXXX Property ID- XXXX has its monthly rent XXXX.'),

                array('alert_name' => 'Tenant Added Comment', 'description' => "This real time alert is sent to specified users that comment has added in tenants account", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Tenant Added Comment','send_to_users'=>'','module_type'=>'tenant','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'The comment has added in the account of tenant with an ID-XXXX ( Name - XXXX , Property ID -XXXX, Unit # XXXX).'),

                array('alert_name' => 'Tenant Rental Ins. Purchased', 'description' => "This real time alert is sent to specified users that the tenant has purchase tenant rental insurance", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Tenant Rental Ins. Purchased','send_to_users'=>'','module_type'=>'tenant','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Tenant with an ID -XXXX has purchased a rental insurance'),

                array('alert_name' => 'Tenant Rental Ins. Expiring', 'description' => "This alert is sent to specified users on a set number of days till last day of the current month that tenant rental insurance is expiring .", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Tenant Rental Ins. Expiring','send_to_users'=>'','module_type'=>'tenant','alert_type'=>'Days Specific', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Rental Insurance of tenant ID -XXXX(Julia Roberts, Property ID - XXXX, Unit # -XXXX) will be expired on DD/MM/YY'),

                array('alert_name' => 'Tenant Work Order Confirmation', 'description' => "This real time alert is sent to specified users that the tenant has generated a work order request", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Tenant Work Order Confirmation','send_to_users'=>'','module_type'=>'tenant','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Work Order is been generated by a Tenant ID - XXXX (Name : XXXX, Property ID -XXXX, Unit XXXX).'),

                array('alert_name' => 'Tenant Ticket Canceled', 'description' => "This real time alert is sent to specified users if ticket is canceled", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Tenant Ticket Canceled','send_to_users'=>'','module_type'=>'tenant','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Ticket Number -XXXX has been canceled for Property ID- XXXX, Building- XXXX, Unit- XXXX.'),

                array('alert_name' => 'Owner Account Created', 'description' => "This real time alert is sent to specified users and respective owner with account creation email along with credentials", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Owner Account Created','send_to_users'=>'','module_type'=>'owner','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'New Account for owner has been created'),

                array('alert_name' => 'Owner Work Order Alert', 'description' => "This real time alert is sent to specified users when an owner gets a work order alert", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Owner Account Created','send_to_users'=>'','module_type'=>'owner','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Work Order is been generated by a Tenant ID - XXXX (Name : XXXX, Property ID -XXXX, Unit XXXX)'),

                array('alert_name' => 'Owner Added a Comment', 'description' => "This real time alert is sent to specified users when an owner adds a comment", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Owner Added a Comment','send_to_users'=>'','module_type'=>'owner','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'The comment has added in the account of owner with an ID-XXXX ( Name - XXXX)'),

                array('alert_name' => 'Ticket Disapprove', 'description' => "This real time alert is sent to specified users and Tenant if any ticket is disapproved from owner portal", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Ticket Disapprove','send_to_users'=>'','module_type'=>'owner','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>''),

                array('alert_name' => 'Vendor Portal Created', 'description' => "This real time alert is sent to specified users and respective owner with account creation email along with credentials", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Vendor Portal Created','send_to_users'=>'','module_type'=>'vendor','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'New Portal for Vendor has been Created'),

                array('alert_name' => 'Vendor WorkOrder Alert', 'description' => "This real time alert is sent to specified users when an owner gets a work order alert", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Vendor WorkOrder Alert','send_to_users'=>'','module_type'=>'vendor','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Work Order is been generated by a Tenant ID - XXXX (Name : XXXX, Property ID -XXXX, Unit XXXX)'),

                array('alert_name' => 'Vendor Added a Comment', 'description' => "This real time alert is sent to specified users when an owner adds a comment", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Vendor Added a Comment','send_to_users'=>'','module_type'=>'vendor','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'The comment has added in the account of Vendor with an ID-XXXX ( Name - XXXX)'),

                array('alert_name' => 'Vendor Work Order Canceled', 'description' => "This real time alert is sent to specified users if work order is canceled", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Vendor Work Order Canceled','send_to_users'=>'','module_type'=>'vendor','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Work Order Number -XXXX has been canceled for Property ID- XXXX, Building- XXXX, Unit- XXXX.'),

                array('alert_name' => 'Start Date', 'description' => "This alert is sent to specified recipients on a set number of days prior to the start date of a new lease.", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Start Date','send_to_users'=>'','module_type'=>'lease','alert_type'=>'Days Specific', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Start Date of Lease ID - XXXX is falling on DD/MM/YYYY'),

                array('alert_name' => 'Schedule Move Out', 'description' => "This alert is sent to specified recipients on a set number of days prior to the scheduled Move-Out.", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Schedule Move Out','send_to_users'=>'','module_type'=>'lease','alert_type'=>'Days Specific', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Move oUt date for the Tenant ID-XXXX(Name - XXXX PropertyID-XXXX) is falling on DD/MM/YYYY'),

                array('alert_name' => 'Auto Charge Start', 'description' => "This alert is sent to specified recipients on a set number of days prior to start applying charges.", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Auto Charge Start','send_to_users'=>'','module_type'=>'lease','alert_type'=>'Days Specific', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Charges for the Tenant ID-XXXX will be applied on DD/MM/YYYY'),

                array('alert_name' => 'Auto Charge End', 'description' => "This alert is sent to specified recipients on a set number of days prior to stop applying charges.", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Auto Charge End','send_to_users'=>'','module_type'=>'lease','alert_type'=>'Days Specific', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Charges for the Tenant ID-XXXX will be closed on DD/MM/YYYY'),

                array('alert_name' => 'Lease Signed', 'description' => "This real time alert is sent to specified users when any new lease is signed or generated.", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Lease Signed','send_to_users'=>'','module_type'=>'lease','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'New Lease (ID -XXXX) is generated.'),

                array('alert_name' => 'First Notice of Lease Expiration	', 'description' => "This alert is sent to specified recipients on a set number of days prior to send a first notice for lease expiration.", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'First Notice of Lease Expiration','send_to_users'=>'','module_type'=>'lease','alert_type'=>'Days Specific', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Lease with an ID - XXXX will be expired on DD/MM/YYYY'),

                array('alert_name' => 'Second Notice of Lease Expiration', 'description' => "This alert is sent to specified recipients on a set number of days prior to send a second notice for lease expiration", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Second Notice of Lease Expiration','send_to_users'=>'','module_type'=>'lease','alert_type'=>'Days Specific', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Lease with an ID - XXXX will be expired on DD/MM/YYYY'),

                array('alert_name' => 'First Past Due Tenant Notification', 'description' => "This alert is sent to specified recipients on a set number of days prior The system can be set to alert the PM every 2 or 3 days of all past dues.", 'user_id' =>1,'no_of_days_before'=>'','subject'=>'First Past Due Tenant Notification','send_to_users'=>'','module_type'=>'lease','alert_type'=>'Days Specific', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Outstanding Balance or past dues of tenant ID-XXXX (Name : Julia Robert Property ID-XXXX, Unit XXXX) are XXXX'),

                array('alert_name' => 'Second Past Due Tenant Notification', 'description' => "This alert is sent to specified recipients on a set number of days prior The system can be set to alert the PM every 2 or 3 days of all past dues.", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'First Past Due Tenant Notification','send_to_users'=>'','module_type'=>'lease','alert_type'=>'Days Specific', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Outstanding Balance or past dues of tenant ID-XXXX (Name : Julia Robert Property ID-XXXX, Unit XXXX) are XXXX'),

                array('alert_name' => 'Received Tenant Notice to Vacate', 'description' => "This real time alert is sent to specified users when a notice is received from tenant to vacate the unit", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Received Tenant Notice to Vacate','send_to_users'=>'','module_type'=>'lease','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Tenant with an ID -XXXX (Name-XXXX, Property - XXXX, Unit XXXX) has given a notice to vacate'),

                array('alert_name' => 'New Tenant Notification', 'description' => "This real time alert is sent to specified users when any new tenant start residing with us i.e. date of Move In", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'New Tenant Notification','send_to_users'=>'','module_type'=>'lease','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'New Tenant has been Moved in Property -XXXX (Unit XXXX)'),

                array('alert_name' => 'Insufficient Fund Notification', 'description' => "This real time alert is sent to specified users if tenant has non-sufficient funds and check got bounced.", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Insufficient Fund Notification','send_to_users'=>'','module_type'=>'lease','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Tenant with an ID-XXXX has non-sufficient funds and is marked as NSF.'),

                array('alert_name' => 'Work Order Created', 'description' => "This real time alert is sent to specified users if new work order is generated by a tenant", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Work Order Created','send_to_users'=>'','module_type'=>'maintenance','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Work order with an ID-XXXX is created.'),

                array('alert_name' => 'Work Order Changed', 'description' => "This real time alert is sent to specified users if any modification is done in the work order", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Work Order Changed','send_to_users'=>'','module_type'=>'maintenance','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Work order with an ID-XXXX is updated.'),

                array('alert_name' => 'Work Order Completed', 'description' => "This real time alert is sent to specified users if work order is completed", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Work Order Completed','send_to_users'=>'','module_type'=>'maintenance','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Work Order with an ID-XXXX is closed.'),

                array('alert_name' => 'Work Order Canceled', 'description' => "This real time alert is sent to specified users if work order is canceled", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Work Order Canceled','send_to_users'=>'','module_type'=>'maintenance','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Work Order Number -XXXX has been canceled for Property ID- XXXX, Building- XXXX, Unit- XXXX.'),

                array('alert_name' => 'Ticket Canceled', 'description' => "This real time alert is sent to specified users if ticket is canceled", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Ticket Canceled','send_to_users'=>'','module_type'=>'maintenance','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Ticket Number -XXXX has been canceled for Property ID- XXXX, Building- XXXX, Unit- XXXX.'),

                array('alert_name' => 'Work Order Cancel', 'description' => "This real time alert is sent to specified users if work order is cancel", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Work Order Cancel','send_to_users'=>'','module_type'=>'maintenance','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Work Order with an ID-XXXX is Canceled.'),

                array('alert_name' => 'New Prospect', 'description' => "This real time alert is sent to specified users when a new guest card is created.", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'New Prospect','send_to_users'=>'','module_type'=>'lead','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'New prospect has been created with a Guest Card number - XXXX.'),

                array('alert_name' => 'New Applicant', 'description' => "This real time alert is sent to specified users when a new rental application is created.", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'New Applicant','send_to_users'=>'','module_type'=>'lead','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'New applicant is been created with a Rental Application - XXXX.'),

                array('alert_name' => 'Electronic Payment Failed', 'description' => "This real time alert is sent to specified users and tenants that electronic payment got failed.", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Electronic Payment Failed','send_to_users'=>'','module_type'=>'payment','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'EFT file with ID -XXXX is not processed and got failed.'),

                array('alert_name' => 'Tenant Conversation Posted to Portal', 'description' => "This real time alert is sent to specified users and tenant if tenants conversation is posted to portal", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Tenant Conversation Posted to Portal','send_to_users'=>'','module_type'=>'communication','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Conversation of the Tenant ID -XXXX is posted to the Tenant Portal.'),

                array('alert_name' => 'Document Posted to Portal', 'description' => "This real time alert is sent to specified users and tenant if any document is posted to any portal", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Document Posted to Portal','send_to_users'=>'','module_type'=>'communication','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Document (XXXX) is posted to the Tenant Portal(Tenant ID-XXXX)'),

                array('alert_name' => 'Property Insurance is Expiring', 'description' => "This real time alert is sent to specified users for propertys Insurance expiration", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Property Insurance is Expiring','send_to_users'=>'','module_type'=>'business','alert_type'=>'Days Specific', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Insurance of the Property - XXXX Manor will be expired on DD/MM/YYYY'),

                array('alert_name' => 'Owner’s Conversation Postal to Portal', 'description' => "This real time alert is sent to specified users and owner if owners conversation is posted to portal", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Owner’s Conversation Postal to Portal','send_to_users'=>'','module_type'=>'business','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>"New Conversation is posted to the Owners Portal(Owner ID -XXXX, Name -XXXX)"),

                array('alert_name' => 'Pending Deposit to Account', 'description' => "If there is money in the system that has been received for rent but not yet deposited, the system should alert the PM", 'user_id' => 1,'no_of_days_before'=>'','subject'=>'Pending Deposit to Account','send_to_users'=>'','module_type'=>'business','alert_type'=>'Real Time', 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'alert_message'=>'Created batch needs to be deposited.'),

            );

            $stm = $connection->prepare("TRUNCATE TABLE user_alerts");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `user_alerts`(`user_id`,`alert_name`,`description`,`subject`,`send_to_users`,`module_type`,`alert_type`,`status`,`created_at`,`updated_at`,`no_of_days_before`,`alert_message`) VALUES ('{$rows['user_id']}','{$rows['alert_name']}','{$rows['description']}','{$rows['subject']}','{$rows['send_to_users']}','{$rows['module_type']}','{$rows['alert_type']}','{$rows['status']}','{$rows['created_at']}','{$rows['updated_at']}','{$rows['no_of_days_before']}','{$rows['alert_message']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }



    }


    public static function insertInTouchType($connection)
    {
        try{
            $data = array(
                    array("user_id" =>1,'in_touch_type'=>'Access Code','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Accounting','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Address','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Accounts Receivable','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Accounts Payable','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Apartment','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Applicant','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Available','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Background Check','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Ball Park','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Banking','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Basketball Court','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Bedroom','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Billing','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Budgeting','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Building','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Calendar','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Cafeteria','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Checks','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Church','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'City','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Client','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Club','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'College','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Communication','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Commercial','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Condominium','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Contact','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Contract','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Construction','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Contract Renewal','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Country','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Co-Tenant','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Credentialing','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Credit Check','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Custom Field','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Dashboard','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Department','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Drug Test','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Duplex','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Electrical','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Email','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Employee','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Emergency','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'E Signature','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'EMT','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Evaluation','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Eviction','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Farm','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Fees','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Fitness Center','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Financial','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Flat','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Flyer','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Fourplex','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Furniture','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Garage','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Garage Sale','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Golf Course','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Government','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Grocery store','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Guest Card','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'GYM','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'HOA','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Hospital','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Identification','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Industrial Building','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Insurance','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Internet','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Interview','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Issues','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Inspection','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Inventory','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Invoicing','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'In-Touch','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Key Tracker','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Knowledge Base','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Land','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Landscaping','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Laundry Facility','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Lease','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Letters','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Location','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Lost &amp; Found','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Loud Music','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Maintenance','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Management','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Manufacturing','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Market','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Marketing','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Marina','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Medical','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Medical office','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Meeting','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Memo','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Military','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Mini Storage','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Mobile Device','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Mobile Home','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Mobile Home Park','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Move-In','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Move-Out','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Movie','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Multi Family Home','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Night Call','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'NFS','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Notice','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Office','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Opportunity','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Other','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Owner','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Parking','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Parking Lot','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Payroll','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Payment','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Pet','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'People','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Permit','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Phone','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Placement Call','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Play Ground','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Price','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Printing','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Property','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Police','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Pool','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Portal','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Postal Code','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Portfolio','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Purchase Order','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Quote','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Ranch','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Rate','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Referral','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Recreation Center','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Rent','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Registration','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Register','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Renovation','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Reports','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Resident','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Retail Shopping Center','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Sales','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Satellite','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Section 8','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Security Deposit','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Scheduling','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'School','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Signature','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Single Family Home','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Site Visit','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Stadium','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Staff','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Storage','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Street Parking','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Student Housing','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Social Media','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Super Market','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Tenant','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Tennis Court','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Text Messaging','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Ticket','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Time Share','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Town','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Town Home','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Training','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Transportation','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Triplex','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Unit','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'University','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Urgent','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Urgent Care','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'User','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Utilities','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Vacation','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Vendor','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Village','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Violation','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Wi-Fi','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Work Order','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Workshop','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),
                    array("user_id" =>1,'in_touch_type'=>'Zip Code','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'),'status'=>'1'),

                );
            $stm = $connection->prepare("TRUNCATE TABLE in_touch_type");
            $stm->execute();
            foreach($data as $rows)
            {
                $query = "INSERT INTO  `in_touch_type`( `user_id`,`in_touch_type`,`created_at`,`updated_at`,`status`) VALUES ('{$rows['user_id']}'
                ,'{$rows['in_touch_type']}','{$rows['created_at']}','{$rows['updated_at']}','{$rows['status']}')";
                $stm = $connection->prepare($query);
                $stm->execute();
            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)
        {

            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }

    }

    /*seeder for inseption proprty module*/
    /*Communication Letter/Notices Templates */



    /* seeder for caller type */
    public static function insertCallerTypeDetails($connection)

    {

        try{

            $data = array(

                array("user_id" =>1,'call_type' => 'Accounts Payable','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" =>1,'call_type' => 'Accounts Receivable','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" =>1,'call_type' => 'Theft','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Complaint','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array("user_id" =>1,'call_type' => 'Concerns','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Domestic Violence','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Employee Issue','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Family Emergency','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Fire Hazard','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Garage Issue','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'General','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'General Questions','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Landscaping Concerns','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Legal Department','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Lawn Service  Issue','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Lease Issue','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Lost Item','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Payment Issue','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Parking Issue','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Pet Concern','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Pest Control Issue','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Property Destruction','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Rental Contract Issue','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Repair Issue','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Sales','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Security','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Storage Issue','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Suspicious Person','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Trash Collection Issue','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Utility Service Issue','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Water Issue','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Weather Issue','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Wi-Fi Issue','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array("user_id" =>1,'call_type' => 'Other','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE call_type");

            $stm->execute();

            foreach($data as $rows)

            {

                $query = "INSERT INTO  `call_type`( `user_id`,`call_type`,`created_at`,`updated_at`) VALUES ('{$rows['user_id']}','{$rows['call_type']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);

        }catch (Exception $exception)

        {

            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;

        }

    }

    public static function insertProblemCategoryTable($connection){
        try{
            $data = array(
                array('problem_category_name' => 'No Reply to Emails','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('problem_category_name' => 'No Return Calls','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('problem_category_name' => 'Maintenance Issue','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('problem_category_name' => 'Personal Issues','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('problem_category_name' => 'Neighbor Problems','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('problem_category_name' => 'Loud Noise','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('problem_category_name' => 'Other','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
            );

            $stm = $connection->prepare("TRUNCATE TABLE conversation_problem_category");
            $stm->execute();
            foreach($data as $rows) {
                $query = "INSERT INTO `conversation_problem_category`( `problem_category_name`,`created_at`,`updated_at`) VALUES ('{$rows['problem_category_name']}','{$rows['created_at']}','{$rows['updated_at']}')";
                $stm = $connection->prepare($query);
                $stm->execute();
            }
            return array('status' => 'success', 'code'=> 200);
            print_r("sfa");
        }catch (Exception $exception) {
            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;
        }
    }

    public static function insertReasonsTable($connection){
        try{
            $data = array(
                array('reason' => 'Apartment Marketing','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Appointment','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Birthday Party','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Business Dinner','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Board Meeting','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Charitable Event','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Complaint','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Conference','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Community Activity','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Dinner','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Inquiry','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Family Event','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Golf Event','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Meeting','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Move In','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Move Out','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Neighborhood Event','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Networking Event','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Opening Ceremony','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Open House','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Party','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Rent Payment','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Resident Appreciation','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Report','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Shareholder Meetings','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Seminar','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Showing','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Site Visit','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Summer Party','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Trade Show','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Wedding','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('reason' => 'Other','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'))
            );

            $stm = $connection->prepare("TRUNCATE TABLE reasons");
            $stm->execute();
            foreach($data as $rows) {
                $query = "INSERT INTO `reasons`( `reason`,`created_at`,`updated_at`) VALUES ('{$rows['reason']}','{$rows['created_at']}','{$rows['updated_at']}')";
                $stm = $connection->prepare($query);
                $stm->execute();
            }
            return array('status' => 'success', 'code'=> 200);
        }catch (Exception $exception) {
            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;
        }
    }





    public static function insertmaintenancecolor($connection){
        try{
            $data = array(
                array('color_name' => 'Beige','created_at' => date('Y-m-d H:i:s')),
                array('color_name' => 'Black','created_at' => date('Y-m-d H:i:s')),
                array('color_name' => 'Blue','created_at' => date('Y-m-d H:i:s')),
                array('color_name' => 'Brown','created_at' => date('Y-m-d H:i:s')),
                array('color_name' => 'Clear','created_at' => date('Y-m-d H:i:s')),
                array('color_name' => 'Gold','created_at' => date('Y-m-d H:i:s')),
                array('color_name' => 'Gray','created_at' => date('Y-m-d H:i:s')),
                array('color_name' => 'Green','created_at' => date('Y-m-d H:i:s')),
                array('color_name' => 'Mixed colors','created_at' => date('Y-m-d H:i:s')),
                array('color_name' => 'Orange','created_at' => date('Y-m-d H:i:s')),
                array('color_name' => 'Pink','created_at' => date('Y-m-d H:i:s')),
                array('color_name' => 'Purple','created_at' => date('Y-m-d H:i:s')),
                array('color_name' => 'Red','created_at' => date('Y-m-d H:i:s')),
                array('color_name' => 'Silver','created_at' => date('Y-m-d H:i:s')),
                array('color_name' => 'White','created_at' => date('Y-m-d H:i:s')),
                array('color_name' => 'Yellow','created_at' => date('Y-m-d H:i:s')),
                array('color_name' => 'Other','created_at' => date('Y-m-d H:i:s'))
            );

            $stm = $connection->prepare("TRUNCATE TABLE maintenance_color");
            $stm->execute();
            foreach($data as $rows) {
                $query = "INSERT INTO `maintenance_color`( `color_name`,`created_at`) VALUES ('{$rows['color_name']}','{$rows['created_at']}')";
                $stm = $connection->prepare($query);
                $stm->execute();
            }
            return array('status' => 'success', 'code'=> 200);
            print_r("sfa");
        }catch (Exception $exception) {
            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;
        }
    }


    public static function insertmaintenancecategory($connection){
        try{
            $data = array(
                array('category_name' => 'Bag','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('category_name' => 'Book','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('category_name' => 'Camera','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('category_name' => 'Cash','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('category_name' => 'Clothing','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('category_name' => 'Electronic Items','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('category_name' => 'Eye/Sunglasses','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('category_name' => 'Jewelry','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('category_name' => 'Keys','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('category_name' => 'Mobile Phone','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('category_name' => 'Pet','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('category_name' => 'Shoes','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('category_name' => 'Tablet / Laptop','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('category_name' => 'Gaming Device','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('category_name' => 'Wallet/Purse','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('category_name' => 'Other Items','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'))

            );

            $stm = $connection->prepare("TRUNCATE TABLE maintenance_category");
            $stm->execute();
            foreach($data as $rows) {
                $query = "INSERT INTO `maintenance_category`( `category_name`,`created_at`,`updated_at`) VALUES ('{$rows['category_name']}','{$rows['created_at']}','{$rows['updated_at']}')";
                $stm = $connection->prepare($query);
                $stm->execute();
            }
            return array('status' => 'success', 'code'=> 200);
            print_r("sfa");
        }catch (Exception $exception) {
            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;
        }
    }


    public static function insertmaintenanceage($connection){
        try{
            $data = array(

                array('item_name' => 'New','created_at' => date('Y-m-d H:i:s')),
                array('item_name' => '3 – 6 months','created_at' => date('Y-m-d H:i:s')),
                array('item_name' => '6 – 12 months','created_at' => date('Y-m-d H:i:s')),
                array('item_name' => '1 – 5 years','created_at' => date('Y-m-d H:i:s')),
                array('item_name' => '6 – 10 years','created_at' => date('Y-m-d H:i:s')),
                array('item_name' => '10+ years','created_at' => date('Y-m-d H:i:s')),
                array('item_name' => 'Don’t know','created_at' => date('Y-m-d H:i:s')),
                array('item_name' => 'Other','created_at' => date('Y-m-d H:i:s'))
            );

            $stm = $connection->prepare("TRUNCATE TABLE maintenance_item");
            $stm->execute();
            foreach($data as $rows) {
                $query = "INSERT INTO `maintenance_item`( `item_name`,`created_at`) VALUES ('{$rows['item_name']}','{$rows['created_at']}')";
                $stm = $connection->prepare($query);
                $stm->execute();
            }
            return array('status' => 'success', 'code'=> 200);
            print_r("sfa");
        }catch (Exception $exception) {
            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;
        }
    }


    public static function workorderstatus($connection){
        try{
            $data = array(

                array('work_order_status' => 'Open','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),
                array('work_order_status' => 'Closed','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),
                array('work_order_status' => 'In Progress','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),
                array('work_order_status' => 'Cancelled','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),
                array('work_order_status' => 'Assigned','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),
                array('work_order_status' => 'Completed','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),
                array('work_order_status' => 'Completed, no need to bill','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),
                array('work_order_status' => 'Estimated','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),
                array('work_order_status' => 'Estimate Requested','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),
                array('work_order_status' => 'New','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),
                array('work_order_status' => 'Parts on Order','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),
                array('work_order_status' => 'Pending Owner Approval','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),
                array('work_order_status' => 'Scheduled','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),
                array('work_order_status' => 'Vendor Notified','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),
                array('work_order_status' => 'Waiting','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),
                array('work_order_status' => 'Other','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_workorder_status");
            $stm->execute();
            foreach($data as $rows) {
                $query = "INSERT INTO `company_workorder_status`( `work_order_status`,`created_at`,`updated_at`,`is_editable`,`user_id`,`status`) VALUES ('{$rows['work_order_status']}','{$rows['created_at']}','{$rows['updated_at']}','{$rows['is_editable']}','{$rows['user_id']}','{$rows['status']}')";
                $stm = $connection->prepare($query);
                $stm->execute();
            }
            return array('status' => 'success', 'code'=> 200);
            print_r("sfa");
        }catch (Exception $exception) {
            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;
        }
    }
 /*public static function workordersource($connection){
        try{
            $data = array(

                array('work_order_source' => 'Phone Call ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'status' => '1'),
                array('work_order_source' => 'Walk In ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'status' => '1'),
                array('work_order_source' => 'Email ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'status' => '1'),
                array('work_order_source' => 'Text Message ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'status' => '1'),
                array('work_order_source' => 'Website ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1',  'status' => '1'),
                array('work_order_source' => 'Internal ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1',  'status' => '1'),
                array('work_order_source' => 'Tenant Portal','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'status' => '1'),
                array('work_order_source' => 'Owner Portal ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'status' => '1'),
                array('work_order_source' => 'Vendor Portal','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'status' => '1'),
                array('work_order_source' => 'Inspection ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'status' => '1'),
                array('work_order_source' => 'Other ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'status' => '1'),
            );

            $stm = $connection->prepare("TRUNCATE TABLE company_workorder_source");
            $stm->execute();
            foreach($data as $rows) {
                $query = "INSERT INTO `company_workorder_source`( `work_order_source`,`created_at`,`updated_at`,`is_editable`,`status`) VALUES ('{$rows['work_order_source']}','{$rows['created_at']}','{$rows['updated_at']}','{$rows['is_editable']}','{$rows['status']}')";
                $stm = $connection->prepare($query);
                $stm->execute();
            }
            return array('status' => 'success', 'code'=> 200);
            print_r("sfa");
        }catch (Exception $exception) {
            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;
        }
    }*/
    public static function workorderrequestedby($connection){
        try{
            $data = array(

                array('work_order_request' => 'Cleaning Staff','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1',  'user_id' => 1,'status' => '1'),
                array('work_order_request' => 'Maintenance Staff','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1',  'user_id' => 1,'status' => '1'),
                array('work_order_request' => 'Office Staff ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),
                array('work_order_request' => 'Other','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1,'status' => '1'),
                array('work_order_request' => 'Owner','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),
                array('work_order_request' => 'Property Manager','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),
                array('work_order_request' => 'Request','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1,'status' => '1'),
                array('work_order_request' => 'Tenant','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1,'status' => '1'),
                array('work_order_request' => 'Vendor','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'user_id' => 1, 'status' => '1'),

            );

            $stm = $connection->prepare("TRUNCATE TABLE company_workorder_requestedby");
            $stm->execute();
            foreach($data as $rows) {
                $query = "INSERT INTO `company_workorder_requestedby`( `work_order_request`,`created_at`,`updated_at`,`is_editable`,`user_id`,`status`) VALUES ('{$rows['work_order_request']}','{$rows['created_at']}','{$rows['updated_at']}','{$rows['is_editable']}','{$rows['user_id']}','{$rows['status']}')";
                $stm = $connection->prepare($query);
                $stm->execute();
            }
            return array('status' => 'success', 'code'=> 200);
            print_r("sfa");
        }catch (Exception $exception) {
            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;
        }
    }


    public static function insertPackageCarrier($connection)

    {

        try{
            $data = array(

                array('carrierName' => 'DHL','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrierName' => 'FedEx','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrierName' => 'Japan Post Service','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrierName' => 'Royal Mail','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrierName' => 'Schenker AG','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrierName' => 'TNT N.V.','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrierName' => 'UPS','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrierName' => 'USPS','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrierName' => 'YRC WorldWide','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('carrierName' => 'Other','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

            );

            $stm = $connection->prepare("TRUNCATE TABLE packageTrackerCarrier_table");
            $stm->execute();

            foreach ($data as $rows) {

                $query = "INSERT INTO `packageTrackerCarrier_table`( `carrierName`,`created_at`,`updated_at`) VALUES ('{$rows['carrierName']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);



        }catch (Exception $exception) {

            print_r($exception);

        }

    }

    public static function workordersource($connection){
        try{
            $data = array(

                array('work_order_source' => 'Phone Call ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'status' => '1'),
                array('work_order_source' => 'Walk In ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'status' => '1'),
                array('work_order_source' => 'Email ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'status' => '1'),
                array('work_order_source' => 'Text Message ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'status' => '1'),
                array('work_order_source' => 'Website ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1',  'status' => '1'),
                array('work_order_source' => 'Internal ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1',  'status' => '1'),
                array('work_order_source' => 'Tenant Portal','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'status' => '1'),
                array('work_order_source' => 'Owner Portal ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'status' => '1'),
                array('work_order_source' => 'Vendor Portal','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'status' => '1'),
                array('work_order_source' => 'Inspection ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'status' => '1'),
                array('work_order_source' => 'Other ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'), 'is_editable' => '1', 'status' => '1'),
            );

            $stm = $connection->prepare("TRUNCATE TABLE company_workorder_source");
            $stm->execute();
            foreach($data as $rows) {
                $query = "INSERT INTO `company_workorder_source`( `work_order_source`,`created_at`,`updated_at`,`is_editable`,`status`) VALUES ('{$rows['work_order_source']}','{$rows['created_at']}','{$rows['updated_at']}','{$rows['is_editable']}','{$rows['status']}')";
                $stm = $connection->prepare($query);
                $stm->execute();
            }
            return array('status' => 'success', 'code'=> 200);
            print_r("sfa");
        }catch (Exception $exception) {
            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;
        }
    }

    public static function insertLocationStored($connection)

    {

        try{
            $data = array(

                array('locationName' => 'Front Desk ','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('locationName' => 'Break Room','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('locationName' => 'Closet','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('locationName' => 'Coat Room','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('locationName' => 'Conference Room','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('locationName' => 'File Cabinet ','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('locationName' => 'Garage','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('locationName' => 'Lobby Area','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('locationName' => 'Lunch Room','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('locationName' => 'Manager’s Office','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('locationName' => 'Open Area','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('locationName' => 'Receptionist Office','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('locationName' => 'Storage Room (1)','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('locationName' => 'Storage Room (2)','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('locationName' => 'Storage Room (3)','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('locationName' => 'Storage Room (4)','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('locationName' => 'Storage Room (5)','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('locationName' => 'Other','created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),


            );

            $stm = $connection->prepare("TRUNCATE TABLE locationStored_table");
            $stm->execute();

            foreach ($data as $rows) {

                $query = "INSERT INTO `locationStored_table`( `locationName`,`created_at`,`updated_at`) VALUES ('{$rows['locationName']}','{$rows['created_at']}','{$rows['updated_at']}')";

                $stm = $connection->prepare($query);

                $stm->execute();

            }

            return array('status' => 'success', 'code'=> 200);



        }catch (Exception $exception) {

            print_r($exception);

        }

    }


    public static function insertMcc($connection)

    {

        try{
            $data = array(

                array('user_id' => 1,'name' => 'A/C, Refrigeration Repair','code' => 7623,'status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Accounting/Bookkeeping Services','code' => 8931,'status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Advertising Services','code' => '7311','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Agricultural Cooperative','code' => '763','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Airlines, Air Carriers','code' => '4511','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Airports, Flying Fields','code' => '4582','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Ambulance Services','code' => '4119','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Amusement Parks/Carnivals','code' => '7996','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Antique Reproductions','code' => '5937','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Antique Shops','code' => '5932','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Aquariums','code' => '7998','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Architectural/Surveying Services','code' => '8911','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Art Dealers and Galleries','code' => '5971','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Artists Supply and Craft Shops','code' => '5970','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Auto Body Repair Shops','code' => '7531','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Auto Paint Shops','code' => '7535','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Auto Service Shops','code' => '7538','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Auto and Home Supply Stores','code' => '5531','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Automated Cash Disburse','code' => '6011','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Automated Fuel Dispensers','code' => '5542','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Automobile Associations','code' => '8675','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Automotive Parts and Accessories Stores','code' => '5533','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Automotive Tire Stores','code' => '5532','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Bail and Bond Payments (payment to the surety for the bond, not the actual bond paid to the government agency)','code' => '9223','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Bakeries','code' => '5462','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Bands, Orchestras','code' => '7929','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Barber and Beauty Shops','code' => '7230','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Betting/Casino Gambling','code' => '7995','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Bicycle Shops','code' => '5940','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Billiard/Pool Establishments','code' => '7932','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Boat Dealers','code' => '5551','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Boat Rentals and Leases','code' => '4457','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Book Stores','code' => '5942','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Books, Periodicals, and Newspapers','code' => '5192','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Bowling Alleys','code' => '7933','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Bus Lines','code' => '4131','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),


                array('user_id' => 1,'name' => 'Business/Secretarial Schools','code' => '8244','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Buying/Shopping Services','code' => '7278','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Cable, Satellite, and Other Pay Television and Radio','code' => '4899','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Camera and Photographic Supply Stores','code' => '5946','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Candy, Nut, and Confectionery Stores','code' => '5441','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Car Rental Agencies','code' => '7512','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Car Washes','code' => '7542','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Car and Truck Dealers (New & Used) Sales, Service, Repairs Parts and Leasing','code' => '5511','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Car and Truck Dealers (Used Only) Sales, Service, Repairs Parts and Leasing','code' => '5521','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Carpentry Services','code' => '1750','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Carpet/Upholstery Cleaning','code' => '7217','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Caterers','code' => '5811','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Charitable and Social Service Organizations - Fundraising','code' => '8398','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Chemicals and Allied Products (Not Elsewhere Classified)','code' => '5169','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Chidrens and Infants Wear Stores','code' => '5641','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Child Care Services','code' => '8351','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Chiropodists, Podiatrists','code' => '8049','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Chiropractors','code' => '8041','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Cigar Stores and Stands','code' => '5993','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Civic, Social, Fraternal Associations','code' => '8641','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Cleaning and Maintenance','code' => '7349','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Clothing Rental','code' => '7296','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Colleges, Universities','code' => '8220','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Commercial Equipment (Not Elsewhere Classified)','code' => '5046','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Commercial Footwear','code' => '5139','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Commercial Photography, Art and Graphics','code' => '7333','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Commuter Transport, Ferries','code' => '4111','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Computer Network Services','code' => '4816','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Computer Programming','code' => '7372','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Computer Repair','code' => '7379','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Computer Software Stores','code' => '5734','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Computers, Peripherals, and Software','code' => '5045','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Concrete Work Services','code' => '1771','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Construction Materials (Not Elsewhere Classified)','code' => '5039','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Consulting, Public Relations','code' => '7392','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Correspondence Schools','code' => '8241','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Cosmetic Stores','code' => '5977','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Counseling Services','code' => '7277','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Country Clubs','code' => '7997','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Courier Services','code' => '4215','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Court Costs, Including Alimony and Child Support - Courts of Law','code' => '9211','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Credit Reporting Agencies','code' => '7321','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Cruise Lines','code' => '4411','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Dairy Products Stores','code' => '5451','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Dance Hall, Studios, Schools','code' => '7911','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Dating/Escort Services','code' => '7273','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Dentists, Orthodontists','code' => '8021','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Department Stores','code' => '5311','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Detective Agencies','code' => '7393','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Direct Marketing - Catalog Merchant','code' => '5964','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Direct Marketing - Combination Catalog and Retail Merchant','code' => '5965','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Direct Marketing - Inbound Telemarketing','code' => '5967','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Direct Marketing - Insurance Services','code' => '5960','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Direct Marketing - Other','code' => '5969','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Direct Marketing - Outbound Telemarketing','code' => '5966','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Direct Marketing - Subscription','code' => '5968','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Direct Marketing - Travel','code' => '5962','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Discount Stores','code' => '5310','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Doctors','code' => '8011','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Door-To-Door Sales','code' => '5963','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Drapery, Window Covering, and Upholstery Stores','code' => '5714','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Drinking Places','code' => '5813','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Drug Stores and Pharmacies','code' => '5912','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Drugs, Drug Proprietaries, and Druggist Sundries','code' => '5122','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),

                array('user_id' => 1,'name' => 'Dry Cleaners','code' => '7216','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Durable Goods (Not Elsewhere Classified)','code' => '5099','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Duty Free Stores','code' => '5309','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Eating Places, Restaurants','code' => '5812','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Educational Services','code' => '8299','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Electric Razor Stores','code' => '5997','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Electrical Parts and Equipment','code' => '5065','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Electrical Services','code' => '1731','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Electronics Repair Shops','code' => '7622','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),


                array('user_id' => 1,'name' => 'Electronics Stores','code' => '5732','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Elementary, Secondary Schools','code' => '8211','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Employment/Temp Agencies','code' => '7361','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Equipment Rental','code' => '7394','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Exterminating Services','code' => '7342','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Family Clothing Stores','code' => '5651','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Fast Food Restaurants','code' => '5814','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Financial Institutions','code' => '6012','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Fines - Government Administrative Entities','code' => '9222','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Fireplace, Fireplace Screens, and Accessories Stores','code' => '718','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Floor Covering Stores','code' => '5713','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Florists','code' => '5992','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Florists Supplies, Nursery Stock, and Flowers','code' => '5193','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Freezer and Locker Meat Provisioners','code' => '5422','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Fuel Dealers (Non Automotive)','code' => '5983','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Funeral Services, Crematories','code' => '7261','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Furniture Repair, Refinishing','code' => '7641','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Furniture, Home Furnishings, and Equipment Stores, Except Appliances','code' => '5712','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Furriers and Fur Shops','code' => '5681','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'General Services','code' => '1520','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Gift, Card, Novelty, and Souvenir Shops','code' => '5947','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Glass, Paint, and Wallpaper Stores','code' => '5231','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Glassware, Crystal Stores','code' => '5950','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Golf Courses - Public','code' => '7992','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Government Services (Not Elsewhere Classified)','code' => '9399','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Grocery Stores, Supermarkets','code' => '5411','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Hardware Stores','code' => '5251','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Hardware, Equipment, and Supplies','code' => '5072','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Health and Beauty Spas','code' => '7298','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Hearing Aids Sales and Supplies','code' => '5975','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Heating, Plumbing, A/C','code' => '1711','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Hobby, Toy, and Game Shops','code' => '5945','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Home Supply Warehouse Stores','code' => '5200','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Hospitals','code' => '8062','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Hotels, Motels, and Resorts','code' => '7011','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Household Appliance Stores','code' => '5722','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Industrial Supplies (Not Elsewhere Classified)','code' => '5085','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Information Retrieval Services','code' => '7375','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Insurance - Default','code' => '6399','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Insurance Underwriting, Premiums','code' => '6300','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Intra-Company Purchases','code' => '9950','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Jewelry Stores, Watches, Clocks, and Silverware Stores','code' => '5944','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Landscaping Services','code' => '780','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Laundries','code' => '7211','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Laundry, Cleaning Services','code' => '7210','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Legal Services, Attorneys','code' => '8111','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Luggage and Leather Goods Stores','code' => '5948','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Lumber, Building Materials Stores','code' => '5211','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Manual Cash Disburse','code' => '6010','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Marinas, Service and Supplies','code' => '4468','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Masonry, Stonework, and Plaster','code' => '1740','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Massage Parlors','code' => '7297','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Medical Services','code' => '8099','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Medical and Dental Labs','code' => '8071','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Medical, Dental, Ophthalmic, and Hospital Equipment and Supplies','code' => '5047','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Membership Organizations','code' => '8699','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Mens and Boys Clothing and Accessories Stores','code' => '5611','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Mens, Womens Clothing Stores','code' => '5691','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Metal Service Centers','code' => '5051','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Miscellaneous Apparel and Accessory Shops - 5699','code' => '5699','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Miscellaneous Auto Dealers','code' => '5599','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Miscellaneous Business Services','code' => '7399','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Miscellaneous Food Stores - Convenience Stores and Specialty Markets','code' => '5499','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Miscellaneous General Merchandise','code' => '5399','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Miscellaneous General Services','code' => '7299','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Miscellaneous Home Furnishing Specialty Stores','code' => '5719','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Miscellaneous Publishing and Printing','code' => '2741','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Miscellaneous Recreation Services','code' => '7999','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Miscellaneous Repair Shops','code' => '7699','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Miscellaneous Specialty Retail','code' => '5999','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Mobile Home Dealers','code' => '5271','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Motion Picture Theaters','code' => '7832','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Motor Freight Carriers and Trucking - Local and Long Distance, Moving and Storage Companies, and Local Delivery Services','code' => '4214','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Motor Homes Dealers','code' => '5592','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Motor Vehicle Supplies and New Parts3','code' => '5013','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Motorcycle Shops and Dealers','code' => '5571','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Motorcycle Shops, Dealers','code' => '5561','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Music Stores-Musical Instruments, Pianos, and Sheet Music - 5733','code' => '5733','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'News Dealers and Newsstands','code' => '5994','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Non-FI, Money Orders','code' => '6051','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Non-FI, Stored Value Card Purchase/Load','code' => '6540','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Nondurable Goods (Not Elsewhere Classified)','code' => '5199','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Nurseries, Lawn and Garden Supply Stores','code' => '5261','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Nursing/Personal Care','code' => '8050','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Office and Commercial Furniture','code' => '5021','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Opticians, Eyeglasses','code' => '8043','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Optometrists, Ophthalmologist','code' => '8042','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Orthopedic Goods - Prosthetic Devices','code' => '5976','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Osteopaths','code' => '8031','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Package Stores-Beer, Wine, and Liquor','code' => '5921','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Paints, Varnishes, and Supplies','code' => '5198','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Parking Lots, Garages','code' => '7523','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Passenger Railways','code' => '4112','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Pawn Shops','code' => '5933','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Pet Shops, Pet Food, and Supplies','code' => '5995','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Petroleum and Petroleum Products','code' => '5172','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Photo Developing','code' => '7395','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Photographic Studios','code' => '7221','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Photographic, Photocopy, Microfilm Equipment, and Supplies - 5044','code' => '5044','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Picture/Video Production','code' => '7829','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Piece Goods, Notions, and Other Dry Goods','code' => '5131','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Plumbing, Heating Equipment, and Supplies','code' => '5074','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Political Organizations','code' => '8651','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Postal Services - Government Only','code' => '9402','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Precious Stones and Metals, Watches and Jewelry','code' => '5094','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Professional Services','code' => '8999','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Public Warehousing and Storage - Farm Products, Refrigerated Goods, Household Goods, and Storage','code' => '4225','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Quick Copy, Repro, and Blueprint','code' => '7338','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Railroads','code' => '4011','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Real Estate Agents and Managers - Rentals','code' => '6513','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Record Stores','code' => '5735','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Recreational Vehicle Rentals','code' => '7519','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Religious Goods Stores','code' => '5973','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Religious Organizations','code' => '8661','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Roofing/Siding, Sheet Metal','code' => '1761','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Secretarial Support Services','code' => '7339','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Security Brokers/Dealers','code' => '6211','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Service Stations','code' => '5541','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Sewing, Needlework, Fabric, and Piece Goods Stores','code' => '5949','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Shoe Repair/Hat Cleaning','code' => '7251','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Shoe Stores','code' => '5661','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Small Appliance Repair','code' => '7629','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Snowmobile Dealers','code' => '5598','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Special Trade Services','code' => '1799','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Specialty Cleaning','code' => '2842','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Sporting Goods Stores','code' => '5941','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Sporting/Recreation Camps','code' => '7032','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Sports Clubs/Fields','code' => '7941','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Sports and Riding Apparel Stores','code' => '5655','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Stamp and Coin Stores','code' => '5972','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Stationary, Office Supplies, Printing and Writing Paper','code' => '5111','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Stationery Stores, Office, and School Supply Stores','code' => '5943','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Swimming Pools Sales','code' => '5996','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'TUI Travel - Germany','code' => '4723','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Tailors, Alterations','code' => '5697','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Tax Payments - Government Agencies','code' => '9311','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Tax Preparation Services','code' => '7276','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Taxicabs/Limousines','code' => '4121','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Telecommunication Equipment and Telephone Sales','code' => '4812','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Telecommunication Services','code' => '4814','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Telegraph Services','code' => '4821','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Tent and Awning Shops','code' => '5998','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Testing Laboratories','code' => '8734','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Theatrical Ticket Agencies','code' => '7922','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Timeshares','code' => '7012','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Tire Retreading and Repair','code' => '7534','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Tolls/Bridge Fees','code' => '4784','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Tourist Attractions and Exhibits','code' => '7991','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Towing Services','code' => '7549','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Trailer Parks, Campgrounds','code' => '7033','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Transportation Services (Not Elsewhere Classified)','code' => '4789','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Travel Agencies, Tour Operators','code' => '4722','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Truck StopIteration','code' => '7511','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Truck/Utility Trailer Rentals','code' => '7513','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Typesetting, Plate Making, and Related Services','code' => '2791','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Typewriter Stores','code' => '5978','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'U.S. Federal Government Agencies or Departments','code' => '9405','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Uniforms, Commercial Clothing','code' => '5137','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Used Merchandise and Secondhand Stores','code' => '5931','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Utilities','code' => '4900','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Variety Stores','code' => '5331','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Veterinary Services','code' => '742','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Video Amusement Game Supplies','code' => '7993','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Video Game Arcades','code' => '7994','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Video Tape Rental Stores','code' => '7841','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Vocational/Trade Schools','code' => '8249','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Watch/Jewelry Repair','code' => '7631','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Welding Repair','code' => '7692','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Wholesale Clubs','code' => '5300','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Wig and Toupee Stores','code' => '5698','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Wires, Money Orders','code' => '4829','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Womens Accessory and Specialty Shops','code' => '5631','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Womens Ready-To-Wear Stores','code' => '5621','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
                array('user_id' => 1,'name' => 'Wrecking and Salvage Yards','code' => '5935','status' => 1,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
            );

            $stm = $connection->prepare("TRUNCATE TABLE mcc_types");
            $stm->execute();
            foreach ($data as $rows) {
                $query = "INSERT INTO `mcc_types`( `user_id`, `name`, `code`, `status`,`created_at`,`updated_at`) VALUES ('{$rows['user_id']}','{$rows['name']}','{$rows['code']}','{$rows['status']}','{$rows['created_at']}','{$rows['updated_at']}')";
                $stm = $connection->prepare($query);
                $stm->execute();
            }

            return array('status' => 'success', 'code'=> 200);



        }catch (Exception $exception) {

            print_r($exception);

        }

    }


    public static function calendargroups($connection){
        try{
            $data = array(

                array('calendar_names' => 'My Calendar','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('calendar_names' => 'Company Calendar','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('calendar_names' => 'Regional Calendar ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('calendar_names' => 'Property Calendar ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('calendar_names' => 'Staff Calendar ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
                array('calendar_names' => 'Group Calendar ','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')),
            );

            $stm = $connection->prepare("TRUNCATE TABLE calendar_groups");
            $stm->execute();
            foreach($data as $rows) {
                $query = "INSERT INTO `calendar_groups`( `calendar_names`,`created_at`,`updated_at`) VALUES ('{$rows['calendar_names']}','{$rows['created_at']}','{$rows['updated_at']}')";
                $stm = $connection->prepare($query);
                $stm->execute();
            }
            return array('status' => 'success', 'code'=> 200);
            print_r("sfa");
        }catch (Exception $exception) {
            echo '<pre>'; print_r($exception->getMessage()); echo '</pre>'; die;
        }
    }

}
$ddl = new SeederCusers();