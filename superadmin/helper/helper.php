<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/17/2019
 * Time: 3:23 PM
 */
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //include_once ('../../constants.php');
    header('Location: ' . BASE_URL);
};

function printErrorLog($errorMessage)
{
    error_log('['.date("Y-m-d h:i:s").'] '.$errorMessage.PHP_EOL,'3',ROOT_URL . "/superadmin/apexSuperAdmin.log");
}

/**
 * @param $request
 * @param $method
 * @param $request_url
 * @return mixed|string
 */
function curlRequest($request) {

    try {
     //   print_r($request); die;
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => 'http://rentconnect.net/apex-email/index.php/api/email',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => http_build_query($request),
        ));
        return $server_output = curl_exec($ch);

        //  echo '<pre>'; print_r($server_output); echo '</pre>'; die;
// Further processing ...
//        if ($server_output->status == "success" && $server_output->code == 200) {
//            return 1;
//        } else {
//            return 0;
//        }
    } catch (Exception $exception) {
        return $exception->getMessage();
    }
}

/**
 * function for super admin user login
 * @author Deepak
 * @return array
 */
function getCountryCode(){
    try {
        $db = $_POST['db'];
        $db = new DBConnection();
        $query = $db->conn->query("SELECT * FROM countries");
        $data = $query->fetchAll();

        if(!empty($data)) {
            return array('status' => 'success', 'data' => $data, 'message' => 'Data Retrieved Successfully.');
        }

        return array('status' => 'error', 'data' => $data, 'message' => 'No Data!');
    }
    catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * Random String!
 * Use this endpoint to generate the eight number random string.
 * @author Parvesh
 * @param int $length
 * @success Success
 * @error (Exceptions) Validation of Model failed. See status.returnValues
 * @return string
 */
function randomString($length = 8)
{
    $str = "";
    $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
    $max = count($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
        $rand = mt_rand(0, $max);
        $str .= $characters[$rand];
    }
    return $str;
}

/**
 * Random String!
 * Use this endpoint to generate the random string.
 * @author Sarita
 * @param int $length
 * @success Success
 * @error (Exceptions) Validation of Model failed. See status.returnValues
 * @return string
 */
function randomTokenString($length)
{
    $str = "";
    $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
    $max = count($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
        $rand = mt_rand(0, $max);
        $str .= $characters[$rand];
    }
    return $str;
}

/**
 * function to get post data array
 * @author Deepak
 * @param $post
 * @return array
 */
function postArray($post)
{
    $data = [];

    foreach ($post as $key => $value) {
        if (!empty($value['value']) && isset($value['value'])) {
            $dataValue = test_input($value['value']);
        } else {
            $dataValue = $value['value'];
        }
        $data[$value['name']] = $dataValue;
    }
    return $data;
}

/**
 * Server side validation function
 * @author Deepak
 * @param $data
 * @return string
 */
function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

/**
 * Server side validation function
 * @author Deepak
 * @param $data
 * @return array
 */
function createSqlColVal($data)
{
    $columns = '';
    $columnsValues = '';
    foreach ($data as $key => $value) {
        $columns .= $key . ',';
        $columnsValues .= ':' . "$key" . ',';
    }
    $columns = substr_replace($columns, "", -1);
    $columnsValues = substr_replace($columnsValues, "", -1);
    $sqlData = ['columns' => $columns, 'columnsValues' => $columnsValues];
    return $sqlData;
}

/**
 * Column value pair for update query
 * @param $data
 * @return array
 */
function createSqlColValPair($data) {
    $columnsValuesPair = '';
    foreach ($data as $key=>$value){
        if($key == 'c_id'){
            //do nothing
        }else{
            $columnsValuesPair .=  $key."='".$value."',";
        }
    }
    $columnsValuesPair = substr_replace($columnsValuesPair ,"",-1);
    $sqlData = ['columnsValuesPair'=>$columnsValuesPair];
    return $sqlData;
}

/**
 * function to check validation array
 * @author Deepak
 * @param $data
 * @return bool
 */
function checkValidationArray($data){
    foreach ($data as $key=>$value){
        if(!empty($value)){
            return true;
        }
    }
    return false;
}

/**
 * Server side Validations
 * @author Deepak
 * @param $data
 * @param $db
 * @param array $required_array
 * @param array $maxlength_array
 * @param array $number_array
 * @return array
 */
function validation($data,$db,$required_array=[],$maxlength_array=[],$number_array=[],$updateId=null){
    $err_array = [];
    foreach ($data as $key => $value) {
        $errName = $key . 'Err';
        $err_array[$errName] = [];
        //Required Validations
//         echo '<pre>'; print_r($required_array); echo '</pre>';
        if(in_array($key,$required_array)){
            if (empty($value)) {
                $colName = ucfirst(str_replace('_',' ',$key));
                $error = $colName." is required.";
                array_push($err_array[$errName],$error);
            }
        }
        //Max length Validations
        if(array_key_exists($key,$maxlength_array)){
            $length = strlen($value);
            if (!empty($value) && $length > $maxlength_array[$key]) {
                $error = '* Please enter less than '.$maxlength_array[$key].' characters.';
                array_push($err_array[$errName],$error);
            }
        }
        //Number Validations
        if(in_array($key,$number_array)){
            $number = is_numeric(str_replace('-','',$value));
            if (!empty($value) && !$number) {
                $error = '* Only number are allowed!';
                array_push($err_array[$errName],$error);
            }
        }
        //email validation
        if($key == 'email' && !empty($value)){
            if(!filter_var($value, FILTER_VALIDATE_EMAIL)){
                $error = "* Invalid email format";
                array_push($err_array[$errName], $error);
            } else {
                $emailqry = "SELECT * FROM users WHERE email='$value'";
                $emailData = $db->query($emailqry)->fetch();
                if (!empty($emailData)){
                    if(!empty($updateId)){
                        if($updateId != $emailData['id']){
                            $error = "* Email already exist!";
                            array_push($err_array[$errName], $error);
                        }
                    } else {
                        $error = "* Email already exist!";
                        array_push($err_array[$errName], $error);
                    }
                }
            }
        }

        //email validation
        if($key == 'domain_name' && !empty($value)){
            $domainqry = "SELECT domain_name FROM users WHERE domain_name='$value'";
            $domainData = $db->query($domainqry)->fetch();
            if (!empty($domainData)){
                $error = "* Domain already exist!";
                array_push($err_array[$errName], $error);
            }
        }

        if(empty($err_array[$errName]))
        {
            unset($err_array[$errName]);
        }
    }
    return $err_array;
}

/**
 * function to get company connection
 * @param null $companyid
 * @return PDO|string
 */
function getCompanyConnection($companyid = null)
{
    try{
        $db = new DBConnection();
        $dbDetails = $db->conn->query("SELECT * FROM users where id=".$companyid)->fetch();
        //   echo '<pre>'; print_r($dbDetails); echo '</pre>'; die;
        $connection = DBConnection::dynamicDbConnection($dbDetails['host'],$dbDetails['database_name'],$dbDetails['db_username'],$dbDetails['db_password']);
        return $connection;
    }catch (Exception $exception)
    {
        return $exception->getMessage();
    }
}

/**
 * function to change date specified in date picker to mysql default format
 * @param $date
 * @param null $id
 * @return DateTime
 * @throws Exception
 */
function mySqlDateFormat($date, $id = null) {

    if($id === null) {
        $id = $_SESSION[SESSION_DOMAIN]['user_id'];
    }
    $sql = "SELECT * FROM default_date_clock_settings WHERE user_id=".$id;
    $db = new DBConnection();
    $dateFormatVal = $db->conn->query($sql)->fetch();


    if (!empty($dateFormatVal)) {
        $date_format_val = $dateFormatVal['date_format'];
    } else {
        $date_format_val = 1;
    }

    $result = explode(" (", $date);
    $date = trim($result[0]);
    //New date Format data
    $value = $date_format_val;
    switch ($value) {
        case "1":
            $format = explode("/",$date);
            $month = $format[0];
            $day = $format[1];
            $year = $format[2];
            $formatDate = $year.'-'.$month.'-'.$day;
            break;
        case "2":
            $format = explode("-",$date);
            $month = $format[0];
            $day = $format[1];
            $year = $format[2];
            $formatDate = $year.'-'.$month.'-'.$day;
            break;
        case "3":
            $format = explode(".",$date);
            $month = $format[0];
            $day = $format[1];
            $year = $format[2];
            $formatDate = $year.'-'.$month.'-'.$day;
            break;
        case "4":
            $format = explode("/",$date);
            $day = $format[0];
            $month = $format[1];
            $year = $format[2];
            $formatDate = $year.'-'.$month.'-'.$day;
            break;
        case "5":
            $format = explode("-",$date);
            $day = $format[0];
            $month = $format[1];
            $year = $format[2];
            $formatDate = $year.'-'.$month.'-'.$day;
            break;
        case "6":
            $format = explode(".",$date);
            $day = $format[0];
            $month = $format[1];
            $year = $format[2];
            $formatDate = $year.'-'.$month.'-'.$day;
            break;
        case "7":
            $format = explode("/",$date);
            $year = $format[0];
            $month = $format[1];
            $day = $format[2];
            $formatDate = $year.'-'.$month.'-'.$day;
            break;
        case "8":
            $format = explode("-",$date);
            $year = $format[0];
            $month = $format[1];
            $day = $format[2];
            $formatDate = $year.'-'.$month.'-'.$day;
            break;
        case "9":
            $format = explode(".",$date);
            $year = $format[0];
            $month = $format[1];
            $day = $format[2];
            $formatDate = $year.'-'.$month.'-'.$day;
            break;
        case "10":
            $format = explode("/",$date);
            $year = $format[0];
            $day = $format[1];
            $month = $format[2];
            $formatDate = $year.'-'.$month.'-'.$day;
            break;
        case "11":
            $format = explode("-",$date);
            $year = $format[0];
            $day = $format[1];
            $month = $format[2];
            $formatDate = $year.'-'.$month.'-'.$day;
            break;
        case "12":
            $format = explode(".",$date);
            $year = $format[0];
            $day = $format[1];
            $month = $format[2];
            $formatDate = $year.'-'.$month.'-'.$day;
            break;
        case "13":
            $newDate = str_replace(',', "", $date);
            $data = date('d/m/Y', strtotime($newDate));
            $format = explode("/",$data);
            $year = $format[2];
            $day = $format[0];
            $month = $format[1];
            $formatDate = $year.'-'.$month.'-'.$day;
            break;
        case "14":
            $reformat = explode(",",$date);
            $reformat2 = explode(" ",$reformat[0]);
            $newDay = $reformat2[0];
            $newMonth = $reformat2[1];
            $newYear = trim($reformat[1]);
            $data = date('d/m/Y', strtotime($newMonth.''.$newDay.', '.$newYear));
            $format = explode("/",$data);
            $year = $format[2];
            $day = $format[0];
            $month = $format[1];
            $formatDate = $year.'-'.$month.'-'.$day;
            break;
        default:
            $reformat = explode(",",$date);
            $reformat2 = explode(" ",$reformat[0]);
            $newDay = $reformat2[0];
            $newMonth = $reformat2[1];
            $newYear = trim($reformat[1]);
            $data = date('d/m/Y', strtotime($newMonth.''.$newDay.', '.$newYear));
            $format = explode("/",$data);
            $year = $format[2];
            $day = $format[0];
            $month = $format[1];
            $formatDate = $year.'-'.$month.'-'.$day;
    }

    return date('Y-m-d',strtotime($formatDate));
}

/**
 * function to get user specified date format
 * @param $date
 * @param null $id
 * @return mixed|string
 */
function dateFormatUser($date, $id = null) {
    try{if($date == ''){
        return $date;
    }
        if($id === null) {
            $id = $_SESSION[SESSION_DOMAIN]['user_id'];
        }
        $sql = "SELECT * FROM default_date_clock_settings WHERE user_id=".$id;
        $db = new DBConnection();
        $dateFormatVal = $db->conn->query($sql)->fetch();

        if (!empty($dateFormatVal) && is_array($dateFormatVal)) {
            $date_format_val = $dateFormatVal['date_format'];
        } else {
            $date_format_val = 1;
        }

        $pos = strpos($date, "(");
        $pos2 = strpos($date, "-");

        if ($pos !== false) {
            $date = str_replace(array('(', ')'), "", $date);
        }

        $dat = strtotime($date);
        $date_created = date_create($date);
        $weekendDay = date_format($date_created,'d-m-Y');
        $dateString = strtotime($weekendDay);
        $day = date('l',$dateString);
        if (!empty($day)) {
            $day = substr($day, 0, 3);
            $day = $day . ".";
        }

        $value = $date_format_val;
        switch ($value) {
            case "1":
                $formatDate = date('m/d/Y',$dateString);
                break;
            case "2":
                $formatDate = date('m-d-Y',$dateString);
                break;
            case "3":
                $formatDate = date('m.d.Y',$dateString);
                break;
            case "4":
                $formatDate = date('d/m/Y',$dateString);
                break;
            case "5":
                $formatDate = date('d-m-Y',$dateString);
                break;
            case "6":
                $formatDate = date('d.m.Y',$dateString);
                break;
            case "7":
                $formatDate = date('Y/m/d',$dateString);
                break;
            case "8":
                $formatDate = date('Y-m-d',$dateString);
                break;
            case "9":
                $formatDate = date('Y.m.d',$dateString);
                break;
            case "10":
                $formatDate = date('Y/d/m',$dateString);
                break;
            case "11":
                $formatDate = date('Y-d-m',$dateString);
                break;
            case "12":
                $formatDate = date('Y.d.m',$dateString);
                break;
            case "13":
                $formatDate = date('M d, Y',$dateString);
                break;
            case "14":
                $formatDate = date('d M, Y',$dateString);
                break;
            default:
                $formatDate = date('d F, Y',$dateString);
        }
        $date = $formatDate.' (' . $day . ')';
        return $date;
    } catch (Exception $exception) {
       // Log::error($exception->getMessage());
    }
}

/**
 * function to get user specified time format
 * @param $time
 * @param null $id
 * @return false|string
 */
function timeFormat($time,$id = null) {
    if($id === null) {
        $id = $_SESSION[SESSION_DOMAIN]['user_id'];
    }
    $sql = "SELECT * FROM default_date_clock_settings WHERE user_id=".$id;
    $db = new DBConnection();
    $userFormat = $db->conn->query($sql)->fetch();
    if (!empty($userFormat)) {
        if ($userFormat['default_clock_format'] == '12') {
            //change current time to 12 hour format
            $timeFormat = date("g:i A", strtotime($time));
        } elseif ($userFormat['default_clock_format'] == '24') {
            $timeFormat = date("H:i", strtotime($time));
        }
    } else {
        $timeFormat = date("g:i A", strtotime($time));
    }
    return $timeFormat;
}

/**
 * Get date format for datepicker of user.
 * @param null $id
 * @return string
 */
function getDatePickerFormat($id = null)
{
    if($id === null) {
        $id = $_SESSION[SESSION_DOMAIN]['user_id'];
    }

    $sql = "SELECT * FROM default_date_clock_settings WHERE user_id=".$id;
    $db = new DBConnection();
    $dateFormatVal = $db->conn->query($sql)->fetch();
    if (isset($dateFormatVal) && (is_array($dateFormatVal)) && (count($dateFormatVal) > 0)) {
        $date_format_val = $dateFormatVal['date_format'];
    } else {
        $date_format_val = 1;
    }
    $value = $date_format_val;
    switch ($value) {
        case "1":
            $date = 'm/d/yy (D.)';
            break;
        case "2":
            $date = 'm-d-yy (D.)';
            break;
        case "3":
            $date = 'm.d.yy (D.)';
            break;
        case "4":
            $date = 'd/m/yy (D.)';
            break;
        case "5":
            $date = 'd-m-yy (D.)';
            break;
        case "6":
            $date = 'd.m.yy (D.)';
            break;
        case "7":
            $date = 'yy/m/d (D.)';
            break;
        case "8":
            $date = 'yy-m-d (D.)';
            break;
        case "9":
            $date = 'yy.m.d (D.)';
            break;
        case "10":
            $date = 'yy/d/m (D.)';
            break;
        case "11":
            $date = 'yy-d-m (D.)';
            break;
        case "12":
            $date = 'yy.d.m (D.)';
            break;
        case "13":
            $date = 'M d, yy (D.)';
            break;
        case "14":
            $date = 'd M, yy (D.)';
            break;
        default:
            $date = 'd F, yy (D.)';
    }
    return $date;
}

/**
 * @param $time
 * @return mixed
 */
function mySqlTimeFormat($time) {
    $time = date('H:i:s', strtotime($time));
    return $time;
}

/**
 * function to get username
 * @param null $id
 * @param $table
 * @return string
 */
function userName($id = null,$table) {
    if($id === null) {
        $id = $_SESSION[SESSION_DOMAIN]['user_id'];
    }
    $sql = "SELECT * FROM $table WHERE id=".$id;
    $db = new DBConnection();
    $data = $db->conn->query($sql)->fetch();
    $name = '';
    //user name
    if(!empty($data) && count($data) > 0){
        $firstName = !empty($data['first_name'])?$data['first_name']:null;
        $lastName = !empty($data['last_name'])?$data['last_name']:null;
        if(!empty($data['nick_name'])){
            $name = $data['nick_name'].' '.$lastName;
        } else {
            $name = $firstName.' '.$lastName;
        }
    }

    return $name;
}


function noOfHits($id = null)
{
    $db = new DBConnection();
    $dbDetails = $db->conn->query("SELECT * FROM users where id=".$id)->fetch();
    $connection = DBConnection::dynamicDbConnection($dbDetails['host'],$dbDetails['database_name'],$dbDetails['db_username'],$dbDetails['db_password']);
    $data = $connection->query("SELECT * FROM audit_trial where action='login'")->rowCount();
    $data = (string)$data;
    return $data;
}

function lastLogin($id = null)
{
    $db = new DBConnection();
    $dbDetails = $db->conn->query("SELECT * FROM users where id=" . $id)->fetch();
    $connection = DBConnection::dynamicDbConnection($dbDetails['host'], $dbDetails['database_name'], $dbDetails['db_username'], $dbDetails['db_password']);
    $data = $connection->query("SELECT * FROM audit_trial where action='login' order by id desc ")->fetch();
    if(!empty($data)) {
        return dateFormatUser($data['created_at'], null);
    }
    return '';
}
/**
 * function to count record in table
 * @param $connection
 * @param $table
 * @return mixed
 */
function getRecordCount($connection, $table)
{
    try {
        $data = $connection->query("SELECT COUNT(*) FROM $table WHERE deleted_at IS NULL")->fetch();
        return $data['COUNT(*)'];
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * function to print formated data
 * @param $data
 */
function dd($data){
    try {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        die;
    }
    catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * Function for fetching data based on id
 * @param $connection
 * @param $table
 * @param $id
 * @return array
 */
function getDataById($connection, $table, $id)
{
    try {
        $property_subtype_id = $id;
        $data =$connection->query("SELECT * FROM ".$table." WHERE id =".$property_subtype_id)->fetch();
        return ['code'=>200, 'status'=>'success', 'data'=>$data];
    }
    catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * function to get single record from table
 * @param $connection
 * @param $id
 * @param $table
 * @return array
 */
function getSingleRecord($connection ,$condition, $table){
    $data =$connection->query("SELECT * FROM ".$table." WHERE ".$condition['column']."= '".$condition['value']."'")->fetch();
    if(!empty($data)){
        return ['msg' => 'Record Retrieved Successfully.','data'=>$data,'code'=>200];
    }
    return ['msg' => 'No Record Found!','code'=>400];
}

/**
 * function to get the carrier from phone number
 * @param $ext
 * @param $phone_number
 * @param $connection
 * @return array
 */
function getPhoneCarrier($ext,$phone_number,$connection){
    try{
        if($ext == '+1') {
            $phone_number = str_replace('-', '', $phone_number);
            $userData = 'SELECT id,sms_carrier,name FROM users WHERE phone_number="'.$phone_number.'"';
            $userData = $connection->query($userData)->fetch();
            if(!empty($userData) && (isset($userData['sms_carrier']) && !empty($userData['sms_carrier']))){
                return  ['status' => SUCCESS, 'code' => SUCCESS_CODE, 'sms_gateway' =>$userData['sms_carrier'] ,'address' => '+1'.$phone_number.'@'.$userData['sms_gateway']];
            }
            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => 'http://apilayer.net/api/validate?access_key='.API_LAYER.'&number=1'.$phone_number,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 200000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET'
            ));
            $server_output = curl_exec($ch);
            $server_output = json_decode($server_output);
            if(!empty($server_output) && !empty($server_output->carrier)){
                $sdata = strtolower($server_output->carrier);
                $sql = 'SELECT sms_gateway FROM carrier WHERE LOWER(carrier)="'.$sdata.'"';
                $data = $connection->query($sql)->fetch();
                if(!empty($data['sms_gateway'])){
                    if(!empty($userData)){
                        $data = [];
                        $data['sms_carrier'] = $data['sms_gateway'];
                        $sqlData = createSqlColValPair($data);
                        $query = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where phone_number='$phone_number'";
                        $stmt = $this->conn->prepare($query);
                        $stmt->execute();
                    }
                    return  ['status' => SUCCESS, 'code' => SUCCESS_CODE, 'sms_gateway' =>$data['sms_gateway'], 'address' => '+1'.$phone_number.'@'.$data['sms_gateway']];
                }
            }
            return  ['status' => ERROR, 'code' => ERROR_CODE, 'sms_gateway' =>'No Sms Gateway Specified!'];
        }
        return  ['status' => ERROR, 'code' => ERROR_CODE, 'sms_gateway' =>'No Sms Gateway Specified!'];
    } catch(Exception $exception){
        return ['msg' => ERROR_SERVER_MSG, 'code' => 500, 'message' =>$exception->getMessage()];
    }
}

/**
 * function to save user account detail
 * @param $id
 * @param $connection
 * @return array
 */
function saveUserAccountDetail($connection,$stripe_account_array){
    $address1 = "address_full_match"; /*$fdata['faddress1'];*/
    $address2 = ""; /*$fdata['faddress2'];*/
    $sqlData1 = createSqlColVal($stripe_account_array);
    $query2 = "INSERT INTO user_account_detail (" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
    $stmt2 = $connection->prepare($query2);
    $stmt2->execute($stripe_account_array);
}

function createSqlUpdateCase($data) {
    $columnsValuesPair = '';
    $dataPair = [];
    foreach ($data as $key => $value) {
        $columnsValuesPair .= $key . "=? ,";

        if(empty($value) && $value != 0 ) $value = null;
        array_push($dataPair,$value);
    }
    $columnsValuesPair = substr_replace($columnsValuesPair, "", -1);
    $sqlData = ['columnsValuesPair' => $columnsValuesPair, 'data' => $dataPair];
    return $sqlData;
}

/**
 * function to replace last occurance from a string
 * @param $search
 * @param $replace
 * @param $subject
 * @return mixed
 */
function str_replace_last($search, $replace, $subject) {
    $pos = strrpos($subject, $search);
    if ($pos !== false) {
        $subject = substr_replace($subject, $replace, $pos, strlen($search));
    }
    return $subject;
}

/**
 * Function for fetching Role based on user_type
 * @param $role
 * @return string
 */
function getRole($role) {
    try {
        switch ($role) {
            case "0":
                $roleName = 'Super Admin';
                break;
            case "1":
                $roleName = 'Company User';
                break;
            case "2":
                $roleName = 'Tenant';
                break;
            case "3":
                $roleName = 'Vendor';
                break;
            case "4":
                $roleName = 'Owner';
                break;
            case "5":
                $roleName = 'Contact';
                break;
            case "6":
                $roleName = 'Guest';
                break;
            case "7":
                $roleName = 'Rental';
                break;
            case "8":
                $roleName = 'Employee';
                break;
            case "9":
                $roleName = 'Manager';
                break;
            default:
                $roleName = 'None';
        }
        return $roleName;
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}
?>
