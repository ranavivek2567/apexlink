<?php

/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/17/2019
 * Time: 3:30 PM
 */

include("$_SERVER[DOCUMENT_ROOT]/config.php");
include_once( "$_SERVER[DOCUMENT_ROOT]/superadmin/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};

class AnnouncementAjax extends DBConnection {

    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     *  function to create a user for super admin
     */
    public function addAnnouncement() {
        try {
            $data = $_POST['form'];
            $data = $this->postArray($data);
            $timezone_offset =  $_REQUEST["timezone"];
            $timezone_name = timezone_name_from_abbr("", $timezone_offset*60, false);
            date_default_timezone_set($timezone_name);
            //print_r($timezone_offset); exit;
            //Required variable array
            $required_array = ['title','start_date','end_date','start_time','end_time'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($data['ism']) && !empty($data['ism'])){
                 $data['end_time'] = date("H:i",strtotime('+2 hour ',strtotime($data['start_time'])));
                 unset($data['ism']);
                      
                      
                }else{
                    $data['end_time'] = date("H:i", strtotime($data['end_time']));
                }
                if(isset($data['announcementID']) && !empty($data['announcementID'])){
                    unset($data['announcementID']);
                }

                $start_date =  mySqlDateFormat($data['start_date'],$_SESSION[SESSION_DOMAIN]["user_id"]);

                $start_time =  date("H:i", strtotime($data['start_time']));
                $new_start_date_time  = date('Y-m-d H:i:s', strtotime("$start_date $start_time"));
                $str_start_date_to_time = strtotime($new_start_date_time);
                $current_date_time = $date = strtotime(date('Y-m-d H:i:s'));
                $current_date_time_diff = $str_start_date_to_time - $current_date_time;

                $end_date =  mySqlDateFormat($data['end_date'],$_SESSION[SESSION_DOMAIN]["user_id"]);

                $end_time =  date("H:i", strtotime($data['end_time']));
                $new_end_date_time  = date('Y-m-d H:i:s', strtotime("$end_date $end_time"));
                $str_end_date_to_time = strtotime($new_end_date_time);
                $end_date_time_diff = $str_end_date_to_time - $str_start_date_to_time;
                if ($current_date_time_diff <= 0) {
                    return array('code' => 502, 'status' => 'error', 'message' => 'The time you have selected has past, please schedule a time in the future.');
                }
                if ($end_date_time_diff == 0) {
                    return array('code' => 502, 'status' => 'error', 'message' => 'Start Time and End Time cannot be Equal.');
                }
                if ($end_date_time_diff < 0) {
                    return array('code' => 502, 'status' => 'error', 'message' => 'End Time cannot be Less than start time.');

                }
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
                $data['start_date'] =    mySqlDateFormat($data['start_date'],$_SESSION[SESSION_DOMAIN]['user_id']);
                $data['end_date'] = mySqlDateFormat( $data['end_date'],$_SESSION[SESSION_DOMAIN]['user_id']);
                $data['start_time'] = date("H:i", strtotime($data['start_time']));     
                $sqlData = $this->createSqlColVal($data);
                $query = "INSERT INTO announcements (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->conn->prepare($query);
                $stmt->execute($data);
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records Added successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  function to get post data array
     */
    public function postArray($post) {
        $data = [];
        foreach ($post as $key => $value) {
            if (!empty($value['value'])) {
                $dataValue = $this->test_input($value['value']);
            } else {
                $dataValue = $value['value'];
            }
            $data[$value['name']] = $dataValue;
        }
        return $data;
    }

    /**
     *  Server side validation function
     */
    public function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    /**
     *  Server side validation function
     */
    public function createSqlColVal($data) {
        $columns = '';
        $columnsValues = '';
        foreach ($data as $key => $value) {
            $columns .= $key . ',';
            $columnsValues .= ':' . "$key" . ',';
        }
        $columns = substr_replace($columns, "", -1);
        $columnsValues = substr_replace($columnsValues, "", -1);
        $sqlData = ['columns' => $columns, 'columnsValues' => $columnsValues];
        return $sqlData;
    }

    /**
     * Random String!
     * Use this endpoint to generate the eight number random string.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function randomString($length = 8) {
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }



    /**
     * Get Announcement by id
     * @param Request $request
     * @return array
     */
    public function getAnnouncement() {
        try {
            $announcement_id = $_GET['id'];
            $query = $this->conn->query("SELECT * FROM announcements WHERE id = '$announcement_id' AND status = '1' ");
            $announcementData = $query->fetch();
            if($announcementData){
                //print_r($announcementData['start_date']);exit;
                $announcementData['start_date']= dateFormatUser($announcementData['start_date'],$_SESSION[SESSION_DOMAIN]["user_id"]);
                $announcementData['end_date']= dateFormatUser( $announcementData['end_date'],$_SESSION[SESSION_DOMAIN]["user_id"]);
                return array('status' => 'success', 'data' => $announcementData ,'code' => 200);
            }else{
                return array('status' => 'error', 'message' => 'No Records Found','code' => 400);
            }
            return $json_data;
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  function to create a user for super admin
     */
    public function editAnnouncement() {
        try {
            $data = $_POST['form'];
            $data = $this->postArray($data);
            $annoucement_id = $data['announcementID'];
            $timezone_offset =  $_REQUEST["timezone"];
            $timezone_name = timezone_name_from_abbr("", $timezone_offset*60, false);
            date_default_timezone_set($timezone_name);
            //Required variable array
            $required_array = ['title','start_date','end_date','start_time','end_time'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($data['ism']) && !empty($data['ism'])){
                    $data['end_time'] = date("H:i",strtotime('+2 hour ',strtotime($data['start_time'])));
                    unset($data['ism']);
                }else{
                    $data['end_time'] = date("H:i", strtotime($data['end_time']));
                }
                if(isset($data['announcementID']) && !empty($data['announcementID'])){
                    unset($data['announcementID']);


                }

                $start_date =  mySqlDateFormat($data['start_date'],$_SESSION[SESSION_DOMAIN]["user_id"]);
                $start_time =  date("H:i", strtotime($data['start_time']));
                $new_start_date_time  = date('Y-m-d H:i:s', strtotime("$start_date $start_time"));
                $str_start_date_to_time = strtotime($new_start_date_time);
                $current_date_time = $date = strtotime(date('Y-m-d H:i:s'));
                $current_date_time_diff = $str_start_date_to_time - $current_date_time;

                $end_date =  mySqlDateFormat($data['end_date'],$_SESSION[SESSION_DOMAIN]["user_id"]);

                $end_time =  date("H:i", strtotime($data['end_time']));
                $new_end_date_time  = date('Y-m-d H:i:s', strtotime("$end_date $end_time"));
                $str_end_date_to_time = strtotime($new_end_date_time);
                $end_date_time_diff = $str_end_date_to_time - $str_start_date_to_time;
                if ($current_date_time_diff <= 0) {
                    return array('code' => 502, 'status' => 'error', 'message' => 'The time you have selected has past, please schedule a time in the future.');
                }
                if ($end_date_time_diff == 0) {
                    return array('code' => 502, 'status' => 'error', 'message' => 'Start Time and End Time cannot be Equal.');
                }
                if ($end_date_time_diff < 0) {
                    return array('code' => 502, 'status' => 'error', 'message' => 'End Time cannot be Less than start time.');

                }

                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
                $data['start_date'] = mySqlDateFormat($data['start_date'],$_SESSION[SESSION_DOMAIN]["user_id"]);
                $data['end_date'] = mySqlDateFormat($data['end_date'],$_SESSION[SESSION_DOMAIN]["user_id"]);
                $data['start_time'] = date("H:i", strtotime($data['start_time']));
                $sqlData = $this->createSqlColValPair($data);
                $query = "UPDATE announcements SET ".$sqlData['columnsValuesPair']." where id='$annoucement_id'";
                $stmt = $this->conn->prepare($query);

                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records Added successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    /**
     *  function to create a user for super admin
     */
    public function deleteAnnouncement() {
        try {
            $data=[];
            $annoucement_id = $_POST['annoucement_id'];
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['deleted_at'] = date('Y-m-d H:i:s');
                $sqlData = $this->createSqlColValPair($data);
                $query = "UPDATE announcements SET ".$sqlData['columnsValuesPair']." where id='$annoucement_id'";
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Announcement Deleted Successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  column value pair for update query
     */
    public function createSqlColValPair($data) {
        $columnsValuesPair = '';
        foreach ($data as $key=>$value){
            if($key == 'c_id'){
                //do nothing
            }else{
                $columnsValuesPair .=  $key."='".$value."',";
            }
            //$columnsValuesPair .= $key = ".$value.".',';
        }
        $columnsValuesPair = substr_replace($columnsValuesPair ,"",-1);
        $sqlData = ['columnsValuesPair'=>$columnsValuesPair];
        return $sqlData;
    }

}

$announcement = new AnnouncementAjax();
