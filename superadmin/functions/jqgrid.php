<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/17/2019
 * Time: 3:30 PM
 */
include(ROOT_URL."/config.php");
include_once( SUPERADMIN_DIRECTORY_URL."/helper/helper.php");
include_once( SUPERADMIN_DIRECTORY_URL."/helper/predis.php");


//include_once ('constants.php');
//include_once (ROOT_URL.'/vendor/jqGridPHP-master/php/jqGridLoader.php');
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__))
{
    $url = BASE_URL."login";
    header('Location: '.$url);
};

/**
 * JqGrid Server Side Implementation
 * Class jqGrid
 */
class jqGrid extends DBConnection
{
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * function to display jqGrid listing dynamic
     */
    public function listing_ajax()
    {
        $page = $_REQUEST['page']; // get the requested page
        $limit = $_REQUEST['rows']; // get how many rows we want to have into the grid
        $sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort
        $sord = $_REQUEST['sord']; // get the direction
        $deleted_at = isset($_REQUEST['deleted_at'])? $_REQUEST['deleted_at'] : true;
        $query = isset($_REQUEST['query'])?$_REQUEST['query']:false;
        // Db details for jq grid
        $table = $_REQUEST['table'];
        $select = isset($_REQUEST['select'])?$_REQUEST['select']:null;
        $columns_options = $_REQUEST['columns_options'];
        $columns = $this->get_columns_info($columns_options);
        $sidx = $this->get_sidx($sidx,$columns);
        if (!$sidx) $sidx = 1;
        $status = isset($_REQUEST['status'])?$_REQUEST['status']:'All';
        $deleted_at_where = '';
        $extra_where = isset($_REQUEST['extra_where'])?$_REQUEST['extra_where']:null;

        //Specifying joins if any
        $joins = (isset($_REQUEST['joins']) && !empty($_REQUEST['joins']))?$this->build_join_query($_REQUEST['joins']):$this->build_join_query([]);
        $extra_columns = (isset($_REQUEST['extra_columns']) && !empty($_REQUEST['extra_columns']))? $this->build_extra_columns($_REQUEST['extra_columns']):'';

        //building single where condition for ignore list and status
        $ignore_data = (isset($_REQUEST['ignore']) && !empty($_REQUEST['ignore']))? $_REQUEST['ignore']: null;
        if ($deleted_at == 'true') $deleted_at_where = ' WHERE deleted_at IS NULL';
        if ($deleted_at == 'false') $deleted_at_where = ' WHERE deleted_at IS NOT NULL';

        $combined_where = $this->get_combined_where($ignore_data,$status,$table,$deleted_at_where,$extra_where);

        //building columns data
        $columns_string = $this->build_column_string($columns);

        // filter data.
        if ($_REQUEST['_search'] == 'true')
        {
            $filters = (isset($_REQUEST['filters']) && !empty($_REQUEST['filters']))? json_decode($_REQUEST['filters']) : null;
            if(!empty($filters)){
                $whereString = " WHERE (";
                $allempty = 'true';
                $con = ' AND';
                foreach ($filters as $key=>$value) {
                    if($value->data == 'all') continue;
                    $searchField = ($this->getAdvanceSearchColumn($value->field, $columns) != '') ? $this->getAdvanceSearchColumn($value->field, $columns) : $value->field;
                    $searchString = $value->data;
                    $searchString2 = (isset($value->data2) && !empty($value->data2))?$value->data2:null;
                    if ((strtolower($value->field) == 'status' && strtolower($searchString) == 'active') || (strtolower($value->field) == 'is_default' && strtolower($searchString) == 'yes') || (strtolower($value->field) == 'posting_status' && strtolower($searchString) == 'posting')) $searchString = 1;
                    if ((strtolower($value->field) == 'status' && strtolower($searchString) == 'inactive') || (strtolower($value->field) == 'is_default' && strtolower($searchString) == 'no') || (strtolower($value->field) == 'posting_status' && strtolower($searchString) == 'nonposting')) $searchString = 0;
//                    if ((strtolower($value->field) == 'announcement_for' && strtolower($searchString) == 'tenant')) $searchString = '2';
//                    if ((strtolower($value->field) == 'announcement_for' && strtolower($searchString) == 'vendor')) $searchString = '3';
//                    if ((strtolower($value->field) == 'announcement_for' && strtolower($searchString) == 'owner')) $searchString = '4';
                    $searchOper = $value->op;
                    $int = isset($value->int)?$value->int:null;
                    //Advance search where condition
                    $whereFilters = $this->advance_search_filters($searchField, $searchString, $searchOper,$int,$searchString2);
                    $whereString .= $whereFilters;
                    if($value->data != 'all'){
                        $allempty = 'false';
                    }

                    if(isset($value->con)){
                        $con = $value->con;
                    } else {
                        $con = 'AND';
                    }
                    $whereString .= ' '.$con;
                }
                $whereStringBefore = str_replace_last(' '.$con, '', $whereString);
                $whereStringBefore .= ")";
                if($allempty == 'true'){
                    $whereStringBefore = "";
                }
                $where = $whereStringBefore;
            } else {
                $searchField = $this->getAdvanceSearchColumn($_REQUEST['searchField'], $columns);
               // dd($_REQUEST);
                $searchString = $_REQUEST['searchString'];
                if((strtolower($_REQUEST['searchField']) == 'status' && (strtolower($searchString) == 'active' || strtolower($searchString) == 'true')) || (strtolower($_REQUEST['searchField']) == 'is_default' && strtolower($searchString) == 'yes')) $searchString = 1;
                if((strtolower($_REQUEST['searchField']) == 'status' && (strtolower($searchString) == 'inactive' || strtolower($searchString) == 'false')) || (strtolower($_REQUEST['searchField']) == 'is_default' && strtolower($searchString) == 'no')) $searchString = 0;
                $searchOper = $_REQUEST['searchOper'];
                //Advance search where condition
                $where = $this->advance_search($searchField,$searchString,$searchOper);
            }

        }

        $totalrows = isset($_REQUEST['totalrows']) ? $_REQUEST['totalrows'] : false;

        if ($totalrows) {
            $limit = $totalrows;
        }

        if(empty($combined_where)){
            $sql = 'SELECT '.$extra_columns.' '.$table.'.id, '.$columns_string.' FROM '.$table. $joins.$deleted_at_where;
        } else {
            $sql = 'SELECT '.$extra_columns.' '.$table.'.id, '.$columns_string.' FROM '.$table. $joins. $combined_where;
        }

        if(isset($where)){
            if(empty($combined_where)){
                $sql = $sql. $where;
            } else {
                $where = str_replace("WHERE ","",$where);
                $sql = $sql.' AND '.$where;
            }
        }
       // dd($query);
        if($query){
            $sql =  $query;
        }

        $result = $this->conn->query($sql);
        $users = $result->fetchAll();

        $row = $users;
        $count = count($row);

        if ($count > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages) $page = $total_pages;
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        if($start < 0) $start = 0;

        if(isset($where)){
            $SQL = $this->conn->query("$sql ORDER BY $sidx $sord LIMIT $limit");
        } else {
            if(empty($combined_where)){
                $SQL = $this->conn->query("$sql ORDER BY $sidx $sord LIMIT $start , $limit");
            } else {
                $SQL = $this->conn->query("$sql ORDER BY $sidx $sord LIMIT $start , $limit");
            }
        }
        //dd($sql);
        $data = $SQL->fetchAll();
       // dd($data);
        //making column data
        $json_data = [];
        if (count($data) > 0){
            foreach ($data as $key => $value) {
                //buid dynamic data
                $json_data[$key]['id'] = $value['id'];
                foreach ($columns as $key1 => $value1){
                    $original_columns_name = !empty($value1['original_index'])?$value1['original_index']:$value1['original'];
                    $alias_column = (isset($value1['alias']) && !empty($value1['alias']))?$value1['alias']:$original_columns_name;
                    if(empty($value1['default'])){
                        if($value1['original'] == 'created_at' || $value1['original'] == 'updated_at' || $value1['original'] == 'deleted_at'){
                            if($value[$value1['original']] != null) {
                                $json_data[$key][$value1['display']] = dateFormatUser($value[$value1['original']], null);
                            }
                            continue;
                        }
                    }

                    if(!empty($value1['change'])){
                        if($value1['change'] == 'date'){
                            $json_data[$key][$value1['display']] = ($value[$alias_column] != null && $value[$alias_column] != '') ? dateFormatUser($value[$alias_column], null) :'';
                            continue;
                        } elseif($value1['change'] == 'time'){
                            $json_data[$key][$value1['display']] = ($value[$alias_column] != null && $value[$alias_column] != '') ? timeFormat($value[$alias_column], null) :'';
                            continue;
                        } elseif($value1['change'] == 'name'){
                            $json_data[$key][$value1['display']] = ($value[$alias_column] != null && $value[$alias_column] != '') ? userName($value['id'],$value1['table']) :'';
                            continue;
                        } elseif($value1['change'] == 'no_of_hits'){
                            $json_data[$key][$value1['display']] = noOfHits($value['id']);
                            continue;
                        } elseif($value1['change'] == 'last_login'){
                            $json_data[$key][$value1['display']] = lastLogin($value['id']);
                            continue;
                        } elseif($value1['change'] == 'number_format'){
                            $json_data[$key][$value1['display']] = number_format((float)$value[$alias_column], 2, '.', '');
                            continue;
                        } elseif($value1['change'] == 'count_Users'){
                            $json_data[$key][$value1['display']] = $this->count_Users($value['country'],$value['state']);
                            continue;
                        } elseif($value1['change'] == 'count_ActiveUsers'){
                            $json_data[$key][$value1['display']] = $this->count_ActiveUsers($value['country'],$value['state']);
                            continue;
                        }  elseif($value1['change'] == 'count_PaidUsers'){
                        //    dd($value);
                            $json_data[$key][$value1['display']] = $this->count_PaidUsers($value['country'],$value['state']);
                            continue;
                        }
                    }
                    $json_data[$key][$value1['display']] = ($value[$alias_column] != null && $value[$alias_column] != '') ? $value[$alias_column] :'';
                }
                $status = isset($value['status'])? $value['status']: null;
                $json_data[$key]['Action'] = $this->build_select($select ,$value['id'],$status);
            }
        }
        $response = [];
        $response['page'] = $page;
        $response['total'] = $total_pages;
        $response['records'] = $count;
        $response['rows'] = $json_data;

        echo json_encode($response);
        die;
    }

    /**
     * Building select box for jqGrid rows
     * @param $select
     * @param $id
     * @param null $status
     * @return string
     */
    public function build_select($select ,$id,$status=null){
        if(empty($select)) return '';
        $data = '<select class="form-control select_options" data_id="'.$id.'"><option value="default">SELECT</option>';
        foreach ($select as $key=>$value){
            if($status != null && $value == 'status'){
                $value = $status==0?'Activate':'Deactivate';
            }
            $data .= '<option value="'.$value.'">'.strtoupper($value).'</option>';
        }
        $data .= '</select>';
        return $data;
    }

    /**
     * Extracting displayed and origingal columns names
     * @param $columns_options
     * @return array
     */
    public function get_columns_info($columns_options){
        $data = [];
        foreach ($columns_options as $key=>$value){
            $table = isset($value['table'])?$value['table']:'';
            $column_alias = '';
            if($value['name'] == 'Action' && !isset($value['alias'])) continue;
            if(!empty($value['as'])) $table = $value['as'];
            if(!empty($value['alias'])) $column_alias = ' AS '.$value['alias'];
            $arr = ['original'=>$value['index'],'display'=> $value['name'], 'columns'=>$table.".".$value['index'].$column_alias, 'table'=>$value['table'],'change'=>isset($value['change_type'])?$value['change_type']:null,'as'=>isset($value['as'])?$value['as']:null,'join'=>isset($value['join'])?$value['join']:null,'index2'=>isset($value['index2'])?$value['index2']:null,'index3'=>isset($value['index3'])?$value['index3']:null,'secondTable'=>isset($value['secondTable'])?$value['secondTable']:null,'extra_columns'=>isset($value['extra_columns'])?$value['extra_columns']:null,'search_type'=>isset($value['search_type'])?$value['search_type']:null,'update_column'=>isset($value['update_column'])?$value['update_column']:null,'original_index'=>isset($value['original_index'])?$value['original_index']:null,'attr'=>isset($value['attr'])?$value['attr']:null,'alias'=>isset($value['alias'])?$value['alias']:null,'name_id'=>isset($value['name_id'])?$value['name_id']:null,'type'=>isset($value['type'])?$value['type']:null,'weekType'=>isset($value['weekType'])?$value['weekType']:null,'default'=>isset($value['default'])?$value['default']:null];
            array_push($data,$arr);
        }
        return $data;
    }

    /**
     * Advance Search functionality for jqGrid
     * @param $searchField
     * @param $searchString
     * @param $searchOper
     * @return string
     */
    public function advance_search($searchField,$searchString,$searchOper){
        $where = " WHERE (";
        $tmpDataField = "";
        $tmpFilterOperator = "";

        // get the filter's value.
        $filterValue = trim($searchString);
        // get the filter's column.
        $filterDataField = $searchField;
        // get the filter's operator.
        $filterOperator = $searchOper;

        if ($tmpDataField == "")
        {
            $tmpDataField = $filterDataField;
        }
        else if ($tmpDataField <> $filterDataField)
        {
            $where .= ")AND(";
        }
        else if ($tmpDataField == $filterDataField)
        {
            if ($tmpFilterOperator == 0)
            {
                $where .= " AND ";
            }
            else $where .= " OR ";
        }

        // build the "WHERE" clause depending on the filter's condition, value and datafield.
        switch($searchOper)
        {
            case "cn":
                $where .= " " . $filterDataField . " LIKE '%" . $filterValue ."%'";
                break;
            case "nc":
                $where .= " " . $filterDataField . " NOT LIKE '%" . $filterValue ."%'";
                break;
            case "eq":
                $where .= " " . $filterDataField . " = '" . $filterValue ."'";
                break;
            case "bw":
                $where .= " " . $filterDataField . " LIKE '" . $filterValue ."%'";
                break;
            case "ew":
                $where .= " " . $filterDataField . " LIKE '%" . $filterValue ."'";
                break;
            case "in":
                $string = "'".$filterValue."'";
                if(strpos($filterValue, ',') !== false){
                    $data = explode(",",$filterValue);
                    $string = '';
                    foreach ($data as $key=>$value){
                        $semi = '';
                        if($key != 0) $semi = ",";
                        $string .= $semi." '".$value."'";
                    }
                }
                $where .= " " . $filterDataField . " IN  (" . $string .")";
                break;
        }
        $where .= ")";

        $tmpFilterOperator = $filterOperator;
        $tmpDataField = $filterDataField;

        // build the query.
        return $where;
    }

    /**
     * function to dont display certain columns and values
     * @param $ignore_list
     * @return string
     */
    public function get_combined_where($ignore_list=null,$status,$table,$deleted_at,$extra_where=null){
        $ignore_list = isset($ignore_list)? $ignore_list: [];
        if($deleted_at == ' WHERE deleted_at IS NULL') array_push($ignore_list,['column'=>'deleted_at','value'=>' IS NULL']);
        if($deleted_at == ' WHERE deleted_at IS NOT NULL') array_push($ignore_list,['column'=>'deleted_at','value'=>' IS NOT NULL']);
        if(strtolower($status) != 'all'){
            array_push($ignore_list,['column'=>'status','value'=>$status]);
        }
        $where = '';
        if(!empty($ignore_list)) {
            $where = " WHERE (";
            foreach ($ignore_list as $key => $value) {
                if ($key > 0) $where .= ' AND';
                if($value['column'] == 'deleted_at'){
                    $where .= " " . $table.".".$value['column'] . " IS NULL";
                } else if ($value['column'] == 'users.user_type') {
                    $where .= " " . $value['column'] . " = '" . $value['value'] . "'";
                } else if ($value['column'] != 'status') {
                    $where .= " " . $value['column'] . " != '" . $value['value'] . "'";
                } else {
                    $where .= " " . $table.".".$value['column'] . " = '" . $value['value'] . "'";
                }
            }
            if(empty($extra_where)) $where .= ")";
        }
        if(!empty($extra_where)) {
            if(empty($ignore_list)) $where .= " WHERE (";
            foreach ($extra_where as $key => $value) {
                if (!empty($ignore_list) || $key > 0) $where .= ' AND';
                if ($value['condition'] == 'IS NULL') {
                    $where .= " " . $table . "." . $value['column'] . " ".$value['condition'];
                } else if ($value['condition'] == 'IS NOT NULL') {
                    $where .= " " . $table . "." . $value['column'] . " ".$value['condition'];
                } else {
                    $where .= " " . $table . "." . $value['column'] . " ".$value['condition']." '" . $value['value'] . "'";
                }
            }
            $where .= ")";
        }
        return $where;
    }

    /**
     * function to build join query for the Select statement
     * @param $join
     * @return string
     */
    public function build_join_query($join){
        $join_query = '';
        if(!empty($join)){
            foreach ($join as $key=>$value){
                $as = '';
                $alisa = $value['on_table'];
                $type = isset($value['type']) && !empty($value['type'])?$value['type']:'LEFT JOIN';
                if(isset($value['as']) && !empty($value['as'])){
                    $as = ' as '.$value['as'].' ';
                    $alisa = $value['as'];
                }
                $join_query .= " ".$type." ".$value['on_table'].$as." ON ".$value['table'].".".$value['column']."=".$alisa.".".$value['primary'];
            }
        }
        // dd($join_query);
        return $join_query;
    }

    /**
     * function to build column string for select query
     * @param $columns
     * @return string
     */
    public function build_column_string($columns){
        $data = '';
        foreach ($columns as $key=>$value){
            $semi = '';
            if($key != 0) $semi = ",";
            $data .= $semi." ".$value['columns'];
        }
        return $data;
    }

    /**
     * Get Dynamic  column name for sorting
     * @param $sidx
     * @param $columns
     * @return mixed
     */
    public function get_sidx($sidx,$columns){
        if($sidx == 'updated_at') return $sidx;
        foreach ($columns as $key => $value) {
            if ($sidx == $value['original']) return $value['columns'];
        }
    }

    /**
     * function to specify extra columns
     * @param $columns
     * @return string
     */
    public function build_extra_columns($columns){
        $data = '';
        foreach ($columns as $key=>$value){
            if(empty($value)) continue;
            $semi = ',';
            $data .= $value.$semi;
        }
        return $data;
    }

    /**
     * function to show Alphabt nheighlighted
     * @return array
     */
    public function alphabeticSearch(){
        //Required params
        $table = (isset($_REQUEST['table']) && !empty($_REQUEST['table']))? $_REQUEST['table']:null;
        $column = (isset($_REQUEST['column']) && !empty($_REQUEST['column']))? $_REQUEST['column']:null;// get the requested page
        if(empty($table) && empty($column)){
            return array('code' => 400, 'status' => 'error', 'message' => 'please specify table and column');
        }
        $deleted_at =(isset($_REQUEST['deleted_at']) && !empty($_REQUEST['deleted_at']))? 'WHERE deleted_at == null':'';
        $sql = 'SELECT DISTINCT LEFT('.$column.', 1) as letter FROM '.$table.' '.$deleted_at.' ORDER BY letter';
        $result = $this->conn->query($sql);
        $data = $result->fetchAll();
        $alphas = range('A', 'Z');
        $response = [];
        $data_alpha = [];
        if(count($data) > 0){
            foreach($data as $key=> $value){
                if(empty($value['letter'])) continue;
                array_push($data_alpha,strtoupper($value['letter']));
            }
        }
        foreach ($alphas as $key => $value) {
            if(in_array($value,$data_alpha, TRUE)){
                $response[$value] = 1;
            } else {
                $response[$value] = 0;
            }
        }
        return array('code' => 200, 'status' => 'success', 'data' =>$response, 'message' => 'Alphabatic search record retrieved successfully.');
    }

    /**
     * function to get specific advance column
     * @param $searchField
     * @param $columns
     * @return mixed
     */
    public function getAdvanceSearchColumn($searchField,$columns){
        foreach ($columns as $key=>$value){
            if($value['original'] == $searchField) return $value['columns'];
        }
    }

    /**
     * @param $searchField
     * @param $searchString
     * @param $searchOper
     * @param null $int
     * @param null $searchString2
     * @return string
     * @throws Exception
     */
    public function advance_search_filters($searchField,$searchString,$searchOper,$int=null,$searchString2=null){
        $where = "";
        $tmpDataField = "";
        $tmpFilterOperator = "";

        // get the filter's value.
        $filterValue = trim($searchString);
        $filterValue2 = !empty($searchString2)?$searchString2:null;
        // get the filter's column.
        $filterDataField = $searchField;
        // get the filter's operator.
        $filterOperator = $searchOper;

        if ($tmpDataField == "")
        {
            $tmpDataField = $filterDataField;
        }
        else if ($tmpDataField <> $filterDataField)
        {
            $where .= ")AND(";
        }
        else if ($tmpDataField == $filterDataField)
        {
            if ($tmpFilterOperator == 0)
            {
                $where .= " AND ";
            }
            else $where .= " OR ";
        }

        // build the "WHERE" clause depending on the filter's condition, value and datafield.
        switch($searchOper)
        {
            case "cn":
                $where .= " " . $filterDataField . " LIKE '%" . $filterValue ."%'";
                break;
            case "nc":
                $where .= " " . $filterDataField . " NOT LIKE '%" . $filterValue ."%'";
                break;
            case "eq":
                if(!empty($int)){
                    $where .= " " . $filterDataField . " = " . $filterValue;
                } else {
                    $where .= " " . $filterDataField . " = '" . $filterValue . "'";
                }
                break;
            case "bw":
                $where .= " " . $filterDataField . " LIKE '" . $filterValue ."%'";
                break;
            case "ew":
                $where .= " " . $filterDataField . " LIKE '%" . $filterValue ."'";
                break;
            case "gr":
                if(!empty($int)){
                    $where .= " " . $filterDataField . " >= " . $filterValue;
                } else {
                    $where .= " " . $filterDataField . " >= '" . $filterValue ."'";
                }
                break;
            case "lt":
                if(!empty($int)){
                    $where .= " " . $filterDataField . " <= " . $filterValue;
                } else {
                    $where .= " " . $filterDataField . " <= '" . $filterValue ."'";
                }
                break;
            case "in":
                $string = "'".$filterValue."'";
                if(strpos($filterValue, ',') !== false){
                    $data = explode(",",$filterValue);
                    $string = '';
                    foreach ($data as $key=>$value){
                        $semi = '';
                        if($key != 0) $semi = ",";
                        $string .= $semi." '".$value."'";
                    }
                }
                $where .= " " . $filterDataField . " IN  (" . $string .")";
                break;
            case "dateByString":
                $date = $this->x_week_range(date('Y-m-d'));
                if($filterValue == 'Weekly'){
                    $start = $date[0];
                    $end = $date[1];
                } elseif($filterValue == 'Bi-Weekly'){
                    $strData = strtotime($date[0]);
                    $start = date('Y-m-d', strtotime('-7 days', $strData));
                    $end = $date[1];
                } else {
                    $timestamp    = strtotime($filterValue);
                    $start = date('Y-m-01', $timestamp);
                    $end  = date('Y-m-t', $timestamp);
                }
                $where .= " DATE(". $filterDataField.") BETWEEN '".$start."' AND '".$end."'";
                break;
            case "dateBetween":
                $start = !empty($filterValue)?mySqlDateFormat($filterValue,null,$this->companyConnection):date('Y-m-d');
                $end = !empty($filterValue2)?mySqlDateFormat($filterValue2,null,$this->companyConnection):date('Y-m-d');
                $where .= " DATE(". $filterDataField.") BETWEEN '".$start."' AND '".$end."'";
                break;
        }
        $where .= "";

        $tmpFilterOperator = $filterOperator;
        $tmpDataField = $filterDataField;

        // build the query.
        return $where;
    }

    public function count_Users($country,$state){

        if(!empty($country) && !empty($state)) {
       $sql = "SELECT COUNT(id) as total FROM users WHERE country= '$country' AND state='$state'";
            $result = $this->conn->query($sql)->fetch();
            return $result['total'];
         }
    }

    public function count_ActiveUsers($country,$state){
        if(!empty($country) && !empty($state)) {
         echo   $sql = "SELECT COUNT(id) FROM users WHERE status='1' AND country= '$country' AND state='$state'";
            $result = $this->conn->query($sql)->fetch();
            return $result['COUNT(id)'];
            }
        }

    public function count_PaidUsers($country,$state){
        if(!empty($country) && !empty($state)) {
            $sql = "SELECT COUNT(id) FROM users WHERE status='1' AND  country= '$country' AND state='$state' AND free_plan= '0'";
            $result = $this->conn->query($sql)->fetch();
            return $result['COUNT(id)'];
        }
    }


}

$helper = new jqGrid();