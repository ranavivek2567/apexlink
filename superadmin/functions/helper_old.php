<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/17/2019
 * Time: 3:30 PM
 */

include_once(ROOT_URL.'/config.php');
include_once ('constants.php');
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__))
{
   // $url = BASE_URL."login";
    header('Location: '.BASE_URL);
};

class HelperAjax extends DBConnection {

    /**
     * HelperAjax constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * function for super admin user login
     * @return array
     */
    public function getCountryCode(){
        try {
            $query = $this->conn->query("SELECT * FROM countries");
            $data = $query->fetchAll();

            if(!empty($data)) {
                return array('status' => 'success', 'data' => $data, 'message' => 'Data Retrieved Successfully.');
            }

            return array('status' => 'error', 'data' => $data, 'message' => 'No Data!');
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}

$helper = new HelperAjax();