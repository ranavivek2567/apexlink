<?php
/**
 * Created by PhpStorm.
 * User: ptripathi
 * Date: 1/21/2019
 * Time: 6:00 PM
 */
error_reporting(E_ALL);

// Same as error_reporting(E_ALL);
ini_set("display_errors", 1);
include_once(ROOT_URL.'/config.php');
include_once (ROOT_URL.'/constants.php');
include_once( SUPERADMIN_DIRECTORY_URL."/helper/helper.php");
include_once( "$_SERVER[DOCUMENT_ROOT]/superadmin/helper/ddl.php");

if (basename($_SERVER['PHP_SELF']) == basename(__FILE__))
{
    $url = BASE_URL."login";
    header('Location: '.$url);
}

class DefaultSettingsAjax extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        //echo $action; die;
        echo json_encode($this->$action());
    }


    /**
     *  function to create a settings for super admin
     */
    public function addSettings(){
        try {
            $data = $_POST['form'];
            $data = $this->postArray($data);
            
            $err_array = [];
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                foreach ($data as $key=>$value){
                    switch ($key) {
                        case 'company_name' :
                            if(empty($value)) {
                                $errName = $key . 'Err';
                                $err_array[$errName] = "company name is required";
                            }
                            break;
                        case 'zip_code' :
                            if(empty($value)) {
                                $errName = $key . 'Err';
                                $err_array[$errName] = "Zip Code is required";
                            }
                            break;
                        case 'timeout' :
                            if(empty($value)) {
                                $errName = $key . 'Err';
                                $err_array[$errName] = "Timeout is required";
                            }
                            break;
                        case 'address1' :
                            if(empty($value)) {
                                $errName = $key . 'Err';
                                $err_array[$errName] = "Address 1 is required";
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            //Checking server side validation
            if(!empty($err_array)){
                return array('code' => 400,'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $record = getSingleRecord($this->conn ,['column'=>'user_id','value'=>$_SESSION[SESSION_DOMAIN]['user_id']], 'default_settings');
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['user_id'];

               if($record['code'] == 200){
                $id = $_SESSION[SESSION_DOMAIN]['user_id'];
                $sqlData = $this->createSqlColValPair($data); 
                $query = "UPDATE default_settings SET ".$sqlData['columnsValuesPair']." where user_id='$id'";
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                $msg = 'Records Updated successfully';
               } else {
                $sqlData = $this->createSqlColVal($data);
                $query = "INSERT INTO default_settings (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->conn->prepare($query);
                $stmt->execute($data);
                $msg = 'Records Added successfully';
               }
                return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => $msg);
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  function to get post data array
     */
    public function ddlOperations(){
        $returnData = DDL::insert($this->conn);
        $alterColumn = array(
            array('col'=>'company_id2', 'data_type'=>'varchar', 'length'=>'255',"new_col" => ''),
            array('col'=>'c_id2', 'data_type'=>'varchar', 'length'=>'255',"new_col" => ''),
        );
        $dropColumn = array('company_id2','c_id2');
        //$returnData = DDL::update($this->conn, array('default_date_clock_settings2'), 'add', $alterColumn);
        $returnData = DDL::delete($this->conn, array('default_date_clock_settings2'));
        
        
        echo "<pre>";
        print_r($returnData);
        die();
        
    }

    /**
     *  function to get post data array
     */
    public function postArray($post){
        $data = [];
        foreach ($post as $key=>$value){
            if(!empty($value['value'])){
                $dataValue = $this->test_input($value['value']);
            } else {
                $dataValue = $value['value'];
            }
            $data[$value['name']] = $dataValue;
        }
        return $data;
    }

    /**
     *  Server side validation function
     */
    public function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    /**
     *  column value pair for insert query
     */
    public function createSqlColVal($data) {
        $columns = '';
        $columnsValues = '';
        foreach ($data as $key=>$value){
            $columns .= $key.',';
            $columnsValues .= ':'."$key".',';
        }
        $columns = substr_replace($columns ,"",-1);
        $columnsValues = substr_replace($columnsValues ,"",-1);
        $sqlData = ['columns'=>$columns,'columnsValues'=>$columnsValues];
        return $sqlData;
    }
    /**
     *  column value pair for update query
     */
    public function createSqlColValPair($data) {
        $columnsValuesPair = '';
        foreach ($data as $key=>$value){
            if($key == 'c_id'){
            //do nothing    
            }else{
            $columnsValuesPair .=  $key."='".$value."',";
            }

            //$columnsValuesPair .= $key = ".$value.".',';
        }
        $columnsValuesPair = substr_replace($columnsValuesPair ,"",-1);
        $sqlData = ['columnsValuesPair'=>$columnsValuesPair];
        return $sqlData;
    }
    /**
     * Random String!
     * Use this endpoint to generate the eight number random string.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function randomString($length = 8) {
        $str = "";
        $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    /**
     *  function for listing of plans
     */
    public function viewSettings(){
        try {
            $user_id = $_SESSION[SESSION_DOMAIN]['user_id'];
            $query = $this->conn->query("SELECT * FROM default_settings WHERE user_id=$user_id");
            $settings = $query->fetch();
            if($settings){                
                return array('status' => 'success', 'data' => $settings);
            }else{
                return array('status' => 'error', 'message' => 'No Records Found');
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  function for listing of plans
     */
    public function viewClockSettings(){
        try {
            $user_id = $_SESSION[SESSION_DOMAIN]['user_id'];
            $query = $this->conn->query("SELECT * FROM default_date_clock_settings where  user_id='$user_id'");
            $settings = $query->fetch();
            if($settings){                
                return array('status' => 'success', 'data' => $settings);
            }else{
                return array('status' => 'error', 'message' => 'No Records Found');
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  function to create a clock settings for super admin
     */
    public function addClockSettings(){
        try {
            $data = $_POST['form'];
            $data = $this->postArray($data);

            $err_array = [];
//            $err_array = validation();
            //Checking server side validation
            if(!empty($err_array)){
                return array('code' => 400,'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $id = $_SESSION[SESSION_DOMAIN]['user_id'];
                $query = $this->conn->query("SELECT * FROM default_date_clock_settings");
                $count = $query->fetchAll();
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
                $data['company_id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
               if(!empty($count['data'])){
                $sqlData = $this->createSqlColValPair($data); 
                $query = "UPDATE default_date_clock_settings SET ".$sqlData['columnsValuesPair']." where user_id=$id";
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                $msg = 'Records updated successfully';
               } else {
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO default_date_clock_settings (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";

                $stmt = $this->conn->prepare($query);
                $stmt->execute($data);
//                dd($stmt);
                $msg = 'Records added successfully';
               }
                $_SESSION[SESSION_DOMAIN]['formated_date'] = dateFormatUser(date('Y-m-d H:i:s'), $_SESSION[SESSION_DOMAIN]['user_id']);
                $data['formatted_date'] = $_SESSION[SESSION_DOMAIN]['formated_date'];
                $_SESSION[SESSION_DOMAIN]['datepicker_format'] = getDatePickerFormat($_SESSION[SESSION_DOMAIN]['user_id']);
                $data['datepicker_format'] = $_SESSION[SESSION_DOMAIN]['datepicker_format'];
                return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => $msg);
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function systemLogout(){
        $query = $this->conn->query("SELECT timeout FROM default_settings");
        $data1 = $query->fetch();
        return $data1['timeout'];
    }

    /**
     * Function for company user logout
     * @return array
     */
    public function logout(){
        $url=$_POST['data'];

        $condition = ['column'=>'id','value'=>$_SESSION[SESSION_DOMAIN]['user_id']];
        $user = getSingleRecord($this->conn,$condition,'users');
        if(!empty($url)){
            $update_last_url_company= $this->conn->prepare("UPDATE users SET last_user_url=? WHERE id=?")
                ->execute([$url, $_SESSION[SESSION_DOMAIN]['user_id']]);

            $update_last_url_super_admin= $this->conn
                ->prepare("UPDATE users SET last_user_url=? WHERE id=?")
                ->execute([$url, $_SESSION[SESSION_DOMAIN]['user_id']]);
        }
        $this->audiTrail($user['data'],'logout');
        unset($_SESSION[SESSION_DOMAIN]['user_id']);
        return array('status' => 'success', 'data' =>[$update_last_url_company, $update_last_url_super_admin]);
    }

    public function audiTrail($data,$type){
        $insert = [];
        $insert['user_id'] = $data['id'];
        $insert['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $insert['action'] = $type;
        $insert['created_at'] =  date('Y-m-d H:i:s');
        $insert['description'] = ($type == 'login')?'Login : System Login':'Login : System Logout';
        $sqlData = createSqlColVal($insert);
        $query = "INSERT INTO audit_trial (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
        $stmt = $this->conn->prepare($query);
        $stmt->execute($insert);
    }
}

$user = new DefaultSettingsAjax();
