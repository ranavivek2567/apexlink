<?php
/**
 * Created by PhpStorm.
 * User: ptripathi
 * Date: 1/21/2019
 * Time: 6:00 PM
 */
//include_once('../config.php');
include_once ('constants.php');
include_once( SUPERADMIN_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__))
{
    // $url = BASE_URL."login";
    header('Location: '.BASE_URL);
};

class PlanAjax extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
//        print_r($action); die('>>>>$action$action');
        echo json_encode($this->$action());
    }

    /**
     * function for add new plan
     * @return array
     */
    public function insert(){

        try {
            $data = $_POST['form'];
            $data = postArray($data);

            //Required variable array
            $required_array = ['plan_name','number_of_units'];

            /* Max length variable array */
            $maxlength_array = [];

            //Number variable array
            $number_array = [];

            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);

            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                if(isset($data['save'])){
                    unset($data['save']);
                }
                $checkExist = $this->conn->query("SELECT id FROM plans WHERE plan_name ='".$data['plan_name']."' ");
                 $exist = $checkExist->fetch();
                 if(!empty($exist)){
                     return array('code'=>2002,'message'=>'Plan Name Already exist');
                 }
                $number_of_units = explode('-',$data['number_of_units']);
                $data['min_units'] = $number_of_units[0];
                $data['max_units'] = $number_of_units[1];
                $data['max_units']    = $number_of_units[1].'.00';
                $data['status']    = '1';
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO plans (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->conn->prepare($query);
                $stmt->execute($data);
                $_SESSION[SESSION_DOMAIN]["message"]='This record saved successfully';
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'This record saved successfully.');
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * function for update a plan
     * @return array
     */
    public function update(){

        try {
            $data = $_POST['form'];
            $data = postArray($data);
            $hidden_plan_id = $data['hidden_plan_id'];

            //Required variable array
            $required_array = ['plan_name','number_of_units'];

            /* Max length variable array */
            $maxlength_array = [];

            //Number variable array
            $number_array = [];

            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);

            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($hidden_plan_id) && !empty($hidden_plan_id)){
                    unset($data['hidden_plan_id']);
                }
                if(isset($data['save'])){
                    unset($data['save']);
                }
                $number_of_units = explode('-',$data['number_of_units']);
                $data['min_units'] = $number_of_units[0];
                $data['max_units'] = $number_of_units[1];
                $data['updated_at'] = date('Y-m-d H:i:s');

                $sqlData = createSqlColValPair($data);
                $query = "UPDATE plans SET ".$sqlData['columnsValuesPair']." where id='$hidden_plan_id'";
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
//                print_r($stmt);die('asasa');
                $_SESSION[SESSION_DOMAIN]["message"]='This record updated successfully';
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'This record updated successfully.');
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * function for view plan
     * @return array
     */
    public function view(){
        try {
            $id = $_POST['id'];
            $query = $this->conn->query("SELECT * FROM plans where  id='$id'");
            $plans = $query->fetch();
            if($plans){
                return array('status' => 'success', 'data' => $plans);
            }else{
                return array('status' => 'error', 'message' => 'No Records Found');
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }

    }


    public function updateStatus(){
        try {
            $id = $_POST['id'];
            $status = $_POST['status'];

            $checkExistQuery = $this->conn->query("SELECT * FROM users WHERE EXISTS (SELECT * FROM users WHERE  users.subscription_plan = '$id')");
            $checkExists = $checkExistQuery->fetch();
            if($checkExists){
                $_SESSION[SESSION_DOMAIN]["status_message"]='This plan is in use.';
                return array('code' => 400,'status' => 'error', 'message' => $_SESSION[SESSION_DOMAIN]["status_message"]);
            }
            if($status == 1)
            {
                $query = "UPDATE plans SET status='1' where id='$id'";
            } else {
                $query = "UPDATE plans SET status='0' where id='$id'";
            }

            $plans = $this->conn->prepare($query);
            $plans->execute();
            if($status == '1'){
                $_SESSION[SESSION_DOMAIN]["status_message"]='Plan activated successfully.';
            }
            if($status == '0'){
                $_SESSION[SESSION_DOMAIN]["status_message"]='Plan deactivated successfully.';
            }

            if($plans){
                return array('code' => 200,'status' => 'success', 'data' => $plans, 'message' => $_SESSION[SESSION_DOMAIN]["status_message"]);
            }else{
                return array('code' => 400,'status' => 'error', 'message' => 'No record found.');
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function checkPlanInUse(){
        try{
            $id = $_POST['id'];
            $checkExistQuery = $this->conn->query("SELECT * FROM users WHERE EXISTS (SELECT * FROM users WHERE  users.subscription_plan = '$id')");
            $checkExists = $checkExistQuery->fetch();
            if($checkExists){
                $_SESSION[SESSION_DOMAIN]["status_message"]='This plan is in use.';
                return array('code' => 200,'status' => 'success', 'message' => $_SESSION[SESSION_DOMAIN]["status_message"]);
            }else{
                return array('code' => 400,'status' => 'error', 'message' => 'No record found.');
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

}

$user = new PlanAjax();
