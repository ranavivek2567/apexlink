<?php
/**
 * Created by PhpStorm.
 * User: Vivek Rana
 * Date: 22-May-19
 * Time: 11:44 AM
 */

include_once(ROOT_URL.'/DBConnection.php');
require_once ROOT_URL.'/helper/globalHelper.php';
charge_subscription();
function charge_subscription(){
    $connection = new DBConnection();
    try {
        //fetching all companies
        $query = "SELECT * FROM USERS";
        $companyUserData = $connection->conn->query($query)->fetchAll();
        print_r($companyUserData);die;
        //checking company data
        if(!empty($companyUserData)){
            foreach ($companyUserData as $key=>$value){
                $destination_stripe = $value['stripe_account_id'];
                $dbConnection = DBConnection::dynamicDbConnection($value['host'],$value['database_name'],$value['db_username'],$value['db_password']);
                $companyQuery = "SELECT * FROM USERS WHERE id='1' AND stripe_customer_id IS NOT NULL";
                $companyUserData = $dbConnection->fetch($companyQuery)->fetch();
                if(!empty($companyUserData)){
                    $customer_id = [];
                    $customer_id['customer_id'] = $companyUserData['stripe_customer_id'];
                    $customerData = getCustomer($customer_id);
                    $amount_array = paymentCalculateForPM($dbConnection,$customerData);
                    if($customerData['code'] == 200 && $amount_array['code'] == 200){
                        $customerSource = json_decode($companyUserData['customer_data']['default_source']);
                        if(!empty($customerSource)){
                            $chargeAmount =(int)(number_format($amount_array['charge'], 2)*100);
                            $transactionCount =  $dbConnection->query("SELECT COUNT(id) AS NumberOfRecods FROM transactions")->fetchAll();
                            if($amount_array['term_plan'] == 'YEARLY') {
                                $checkSubscriptionType = $dbConnection->query("SELECT * FROM transactions WHERE term_plan = 'YEARLY' AND user_type = 'PM' AND payment_status = 'SUCCESS' AND CURRENT_DATE() >= DATE_ADD(DATE(created_at), INTERVAL +12 MONTH) ORDER BY id DESC")->fetch();
                            }  else {
                                $checkSubscriptionType = 'MONTHLY';
                            }
                            if(!empty($checkSubscriptionType) || $transactionCount ==0){
                                $chargeData = [];
                                $chargeData['amount'] = $chargeAmount;
                                $chargeData['currency'] = 'usd';
                                $chargeData['token'] = $customerSource;
                                $chargeData['customer_id'] = $companyUserData['stripe_customer_id'];
                                $stripe_payment = createSuperAdminCharge($chargeData);
                                if($stripe_payment['code'] == 200) {
                                    $charge_object = json_decode($stripe_payment['charge_data']);
                                    $transactionData = [];
                                    if ($charge_object['status'] == 'succeeded') {
                                        $transactionData['charge_id'] = $charge_object['id'];
                                        $transactionData['stripe_status'] = $stripe_payment['charge_data'];
                                        $transactionData['payment_status'] = 'SUCCESS';
                                        $transactionData['payment_response'] = 'Transaction Completed.';
                                    } else {
                                        $transactionData['payment_status'] = 'ERROR';
                                        $transactionData['payment_response'] = 'Transaction Failed.';
                                    }
                                    $transactionData['transaction_id'] = generateTransactionId();
                                    $transactionData['user_id'] = $companyUserData['id'];
                                    $transactionData['user_type'] = 'SUBSCRIPTION';
                                    $transactionData['amount'] = $amount_array['amount'];
                                    $transactionData['apex_link_service_fee'] = $amount_array['apexlinkServiceFee'];
                                    $transactionData['pay_plan_price'] = $amount_array['pay_plan_price'];
                                    $transactionData['apex_link_admin_fee'] = $amount_array['apexlinkAdminFee'];
                                    $transactionData['stripe_account_fee'] = $amount_array['stripeAccountFee'];
                                    $transactionData['stripe_transaction_fee'] = $amount_array['stripeTransactionFee'];
                                    $transactionData['transaction_type'] = $amount_array['transaction_type'];
                                    $transactionData['term_plan'] = $amount_array['term_plan'];
                                    $transactionData['total_charge_amount'] = $amount_array['charge'];
                                    $transactionData['created_at'] = date('Y-m-d');
                                    $transactionData['updated_at'] = date('Y-m-d');

                                    $sqlData = createSqlColVal($transactionData);
                                    $query = "INSERT INTO transactions (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                    $stmt = $dbConnection->fetch($query);
                                    $stmt->execute($transactionData);
                                }
                            }
                        }
                    }

                }

            }
        }
    } catch(Exception $exception){
        return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
    }
}

function paymentCalculateForPM($dbConnection,$customerData){
    try {
        //static payments variables for monthly dedcution
        $apexlinkServiceFee=75.00;
        $stripeTransactionFee=0.25;
        $feeForACH=0.25;
        $feeForCard=1.50;
        $apexlinkAdminFee=0.20;
        $stripeAccountFee=2.00;

        //static payments variables for yearly dedcution
        $apexlinkServiceFeeYearly= 75.00 * 12;
        $stripeTransactionFeeYearly=2.94;
        $feeForCardYearly=17.63;

        $getPlanD = $dbConnection->query("SELECT subscription_plan,term_plan,plan_price,pay_plan_price FROM plans_history WHERE user_id='1' AND status='1'")->fetch();
        $GetSourceDetail = GetSourceDetail($customerData);
        if(!empty($getPlanD)){
            $pay_plan_price=$getPlanD['pay_plan_price'];
            if($getPlanD['term_plan'] == '1'){
                $amount = '';
                $transaction_type = '';
                if($GetSourceDetail == 'no'){
                    $totalsubscriptionCharges=$apexlinkServiceFee + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForACH;
                    $amount = $feeForACH;
                    $transaction_type = 'ACH';
                }else if($GetSourceDetail == 'yes'){
                    $totalsubscriptionCharges=$apexlinkServiceFee + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForCard;
                    $amount = $feeForACH;
                    $transaction_type = 'CARD';
                }

                return array('code' => 200, 'status' => 'success','charge'=>$totalsubscriptionCharges,'term_plan'=>'MONTHLY','apexlinkServiceFee'=>$apexlinkServiceFee,'pay_plan_price'=>$pay_plan_price,'apexlinkAdminFee'=>$apexlinkAdminFee,'stripeAccountFee'=>$stripeAccountFee,'stripeTransactionFee'=>$stripeTransactionFee,'amount'=>$amount,'transaction_type'=>$transaction_type);
            }
            else if($getPlanD['term_plan'] == '4'){
                $pay_plan_amount = $pay_plan_price;
                $pay_plan_price=$pay_plan_price * 11;
                $amount = '';
                $transaction_type = '';
                if($GetSourceDetail == 'no'){
                    $totalsubscriptionCharges=$apexlinkServiceFeeYearly + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFeeYearly + $feeForACH;
                    $amount = $feeForACH;
                    $transaction_type = 'ACH';
                }else if($GetSourceDetail == 'yes'){
                    $totalsubscriptionCharges=$apexlinkServiceFeeYearly + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFeeYearly + $feeForCardYearly;
                    $amount = $feeForCardYearly;
                    $transaction_type = 'CARD';
                }
                return array('code' => 200, 'status' => 'success','charge'=>$totalsubscriptionCharges,'term_plan'=>'YEARLY','apexlinkServiceFee'=>$apexlinkServiceFeeYearly,'pay_plan_price'=>$pay_plan_amount,'apexlinkAdminFee'=>$apexlinkAdminFee,'stripeAccountFee'=>$stripeAccountFee,'stripeTransactionFee'=>$stripeTransactionFeeYearly,'amount'=>$amount,'transaction_type'=>$transaction_type);
            }

            else{
                return array('code' => 500, 'status' => 'error');
            }

        }


    } catch (Exception $exception) {
        return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        printErrorLog($e->getMessage());
    }

}

/**
 * Server side validation function
 * @author Deepak
 * @param $data
 * @return array
 */
function createSqlColVal($data) {
    $columns = '';
    $columnsValues = '';
    foreach ($data as $key => $value) {
        $columns .= $key . ',';
        $columnsValues .= ':' . "$key" . ',';
    }
    $columns = substr_replace($columns, "", -1);
    $columnsValues = substr_replace($columnsValues, "", -1);
    $sqlData = ['columns' => $columns, 'columnsValues' => $columnsValues];
    return $sqlData;
}

function GetSourceDetail($getDetailUser){
    $sources = $getDetailUser['customer_data']['default_source'];
    if(isset($sources) && !empty($sources)) {
        if (strpos($sources, 'card') !== false) {
            $card = 'yes';
        } else {
            $card = 'no';
        }
    }else{
        $card ="";
    }
    return $card;
}