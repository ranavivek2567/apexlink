<?php

include(ROOT_URL."/config.php");
include_once( ROOT_URL."/superadmin/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class UserAjax extends DBConnection {

    /**
     * UserAjax constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * function for super admin user login
     * @return array
     */
    public function login() {
        try {

            $email = $_POST['email'];
            $password = md5($_POST['password']);
            $domain = ADMIN_URL;


            $query = $this->conn->query("SELECT * FROM users WHERE email='$email' AND password= '$password' AND domain_name='$domain' AND user_type='0'");
            $user = $query->fetch();

//            echo '<pre>'; print_r($query); echo '</prE>'; die;
            if ($user['id']) {
                $_SESSION[SESSION_DOMAIN]['user_id'] = $user['id'];
                $_SESSION[SESSION_DOMAIN]['name'] = $user['name'];
                $_SESSION[SESSION_DOMAIN]['formated_date'] = dateFormatUser(date('Y-m-d H:i:s'), $user['id']);
                $_SESSION[SESSION_DOMAIN]['datepicker_format'] = getDatePickerFormat($user['id']);
                $_SESSION[SESSION_DOMAIN]['email'] = $email;

                if(!empty($_POST["remember"]) && $_POST["remember"] == 'on') {
                    $remember_login_data['email'] = $user["email"];
                    $remember_login_data['password'] = $user["actual_password"];
                    $remember_login_data['remember_token'] = $user["remember_token"];

                    $expire = time() + 60 * 60 * 24 * 30; // expires in one month
                    setcookie("remember_login", serialize($remember_login_data), $expire);
                } else {
                    setcookie("remember_login", '');
                }
                $logdata = [];
                $logdata['id'] = $user['id'];
                $logdata['ip_address'] = $_SERVER['REMOTE_ADDR'];
                $this->audiTrail($logdata,'login');
                return array('status' => 'success', 'data' => $user);
            } else {
                return array('status' => 'error', 'message' => 'Invalid Email or password');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Function for super admin user logout
     * @return array
     */
    public function logout(){
        $url = $_POST['data'];
        if(!empty($url)){
            $update_last_url= $this->conn
                ->prepare("UPDATE users SET last_user_url=? WHERE id=?")
                ->execute([$url, $_SESSION[SESSION_DOMAIN]['user_id']]);
        }
        $logData = [];
        $logData['id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
        $logData['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $this->audiTrail($logData,'logout');
        unset($_SESSION[SESSION_DOMAIN]['user_id']);
//        unset($_COOKIE['member_login']);
//        if (isset($_SERVER['HTTP_COOKIE'])) {
//            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
//            foreach($cookies as $cookie) {
//                $parts = explode('=', $cookie);
//                $name = trim($parts[0]);
//                setcookie($name, '', time()-1000);
//                setcookie($name, '', time()-1000, '/');
//            }
//        }
//        $member_login_data['email'] = '';
//        $member_login_data['remember_token'] = '';
//        setcookie("member_login", serialize($member_login_data), time() + (10 * 365 * 24 * 60 * 60));
        return array('status' => 'success', 'data' =>$update_last_url);
    }

    /* function for superadmin logs*/

    public function audiTrail($data,$type){
        $insert = [];
        $insert['user_id'] = $data['id'];
        $insert['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $insert['action'] = $type;
        $insert['created_at'] =  date('Y-m-d H:i:s');
        $insert['description'] = ($type == 'login')?'Login : System Login':'Login : System Logout';
        $sqlData = createSqlColVal($insert);
        $query = "INSERT INTO audit_trial (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
        $stmt = $this->conn->prepare($query);
        $stmt->execute($insert);
    }

    /**
     *  function for super admin user forgot password
     */
    public function forgotPassword() {
        try {
            $email = $_POST['email'];
            $domain = ADMIN_URL;
            $query = $this->conn->query("SELECT * FROM users WHERE email='$email' AND domain_name='$domain' AND user_type='0'");
            $user = $query->fetch();

            if ($user) {
                $forgot_password_token = randomTokenString(50);
                $email = $user['email'];

                $update_forgot_password_token = $this->conn
                    ->prepare("UPDATE users SET reset_password_token=? WHERE email=?")
                    ->execute([$forgot_password_token, $email]);

//
//                $reset_password_link = SITE_URL.'User/ResetPassword?token='.$forgot_password_token;
//                $logo = SUBDOMAIN_URL."/company/images/logo.png";
//                $body = file_get_contents(COMPANY_DIRECTORY_URL.'/views/Emails/forgot_password.php');
//                $body = str_replace("#logo#",$logo,$body);
//                $body = str_replace("#user_name#",$user['name'],$body);
//                $body = str_replace("#reset_password_link#",$reset_password_link,$body);

                $IP_address = $_SERVER['SERVER_ADDR'];
                $user_name = (!empty($user['nick_name'])) ? $user['nick_name'] : $user['first_name'];
                $user['message'] = $this->getForgotPasswordHtml($user_name,$user, $forgot_password_token, $IP_address);
//                print_r(DOMAIN_URL.'/superadmin/images/logo.png');die('<<<<<<<<<<<');
                //curlRequest($request);
                return (['status' => 'success',$user, $update_forgot_password_token]);
            } else {
                return array('status' => 'error', 'message' => 'Details corresponding to this email-Id does not exist.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * forgot password mail functionality
     * @param $user
     * @param $forgot_password_token
     * @return string
     */
    public function getForgotPasswordHtml($user_name,$user, $forgot_password_token, $IP_address) {

        $html = '<table id="background-table" style="background-color: #ececec; width: 100%;" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr style="border-collapse: collapse;">
                <td style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" align="center" bgcolor="#ececec">
                <table class="w640" style="margin: 0px 10px; width: 640px;" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr style="background-color: #00b0f0; height: 20px;">
                <td>&nbsp;</td>
                </tr>
                <tr style="border-collapse: collapse; background-color: #fff;">
                <td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580">
                    <div style="margin-top: 1px; margin-bottom: 10px; text-align: center">
                        <img src="'.DOMAIN_URL.'/superadmin/images/logo.png"/>
                    </div>
                </td>
                </tr>
                <tr style="background-color: #00b0f0; height: 20px;">
                <td>&nbsp;</td>
                </tr>
                <tr id="simple-content-row" style="border-collapse: collapse;">
                <td class="w640" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" bgcolor="#ffffff" width="640">
                <table class="w640" style="width: 640px;" border="0" cellspacing="0" cellpadding="0" align="left">
                <tbody>
                <tr style="border-collapse: collapse;">
                <td class="w30" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="30">&nbsp;</td>
                <td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580">
                <table class="w580" style="width: 580px;" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr style="border-collapse: collapse;">
                <td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580">
                <p class="article-title" style="font-size: 22px; line-height: 24px; color: #ff0000; font-weight: bold; text-align: center; margin-top: 0px; margin-bottom: 18px; font-family: Arial, Helvetica, Geneva, sans-serif;" align="left">Forgot your password?</p>
                <div class="article-content" style="font-size: 13px; line-height: 18px; color: #444444; margin-top: 0px; margin-bottom: 18px; font-family: Arial, Helvetica, Geneva, sans-serif;" align="left">
                <p style="margin-bottom: 15px;">Dear ' . $user_name . ',</p>    
                <p style="margin-bottom: 15px;">This email was sent automatically to you by Apexlink in response to your request to recover your password. This is done for your protection; only you, the recipient of this email can take the necessary step in your password recover process.</p>
                <p style="margin-bottom: 15px;">To reset your password and access your account either click on the following link below or copy link and paste the link into the address bar of your browser:</p>
                <p style="margin-bottom: 15px;"><a href="'.SITE_URL.'User/ResetPassword?token='. $forgot_password_token . '">Click here to reset your password.</a></p>
                <p style="margin-bottom: 15px;">This request was made from:</p>
                <p style="margin-bottom: 15px;">IP address: '.$IP_address.'</p>
                <p style="margin-bottom: 15px;">Thank you</p>
                <p style="margin-bottom: 15px;">Apexlink Team</p>
                </div>
                </td>
                </tr>
                <tr style="border-collapse: collapse;">
                <td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="580" height="10">&nbsp;</td>
                </tr>
                </tbody>
                </table>
                </td>
                <td class="w30" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="30">&nbsp;</td>
                </tr>
                </tbody>
                </table>
                </td>
                </tr>
                <tr style="border-collapse: collapse;">
                <td class="w640" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="640">
                <table id="footer" class="w640" style="border-radius: 0px 0px 6px 6px; -webkit-font-smoothing: antialiased; background-color: #00b0f0; color: #ffffff; width: 640px;" border="0" cellspacing="0" cellpadding="0" bgcolor="#00b0f0">
                <tbody>
                <tr style="border-collapse: collapse;">
                <td class="w580" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" colspan="4" valign="top" width="360">
                <p class="footer-content-left" style="-webkit-text-size-adjust: none; -ms-text-size-adjust: none; font-size: 13px; line-height: 15px; color: #ffffff; margin-top: 0px; margin-bottom: 15px; width: 630px;" align="center">ApexLink Property Manager ● <a style="color: #fff; text-decoration: none;" href="javascript:;">support@apexlink.com&nbsp;● 772-212-1950</a></p>
                </td>
                </tr>
                </tbody>
                </table>
                </td>
                </tr>
                <tr style="border-collapse: collapse;">
                <td class="w640" style="font-family: Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse;" width="640" height="60">&nbsp;</td>
                </tr>
                </tbody>
                </table>
                </td>
                </tr>
                </tbody>
                </table>
                ';
        return $html;
    }

    /**
     *  function for super admin user reset password
     */
    public function resetPassword() {
        try {
            $actual_password = $_POST['password'];
            $password = md5($_POST['password']);
            $forgot_password_token = $_POST['forgot_password_token'];
            $empty_forgot_password_token = NULL;
            $query = $this->conn->query("SELECT * FROM users WHERE reset_password_token='$forgot_password_token' ");
            $user_email = $query->fetch();
            $email = $user_email['email'];
            $query = $this->conn->query("SELECT * FROM users WHERE email='$email' ");
            $forgot_password_user = $query->fetch();

            if ($user_email) {
                if ($forgot_password_user) {
                    $email = $forgot_password_user['email'];
                    $forgot_password_token = $password;

                    $update_forgot_password = $this->conn
                        ->prepare("UPDATE users SET password=?, actual_password=? WHERE email=?")
                        ->execute([$forgot_password_token, $actual_password, $email]);

                    if ($update_forgot_password) {
                        $_SESSION[SESSION_DOMAIN]["message"] = "Password changed successfully.";
                        return array('status' => 'success', 'message' => 'Password changed successfully.');
                    } else {
                        return array('status' => 'warning', 'message' => 'Password not changed successfully.');
                    }
                    //return json_decode($response);
                } else {
                    return array('status' => 'error', 'message' => 'This password reset token is invalid.');
                }
            } else {
                return array('status' => 'error', 'message' => 'No user exists with this email-Id.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }



    public function get_domain($url)
    {
    $pieces = parse_url($url);
    $domain = isset($pieces['host']) ? $pieces['host'] : $pieces['path'];
    if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
        return $regs['domain'];
    }
    return false;
    }

    /**
     * function to create a user for super admin
     * @return array
     */
    public function createUser() {

        try {
            $data = $_POST['form'];
            $data = postArray($data);
            //Required variable array
            $required_array = ['first_name','last_name','email','mobile_number','country'];
            /* Max length variable array */
            $maxlength_array = ['first_name'=>20,
                'mi'=>2,
                'last_name'=>20,
                'maiden_name'=>20,
                'nick_name'=>20,
                'email'=>50,
                'work_phone'=>12,
                'mobile_number'=>12,
                'country'=>20,
                'fax'=>12];
            //Number variable array
            $number_array = ['work_phone', 'mobile_number', 'fax'];
            //Server side validation
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['parent_id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
                //$data['domain_name'] = $_SERVER['REQUEST_URI'];
                $data['domain_name'] = ADMIN_URL;
                //$data['domain_name'] = $this->get_domain($_SERVER['HTTP_HOST']);
              //  $data['domain_name'] = $_SERVER['HTTP_HOST'];
                //echo "<pre>";print_r($data['domain_name']);echo"</pre>";die("----");
                $password = randomString();
                $data['actual_password'] = $password;
//                $data['is_admin'] = $password;
                $data['password'] = md5($password);
                $data['name'] = $data['first_name'] . ' ' . $data['last_name'];
                $data['user_type'] = 0;
                $sqlData = createSqlColVal($data);

                $query = "INSERT INTO users (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                $stmt = $this->conn->prepare($query);
                $stmt->execute($data);
                $lastInsertId = $this->conn->lastInsertId();
                $this->sendMail($lastInsertId);

                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records Added successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    public function sendMail($userid = null)
    {
        try{
            $userid = (isset($userid) ? $userid : $_REQUEST['id']);
            $user_details = $this->conn->query("SELECT * FROM users where id=".$userid)->fetch();
            $user_name = userName($user_details['id'], 'users');
            $body = file_get_contents(SUPERADMIN_DIRECTORY_URL.'/views/Emails/welcomesuperadmin.php');
            $body = str_replace("#name#",$user_name,$body);
            $body = str_replace("#email#",$user_details['email'],$body);
            $body = str_replace("#password#",$user_details['actual_password'],$body);
            $body = str_replace("#logo#",SITE_URL.'/company/images/logo.png',$body);
            $body = str_replace("#website#",SITE_URL,$body);
            $request['action']  = 'SendMailPhp';
            $request['to[]']    = $user_details['email'];
            $request['subject'] = 'Welcome in Apexlink!';
            $request['message'] = $body;
            $request['portal']  = '1';
           // $request['attachments[]'] = SITE_URL.'uploads/email_attachments/WelcomeMailAttachment.docx';
             /*echo '<pre>';
             print_r($request);
             echo '</pre>';*/
            curlRequest($request);

            return ['status'=>'success','code'=>200,'data'=>$request,'message' => 'Email send successfully'];
            printErrorLog($exception->getMessage());
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }
    /**
     * function to update a user for super admin
     * @return array
     */
   
    public function updateUser() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            //Required variable array
            $required_array = ['first_name','last_name','email','mobile_number','country'];
            /* Max length variable array */
            $maxlength_array = ['first_name'=>20,
                'last_name'=>20,
                'status'=>1,
                'mobile_number'=>12];
            //Number variable array
            $number_array = ['work_phone', 'mobile_number', 'fax'];
            //Server side validation
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array,$data['id']);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['parent_id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
                $data['domain_name'] = $_SERVER['REQUEST_URI'];
                //$data['domain_name'] = $this->get_domain($_SERVER['HTTP_HOST']);   
                //echo "<pre>";print_r($data['domain_name']); echo "</pre>";  die;
                $data['name'] = $data['first_name'] . ' ' . $data['last_name'];
                $data['user_type'] = 0;
                $user_id = $data['id'];
                $sqlData = $this->createSqlColValPair($data);
                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$user_id'";
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records Added successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * function to get post data array
     * @param $post
     * @return array
     */
    public function getApexlinkList(){
        $data = $_POST['user_id'];
        $query = $this->conn->query("SELECT * FROM apexlink_list WHERE id='$data'");
        $user = $query->fetch();

        if(!empty($user)){
//            foreach ($users as $key=>$value){
                if(!empty($user['date'])) $user['date'] = dateFormatUser($user['date'], null);
              //  continue;
            //}
        }
        echo json_encode($user);
        die();
    }

    /**
     * function to get post data array
     * @param $post
     * @return array
     */
    public function getUserDetails(){
        $data = $_POST['user_id'];
        $query = $this->conn->query("SELECT * FROM users WHERE id='$data'");
        $user = $query->fetch();
        echo json_encode($user);
        die();
    }

    /**
     * Update user password!
     * Use this endpoint to update the user password.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function update_apexlink_list(){
        try {
            $data=[];
            $data['user_id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
            $data['title'] = $_POST['title'];
            $data['id'] = $_POST['id'];
            $data['date'] = $_POST['date'];
            $data['time'] = $_POST['time'];
            $data['description'] = $_POST['description'];
            $data['updated_at'] =  date('Y-m-d H:i:s');
            //Number variable array
            $required_array = ["title","date","time","description"];
            $maxlength_array = [];
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation

            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $record['title'] =$data['title'];
                $record['date'] =  mySqlDateFormat($data['date'], $id = null);
                $record['time'] = mySqlTimeFormat($data['time']);
                $record['description'] = $data['description'];
                $record['updated_at'] =  date('Y-m-d H:i:s');
                $id = $data['id'];
                $sqlData = $this->createSqlColValPair($record);
                $query = "UPDATE apexlink_list SET ".$sqlData['columnsValuesPair']." where id='$id'";
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records has been updated successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }



    /**
     * function to get post data array
     * @param $post
     * @return array
     */
    public function postArray($post) {
        $data = [];
        foreach ($post as $key => $value) {
            if (!empty($value['value'])) {
                $dataValue = $this->test_input($value['value']);
            } else {
                $dataValue = $value['value'];
            }
            $data[$post['name']] = $dataValue;
        }
        return $data;
    }

    /**
     * Server side validation function
     * @param $data
     * @return string
     */
    public function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    /**
     * Server side validation function
     * @param $data
     * @return array
     */
    public function createSqlColVal($data) {
        $columns = '';
        $columnsValues = '';
        foreach ($data as $key => $value) {
            $columns .= $key . ',';
            $columnsValues .= ':' . "$key" . ',';
        }
        $columns = substr_replace($columns, "", -1);
        $columnsValues = substr_replace($columnsValues, "", -1);
        $sqlData = ['columns' => $columns, 'columnsValues' => $columnsValues];
        return $sqlData;
    }

    /**
     * Random String!
     * Use this endpoint to generate the eight number random string.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function randomString($length = 8) {
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }


    /**
     * Delete User!
     * Use this endpoint to delete the user.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function deleteManageUser() {
        try {
            $data=[];
            $manageuser_id = $_POST['manageuser_id'];
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['deleted_at'] = date('Y-m-d H:i:s');
                $sqlData = $this->createSqlColValPair($data);
                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$manageuser_id'";
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'User Deleted Successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Update admin password!
     * Use this endpoint to update the user password.
     * @author Sarita
     * @return array
     */
    public function changePassword(){
        try {
            $data=[];
            $id = $_SESSION[SESSION_DOMAIN]['user_id'];
//            print_r($_POST);
            $current_password = $_POST['current_password'];

            $query = $this->conn->query("SELECT actual_password FROM users WHERE id='$id'");
            $user = $query->fetch();
            if ($user['actual_password'] != $current_password) {
                return array('code' => 503, 'status' => 'error', 'data' => $user['actual_password'], 'message' => 'Invaild current password.');
            }else {

                if ($_POST['id'] == "") {
                    $data['id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
                } else {
                    $data['id'] = $_POST['id'];
                }
                $data['npassword'] = $_POST['nPassword'];
                $data['cpassword'] = $_POST['cPassword'];


                //Required variable array
                $required_array = ['npassword', 'cpassword'];
                /* Max length variable array */
                $maxlength_array = ['npassword' => 15, 'cpassword' => 25];

                //Number variable array
                $number_array = [];
                //Server side validation
                $err_array = [];
                $err_array = $this->validationPassword($data, $this->conn, $required_array, $maxlength_array, $number_array);
                //Checking server side validation
                if (checkValidationArray($err_array)) {
                    return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
                } else {

                    $record['password'] = md5($data['npassword']);
                    $record['actual_password'] = $data['npassword'];
                    $id = $data['id'];
                    $sqlData = $this->createSqlColValPair($record);
                    $query = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id='$id'";
                    $stmt = $this->conn->prepare($query);
                    $stmt->execute();
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Password changed successfully');
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    /**
     * Update admin password!
     * Use this endpoint to update the user password.
     * @author Sarita
     * @return array
     */
    public function changePassword_users(){
        try {
            $data=[];
            $id = $_POST['id'];
            $data['id'] = $id;
            $data['npassword'] = $_POST['nPassword'];
            $data['cpassword'] = $_POST['cPassword'];
            //Required variable array
            $required_array = ['npassword', 'cpassword'];
            /* Max length variable array */
            $maxlength_array = ['npassword' => 15, 'cpassword' => 25];

            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = $this->validationPassword($data, $this->conn, $required_array, $maxlength_array, $number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $record['password'] = md5($data['npassword']);
                $record['actual_password'] = $data['npassword'];
                $id = $data['id'];
                $sqlData = $this->createSqlColValPair($record);
                $query = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id='$id'";
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Password changed successfully');
            }

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    /**
     * Create Apexlink List!
     * Use this endpoint to create the apexlink list.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function createApexlinkList(){
        try {
            $data=[];
            //$data['id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
            $data['user_id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
            $data['title'] = $_POST['title'];
            $data['date'] = mySqlDateFormat($_POST['date'],$_SESSION[SESSION_DOMAIN]['user_id']);
            $data['time'] = mySqlTimeFormat($_POST['time']);
            $data['status'] = 1;
            $data['description'] = $_POST['desc'];
            $data['created_at'] =  date('Y-m-d H:i:s');
            $data['updated_at'] =  date('Y-m-d H:i:s');


            //Required variable array
            $required_array = ['title','date','time'];
            /* Max length variable array */
            $maxlength_array = [];

            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $id = $_SESSION[SESSION_DOMAIN]['user_id'];
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO apexlink_list (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->conn->prepare($query);
                $stmt->execute($data);
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Password has been updated!');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Update status!
     * Use this endpoint to update the user status.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function changeStatusManageUser() {
        try {
            $data=[];
            $manageuser_id = $_POST['manageuser_id'];
            $status_type = $_POST['status_type'];
            if ($status_type == "Deactivate") {
                $status = 0;
            }else{
                $status = 1;
            }
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['status'] = $status;
                $sqlData = $this->createSqlColValPair($data);
                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$manageuser_id'";

                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'User Deactivate Successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * validate user current password!
     * Use this endpoint to validate the user current password.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function checkCurrentPassword(){
        try {
            $data=[];
            $data['id'] = $_SESSION[SESSION_DOMAIN]['user_id'];
            $data['password'] = $_POST['password'];

            if ($data['password'] == "") {
                return array('code' => 400, 'status' => 'error', 'data' => '', 'message' => 'Validation Errors!');
            } else {
                $record['password'] = md5($data['password']);
                $id = $data['id'];

                $query = $this->conn->query("SELECT password FROM users WHERE id='$id'");
                $user = $query->fetch();
                if ($user['password'] === $record['password']) {
                    return array('code' => 200, 'status' => 'success', 'data' => $user['password'], 'message' => 'Password matched!');
                }else{
                    return array('code' => 400, 'status' => 'error', 'data' => '', 'message' => 'Password do not match!');
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  column value pair for update query
     */
    public function createSqlColValPair($data) {
        $columnsValuesPair = '';
        foreach ($data as $key=>$value){
            if($key == 'c_id'){
                //do nothing
            }else{
                $columnsValuesPair .=  $key.'="'.$value.'",';
            }

            //$columnsValuesPair .= $key = ".$value.".',';
        }
        $columnsValuesPair = substr_replace($columnsValuesPair ,"",-1);
        $sqlData = ['columnsValuesPair'=>$columnsValuesPair];
        return $sqlData;
    }

    /**
     * Server side Validations
     * @author Deepak
     * @param $data
     * @param $db
     * @param array $required_array
     * @param array $maxlength_array
     * @param array $number_array
     * @return array
     */
    function validationPassword($data,$db,$required_array=[],$maxlength_array=[],$number_array=[]){
        $err_array = array();

        if(($data['npassword'] !== $data['cpassword'])){
            $error = 'Enter same password';
            $errName = 'NewPasswordErr';
            $err_array[$errName][] = $error;
        }

        foreach ($data as $key => $value) {
            $errName = $key . 'Err';
            $err_array[$errName] = [];
            if(in_array($key,$required_array)){
                if (empty($value)) {
                    $colName = ucfirst(str_replace('_',' ',$key));
                    $error = $colName." is required";
                    array_push($err_array[$errName],$error);
                }
            }

            if(array_key_exists($key,$maxlength_array)){
                $length = strlen($value);
                if ($key != 'npassword') {
                    continue;
                }
                if (!empty($value) && $length < 8) {
                    $error = 'Please enter minimum 8 characters';
                    array_push($err_array[$errName],$error);
                }
            }
            if(empty($err_array[$errName])){
                unset($err_array[$errName]);
            }
        }
        return $err_array;
    }

    /**
     * Delete User!
     * Use this endpoint to delete the user.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function deleteApexNewUser() {
        try {
            $data=[];
            $apexnewuser_id = $_POST['apexnewuser_id'];
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['deleted_at'] = date('Y-m-d H:i:s');
                $sqlData = $this->createSqlColValPair($data);
                $query = "UPDATE apexlink_list SET ".$sqlData['columnsValuesPair']." where id='$apexnewuser_id'";
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'User Deleted Successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Update status!
     * Use this endpoint to update the user status.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function changeStatusApexNewUser() {
        try {
            $data=[];
            $apexnewuser_id = $_POST['apexnewuser_id'];
            $status_type = $_POST['status_type'];
            if ($status_type == "Disable") {
                $status = 0;
            }else{
                $status = 1;
            }
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['status'] = $status;
                $sqlData = $this->createSqlColValPair($data);
                $query = "UPDATE apexlink_list SET ".$sqlData['columnsValuesPair']." where id='".$apexnewuser_id."'";
                /* echo '<pre>';
                 print_r($query);
                 echo '</pre>';*/


                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'User Disable Successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function whatsNewFeedback()
    {
        try{
            //$data = $_POST['user_id'];
            $id = $_POST['id'];
            //$query = $this->conn->query("SELECT * FROM `apexlink_list` ORDER BY ID DESC limit 0,10");
            $query = $this->conn->query("SELECT * FROM `apexlink_list_feedback` WHERE list_id ='$id'");
            $whatsnew = $query->fetchAll();
            //echo "<pre>";print_r($whatsnew);die("hii");

            return ['code'=>200, 'status'=>'success', 'data'=>$whatsnew];
        }catch (Exception $exception)
        {
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }


}

$user = new UserAjax();
