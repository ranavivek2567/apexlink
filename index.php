<?php

include( __DIR__ .'/config.php');
include( __DIR__ .'/mysql.sessions.php');	//Include PHP MySQL sessions
//include_once ('constants.php');
$request = $_SERVER['REQUEST_URI'];
session_start();
//$session = new Session();
$website_url = explode('.', $_SERVER['HTTP_HOST']);
if($website_url[1] == 'apexlinkrealestateproperties'){
    $website_url = $website_url[1].'.'.$website_url[2];
}
//print_r($website_url);
if( ADMIN_URL == $_SERVER['HTTP_HOST'])
{
    require __DIR__ . '/superadmin/index.php';
} else if ($website_url == COMPANY_WEBSITE_URL){
    require __DIR__ . '/marketingSite/index.php';
}else {
    require __DIR__ . '/company/index.php';
}
