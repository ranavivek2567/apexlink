<?php
/**
 * Created by PhpStorm.
 * User: Vivek Rana
 * Date: 22-May-19
 * Time: 11:44 AM
 */

require_once __DIR__.'/conn.php';
require_once __DIR__.'/../helper/globalHelper.php';
include_once (ROOT_URL . "/company/helper/helper.php");
First_notice_lease_expiration();
Sec_notice_lease_expiration();
Tenant_Rental_Ins_expiration();
Tenant_Schedule_Move_out();
Auto_charge_start();
Auto_charge_end();
Property_insurance_expiring();

function First_notice_lease_expiration(){
    $connection = new DBConnection();
    try {
        //fetching all companies
        $query = "SELECT * FROM users WHERE status='1' AND free_plan='0' AND stripe_account_id IS NOT NULL";
        $companyData = $connection->conn->query($query)->fetchAll();
          /*print '<pre>';
          print_r($companyData);
          die;*/
        //checking company data
       if(!empty($companyData)){
            foreach ($companyData as $key=>$value){
                $destination_stripe = $value['stripe_account_id'];
                $dbConnection = DBConnection::dynamicDbConnection($value['host'],$value['database_name'],$value['db_username'],$value['db_password']);
                $companyQuery = "SELECT users.name,users.id as user_id,tenant_lease_details.id as leaseId,tenant_lease_details.user_id as tenantUser_id,tenant_lease_details.start_date,tenant_lease_details.end_date FROM users INNER JOIN tenant_lease_details 
     ON tenant_lease_details.user_id = users.id WHERE user_type='2' ";
                $companyUser = $dbConnection->query($companyQuery)->fetchAll();

                $getdays = "SELECT no_of_days_before FROM user_alerts WHERE alert_name LIKE '%First Notice of Lease Expiration%' ORDER BY alert_name ASC";
                $getdayslist = $dbConnection->query($getdays)->fetch();
                $totaldays = $getdayslist['no_of_days_before'];


                if(!empty($companyUser)) {
                    foreach ($companyUser as $key1 => $value1) {
                        $user_id = $value1['user_id'];
                        $LeaseEndDate = date('Y-m-d', strtotime($value1['end_date'] .'-'.$totaldays.' day'));
                        $todayDate = date('Y-m-d');

                        $endDate = dateFormatUser($value1['end_date'], $user_id, $dbConnection);
                        if($LeaseEndDate == $todayDate){
                             $notificationTitle= "First Notice of Lease Expiration";
                             $module_type = "LEASE";
                             $alert_type = "Days Specific";
                             $descriptionNotifi ='Notification of Lease Expiration for Lease ID -'.$value1['leaseId'].' on '.$endDate.'.';
                             insertNotification($dbConnection,$user_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
                        }

                    }
                }
            }
        }
    } catch(Exception $exception){
        return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
    }
}

function Sec_notice_lease_expiration(){
    $connection = new DBConnection();
    try {
        //fetching all companies
        $query = "SELECT * FROM users WHERE status='1' AND free_plan='0' AND stripe_account_id IS NOT NULL";
        $companyData = $connection->conn->query($query)->fetchAll();
        /*print '<pre>';
        print_r($companyData);
        die;*/
        //checking company data
        if(!empty($companyData)){
            foreach ($companyData as $key=>$value){
                $destination_stripe = $value['stripe_account_id'];
                $dbConnection = DBConnection::dynamicDbConnection($value['host'],$value['database_name'],$value['db_username'],$value['db_password']);
                $companyQuery = "SELECT users.name,users.id as user_id,tenant_lease_details.id as leaseId,tenant_lease_details.user_id as tenantUser_id,tenant_lease_details.start_date,tenant_lease_details.end_date FROM users INNER JOIN tenant_lease_details 
     ON tenant_lease_details.user_id = users.id WHERE user_type='2' ";
                $companyUser = $dbConnection->query($companyQuery)->fetchAll();

                $getdays = "SELECT no_of_days_before FROM user_alerts WHERE alert_name LIKE '%Second Notice of Lease Expiration%' ORDER BY alert_name ASC";
                $getdayslist = $dbConnection->query($getdays)->fetch();
                $totaldays = $getdayslist['no_of_days_before'];


                if(!empty($companyUser)) {
                    foreach ($companyUser as $key1 => $value1) {
                        $user_id = $value1['user_id'];
                        $LeaseEndDate = date('Y-m-d', strtotime($value1['end_date'] .'-'.$totaldays.' day'));
                        $todayDate = date('Y-m-d');

                        $endDate = dateFormatUser($value1['end_date'], $user_id, $dbConnection);
                        if($LeaseEndDate == $todayDate){
                            $notificationTitle= "Second Notice of Lease Expiration";
                            $module_type = "LEASE";
                            $alert_type = "Days Specific";
                            $descriptionNotifi ='Notification of Lease Expiration for Lease ID -'.$value1['leaseId'].' on '.$endDate.'.';
                            insertNotification($dbConnection,$user_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
                        }

                    }
                }
            }
        }
    } catch(Exception $exception){
        return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
    }
}



function Tenant_Rental_Ins_expiration(){
    $connection = new DBConnection();
    try {
        //fetching all companies
        $query = "SELECT * FROM users WHERE status='1' AND free_plan='0' AND stripe_account_id IS NOT NULL";
        $companyData = $connection->conn->query($query)->fetchAll();
        /*print '<pre>';
        print_r($companyData);
        die;*/
        //checking company data
        if(!empty($companyData)){
            foreach ($companyData as $key=>$value){
                $destination_stripe = $value['stripe_account_id'];
                $dbConnection = DBConnection::dynamicDbConnection($value['host'],$value['database_name'],$value['db_username'],$value['db_password']);
                $companyQuery = "SELECT u.name,u.id,tri.id as renter_id ,tri.user_id as renterUser_id,tri.expire_date FROM users as u INNER JOIN tenant_renter_insurance as tri 
     ON tri.user_id = u.id WHERE user_type='2' ";
                $companyUser = $dbConnection->query($companyQuery)->fetchAll();

                $getdays = "SELECT no_of_days_before FROM user_alerts WHERE alert_name LIKE '%Tenant Rental Ins. Expiring%' ORDER BY alert_name ASC";
                $getdayslist = $dbConnection->query($getdays)->fetch();
                $totaldays = $getdayslist['no_of_days_before'];

                if(!empty($companyUser)) {
                    foreach ($companyUser as $key1 => $value1) {
                        $user_id = $value1['id'];
                        $companyQueryNew1 = "SELECT tri.id,tri.user_id,tri.expire_date,tp.property_id,tp.unit_id FROM tenant_renter_insurance as tri JOIN tenant_property as tp on tp.user_id=tri.user_id WHERE tri.user_id = '$user_id' ";
                        $companyUserNew1 = $dbConnection->query($companyQueryNew1)->fetch();

                        $renterEndDate = date('Y-m-d', strtotime($value1['expire_date'] .'-'.$totaldays.' day'));
                        $todayDate = date('Y-m-d');

                        $endDate = dateFormatUser($value1['expire_date'], $user_id, $dbConnection);
                        if($renterEndDate == $todayDate){
                            $notificationTitle= "Tenant Rental Ins. Expiring";
                            $module_type = "TENANT";
                            $alert_type = "Days Specific";
                            $descriptionNotifi ='Rental Insurance for tenant ID-'.$user_id.' (Name - '.$value1['name'].', Property ID - '.$companyUserNew1['property_id'].', Unit # -'.$companyUserNew1['unit_id'].') will expire on '.$endDate.'.';
                            insertNotification($dbConnection,$user_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
                        }

                    }
                }
            }
        }
    } catch(Exception $exception){
        return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
    }
}


function Tenant_Schedule_Move_out(){
    $connection = new DBConnection();
    try {
        //fetching all companies
        $query = "SELECT * FROM users WHERE status='1' AND free_plan='0' AND stripe_account_id IS NOT NULL";
        $companyData = $connection->conn->query($query)->fetchAll();
        /*print '<pre>';
        print_r($companyData);
        die;*/
        //checking company data
        if(!empty($companyData)){
            foreach ($companyData as $key=>$value){
                $destination_stripe = $value['stripe_account_id'];
                $dbConnection = DBConnection::dynamicDbConnection($value['host'],$value['database_name'],$value['db_username'],$value['db_password']);
                $companyQueryMO = "SELECT u.name, u.id, mot.id as mot_id, mot.user_id as motUser_id, mot.scheduledMoveOutDate FROM users as u INNER JOIN moveouttenant as mot ON mot.user_id = u.id WHERE user_type = '2' ";
                $companyUserMO = $dbConnection->query($companyQueryMO)->fetchAll();

                $getdays = "SELECT no_of_days_before FROM user_alerts WHERE alert_name LIKE '%Schedule Move Out%' ORDER BY alert_name ASC";
                $getdayslist = $dbConnection->query($getdays)->fetch();
                $totaldays = $getdayslist['no_of_days_before'];

                if(!empty($companyUserMO)) {
                    foreach ($companyUserMO as $key1 => $value1) {
                        $user_id = $value1['id'];
                        $companyQueryNew1 = "SELECT mot.id,mot.user_id,mot.scheduledMoveOutDate,tp.property_id,tp.unit_id FROM moveouttenant as mot JOIN tenant_property as tp on tp.user_id=mot.user_id WHERE mot.user_id = '$user_id' ";
                        $companyUserNew1 = $dbConnection->query($companyQueryNew1)->fetch();
                        $renterEndDate = date('Y-m-d', strtotime($value1['scheduledMoveOutDate'] .'-'.$totaldays.' day'));
                        $todayDate = date('Y-m-d');

                        $endDate = dateFormatUser($value1['scheduledMoveOutDate'], $user_id, $dbConnection);
                        if($renterEndDate == $todayDate){
                            $notificationTitle= "Schedule Move Out";
                            $module_type = "TENANT";
                            $alert_type = "Days Specific";
                            $descriptionNotifi ='Notification of Move Out Date for Tenant ID-'.$user_id.' (Name - '.$value1['name'].', Property ID - '.$companyUserNew1['property_id'].', Unit # -'.$companyUserNew1['unit_id'].') on '.$endDate.'.';
                            insertNotification($dbConnection,$user_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
                        }

                    }
                }
            }
        }
    } catch(Exception $exception){
        return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
    }
}

function Auto_charge_start(){
    $connection = new DBConnection();
    try {
        //fetching all companies
        $query = "SELECT * FROM users WHERE status='1' AND free_plan='0' AND stripe_account_id IS NOT NULL";
        $companyData = $connection->conn->query($query)->fetchAll();
        /*print '<pre>';
        print_r($companyData);
        die;*/
        //checking company data
        if(!empty($companyData)){
            foreach ($companyData as $key=>$value){
                $destination_stripe = $value['stripe_account_id'];
                $dbConnection = DBConnection::dynamicDbConnection($value['host'],$value['database_name'],$value['db_username'],$value['db_password']);
                $companyQueryPI = "SELECT u.name, u.id, pi.id as pi_id, pi.user_id as piUser_id, pi.payment_type,pi.interval_count,pi.interval_used_count,pi.payment_category,pi.payment_date,pi.process_date FROM users as u INNER JOIN payment_interval as pi ON pi.user_id = u.id WHERE user_type = '2' and pi.status='active' and pi.interval_count != pi.interval_used_count and payment_category = '2'";
                $companyUserPI = $dbConnection->query($companyQueryPI)->fetchAll();

                $getdays = "SELECT no_of_days_before FROM user_alerts WHERE alert_name LIKE '%Auto Charge Start%' ORDER BY alert_name ASC";
                $getdayslist = $dbConnection->query($getdays)->fetch();
                $totaldays = $getdayslist['no_of_days_before'];

                if(!empty($companyUserPI)) {
                    foreach ($companyUserPI as $key1 => $value1) {
                        $user_id = $value1['id'];
                        $conDataCharge = $value1['payment_type'];

                        switch ($conDataCharge) {
                            case "weekly":
                                $renterEndDate = date('Y-m-d', strtotime($value1['process_date'] .'-'.$totaldays.' days'));
                                $endDate = dateFormatUser($value1['process_date'], $user_id, $dbConnection);
                                break;
                            case "bi-weekly":
                                $renterEndDate = date('Y-m-d', strtotime($value1['process_date'] .'-'.$totaldays.' days'));
                                $endDate = dateFormatUser($value1['process_date'], $user_id, $dbConnection);
                                break;
                            case "monthly":
                                $daCharge = $value1['payment_date'];
                                $month = date('m');
                                $year = date('Y');
                                $proccessDate = $year.'-'.$month.'-'.$daCharge;
                                $renterEndDate = date('Y-m-d', strtotime($proccessDate .'-'.$totaldays.' days'));
                                $endDate = dateFormatUser($proccessDate, $user_id, $dbConnection);
                                break;
                            default:
                                $renterEndDate = date('Y-m-d', strtotime($value1['process_date'] .'-'.$totaldays.' days'));
                                $endDate = dateFormatUser($value1['process_date'], $user_id, $dbConnection);
                        }



                        $todayDate = date('Y-m-d');


                        if($renterEndDate == $todayDate){
                            $notificationTitle= "Auto Charge Start";
                            $module_type = "TENANT";
                            $alert_type = "Days Specific";
                            $descriptionNotifi ='Charges for Tenant ID-'.$user_id.' will be applied on '.$endDate.'.';
                            insertNotification($dbConnection,$user_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
                        }

                    }
                }
            }
        }
    } catch(Exception $exception){
        return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
    }
}


function Auto_charge_end(){
    $connection = new DBConnection();
    try {
        //fetching all companies
        $query = "SELECT * FROM users WHERE status='1' AND free_plan='0' AND stripe_account_id IS NOT NULL";
        $companyData = $connection->conn->query($query)->fetchAll();
        /*print '<pre>';
        print_r($companyData);
        die;*/
        //checking company data
        if(!empty($companyData)){
            foreach ($companyData as $key=>$value){
                $destination_stripe = $value['stripe_account_id'];
                $dbConnection = DBConnection::dynamicDbConnection($value['host'],$value['database_name'],$value['db_username'],$value['db_password']);
                $companyQueryPI = "SELECT u.name, u.id, pi.id as pi_id, pi.user_id as piUser_id, pi.payment_type,pi.interval_count,pi.interval_used_count,pi.payment_category,pi.payment_date,pi.process_date FROM users as u INNER JOIN payment_interval as pi ON pi.user_id = u.id WHERE user_type = '2' and pi.status='active' and pi.interval_count != pi.interval_used_count and payment_category = '2'";
                $companyUserPI = $dbConnection->query($companyQueryPI)->fetchAll();

                $getdays = "SELECT no_of_days_before FROM user_alerts WHERE alert_name LIKE '%Auto Charge Start%' ORDER BY alert_name ASC";
                $getdayslist = $dbConnection->query($getdays)->fetch();
                $totaldays = $getdayslist['no_of_days_before'];

                if(!empty($companyUserPI)) {
                    foreach ($companyUserPI as $key1 => $value1) {
                        $user_id = $value1['id'];

                        $conDataCharge = $value1['payment_type'];

                        switch ($conDataCharge) {
                            case "weekly":
                                if($value1['interval_used_count'] < $value1['interval_count']){
                                    $getweek = $value1['interval_count'];
                                    $getweek1 = $value1['interval_used_count'];
                                    $getweekEndDate = $value1['interval_count'] - $value1['interval_used_count'];
                                }else if($value1['interval_used_count'] = $value1['interval_count']){
                                    $getweekEndDate = 0;
                                }else{
                                    $getweekEndDate = $value1['interval_count'];
                                }

                                $getChargeEnd = date('Y-m-d', strtotime($value1['process_date'] .'+'.$getweekEndDate.' weeks'));
                                $renterEndDate = date('Y-m-d', strtotime($getChargeEnd .'-'.$totaldays.' days'));
                                $endDate = dateFormatUser($value1['process_date'], $user_id, $dbConnection);
                                break;
                            case "bi-weekly":
                                if($value1['interval_used_count'] < $value1['interval_count']){
                                    $getweek = $value1['interval_count'];
                                    $getweek1 = $value1['interval_used_count'];
                                    $getweekED = $value1['interval_count'] - $value1['interval_used_count'];
                                }else if($value1['interval_used_count'] = $value1['interval_count']){
                                    $getweekED = 0;
                                }else{
                                    $getweekED = $value1['interval_count'];
                                }
                                $getweekEndDate = 2 * $getweekED;
                                $getChargeEnd = date('Y-m-d', strtotime($value1['process_date'] .'+'.$getweekEndDate.' weeks'));
                                $renterEndDate = date('Y-m-d', strtotime($getChargeEnd .'-'.$totaldays.' days'));
                                $endDate = dateFormatUser($value1['process_date'], $user_id, $dbConnection);
                                break;
                            case "monthly":
                                $daCharge = $value1['payment_date'];
                                $month = date('m');
                                $year = date('Y');
                                $proccessDate = $year.'-'.$month.'-'.$daCharge;
                                if($value1['interval_used_count'] < $value1['interval_count']){
                                    $getMonth = $value1['interval_count'];
                                    $getMonth1 = $value1['interval_used_count'];
                                    $getMonthED = $value1['interval_count'] - $value1['interval_used_count'];
                                }else if($value1['interval_used_count'] = $value1['interval_count']){
                                    $getMonthED = 0;
                                }else{
                                    $getMonthED = $value1['interval_count'];
                                }
                                $getChargeEnd = date('Y-m-d', strtotime($value1['process_date'] .'+'.$getMonthED.' months'));
                                $renterEndDate = date('Y-m-d', strtotime($getChargeEnd .'-'.$totaldays.' days'));
                                $endDate = dateFormatUser($proccessDate, $user_id, $dbConnection);
                                break;
                            default:
                                if($value1['interval_used_count'] < $value1['interval_count']){
                                    $getyear = $value1['interval_count'];
                                    $getyear1 = $value1['interval_used_count'];
                                    $getyearEndDate = $value1['interval_count'] - $value1['interval_used_count'];
                                }else if($value1['interval_used_count'] = $value1['interval_count']){
                                    $getyearEndDate = 0;
                                }else{
                                    $getyearEndDate = $value1['interval_count'];
                                }

                                $getChargeEnd = date('Y-m-d', strtotime($value1['process_date'] .'+'.$getyearEndDate.' years'));
                                $renterEndDate = date('Y-m-d', strtotime($value1['process_date'] .'-'.$totaldays.' days'));
                                $endDate = dateFormatUser($value1['process_date'], $user_id, $dbConnection);
                        }


                        $todayDate = date('Y-m-d');
                       // $todayDate = "2020-03-25";


                        if($renterEndDate == $todayDate){
                            $notificationTitle= "Auto Charge End";
                            $module_type = "TENANT";
                            $alert_type = "Days Specific";
                            $descriptionNotifi ='Charges for Tenant ID-'.$user_id.' will be closed on '.$endDate.'.';
                            insertNotification($dbConnection,$user_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
                        }

                    }
                }
            }
        }
    } catch(Exception $exception){
        return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
    }
}

function Property_insurance_expiring(){
    $connection = new DBConnection();
    try {
        //fetching all companies
        $query = "SELECT * FROM users WHERE status='1' AND free_plan='0' AND stripe_account_id IS NOT NULL";
        $companyData = $connection->conn->query($query)->fetchAll();
        /*print '<pre>';
        print_r($companyData);
        die;*/
        //checking company data
        if(!empty($companyData)){
            foreach ($companyData as $key=>$value){
                $destination_stripe = $value['stripe_account_id'];
                $dbConnection = DBConnection::dynamicDbConnection($value['host'],$value['database_name'],$value['db_username'],$value['db_password']);
                $companyQueryOPO = "SELECT u.name, u.id, opo.id as opo_id, opo.user_id as opoUser_id,opo.property_id FROM users as u INNER JOIN owner_property_owned as opo ON opo.user_id = u.id where u.user_type = '4'";
                $companyUserOPO = $dbConnection->query($companyQueryOPO)->fetchAll();

                $getdays = "SELECT no_of_days_before FROM user_alerts WHERE alert_name LIKE '%Property Insurance is Expiring%' ORDER BY alert_name ASC";
                $getdayslist = $dbConnection->query($getdays)->fetch();
                $totaldays = $getdayslist['no_of_days_before'];

                if(!empty($companyUserOPO)) {
                    foreach ($companyUserOPO as $key1 => $value1) {
                        $user_id = $value1['id'];
                        $property_id = $value1['property_id'];
                        $companyQueryNew1 = "SELECT pi.end_date FROM property_insurance as pi WHERE pi.property_id = '$property_id' ";
                        $companyUserNew1 = $dbConnection->query($companyQueryNew1)->fetch();
                        $renterEndDate = date('Y-m-d', strtotime($companyUserNew1['end_date'] .'-'.$totaldays.' day'));
                        $todayDate = date('Y-m-d');

                        $endDate = dateFormatUser($companyUserNew1['end_date'], $user_id, $dbConnection);
                        if($renterEndDate == $todayDate){
                            $notificationTitle= "Property Insurance is Expiring";
                            $module_type = "PROPERTYINSURANCE";
                            $alert_type = "Days Specific";
                            $descriptionNotifi ='Insurance for Property ID-'.$property_id.' Manor will expire on '.$endDate.'.';
                            insertNotification($dbConnection,$user_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
                        }

                    }
                }
            }
        }
    } catch(Exception $exception){
        return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
    }
}