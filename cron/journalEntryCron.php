<?php
/**
 * Created by PhpStorm.
 * User: Vivek Rana
 * Date: 22-May-19
 * Time: 11:44 AM
 */

require_once __DIR__.'/conn.php';
require_once __DIR__.'/../helper/globalHelper.php';
journal_entry_recurring();
function journal_entry_recurring(){
    $connection = new DBConnection();
    try {
        //fetching all companies
        $query = "SELECT * FROM users WHERE status='1' AND id = '98' ";
        $companyData = $connection->conn->query($query)->fetchAll();
        if(!empty($companyData)){
            foreach ($companyData as $key=>$value){

                //$destination_stripe = $value['stripe_account_id'];
                $dbConnection = DBConnection::dynamicDbConnection($value['host'],$value['database_name'],$value['db_username'],$value['db_password']);
                $journalEntryQuery = "SELECT * FROM accounting_journal_entries WHERE journal_entry_type='monthly' AND journal_entry_type_select='1' AND status='1'";
                $journalEntryData = $dbConnection->query($journalEntryQuery)->fetchAll();
                if(!empty($journalEntryData)){
                    foreach ($journalEntryData as $key1=>$value1) {
                        $date = date('Y-m-d', strtotime('+1 month', strtotime($value1['created_at'])));
                        $today_date = date('Y-m-d');
                        if($today_date == $date){
                           reverse($dbConnection,$value1['id']);
                        }
                    }
                }
            }
        }
    } catch(Exception $exception){
        return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
    }
}



/**
 * function to add custom field.
 * @return array|void
 */
 function Reverse($dbConnection,$id){
    try {

        $renovation_detail_html="";
        $data = [];
        $custom_data=[];
       // $id= $_GET["id"];
        $data = $dbConnection->query("SELECT aje.id,aje.user_id,aje.journal_entry_type,aje.journal_entry_type_select ,aje.portfolio_id,aje.property_id,aje.building_id,aje.unit_id,aje.accounting_period,aje.bank_account_id,aje.refrence_number,aje.notes,aje.status,ud.unit_prefix,gp.property_name,cpp.portfolio_name FROM accounting_journal_entries as aje LEFT JOIN general_property as gp ON gp.id=aje.property_id LEFT JOIN unit_details as ud ON ud.id=aje.unit_id LEFT JOIN company_property_portfolio as cpp ON cpp.id=aje.portfolio_id   WHERE aje.id=".$id)->fetch();

        if(!empty($data)){
            reverseJournalEntery($dbConnection,$data);
            return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Journal Entry reversed successfully.');
        }else{
            return array('code' => 400, 'status' => 'error', 'data' => $data,'message' => 'Unable to reverse journal entery.');
        }
    } catch (PDOException $e) {
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        return;
    }
}




 function reverseJournalEntery($dbConnection,$data){

    $journal_entry_data['user_id'] =   1;
    $journal_entry_data['journal_entry_type'] =  (isset($data['journal_entry_type']) && !empty($data['journal_entry_type'])? $data['journal_entry_type'] : NULL);
    $journal_entry_data["journal_entry_type_select"] = (isset($data['journal_entry_type_select']) && !empty($data['journal_entry_type_select'])? $data['journal_entry_type_select'] :NULL);
    $journal_entry_data["portfolio_id"] = (isset($data['portfolio_id']) && !empty($data['portfolio_id'])? $data['portfolio_id'] :NULL);
    $journal_entry_data["property_id"] = (isset($data['property_id']) && !empty($data['property_id'])? $data['property_id'] :NULL);
    $journal_entry_data["building_id"] = (isset($data['building']) && !empty($data['building'])? $data['building'] :NULL);
    $journal_entry_data['unit_id'] = (isset($data['building_id']) && !empty($data['building_id'])? $data['building_id'] :NULL);
    $journal_entry_data["accounting_period"] = (isset($data['accounting_period']) && !empty($data['accounting_period'])? $data['accounting_period'] :NULL);
    $journal_entry_data["bank_account_id"] = (isset($data['bank_account_id']) && !empty($data['bank_account_id'])? $data['bank_account_id'] :NULL);
    $journal_entry_data["refrence_number"] = (isset($data['refrence_number']) && !empty($data['refrence_number'])? $data['refrence_number'] :NULL);
    $journal_entry_data['status'] = "1";
    $journal_entry_data['created_at'] = date('Y-m-d H:i:s');
    $journal_entry_data['updated_at'] = date('Y-m-d H:i:s');
   // print_r($journal_entry_data); exit;
    /*Save Data in Company Database*/
    $sqlData = createSqlColVal($journal_entry_data);
    $query = "INSERT INTO accounting_journal_entries (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
    $stmt = $dbConnection->prepare($query);
    $stmt->execute($journal_entry_data);
    $last_insert_id = $dbConnection->lastInsertId();
    if($last_insert_id){

        $data2 = $dbConnection->query("SELECT * FROM accounting_file_uploads as afu WHERE afu.module_name ='journal_entry' AND afu.module_id=".$data["id"])->fetchAll();

        $data1 = $dbConnection->query("SELECT ajd.id,ajd.chart_account_id,ajd.description,ajd.debit,ajd.credit,cca.account_name FROM journal_entry_details as ajd LEFT JOIN company_chart_of_accounts as cca ON cca.id=ajd.chart_account_id WHERE ajd.journal_id=".$data["id"])->fetchAll();

        if(!empty($data2)){
            reverseFileLlibrary($dbConnection,$data2,$last_insert_id);
        }

        if($data1){
            reverseAddJournalEntryDetails($dbConnection,$data1,$last_insert_id);
        }

        return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Journal Entry reversed successfully.');
    }else{
        return array('code' => 400, 'status' => 'error', 'data' => $data,'message' => 'Unable to reverse journal entery.');
    }
}



 function reverseFileLlibrary($dbConnection,$data2,$journal_id){
    try {


        foreach ($data2 as $key => $value) {
            $data = [];
            $data['module_name'] = 'journal_entry';
            $data['module_id'] = $journal_id;
            $data['user_id'] = 1;
            $data['file_name'] = (isset($value['file_name']) && !empty($value['file_name'])? $value['file_name'] :NULL);
            $data['file_size'] = (isset($value['file_size']) && !empty($value['file_size'])? $value['file_size'] :NULL);
            $data['file_location'] = (isset($value['file_location']) && !empty($value['file_location'])? $value['file_location'] :NULL);
            $data['file_extension'] = (isset($value['file_extension']) && !empty($value['file_extension'])? $value['file_extension'] :NULL);
            $data['marketing_site'] = '0';
            $data['file_type'] = (isset($value['file_type']) && !empty($value['file_type'])? $value['file_type']:NULL);
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO accounting_file_uploads (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $dbConnection->prepare($query);
            $stmt->execute($data);
        }
        return array('code' => 200, 'status' => 'success', 'message' => 'Documents uploaded successfully.');

    } catch (PDOException $e) {
        return array('code' => 500, 'status' => 'failed','message' => $e->getMessage());
    }
}




 function reverseAddJournalEntryDetails($dbConnection,$data1,$journal_id){
    $journal_entry_details_data = [];
    $chart_accounts = $data1;
    $journal_entry_data =[];
    $i=0;
    foreach($chart_accounts as $chart_account)
    {
        $journal_entry_details_data['journal_id'] =  $journal_id;
        $journal_entry_details_data['chart_account_id'] = (isset($chart_account['chart_account_id']) && !empty($chart_account['chart_account_id'])?$chart_account['chart_account_id'] :NULL);
        $journal_entry_details_data['description'] = (isset($chart_account['description']) && !empty($chart_account['description'])? $chart_account['description'] :NULL);
        $journal_entry_details_data['debit'] = (isset($chart_account['credit']) && !empty($chart_account['credit'])? $chart_account['credit'] :NULL);
        $journal_entry_details_data['credit'] = (isset($chart_account['debit']) && !empty($chart_account['debit'])? $chart_account['debit'] :NULL);
        $journal_entry_details_data['status'] = 1;
        $journal_entry_details_data['created_at'] = date('Y-m-d H:i:s');
        $journal_entry_details_data['updated_at'] = date('Y-m-d H:i:s');
        $sqlData = createSqlColVal($journal_entry_details_data);
        $query = "INSERT INTO journal_entry_details(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
        $stmt = $dbConnection->prepare($query);
        $stmt->execute($journal_entry_details_data);

    }




    return array('status' => 'success',  'message' => 'Data added successfully.','table'=>'tenant_charges');
}

/**
 * Server side validation function
 * @author Deepak
 * @param $data
 * @return array
 */
function createSqlColVal($data) {
    $columns = '';
    $columnsValues = '';
    foreach ($data as $key => $value) {
        $columns .= $key . ',';
        $columnsValues .= ':' . "$key" . ',';
    }
    $columns = substr_replace($columns, "", -1);
    $columnsValues = substr_replace($columnsValues, "", -1);
    $sqlData = ['columns' => $columns, 'columnsValues' => $columnsValues];
    return $sqlData;
}


