<?php
/**
 * Created by PhpStorm.
 * User: Vivek Rana
 * Date: 22-May-19
 * Time: 11:44 AM
 */

require_once __DIR__.'/conn.php';
require_once __DIR__.'/../helper/globalHelper.php';
check_user_account_verified();
function check_user_account_verified(){
    try {
        //Save Data in Company Database
        $request = [];

        $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $user_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $_SESSION[SESSION_DOMAIN]['cuser_id']], 'users');
        if(!empty($user_data["data"])){
            if(!empty($user_data["data"]["stripe_account_id"])) {
                $account_id = $user_data["data"]["stripe_account_id"];
//                        $account_id = 'acct_1Fb1FXIYilvAmUaE';
                $request["account_id"] = $account_id;
                $stripe_account_array["stripe_account_id"] = $account_id;
                $connected_account_detail = getConnectedAccount($request);
                if($connected_account_detail['account_data']["business_type"]=="company"){
                    $get_person_detail = getPerson($request);
                    $status = (isset($get_person_detail["person_detail"]["data"][0]["verification"]["status"]) && $get_person_detail["person_detail"]["data"][0]["verification"]["status"] == "verified"  && empty($connected_account_detail["account_data"]["company"]["requirements"]["current_deadline"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["currently_due"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["disabled_reason"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["eventually_due"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["past_due"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["pending_verification"]) ) ? 'Verified' : "Not Verified";
                }else {
                    $status = (isset($connected_account_detail["account_data"]["individual"]["verification"]["status"]) && $connected_account_detail["account_data"]["individual"]["verification"]["status"] == "verified" && empty($connected_account_detail["account_data"]["individual"]["requirements"]["currently_due"]) && empty($connected_account_detail["account_data"]["individual"]["requirements"]["eventually_due"])&& empty($connected_account_detail["account_data"]["individual"]["requirements"]["past_due"])&& empty($connected_account_detail["account_data"]["individual"]["requirements"]["pending_verification"])) ? 'Verified' : "Not Verified";
                }
            }else{
                $connected_account_detail["account_data"] = '';
                $status =  "Not Registered";
            }
        }else{
            $status =  "Not Registered";
            $connected_account_detail["account_data"] = '';
        }

        return array('code' => 200, 'account_status' => $status, 'status' => 'success','account_data'=>$connected_account_detail["account_data"],   'message' => 'Account status fetched successfully.');
    } catch (PDOException $e) {
        echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
        return;
    }
}


