<?php
/**
 * Created by PhpStorm.
 * User: Vivek Rana
 * Date: 22-May-19
 * Time: 11:44 AM
 */

require_once __DIR__.'/conn.php';
require_once __DIR__.'/../helper/globalHelper.php';
charge_tenant_payment();
function charge_tenant_payment(){
    $connection = new DBConnection();
    try {
     
        $query = "SELECT * FROM users WHERE status='1' AND free_plan='0' AND stripe_account_id IS NOT NULL";
        $companyData = $connection->conn->query($query)->fetchAll();
      
        if(!empty($companyData)){
            foreach ($companyData as $key=>$value){
                $destination_stripe = $value['stripe_account_id'];
                $dbConnection = DBConnection::dynamicDbConnection($value['host'],$value['database_name'],$value['db_username'],$value['db_password']);
                tenantPaymentDeduction($dbConnection);
             
       
            }
        }
    } catch(Exception $exception){
        return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage(),"line"=>$exception->getLine()];
    }
}



function tenantPaymentDeduction($dbconn)
{
    $connection = new DBConnection();
     $today = date("Y-m-d");
    
    $tenantQuery = "SELECT users.id,users.customer_id,users.stripe_customer_id,payment_interval.* FROM users inner join payment_interval on users.id=payment_interval.user_id WHERE users.user_type='2' AND users.stripe_customer_id != '' and users.record_status!='6' and payment_interval.status!='cancel' and payment_interval.interval_count!=payment_interval.interval_used_count and payment_interval.process_date='$today' or payment_interval.process_date IS NULL";
    $tenantData = $dbconn->query($tenantQuery)->fetchAll();


/*    print '<pre>';
    print_r($tenantData);
    exit;*/
    $getAccountId = $dbconn->query("SELECT stripe_account_id FROM users WHERE id ='1'")->fetch();
    $account_id = $getAccountId['stripe_account_id'];

    foreach($tenantData as $interval)
    {

      $user_id = $interval['user_id'];
      $customer_id = $interval['stripe_customer_id'];
      $paymentType = $interval['payment_type']; 
      $getCredit = checkCredit($user_id,$dbconn);
      $credit = $getCredit['credit'];

          switch($paymentType):
          case "weekly":
          $interval_id = $interval['id'];
          $stripe_payment      = $interval['amount'];
        
          $interval_used_count = $interval['interval_used_count'];
        if($interval['payment_category']=='1') /*1 is for rent*/
        {
        $chargeDeduction = createChargeForRent($user_id,'weekly',$dbconn,$credit,$stripe_payment,$customer_id,$account_id);  
        } 
        else if($interval['payment_category']=='2')
        {
            /*echo 'hi';
            exit;*/
        $chargeDeduction = createChargeForUtility($user_id,'weekly',$dbconn,$credit,$stripe_payment,$customer_id,$account_id);
        print '<pre>';
        print_r($chargeDeduction);
        exit;
        }
        else if($interval['payment_category']=='3')
        {
        $chargeDeduction = createChargeForMiscellaneous($user_id,'weekly',$dbconn,$stripe_payment,$customer_id,$credit,$account_id);   
        }  
      break;
        case "bi-weekly":
          $interval_id = $interval['id'];
          $stripe_payment      = $interval['amount'];
        
          $interval_used_count = $interval['interval_used_count'];
        if($interval['payment_category']=='1') /*1 is for rent*/
        {
        $chargeDeduction = createChargeForRent($user_id,'weekly',$dbconn,$credit,$stripe_payment,$customer_id,$account_id);  
        } 
        else if($interval['payment_category']=='2')
        {
        $chargeDeduction = createChargeForUtility($user_id,'weekly',$dbconn,$credit,$stripe_payment,$customer_id,$account_id);
        }
        else if($interval['payment_category']=='3')
        {
        $chargeDeduction = createChargeForMiscellaneous($user_id,'weekly',$dbconn,$stripe_payment,$customer_id,$credit,$account_id);   
        }  
      break;
          case "annual":
          $interval_id = $interval['id'];
          $stripe_payment      = $interval['amount'];
        
          $interval_used_count = $interval['interval_used_count'];
        if($interval['payment_category']=='1') /*1 is for rent*/
        {
        $chargeDeduction = createChargeForRent($user_id,'weekly',$dbconn,$credit,$stripe_payment,$customer_id,$account_id);  
        } 
        else if($interval['payment_category']=='2')
        {
        $chargeDeduction = createChargeForUtility($user_id,'weekly',$dbconn,$credit,$stripe_payment,$customer_id,$account_id);
        }
        else if($interval['payment_category']=='3')
        {
        $chargeDeduction = createChargeForMiscellaneous($user_id,'weekly',$dbconn,$stripe_payment,$customer_id,$credit,$account_id);   
        }     
          break;
          case "monthly":
          $interval_id = $interval['id'];
          $stripe_payment  = $interval['amount'];
          $payment_date = $interval['payment_date'];
          $interval_used_count = $interval['interval_used_count'];
          $monthlyProcessDate =  date("d");
        
        if($monthlyProcessDate==$payment_date)
          {
           if($interval['payment_category']=='1') /*1 is for rent*/
        {
        $chargeDeduction = createChargeForRent($user_id,'weekly',$dbconn,$credit,$stripe_payment,$customer_id,$account_id);  
        } 
        else if($interval['payment_category']=='2')
        {
        $chargeDeduction = createChargeForUtility($user_id,'weekly',$dbconn,$credit,$stripe_payment,$customer_id,$account_id);
        }
        else if($interval['payment_category']=='3')
        {
        $chargeDeduction = createChargeForMiscellaneous($user_id,'weekly',$dbconn,$stripe_payment,$customer_id,$credit,$account_id);   
        } 

          }  
    
          break;   

      endswitch;

     }
      }



      function createChargeForRent($user_id,$chargeInterval,$dbconn,$credit,$stripe_payment,$customer_id,$account_id) /*$chargeInterval = weekly,monthly,bi-weekly,annually*/
      {

             $payment_deduction = deductPayment($customer_id,$stripe_payment,$account_id);


            $totalrentpaid =  getTotalRentPaid($user_id,$dbconn);
            /*get total rent amount already paid by tenant*/
            $gettotalrent  =  getNewRentAmountToBePaid($user_id,$dbconn);
     $totalrentpaid =  (isset($totalrentpaid )&& !empty($totalrentpaid))?$totalrentpaid : '0';
            $total_rent = $gettotalrent["total_rent"];
            $rent_to_be_paid = $total_rent - $totalrentpaid;


              if ($rent_to_be_paid > 0) {
                    $payment_method = 'reccuring';
                    $payment_module = 'tenant_rent';

                    if($credit <= $rent_to_be_paid){
                        $transaction_amount = $credit;
                    }else{
                        $transaction_amount = $rent_to_be_paid;
                    }
                    if(empty($totalrentpaid) || $totalrentpaid == 0){
                        $type = 'RENT_FIRSTTIME';
                    }else{
                        $type = 'RENT';
                    }

                    if(($rent_to_be_paid > 0) && ($rent_to_be_paid > $stripe_payment) ){
                        if(empty($credit) || $credit ==0 ) {
                            $transaction_amount = $stripe_payment;
                            $transaction_credit = 0;
                            $stripe_payment = 0;
                            $credit_left = 0;
                        }elseif($credit > 0){
                            $transaction_amount = $stripe_payment;
                            $remaining_rent_after_deducting_stripe = $rent_to_be_paid - $stripe_payment;
                            if($remaining_rent_after_deducting_stripe > 0 ){
                                if($credit < $remaining_rent_after_deducting_stripe){
                                    $transaction_credit = $credit;
                                    $credit_left = 0;
                                }else{
                                    $transaction_credit = $remaining_rent_after_deducting_stripe;
                                $credit_left = $credit - $remaining_rent_after_deducting_stripe;
                                }
                            }
                     
                        }
                    }else{
                        $transaction_amount = $rent_to_be_paid;
                        $stripe_payment = $stripe_payment - $rent_to_be_paid;
                        $transaction_credit = 0;
                        $credit_left = $credit +$stripe_payment;
                        
                        
                    
                    }
                    if($payment_deduction['status']=='success')
                    {
                    $addTransactionData = addTransactionsData($payment_deduction["charge"]["id"], $payment_deduction, $transaction_amount, $user_id, $payment_method, $type, $payment_module,$transaction_credit,$dbconn);
                  /*  print '<pre>';
                    print_r($addTransactionData);
                    exit;*/
                        if ($addTransactionData["code"] == 200) {
                        $left_amount = $credit - $transaction_amount;
                        if($left_amount < 0){
                        $left_amount = 0;
                        }else{
                        $left_amount;
                        $updateCredit = updateCredit($user_id,$credit_left,$dbconn);
                        }
                      }

                    }
                

                }
           }



 function deductPayment($customer_id,$total_amount_to_deduct,$account_id){
    

    \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
    try {
        $charge = \Stripe\Charge::create([
            "amount" => $total_amount_to_deduct * 100,
            "currency" => 'usd',
           "customer"=> $customer_id,
            "destination" => array("account"=>$account_id)
        ]);
        return array('code' => 200, 'status' => 'success','message' => 'Charge Deducted successfully' , 'charge' => $charge);
    } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage(),'line' => $e->getLine());
    }
}


    
    function addTransactionsData($charge_id,$charge,$amount,$user_id,$payment_mode,$type,$payment_module,$credit,$dbconn){
        
    try {
        $charge_id = $charge_id;
        $transaction_id =  generateTransactionId();
        $stripe_status  = $charge;
        $payment_response  = 'Payment has been done successfully.';
        $data3['transaction_id'] = $charge_id;
        $data3['charge_id'] = $transaction_id;
        $data3['user_id'] = $user_id;
        $data3['user_type'] = 'TENANT';
        $data3['amount'] = $amount;
        $data3['total_charge_amount'] = $amount;
        $data3['prorated_payment_amount'] = $amount;
        $data3['payment_mode'] = $payment_mode;
        $data3['type'] = "SECURITY";
        $data3['payment_module'] = $payment_module;
        $data3['payment_status'] = 'SUCCESS';
        $data3['type'] = $type;
       // $data3['stripe_status'] = serialize($stripe_status);
        $data3['credit'] = $credit;
        $data3['payment_response'] = $payment_response;
        $data3['auto_transaction_id'] = getAutoTransactionId($dbconn);
        $data3['created_at'] = date('Y-m-d H:i:s');
        $data3['updated_at'] = date('Y-m-d H:i:s');
        $sqlData = createSqlColVal($data3);
       /* print "<pre>";
        print_r($data3);
        exit;*/
      
        $query = "INSERT INTO transactions(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
        $stmt = $dbconn->prepare($query);
        $stmt->execute($data3);
        return array('code' => 200, 'status' => 'success', 'message' => "Security deposited succesfully" );
    }catch(Exception $e){
        print '<pre>';
        print_r($e);
        exit;
        return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(),'line'=>$e->getLine());
    }
}




      function createChargeForUtility($user_id,$chargeInterval,$dbconn,$credit,$stripe_payment,$customer_id,$account_id)
      {

                $payment_deduction = deductPayment($customer_id,$stripe_payment,$account_id);
            /*    print '<pre>';
                print_r($payment_deduction);
                exit;*/
                
            if(isset($remaining_amount) && !empty($remaining_amount)){
                $total_amount = $remaining_amount["credit_left"] + $remaining_amount["stripe_payment"] ; /* + $data["amount"]*/;
            }else{
                $total_amount =   $credit + $stripe_payment;
            }
            $getAllUtilties = getAllCharges($user_id,$dbconn);
         /*   print '<pre>';
            print_r( $getAllUtilties);
            exit;*/
            $sum_of_charges_amount = 0;
            if(!empty($getAllUtilties)){
                foreach($getAllUtilties as $key => $value){
                    if($value["amount"] <= $total_amount){
                        $sum_of_charges_amount = $sum_of_charges_amount + $value["amount"];
                        $total_amount = $total_amount - $value["amount"];
                        $change_charge_status =  UpdateTenantChargeStatus($value["id"],$value["amount"],$dbconn);
                      
                    }else{
                        break;
                    }
                }
                $payment_method = 'reccuring';
                $payment_module = 'tenant_charge';

                    if($stripe_payment >= $sum_of_charges_amount){
                        $transaction_amount = $sum_of_charges_amount;
                        $stripe_payment  = $stripe_payment - $transaction_amount;
                        $transaction_credit = 0;
                        $credit_left = $credit;
                    }else{
                        $remaining_charge_amount = $sum_of_charges_amount - $stripe_payment;
                        $transaction_amount = $stripe_payment;
                        $stripe_payment = 0;
                        if($remaining_charge_amount >=  $credit){
                            $transaction_credit = $credit;
                            $credit_left = 0;
                        }else{
                            $transaction_credit = $credit- $remaining_charge_amount;
                            $credit_left = $credit- $remaining_charge_amount;
                        }
                    }
        
                $type = 'CHARGES';
                if($payment_deduction['status']=='success')
                {
                $addTransactionData = addTransactionsData($payment_deduction["charge"]["id"], $payment_deduction, $transaction_amount, $user_id, $payment_method, $type, $payment_module,$transaction_credit,$dbconn);
             /*   print '<pre>';
                print_r($addTransactionData);
                exit;*/
                if ($addTransactionData["code"] == 200) {
                     $updateCredit = updateCredit($user_id,$credit_left,$dbconn);
                    return array('code' => 200, 'status' => 'success','transaction'=>$transaction_amount,'transaction_credit'=>$transaction_credit ,'stripe_payment'=>$stripe_payment ,'credit_left'=>$credit_left );
                       }  
                }
             
           
            }
            else
            {
            $payment_method = 'one_time';
            $payment_module = 'tenant_rent';
            $type = 'CREDIT';
            // $add_transaction = $this->addTransaction($amount,$total_amount_to_deduct,$user_id,$user_type,$payment_deduction['charge']);
            $add_credit = $this->addCredit($amount, $total_amount_to_deduct, $user_id, $user_type, $payment_deduction['charge'], 'not-selected',$dbconn);
            $addTransactionData = $this->addTransactionsData($payment_deduction["charge"]["id"], $payment_deduction, 0, $user_id, $payment_method, $type, $payment_module,$amount);
            return array('code' => 200, 'status' => 'success', 'message' => 'Amount Deducted successfully');
            } else {
            return array('code' => 400, 'status' => 'failed', 'message' => $payment_deduction["message"]);
               
            }
        }




     public function addCredit($amount,$total_amount_to_deduct,$user_id,$user_type,$charge,$category){
    try {
        $getpreviouscredit = $this->companyConnection->query("select overpay_underpay as credit from accounting_manage_charges  where user_id ='".$user_id."'")->fetch();
        $previouscredit = (isset($getpreviouscredit['credit']) && !empty($getpreviouscredit['credit']) ) ? $getpreviouscredit['credit'] : 0;
        $total_credit = $amount + $previouscredit;
        if($category == 'selected') {
            $query = "update accounting_manage_charges set overpay_underpay = $total_credit , stripe_payment = $amount  where user_id ='" . $user_id . "'";
        }else{
            $query = "update accounting_manage_charges set overpay_underpay = $total_credit where user_id ='" . $user_id . "'";
        }
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        return array('code' => 200, 'status' => 'success', 'message' => "Record added successfully", 'table'=>'accounting_manage_charges');
    }catch (Exception $e){
        return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
    }
}   




      function createChargeForMiscellaneous($user_id,$chargeInterval,$dbconn,$credit,$stripe_payment,$customer_id,$account_id)
      {
        $payment_deduction = deductPayment($customer_id,$stripe_payment,$account_id);
           try{
        $remaining_amount =  $this->paySecurity($payment_deduction,$stripe_payment,$user_id,$check_credit,$dbconn);
        if(!empty($remaining_amount)){
            $remaining_amount   =   $this->payCam($payment_deduction,$stripe_payment,$user_id,$remaining_amount,$dbconn);
        }else{
            $remaining_amount =      $this->payCam($payment_deduction,$stripe_payment,$user_id,$check_credit,$dbconn);
        }
        return $remaining_amount;
       }catch(Exception $e){
      }
     }




      function payCam($payment_deduction,$stripe_payment,$user_id,$remaining_amount,$check_credit,$dbconn){
    try {
        $getSecurityCharges = $this->getTotalSecurityChargesToPaid($user_id);
        $security_remaining = $getSecurityCharges["cam_remaining"];
        if(!empty($security_remaining)) {
            if (isset($remaining_amount) && !empty($remaining_amount)) {
                $total_amount = $remaining_amount["credit_left"] + $remaining_amount["stripe_payment"]; /* + $data["amount"]*/;
                if (($remaining_amount["stripe_payment"] >= $security_remaining)) {
                    $transaction_amount = $security_remaining;
                    $transaction_credit = 0 ;
                    $stripe_payment = $remaining_amount["stripe_payment"]-$security_remaining;
                    $credit_left = $remaining_amount["credit_left"];
                } else {
                    $transaction_amount = $remaining_amount["stripe_payment"];
                    $security_remaining = $security_remaining - $remaining_amount["stripe_payment"];
                    $stripe_payment = 0;
                    if($security_remaining >  $remaining_amount["credit"]){
                        $transaction_credit = $remaining_amount["credit"];
                        $credit_left = 0;
                    }else{
                        $transaction_credit = $security_remaining;
                        $credit_left = $remaining_amount["credit_left"]-$security_remaining;
                    }
                    $stripe_payment = $stripe_payment - $security_remaining;
                    $transaction_credit = 0;
                    $credit_left = $check_credit["credit_left"];
                }
            } else {
                $total_amount = $check_credit["credit"] + $stripe_payment;
                if (($security_remaining >= $stripe_payment)) {
                    $transaction_amount = $stripe_payment;
                    $security_remaining  = $security_remaining- $stripe_payment;
                    $stripe_payment = 0;
                    if($security_remaining >  $check_credit["credit"]){
                        $transaction_credit = $check_credit["credit"];
                        $credit_left = 0;
                    }else{
                        $transaction_credit = $security_remaining;
                        $credit_left = $check_credit["credit_left"]-$security_remaining;
                    }
                } else {
                    $transaction_amount = $security_remaining;
                    $stripe_payment = $stripe_payment - $security_remaining;
                    $transaction_credit = 0;
                    $credit_left = $check_credit["credit_left"];
                }
            }
            $payment_method = 'reccuring';
            $type = 'CAM';
            $payment_module = 'tenant_miscellaneous_charges';
            $addTransactionData = $this->addTransactionsData($payment_deduction["charge"]["id"], $payment_deduction, $transaction_amount, $data["user_id"], $payment_method, $type, $payment_module,$transaction_credit);
            if ($addTransactionData["code"] == 200) {
                return array('code' => 200, 'status' => 'success','transaction'=>$transaction_amount,'transaction_credit'=>$transaction_credit ,'stripe_payment'=>$stripe_payment ,'credit_left'=>$credit_left);
            }
        }else{
            if(!empty($remaining_amount)){
                return  $remaining_amount  ;
            }else{
                return  $check_credit;
            }
        }
    }catch(\Stripe\Error\RateLimit $e){
    }
}


function getTotalSecurityChargesToPaid($user_id){
    $get_cam_security_charges =  $this->companyConnection->query("SELECT * from  tenant_lease_details  WHERE user_id ='" . $user_id . "'")->fetch();
    $cam_amount = 0.00;
    $security_deposit = 0.00;
    if(!empty($get_cam_security_charges)){
        $cam_amount = (isset($get_cam_security_charges["cam_amount"]) && !empty($get_cam_security_charges["cam_amount"])) ? $get_cam_security_charges["cam_amount"]:0.00;
        $security_deposit = (isset($get_cam_security_charges["security_deposite"]) && !empty($get_cam_security_charges["security_deposite"])) ? $get_cam_security_charges["security_deposite"]:0.00;
    }
    $get_total_paid_cam =  $this->companyConnection->query("SELECT SUM(amount) as paid_cam_charges  from transactions  WHERE user_id ='" . $user_id . "' AND user_type ='TENANT' AND payment_status = 'SUCCESS' AND type IN ('CAM')")->fetch();
    $get_total_paid_security =  $this->companyConnection->query("SELECT SUM(amount) as paid_security_charges  from transactions  WHERE user_id ='" . $user_id . "' AND user_type ='TENANT' AND payment_status = 'SUCCESS' AND type IN ('SECURITY')")->fetch();
    if(!empty($get_total_paid_cam)){
        $cam_amount = 0.00;
    }
    if(!empty($get_total_paid_security)){
        $security_deposit == 0.00;
    }
    $total_due_cam_security_charges = str_replace( ',', '', $cam_amount) + str_replace( ',', '', $security_deposit);
    return array('code' => 200, 'status' => 'success', 'message' => "Total cam an security charge fetched successfully",'security_remaining' => str_replace( ',', '', $security_deposit),'cam_remaining' => str_replace( ',', '', $cam_amount));
    //return $total_due_cam_security_charges;
}




 function paySecurity($payment_deduction,$stripe_payment,$user_id,$remaining_amount,$check_credit,$dbconn){
    try {
        $getSecurityCharges = $this->getTotalSecurityChargesToPaid($user_id);
        $security_remaining = $getSecurityCharges["security_remaining"];
        if(!empty($security_remaining)) {
            if (isset($remaining_amount) && !empty($remaining_amount)) {
                $total_amount = $remaining_amount["credit_left"] + $remaining_amount["stripe_payment"]; /* + $data["amount"]*/;
                if (($remaining_amount["stripe_payment"] >= $security_remaining)) {
                    $transaction_amount = $security_remaining;
                    $transaction_credit = 0 ;
                    $stripe_payment = $remaining_amount["stripe_payment"]-$security_remaining;
                    $credit_left = $remaining_amount["credit_left"];
                } else {
                    $transaction_amount = $remaining_amount["stripe_payment"];
                    $security_remaining = $security_remaining - $remaining_amount["stripe_payment"];
                    $stripe_payment = 0;
                    if($security_remaining >  $remaining_amount["credit_left"]){
                        $transaction_credit = $remaining_amount["credit_left"];
                        $credit_left = 0;
                    }else{
                        $transaction_credit = $security_remaining;
                        $credit_left = $remaining_amount["credit_left"]-$security_remaining;
                    }
                }
            } else {
                $total_amount = $check_credit["credit"] + $stripe_payment;
                if (($security_remaining >= $stripe_payment)) {
                    $transaction_amount = $stripe_payment;
                    $security_remaining  = $security_remaining- $stripe_payment;
                    $stripe_payment = 0;
                    if($security_remaining >  $check_credit["credit"]){
                        $transaction_credit = $check_credit["credit"];
                        $credit_left = 0;
                    }else{
                        $transaction_credit = $security_remaining;
                        $credit_left = $check_credit["credit"]-$security_remaining;
                    }
                } else {
                    $transaction_amount = $security_remaining;
                    $stripe_payment = $stripe_payment - $security_remaining;
                    $transaction_credit = 0;
                    $credit_left = $check_credit["credit_left"];
                }
            }
            $payment_method = 'reccuring';
            $type = 'SECURITY';
            $payment_module = 'tenant_miscellaneous_charges';
            $addTransactionData = $this->addTransactionsData($payment_deduction["charge"]["id"], $payment_deduction, $transaction_amount, $data["user_id"], $payment_method, $type, $payment_module,$transaction_credit);
            if ($addTransactionData["code"] == 200) {
                return array('code' => 200, 'status' => 'success','transaction'=>$transaction_amount,'transaction_credit'=>$transaction_credit ,'stripe_payment'=>$stripe_payment ,'credit_left'=>$credit_left );
            }
        }else{
            if(!empty($remaining_amount)){
                return  $remaining_amount ;
            }else{
                return  $check_credit;
            }
        }
    }catch(\Stripe\Error\RateLimit $e){
    }
}






      function  getAllCharges($user_id,$dbconn){
    try{
        $getData = [];
        $getData =  $dbconn->query("SELECT tc.id,tc.created_at,tc.amount,tc.amount_due,tc.amount_paid,tc.amount_refunded,tc.start_date,tc.end_date,tc.waive_of_amount,tc.waive_of_comment,tc.status,ca.description,ca.charge_code,ca.priority FROM tenant_charges as tc inner join company_accounting_charge_code as ca on tc.charge_code = ca.id   where tc.user_id='$user_id' AND tc.status != 1 ")->fetchAll();
        if(!empty($getData)){
            return $getData;
        }else{
            return $getData;
        }
    }catch(Exception $e){
    }
}



function UpdateTenantChargeStatus ($id,$charge_amount,$dbconn){
    try {
        $chargeTableId = $id; /*id of charge table*/
        $getData =  $dbconn->query("SELECT * FROM tenant_charges where id='$chargeTableId'")->fetch();
       /* print '<pre>';
        print_r($getData);
        exit;*/
        $currentPayment = $charge_amount;
        $currentPayment =      str_replace(',', '', $currentPayment);
        $amt = $getData['amount'];
        $amt =      str_replace(',', '', $amt);
        $waveAmt = 0;
        $alreadyPaid = $getData['amount_paid'];
        if($alreadyPaid=='')
        {
            $alreadyPaid = 0;
        }
        $currentAmtPaid = $alreadyPaid + $currentPayment;
        $currentDueAmt = $amt - $currentPayment - $waveAmt;
        $data['amount_due'] = $currentDueAmt;
        $data['amount_paid'] = $currentAmtPaid;
        $data['waive_of_amount'] = $waveAmt;
        $data['status'] = '1';
     
        $sqlData = createSqlUpdateCase($data);
        $query = "UPDATE tenant_charges SET " . $sqlData['columnsValuesPair'] . " where id=" . $chargeTableId;
       
        $stmt = $dbconn->prepare($query);
        $stmt->execute($sqlData['data']);
        return array('code' => 200, 'status' => 'success', 'message' => "charge adjusted successfully");
    }catch(Exception $e){
        print '<pre>';
        print_r($e);
        exit;
        return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(),'line'=>$e->getLine());
    }
}
   
  
     


       function checkCredit($user_id,$dbconn){
       
    $getpreviouscredit = $dbconn->query("select overpay_underpay as credit from accounting_manage_charges  where user_id ='".$user_id."'")->fetch();
 $previouscredit = (isset($getpreviouscredit['credit']) && !empty($getpreviouscredit['credit']) ) ? $getpreviouscredit['credit'] : 0;
    $stripe_amount = (isset($getpreviouscredit['stripe_payment']) && !empty($getpreviouscredit['stripe_payment']) ) ? $getpreviouscredit['stripe_payment'] : 0;
    return array('code' => 200, 'status' => 'success','credit'=> $previouscredit , 'stripe_amount'=>$stripe_amount);
                 }



     function updateCredit($user_id,$credit,$dbconn)
       {
        $data['overpay_underpay'] = $credit;
        $sqlData = createSqlUpdateCase($data);
        $updateCredit = "UPDATE accounting_manage_charges SET ".$sqlData['columnsValuesPair']." where id=$user_id";
        $stmt = $dbconn->prepare($updateCredit);
        $stmt->execute($sqlData['data']);

       }          



function createSqlColVal($data) {
    $columns = '';
    $columnsValues = '';
    foreach ($data as $key => $value) {
        $columns .= $key . ',';
        $columnsValues .= ':' . "$key" . ',';
    }
    $columns = substr_replace($columns, "", -1);
    $columnsValues = substr_replace($columnsValues, "", -1);
    $sqlData = ['columns' => $columns, 'columnsValues' => $columnsValues];
    return $sqlData;
}

/**
 * function to update user status for current month
 */
function updateUserPaymentStatus($status,$dbConn,$id){
    $userstatus = [];
    $userstatus['payment_status'] = $status;
    $sqlData = createSqlUpdateCase($userstatus);
    $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id=$id";
    $stmt = $dbConn->prepare($query);
    $stmt->execute($sqlData['data']);
}

/**
 * Column value pair for update query
 * @param $data
 * @return array
 */
function createSqlUpdateCase($data) {
    $columnsValuesPair = '';
    $dataPair = [];
    foreach ($data as $key => $value) {
        $columnsValuesPair .= $key . "=? ,";

        if(empty($value) && $value != 0 ) $value = null;
        array_push($dataPair,$value);
    }
    $columnsValuesPair = substr_replace($columnsValuesPair, "", -1);
    $sqlData = ['columnsValuesPair' => $columnsValuesPair, 'data' => $dataPair];
    return $sqlData;
}

/**
 * @param $customer_id
 * @param $user_id
 * @param $paid_amount
 * @param $card_id
 * @return array
 */


function getTotalRentPaid($user_id,$dbconn){

    $get_total_paid_rent_data =  $dbconn->query("SELECT SUM(amount) as rent_amount ,SUM(credit) as credit,  (SUM(amount) + SUM(credit)) as paid_rent_amount  from transactions  WHERE user_id ='" . $user_id . "' AND user_type ='TENANT' AND payment_status = 'SUCCESS' AND type IN ('RENT','RENT_FIRSTTIME') ")->fetch();
 /*   print '<pre>';
    print_r($get_total_paid_rent_data);
    exit;*/
    if( isset($get_total_paid_rent_data["paid_rent_amount"]) && !empty($get_total_paid_rent_data["paid_rent_amount"])){
        $total_paid_rent = $get_total_paid_rent_data["paid_rent_amount"];
    }else{
        $total_paid_rent = 0;
    }
    return  $total_paid_rent;
    
    // dd($total_paid_rent);
}


function getNewRentAmountToBePaid($id,$dbconn)
{
    $tenant_id = $id;
    $total_amount_due = 0.00;
    $show_total_amount_due = 0.00;
    $getRentAmount =   $dbconn->query("SELECT * from tenant_lease_details  WHERE user_id ='" . $tenant_id . "'")->fetch();
    $rent_amount = $getRentAmount['rent_amount'];
    $lease_date = $getRentAmount['start_date'];
    $getDates =   $dbconn->query("SELECT * from tenant_move_in  WHERE user_id ='" . $tenant_id . "'")->fetch();
    $actualDate = $getDates['actual_move_in'];
    $get_total_charges_due = getTotalChargesDue($tenant_id,$dbconn);
    if($actualDate=='')
    {
        $month_differnce = getMonthDifference($lease_date);
        $total_rent = $month_differnce * $rent_amount;
        $total_amount_due=   getLeaseDateRentAmount($tenant_id,$total_rent,$dbconn);
        $show_total_amount_due = number_format($total_amount_due,2);
    }
    else if($actualDate<$lease_date)
    {
        // dd($actualDate);
        $month_differnce = getMonthDifference("2020-01-27");
        $total_rent = $month_differnce * $rent_amount;
        $total_amount_due = getActualDateRentAmount($tenant_id,$total_rent,$dbconn);
        $show_total_amount_due = number_format($total_amount_due,2);
    }
    else
    {
        $month_differnce = getMonthDifference($lease_date);
        $total_rent = $month_differnce * $rent_amount;
        $total_amount_due=   getLeaseDateRentAmount($tenant_id,$total_rent,$dbconn);
        $show_total_amount_due = number_format($total_amount_due,2);
    }
    /*Due to changes on this function i am removing prorated amount from here and tenant has to change full rent amount for one day payment*/
    return array('total_amoun_due'=>$total_amount_due,'show_total_amount_due'=>$show_total_amount_due,'total_rent'=>$total_rent,'total_charges_due'=>$get_total_charges_due);
}







  function updatedataAfterDeduction($interval_id,$payment_type,$interval_used_count,$process_date=null)
 {
    $newUsedCount = $interval_used_count + 1;
    if($payment_type=='weekly')
    {
    $newDate = date("Y-m-d", strtotime("+1 week"));
    $data['process_date'] = $newDate;
    $data['interval_used_count'] = $newUsedCount;
    }
    else if($payment_type=="bi-weekly")
    {
    $newDate =  date("Y-m-d", strtotime("+15 days"));
    $data['process_date'] = $newDate;
    $data['interval_used_count'] = $newUsedCount;
    }
    else if($payment_type=="annual")
    {
    $newDate   = date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " + 365 day"));
    $data['process_date'] = $newDate;
    $data['interval_used_count'] = $newUsedCount;
    }
    else if($payment_type=="monthly")
    {

     $data['interval_used_count'] = $newUsedCount;
    }


    $sqlData = createSqlUpdateCase($data);
    $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id=$interval_id";
    $stmt = $dbConn->prepare($query);
    $stmt->execute($sqlData['data']);


   




 }


 function  getLeaseDateRentAmount($tenant_id,$total_rent,$dbconn){
    $rent = getTotalRentPaid($tenant_id,$dbconn);
    $rent_to_be_paid = $total_rent-  $rent;
    $get_total_charges_due = getTotalChargesDue($tenant_id,$dbconn);
    $get_total_cam_cahrges = getTotalCamChargesDue($tenant_id,$dbconn);
    $total_rent_charges_cam_security_due = $rent_to_be_paid+$get_total_charges_due+$get_total_cam_cahrges;
    return $total_rent_charges_cam_security_due;
}



function  getActualDateRentAmount($tenant_id,$total_rent,$dbconn){
    $rent = getTotalRentPaid($tenant_id,$dbconn);
    $rent_to_be_paid = $total_rent-  $rent;
    $get_total_charges_due = getTotalChargesDue($tenant_id,$dbconn);
    $get_total_cam_cahrges = getTotalCamChargesDue($tenant_id,$dbconn);
    $total_rent_charges_cam_security_due = $rent_to_be_paid+$get_total_charges_due+$get_total_cam_cahrges;
    return $total_rent_charges_cam_security_due;
}




function getTotalChargesDue($user_id,$dbconn){
    // dd($user_id);
    $get_total_charges_due =  $dbconn->query("SELECT SUM(amount) as unpaid_charges  from tenant_charges  WHERE user_id ='" . $user_id . "' AND status ='0' ")->fetch();
    if(!empty($get_total_charges_due["unpaid_charges"])){
        $total_unpaid_charges = $get_total_charges_due["unpaid_charges"];
    }else{
        $total_unpaid_charges = 0;
    }
    return $total_unpaid_charges;
    // dd($total_paid_rent);
} 

function getTotalCamChargesDue($user_id,$dbconn){
    // dd($user_id);
    $get_cam_security_charges =   $dbconn->query("SELECT * from  tenant_lease_details  WHERE user_id ='" . $user_id . "'")->fetch();
    $cam_amount = 0.00;
    $security_deposit = 0.00;
    if(!empty($get_cam_security_charges)){
        $cam_amount = (isset($get_cam_security_charges["cam_amount"]) && !empty($get_cam_security_charges["cam_amount"])) ? $get_cam_security_charges["cam_amount"]:0.00;
        $security_deposit = (isset($get_cam_security_charges["security_deposite"]) && !empty($get_cam_security_charges["security_deposite"])) ? $get_cam_security_charges["security_deposite"]:0.00;
    }
    $get_total_paid_cam =  $dbconn->query("SELECT SUM(amount) as paid_cam_charges  from transactions  WHERE user_id ='" . $user_id . "' AND user_type ='TENANT' AND payment_status = 'SUCCESS' AND type IN ('CAM')")->fetch();
    $get_total_paid_security =  $dbconn->query("SELECT SUM(amount) as paid_security_charges  from transactions  WHERE user_id ='" . $user_id . "' AND user_type ='TENANT' AND payment_status = 'SUCCESS' AND type IN ('SECURITY')")->fetch();
    if(!empty($get_total_paid_cam)){
        $cam_amount = 0.00;
    }
    if(!empty($get_total_paid_security)){
        $security_deposit == 0.00;
    }
    $total_due_cam_security_charges = str_replace( ',', '', $cam_amount) + str_replace( ',', '', $security_deposit);
    return $total_due_cam_security_charges;
    // dd($total_paid_rent);
}


function getMonthDifference($date){
    $today = date("Y-m-d");
    $date1=date_create($today);
    $date2=date_create($date);
    $diff=date_diff($date1,$date2);
    $month_differnce = $diff->m;
    return  $month_differnce;
} 


function getAutoTransactionId($connection){
    $data = $connection->query("SELECT * FROM transactions where id = (select MAX(id) from transactions)")->fetch();
//    dd($data['auto_transaction_id']);
    if (!empty($data)){
        return $data['auto_transaction_id']+1;
    } else {
        return 100000;
    }
}


     function getBankId($property_id)
    {
      $getData =  $this->companyConnection->query("SELECT * FROM property_bank_details WHERE property_id ='" . $property_id . "' and is_default='1'")->fetch();
      if(empty($getData))
      {
        return null;
      }
      else
      {
       return $bank_id = $getData['bank_id'];
      }
       
    }


     function getPropertyId($user_id)
    {
        
         $getData =  $this->companyConnection->query("SELECT property_id FROM tenant_property WHERE user_id ='" . $user_id . "'")->fetch();
         return $property_id = $getData['property_id'];
    }





  //  $checkTenantCron = checkTenantCron($tenant_id);
     






