<?php

require_once __DIR__.'/conn.php';

function changeStatus($conn){
    try{

        $status = 0;
        $date = date('Y-m-d');
//        $time = date('H:i:s');

        $dbDetails = $conn->query("SELECT * FROM users WHERE user_type='1' and free_plan='1' and expiration_date <= '$date'")->fetchAll();

        $sql = "UPDATE users SET status=?  WHERE user_type='1' and expiration_date <= '$date'";
        $stmt= $conn->prepare($sql);
        $stmt->execute([$status]);
        exit;

    }catch (Exception $exception)
    {
        return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        printErrorLog($exception->getMessage());
    }
}

echo changeStatus($conn);

