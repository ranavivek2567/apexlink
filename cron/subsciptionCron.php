<?php
/**
 * Created by PhpStorm.
 * User: Vivek Rana
 * Date: 22-May-19
 * Time: 11:44 AM
 */

require_once __DIR__.'/conn.php';
require_once __DIR__.'/../helper/globalHelper.php';
charge_subscription();
function charge_subscription(){
    $connection = new DBConnection();
    try {
        //fetching all companies
        $query = "SELECT * FROM users WHERE status='1' AND free_plan='0' AND payment_status='1' AND stripe_account_id IS NOT NULL";
        $companyUserData = $connection->conn->query($query)->fetchAll();
//        print_r($companyUserData);die;
//        echo '<pre>';
//        print_r($companyUser);die;
//        echo '</pre>';
        //checking company data
        if(!empty($companyUserData)){
            foreach ($companyUserData as $key=>$value){
                $destination_stripe = $value['stripe_account_id'];
                $dbConnection = DBConnection::dynamicDbConnection($value['host'],$value['database_name'],$value['db_username'],$value['db_password']);
                $companyQuery = "SELECT * FROM users WHERE id='1' AND stripe_customer_id IS NOT NULL";
                $companyUser = $dbConnection->query($companyQuery)->fetch();
               // print_r($companyUser);die;

                if(!empty($companyUser)){
                    $customer_id = [];
                    $customer_id['customer_id'] = $companyUser['stripe_customer_id'];
                    $customerData = getCustomer($customer_id);

                    $amount_array = paymentCalculateForPM($dbConnection,$customerData,$companyUser['id']);

                    if($customerData['code'] == 200 && $amount_array['code'] == 200){
                        $customerSource = $customerData['customer_data']->default_source;

                        if(!empty($customerSource)){

                            $chargeAmount =(int)(round($amount_array['charge'], 2)*100);
                            $transactionCount =  $dbConnection->query("SELECT COUNT(id) AS NumberOfRecords FROM transactions")->fetch();

                            if($amount_array['term_plan'] == 'YEARLY') {
                                $checkSubscriptionType = $dbConnection->query("SELECT * FROM transactions WHERE term_plan = 'YEARLY' AND user_type = 'PM' AND payment_status = 'SUCCESS' AND CURRENT_DATE() >= DATE_ADD(DATE(created_at), INTERVAL +12 MONTH) ORDER BY id DESC")->fetch();
                            }  else {
                                $checkSubscriptionType = 'MONTHLY';
                            }
                            if(!empty($checkSubscriptionType) || $transactionCount['NumberOfRecords'] ==0){
                                $chargeData = [];
                                $chargeData['amount'] = $chargeAmount;
                                $chargeData['currency'] = 'usd';
                                $chargeData['token'] = $customerSource;
                                $chargeData['customer_id'] = $companyUser['stripe_customer_id'];
                                if($amount_array['term_plan'] == 'YEARLY'){
                                    $chargeData['company_info'] = $value['company_name'].' ('.$value['id'].') : Subscription payment for the year of '.date("Y");
                                } else {
                                    $chargeData['company_info'] = $value['company_name'].' ('.$value['id'].') : Subscription payment for the month of '.date("F");
                                }

                                $stripe_payment = createSuperAdminCharge($chargeData);

                                if($stripe_payment['code'] == 200) {
                                    $charge_object = $stripe_payment['charge_data'];
                                    $transactionData = [];
                                    if ($charge_object->status == 'succeeded') {
                                        print_r('Charge deducted successfully');
                                        $transactionData['charge_id'] = $charge_object->id;
                                        $transactionData['stripe_status'] = $stripe_payment['charge_data'];
                                        $transactionData['payment_status'] = 'SUCCESS';
                                        $transactionData['payment_response'] = 'Transaction Completed.';
                                    } else if($charge_object->status == 'pending') {
                                        print_r('Charge deducted successfully');
                                        $transactionData['charge_id'] = $charge_object->id;
                                        $transactionData['stripe_status'] = $stripe_payment['charge_data'];
                                        $transactionData['payment_status'] = 'PENDING';
                                        $transactionData['payment_response'] = 'Transaction Pending.';
                                    } else {
                                        $transactionData['payment_status'] = 'ERROR';
                                        $transactionData['payment_response'] = 'Transaction Failed.';
                                    }
                                    $transactionData['transaction_id'] = generateTransactionId();
                                    $transactionData['user_id'] = $companyUser['id'];
                                    $transactionData['user_type'] = 'PM';
                                    $transactionData['amount'] = $amount_array['amount'];
                                    $transactionData['plan_id'] = $amount_array['subscription_plan'];
                                    $transactionData['apex_link_service_fee'] = $amount_array['apexlinkServiceFee'];
                                    $transactionData['pay_plan_price'] = $amount_array['pay_plan_price'];
                                    $transactionData['apex_link_admin_fee'] = $amount_array['apexlinkAdminFee'];
                                    $transactionData['stripe_account_fee'] = $amount_array['stripeAccountFee'];
                                    $transactionData['stripe_transaction_fee'] = $amount_array['stripeTransactionFee'];
                                    $transactionData['payment_type'] = $amount_array['transaction_type'];
                                    $transactionData['term_plan'] = $amount_array['term_plan'];
                                    $transactionData['total_charge_amount'] = $amount_array['charge'];
                                    $transactionData['created_at'] = date('Y-m-d');
                                    $transactionData['updated_at'] = date('Y-m-d');

                                    //Saving Data in Super admin transaction table
                                    $superAdminData = $transactionData;
                                    $superAdminData['user_id'] = $value['id'];
                                    $superAdminData['payment_type'] = 'CRON';
                                    $sqlDataSuperAdmin =  createSqlColVal($superAdminData);
                                    $querySuperAdmin = "INSERT INTO transactions (" . $sqlDataSuperAdmin['columns'] . ") VALUES (" . $sqlDataSuperAdmin['columnsValues'] . ")";
                                    $stmtSuperAdmin = $connection->conn->prepare($querySuperAdmin);
                                    $stmtSuperAdmin->execute($superAdminData);

                                    //Saving Data in company transaction table
                                    $sqlData = createSqlColVal($transactionData);
                                    $query = "INSERT INTO transactions (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                    $stmt = $dbConnection->prepare($query);
                                    $stmt->execute($transactionData);

                                }
                            }
                        }
                    }

                }

            }
        }
    } catch(Exception $exception){
        echo '<pre>';
        print_r($exception);die;
        return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
    }
}

// function paymentCalculateForPM($dbConnection,$customerData){
//     try {

//         //static payments variables for monthly dedcution
//         $apexlinkServiceFee=75.00;
// //        $stripeTransactionFee=0.25;


//         $apexlinkAdminFee=0.20;
//         $stripeAccountFee=2.00;

//         //static payments variables for yearly dedcution
//         $apexlinkServiceFeeYearly= 75.00 * 12;
// //        $stripeTransactionFeeYearly=2.94;


//         $getPlanD = $dbConnection->query("SELECT subscription_plan,term_plan,plan_price,pay_plan_price FROM plans_history WHERE user_id='1' AND status='1' ORDER BY id DESC")->fetch();
// //        echo '<pre>';
// ////        print_r($getPlanD);die;
// ////        echo '</pre>';
//         $GetSourceDetail = GetSourceDetail($customerData);

//         if(!empty($getPlanD)){
//             $pay_plan_price = !empty($getPlanD['pay_plan_price'])?$getPlanD['pay_plan_price']:0;
//             $feeForACH= $getPlanD['pay_plan_price'] * 0.25/100;
//             $feeForCardYearly=$getPlanD['pay_plan_price'] * 17.63/100;
//             $feeForCard=$getPlanD['pay_plan_price'] * 1.50/100;
//             if($getPlanD['term_plan'] == '1'){
//                 $stripeTransactionFee=$getPlanD['pay_plan_price'] * 0.25/100;
//                 $amount = '';
//                 $transaction_type = '';
//                 if($GetSourceDetail == 'no'){
//                     $totalsubscriptionCharges=$apexlinkServiceFee + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForACH;
//                     $amount = $feeForACH;
//                     $transaction_type = 'ACH';
//                 }else if($GetSourceDetail == 'yes'){
//                     $totalsubscriptionCharges=$apexlinkServiceFee + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForCard;
//                     $amount = $feeForACH;
//                     $transaction_type = 'CARD';
//                 }

//                 return array('code' => 200, 'status' => 'success','charge'=>$totalsubscriptionCharges,'term_plan'=>'MONTHLY','apexlinkServiceFee'=>$apexlinkServiceFee,'pay_plan_price'=>$pay_plan_price,'apexlinkAdminFee'=>$apexlinkAdminFee,'stripeAccountFee'=>$stripeAccountFee,'stripeTransactionFee'=>$stripeTransactionFee,'amount'=>$amount,'transaction_type'=>$transaction_type,'subscription_plan'=>$getPlanD['subscription_plan']);
//             }
//             else if($getPlanD['term_plan'] == '4'){
//                 $stripeTransactionFee=$getPlanD['pay_plan_price'] * 2.94 /100;
//                 $pay_plan_amount = $pay_plan_price;
//                 $pay_plan_price=$pay_plan_price * 11;
//                 $amount = '';
//                 $transaction_type = '';

//                 if($GetSourceDetail == 'no'){
//                     $totalsubscriptionCharges=$apexlinkServiceFeeYearly + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForACH;
//                     $amount = $feeForACH;
//                     $transaction_type = 'ACH';
//                 }else if($GetSourceDetail == 'yes'){
//                     $totalsubscriptionCharges=$apexlinkServiceFeeYearly + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForCardYearly;

//                     $amount = $feeForCardYearly;

//                     $transaction_type = 'CARD';
//                 }
//                 return array('code' => 200, 'status' => 'success','charge'=>$totalsubscriptionCharges,'term_plan'=>'YEARLY','apexlinkServiceFee'=>$apexlinkServiceFeeYearly,'pay_plan_price'=>$pay_plan_amount,'apexlinkAdminFee'=>$apexlinkAdminFee,'stripeAccountFee'=>$stripeAccountFee,'stripeTransactionFee'=>$stripeTransactionFee,'amount'=>$amount,'transaction_type'=>$transaction_type,'subscription_plan'=>$getPlanD['subscription_plan']);
//             }

//             else{
//                 return array('code' => 500, 'status' => 'error');
//             }

//         }


//     } catch (Exception $exception) {

//         return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
//         printErrorLog($e->getMessage());
//     }

// }

function paymentCalculateForPM($dbConnection,$customerData,$id){
    try {
        //static payments variables for monthly dedcution
        $apexlinkServiceFee=75.00;
        $apexlinkAdminFee=0.20;
        $stripeAccountFee=2.00;
        //static payments variables for yearly dedcution
        $apexlinkServiceFeeYearly= 75.00 * 12;


        $getPlanD=  $dbConnection->query("SELECT subscription_plan,term_plan,plan_price,pay_plan_price FROM plans_history WHERE user_id=1 and status='1'")->fetch();

        $GetSourceDetail=GetSourceDetail($customerData);
        if(!empty($getPlanD)){
            $pay_plan_price=$getPlanD['pay_plan_price'];

            if($getPlanD['term_plan'] == '1'){
                $stripeTransactionFee=$getPlanD['pay_plan_price'] * 0.25/100;
                $feeForACH= $getPlanD['pay_plan_price'] * 0.25/100;
                $feeForCard= $getPlanD['pay_plan_price']  * 1.5/100;
                $amount = $pay_plan_price;
                $transaction_type = '';

                if($GetSourceDetail == 'no'){
                    $totalsubscriptionCharges=$apexlinkServiceFee + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForACH;
                    $transaction_type = 'ACH';
                }else if($GetSourceDetail == 'yes'){
                    $totalsubscriptionCharges=$apexlinkServiceFee + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForCard;
                    $transaction_type = 'CARD';
                }else{
                    $totalsubscriptionCharges =$apexlinkServiceFee + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForCard;
                    $transaction_type = '';
                }
                

                return array('code' => 200, 'status' => 'success','charge'=>$totalsubscriptionCharges,'term_plan'=>'MONTHLY','apexlinkServiceFee'=>$apexlinkServiceFee,'pay_plan_price'=>$pay_plan_price,'apexlinkAdminFee'=>$apexlinkAdminFee,'stripeAccountFee'=>$stripeAccountFee,'stripeTransactionFee'=>$stripeTransactionFee,'amount'=>$amount,'transaction_type'=>$transaction_type,'subscription_plan'=>$getPlanD['subscription_plan']);
            }
            else if($getPlanD['term_plan'] == '4'){
                $stripeTransactionFee=$getPlanD['pay_plan_price'] * 2.94/100;
                $feeForACH= $getPlanD['pay_plan_price'] * 0.25/100;
                $feeForCard= $getPlanD['pay_plan_price']  * 17.63/100;
                $pay_plan_price=(double)$pay_plan_price * 11;
                $amount = $pay_plan_price;
                $transaction_type = '';
                if($GetSourceDetail == 'no'){
                    $totalsubscriptionCharges=$apexlinkServiceFeeYearly + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForACH;
                    $transaction_type = 'ACH';
                }else if($GetSourceDetail == 'yes'){
                    $totalsubscriptionCharges=$apexlinkServiceFeeYearly + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForCard;
                    $transaction_type = 'CARD';
                }else{
                    $totalsubscriptionCharges = $apexlinkServiceFeeYearly + $pay_plan_price + $apexlinkAdminFee + $stripeAccountFee + $stripeTransactionFee + $feeForCard;
                    $transaction_type = NULL;
                }

                return array('code' => 200, 'status' => 'success','charge'=>$totalsubscriptionCharges,'term_plan'=>'YEARLY','apexlinkServiceFee'=>$apexlinkServiceFeeYearly,'pay_plan_price'=>$pay_plan_price,'apexlinkAdminFee'=>$apexlinkAdminFee,'stripeAccountFee'=>$stripeAccountFee,'stripeTransactionFee'=>$stripeTransactionFee,'amount'=>$amount,'transaction_type'=>$transaction_type,'subscription_plan'=>$getPlanD['subscription_plan']);
            }

            else{
                return array('code' => 500, 'status' => 'error','message'=>'Something Went Wrong');
            }

        }


    } catch (Exception $exception) {
        return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        printErrorLog($e->getMessage());
    }

}


/**
 * Server side validation function
 * @author Deepak
 * @param $data
 * @return array
 */
function createSqlColVal($data) {
    $columns = '';
    $columnsValues = '';
    foreach ($data as $key => $value) {
        $columns .= $key . ',';
        $columnsValues .= ':' . "$key" . ',';
    }
    $columns = substr_replace($columns, "", -1);
    $columnsValues = substr_replace($columnsValues, "", -1);
    $sqlData = ['columns' => $columns, 'columnsValues' => $columnsValues];
    return $sqlData;
}

function GetSourceDetail($getDetailUser){
    $sources = $getDetailUser['customer_data']['default_source'];
    if(isset($sources) && !empty($sources)) {
        if (strpos($sources, 'card') !== false) {
            $card = 'yes';
        } else {
            $card = 'no';
        }
    }else{
        $card ="";
    }
    return $card;
}