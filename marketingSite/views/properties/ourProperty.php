<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/marketingSite/views/layouts/website_header.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/marketingSite/views/layouts/website_top_navigation.php");
?>
<section class="real-state-main-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="real-estate-propty-srch">
                    <div class="row">
                        <div class="col-md-3 col-sm-4">
                            <div class="real-estate-proprty-span-outer">
                                <label class="control-label">Filter By:</label>
                                <select id="ddlOurPropertyStateId" class="form-control">
                                    <option value=""> Select </option>
                                    <option value=""></option>
                                    <option value="AL">AL</option>
                                    <option value="CA">CA</option>
                                    <option value="CO">CO</option>
                                    <option value="Dubai">Dubai</option>
                                    <option value="FL">FL</option>
                                    <option value="KY">KY</option>
                                    <option value="Minnesota">Minnesota</option>
                                    <option value="MN">MN</option>
                                    <option value="MS">MS</option>
                                    <option value="NY">NY</option>
                                    <option value="OH">OH</option>
                                    <option value="PK">PK</option>
                                    <option value="TX">TX</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4">
                            <div class="real-estate-proprty-span-outer">
                                <label class="control-label">Sort By:</label>
                                <div class="checkbox">
                                    <label>Vacant Properties
                                        <input type="checkbox" value="">
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="real-estate-feature-sct">
                    <h2 class="heading-small">Property List</h2>
                    <div class="propty1-inner">

                    </div>
                    <div id="pagination">

                    </div>
                </div>

            </div>
        </div>

    </div>
</section>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/marketingSite/views/layouts/website_footer.php");
?>
<script src="<?php echo WEBSITE_URL; ?>/js/properties/properties.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.twbsPagination.min.js" type="text/javascript"></script>
<script>
</script>

<div class="container">
    <div class="modal fade" id="applyonlineForm" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="padding: 0">
                    <div class="form-hdr">

                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="hdrtxt">Apply Online</h4>

                    </div>
                </div>

                <div class="modal-body">
                    <form id="apply_online_id">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="txtFirstNameApply">
                                        First Name<span style="color: Red;"> *</span>
                                    </label>
                                    <input name="first_name" id="txtFirstNameApply" type="text" class="form-control validate[required] clsCapitaliseChr clsPreviewInput" maxlength="50" spellcheck="true">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="txtLastNameApply">
                                        Last Name<font style="color: Red;"> *</font>
                                    </label>
                                    <input name="last_name" id="txtLastNameApply" type="text" class="form-control validate[required] clsCapitaliseChr clsPreviewInput" maxlength="50" spellcheck="true">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>
                                        Email<font style="color: Red;"> *</font>
                                    </label>
                                    <input name="email" id="txtEmailApplyOnline" type="text" maxlength="50" class="form-control validate[required] validate[custom[email]] clsPreviewInput clsCapitaliseChr" spellcheck="true">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="">
                                        Carrier<font style="color: Red;"> *</font>
                                    </label>
                                    <select name="carrier" class="form-control additional_carrier customCarriervalidations"
                                            data_required="true"><option>Select</option></select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>
                                        Country Code<font style="color: Red;"> *</font>
                                    </label>
                                    <select name="country" class="form-control additional_country"
                                            data_required="true"><option>Select</option></select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="phone_number_id">
                                        Phone Number<font style="color: Red;"> *</font>
                                    </label>
                                    <input name="phone_number" id="phone_number_id" type="text" maxlength="10" class="form-control validate[required] add-input PreviewClsRenterPhone validate[custom[number]] clsPreviewInput clsCapitaliseChr" spellcheck="true">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="txtSSNSNINDApplyOnline">
                                        <em class="ss-spacing">SS</em>N/SIN/ID<font style="color: Red;"> *</font>
                                    </label>
                                    <input id="txtSSNSNINDApplyOnline" name="ssn_sin_id" type="text" class="form-control validate[required] AddSSNSINIDValPreview clsPreviewInput clsCapitaliseChr" maxlength="50" spellcheck="true">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="ddlRentalApplicantGenderApplyOnline">
                                        Gender <font style="color: Red;"> *</font>
                                    </label>

                                    <select id="ddlRentalApplicantGenderApplyOnline" class="form-control validate[required]" name="gender">
                                        <option value="">Select</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        <option value="Prefer Not To Say">Prefer Not To Say</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6" id="otherGenderDivApplyOnlne" style="display: none">
                                <div class="form-group" >
                                    <label for="txtOtherGenderApplyOnline">
                                        Other Gender <font class="red spnredstarPreview"> *</font>
                                    </label>
                                    <input type="text" id="txtOtherGenderApplyOnline" class="form-control validate[required] clsCapitaliseChr" spellcheck="true">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label id="ddlApplyProperty">
                                        Select Property<font style="color: Red;"> *</font>
                                    </label>
                                    <input id="ddlApplyProperty" name="prop_id" type="text" class="form-control validate[required] AddSSNSINIDValPreview clsPreviewInput clsCapitaliseChr ddlApplyPropertyClass" maxlength="50" spellcheck="true" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="ddlApplyBuilding">
                                        Select Building<font style="color: Red;"> *</font>
                                    </label>
                                    <select name="build_id" id="ddlApplyBuilding" class="form-control validate[required] ddlApplyBuildingClass"><option value="">Select</option></select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label id="ddlApplyUnit">
                                        Select Unit<font style="color: Red;"> *</font>
                                    </label>
                                    <select id="ddlApplyUnit" class="form-control validate[required]" name="unit_id">
                                        <option value="">Select Unit</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="move-in-date">
                                        Expected Move-In Date<font style="color: Red;"> *</font>
                                    </label>
                                    <input name="exp_move_in" type="text" id="move-in-date" class="form-control datepick validate[required] clsCapitaliseChr hasDatepicker" readonly="readonly" spellcheck="true">
                                    <input type="hidden" id="propertyIdHidden" spellcheck="true">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="ddlRequstedLeaseTermApplyonline">
                                        Requested Lease Term<font style="color: Red;"> *</font>
                                    </label>
                                    <input name="lease_term" onpaste="return false;" ondrop="return false;" id="ddlRequstedLeaseTermApplyonline" type="text" class="form-control validate[required] validate[custom[number]] clsCapitaliseChr" spellcheck="true">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="ddlRequstedLeaseYearMonth">
                                        &nbsp;
                                    </label>
                                    <select class="form-control" name="lease_tenure" id="ddlRequstedLeaseYearMonth" style="margin: 2px 0 15px;">
                                        <option value="1">Months</option>
                                        <option value="2">Year</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="btn-outer text-center">
                                        <input id="btnApplyOnlineRentalSave" type="submit" value="Submit" spellcheck="true" style="float: none">
                                        <input id="btnApplyCancel" type="button" value="Cancel" spellcheck="true"  style="float: none">

                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>