<?php
/**
 * Created by PhpStorm.
 * User: ptripathi
 * Date: 1/21/2019
 * Time: 6:00 PM
 */
include_once(ROOT_URL.'/config.php');
include_once (ROOT_URL.'/constants.php');
//include_once( ROOT_URL."/company/helper/helper.php");
//include_once( "$_SERVER[DOCUMENT_ROOT]/company/helper/ddl.php");

if (basename($_SERVER['PHP_SELF']) == basename(__FILE__))
{
//    $url = BASE_URL."login";
//    header('Location: '.$url);
}

?>
<header class="real-estate-header">
    <div class="container">
        <div class="row">
            <div class="col-md-8 pull-right">
                <nav class="navbar">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="myNavbar">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="/">Home</a></li>
                                <li><a href="/propertylisting/ourproperty">Our Properties</a></li>
                                <li><a href="/propertylisting/Campaign">Campaigns</a></li>
                                <li><a href="/home/ContactUs">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- MAIN Navigation Starts -->

<!-- MAIN Navigation Ends -->



