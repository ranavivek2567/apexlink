<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Apexlink</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo WEBSITE_URL;?>/css/bootstrap-glyphicons.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="<?php echo WEBSITE_URL;?>/css/bootstrap.min.css">
    <link href="<?php echo COMPANY_SITE_URL;?>/css/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo WEBSITE_URL;?>/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="<?php echo WEBSITE_URL;?>/css/jquery.timepicker.min.css">
    <link rel="stylesheet" href="<?php echo WEBSITE_URL;?>/css/jquery-ui.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo WEBSITE_URL; ?>/css/jquery.multiselect.css"/>
    <link rel="stylesheet" href="<?php echo WEBSITE_URL; ?>/css/bootstrap-select.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="<?php echo WEBSITE_URL;?>/css/main.css"/>
    <link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/ui.jqgrid.min.css">

    <link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
    <!-- include css files-->

    <!-- include libraries(jQuery, bootstrap) -->


    <script src="<?php echo WEBSITE_URL; ?>/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo WEBSITE_URL; ?>/js/jquery.validate.min.js"></script>
    <script src="<?php echo WEBSITE_URL; ?>/js/jquery-ui.js"></script>
    <script src="<?php echo WEBSITE_URL; ?>/js/jquery.mask.min.js"></script>
    <script src="<?php echo WEBSITE_URL; ?>/js/bootbox.js"></script>
    <script src="<?php echo WEBSITE_URL; ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo WEBSITE_URL; ?>/js/jquery.timepicker.min.js"></script>
    <script src="<?php echo WEBSITE_URL; ?>/js/jquery.datetimepicker.js"></script>
    <script src="<?php echo WEBSITE_URL; ?>/js/bootstrap-select.min.js"></script>
    <script src="<?php echo WEBSITE_URL; ?>/js/jquery.responsivetabs.js"></script>
    <script src="<?php echo WEBSITE_URL; ?>/js/jquery.timepicker.min.js"></script>
    <script src="<?php echo WEBSITE_URL; ?>/js/jquery.validate.min.js"></script>
    <script src="<?php echo WEBSITE_URL; ?>/js/toastr.min.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.jqgrid.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/core-js/2.1.4/core.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />
    <script defer>

        // Toast Messages
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": true,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "400",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };



        jQuery.extend(jQuery.validator.messages, {
            required: "* This field is required.",
        });
    </script>
</head>

<body>

