$(document).ready(function () {

    fetchAllProperty('0');

    $("#apply_online_id").validate({
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            email :{
                required: true
            },
            carrier :{
                required: true
            },
            country :{
                required: true
            },
            phone_number :{
                required: true
            },
            ssn_sin_id :{
                required: true
            },
            gender :{
                required: true
            },
            prop_id :{
                required: true
            },
            build_id :{
                required: true
            },
            unit_id :{
                required: true
            },
            exp_move_in :{
                required: true
            },
            lease_term:{
                required: true,
                number: true
            }
        }
    });

})
function fetchAllProperty(page) {
    $.ajax({
        type: 'post',
        url: '/properties-Ajax',
        data: {
            class: 'properties',
            action: 'fetchProperty',
            pagination: page
        },
        success: function (response) {
            var res = JSON.parse(response);
            getPropertyHtml(res.data);
            totalpages = res.total_pages;
            if(page == '0') {
                $('#pagination').show();
                $('#pagination')
                    .empty()
                    .removeData("twbs-pagination")
                    .unbind("page");
                paginationGrid(totalpages);
            }
        },
    });

}
function getPropertyHtml(data){
    var html='';
    $.each(data,function(key,value){
        var fulladdress=value.address1+''+value.address2+''+value.address3+''+value.address4+','+value.city+','+value.zipcode;
        var property_squareFootage=value.property_squareFootage == ''? '0.00' : value.property_squareFootage;
        var property_price1=value.property_price == '' ? '0.00' : value.property_price;

        var upload_url = window.location.origin;
        var src =upload_url +"/company/images/li-bg.png";
        var src1 =upload_url +"/company/images/li-bg.png";
        var applyButton=value.online_application == null ? '' : '<a rel="'+value.id+'" class="btn blue-btn applyOnlineClass">Apply Online</a>';
        html +='<div class="row">\n' +
            '<div class="col-sm-8">\n' +
            '<div class="real-estate-property-newtop real-estate-property-detailstop">\n' +
            '<div class="row">\n' +
            '<div class="col-md-6">\n' +
            '<p>'+value.property_name+'</p>\n' +
            '<p>'+value.property_name+''+fulladdress+'</p>\n' +
            '<a href="/PropertyListing/PropertyDetails?id='+value.id+'" class="btn blue-btn">More Details</a>\n' +
            applyButton+
            '</div>\n' +
            '<div class="col-md-6">\n' +
            '<p><img src="'+src+'">' + property_price1 + '/month</p>\n' +
            '<p><img src="'+src1+'">' + property_squareFootage + 'Sq. Ft</p>\n' +
            '</div>\n' +
            '</div> \n' +
            '</div>\n' +
            '</div>\n' +
            '<div class="col-sm-4">\n' +
            '<div id="myCarousel_'+value.id+'" class="carousel slide" data-ride="carousel">'+
            '<div class="carousel-inner">';
                if (typeof value.property_images_data !== 'undefined' && value.property_images_data.length > 0) {
                    $.each(value.property_images_data, function (key1, value1) {
                        var path = upload_url + '/company/' + value1.file_location;
                        var active = (key1 == 0) ? 'active' : '';

                        html += '<div class="item '+active+'">\n' +
                            '<img src="'+path+'" alt="Slide1">\n' +
                            '</div>\n';
                    });
                } else {
                    html += '<div class="item active">\n' +
                        '<img src="/company/images/img-nt-available.png" alt="Slide1">\n' +
                        '</div>\n';
                }
           html +=  '<a class="left carousel-control" href="#myCarousel_'+value.id+'" data-slide="prev">' +
                    '<i class="fa fa-angle-left" aria-hidden="true"></i>' +
                    '</a>' +
                    '<a class="right carousel-control" href="#myCarousel_'+value.id+'" data-slide="next">' +
                    '<i class="fa fa-angle-right" aria-hidden="true"></i>' +
                    '</a></div>'+
                    '</div>'+
                    '</div>\n' +
                    '</div>';
    });
    $('.propty1-inner').html(html);
}
function paginationGrid(totalpages){
    window.pagObj = $('#pagination').twbsPagination({
        totalPages: totalpages,
        visiblePages: 3,
        onPageClick: function (event, page) {
            //
        }
    }).on('page', function (event, page) {
        fetchAllProperty(page);
    });
}
$(document).on('change','#ddlOurPropertyStateId',function () {
    var val1=$(this).val();
    searchByState(val1,0);
});
function searchByState(val,page){

        $.ajax({
            type: 'post',
            url: '/properties-Ajax',
            data: {
                class: 'properties',
                action: 'searchByStateAjax',
                val: val,
                pagination: page
            },
            success: function (response) {
                var res = JSON.parse(response);
                getPropertyHtml(res.data);
                totalpages = res.total_pages;
                if (page == '0') {
                    $('#pagination').show();
                    $('#pagination')
                        .empty()
                        .removeData("twbs-pagination")
                        .unbind("page");
                    paginationGrid(totalpages);
                }

            },
        });

}
$(document).on('click','.applyOnlineClass',function () {
    var rel=$(this).attr('rel');
    $("#applyonlineForm").modal('show');
    fetchAllCarrier();
    fetchAllCountry();
    $("#move-in-date").datepicker();
    jQuery('#phone_number_id').mask('000-000-0000', {reverse: true});
    $("#propertyIdHidden").val(rel);
    getBuilding();

})

function fetchAllCarrier() {
    $.ajax({
        type: 'post',
        url: '/properties-Ajax',
        data: {
            class: 'properties',
            action: 'fetchAllCarrierAjax'},
        success: function (response) {
            var res = JSON.parse(response);
            $('.additional_carrier').html(res.data);
        },
    });
}
function fetchAllCountry() {
    $.ajax({
        type: 'post',
        url: '/properties-Ajax',
        data: {
            class: 'properties',
            action: 'fetchAllCountryAjax'},
        success: function (response) {
            var res = JSON.parse(response);
            $('.additional_country').html(res.data);
        },
    });
}
function getBuilding() {
    var property_id= $("#propertyIdHidden").val();
    $.ajax({
        type: 'post',
        url: '/properties-Ajax',
        data: {
            class: 'properties',
            action: 'getBuildingAjax',
            property_id:property_id
        },
        success: function (response) {
            var res = JSON.parse(response);
            $('#ddlApplyBuilding').html(res.data);
            alert(res.propertydata.property_name);
            $('.ddlApplyPropertyClass').val(res.propertydata.property_name)
        },
    });
}
$(document).on('change','#ddlApplyBuilding',function () {
    getUnit(this.value);
})

function getUnit(id) {
    $.ajax({
        type: 'post',
        url: '/properties-Ajax',
        data: {
            class: 'properties',
            action: 'getUnitAjax',
            id:id
        },
        success: function (response) {
            var res = JSON.parse(response);
            $('#ddlApplyUnit').html(res.data);
        },
    });
}
$(document).on("submit","#apply_online_id",function (e) {
    e.preventDefault();
    if ($('#apply_online_id').valid()) {
        var formData = $('#apply_online_id').serializeArray();
        $.ajax({
            type: 'post',
            url: '/properties-Ajax',
            data: {
                form: formData,
                class: 'properties',
                action: 'addApplyOnline'

            },
            success: function (result) {
                var response = JSON.parse(result);
                if ((response.status == 'success') && (response.code == 200)) {
                    toastr.success(response.message);
                    $("#applyonlineForm").modal('hide');
                    $("#apply_online_id").trigger('reset');
                }
                else if (response.code == '400') {
                    toastr.warning(response.message);
                }
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }

});
$(document).on("click","#btnApplyCancel",function () {
    bootbox.confirm({
        message: "Do you want to Cancel this ?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $("#applyonlineForm").modal('hide');
            }
        }
    });
});