$(document).ready(function () {

    jqGrid('All',true);

})

function jqGrid(status, deleted_at) {


    var getPrice=  localStorage.getItem("price");
    var getProperty_type_options=    localStorage.getItem("property_type_options");
    var getBedrooms=    localStorage.getItem("Bedrooms");
    var getBathrooms=    localStorage.getItem("Bathrooms");
    var getAmenties=    (localStorage.getItem("amenties") == null)?'':localStorage.getItem("amenties");
    var getSq_feet=   localStorage.getItem("sq_feet");
    var getZip_code_search=   localStorage.getItem("zip_code_search");
    var table = 'general_property';
    var columns = ['Property Name', 'Property Type', 'Minimum Unit Price', 'No. of Bedrooms', 'No. of Bathrooms', 'Square Feet', 'Zip Code', 'City', 'State', 'Country','Amenities','Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [{table: 'general_property', column: 'property_type', primary: 'id', on_table: 'company_property_type'},{table: 'general_property', column: 'id', primary: 'property_id', on_table: 'unit_details'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [{column: 'property_type', value: getProperty_type_options, condition: '='},{column: 'property_price', value: getPrice, condition: '='},
        {column: 'property_squareFootage', value: getSq_feet, condition: '='},{column: 'zipcode', value: getZip_code_search, condition: '='}];
    var extra_columns = ['general_property.deleted_at', 'general_property.update_at'];
    var columns_options = [
        {name: 'Property Name', index: 'property_name',width:'200',  align: "left", searchoptions: {sopt: conditions}, table: table},
        {name: 'Property Type', index: 'property_type',width:'200',  align: "left", searchoptions: {sopt: conditions}, table: 'company_property_type'},
        {name: 'Minimum Unit Price', index: 'base_rent', width: 200, align: "left", searchoptions: {sopt: conditions}, table: 'unit_details'},
        {name: 'No. of Bedrooms', index: 'bedrooms_no',  align: "left", searchoptions: {sopt: conditions}, table: 'unit_details'},
        {name: 'No. of Bathrooms', index: 'bathrooms_no',  align: "left", searchoptions: {sopt: conditions}, table: 'unit_details'},
        {name: 'Square Feet', index: 'property_squareFootage',  align: "left", searchoptions: {sopt: conditions}, table: table},
        {name: 'Zip Code', index: 'zipcode',  align: "left", searchoptions: {sopt: conditions}, table: table},
        {name: 'City', index: 'city', align: "left", searchoptions: {sopt: conditions}, table: table},
        {name: 'State', index: 'state',  align: "left", searchoptions: {sopt: conditions}, table: table},
        {name: 'Country', index: 'country',  align: "left", searchoptions: {sopt: conditions}, table: table, formatter: statusFormatter},
        {name: 'Amenities',width:'500', index: 'amenities', title: false, align: "left", sortable: false, search: false, table: table,change_type:'serialize',type:'line',secondTable: 'company_property_amenities', index2: 'name',searchData:getAmenties},
        {name: 'Action', index: 'select', title: false, align: "center", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: ActionGridFormatter, edittype: 'select', search: false, table: table}
    ];
    var ignore_array = [];
    jQuery("#jqGrid-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            deleted_at: deleted_at,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'general_property.update_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: 10,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 10, left: 400, drag: true, resize: false} // search options
    );
}


function statusFormatter(cellValue, options, rowObject) {
    if (cellValue == 1)
        return "Active";
    else if (cellValue == '0')
        return "InActive";
    else
        return '';
}

function ActionGridFormatter(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {
        var upload_url = window.location.origin;
        var src = upload_url + '/company/images/view.png';
        var  src2 = upload_url + '/company/images/map-icon.png';
        var href1="/PropertyListing/PropertyDetails?id="+rowObject.id;

        var data = '<a title="View Detail" class="ancViewDetail"  href="'+href1+'"><img src="'+src+'"></a>\n' +
            ' <a title="View Map"  class="ancViewMap" href="JAVASCRIPT:">\n' +
            '<img src="'+src2+'">';
        return data;
    }
}