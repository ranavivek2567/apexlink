<?php

include_once( ROOT_URL . "/company/functions/FlashMessage.php");
//include_once (COMPANY_DIRECTORY_URL.'/constants.php');
include_once ('constants.php');
$request = $_SERVER['REQUEST_URI'];

//$request = preg_replace('{/$}', '', $request);
//print_r($request);die();
$request1 = explode('/', $_SERVER["REQUEST_URI"]);
$key = end($request1);
if (is_numeric($key)) {
    array_pop($request1);
    $request = implode('/', $request1);
}

$var1 = strrpos($request, '?');
if (!empty($var1)) {
    $url = explode('?', $request);
    array_pop($url);
    $request = end($url);
}
//print_r(__DIR__);die();
switch ($request) {
    case '/' :
        require __DIR__ . '/views/homePage.php';
        break;
    case '/HomePage-Ajax' :
        require __DIR__ . '/functions/home/homePage.php';
        break;
    case '/Home/SearchResult' :
        require __DIR__ . '/views/home/searchResult.php';
        break;
    case '/List/jqgrid' :
        require __DIR__ . '/functions/jqgrid.php';
        break;

    case '/propertylisting/Campaign' :
        require __DIR__ . '/views/campaign/campaign.php';
        break;
    case '/home/ContactUs' :
        require __DIR__ . '/views/contact/contactUs.php';
        break;
    case '/campaign-Ajax' :
        require __DIR__ . '/functions/campaign/campaign.php';
        break;
    case '/contact-Ajax' :
        require __DIR__ . '/functions/contact/contact.php';
        break;
    case '/properties-Ajax' :
        require __DIR__ . '/functions/properties/properties.php';
        break;
    case '/propertylisting/ourproperty' :
        require __DIR__ . '/views/properties/ourProperty.php';
        break;
    case '/PropertyListing/PropertyDetails' :
        require __DIR__ . '/views/flyer/viewFlyer.php';
        break;
    case '/flyer-Ajax' :
        require __DIR__ . '/functions/flyer/flyer.php';
        break;
}