<?php
include(ROOT_URL."/config.php");
include_once( ROOT_URL."/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class homePage extends DBConnection {

    /**
     * forgotPassword constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     *  function for company user forgot password
     */
    public function checkSitePublished() {
        try{
            $query = $this->companyConnection->query("SELECT * FROM marketing_contact_detail ");
            $data = $query->fetch();
            return array('status' => 'success', 'code' => 200, 'data' => $data);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function fetchPropertytype() {
        $html = '';
        $sql = "SELECT * FROM company_property_type";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html.='<option value=""></option>';
        foreach ($data as $d) {
            $html.= '<option value='.$d['id'].'>' . $d['property_type'] . '</option>';
        }
        return array('data' => $html, 'status' => 'success');
    }

    public function fetchAllAmenities() {
        $html = '';
        $sql = "SELECT * FROM company_property_amenities";
        $data = $this->companyConnection->query($sql)->fetchAll();
        foreach ($data as $d) {
            $name = $d['name'];
            $idd = $d['id'];
            $html.= '<option value=' . $idd . '>' . $name . '</option>';
        }
        return array('data' => $html, 'status' => 'success');
    }
    public function featuredListingAjax(){
        $html = '';
        $sql = "SELECT gp.id,gp.property_name,gp.vacant FROM general_property as gp JOIN marketing_posts as mp ON gp.id=mp.property_id WHERE mp.is_featured='yes'";
        $data = $this->companyConnection->query($sql)->fetchAll();
        foreach ($data as $d) {
            $name = $d['property_name'];
            $vacant = $d['vacant'];
            $html.= '<a title="View Details" href="/PropertyListing/PropertyDetails?id='.$d['id'].'" class="ancViewProperty" propertyid="280"><li>'.$name.'<span>-'.$vacant.' Vacant Units</span></li></a>';
        }
        return array('html' => $html, 'status' => 'success');
    }


    /**
     *  function for super admin user reset password
     */


}

$homePage = new homePage();
