<?php
include(ROOT_URL."/config.php");
include_once( ROOT_URL."/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class flyer extends DBConnection {

    /**
     * forgotPassword constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     *  function for company user forgot password
     */
    public function getTemplate() {
        try{
            $query = $this->companyConnection->query("SELECT * FROM flyer_posts WHERE status='1'");
            $data = $query->fetch();

            $propertyData=$this->companyConnection->query("SELECT gp.id,gp.property_squareFootage,gp.online_application,gp.property_for_sale,gp.property_name, gp.property_price, gp.property_year, gp.property_squareFootage, gp.address1, gp.address2, gp.address3, gp.address4, gp.no_of_buildings, gp.no_of_units, gp.posting_type, gp.vacant, gp.amenities, gp.description, ud.unit_no, ud.unit_type_id,ud.bedrooms_no,ud.bathrooms_no,cpf.pet_friendly, cpt.property_type, cps.property_style FROM general_property AS gp LEFT JOIN unit_details AS ud ON gp.id = ud.property_id JOIN company_pet_friendly AS cpf ON cpf.id = gp.pet_friendly JOIN company_property_type AS cpt ON cpt.id = gp.property_type JOIN company_property_style AS cps ON cps.id = gp.property_style WHERE gp.id =".$_POST['id']);
            $propertyDataRes = $propertyData->fetch();

            if($propertyDataRes['property_for_sale'] == 'Yes'){
                $listing='For Sale';
            }else{
                $listing='For Rent';
            }

            $unitData=$this->companyConnection->query("SELECT cut.unit_type FROM unit_details AS ud LEFT JOIN company_unit_type AS cut ON cut.id = ud.unit_type_id WHERE ud.property_id =".$_POST['id']);
            $unitDataRes = $unitData->fetch();
            $amenHtml='';
            $img=COMPANY_SITE_URL."/images/li-bg.png";
            if(!empty($propertyDataRes['amenities'])){

                foreach (unserialize($propertyDataRes['amenities']) as $amenity) {
                    $amendata=$this->companyConnection->query("SELECT name FROM company_property_amenities WHERE id=".$amenity);
                    $amendataRes = $amendata->fetch();
                    $amenHtml .='<li><img src="'.$img.'">'.$amendataRes['name'].'</li>';
                }
            }



            $unitHtml='';
            $unitViewData=$this->companyConnection->query("SELECT ud.unit_prefix,ud.unit_no FROM general_property as gp LEFT JOIN unit_details as ud ON gp.id=ud.property_id WHERE gp.id=".$_POST['id']);
            $unitViewDataRes = $unitViewData->fetchAll();
            if(!empty($unitViewDataRes)){
                foreach ($unitViewDataRes as $u){
                    $unt=$u['unit_prefix'].''.$u['unit_no'];
                    $unitHtml .='<li><img src="'.$img.'">'.$unt.'</li>';
                }
            }

            $propertyImages = $this->companyConnection->query("SELECT * FROM property_file_uploads WHERE property_id='".$_POST['id']."' and file_type='1'");
            $propertyImagesRes = $propertyImages->fetchAll();
            $slider='';
            $activeImage = '';
            if(!empty($propertyImagesRes)){
                foreach ($propertyImagesRes as $key=>$propertyImg){
                    $propImageSingle=COMPANY_SITE_URL.'/'.$propertyImg['file_location'];
                    if($key == 0){
                        $activeImage = $propImageSingle;
                    }
                    $slider .='<div class="item"><img class="clickedImage" src="'.$propImageSingle.'"></div>';
                }
            }else{
                $slider .='<div class="item"><img src=""></div>';
            }
            $applyOnline='';
            if($propertyDataRes['online_application'] == 'on'){
                $applyOnline .='<a rel="" class="btn blue-btn applyOnlineClass">Apply Online</a>';
            }else{
                $applyOnline .='';
            }


            $mapQuery = $this->companyConnection->query("SELECT mp.show_on_map FROM  general_property as gp JOIN marketing_posts as mp ON gp.id=mp.property_id WHERE gp.id='".$_POST['id']."'");
            $mapQueryRes = $mapQuery->fetch();
            if($mapQueryRes['show_on_map'] =='yes'){
                $map='<a class="showMapClass" rel="">Show On map</a>';
            }else{
                $map='';
            }


            $fulladdress=$propertyDataRes['address1'].''.$propertyDataRes['address2'].''.$propertyDataRes['address3'].''.$propertyDataRes['address4'];
            $template=$data['template'];
            $template = str_replace("{{propertyName}}", $propertyDataRes['property_name'], $template);
            $template = str_replace("{{propertyPrice}}", $propertyDataRes['property_price'], $template);
            $template = str_replace("{{propertyYear}}", $propertyDataRes['property_year'], $template);
            $template = str_replace("{{listingType}}", $listing, $template);
            $template = str_replace("{{propertyType}}", $propertyDataRes['property_type'], $template);
            $template = str_replace("{{postingTitle}}", $propertyDataRes['posting_type'], $template);
            $template = str_replace("{{numberOfBuilding}}", $propertyDataRes['no_of_buildings'], $template);
            $template = str_replace("{{propertyAddress}}", $fulladdress, $template);
            $template = str_replace("{{unitType}}", $unitDataRes['unit_type'], $template);
            $template = str_replace("{{propertyStyle}}", $propertyDataRes['property_style'], $template);
            $template = str_replace("{{numberOfUnits}}", $propertyDataRes['no_of_units'], $template);
            $template = str_replace("{{vacantUnits}}", $propertyDataRes['vacant'], $template);
            $template = str_replace("{{petAllowed}}", $propertyDataRes['pet_friendly'], $template);
            $template = str_replace("{{parking}}", $propertyDataRes['property_name'], $template);
            $template = str_replace("{{description}}", $propertyDataRes['description'], $template);
            $template = str_replace("{{amenities}}", $amenHtml, $template);
            $template = str_replace("{{name}}", 'John', $template);
            $template = str_replace("{{phoneNumber}}", '012-344-5555', $template);
            $template = str_replace("{{email}}", 'john@yopmail.com', $template);
            $template = str_replace("{{units}}", $unitHtml, $template);
            $template = str_replace("{{propertyImages}}", $slider, $template);
            $template = str_replace("{{activeImage}}", $activeImage, $template);
            $template = str_replace("{{applyOnline}}", $applyOnline, $template);
            $template = str_replace("{{map}}", $map, $template);
            $template = str_replace("{{sgfoot}}", $propertyDataRes['property_squareFootage'], $template);
            $template = str_replace("{{bedroom}}", $propertyDataRes['bedrooms_no'], $template);
            $template = str_replace("{{bathroom}}", $propertyDataRes['bathrooms_no'], $template);


            return array('status' => 'success', 'code' => 200, 'data' => $template);

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function mapLatLongAjax(){
        try{

            $mapQuery = $this->companyConnection->query("SELECT ma.latitude,ma.longitude,gp.property_name FROM  map_address as ma JOIN general_property as gp on gp.id=ma.property_id 
                      WHERE ma.property_id=".$_POST['id']);
            $mapQueryRes = $mapQuery->fetch();
            return array('status' => 'success', 'code' => 200, 'data' => $mapQueryRes);
        }catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


    /**
     *  function for super admin user reset password
     */


}

$flyer = new flyer();
